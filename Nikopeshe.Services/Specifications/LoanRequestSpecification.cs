﻿using Nikopeshe.Entity;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Specifications
{
    public class LoanRequestSpecification
    {
        public static Specification<LoanRequest> DefaultSpec(string text)
        {
            Specification<LoanRequest> specification = new TrueSpecification<LoanRequest>();

            if (!String.IsNullOrWhiteSpace(text))
            {
                var primaryAccountNumber = new DirectSpecification<LoanRequest>(c => c.PrimaryAccountNumber.Contains(text));

                var loanReferenceNumber = new DirectSpecification<LoanRequest>(c => c.LoanReferenceNumber.Contains(text));                

                specification &= (primaryAccountNumber | loanReferenceNumber);
            }

            return specification;
        }

        public static Specification<LoanRequest> CustomerLookUpDetailsSpecifications(int status, string text)
        {
            Specification<LoanRequest> specification = new DirectSpecification<LoanRequest>(c => c.Status == status);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var customerMobileNumber = new DirectSpecification<LoanRequest>(x => x.PrimaryAccountNumber.Contains(text));

                var loanReferenceNumber = new DirectSpecification<LoanRequest>(x => x.LoanReferenceNumber.Contains(text));

                specification &= (customerMobileNumber | loanReferenceNumber);
            }

            return specification;
        }

        public static Specification<LoanRequest> LoanRequestByBranchId(Guid branchId, string text)
        {
            Specification<LoanRequest> specification = new DirectSpecification<LoanRequest>(c => c.BranchId == branchId);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var loanReferenceNoSpec = new DirectSpecification<LoanRequest>(c => c.LoanReferenceNumber.Contains(text));

                var primaryAccountNumberSpec = new DirectSpecification<LoanRequest>(c => c.PrimaryAccountNumber.Contains(text));                

                specification &= (loanReferenceNoSpec | primaryAccountNumberSpec);
            }

            return specification;

        }

        public static Specification<LoanRequest> LoanDisbursementList(string text)
        {
            Specification<LoanRequest> specification = new DirectSpecification<LoanRequest>(x => x.Status != (int)Enumerations.LoanStatus.CustomerRejected || x.Status == (int)Enumerations.LoanStatus.Rejected);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var loanReferenceNoSpec = new DirectSpecification<LoanRequest>(c => c.LoanReferenceNumber.Contains(text));

                var primaryAccountNumberSpec = new DirectSpecification<LoanRequest>(c => c.PrimaryAccountNumber.Contains(text));

                specification &= (loanReferenceNoSpec | primaryAccountNumberSpec);
            }

            return specification;

        }


        public static ISpecification<LoanRequest> LoanRequestByGroupId(Guid id, string text)
        {
            Specification<LoanRequest> specification = new DirectSpecification<LoanRequest>(c => c.CustomerAccountId == id);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var loanReferenceNoSpec = new DirectSpecification<LoanRequest>(c => c.LoanReferenceNumber.Contains(text));

                var primaryAccountNumberSpec = new DirectSpecification<LoanRequest>(c => c.PrimaryAccountNumber.Contains(text));

                specification &= (loanReferenceNoSpec | primaryAccountNumberSpec);
            }

            return specification;
        }

        public static Specification<LoanRequest> PendingDisbursementList(int status, string text)
        {
            Specification<LoanRequest> specification = new DirectSpecification<LoanRequest>(x => x.Status == status);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var loanReferenceNoSpec = new DirectSpecification<LoanRequest>(c => c.CustomerAccount.Customer.IDNumber.Contains(text));

                var primaryAccountNumberSpec = new DirectSpecification<LoanRequest>(c => c.PrimaryAccountNumber.Contains(text));

                specification &= (loanReferenceNoSpec | primaryAccountNumberSpec);
            }

            return specification;

        }

    }
}

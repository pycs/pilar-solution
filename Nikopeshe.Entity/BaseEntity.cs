﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity
{
    [Serializable]
    public class BaseEntity
    {
        public BaseEntity()
        {
            
            Id = Guid.NewGuid();
            CreatedDate = DateTime.Now;
        }
        /// <summary>
        /// Primary Key
        /// </summary>
        /// 
        [Index("ID_Primary_Key")]
        [Key]
        public Guid Id { get; set; }

        [Display(Name = "Created Date")]
        [Index("CreatedDate")]
        public DateTime CreatedDate { get; set; }
    }
}

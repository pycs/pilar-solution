﻿using Nikopeshe.Entity.DataModels;
using System;

namespace Nikopeshe.Entity
{
    public class CustomerLookUpDetails
    {
        public Guid LoanRequestId { get; set; }

        public Customer Customer { get; set; }

        public string CustomerName {get;set;}

        public string CustomerMobileNumber { get; set; }

        public string CustomeIdNumber { get; set; }

        public LoanRequest LoanRequest { get; set; }

        public decimal LoanAmount{get;set;}

        public decimal TotalPaidAmount { get; set; }

        public decimal LoanBalance { get; set; }

        public string LoanReferenceNumber { get; set; }

        public DateTime? NextPaymentDate { get; set; }

        public decimal InstallmentAmount { get; set; }

        public Guid CustomerId { get; set; }
    }
}

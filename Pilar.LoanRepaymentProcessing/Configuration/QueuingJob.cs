﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.Repayments;
using Quartz;
using System;
using System.Configuration;
using System.Messaging;

namespace Pilar.LoanRepaymentProcessing.Configuration
{
    public class QueuingJob : IJob
    {
        private readonly IMessageQueueService _messageQueueService;
        private readonly string _messageRelayQueuePath;
        private readonly IRepayment iRepaymentService;
        private readonly DataContext datacontext = new DataContext();

        public QueuingJob()
        {
            iRepaymentService = new Repayment();
            _messageQueueService = new MessageQueueService();
            _messageRelayQueuePath = ConfigurationManager.AppSettings["LoanProcessingQueuePath"];
        }
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var loanDueDoday= iRepaymentService.GetLoanRequestsDueToday();
                if (loanDueDoday.Count != 0)
                {
                    foreach (var item in loanDueDoday)
                    {
                        _messageQueueService.Send(_messageRelayQueuePath, item.Id, Enumerations.MessageCategory.LoanProcessing, MessagePriority.AboveNormal);
                    }
                }
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogError("Error -> ", ex);
            }
        }
    }
}

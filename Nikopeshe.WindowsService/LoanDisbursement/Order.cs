﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.WindowsService.LoanDisbursement
{
    public class Order
    {
        public int Type { get; set; }
      
        public string TypeDesc { get; set; }

        public Guid CompanyId { get; set; }
        
        public string CompanyDesc { get; set; }

        public string Remarks { get; set; }

        public bool IsDelivered { get; set; }

        public List<OrderLine> OrderLines { get; set; }
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.CustomerService
{
    public class CustomerService : ICustomerService
    {
        private DataContext datacontext = new DataContext();

        public Customer FindCustomerByMobileNumber(string MobileNumber)
        {
            if (string.IsNullOrWhiteSpace(MobileNumber))
                throw new ArgumentNullException("Mobile number cannot be null");

            var findcustomerwithmobilenumber = datacontext.Customer.Where(x => x.MobileNumber == MobileNumber).FirstOrDefault();

            return findcustomerwithmobilenumber;
        }

        public Customer FindCustomerByIdNumber(string IdNumber)
        {
            if (string.IsNullOrWhiteSpace(IdNumber))
                throw new ArgumentNullException("Id Number cannot be null");

            var findcustomerbyidnumber = datacontext.Customer.Where(x => x.IDNumber == IdNumber).FirstOrDefault();

            return findcustomerbyidnumber;
        }

        public decimal CustomerLoanBalance(Guid customerId)
        {
            decimal loanAccountBalance = 0m;

            var customerAccountId = datacontext.CustomersAccount.Where(x => x.CustomerId == customerId && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();

            if (customerAccountId != null)
            {
                var idStartDate = new SqlParameter { ParameterName = "@CustomerAccountID", Value = customerAccountId.Id };

                var results = datacontext.Database.SqlQuery<CustomerAccountInfo>("sp_Customer_Savings_Loan_AccountBalance @CustomerAccountID", idStartDate).Cast<CustomerAccountInfo>().First();

                loanAccountBalance = results.TotalAmountDue;
            }

            return loanAccountBalance;
        }

    }
}

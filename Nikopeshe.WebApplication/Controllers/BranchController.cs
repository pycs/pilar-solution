﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize(Roles = "Administrator")]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class BranchController : BaseController
    {
        private readonly IBranchService branchservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public BranchController(IBranchService _branchservice)
        {
            if (_branchservice == null)
                throw new ArgumentNullException("service cannot be null.");

            this.branchservice = _branchservice;
        }

        // GET: Branch
        public ActionResult Index()
        {            
            return View();
        }

        public JsonResult GetAllBranches(JQueryDataTablesModel datatablemodel)
        {
            var pagecollection = branchservice.GetAllBranches();

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var _brancheslist = GetAllC2BTransactions(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, brancheslist: pagecollection);


            return Json(new JQueryDataTablesResponse<Branch>(items: _brancheslist,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public IList<Branch> GetAllC2BTransactions(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<Branch> brancheslist)
        {
            var _brancheslist = brancheslist;

            totalRecordCount = _brancheslist.Count();

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                _brancheslist = _brancheslist.Where(c => c.BranchName
                    .ToLower().Contains(searchString.ToLower()) || c.BranchCode.ToString().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = _brancheslist.Count;

            IOrderedEnumerable<Branch> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? _brancheslist.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "BranchName":
                        sortedClients = sortedClients == null ? _brancheslist.CustomSort(sortedColumn.Direction, cust => cust.BranchName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.BranchName);
                        break;

                    case "Location":
                        sortedClients = sortedClients == null ? _brancheslist.CustomSort(sortedColumn.Direction, cust => cust.Location)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Location);
                        break;

                    case "TypeDesc":
                        sortedClients = sortedClients == null ? _brancheslist.CustomSort(sortedColumn.Direction, cust => cust.TypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TypeDesc);
                        break;

                    case "C2BNumber":
                        sortedClients = sortedClients == null ? _brancheslist.CustomSort(sortedColumn.Direction, cust => cust.C2BNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.C2BNumber);
                        break;

                    case "BranchCode":
                        sortedClients = sortedClients == null ? _brancheslist.CustomSort(sortedColumn.Direction, cust => cust.BranchCode)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.BranchCode);
                        break;

                    case "IsEnabled":
                        sortedClients = sortedClients == null ? _brancheslist.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled);
                        break;                    
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }    
    
        [HttpGet]
        public ActionResult AddNewBranch()
        {
            ViewBag.BranchType = BranchType(null);
            return View();
        }

        [HttpPost]        
        public ActionResult AddNewBranch(Branch branch)
        {
            ViewBag.BranchType = BranchType(null);

            if (ModelState.IsValid)
            {
                try
                {
                    var findbranchbybranchcode = branchservice.GetAllBranches().Where(x => x.BranchCode == branch.BranchCode);
                    if (findbranchbybranchcode.Count() >= 1)
                    {
                        ModelState.AddModelError(string.Empty, string.Format("A branch with code {0} already exist.", branch.BranchCode));
                        return View(branch);
                    }
                    if (branch.BranchCode.Length != 3)
                    {
                        ModelState.AddModelError(string.Empty, "Branch code must be 3 characters long.");
                        return View(branch);    
                    }
                    branchservice.AddNewBranch(branch, serviceHeader);
                }
                catch(Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(branch);
                }
                return RedirectToAction("Index");
            }
            return View(branch);
        }

        [HttpGet]
        public ActionResult UpdateBranch(Guid Id)
        {
            var findbranchbyid = branchservice.FindBranchById(Id);
            if (findbranchbyid == null)
            {
                ModelState.AddModelError(string.Empty, "Could not find the branch.");
            }
            ViewBag.BranchType = BranchType(findbranchbyid);

            if (Id == null)
                throw new ArgumentNullException("Id cannot be null");

            return View(findbranchbyid);
        }

        [HttpPost]
        [ActionName("UpdateBranch")]        
        public ActionResult UpdateBranch(Branch branch)
        {
            var findbranchbyid = branchservice.FindBranchById(branch.Id);
            if (findbranchbyid == null)
            {
                ModelState.AddModelError(string.Empty, "Could not find the branch.");
            }
            ViewBag.BranchType = BranchType(findbranchbyid);

            if (ModelState.IsValid)
            {
                try
                {
                    if (branch.BranchCode.Length != 3)
                    {
                        ModelState.AddModelError(string.Empty, "Branch code must be 3 characters long.");
                        return View(branch);
                    }
                    
                    var findbranchbybranchcode = branchservice.GetAllBranches().Where(x => x.BranchCode == branch.BranchCode).Count();
                    if (findbranchbybranchcode > 1)
                    {
                        ModelState.AddModelError(string.Empty, string.Format("A branch with code {0} already exist", branch.BranchCode));
                        return View(branch);
                    }
                    findbranchbyid.BranchCode = branch.BranchCode;
                    findbranchbyid.Location = branch.Location;
                    findbranchbyid.C2BNumber = branch.C2BNumber;
                    findbranchbyid.BranchName = branch.BranchName;
                    findbranchbyid.IsEnabled = branch.IsEnabled;
                    branchservice.Update(branch, serviceHeader);
                    return RedirectToAction("Index");
                }

                catch(Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(branch);
                }                
            }
            return View(branch);
        }
    }
}
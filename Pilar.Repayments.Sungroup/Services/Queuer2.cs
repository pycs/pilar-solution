﻿using Crosscutting.NetFramework.Logging;
using Pilar.Services.Services;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Repayments.Sungroup.Services
{
    [Export(typeof(IPlugin))]
    public class Queuer2 : IPlugin
    {
        private readonly IScheduler _scheduler;

        public Queuer2()
        {
            // Get a reference to the scheduler
            var sf = new StdSchedulerFactory();

            // Get an instance of the Quartz.Net scheduler
            _scheduler = sf.GetScheduler();
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{373787B0-CFF5-4E61-8A53-CC90917F1DD7}"); }
        }

        public string Description
        {
            get { return "C2B PROCESSING QUEUER"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                // Start the scheduler if its in standby
                if (!_scheduler.IsStarted)
                    _scheduler.Start();

                // Define the Job to be scheduled
                var job = JobBuilder.Create<Configuration.IPNQueuer2>()
                    .WithIdentity("IPNProcessingQeueingJob", "Pilar")
                    .RequestRecovery()
                    .Build();

                // Associate a trigger with the Job
                var cronExpression = ConfigurationManager.AppSettings["C2BPostingQueueingJobCronExpression"];
                var trigger = (ICronTrigger)TriggerBuilder.Create()
                    .WithIdentity("IPNProcessingQeueingJob", "Pilar")
                    .WithCronSchedule(cronExpression)
                    .StartAt(DateTime.UtcNow)
                    .WithPriority(1)
                    .Build();

                // Validate that the job doesn't already exist
                if (_scheduler.CheckExists(new JobKey("IPNProcessingQeueingJob", "Pilar")))
                {
                    _scheduler.DeleteJob(new JobKey("EmailQueueingJob", "Pilar"));
                }

                var schedule = _scheduler.ScheduleJob(job, trigger);

#if DEBUG
                //Logger.CreateLog("Job '{0}' scheduled for '{1}'" +"EmailQueueingJob" + schedule.ToString("r"));
#endif
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        public void Exit()
        {
            _scheduler.Shutdown();
        }

        #endregion
    }
}

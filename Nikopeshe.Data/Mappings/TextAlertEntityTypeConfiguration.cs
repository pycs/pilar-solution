﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Data.Mappings
{
    public class TextAlertEntityTypeConfiguration : EntityTypeConfiguration<TextAlert>
    {
        public TextAlertEntityTypeConfiguration()
        {
            this.HasKey(x => x.Id);
            ToTable(string.Format("{0}TextAlerts", DefaultSettings.Instance.TablePrefix));
        }
    }
}

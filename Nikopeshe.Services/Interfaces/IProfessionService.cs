﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface IProfessionService
    {
        void AddNewProfessio(Profession profession, ServiceHeader serviceHeader);

        void UpdateProfession(Profession profession, ServiceHeader serviceHeader);

        void DeleteProfession(Profession profession, ServiceHeader serviceHeader);

        IList<Profession> GetAllProfessions(IsolationLevel isolationLevel);

        Profession FindProfessionById(Guid Id);
    }
}

﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Data.Mappings
{
    public class PasswordRecoveryRequestTypeConfiguration : EntityTypeConfiguration<PasswordRecoveryRequest>
    {
        public PasswordRecoveryRequestTypeConfiguration()
        {
            this.HasKey(x => x.Id);
            ToTable(string.Format("{0}PasswordRecoveryRequests", DefaultSettings.Instance.TablePrefix));
        }
    }
}

﻿using Nikopeshe.Services.Interfaces;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nikopeshe.Data;
using System.Data.Entity.Validation;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class CollateralService : ICollateralService
    {
        private IRepository<Collateral> collateralRepository;

        string ErrorMessage = string.Empty;
        public CollateralService(IRepository<Collateral> _collateralRepository)
        {
            if (_collateralRepository == null)
                throw new ArgumentNullException("null repository");

            this.collateralRepository = _collateralRepository;
        }

        public void AddNewCollateral(Collateral collateral, ServiceHeader serviceHeader)
        {
            try
            {
                if (collateral == null)
                    throw new ArgumentNullException("null entity");

                collateralRepository.Insert(collateral, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }  
        }

    }
}

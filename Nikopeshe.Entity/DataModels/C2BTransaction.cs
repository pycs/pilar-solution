﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{  
    [Serializable]
    public class C2BTransaction : BaseEntity
    {
        [Column(TypeName = "VARCHAR")]
        [Index]
        [MaxLength(15)]
        public string MSISDN { get; set; }

        [Index(IsUnique = true)]
        public Guid TransactionId { get; set; }

        [MaxLength(20)]
        public string BusinessShortCode { get; set; }

        [MaxLength(100)]
        public string InvoiceNumber { get; set; }

        [Column(TypeName = "VARCHAR")]
        [Index("TransID", IsUnique = true)]
        [MaxLength(200)]
        public string TransID { get; set; }
        
        [Index]
        public Decimal TransAmount { get; set; }

        [Column(TypeName = "VARCHAR")]
        [Index]
        [MaxLength(100)]
        public string ThirdPartyTransID { get; set; }

        [MaxLength(100)]
        public string TransTime { get; set; }

        [Column(TypeName = "VARCHAR")]        
        [MaxLength(100)]
        public string BillRefNumber { get; set; }

        [MaxLength(100)]
        public string KYCInfo_FirstName { get; set; }

        [MaxLength(100)]
        public string KYCInfo_MiddleName { get; set; }

        [MaxLength(100)]
        public string KYCInfo_LastName { get; set; }

        public string FullName
        {
            get
            {
                return string.Format("{0} {1} {2}", KYCInfo_FirstName, KYCInfo_MiddleName, KYCInfo_LastName);
            }
        }
        
        [Index]
        public int Status { get; set; }

        public string StatusDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.C2BTransactionStatus), Status) ? EnumHelper.GetDescription((Enumerations.C2BTransactionStatus)Status) : string.Empty;
            }
        }

        public string Result { get; set; }

        [NotMapped]
        public string Commands { get; set; }
               
        public string FileName { get; set; }
    }
}

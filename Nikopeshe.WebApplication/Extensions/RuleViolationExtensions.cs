﻿using Infrastructure.Crosscutting.NetFramework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Nikopeshe.WebApplication.Extensions
{
    public static class RuleViolationExtensions
    {
        public static string GetProperty(this RuleViolation ruleViolation)
        {
            if (ruleViolation == null)
                throw new ArgumentNullException("ruleViolation");

            if (!(ruleViolation.PropertyExpression is Expression<Func<string>>))
                throw new ArgumentException("RuleViolation.PropertyExpression must be an Expression<Func<string>>");

            Expression<Func<string>> expression = (Expression<Func<string>>)(ruleViolation.PropertyExpression);

            Func<string> del = expression.Compile();

            string key = del();

            return key;
        }
    }
}
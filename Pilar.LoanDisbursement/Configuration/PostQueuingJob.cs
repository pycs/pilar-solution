﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.Disbursement;
using Quartz;
using System;
using System.Configuration;
using System.Linq;

namespace Pilar.LoanDisbursement.Configuration
{
    public class PostQueuingJob : IJob
    {
        private readonly IMessageQueueService _messageQueueService;
        private readonly ILoanProcessing _iloanprocessing;
        private readonly string _messageRelayQueuePath;
        private DataContext datacontext = new DataContext();
        public PostQueuingJob()
        {
            _messageQueueService = new MessageQueueService();
            _iloanprocessing = new LoanProcessing();
            _messageRelayQueuePath = ConfigurationManager.AppSettings["SPSQueuePath"];
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var findpendingdisbursementloanrequest = _iloanprocessing.GetPendingDisbursmentLoanRequest();
                if (findpendingdisbursementloanrequest != null && findpendingdisbursementloanrequest.Any())
                    foreach (var pendingloanrequest in findpendingdisbursementloanrequest)
                    {
                        _messageQueueService.Send(_messageRelayQueuePath, pendingloanrequest.Id, Nikopeshe.Entity.DataModels.Enumerations.MessageCategory.SPSPostRequest, System.Messaging.MessagePriority.High);                        
                        pendingloanrequest.ProcessingStatus = (int)Enumerations.ProcessingStatus.Queued;
                        //_iloanprocessing.UpdateLoanRequest(pendingloanrequest);
                    }                       
            }

            catch(Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }
    }
}

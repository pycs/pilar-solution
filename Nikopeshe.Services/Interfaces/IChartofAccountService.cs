﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;

namespace Nikopeshe.Services.Interfaces
{
    public interface IChartofAccountService
    {
        IList<ChartofAccount> GetChartofAccountByAccountCategory(int chartofaccountcategory);

        void AddNewChartofAccount(ChartofAccount chartofaccounts, ServiceHeader serviceHeader);

        void UpdateChartofAccount(ChartofAccount chartofaccount, ServiceHeader serviceHeader);

        ChartofAccount FindChartOfAccountByParentId(Guid ParentId);

        ChartofAccount FindChartofAccountById(Guid Id);

        IList<ChartofAccount> GetAllChartofAccounts();

        IList<ChartofAccount> GetChartofAccountByAccountType(int type);

        IList<ChartofAccount> GetAllChartofAccountByAccountType(int type);

        IList<ChartofAccount> GetChartOfAccountsByTypeAndCategory(int type1, int category);

        IList<ChartofAccount> GetChartOfAccountsByTypeAndCategory(int type1, int type2, int category);

        IList<LoanTransactionHistory> GetChartofAccountStatement(Guid ChartofAccountId);

        ChartofAccount FindChartofAccountByAccountCode(int AccountCode);

        bool ValidateAccountCode(int AccountCode);
        
    }
}

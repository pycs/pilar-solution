﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Infrastructure;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using Nikopeshe.WebApplication.Helpers;
using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Nikopeshe.WebApplication
{
    public class MvcApplication : System.Web.HttpApplication
    {       
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            MvcHandler.DisableMvcResponseHeader = true;            
            IFilterProvider[] providers = FilterProviders.Providers.ToArray();
            FilterProviders.Providers.Clear();
            FilterProviders.Providers.Add(new ExcludeFilterProvider(providers));
            
            // Lets MVC know that anytime there is a JQueryDataTablesModel as a parameter in an action to use the
            // JQueryDataTablesModelBinder when binding the model.
            ModelBinders.Binders.Add(typeof(JQueryDataTablesModel), new JQueryDataTablesModelBinder());

            var configCode = 3040;
            var modelCode = 3040;

            int.TryParse(ConfigurationManager.AppSettings["ConfigCode"], out configCode);
            int.TryParse(ConfigurationManager.AppSettings["ModelCode"], out modelCode);

            if (configCode == 3040)
            {
                SecurityHelpers.ProtectConfigurationFile();
            }
            else if (configCode == 4030)
            {
                SecurityHelpers.UnProtectConfigurationFile();
            }

            if (modelCode == 3040)
            {
                DatabaseHelper.ConfigureApplicationDatabase();
                DatabaseHelper.ConfigureMembershipDatabase();
                DatabaseHelper.CreateDefaultBranch();
                DatabaseHelper.StaticSettings();
                DatabaseHelper.InitializeChartOfAccounts();
            }
        }
             
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        protected void Application_PreSendRequestHeaders()
        {
            this.Response.Headers.Remove("Server");
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Server.ClearError();
            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "Error");
            routeData.Values.Add("action", "Index");            
            routeData.Values.Add("exception", exception);
            if (exception.GetType() == typeof(HttpException))
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", exception.Message + exception.InnerException + exception.StackTrace);                
                routeData.Values.Add("statusCode", ((HttpException)exception).GetHttpCode());
            }
            else
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", exception.Message + exception.InnerException + exception.StackTrace);
                routeData.Values.Add("statusCode", 400);                
            }                        
        }        
    }
}

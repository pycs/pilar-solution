﻿using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.IPNService
{
    public interface IIPNService
    {
        IList<C2BTransaction> FindC2BTransactionByStatus(int status);

        void UpdateC2BTransaction(C2BTransaction c2bmapping);

        C2BTransaction FindC2BTransactionById(Guid Id);

        C2BTransaction FindC2BTransactionByThirdPartyTransID(string ThirdPartyTransID);

        IList<LoanProductsCharge> GetRecurringLoanProductChargeByProductId(Guid LoanProductId);

        LoanTransactionHistory FindLoanTransactionHistoryByC2BTransactionId(Guid C2BTransactionId);

        CustomerAccountInfo CustomerSavingsAccountBalance(Guid CustomerSavingsAccountId);

        IList<C2BTransaction> FindC2BPendingVerificationAndProcessing();

        Tuple<decimal, decimal, decimal> ExpectedTotalPrincipalExpectedTotalInterestExpectedTotalCharges(Guid LoanRequestId);

        Tuple<decimal, decimal, decimal> PaidPrincipalPaidInterestPaidCharges(Guid LoanRequestId, Guid CustomerSavingAccountId, Guid LoanProductId);

        Tuple<decimal, decimal, decimal> ExpectedPrincipalExpectedInterestExpectedChargesGivenElapsedSchedules(Guid LoanRequestId, int ElapsedSchedules);

        IList<C2BTransaction> FindQueuedConsumerToBusinessTransactionsWithGivenElapseTimeInMinutes(int minutes);
    }
}

﻿using Crosscutting.NetFramework.Logging;
using Infrastructure.Crosscutting.NetFramework;
using Nikopeshe.Data;
using Nikopeshe.Data.Mappings;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.InterfaceImplementations;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class LoanManagerController : Controller
    {
        private readonly ILoanRequestService loanrequestService;
        private readonly ICustomerService customerservice;
        private readonly IEmailAlertService emailalerservice;
        private readonly ITextAlertService textalertservice;
        private readonly ISystemGeneralLedgerAccountMappingService systemglmappingservice;
        private readonly ISavingsProductService savingsproductservice;
        private readonly ICustomerAccountService customeraccountservice;
        private readonly ILoanProductService loanproductservice;
        private readonly ILoanTransactionHistoryService loantransactionhistoryservice;
        private readonly IChartofAccountService chartofaccountservice;
        private readonly ILoanProductsChargeService loanproductcharges;
        private readonly IGraduatedScaleService graduatedscaleservice;
        private readonly ILoanRequestChargeService loanrequestchargeservice;
        private readonly IStaticSettingService staticsettings;
        private readonly ILoanRequestDocumentsService loanrequestdocumentservice;
        private readonly ICustomerDocumentService customerdocumentservice;
        private readonly UserInfoService userInfoService;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public LoanManagerController(ILoanRequestService _loanrequestservice, ICustomerService _customerservice,
            ILoanProductService _loanproductservice, ISystemGeneralLedgerAccountMappingService _systemglmappingservice,
            ISavingsProductService _savingsproductservice, IChartofAccountService _chartofaccountservice,
            ICustomerAccountService _customeraccountservice, IEmailAlertService _emailalerservice,
            ITextAlertService _textalertservice, ILoanTransactionHistoryService _loantransactionhistoryservice,
            ILoanProductsChargeService _loanproductcharges, IGraduatedScaleService _graduatedscaleservice,
            ILoanRequestChargeService _loanrequestchargeservice, IStaticSettingService _staticsettings,
            ILoanRequestDocumentsService _loanrequestdocumentservice, ICustomerDocumentService _customerdocumentservice,
            UserInfoService _userInfoService)
        {
            if(_loanproductservice == null || _emailalerservice == null || _systemglmappingservice == null
                || _savingsproductservice == null || _chartofaccountservice == null || _textalertservice == null
                || _customeraccountservice == null || _loanrequestservice == null || _customerservice == null 
                || _loantransactionhistoryservice == null || _loanproductcharges == null || _graduatedscaleservice == null
                || _loanrequestchargeservice == null || _customerdocumentservice == null)
                throw new ArgumentNullException("null service reference");

            this.loanproductservice = _loanproductservice;
            this.emailalerservice = _emailalerservice;
            this.systemglmappingservice = _systemglmappingservice;
            this.savingsproductservice = _savingsproductservice;
            this.chartofaccountservice = _chartofaccountservice;
            this.textalertservice = _textalertservice;
            this.customeraccountservice = _customeraccountservice;
            this.loanrequestService = _loanrequestservice;
            this.customerservice = _customerservice;
            this.loantransactionhistoryservice = _loantransactionhistoryservice;
            this.loanproductcharges = _loanproductcharges;
            this.graduatedscaleservice = _graduatedscaleservice;
            this.loanrequestchargeservice = _loanrequestchargeservice;
            this.staticsettings = _staticsettings;
            this.userInfoService = _userInfoService;
            this.loanrequestdocumentservice = _loanrequestdocumentservice;
            this.customerdocumentservice = _customerdocumentservice;
        }

        // GET: LoanManager
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAllLoanRequest(JQueryDataTablesModel datatablemodel)
        {
            var findCurrentUserId = User.Identity.Name;
            var allloanrequestpagecollection = new List<LoanRequest>();

            if (findCurrentUserId != null)
            {
                var findCurrentUserInfo = userInfoService.FindUserByUserName(findCurrentUserId);
                if (findCurrentUserInfo != null)
                {
                    if (findCurrentUserInfo.Branch.BranchCode == "000")
                    {
                        allloanrequestpagecollection = loanrequestService.GetAllLoanRequests().ToList();
                    }
                    else
                    {
                        allloanrequestpagecollection = loanrequestService.GetAllLoanRequests().Where(x => x.Branch.Id == findCurrentUserInfo.BranchId).ToList();
                    }
                }
            }  
             
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetAllLoanRequest(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, LoanRequestList: allloanrequestpagecollection);


            var results = Json(new JQueryDataTablesResponse<LoanRequest>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));

            return results;
        }


        public static IList<LoanRequest> GetAllLoanRequest(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanRequest> LoanRequestList)
        {
            var GetLoanRequestList = LoanRequestList;

            totalRecordCount = GetLoanRequestList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetLoanRequestList = GetLoanRequestList.Where(c => c.LoanProduct.ProductName
                    .ToLower().Contains(searchString.ToLower())
                    || c.LoanReferenceNumber.ToLower().Contains(searchString.ToLower())
                    || c.CustomerAccount.Customer.FullName.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetLoanRequestList.Count;

            IOrderedEnumerable<LoanRequest> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "CustomerAccount.Customer.FullName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName);
                        break;

                    case "CustomerAccount.Customer.IDNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.IDNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.IDNumber);
                        break;

                    case "CustomerAccount.Customer.MobileNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.MobileNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.MobileNumber);
                        break;

                    case "LoanProduct.ProductName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName);
                        break;

                    case "LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber);
                        break;

                    case "LoanAmount":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount);
                        break;

                    case "InterestAmount":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount);
                        break;

                    case "StatusDesc":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc);
                        break;

                    case "Branch.BranchName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <returns></returns>
        /// 
        [Authorize]
        public ActionResult CustomerList()
        {
            return View();
        }

        public JsonResult getallcustomers(JQueryDataTablesModel datatablemodel)
        {
            var findCurrentUserId = User.Identity.Name;
            var pagecollection = new List<Customer>();
            if (findCurrentUserId != null)
            {
                var findCurrentUserInfo = userInfoService.FindUserByUserName(findCurrentUserId);
                if (findCurrentUserInfo != null)
                {
                    if (findCurrentUserInfo.Branch.BranchCode == "000")
                    {
                        pagecollection = customerservice.GetAllCustomers().ToList();
                    }
                    else
                    {
                        pagecollection = customerservice.GetCustomersByBranchId(findCurrentUserInfo.BranchId).ToList();
                    }
                }
            } 

            int totalrecordcount = 0;

            int searchrecordcount = 0;

            var transactionList = GetAllCustomer(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalrecordcount, searchRecordCount: out searchrecordcount, searchString: datatablemodel.sSearch, CustomerList: pagecollection);


            return Json(new JQueryDataTablesResponse<Customer>(items: transactionList,
                totalRecords: totalrecordcount,
                totalDisplayRecords: searchrecordcount,
                sEcho: datatablemodel.sEcho));

        }

        public static IList<Customer> GetAllCustomer(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<Customer> CustomerList)
        {
            var GetCustomersList = CustomerList;

            totalRecordCount = GetCustomersList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomersList = GetCustomersList.Where(c => c.FullName
                    .ToLower().Contains(searchString.ToLower())
                    || c.IDNumber.ToLower().Contains(searchString.ToLower()) || c.MobileNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetCustomersList.Count;

            IOrderedEnumerable<Customer> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "FullName":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.FullName);
                        break;

                    case "MobileNumber":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.MobileNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.MobileNumber);
                        break;

                    case "IDNumber":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.IDNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IDNumber);
                        break;

                    case "Branch.BranchName":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName);
                        break;

                    case "IncomeRange":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.IncomeRange)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IncomeRange);
                        break;

                    case "IsEnabled":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        #region Approve Loan Request
        /// <summary>
        /// Approve loan request
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        [Authorize(Roles = "LoanManager")]
        public ActionResult ApproveLoan(Guid Id)
        {
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(Id);

            return View(findloanrequestbyid);
        }

        [ActionName("approveloan")]
        [HttpPost]        
        public ActionResult ApproveCustomerLoanRequest(LoanRequest loanrequest)
        {
            var finddstaticbykey = staticsettings.FindStaticSettingByKey("ENFORCE_CUSTOMER_AND_LOAN_DOCUMENTS_BEFORE_LOAN_APPPROVAL");
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(loanrequest.Id);

            if (finddstaticbykey.Value == "1" && finddstaticbykey.IsLocked == false)
            {
                bool iscustomerdocumentavailable = customerdocumentservice.VerifyAllCustomerDocumentExistence(loanrequest.CustomerAccount.Customer.Id);
                bool isloanrequestdocumentavailable = loanrequestdocumentservice.VerifyLoanRequestDocument(loanrequest.Id);
                if (iscustomerdocumentavailable && isloanrequestdocumentavailable)
                {

                    if (findloanrequestbyid == null)
                    {
                        ModelState.AddModelError(string.Empty, "Failed while retrieving loan request information. No information was found.");
                        return View(loanrequest);
                    }

                    if (findloanrequestbyid.Status == (int)Enumerations.LoanStatus.Pending)
                    {
                        var findloanproduct = loanproductservice.FindLoanProductById(findloanrequestbyid.LoanProductId);

                        var FindCustomerAccountByCustomerIdandAccountType = customeraccountservice.FindCustomerByCustomerIdandAccountType(findloanrequestbyid.CustomerAccount.CustomerId, (int)Enumerations.ProductCode.Savings);

                        var findcustomeraccountbycustomerid = customeraccountservice.FindCustomersAccountByCustomerId(findloanrequestbyid.CustomerAccountId);

                        var findchartofaccount = chartofaccountservice.GetChartOfAccountsByTypeAndCategory((int)Enumerations.ChartOfAccountType.Asset, (int)Enumerations.ChartOfAccountCategory.DetailAccount);
                        var findcustomerbyid = customerservice.FindCustomerById(findloanrequestbyid.CustomerAccountId);
                        string customeraccountid = findloanrequestbyid.CustomerAccountId.ToString();
                        if (string.IsNullOrWhiteSpace(loanrequest.Remarks))
                        {
                            ModelState.AddModelError(string.Empty, "You must provide approval remarks.");
                            return View(findloanrequestbyid);
                        }

                        try
                        {
                            findloanrequestbyid.MCCMNC = (int)Enumerations.MobileNetworkCode.KE_Safaricom;
                            findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Pending;
                            findloanrequestbyid.Remarks = loanrequest.Remarks;
                            findloanrequestbyid.ApprovedDate = DateTime.Today;
                            findloanrequestbyid.SystemTraceAuditNumber = findloanrequestbyid.Id;

                            #region Debit Loan Transaction History Table
                            var defaultsavingproductglaccount = savingsproductservice.FindDefaultSavingsProduct();
                            if (defaultsavingproductglaccount == null)
                            {
                                ModelState.AddModelError(string.Empty, "Failed. There is no default saving product.");
                                return View(findloanrequestbyid);
                            }
                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                            LoanTransactionHistory debitloanaccount = new LoanTransactionHistory()
                            {
                                LoanRequestId = findloanrequestbyid.Id,
                                ChartofAccountId = findloanproduct.LoanGLAccountId,
                                ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                TransactionType = Enumerations.TransactionType.Debit,
                                CustomersAccountId = findloanrequestbyid.CustomerAccountId,                                
                                Description = findloanrequestbyid.Remarks,
                                CreatedBy = User.Identity.Name,
                                IsApproved = true,
                                TransactionCategory = (int)Enumerations.TransactionCategory.Approval,
                                TransactionAmount = findloanrequestbyid.LoanAmount,
                                TransactionBatchNumber = batchNo
                            };
                            
                            #endregion

                            #region Credit Loan Transaction History Table
                            var id = findloanproduct.LoanGLAccountId.ToString();
                            string _customeraccountId = FindCustomerAccountByCustomerIdandAccountType.Id.ToString();
                            LoanTransactionHistory creditsavingglaccount = new LoanTransactionHistory() 
                            {
                                LoanRequestId = findloanrequestbyid.Id,
                                ChartofAccountId = defaultsavingproductglaccount.ChartofAccountId,                        
                                ContraAccountId = Guid.Parse(id),
                                TransactionType = Enumerations.TransactionType.Credit,
                                CustomersAccountId = Guid.Parse(_customeraccountId),                                
                                Description = findloanrequestbyid.Remarks,
                                IsApproved = true,
                                TransactionCategory = (int)Enumerations.TransactionCategory.Approval,
                                CreatedBy = User.Identity.Name,
                                TransactionAmount = findloanrequestbyid.LoanAmount,
                                TransactionBatchNumber = batchNo
                            };
                            
                            #endregion

                            findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Approved;
                            findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Pending;
                            using (DataContext datacontext = new DataContext())
                            {
                                using (var transaction = datacontext.Database.BeginTransaction())
                                {
                                    try
                                    {
                                        loanrequestService.UpdateLoanRequest(findloanrequestbyid, serviceHeader);
                                        loantransactionhistoryservice.AddNewLoanHistory(debitloanaccount, serviceHeader);
                                        loantransactionhistoryservice.AddNewLoanHistory(creditsavingglaccount, serviceHeader);
                                        var emailAlertDTO = EmailNotification(findloanrequestbyid);
                                        emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                                        NewTextAlertOnApproval(findloanrequestbyid.CustomerAccount.Customer.FullName, findloanrequestbyid.CustomerAccount.Customer.MobileNumber, findloanrequestbyid.LoanReferenceNumber, findloanrequestbyid.LoanAmount);
                                        transaction.Commit();
                                    }

                                    catch (Exception ex)
                                    {
                                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                        transaction.Rollback();
                                    }
                                }
                            }
                        }

                        catch (Exception ex)
                        {
                            ModelState.AddModelError(string.Empty, "An error has occurred while processing your request.");
                            LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                        }
                    }

                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Failed. Some required documents are missing. Make sure customer and loan application documents are available before approving.");
                    return View(findloanrequestbyid);
                }
            }
            else
            {
                if (findloanrequestbyid == null)
                {
                    ModelState.AddModelError(string.Empty, "Failed while retrieving loan request information. No information was found.");
                    return View(loanrequest);
                }

                if (findloanrequestbyid.Status == (int)Enumerations.LoanStatus.Pending)
                {
                    var findloanproduct = loanproductservice.FindLoanProductById(findloanrequestbyid.LoanProductId);

                    var FindCustomerAccountByCustomerIdandAccountType = customeraccountservice.FindCustomerByCustomerIdandAccountType(findloanrequestbyid.CustomerAccount.CustomerId, (int)Enumerations.ProductCode.Savings);

                    var findcustomeraccountbycustomerid = customeraccountservice.FindCustomersAccountByCustomerId(findloanrequestbyid.CustomerAccountId);

                    var findchartofaccount = chartofaccountservice.GetChartOfAccountsByTypeAndCategory((int)Enumerations.ChartOfAccountType.Asset, (int)Enumerations.ChartOfAccountCategory.DetailAccount);
                    var findcustomerbyid = customerservice.FindCustomerById(findloanrequestbyid.CustomerAccountId);
                    string customeraccountid = findloanrequestbyid.CustomerAccountId.ToString();
                    if (string.IsNullOrWhiteSpace(loanrequest.Remarks))
                    {
                        ModelState.AddModelError(string.Empty, "You must provide approval remarks.");
                        return View(findloanrequestbyid);
                    }

                    try
                    {
                        findloanrequestbyid.MCCMNC = (int)Enumerations.MobileNetworkCode.KE_Safaricom;
                        findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Pending;
                        findloanrequestbyid.Remarks = loanrequest.Remarks;
                        findloanrequestbyid.ApprovedDate = DateTime.Now;
                        findloanrequestbyid.ApprovedBy = User.Identity.Name;
                        findloanrequestbyid.SystemTraceAuditNumber = findloanrequestbyid.Id;

                        #region Debit Loan Transaction History Table
                        var defaultsavingproductglaccount = savingsproductservice.FindDefaultSavingsProduct();
                        if (defaultsavingproductglaccount == null)
                        {
                            ModelState.AddModelError(string.Empty, "Failed. There is no default saving product.");
                            return View(findloanrequestbyid);
                        }
                        int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                        LoanTransactionHistory debitloanaccount = new LoanTransactionHistory()
                        {
                            LoanRequestId = findloanrequestbyid.Id,
                            ChartofAccountId = findloanproduct.LoanGLAccountId,
                            ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                            TransactionType = Enumerations.TransactionType.Debit,
                            BranchId = findloanrequestbyid.BranchId,
                            CustomersAccountId = findloanrequestbyid.CustomerAccountId,                            
                            Description = findloanrequestbyid.Remarks,
                            CreatedBy = User.Identity.Name,
                            IsApproved = true,
                            TransactionCategory = (int)Enumerations.TransactionCategory.Approval,
                            TransactionAmount = findloanrequestbyid.LoanAmount,
                            TransactionBatchNumber = batchNo
                        };
                        #endregion

                        #region Credit Loan Transaction History Table
                        var id = findloanproduct.LoanGLAccountId.ToString();
                        string _customeraccountId = FindCustomerAccountByCustomerIdandAccountType.Id.ToString();
                        LoanTransactionHistory creditsavingglaccount = new LoanTransactionHistory()
                        {
                            LoanRequestId = findloanrequestbyid.Id,
                            ChartofAccountId = defaultsavingproductglaccount.ChartofAccountId,
                            ContraAccountId = Guid.Parse(id),
                            BranchId = findloanrequestbyid.BranchId,
                            TransactionType = Enumerations.TransactionType.Credit,
                            CustomersAccountId = Guid.Parse(_customeraccountId),                            
                            Description = findloanrequestbyid.Remarks,
                            IsApproved = true,
                            TransactionCategory = (int)Enumerations.TransactionCategory.Approval,
                            CreatedBy = User.Identity.Name,
                            TransactionAmount = findloanrequestbyid.LoanAmount,
                            TransactionBatchNumber = batchNo
                        };
                        #endregion

                        findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Approved;
                        findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Pending;
                        using (DataContext datacontext = new DataContext())
                        {
                            using (var transaction = datacontext.Database.BeginTransaction())
                            {
                                try
                                {
                                    loanrequestService.UpdateLoanRequest(findloanrequestbyid, serviceHeader);
                                    loantransactionhistoryservice.AddNewLoanHistory(debitloanaccount, serviceHeader);
                                    loantransactionhistoryservice.AddNewLoanHistory(creditsavingglaccount, serviceHeader);
                                    var emailAlertDTO = EmailNotification(findloanrequestbyid);
                                    emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                                    NewTextAlertOnApproval(findloanrequestbyid.CustomerAccount.Customer.FullName, findloanrequestbyid.CustomerAccount.Customer.MobileNumber, findloanrequestbyid.LoanReferenceNumber, findloanrequestbyid.LoanAmount);
                                    transaction.Commit();
                                }

                                catch (Exception ex)
                                {
                                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                    transaction.Rollback();
                                }
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        ModelState.AddModelError(string.Empty, "An error has occurred while processing your request.");
                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                        return View(findloanrequestbyid);
                    }
                }
            }
            return RedirectToAction("Index");
        }

        #endregion

        #region Reject Loan Request
        /// <summary>
        /// Reject loan reject
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        ///
        [HttpGet]
        [Authorize(Roles = "LoanManager")]
        public ActionResult RejectLoan(Guid Id)
        {
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(Id);

            return View(findloanrequestbyid);
        }

        [ActionName("rejectloan")]
        [HttpPost]        
        public ActionResult RejectCustomerLoanRequest(LoanRequest loanrequest)
        {
            try
            {
                var findloanrequestbyid = loanrequestService.GetLoanRequestById(loanrequest.Id);

                if (string.IsNullOrWhiteSpace(loanrequest.Remarks))
                {
                    ModelState.AddModelError(string.Empty, "You must provide rejection remarks.");
                    return View(loanrequest);
                }

                if (findloanrequestbyid.Status == (int)Enumerations.LoanStatus.Pending)
                {
                    findloanrequestbyid.Remarks = loanrequest.Remarks;
                    findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Rejected;
                    findloanrequestbyid.ApprovedBy = User.Identity.Name;
                    findloanrequestbyid.ApprovedDate = DateTime.Now;
                    loanrequestService.UpdateLoanRequest(findloanrequestbyid, serviceHeader);

                    StringBuilder bodyBuilder = new StringBuilder();
                    bodyBuilder.Append(string.Format("Dear {0} ", findloanrequestbyid.CustomerAccount.Customer.FullName));
                    bodyBuilder.AppendLine("<br />");
                    bodyBuilder.AppendLine("<br />");
                    bodyBuilder.Append(string.Format("Your loan with reference number-{0} has been declined.", findloanrequestbyid.LoanReferenceNumber));
                    bodyBuilder.AppendLine("<br />");
                    bodyBuilder.AppendLine("<br />");

                    var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
                    bodyBuilder.Append(emailSignature);

                    var emailAlertDTO = new EmailAlert
                    {
                        MailMessageFrom = DefaultSettings.Instance.Email,
                        MailMessageTo = string.Format("{0}", findloanrequestbyid.CustomerAccount.Customer.EmailAddress),
                        MailMessageSubject = string.Format("Loan Request Declined - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                        MailMessageBody = string.Format("{0}", bodyBuilder),
                        MailMessageIsBodyHtml = true,
                        MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                        MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                        MailMessageSecurityCritical = false,
                        CreatedBy = "System",
                        MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                    };
                    emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                }
                StringBuilder _bodyBuilder = new StringBuilder();
                _bodyBuilder.Append(string.Format("Dear {0} ", findloanrequestbyid.CustomerAccount.Customer.FullName));
                _bodyBuilder.Append(string.Format("Your loan with reference number-{0} has been declined.", findloanrequestbyid.LoanReferenceNumber));

                var textalert = new TextAlert
                {
                    TextMessageRecipient = string.Format("{0}", findloanrequestbyid.CustomerAccount.Customer.MobileNumber),
                    TextMessageBody = string.Format("{0}", _bodyBuilder),
                    MaskedTextMessageBody = "",
                    TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                    TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                    TextMessageSecurityCritical = false,
                    CreatedBy = User.Identity.Name,
                    TextMessageSendRetry = 3
                };
                textalertservice.AddNewTextAlert(textalert, serviceHeader);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "An error has occurred while processing your request.");
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
            return View(loanrequest);
        }
        #endregion

        //#region Disburse Loan
        //[HttpGet]
        //[Authorize(Roles = "LoanManager")]
        //public ActionResult DisburseLoan(Guid Id)
        //{
        //    var findloanrequestbyid = loanrequestService.GetLoanRequestById(Id);

        //    return View(findloanrequestbyid);
        //}

        //[HttpPost]
        //[ActionName("DisburseLoan")]           
        //public ActionResult DisburseLoan(LoanRequest loanrequest)
        //{
        //    LoanTransactionHistory loantransactionhistory = new LoanTransactionHistory();

        //    decimal totalcharges = 0;

        //    var findloanrequestbyid = loanrequestService.GetLoanRequestById(loanrequest.Id);
        //    if (findloanrequestbyid == null)
        //    {
        //        ModelState.AddModelError(string.Empty, "An expected error has occurred.");
        //        return View(loanrequest);
        //    }
        //    if (findloanrequestbyid.Status == (int)Enumerations.LoanStatus.Approved)
        //    {
        //        using (var datacontext = new DataContext())
        //        {
        //            using (var transaction = datacontext.Database.BeginTransaction())
        //            {
        //                try
        //                {
        //                    var MpesaGL = systemglmappingservice.FindSystemGeneralLedgerAccountMappingsByGLCode((int)Enumerations.SystemGeneralLedgerAccountCode.MPesaSettlementAccount);

        //                    if (MpesaGL == null)
        //                    {
        //                        ModelState.AddModelError(string.Empty, "Failed. There is no M-Pesa Settlement GL Account  available.");
        //                        return View(findloanrequestbyid);
        //                    }
        //                    try
        //                    {
        //                        var FindCustomerAccountByCustomerIdandAccountType = customeraccountservice.FindCustomerByCustomerIdandAccountType(findloanrequestbyid.CustomerAccount.CustomerId, (int)Enumerations.ProductCode.Savings);
        //                        var defaultsavingproductglaccount = savingsproductservice.FindDefaultSavingsProduct();
        //                        var findloanproductcharges = loanproductcharges.GetAllLoanProductCharges(findloanrequestbyid.LoanProductId);
        //                        if (findloanproductcharges != null && findloanproductcharges.Any())
        //                        {
        //                            foreach (var charge in findloanproductcharges)
        //                            {
        //                                var findgraduatedscalebychargeid = graduatedscaleservice.FindGraduatedScaleUsingRangeAndChargeId(loanrequest.LoanAmount, charge.ChargeId);
        //                                if (findgraduatedscalebychargeid != null && findgraduatedscalebychargeid.Any())
        //                                {
        //                                    foreach (var graduatedscale in findgraduatedscalebychargeid)
        //                                    {
        //                                        var findloanamountrangeingradutedscale = graduatedscaleservice.FindGraduatedScaleUsingRangeAndChargeId(findloanrequestbyid.LoanAmount, graduatedscale.ChargeId);
        //                                        var customersavingsaccountid = FindCustomerAccountByCustomerIdandAccountType.Id.ToString();
        //                                        if (graduatedscale.ChargeType == Enumerations.ChargeTypes.FixedAmount)
        //                                        {
        //                                            if (findloanamountrangeingradutedscale != null)
        //                                            {
        //                                                var fixedchargetypeamount = graduatedscale.Value;
        //                                                LoanTransactionHistory postchargesdebit = new LoanTransactionHistory();
        //                                                LoanRequestCharges(findloanrequestbyid.Id, charge.Charges.ChartofAccountId, graduatedscale.Id, fixedchargetypeamount);

        //                                                #region Debit
        //                                                postchargesdebit.LoanRequestId = findloanrequestbyid.Id;
        //                                                postchargesdebit.ChartofAccountId = MpesaGL.ChartofAccountId;
        //                                                postchargesdebit.ContraAccountId = defaultsavingproductglaccount.ChartofAccountId;
        //                                                postchargesdebit.TransactionType = Enumerations.TransactionType.Debit;
        //                                                postchargesdebit.CustomersAccountId = Guid.Parse(customersavingsaccountid);                                                        
        //                                                postchargesdebit.Description = findloanrequestbyid.Remarks;
        //                                                postchargesdebit.IsApproved = true;
        //                                                postchargesdebit.TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement;
        //                                                postchargesdebit.CreatedBy = User.Identity.Name;
        //                                                postchargesdebit.TransactionAmount = fixedchargetypeamount;
        //                                                loantransactionhistoryservice.AddNewLoanHistory(postchargesdebit, serviceHeader);
        //                                                #endregion

        //                                                LoanTransactionHistory postchargescredit = new LoanTransactionHistory();
        //                                                #region Credit
        //                                                postchargescredit.LoanRequestId = findloanrequestbyid.Id;
        //                                                postchargescredit.ChartofAccountId = MpesaGL.ChartofAccountId;
        //                                                postchargescredit.ContraAccountId = defaultsavingproductglaccount.ChartofAccountId;
        //                                                postchargescredit.TransactionType = Enumerations.TransactionType.Credit;
        //                                                postchargescredit.CustomersAccountId = Guid.Parse(customersavingsaccountid);                                                        
        //                                                postchargescredit.Description = findloanrequestbyid.Remarks;
        //                                                postchargescredit.TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement;
        //                                                postchargescredit.CreatedBy = User.Identity.Name;
        //                                                postchargescredit.IsApproved = true;
        //                                                postchargescredit.TransactionAmount = fixedchargetypeamount;
        //                                                loantransactionhistoryservice.AddNewLoanHistory(postchargescredit, serviceHeader);
        //                                                #endregion
        //                                            }
        //                                        }

        //                                        else if (graduatedscale.ChargeType == Enumerations.ChargeTypes.Percentage)
        //                                        {
        //                                            if (findloanamountrangeingradutedscale != null)
        //                                            {
        //                                                var percetagechargetypeamount = ((graduatedscale.Value) * findloanrequestbyid.LoanAmount) / 100;
        //                                                LoanRequestCharges(findloanrequestbyid.Id, charge.Charges.ChartofAccountId, graduatedscale.Id, percetagechargetypeamount);
        //                                                LoanTransactionHistory postchargesdebit = new LoanTransactionHistory();

        //                                                #region Debit
        //                                                postchargesdebit.LoanRequestId = findloanrequestbyid.Id;
        //                                                postchargesdebit.ChartofAccountId = MpesaGL.ChartofAccountId;
        //                                                postchargesdebit.ContraAccountId = defaultsavingproductglaccount.ChartofAccountId;
        //                                                postchargesdebit.TransactionType = Enumerations.TransactionType.Debit;
        //                                                postchargesdebit.CustomersAccountId = Guid.Parse(customersavingsaccountid);                                                        
        //                                                postchargesdebit.Description = findloanrequestbyid.Remarks;
        //                                                postchargesdebit.IsApproved = true;
        //                                                postchargesdebit.TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement;
        //                                                postchargesdebit.CreatedBy = User.Identity.Name;
        //                                                postchargesdebit.TransactionAmount = percetagechargetypeamount;
        //                                                loantransactionhistoryservice.AddNewLoanHistory(postchargesdebit, serviceHeader);
        //                                                #endregion

        //                                                LoanTransactionHistory postchargescredit = new LoanTransactionHistory();
        //                                                #region Credit
        //                                                postchargescredit.LoanRequestId = findloanrequestbyid.Id;
        //                                                postchargescredit.ChartofAccountId = MpesaGL.ChartofAccountId;
        //                                                postchargescredit.ContraAccountId = defaultsavingproductglaccount.ChartofAccountId;
        //                                                postchargescredit.TransactionType = Enumerations.TransactionType.Credit;
        //                                                postchargescredit.CustomersAccountId = Guid.Parse(customersavingsaccountid);                                                        
        //                                                postchargescredit.Description = findloanrequestbyid.Remarks;
        //                                                postchargescredit.TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement;
        //                                                postchargescredit.CreatedBy = User.Identity.Name;
        //                                                postchargescredit.IsApproved = true;
        //                                                postchargescredit.TransactionAmount = percetagechargetypeamount;
        //                                                loantransactionhistoryservice.AddNewLoanHistory(postchargescredit, serviceHeader);
        //                                                #endregion
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            ModelState.AddModelError(string.Empty, "Failed while trying to determine product charge type. Contact your system administrator for assistance.");
        //                                            return View(findloanrequestbyid);
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        #region Debit Loan Transaction History Table
        //                        var customeraccountid = FindCustomerAccountByCustomerIdandAccountType.Id.ToString();
        //                        LoanTransactionHistory debitsavingsaccount = new LoanTransactionHistory();
        //                        debitsavingsaccount.LoanRequestId = findloanrequestbyid.Id;
        //                        debitsavingsaccount.ChartofAccountId = MpesaGL.ChartofAccountId;
        //                        debitsavingsaccount.ContraAccountId = defaultsavingproductglaccount.ChartofAccountId;
        //                        debitsavingsaccount.TransactionType = Enumerations.TransactionType.Debit;
        //                        debitsavingsaccount.CustomersAccountId = Guid.Parse(customeraccountid);                                
        //                        debitsavingsaccount.Description = findloanrequestbyid.Remarks;
        //                        debitsavingsaccount.IsApproved = true;
        //                        debitsavingsaccount.TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement;
        //                        debitsavingsaccount.CreatedBy = User.Identity.Name;
        //                        debitsavingsaccount.TransactionAmount = findloanrequestbyid.LoanAmount;
        //                        #endregion

        //                        #region Credit Loan Transaction History Table
        //                        LoanTransactionHistory creditmpesaglaccount = new LoanTransactionHistory();
        //                        creditmpesaglaccount.LoanRequestId = findloanrequestbyid.Id;
        //                        creditmpesaglaccount.ChartofAccountId = MpesaGL.ChartofAccountId;
        //                        creditmpesaglaccount.ContraAccountId = defaultsavingproductglaccount.ChartofAccountId;
        //                        creditmpesaglaccount.TransactionType = Enumerations.TransactionType.Credit;
        //                        creditmpesaglaccount.CustomersAccountId = Guid.Parse(customeraccountid);                                
        //                        creditmpesaglaccount.Description = findloanrequestbyid.Remarks;
        //                        creditmpesaglaccount.TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement;
        //                        creditmpesaglaccount.CreatedBy = User.Identity.Name;
        //                        creditmpesaglaccount.IsApproved = true;
        //                        creditmpesaglaccount.TransactionAmount = findloanrequestbyid.LoanAmount;
        //                        #endregion

        //                        var findloanrequestcharge = loanrequestchargeservice.FindAllLoanRequestChargesByLoanRequestId(findloanrequestbyid.Id);
        //                        if (findloanrequestcharge != null && findloanrequestcharge.Any())
        //                        {
        //                            totalcharges = findloanrequestcharge.Sum(x => x.TransactionAmount);
        //                        }
        //                        findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Disbursed;
        //                        findloanrequestbyid.DisbursedAmount = findloanrequestbyid.LoanAmount - totalcharges;
        //                        findloanrequestbyid.DisbursementDate = DateTime.Today;
        //                        findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Disburse;
        //                        loanrequestService.UpdateLoanRequest(findloanrequestbyid, serviceHeader);
        //                        loantransactionhistoryservice.AddNewLoanHistory(creditmpesaglaccount, serviceHeader);
        //                        loantransactionhistoryservice.AddNewLoanHistory(debitsavingsaccount, serviceHeader);
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        ModelState.AddModelError(string.Empty, "An error has occurred while processing your request.");
        //                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
        //                    }
        //                    transaction.Commit();
        //                }

        //                catch (Exception ex)
        //                {
        //                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
        //                    transaction.Rollback();
        //                }
        //            }
        //        }
        //    }
        //    return RedirectToAction("Index");
        //}
        //#endregion

        #region Notifications
        private static EmailAlert EmailNotification(LoanRequest findloanrequestbyid)
        {
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append(string.Format("Dear {0}", findloanrequestbyid.CustomerAccount.Customer.FullName));
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.Append(string.Format("Your loan of KES {0} with reference number-{1} has been approved.", findloanrequestbyid.LoanAmount, findloanrequestbyid.LoanReferenceNumber));
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.AppendLine("<br />");

            var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
            bodyBuilder.Append(emailSignature);

            var emailAlertDTO = new EmailAlert
            {
                MailMessageFrom = DefaultSettings.Instance.Email,
                MailMessageTo = string.Format("{0}", findloanrequestbyid.CustomerAccount.Customer.EmailAddress),
                MailMessageSubject = string.Format("Loan Request Approved - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                MailMessageBody = string.Format("{0}", bodyBuilder),
                MailMessageIsBodyHtml = true,
                MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                MailMessageSecurityCritical = false,
                CreatedBy = "System",
                MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
            };
            return emailAlertDTO;
        }

        private void NewTextAlertOnApproval(string RecipientName, string RecepientNumber, string LoanRefNo, decimal LoanAmount)
        {
            //Create email alert and insert in the database
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append(string.Format("Dear {0} ", RecipientName));
            bodyBuilder.Append(string.Format("Your loan of KES {0} with reference number-{1} has been approved.", LoanAmount, LoanRefNo));

            var textalert = new TextAlert
            {
                TextMessageRecipient = string.Format("{0}", RecepientNumber),
                TextMessageBody = string.Format("{0}", bodyBuilder),
                MaskedTextMessageBody = "",
                TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                TextMessageSecurityCritical = false,
                CreatedBy = User.Identity.Name,
                TextMessageSendRetry = 3
            };

            textalertservice.AddNewTextAlert(textalert, serviceHeader);
        }
        #endregion

        private void LoanRequestCharges(Guid LoanRequestId, Guid ChartofAccountsId, Guid GraduatedScaleId, decimal amount)
        {
            LoanRequestCharge loanrequestcharge = new LoanRequestCharge();
            loanrequestcharge.GraduatedScaleId = GraduatedScaleId;
            loanrequestcharge.ChartofAccountId = ChartofAccountsId;
            loanrequestcharge.LoanRequestId = LoanRequestId;
            loanrequestcharge.TransactionAmount = amount;
            loanrequestchargeservice.AddLoanRequestCharge(loanrequestcharge, serviceHeader);
        }
    }
}
﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.DTO;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.Services.Specifications;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;

namespace Nikopeshe.Services.InterfaceImplementations
{
    [Export(typeof(IC2BTransactionService))]
    public class C2BTransactionService : IC2BTransactionService
    {
        private readonly IRepository<C2BTransaction> c2btransactionrepository;

        private readonly IRepository<Branch> branchrepository;

        string ErrorMessage = string.Empty;

        public C2BTransactionService(IRepository<C2BTransaction> _c2btransactionrepository, IRepository<Branch> _branchrepository)
        {
            if (_c2btransactionrepository == null)
                throw new ArgumentNullException("c2btransactionrepository");
            this.c2btransactionrepository = _c2btransactionrepository;
            this.branchrepository = _branchrepository;
        }

        public void AddNewC2BTransaction(C2BTransaction c2bmapping, ServiceHeader serviceHeader)
        {
            if (c2bmapping == null)
                throw new ArgumentNullException("entity cannot be null");

            try
            {
                c2btransactionrepository.Insert(c2bmapping, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public void UpdateC2BTransaction(C2BTransaction c2bmapping, ServiceHeader serviceHeader)
        {
            if (c2bmapping == null)
                throw new ArgumentNullException("entity cannot be null");

            try
            {
                c2btransactionrepository.Update(c2bmapping, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public C2BTransaction FindC2BTransactionById(Guid Id)
        {
            if (Id == Guid.Empty)
                throw new ArgumentNullException("Id cannot be null");

            var findc2bmappingbyid = c2btransactionrepository.GetById(Id);

            return findc2bmappingbyid;
        }

        public IList<C2BTransaction> GetAllC2BTransaction()
        {
            var findallc2bmappings = c2btransactionrepository.GetAll(IsolationLevel.ReadUncommitted);

            return findallc2bmappings.ToList();
        }

        public C2BTransaction FindC2BTransactionByTransactionId(string TransactionId)
        {
            if (string.IsNullOrWhiteSpace(TransactionId))
                throw new ArgumentNullException("Transaction Id cannot be null");

            var findc2btransctionbytransactionid = c2btransactionrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.TransID == TransactionId);

            return findc2btransctionbytransactionid.FirstOrDefault();
        }

        public C2BTransaction FindC2BTransactionByThirdPartyTransID(string ThirdPartyTransID)
        {
            if (string.IsNullOrWhiteSpace(ThirdPartyTransID))
                throw new ArgumentNullException("Third Party Transaction cannot be null");

            var findc2btransactionbythirdpartyid = c2btransactionrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.ThirdPartyTransID == ThirdPartyTransID);

            return findc2btransactionbythirdpartyid.FirstOrDefault();
        }

        public IList<C2BTransaction> GetC2bTransactionsByStatus(int Status)
        {
            var findc2bbystatus = c2btransactionrepository.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.Status == Status);

            return findc2bbystatus.ToList();
        }

        public IList<C2BTransaction> GetUnkownAndSuspenseC2BTransactions(int StatusA, int statusB, int StatusC)
        {
            var findc2bbystatus = c2btransactionrepository.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.Status == StatusA || x.Status == statusB || x.Status == StatusC);

            return findc2bbystatus.ToList();
        }

        public C2BTransaction FindC2BTransactionByMpesaReceiptCode(string MpesaReceiptCode)
        {
            var findC2BByMpesaCode = c2btransactionrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.TransID == MpesaReceiptCode).FirstOrDefault();

            return findC2BByMpesaCode;
        }

        public PageCollectionInfo<C2BTransaction> FindC2BTransactionByIdAndFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = C2BTransactionSpecifications.DefaultSpec(searchString);

            ISpecification<C2BTransaction> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var orderLinePagedCollection = c2btransactionrepository.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (orderLinePagedCollection != null)
            {
                var pageCollection = orderLinePagedCollection.Items.ToList();

                var totalRecordCount = orderLinePagedCollection.TotalItems;

                return new PageCollectionInfo<C2BTransaction> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }

        public PageCollectionInfo<C2BTransaction> FindC2BTransactionByStatusAndFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = C2BTransactionSpecifications.FindC2BTransactionsByStatus(searchString);

            ISpecification<C2BTransaction> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var orderLinePagedCollection = c2btransactionrepository.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (orderLinePagedCollection != null)
            {
                var pageCollection = orderLinePagedCollection.Items.ToList();

                var totalRecordCount = orderLinePagedCollection.TotalItems;

                return new PageCollectionInfo<C2BTransaction> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }

        public PageCollectionInfo<C2BTransaction> FindSuspenseAndUnknownTransactionsFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = C2BTransactionSpecifications.FindSuspenseAndUnknownTransaction(searchString);

            ISpecification<C2BTransaction> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var orderLinePagedCollection = c2btransactionrepository.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (orderLinePagedCollection != null)
            {
                var pageCollection = orderLinePagedCollection.Items.ToList();

                var totalRecordCount = orderLinePagedCollection.TotalItems;

                return new PageCollectionInfo<C2BTransaction> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }
    }
}

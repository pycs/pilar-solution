﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.TextAlertServices;
using System;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Linq;
using System.Messaging;

namespace Pilar.TextAlertDispatcher.Services
{
    public class AfricasTalkingGatewayException : Exception
    {
        public AfricasTalkingGatewayException(string message)
            : base(message) { }
    }

    [Export(typeof(IPlugin))]
    public class Dispatcher : IPlugin
    {
        // Specify your login credentials
        private string AfricasTakingAPIKey = ConfigurationManager.AppSettings["AfricasTalkingAPIKey"].ToString();
        private string AfricasTakingUserName = ConfigurationManager.AppSettings["AfricasTalkingUsername"].ToString();
        private string AfricasTakingAPIEndPoint = ConfigurationManager.AppSettings["SMS_URLString"].ToString();
        private static string SenderId = ConfigurationManager.AppSettings["SMSSenderId"].ToString();
        private MessageQueue _messageQueue;
        private ITextAlertService _textAlertService;
        private readonly string _queuePath;

        [ImportingConstructor]
        public Dispatcher()
        {
            _textAlertService = new TextAlertService();            

            _queuePath = ConfigurationManager.AppSettings["TextRelayQueuePath"];
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{777826C1-88C1-4013-8FA8-5B0BAA0B015F}"); }
        }

        public string Description
        {
            get { return "TEXT DISPATCHER"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                if (!MessageQueue.Exists(_queuePath))
                    throw new ArgumentException("Queue does not exist at specified path", "_queuePath");
                else
                {
                    // initialize message queue
                    _messageQueue = new MessageQueue(_queuePath);

                    // enable the AppSpecific field in the messages
                    _messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                    // set the formatter to binary.
                    _messageQueue.Formatter = new BinaryMessageFormatter();

                    // set an event to listen for new messages.
                    _messageQueue.ReceiveCompleted += MessageQueue_ReceiveCompleted;

                    // start listening
                    _messageQueue.BeginReceive();
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        public void Exit()
        {
            _messageQueue.Close();
        }

        #region Event Handlers

        private void MessageQueue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                // retrieve sms alert id
                var recordId = (Guid)e.Message.Body;

                // retrieve message category
                var messageCategory = (Enumerations.MessageCategory)e.Message.AppSpecific;

                switch (messageCategory)
                {
                    case Enumerations.MessageCategory.SMSAlert:

                        #region email

                        var textAlert = _textAlertService.FindTextAlertById(recordId);

                        if (textAlert != null)
                        {
                            switch ((Enumerations.DLRStatus)textAlert.TextMessageDLRStatus)
                            {
                                case Enumerations.DLRStatus.UnKnown:
                                case Enumerations.DLRStatus.Pending:

                                    try
                                    {
                                        // Create a new instance of our gateway class
                                        AfricasTalkingGateway gateway = new AfricasTalkingGateway(AfricasTakingUserName, AfricasTakingAPIKey);

                                        var findpendingtextalerts = _textAlertService.FindTextAlertByDRLStatus((int)Enumerations.DLRStatus.Pending);
                                        if (findpendingtextalerts != null && findpendingtextalerts.Any())
                                        {
                                            foreach (var pendingtext in findpendingtextalerts)
                                            {
                                                string recipients = pendingtext.TextMessageRecipient;
                                                string message = pendingtext.TextMessageBody;
                                                dynamic results = null;
                                                if (string.IsNullOrWhiteSpace(SenderId))
                                                {
                                                    results = gateway.sendMessage(recipients, message);
                                                }

                                                else
                                                {
                                                    results = gateway.sendMessage(recipients, message, SenderId);
                                                }
                                                foreach (dynamic result in results)
                                                {

                                                    var number = result["number"];
                                                    var status = result["status"].ToString();
                                                    _textAlertService.UpdateTextAlertDRLStatus(pendingtext.Id, (int)Enumerations.DLRStatus.Delivered);
                                                    if (status.ToLower() == "Success".ToLower())
                                                    {                                                       
                                                        _textAlertService.UpdateTextAlertDRLStatus(pendingtext.Id, (int)Enumerations.DLRStatus.Delivered, result["messageId"], result["cost"], status);
                                                    }

                                                    else
                                                    {
                                                        _textAlertService.UpdateTextAlertDRLStatus(pendingtext.Id, (int)Enumerations.DLRStatus.Failed);
                                                    }
                                                }
                                            }
                                        }                                                                                  
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerFactory.CreateLog().LogInfo("MessageCategory.EmailAlert" + ex);
                                    }

                                    break;
                                default:
                                    break;
                            }
                        }

                        #endregion

                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->Process->ReceiveCompleted FAILED...ReceiveCompletedEventArgs={1}" + ex + Description + e);
            }

            finally
            {
                // listen for the next message.
                _messageQueue.BeginReceive();
            }
        }

        #endregion
        #endregion
    }
}

namespace Nikopeshe.Data.Migrations
{
    using Nikopeshe.Entity.DataModels;
    using System;
    using System.Configuration;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web.Security;

    public sealed class Configuration : DbMigrationsConfiguration<Nikopeshe.Data.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            CommandTimeout = 3600;
        }

        protected override void Seed(Nikopeshe.Data.DataContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            var modelcode = ConfigurationManager.AppSettings["ModelCode"];

            if (modelcode == "1")
            {
                context.Branch.AddOrUpdate(new Branch()
                {
                    BranchCode = "002",
                    BranchName = "Head Office",
                    IsEnabled = true
                });

                context.StaticSetting.AddOrUpdate(new StaticSetting()
                {
                    Key = "ENFORCE_CUSTOMER_DOCUMENTS_AFTER_REGISTRATION",
                    Value = "0",
                    IsLocked = false
                },
                new StaticSetting()
                {
                    Key = "ENFORCE_LOAN_DOCUMENTS_BEFORE_LOAN_APPROVAL",
                    Value = "0",
                    IsLocked = false
                },
                 new StaticSetting()
                 {
                     Key = "ENFORCE_CUSTOMER_AND_LOAN_DOCUMENTS_BEFORE_LOAN_APPPROVAL",
                     Value = "0",
                     IsLocked = false
                 });
            }                                     
        }
    }
}

﻿using Pilar.Services.Services.Utility;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Utility.Configuration
{
    public class ClearLoanJob : IJob
    {
        readonly IUtility _utility;

        public ClearLoanJob()
        {
            _utility = new Pilar.Services.Services.Utility.Utility();
        }

        public void Execute(IJobExecutionContext context)
        {
            _utility.ClearLoans();
        }
    }
}

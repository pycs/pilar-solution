﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Data.Mappings
{
    public class LoanRequestEntityTypeConfiguration : EntityTypeConfiguration<LoanRequest>
    {
        public LoanRequestEntityTypeConfiguration()
        {
            this.HasKey(e => e.Id);
            this.ToTable(string.Format("{0}LoanRequests", DefaultSettings.Instance.TablePrefix));
        }
    }
}

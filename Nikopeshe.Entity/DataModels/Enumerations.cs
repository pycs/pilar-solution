﻿using System;
using System.ComponentModel;

namespace Nikopeshe.Entity.DataModels
{
    public class Enumerations
    {
        public enum LoanStatus
        {
            [Description("Pending")]
            Pending = 0,

            [Description("Processing")]
            Processing = 1,

            [Description("Disbursed")]
            Disbursed = 2,

            [Description("Rejected")]
            Rejected = 3,

            [Description("Cleared")]
            Cleared = 5,

            [Description("UnCleared")]
            Uncleared = 6,

            [Description("Approved")]
            Approved = 7,

            [Description("Canceled by Customer")]
            CustomerRejected = 8,

            [Description("Active")]
            Active = 9,

            [Description("Failed at Disbursing")]
            FailedAtDisbursing = 10,

            [Description("Review")]
            Review = 11,

            [Description("Transaction Reversed")]
            TransactionReversed = 12,

            [Description("Rejected and Reversed")]
            Reversed = 13,

            [Description("Transactions Reversed")]
            CompleteReversal = 14
        }

        public enum Type
        {
            [Description("Airtime")]
            Airtime = 1,

            [Description("Paybill")]
            Paybill = 2,
        }

        public enum TransactionType
        {
            [Description("Debit")]
            Debit = 1,

            [Description("Credit")]
            Credit = 2
        }

        public enum PaymentFrequency
        {
            [Description("Weekly")]
            Weekly = 4,

            [Description("Monthly")]
            Monthly = 1,            
        }

        public enum RepaymentType
        {
            [Description("M-Pesa")]
            MPesa = 0,

            [Description("Cash")]
            Cash = 1,

            [Description("Cheque")]
            Cheque = 2
        }

        public enum ChartOfAccountType
        {
            [Description("Asset")]
            Asset = 0xF4240,/*1000000*/
            [Description("Liability")]
            Liability = 0x1E8480,/*2000000*/
            [Description("Equity/Capital")]
            Equity = 0x2DC6C0,/*3000000*/
            [Description("Income/Revenue")]
            Income = 0x3D0900,/*4000000*/
            [Description("Expense")]
            Expense = 0x4C4B40,/*5000000*/
        }

        public enum ChartOfAccountCategory
        {
            [Description("Header Account (Non-Postable)")]
            HeaderAccount = 0x1000,/*4096*/
            [Description("Detail Account (Postable)")]
            DetailAccount = 0x1000 + 1/*4097*/
        }

        public enum ProductCode
        {
            [Description("Savings")]
            Savings = 100,
            [Description("Loan")]
            Loan = 200,
            [Description("Investment")]
            Investment = 300,
            [Description("Arrears")]
            Arrears = 400,
        }

        [Flags]
        public enum DLRStatus
        {
            [Description("Awaiting Delivery")]
            UnKnown = 1,
            [Description("Failed")]
            Failed = 2,
            [Description("Pending")]
            Pending = 4,
            [Description("Delivered")]
            Delivered = 8,
            [Description("Not Applicable")]
            NotApplicable = 16,
            [Description]
            Queued = 0
        }

        [Flags]
        public enum MessageOrigin
        {
            [Description("Internal")]
            Within = 1,
            [Description("External")]
            Without = 2,
            [Description("Other")]
            Other = 4
        }

        public enum QueuePriority
        {
            [Description("Lowest")]
            Lowest = 0,
            [Description("Very Low")]
            VeryLow = 1,
            [Description("Low")]
            Low = 2,
            [Description("Normal")]
            Normal = 3,
            [Description("Above Normal")]
            AboveNormal = 4,
            [Description("High")]
            High = 5,
            [Description("Very High")]
            VeryHigh = 6,
            [Description("Highest")]
            Highest = 7,
        }

        public enum EFTType
        {
            [Description("To Bank Account")]
            AccountToAccount = 0,
            [Description("To Mobile Account")]
            AccountToMobile = 1,
            [Description("Business to Business")]
            BusinessToBusiness = 2,
            [Description("Cheque")]
            Cheque = 3
        }

        public enum MobileEFTCode
        {
            [Description("Business Buy Goods")]
            BusinessBuyGoods = 6,
            [Description("Business Pay Bill")]
            BusinessPayBill = 7,
        }

        public enum MobileNetworkCode
        {
            [Description("Kenya - Safaricom")]
            KE_Safaricom = 63902,
            [Description("Kenya - Airtel")]
            KE_Airtel = 63903,
            [Description("Kenya - Yu")]
            KE_Yu = 63905,
            [Description("Kenya - Orange")]
            KE_Orange = 63907,
            [Description("Rwanda - MTN")]
            RW_MTN = 63510,
        }

        public enum ProcessingStatus
        {
            [Description("Pending")]
            Pending = 0,
            [Description("Queued")]
            Queued = 1,
            [Description("Submitted")]
            Submitted = 2,
            [Description("QueryStatus")]
            QueryStatus = 3,
            [Description("Completed")]
            Completed = 4,
            [Description("Retry")]
            Retry = 5,
            [Description("Disburse")]
            Disburse = 6,
            [Description("Rejected")]
            Rejected = 7,
            [Description("Failed")]
            Failed = 8
        }

        [Flags]
        public enum MessageCategory
        {
            [Description("SMS Alert")]
            SMSAlert = 1,
            [Description("Loan Disbursement")]
            LoanDisbursment = 2,
            [Description("Email Alert")]
            EmailAlert = 4,
            [Description("SPS Post Request")]
            SPSPostRequest = 8,
            [Description("SPS Get Request")]
            SPSGetRequest = 16,
            [Description("Loan Status Watch Dog")]
            LoanStatusWatchDog = 32,
            [Description("SPS Get Response")]
            SPSGetResponse = 64,
            [Description("SPS Post Response")]
            SPSPostResponse = 128,
            [Description("Arrears")]                       
            C2BConfirmation = 256,
            [Description("Mark Loan Request Active")]
            MarkLoanRequestActive = 512,
            [Description("C2B Processing")]
            C2BProcessing = 1024,
            [Description("Arrears")]
            Arrears = 2048,
            [Description("C2B Post To Savings Account")]
            C2BPostToSavingsAccount = 4096,
            [Description("Loan Processing")]
            LoanProcessing = 8192,
            [Description("Loan Processing")]
            C2BReversal = 16384,
            [Description("Loan Transactions Reversal")]
            LoanTransactionsReversal = 32768,
            [Description("Loan Processing Fee")]
            LoanProcessingFee = 65536,
            [Description("Auto Recon")]
            AutoRecon = 131072,
            [Description("C2B Notification Sync")]
            C2BNotificationSync = 262144,
            [Description("C2B Waiting Processing")]
            C2BWaitingProcessing = 524288,
            MicroLoanProcessing = 1048576,
            CompleteLoanTransactionsReveral = 2097152
        }

        public enum EFTStatus
        {
            [Description("Pending")]
            Pending = 0,
            [Description("Queued")]
            Queued = 1,
            [Description("SubmitAcknowledged")]
            SubmitAcknowledged = 2,
            [Description("Completed")]
            Completed = 3,
            [Description("Cached")]
            Cached = 4,
            [Description("Failed")]
            Failed = 5,
            [Description("Submitted")]
            Submitted = 6,
            [Description("Rejected")]
            Rejected = 7,
        }

        public enum LoanPaymentStartDateMode
        {
            [Description("Beginning Of The Month")]
            StartOfTheMonth = 0,

            [Description("End Of The Month")]
            EndMonth = 1
        }

        public enum SystemGeneralLedgerAccountCode
        {
            [Description("M-PESA SETTLEMENT")]
            MPesaSettlementAccount = 0xBEBA + 1,  
          
            [Description("M-PESA REPAYMENT")]
            MPesaRepaymentAccount = 0xBEBA + 2,
            
            [Description("M-PESA REFUNDS")]
            MPesaRefundsAccount = 0xBEBA + 3,

            [Description("Customer Joining Fee")]
            CustomerJoiningFee = 0xBEBA + 4,

            [Description("M-Pesa Suspense Account")]
            MPesaSuspsnseAccount = 0xBEBA + 5
        }

        public enum TransactionCategory
        {
            [Description("Approval")]
            Approval = 0,

            [Description("Disbursement")]
            Disbursement = 1,

            [Description("Repayment")]
            Repayment = 2,

            [Description("Reversal")]
            Reversal = 3,

            [Description("Customer Joining Fee")]
            CustomerJoiningFee = 4,
            
            [Description("M-Pesa Deposit")]
            MPesaDeposit = 5,

            [Description("Loan Processing Fee")]
            LoanProcessingFee = 6,

            [Description("Manual Entry")]
            ManualEntry = 7

        }

        public enum C2BTransactionStatus
        {
            [Description("Pending")]
            Pending = 0,

            [Description("Approved")]
            Approved = 1,

            [Description("Processed")]
            Processed = 2,

            [Description("Rejected")]
            Rejected = 3,

            [Description("Unknown")]
            Unknown = 4,

            [Description("Queued")]
            Queued = 5,

            [Description("Post Queued")]
            PostQueued = 6,

            [Description("Duplicate")]
            Duplicate = 7,

            [Description("Suspense")]
            Suspense = 8,

            [Description("Journal Processed")]
            JournalProcessed = 9,

            [Description("Pending Processing")]
            PendingProcessing = 10,

            [Description("Reverse")]
            Reverse = 11,

            [Description("Reversed")]
            Reversed = 12,

            [Description("Pending Reconciliation")]
            PendingReconciliation = 13,

            [Description("Waiting Customer Creation")]
            Waiting = 14
        }

        public enum LoanRequestDocumentTypes
        {
            [Description("Pay Slip")]
            Payslip = 1,            

            [Description("Bank Statement")]
            BankStatement = 2,

            [Description("Loan Application Form")]
            LoanApplicationForm = 3,

                      
        }

        public enum CustomerDocumentTypes
        {
            [Description("KRA PIN")]            
            KRAPIN = 1,

            [Description("Identity Card")]            
            IDCopy = 2,

            [Description("Passport Photo")]            
            PassportPhoto = 3,

            [Description("Signature")]
            Signature = 4,
        }

        public enum CustomerTypes
        {
            [Description("Individual")]
            Individual = 1,

            [Description("Corporation")]
            Corporation = 2,

            [Description("Group")]
            Group = 3,
        }

        public enum ChargeTypes
        {
            [Description("Fixed Amount")]
            FixedAmount = 1,

            [Description("Percentage")]
            Percentage = 2,
        }

        public enum SPSMessageCatergory
        {
            POST = 1,

            GET = 2
        }

        public enum CustomerRegistrationType
        {
            [Description("Self Registered")]
            SelfRegistered = 0,

            [Description("Walk In")]
            WalkIn = 1
        }

        public enum MaritalStatus
        {
            [Description("Single")]
            Single = 1,

            [Description("Married")]
            Married = 2,

            [Description("Divorced")]
            Divorced = 3,

            [Description("Widowed")]
            Widowed = 4
        }

        public enum Gender
        {
            [Description("Male")]
            Male = 1,

            [Description("Female")]
            Female = 2
        }

        public enum B2BTransactionType
        {
            [Description("Lipa na M-Pesa")]
            LipanaMPesa = 6,

            [Description("PayBill")]
            PayBill = 7,

            [Description("Business to Agent")]
            BusinessToAgent = 16
        }

        public enum RoleCategory
        {
            [Description("Administrator")]
            Administrator = 1,

            [Description("LoanManager")]
            LoanManager = 2,

            [Description("LoanOfficer")]
            LoanOfficer = 3,

            [Description("Customer")]
            Customer = 4,

            [Description("FinanceManager")]
            FinanceManager = 5,
        }

        public enum BranchType
        {
            [Description("Branch")]
            Branch = 0,

            [Description("Agency")]
            Agency = 1
        }

        public enum InterestChargeType
        {
            [Description("Fixed Amount")]
            Fixed = 0,

            [Description("Percentage(%)")]
            Percentage = 1
        }

        public enum ChargeRecoveryMethod
        {
            [Description("Up Front")]
            UpFront = 0,

            [Description("Per Installment")]
            PerInstallment = 1
        }

        public enum SPSAPIErrorCodes
        {
            INVALIDUSERNAMEORPASSWORD = 401,
            MANDTORYFIELDMISSING = 403,
            INVALIDURL = 403,
            WRONGCOMPNAYIDORDUPLICATESTAN = 412,
            INVALIDBANKCODE = 500,
            QUERYTRANSACTIONWITHIDTHATDOESNTEXIST = 400,
            TOOMANYREQUESTS = 429
        }

        public enum UploadFileCategory
        {
            [Description("Processing Fee")]
            ProcessingFee = 1,

            [Description("Reconciliation")]
            Reconciliation = 2
        }

        public enum FileProcessingStatus
        {
            [Description("Pending")]
            Pending = 0,

            [Description("Queued")]
            Queued = 1,

            [Description("Processed")]
            Processed = 2,

            [Description("Rejected")]
            Rejected = 3
        }

        public enum FileContentType
        {
            [Description("application/vnd.ms-excel")]
            applicationvndmsexcel = 1,

            [Description("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")]
            applicationvndopenxmlformatsofficedocumentspreadsheetmlsheet = 2
        }

        public enum Elections
        {
            [Description("Democratic")]
            Democratic = 1,

            [Description("Rotational")]
            Rotational = 2,

            [Description("Rotational")]
            Others = 3
        }

        public enum GroupRoles
        {
            [Description("Chair Person")]
            Chairman = 1,

            [Description("Secretary")]
            Secretary = 2,

            [Description("Treasurer")]
            Treasurer = 3,

            [Description("Member")]
            Member = 4
        }

        public enum ReportCategory
        {
            [Description("Management")]
            Management = 1,

            [Description("Field")]
            Field = 2,

            [Description("Branch")]
            Branch =3,

            [Description("Back Office")]
            BackOffice = 4
        }

        public enum MeetingDay
        {
            Monday = 1,

            Tuesday = 2,

            Wednesday = 3,

            Thursday = 4,

            Friday = 5,

            Saturday = 6,

            Sunday = 7
        }

        public enum MeetingFrequency
        {
            Weekly = 1,

            Monthy = 2,

            FortNightly = 3
        }

        public enum RecordStatus
        {
            [Description("Approved")]
            Approved,
            [Description("New")]
            New,
            [Description("Edited")]
            Edited,
            [Description("Rejected")]
            Rejected,
            [Description("Extracted")]
            Extracted,
        }

        public enum PrincipalRecoveryMethod
        {
            PerInstallment = 0,

            Full = 1
        }

        public enum Title
        {
            [Description("Mr")]
            Mr = 1,
            
            [Description("Mrs")]
            Mrs = 2,
            
            [Description("Miss")]
            Miss = 3,
            
            [Description("Prof")]
            Prof = 4,
            
            [Description("Dr")]
            Dr = 5
        }

        public enum LoanType
        {
            [Description("Individual Loan")]
            Individual = 0,

            [Description("Group Loan")]
            Group = 1
        }

        public enum GroupType
        {
            [Description("Registered")]
            Registered = 1,

            [Description("Unregistered")]
            Unregistered = 2
        }

        
    }
}

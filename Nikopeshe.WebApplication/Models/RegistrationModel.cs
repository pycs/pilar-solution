﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nikopeshe.WebApplication.Models
{
    public class RegistrationModel
    {
        [Required]        
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        [Required]        
        public string LastName { get; set; }

        [Required]                    
        [RegularExpression(@"^254(?:[0-9]??){6,14}[0-9]$", ErrorMessage = "The mobile number should start 254, followed by the national number")]
        public string MobileNumber { get; set; }
        
        [Required]        
        [RegularExpression("^[0-9]+$", ErrorMessage = "Invalid Id number. Must be 0-9")]        
        public string IDNumber { get; set; }

        [Display(Name = "KRA PIN")]        
        public string KRAPIN { get; set; }
        
        [Required]        
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [Required]        
        public string DateofBirth { get; set; }
        
        [Required]        
        public string Occupation { get; set; }
        
        [Required]        
        public string IncomeRange { get; set; }

        /// <summary>
        /// AKA Password
        /// </summary>
        /// 
        [Required]                
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$", ErrorMessage = "The Password must have a minimum 8 characters atleast 1 Alphabet, 1 Number and 1 Special Character")]
        public string PIN { get; set; }      

        public int MaritalStatus { get; set; }

        public int Gender { get; set; }      
    }
}
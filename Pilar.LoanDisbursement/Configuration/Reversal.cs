﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.Disbursement;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.LoanDisbursement.Configuration
{
    public class Reversal : IJob
    {
        private readonly IMessageQueueService _messageQueueService;
        private readonly ILoanProcessing _iloanprocessing;
        private readonly string _messageRelayQueuePath;
        private DataContext datacontext = new DataContext();
        public Reversal()
        {
            _messageQueueService = new MessageQueueService();
            _iloanprocessing = new LoanProcessing();
            _messageRelayQueuePath = ConfigurationManager.AppSettings["SPSQueuePath"];
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                //1. Find Rejected Loans at disbursements
                var findpendingdisbursementloanrequest = _iloanprocessing.FindLoanRequestByStatus((int)Enumerations.LoanStatus.Review);
                if (findpendingdisbursementloanrequest != null && findpendingdisbursementloanrequest.Any())
                    foreach (var pendingloanrequest in findpendingdisbursementloanrequest)
                    {
                        _messageQueueService.Send(_messageRelayQueuePath, pendingloanrequest.Id, Nikopeshe.Entity.DataModels.Enumerations.MessageCategory.LoanTransactionsReversal, System.Messaging.MessagePriority.High);
                        pendingloanrequest.ProcessingStatus = (int)Enumerations.LoanStatus.Reversed;
                        _iloanprocessing.UpdateLoanRequest(pendingloanrequest);
                    }  
                
                //2. Find reversed loans after disbursement

                var findLoanRequestWithCompleteReversalStatus = _iloanprocessing.FindLoanRequestByStatus((int)Enumerations.LoanStatus.CompleteReversal);

                if (findLoanRequestWithCompleteReversalStatus.Any() && findLoanRequestWithCompleteReversalStatus != null)
                {
                    foreach (var reversalLoan in findLoanRequestWithCompleteReversalStatus)
                    {
                        _messageQueueService.Send(_messageRelayQueuePath, reversalLoan.Id, Nikopeshe.Entity.DataModels.Enumerations.MessageCategory.CompleteLoanTransactionsReveral, System.Messaging.MessagePriority.High);
                        reversalLoan.ProcessingStatus = (int)Enumerations.LoanStatus.Reversed;
                        _iloanprocessing.UpdateLoanRequest(reversalLoan);
                    }
                }
            }

            catch(Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }
    }
}

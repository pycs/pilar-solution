﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Nikopeshe.Entity.DataModels
{    
    public class DashBoard : BaseEntity
    {
        [MaxLength(100)]
        public string Title { get; set; }

        //[AllowHtml]
        [MaxLength(1000)]
        public string Content { get; set; }

        public bool IsPublished { get; set; }
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class ReportService : IReportService
    {
        private readonly IRepository<Report> reportServiceRepository;

        public ReportService(IRepository<Report> _reportServiceRepository)
        {
            if (_reportServiceRepository == null)
                throw new ArgumentNullException("reports service cannot be null.");

            this.reportServiceRepository = _reportServiceRepository;
        }

        public void AddNewReport(Report report, ServiceHeader serviceHeader)
        {
            if (report == null)
                throw new ArgumentNullException("null entity report");

            reportServiceRepository.Insert(report, serviceHeader);
        }

        public void UpdateReport(Report report, ServiceHeader serviceHeader)
        {
            if (report == null)
                throw new ArgumentNullException("null entity report");

            reportServiceRepository.Update(report, serviceHeader);
        }

        public IList<Report> GetAllReports()
        {
            var findallreports = reportServiceRepository.GetAll(IsolationLevel.ReadUncommitted).ToList();

            return findallreports;
        }

        public IList<Report> GetAllReportsPerRole(int RoleId)
        {
            var findallreports = reportServiceRepository.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.RoleCategory == RoleId).ToList();

            return findallreports;
        }

        public void DeleteReport(Report report, ServiceHeader serviceHeader)
        {
            if (report == null)
                throw new ArgumentNullException("null entity report");

            report.IsEnabled = false;
            reportServiceRepository.Update(report, serviceHeader);
        }

        public Report FindReportById(Guid Id)
        {
            var findreportbyId = reportServiceRepository.GetById(Id);
            return findreportbyId;
        }

        public IList<Report> GetAllReportsByRole(int RoleCategory)
        {
            if (RoleCategory == 0)
                throw new ArgumentNullException("Role Category cannot be null");

            var findReportsByRoleCategory = reportServiceRepository.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.RoleCategory == RoleCategory);

            return findReportsByRoleCategory.ToList();
        }

        //public IList<Report> GetAllReportsByRoleName(string roleName)
        //{
        //    int roleCatergory = -1;
        //    if (roleName.ToLower().Equals(EnumHelper.GetDescription(Enumerations.RoleCategory.Administrator).ToLower()))
        //    {
        //        roleCatergory = (int)Enumerations.RoleCategory.Administrator;
        //    }
        //    else if (roleName.ToLower().Equals(EnumHelper.GetDescription(Enumerations.RoleCategory.FinanceManager).ToLower()))
        //    {
        //        roleCatergory = (int)Enumerations.RoleCategory.FinanceManager;
        //    }
        //    else if (roleName.ToLower().Equals(EnumHelper.GetDescription(Enumerations.RoleCategory.LoanManager).ToLower()))
        //    {
        //        roleCatergory = (int)Enumerations.RoleCategory.LoanManager;
        //    }
        //    else
        //    {
        //        roleCatergory = (int)Enumerations.RoleCategory.LoanOfficer;
        //    }

        //    if (string.IsNullOrWhiteSpace(roleName))
        //        throw new ArgumentNullException("Role Category cannot be null");

        //    //var findReportsByRole = reportServiceRepository.GetAll().Where(x => x roleCatergory).ToList();

        //    return findReportsByRole;
        //}
    }
}

﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize(Roles = "Administrator")]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class ChartofAccountController : Controller
    {
        private readonly IChartofAccountService chartofaccountservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public ChartofAccountController(IChartofAccountService _chartofaccountservice)
        {
            if (_chartofaccountservice == null)
                throw new ArgumentNullException("null service reference");

            this.chartofaccountservice = _chartofaccountservice;
        }


        public ActionResult chartofaccountslist()
        {
            return View();
        }
        //
        // GET: /ChartofAccount/
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getallchartofaccounts(int id, JQueryDataTablesModel datatablemodel)
        {
            var pagecollection = chartofaccountservice.GetAllChartofAccountByAccountType(id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var ChartofAccountList = GetAllChartofAccounts(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, ChartofAccountList: pagecollection);


            return Json(new JQueryDataTablesResponse<ChartofAccount>(items: ChartofAccountList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<ChartofAccount> GetAllChartofAccounts(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<ChartofAccount> ChartofAccountList)
        {
            var ChartofAccount = ChartofAccountList;

            totalRecordCount = ChartofAccount.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                ChartofAccount = ChartofAccount.Where(c => c.AccountName
                    .ToLower().Contains(searchString.ToLower()) || c.AccountCode.ToString().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = ChartofAccount.Count;

            IOrderedEnumerable<ChartofAccount> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "Parent.AccountName":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.Parent.AccountName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Parent.AccountName);
                        break;

                    case "ChartOfAccountTypeDescription":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.ChartOfAccountTypeDescription)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChartOfAccountTypeDescription);
                        break;

                    case "AccountCode":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.AccountCode)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.AccountCode);
                        break;

                    case "AccountName":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.AccountName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.AccountName);
                        break;

                    case "ChartOfAccountCategoryDescription":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.ChartOfAccountCategoryDescription)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChartOfAccountCategoryDescription);
                        break;

                    case "IsActive":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.IsActive)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsActive);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult addnewchartofaccount()
        {
            ViewBag.AccountIdentifier = ChartOfAccountCategorySelectList();

            return View();
        }

        [HttpPost]
        public ActionResult addnewchartofaccount(ChartofAccount chartofaccount, FormCollection collection)
        {
            try
            {
                ViewBag.AccountIdentifier = ChartOfAccountCategorySelectList();

                string parentid = Request.Form["parent"];

                if (string.IsNullOrWhiteSpace(parentid))
                {
                    ModelState.AddModelError(string.Empty, "Parent GL cannot be null");
                    return View(chartofaccount);
                }

                chartofaccount.ParentId = Guid.Parse(parentid);

                if (chartofaccount.ChartOfAccountCategory == (int)Enumerations.ChartOfAccountCategory.HeaderAccount)
                {
                    chartofaccount.Depth = 1;
                }
                else
                {
                    chartofaccount.Depth = 2;
                }

                bool validateaccountcode = chartofaccountservice.ValidateAccountCode(chartofaccount.AccountCode);

                if (validateaccountcode)
                {
                    ModelState.AddModelError(string.Empty, "Account code already exist.");
                    return View(chartofaccount);
                }

                chartofaccountservice.AddNewChartofAccount(chartofaccount, serviceHeader);

                return RedirectToAction("chartofaccountslist");
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View(chartofaccount);
            }
        }

        public ActionResult chartofaccountsdetails(Guid Id)
        {
            var findchartofaccountbyid = chartofaccountservice.FindChartofAccountById(Id);
            string parentaccountname = "n/a";
            if (findchartofaccountbyid.ParentId != null && findchartofaccountbyid.ParentId != Guid.Empty)
            {
                var parentid = Guid.Parse(findchartofaccountbyid.ParentId.ToString());
                var parentachartofaccount = chartofaccountservice.FindChartOfAccountByParentId(parentid);
                parentaccountname = parentachartofaccount.AccountName;
            }
            ViewBag.ParentName = parentaccountname;
            return View(findchartofaccountbyid);
        }

        [HttpGet]
        public ActionResult NewChartofAccount()
        {
            ViewBag.ParentAccountSelectList = ChartOfAccountCategorySelectList();
            ViewBag.AccountTypeSelectList = GetChartofAccountTypesSelectList(string.Empty);
            ViewBag.AccountCategorySelectList = GetChartofAccountsByCategorySelectList(string.Empty, string.Empty);

            return View();
        }

        [HttpPost]
        public ActionResult NewChartofAccount(ChartofAccount chartofaccount)
        {
            ViewBag.ParentAccountSelectList = ChartOfAccountCategorySelectList();
            ViewBag.AccountTypeSelectList = GetChartofAccountTypesSelectList(string.Empty);
            ViewBag.AccountCategorySelectList = GetChartofAccountsByCategorySelectList(string.Empty, string.Empty);

            try
            {
                bool validateaccountcode = chartofaccountservice.ValidateAccountCode(chartofaccount.AccountCode);

                if (validateaccountcode)
                {
                    ModelState.AddModelError(string.Empty, "Account code already exist.");
                    return View(chartofaccount);
                }

                chartofaccountservice.AddNewChartofAccount(chartofaccount, serviceHeader);
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View(chartofaccount);
            }
            return View();
        }

        [HttpGet]
        public ActionResult EditChartofAccounts(Guid Id)
        {
            var findchartofaccountbyid = chartofaccountservice.FindChartofAccountById(Id);
            ViewBag.ParentAccountSelectList = ChartOfAccountCategorySelectList();
            ViewBag.AccountTypeSelectList = GetChartofAccountTypesSelectList(string.Empty);
            ViewBag.AccountCategorySelectList = GetChartofAccountsByCategorySelectList(string.Empty, string.Empty);

            return View(findchartofaccountbyid);
        }

        [HttpPost]
        public ActionResult EditChartofAccounts(ChartofAccount chartofaccounts)
        {
            ViewBag.ParentAccountSelectList = ChartOfAccountCategorySelectList();
            ViewBag.AccountTypeSelectList = GetChartofAccountTypesSelectList(string.Empty);
            ViewBag.AccountCategorySelectList = GetChartofAccountsByCategorySelectList(string.Empty, chartofaccounts.ParentId.ToString());

            try
            {
                ChartofAccount _chartofaccount = chartofaccountservice.FindChartofAccountById(chartofaccounts.Id);
                if (chartofaccounts.ParentId == Guid.Empty)
                {
                    _chartofaccount.ParentId = _chartofaccount.ParentId;
                }
                _chartofaccount.ParentId = chartofaccounts.ParentId;
                _chartofaccount.AccountCode = chartofaccounts.AccountCode;
                _chartofaccount.AccountName = chartofaccounts.AccountName;
                _chartofaccount.ChartOfAccountCategory = chartofaccounts.ChartOfAccountCategory;
                _chartofaccount.ChartOfAccountType = chartofaccounts.ChartOfAccountType;
                _chartofaccount.IsActive = chartofaccounts.IsActive;
                chartofaccountservice.UpdateChartofAccount(_chartofaccount, serviceHeader);
                return RedirectToAction("chartofaccountslist");
            }

            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Operation was unsuccessful.");
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View(chartofaccounts);
            }
        }

        [HttpGet]
        public ActionResult EditChartofAccount(Guid Id)
        {
            var findchartofaccountbyid = chartofaccountservice.FindChartofAccountById(Id);

            ViewBag.AccountIdentifier = ChartOfAccountCategorySelectList();

            //ViewBag.Productname = findproductbyid.ProductName;

            return View(findchartofaccountbyid);
        }

        [HttpPost]
        public ActionResult EditChartofAccount(ChartofAccount chartofaccount, FormCollection collection)
        {
            chartofaccount.CreatedDate = DateTime.Now;

            ViewBag.AccountIdentifier = ChartOfAccountCategorySelectList();

            string parentid = collection["parent"].ToString();

            if (!string.IsNullOrWhiteSpace(parentid))
            {
                chartofaccount.ParentId = Guid.Parse(parentid);
            }

            bool validateaccountcode = chartofaccountservice.ValidateAccountCode(chartofaccount.AccountCode);


            //if(validateaccountcode)
            //{
            //    ModelState.AddModelError(string.Empty, "Account code already exist.");
            //    return View(chartofaccount);
            //}

            try
            {
                ChartofAccount _chartofaccount = chartofaccountservice.FindChartofAccountById(chartofaccount.Id);
                if (chartofaccount.ParentId == Guid.Empty)
                {
                    chartofaccount.ParentId = _chartofaccount.ParentId;
                }
                _chartofaccount.ParentId = chartofaccount.ParentId;
                _chartofaccount.AccountCode = chartofaccount.AccountCode;
                _chartofaccount.AccountName = chartofaccount.AccountName;
                _chartofaccount.ChartOfAccountCategory = chartofaccount.ChartOfAccountCategory;
                _chartofaccount.ChartOfAccountType = chartofaccount.ChartOfAccountType;
                _chartofaccount.IsActive = chartofaccount.IsActive;
                chartofaccountservice.UpdateChartofAccount(_chartofaccount, serviceHeader);
                return RedirectToAction("chartofaccountslist");
            }

            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View(chartofaccount);
            }
        }

        public ActionResult Accountstatement(int Id)
        {
            ChartofAccount chartofaccount = chartofaccountservice.FindChartofAccountByAccountCode(Id);

            ViewBag.AccountCode = chartofaccount.AccountCode;

            return View();
        }

        public JsonResult GetGLAccountStatement(int Id, JQueryDataTablesModel datatablemodel)
        {
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            ChartofAccount chartofaccount = chartofaccountservice.FindChartofAccountByAccountCode(Id);

            var pagecollection = chartofaccountservice.GetChartofAccountStatement(chartofaccount.Id);

            var ChartofAccountStatementpagecollection = ChartofAccountStatement(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, ChartofAccountStatementList: pagecollection);


            return Json(new JQueryDataTablesResponse<LoanTransactionHistory>(items: ChartofAccountStatementpagecollection,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<LoanTransactionHistory> ChartofAccountStatement(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanTransactionHistory> ChartofAccountStatementList)
        {
            var ChartofAccountStament = ChartofAccountStatementList;

            totalRecordCount = ChartofAccountStament.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                ChartofAccountStament = ChartofAccountStament.Where(c => c.ChartofAccount.AccountName
                    .ToLower().Contains(searchString.ToLower()) || c.ContraAccountChartofAccount.AccountName.ToString().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = ChartofAccountStament.Count;

            IOrderedEnumerable<LoanTransactionHistory> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? ChartofAccountStament.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "LoanChartofAccountGL.AccountName":
                        sortedClients = sortedClients == null ? ChartofAccountStament.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.AccountName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.AccountName);
                        break;

                    case "ContraAccountChartofAccount.AccountName":
                        sortedClients = sortedClients == null ? ChartofAccountStament.CustomSort(sortedColumn.Direction, cust => cust.ContraAccountChartofAccount.AccountName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ContraAccountChartofAccount.AccountName);
                        break;

                    case "TransactionAmount":
                        sortedClients = sortedClients == null ? ChartofAccountStament.CustomSort(sortedColumn.Direction, cust => cust.TransactionAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransactionAmount);
                        break;

                    case "TransactionTypeDesc":
                        sortedClients = sortedClients == null ? ChartofAccountStament.CustomSort(sortedColumn.Direction, cust => cust.TransactionTypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransactionTypeDesc);
                        break;

                    case "LoanRequest.LoanReferenceNumber":
                        sortedClients = sortedClients == null ? ChartofAccountStament.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.LoanReferenceNumber);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [NonAction]
        protected List<SelectListItem> GetChartofAccountTypesSelectList(string selectedValue)
        {
            List<SelectListItem> banksSelectList = new List<SelectListItem> { };

            var defaultBankItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = "Choose Account Type", Value = string.Empty };

            banksSelectList.Add(defaultBankItem);

            var otherBankItems = ChartOfAccountTypeSelectList();

            if (otherBankItems != null)
            {
                foreach (var item in otherBankItems)
                {
                    banksSelectList.Add(new SelectListItem { Selected = item.Selected.ToString() == selectedValue.ToString(), Text = item.Text, Value = item.Value });
                }
            }
            return banksSelectList;
        }

        [NonAction]
        protected List<SelectListItem> GetChartofAccountsByCategorySelectList(string bankCode, string selectedValue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultBranchItem = new SelectListItem { Selected = (selectedValue == string.Empty), Disabled = true, Text = "Choose Parent Chart of Accounts", Value = string.Empty };

            SelectList.Add(defaultBranchItem);

            var otherBranchItems = GetDetailAccountTypes(selectedValue);

            if (otherBranchItems != null)
            {
                foreach (var item in otherBranchItems)
                {
                    SelectList.Add(new SelectListItem { Selected = item.Selected.ToString() == selectedValue, Text = item.Text.ToString(), Value = item.Value });
                }
            }

            return SelectList;
        }

        [NonAction]
        public List<SelectListItem> GetDetailAccountTypes(string selectedValue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultItem = new SelectListItem { Selected = (string.Concat(selectedValue) == string.Empty), Text = "", Value = string.Empty };

            SelectList.Add(defaultItem);
            int x = 0;
            if (!string.IsNullOrWhiteSpace(selectedValue))
            {
                x = int.Parse(selectedValue);
            }
            var otherItems = chartofaccountservice.GetChartofAccountByAccountType(x);

            if (otherItems != null)
            {
                foreach (var item in otherItems)
                {
                    SelectList.Add(new SelectListItem { Selected = item.Id.ToString() == selectedValue, Text = item.ChartOfAccountDescriptionByCategory, Value = item.Id.ToString() });
                }
            }
            return SelectList;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult AccountSelectList(string id)
        {
            //if(string.IsNullOrWhiteSpace(id))
            //{
            //    throw new ArgumentNullException("Must select a value from the drop down");             
            //}
            var accountList = GetDetailAccountTypes(id);

            //var accountlist = GetBranchesList(id);

            var modelData = accountList.Select(m => new SelectListItem()
            {
                Text = m.Text,
                Value = m.Value,
                Selected = m.Selected,
            });

            return Json(modelData, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public List<SelectListItem> ChartOfAccountCategorySelectList()
        {
            List<SelectListItem> obj = new List<SelectListItem>();
            obj.Add(new SelectListItem { Text = "Select Account Category", Value = "" });

            obj.Add(new SelectListItem { Text = "HeaderAccount", Value = (0x1000).ToString() });

            obj.Add(new SelectListItem { Text = "DetailAccount", Value = (0x1000 + 1).ToString() });

            return obj;
        }

        [NonAction]
        public List<SelectListItem> ChartOfAccountTypeSelectList()
        {
            List<SelectListItem> obj = new List<SelectListItem>();

            obj.Add(new SelectListItem { Text = "Asset", Value = (0xF4240).ToString() });

            obj.Add(new SelectListItem { Text = "Liability", Value = (0x1E8480).ToString() });

            obj.Add(new SelectListItem { Text = "Equity", Value = (0x2DC6C0).ToString() });

            obj.Add(new SelectListItem { Text = "Income", Value = (0x3D0900).ToString() });

            obj.Add(new SelectListItem { Text = "Expense", Value = (0x4C4B40).ToString() });

            return obj;
        }

    }
}
﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    [Serializable]
    public class EmailAlert : BaseEntity
    {        
        [Display(Name = "From")]
        [Required]
        [Column(TypeName = "VARCHAR")]
        [Index]
        [MaxLength(100)]
        public string MailMessageFrom { get; set; }

        [Display(Name = "Recipient")]
        [Column(TypeName = "VARCHAR")]
        [Index]
        [MaxLength(100)]
        public string MailMessageTo { get; set; }

        [Display(Name = "CC")]
        [MaxLength(100)]
        public string MailMessageCC { get; set; }

        [Display(Name = "Subject")]
        [Required]
        [MaxLength(100)]
        [Column(TypeName = "VARCHAR")]
        [Index]
        public string MailMessageSubject { get; set; }

        [Display(Name = "Message")]
        [Required]
        [Column(TypeName = "VARCHAR")]
        [Index]
        [MaxLength(899)]
        public string MailMessageBody { get; set; }

        [Display(Name = "Message")]
        [MaxLength(100)]
        public string MaskedMailMessageBody { get; set; }

        [Display(Name = "Is Body Html?")]
        public bool MailMessageIsBodyHtml { get; set; }

        [Display(Name = "DLR Status")]
        [Index]
        public int MailMessageDLRStatus { get; set; }

        [Display(Name = "DLR Status")]
        public string MailMessageDLRStatusDescription
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.DLRStatus), MailMessageDLRStatus) ? EnumHelper.GetDescription((Enumerations.DLRStatus)MailMessageDLRStatus) : string.Empty;
            }
        }

        [Display(Name = "Origin")]
        public int MailMessageOrigin { get; set; }
        
        [Display(Name = "Origin")]
        public string MailMessageOriginDescription
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.MessageOrigin), MailMessageOrigin) ? EnumHelper.GetDescription((Enumerations.MessageOrigin)MailMessageOrigin) : string.Empty;
            }
        }

        [Display(Name = "Send Retry")]
        public int MailMessageSendRetry { get; set; }

        [Display(Name = "Security Critical")]
        public bool MailMessageSecurityCritical { get; set; }

        [Display(Name = "Priority")]
        public int MailMessagePriority { get; set; }
        
        [Display(Name = "Created By")]
        [MaxLength(100)]
        public string CreatedBy { get; set; }

    }
}

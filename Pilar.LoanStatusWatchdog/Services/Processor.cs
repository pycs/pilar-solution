﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using System;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Messaging;

namespace Pilar.LoanStatusWatchdog.Services
{
    [Export(typeof(IPlugin))]
    public class Processor : IPlugin
    {
        private MessageQueue _messageQueue;
        private readonly string _queuePath;
        private DataContext datacontext = new DataContext();

        [ImportingConstructor]
        public Processor()
        {
            _queuePath = ConfigurationManager.AppSettings["LoanStatusWatchDogQueuePath"];
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{6C1EE688-C724-4BA6-89B5-652A73547420}"); }
        }

        public string Description
        {
            get { return "LOAN STATUS WATCH DOG PROCESSOR"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                if (!MessageQueue.Exists(_queuePath))
                    throw new ArgumentException("Queue does not exist at specified path", "_queuePath");
                else
                {
                    // initialize message queue
                    _messageQueue = new MessageQueue(_queuePath);

                    // enable the AppSpecific field in the messages
                    _messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                    // set the formatter to binary.
                    _messageQueue.Formatter = new BinaryMessageFormatter();

                    // set an event to listen for new messages.
                    _messageQueue.ReceiveCompleted += MessageQueue_ReceiveCompleted;

                    // start listening
                    _messageQueue.BeginReceive();
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        public void Exit()
        {
            _messageQueue.Close();
        }

        private void MessageQueue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                // retrieve sms alert id
                var recordId = (Guid)e.Message.Body;

                // retrieve message category
                var messageCategory = (Enumerations.MessageCategory)e.Message.AppSpecific;

                switch (messageCategory)
                {
                    case Enumerations.MessageCategory.LoanStatusWatchDog:

                        #region email

                        var item = datacontext.LoanRequest.Find(recordId);

                        if (item != null)
                        {
                            item.Status = (int)Enumerations.LoanStatus.Active;
                            var entity = datacontext.Entry(item);
                            entity.State = System.Data.Entity.EntityState.Modified;
                            datacontext.SaveChanges();
                        }

                        #endregion

                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->Process->ReceiveCompleted FAILED...ReceiveCompletedEventArgs={1}" + ex + Description + e);
            }

            finally
            {
                // listen for the next message.
                _messageQueue.BeginReceive();
            }
        }

        #region Event Handlers
        #endregion
        #endregion
    }
}

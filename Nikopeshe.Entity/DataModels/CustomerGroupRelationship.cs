﻿using Nikopeshe.Entity.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Entity.DataModels
{
    public class CustomerGroupRelationship : BaseEntity
    {
        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        public Guid GroupId { get; set; }

        public virtual Group Group { get; set; }

        public string GroupMemberNo { get; set; }

        public int GroupRole { get; set; }

        public string GroupRoleDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.GroupRoles), GroupRole) ? EnumHelper.GetDescription((Enumerations.GroupRoles)GroupRole) : string.Empty;
            }
        }
    }
}

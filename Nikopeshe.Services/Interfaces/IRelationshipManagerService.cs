﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface IRelationshipManagerService
    {
        void AddNewRelationshipManager(RelationshipManager relationshipManager, ServiceHeader serviceHeader);

        void UpdateRelationshipManager(RelationshipManager relationshipManager, ServiceHeader serviceHeader);

        List<RelationshipManager> GetAllRelationshipManagers(IsolationLevel isolationLevel);

        RelationshipManager FindRelationshipManagerById(Guid Id);

        List<RelationshipManager> FindRelationshipManagerSelectListById(Guid Id);

        RelationshipManager FindRelationshipManagerByIdNumber(string IdNumber);

        RelationshipManager FindRelationshipManagerByMobileNumber(string MobileNumber);

        List<RelationshipManager> FindRelationshipManagerByBranchId(Guid BranchId);

        List<RelationshipManager> FindRelationshipManagerByBranchIdAndRelationshipManagerId(Guid BranchId, Guid RelationshipManagerid);
    }
}

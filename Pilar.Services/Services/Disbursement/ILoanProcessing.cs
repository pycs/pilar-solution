﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
namespace Pilar.Services.Services.Disbursement
{
    public interface ILoanProcessing
    {
        IList<LoanRequest> GetPendingDisbursmentLoanRequest();

        void UpdateLoanRequest(LoanRequest loanrequest);

        IList<LoanRequest> GetLoanDibursementWithPendingGetStatus();

        LoanRequest FindLoanRequestById(Guid Id);

        IList<LoanRequest> FindLoanRequestDueGivenDays();

        IList<LoanRequest> FindLoanRequestByStatus(int Status);

        IList<LoanTransactionHistory> FindLoanTransactionHistoryByLoanRequestId(Guid LoanRequestId);

        IList<LoanTransactionHistory> GetAllLoanRequestTransactionsByLoanRequestId(Guid LoanRequestId);
    }
}

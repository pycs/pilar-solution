﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{
    public class GroupHelper : BaseEntity
    {
        public string GroupCode { get; set; }

        public string GroupName { get; set; }

        public int CountyCode { get; set; }

        public string GroupCodeGroupName
        {
            get
            {
                return GroupCode + " => " + GroupName;
            }
        }
    }
}

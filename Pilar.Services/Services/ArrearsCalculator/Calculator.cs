﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.ArrearsCalculator
{
    public class Calculator : ICalculator
    {
        DataContext context = new DataContext();
        public List<SPResult> SPResultsList()
        {
            var returnList = new List<SPResult>();
            using (var dataContext = new DataContext())
            {
                dataContext.Database.CommandTimeout = 1800;   
                try
                {
                    var idParam = new SqlParameter { ParameterName = "BranchId",  IsNullable = true, Value = 1000};
                    var results = dataContext.Database.SqlQuery<SPResult>("spPilarOutstandingLoanBalance @BranchId", idParam).ToList<SPResult>();

                    returnList = results;
                }

                catch(Exception ex)
                {
                    //LoggerFactory.CreateLog().LogInfo("{0}->Arrears Calculator..." + ex);
                }

                finally
                {
                    dataContext.Database.Connection.Close();
                }
                return returnList;
            }
        }

        public Guid FindCustomerSavingsAccount(Guid LoanRequestId)
        {
            var customerSavingsAccountId = Guid.Empty;

            if (LoanRequestId == Guid.Empty)
                throw new ArgumentNullException("id cannot be null");

            var findLoanRequestByLoanRequestId = context.LoanRequest.Find(LoanRequestId);
            if (findLoanRequestByLoanRequestId != null)
            {
                var customerId = findLoanRequestByLoanRequestId.CustomerAccount.CustomerId;
                var findCustomerSavingAccountId = context.CustomersAccount.Where(x => x.CustomerId == customerId && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();

                if (findCustomerSavingAccountId != null)
                {
                    customerSavingsAccountId = findCustomerSavingAccountId.Id;
                }

            }

            return customerSavingsAccountId;
        }
    }
}

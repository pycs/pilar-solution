﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    public class UserInfo : BaseEntity
    {
        [Index]
        [Display(Name = "Branch Name")]
        public Guid BranchId { get; set; }

        public virtual Branch Branch { get; set; }

        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "Email is a mandatory field")]
        [MaxLength(100)]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Not a valid Email Address")]
        public string Email { get; set; }     

        [Display(Name = "Is Approved?")]
        public bool IsApproved { get; set; }

        [Display(Name = "Is Locked Out?")]
        public bool IsLockedOut { get; set; }

        [Display(Name = "Username")]
        [Required(ErrorMessage = "Username is a mandatory field")]
        [MaxLength(100)]
        [Column(TypeName = "VARCHAR")]
        [Index("UserName")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "First Name is a mandatory field")]
        [Display(Name = "First Name")]
        [MaxLength(100)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is a mandatory field")]
        [Display(Name = "Last Name")]
        [MaxLength(100)]
        public string LastName { get; set; }

        [Display(Name = "Role(s)")]
        [NotMapped]
        public string Roles { get; set; }

        [NotMapped]
        [Display(Name = "Commands")]
        public string Commands { get; set; }

        [NotMapped]
        public string BranchName { get; set; }
    }
}

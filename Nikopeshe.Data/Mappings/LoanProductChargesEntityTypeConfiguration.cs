﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Data.Mappings
{
    public class LoanProductChargesEntityTypeConfiguration : EntityTypeConfiguration<LoanProductsCharge>
    {
        public LoanProductChargesEntityTypeConfiguration()
        {
            this.HasKey(x => x.Id);
            this.ToTable(string.Format("{0}LoanProductsCharges", DefaultSettings.Instance.TablePrefix));
        }
    }
}

﻿using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crosscutting.NetFramework.Logging
{
    public class ExceptionLoggerFactory
    {
        public static void CreateLog(string errorMessage)
        {
            Logger.SetLogWriter(new LogWriterFactory().Create());
            LogEntry entry = new LogEntry();
            entry.Message = errorMessage;
            Logger.Write(entry);
        }
    }
}

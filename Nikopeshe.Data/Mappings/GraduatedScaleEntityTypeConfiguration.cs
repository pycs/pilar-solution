﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Data.Mappings
{
    public class GraduatedScaleEntityTypeConfiguration : EntityTypeConfiguration<GraduatedScale>
    {
        public GraduatedScaleEntityTypeConfiguration()
        {
            this.HasKey(x => x.Id);
            this.ToTable(string.Format("{0}GraduatedScales", DefaultSettings.Instance.TablePrefix));
        }
    }
}

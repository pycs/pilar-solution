﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    public class LoanRequestCharge : BaseEntity
    {
        [Index]
        public Guid LoanRequestId { get; set; }

        public virtual LoanRequest LoanRequest { get; set; }

        [Index]
        public Guid GraduatedScaleId { get; set; }

        public virtual GraduatedScale GraduatedScale { get; set; }

        [Index]
        public Guid ChartofAccountId { get; set; }

        public virtual ChartofAccount ChartofAccount { get; set; }

        public decimal TransactionAmount { get; set; }
    }
}

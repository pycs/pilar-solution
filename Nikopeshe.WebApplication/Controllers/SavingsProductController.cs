﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize(Roles = "Administrator")]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class SavingsProductController : Controller
    {
        private readonly ISavingsProductService savingsproductservice;
        private readonly IChartofAccountService chartofaccountservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public SavingsProductController(ISavingsProductService _savingsproductservice, IChartofAccountService _chartofaccountservice)
        {
            if(_savingsproductservice == null || _chartofaccountservice == null)
                throw new ArgumentNullException("null service reference");

            this.savingsproductservice = _savingsproductservice;
            this.chartofaccountservice = _chartofaccountservice;
        }

        // GET: SavingsProduct
        public ActionResult Index()
        {            
            return View();
        }

        public JsonResult GetAllSavingsProduct(JQueryDataTablesModel datatablemodel)
        {
            var totalrecordcount = 0;

            var searchedrecordcount = 0;

            var pagecollection = savingsproductservice.GetAllSavingsAccountProducts(IsolationLevel.ReadUncommitted);

            var loanrequestList = GetAllSavingsProductList(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalrecordcount, searchRecordCount: out searchedrecordcount, searchString: datatablemodel.sSearch, SavingsProductList: pagecollection);


            return Json(new JQueryDataTablesResponse<SavingsProduct>(items: loanrequestList,
                totalRecords: totalrecordcount,
                totalDisplayRecords: searchedrecordcount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<SavingsProduct> GetAllSavingsProductList(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<SavingsProduct> SavingsProductList)
        {
            var GetSavingsProductList = SavingsProductList;

            totalRecordCount = GetSavingsProductList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetSavingsProductList = SavingsProductList.Where(c => c.ProductName
                    .ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetSavingsProductList.Count;

            IOrderedEnumerable<SavingsProduct> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetSavingsProductList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "ChartofAccount.AccountName":
                        sortedClients = sortedClients == null ? GetSavingsProductList.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.AccountName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.AccountName);
                        break;

                    case "ProductName":
                        sortedClients = sortedClients == null ? GetSavingsProductList.CustomSort(sortedColumn.Direction, cust => cust.ProductName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ProductName);
                        break;

                    case "ProductCode":
                        sortedClients = sortedClients == null ? GetSavingsProductList.CustomSort(sortedColumn.Direction, cust => cust.ProductCode)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ProductCode);
                        break;

                    case "IsDefault":
                        sortedClients = sortedClients == null ? GetSavingsProductList.CustomSort(sortedColumn.Direction, cust => cust.IsDefault)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsDefault);
                        break;

                    case "IsEnabled":
                        sortedClients = sortedClients == null ? GetSavingsProductList.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult CreateNewSavingsProduct()
        {
            ViewBag.LiabilityChartofAccounts = GetLiabilityAccountTypes(Guid.Empty.ToString());

            return View();
        }

        [HttpPost]        
        public ActionResult CreateNewSavingsProduct(SavingsProduct savingproduct)
        {
            ViewBag.LiabilityChartofAccounts = GetLiabilityAccountTypes(Guid.Empty.ToString());

            try
            {                 
                if(ModelState.IsValid)
                {
                    string nextproductcode = savingsproductservice.NextSavingProductCode();
                    savingproduct.ProductCode = nextproductcode;
                    if (savingproduct.ChartofAccountId == Guid.Empty)
                    {
                        ModelState.AddModelError(string.Empty, "Chart of Chart Account cannot be null.");
                        return View(savingproduct);
                    }
                    if (savingproduct.IsDefault == true)
                    {
                        var defaultsavingsproductcount = savingsproductservice.DefaultSavingsProductCount();
                        if (defaultsavingsproductcount.Count() <= 0)
                        {
                            savingsproductservice.AddNewSavingsProduct(savingproduct, serviceHeader);
                            return RedirectToAction("Index");
                        }

                        else
                        {
                            ModelState.AddModelError(string.Empty, "Sorry, there is already a default savings product.");
                            return View(savingproduct);
                        }
                    }
                    else
                    {
                        savingsproductservice.AddNewSavingsProduct(savingproduct, serviceHeader);
                        return RedirectToAction("Index");
                    }    
                }
                return View(savingproduct);
            }                         
            
            catch(Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View(savingproduct);
            }            
        }

        public ActionResult SavingProductDetails(Guid Id)
        {
            var findsavingproductbyid = savingsproductservice.FindSavingsProductById(Id);

            ViewBag.SavingsProductname = findsavingproductbyid.ProductName;

            return View(findsavingproductbyid);
        }

        [HttpGet]
        public ActionResult EditSavingProduct(Guid Id)
        {
            var findsavingsproductbyid = savingsproductservice.FindSavingsProductById(Id);

            ViewBag.LiabilityChartofAccounts = GetLiabilityAccountTypes(Guid.Empty.ToString());

            return View(findsavingsproductbyid);
        }
        
        [HttpPost]
        public ActionResult EditSavingProduct(SavingsProduct savingproduct)
        {
            ViewBag.LiabilityChartofAccounts = GetLiabilityAccountTypes(Guid.Empty.ToString());

            if (ModelState.IsValid)
            {
                try
                {
                    if (savingproduct.ChartofAccountId == Guid.Empty)
                    {
                        ModelState.AddModelError(string.Empty, "Chart of Chart Account cannot be null.");
                        return View(savingproduct);
                    }
                    var defaultsavingsproductcount = savingsproductservice.DefaultSavingsProductCount();
                    var findsavingsproductbyid = savingsproductservice.FindSavingsProductById(savingproduct.Id);
                    if (savingproduct.IsDefault == true)
                    {                        
                        if(defaultsavingsproductcount.Count() == 0)
                        {
                            findsavingsproductbyid.ChartofAccountId = savingproduct.ChartofAccountId;
                            findsavingsproductbyid.ProductName = savingproduct.ProductName;
                            findsavingsproductbyid.IsEnabled = savingproduct.IsEnabled;
                            findsavingsproductbyid.IsDefault = savingproduct.IsDefault;
                            savingsproductservice.UpdateSavingProduct(findsavingsproductbyid, serviceHeader);
                            return RedirectToAction("Index");
                        }

                        else
                        {
                            ModelState.AddModelError(string.Empty, "Sorry, there is already a default savings product. To set this as the default product, unset the default product and then try again.");
                            return View(savingproduct);
                        }                       
                    }

                    else
                    {
                        findsavingsproductbyid.ChartofAccountId = savingproduct.ChartofAccountId;
                        findsavingsproductbyid.ProductName = savingproduct.ProductName;
                        findsavingsproductbyid.IsEnabled = savingproduct.IsEnabled;
                        findsavingsproductbyid.IsDefault = savingproduct.IsDefault;
                        savingsproductservice.UpdateSavingProduct(findsavingsproductbyid, serviceHeader);
                        return RedirectToAction("Index");
                    }
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(savingproduct);
                }                
            }
            return View(savingproduct);
        }


        [NonAction]
        public List<SelectListItem> GetLiabilityAccountTypes(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultaccount = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Chart of Account", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultaccount);

            var otherliabilityaccounts = chartofaccountservice.GetChartOfAccountsByTypeAndCategory((int)Enumerations.ChartOfAccountType.Liability, (int)Enumerations.ChartOfAccountCategory.DetailAccount);

            foreach (var item in otherliabilityaccounts)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.ChartofAccountDescription, Value = item.Id.ToString("D") });

            return SelectList;
        }
    }
}
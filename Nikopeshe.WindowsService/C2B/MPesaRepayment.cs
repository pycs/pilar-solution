﻿using Nikopeshe.Data;
using Nikopeshe.Data.Mappings;
using Nikopeshe.Entity.DataModels;
using Quartz;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Messaging;
using System.Text;

namespace Nikopeshe.WindowsService.C2B
{
    public class MPesaRepayment : IJob
    {        
        string QueueName = ConfigurationManager.AppSettings["C2BQueuePath"].ToString();

        public void Execute(IJobExecutionContext context)
        {
            using (var datacontext = new DataContext())
            {
                try
                {
                    var findapprovedc2btransaction = datacontext.C2BTransaction.Where(s => s.Status == (int)Enumerations.C2BTransactionStatus.Approved).FirstOrDefault();

                    if (findapprovedc2btransaction != null)
                    {

                        var findcustomerusingmobilnumber = datacontext.Customer.Where(x => x.MobileNumber == findapprovedc2btransaction.MSISDN).FirstOrDefault();

                        if (findcustomerusingmobilnumber != null)
                        {
                            var findcustomeraccountbyid = datacontext.CustomersAccount.Where(x => x.CustomerId == findcustomerusingmobilnumber.Id);

                            var findmpesarepaymentaccountid = datacontext.SystemGeneralLedgerAccountMapping.Where(x => x.SystemGeneralLedgerAccountCode == Enumerations.SystemGeneralLedgerAccountCode.MPesaRepaymentAccount).FirstOrDefault();

                            var findcustomeractiveloanbycustomeraccountid = (from a in datacontext.Customer
                                                                             join b in datacontext.CustomersAccount
                                                                                 on a.Id equals b.CustomerId
                                                                             join c in datacontext.LoanRequest on b.Id equals c.CustomerAccountId
                                                                             where c.Status == (int)Enumerations.LoanStatus.Active && a.Id == findcustomerusingmobilnumber.Id
                                                                             select c).FirstOrDefault();

                            if (findcustomeractiveloanbycustomeraccountid != null)
                            {
                                var finddefaultsavingaccountid = datacontext.SavingsProduct.Where(x => x.IsDefault == true).FirstOrDefault();

                                var findcustomersavingsaccountid = datacontext.CustomersAccount.Where(x => x.CustomerId == findcustomerusingmobilnumber.Id && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();

                                var findloanproductbyid = datacontext.LoanProduct.Find(findcustomeractiveloanbycustomeraccountid.LoanProductId);

                                var interestamount = findapprovedc2btransaction.TransAmount * ((findloanproductbyid.InterestValue) / 100) * findloanproductbyid.Term / 12;

                                var principalamount = findapprovedc2btransaction.TransAmount - interestamount;

                                #region Dr M-Pesa Repayment GL, Cr Savings GL
                                LoanTransactionHistory loantransactionhistory = new LoanTransactionHistory();
                                loantransactionhistory.LoanRequestId = findcustomeractiveloanbycustomeraccountid.Id;
                                loantransactionhistory.TransactionType = Enumerations.TransactionType.Debit;
                                loantransactionhistory.RepaymentType = Enumerations.RepaymentType.MPesa;
                                loantransactionhistory.TransactionAmount = principalamount;
                                loantransactionhistory.InterestAmount = interestamount;
                                loantransactionhistory.C2BTransactionId = findapprovedc2btransaction.Id;
                                loantransactionhistory.ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId;
                                loantransactionhistory.ContraAccountId = finddefaultsavingaccountid.ChartofAccountId;
                                loantransactionhistory.CustomersAccountId = findcustomersavingsaccountid.Id;
                                loantransactionhistory.Description = findapprovedc2btransaction.BillRefNumber + " " + findapprovedc2btransaction.MSISDN + " " + findapprovedc2btransaction.TransID;
                                loantransactionhistory.CreatedBy = "System";
                                loantransactionhistory.IsApproved = true;
                                loantransactionhistory.ApprovedBy = "System";
                                loantransactionhistory.TransactionCategory = (int)Enumerations.TransactionCategory.Repayment;
                                #endregion

                                #region Cr Savings GL, Dr M-Pesa Repayment GL
                                LoanTransactionHistory loantransaction = new LoanTransactionHistory();
                                loantransaction.LoanRequestId = findcustomeractiveloanbycustomeraccountid.Id;
                                loantransaction.TransactionType = Enumerations.TransactionType.Credit;
                                loantransaction.RepaymentType = Enumerations.RepaymentType.MPesa;
                                loantransaction.C2BTransactionId = findapprovedc2btransaction.Id;
                                loantransaction.TransactionAmount = principalamount;
                                loantransaction.InterestAmount = interestamount;
                                loantransaction.ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId;
                                loantransaction.ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId;
                                loantransaction.CustomersAccountId = findcustomersavingsaccountid.Id;
                                loantransaction.Description = findapprovedc2btransaction.BillRefNumber + " " + findapprovedc2btransaction.MSISDN + " " + findapprovedc2btransaction.TransID;
                                loantransaction.CreatedBy = "System";
                                loantransaction.IsApproved = true;
                                loantransaction.ApprovedBy = "System";
                                loantransaction.TransactionCategory = (int)Enumerations.TransactionCategory.Repayment;
                                #endregion

                                #region  Debit Savings GL
                                LoanTransactionHistory debitsavingsgl = new LoanTransactionHistory();
                                debitsavingsgl.LoanRequestId = findcustomeractiveloanbycustomeraccountid.Id;
                                debitsavingsgl.TransactionType = Enumerations.TransactionType.Debit;
                                debitsavingsgl.RepaymentType = Enumerations.RepaymentType.MPesa;
                                debitsavingsgl.TransactionAmount = principalamount;
                                debitsavingsgl.C2BTransactionId = findapprovedc2btransaction.Id;
                                debitsavingsgl.InterestAmount = interestamount;
                                debitsavingsgl.ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId;
                                debitsavingsgl.ContraAccountId = Guid.Parse(findloanproductbyid.LoanGLAccountId.ToString());
                                debitsavingsgl.CustomersAccountId = findcustomeractiveloanbycustomeraccountid.CustomerAccountId;
                                debitsavingsgl.Description = findapprovedc2btransaction.BillRefNumber + " " + findapprovedc2btransaction.MSISDN + " " + findapprovedc2btransaction.TransID;
                                debitsavingsgl.CreatedBy = "System";
                                debitsavingsgl.IsApproved = true;
                                debitsavingsgl.ApprovedBy = "System";
                                debitsavingsgl.TransactionCategory = (int)Enumerations.TransactionCategory.Repayment;
                                #endregion

                                #region  Credit Loan GL
                                LoanTransactionHistory creditsavingsgl = new LoanTransactionHistory();
                                creditsavingsgl.LoanRequestId = findcustomeractiveloanbycustomeraccountid.Id;
                                creditsavingsgl.TransactionType = Enumerations.TransactionType.Credit;
                                creditsavingsgl.RepaymentType = Enumerations.RepaymentType.MPesa;
                                creditsavingsgl.TransactionAmount = principalamount;
                                creditsavingsgl.C2BTransactionId = findapprovedc2btransaction.Id;
                                creditsavingsgl.InterestAmount = interestamount;
                                creditsavingsgl.ChartofAccountId = findloanproductbyid.LoanGLAccountId;
                                creditsavingsgl.ContraAccountId = finddefaultsavingaccountid.ChartofAccountId;
                                creditsavingsgl.CustomersAccountId = findcustomeractiveloanbycustomeraccountid.CustomerAccountId;
                                creditsavingsgl.Description = findapprovedc2btransaction.BillRefNumber + " " + findapprovedc2btransaction.MSISDN + " " + findapprovedc2btransaction.TransID;
                                creditsavingsgl.CreatedBy = "System";
                                creditsavingsgl.IsApproved = true;
                                creditsavingsgl.ApprovedBy = "System";
                                creditsavingsgl.TransactionCategory = (int)Enumerations.TransactionCategory.Repayment;
                                #endregion


                                var frequency = findloanproductbyid.PaymentFrequencyType;

                                var findnextpaymentdateindays = 0;

                                if (frequency == Enumerations.PaymentFrequency.Weekly)
                                {
                                    findnextpaymentdateindays = 7;
                                }
                                else
                                {
                                    findnextpaymentdateindays = 28;
                                }
                                findcustomeractiveloanbycustomeraccountid.NextPaymentDate = findcustomeractiveloanbycustomeraccountid.NextPaymentDate.Value.AddDays(findnextpaymentdateindays);                               
                                using (var transaction = datacontext.Database.BeginTransaction())
                                {
                                    try
                                    {
                                        var updateloan = datacontext.Entry(findcustomeractiveloanbycustomeraccountid);
                                        updateloan.State = EntityState.Modified;
                                        datacontext.LoanTransactionHistory.Add(loantransactionhistory);
                                        datacontext.LoanTransactionHistory.Add(loantransaction);
                                        datacontext.LoanTransactionHistory.Add(creditsavingsgl);
                                        datacontext.LoanTransactionHistory.Add(debitsavingsgl);
                                        findapprovedc2btransaction.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                        var entry = datacontext.Entry(findapprovedc2btransaction);
                                        entry.State = EntityState.Modified;
                                        datacontext.SaveChanges();
                                        transaction.Commit();
                                    }
                                    catch(Exception ex)
                                    {
                                        Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
                                        transaction.Rollback();
                                    }
                                    
                                }
                                

                                #region Check if loan has been paid in full
                                var findloantransactionsbyloanrequestid = datacontext.LoanTransactionHistory.Where(x => x.LoanRequestId == findcustomeractiveloanbycustomeraccountid.Id && x.IsApproved == true && x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment
                                    && x.TransactionType == Enumerations.TransactionType.Debit && x.CustomersAccountId == findcustomersavingsaccountid.Id).ToList();
                                var totalpricipalamount = findloantransactionsbyloanrequestid.Sum(x => x.TransactionAmount);
                                var totalinterestamount = findloantransactionsbyloanrequestid.Sum(x => x.InterestAmount);
                                var totalpaidamount = totalinterestamount + totalpricipalamount;
                                var totalbalance = (findcustomeractiveloanbycustomeraccountid.LoanAmount + findcustomeractiveloanbycustomeraccountid.InterestAmount) - totalpaidamount;
                                var totalloanamount = findcustomeractiveloanbycustomeraccountid.LoanAmount + findcustomeractiveloanbycustomeraccountid.InterestAmount;

                                if (totalpaidamount >= totalloanamount && totalbalance <= 0)
                                {
                                    findcustomeractiveloanbycustomeraccountid.Status = (int)Enumerations.LoanStatus.Cleared;
                                    var updateloanstatus = datacontext.Entry(findcustomeractiveloanbycustomeraccountid);
                                    updateloanstatus.State = EntityState.Modified;
                                    datacontext.SaveChanges();
                                }
                                #endregion

                                StringBuilder bodyBuilder = new StringBuilder();
                                StringBuilder textBodyBuilder = new StringBuilder();
                                bodyBuilder.Append(string.Format("Dear {0}", findcustomerusingmobilnumber.FullName));
                                bodyBuilder.AppendLine("<br />");
                                textBodyBuilder.Append(string.Format("Dear {0} ", findcustomerusingmobilnumber.FirstName));
                                string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                                bodyBuilder.AppendLine("<br />");
                                string borrowloanlink = "<a href='" + mvcclientendposint + "'/Loanrequest/newloanrequest> click here</a>";
                                if (totalpaidamount >= totalloanamount && totalbalance <= 0)
                                {
                                    bodyBuilder.Append(string.Format("Your loan repayment of KES{0} has been received and processed successfully.", findapprovedc2btransaction.TransAmount));
                                    bodyBuilder.AppendLine("<br />");
                                    bodyBuilder.AppendLine("<br />");
                                    bodyBuilder.AppendLine(string.Format("Your loan has been cleared. To apply for a new loan {0}", borrowloanlink));
                                    bodyBuilder.AppendLine("<br />");
                                    bodyBuilder.AppendLine("<br />");
                                    textBodyBuilder.Append(string.Format("Your loan repayment of KES{0} has been received and processed successfully.", findapprovedc2btransaction.TransAmount));
                                    textBodyBuilder.AppendLine(string.Format("The loan has been cleared. Go to Nikopeshe and apply for a new loan."));

                                }
                                else
                                {
                                    bodyBuilder.Append(string.Format("Your loan repayment of KES{0} has been received and processed successfully.", findapprovedc2btransaction.TransAmount));
                                    bodyBuilder.AppendLine("<br />");
                                    bodyBuilder.AppendLine("<br />");
                                    bodyBuilder.AppendLine(string.Format("Your outstanding loan balance is KES{0}", totalbalance));
                                    bodyBuilder.AppendLine("<br />");
                                    bodyBuilder.AppendLine("<br />");
                                    bodyBuilder.AppendLine(string.Format(" Next due date is {0}, ", findcustomeractiveloanbycustomeraccountid.NextPaymentDate.Value.ToShortDateString()));
                                    bodyBuilder.AppendLine("<br />");
                                    bodyBuilder.AppendLine("<br />");
                                    textBodyBuilder.Append(string.Format("Your loan repayment of KES{0} has been received and processed successfully.", findapprovedc2btransaction.TransAmount));
                                    textBodyBuilder.AppendLine(string.Format("Balance: KES{0}", totalbalance));
                                    textBodyBuilder.AppendLine(string.Format(" Next due date:{0}", findcustomeractiveloanbycustomeraccountid.NextPaymentDate.Value.ToShortDateString()));

                                }
                                bodyBuilder.AppendLine("Thank you for being our valued client.");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");
                                textBodyBuilder.Append("Thank you.");
                                var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
                                bodyBuilder.Append(emailSignature);

                                var emailAlertDTO = new EmailAlert
                                {
                                    MailMessageFrom = DefaultSettings.Instance.Email,
                                    MailMessageTo = string.Format("{0}", findcustomerusingmobilnumber.EmailAddress),
                                    MailMessageSubject = "Loan Repayment - Nikopeshe",
                                    MailMessageBody = string.Format("{0}", bodyBuilder),
                                    MailMessageIsBodyHtml = true,
                                    MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                    MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                    MailMessageSecurityCritical = false,
                                    CreatedBy = "System",
                                    MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                };
                                datacontext.EmailAlert.Add(emailAlertDTO);
                                datacontext.SaveChanges();

                                var textalert = new TextAlert
                                {
                                    TextMessageRecipient = string.Format("{0}", findcustomerusingmobilnumber.MobileNumber),
                                    TextMessageBody = string.Format("{0}", textBodyBuilder),
                                    MaskedTextMessageBody = "",
                                    TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                    TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                    TextMessageSecurityCritical = false,
                                    CreatedBy = "",
                                    TextMessageSendRetry = 3
                                };
                                datacontext.TextAlert.Add(textalert);
                                datacontext.SaveChanges();
                            }

                            else
                            {
                                #region mark C2B transaction as unknown
                                findapprovedc2btransaction.Status = (int)Enumerations.C2BTransactionStatus.Unknown;
                                var entry = datacontext.Entry(findapprovedc2btransaction);
                                entry.State = EntityState.Modified;
                                datacontext.SaveChanges();
                                #endregion
                            }
                        }
                        else
                        {
                            #region mark C2B transaction as unknown
                            findapprovedc2btransaction.Status = (int)Enumerations.C2BTransactionStatus.Unknown;
                            var entry = datacontext.Entry(findapprovedc2btransaction);
                            entry.State = EntityState.Modified;
                            datacontext.SaveChanges();
                            #endregion
                        }
                    }
                }

                catch (Exception ex)
                {
                    Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
                }
            }            
        }
    }
}

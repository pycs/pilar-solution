﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Entity.Helpers;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class CustomerDocumentService : ICustomerDocumentService
    {
        public string ErrorMessage = string.Empty;

        public IRepository<CustomerDocument> customerdocumentrepository;
        private DataContext datacontext = new DataContext();
        public IRepository<Customer> customerrepository;

        public CustomerDocumentService(IRepository<CustomerDocument> _customerdocumentrepository, IRepository<Customer> _customerrepository)
        {
            if (_customerdocumentrepository == null || _customerrepository == null)
                throw new ArgumentNullException("null object reference");

            this.customerdocumentrepository = _customerdocumentrepository;
            this.customerrepository = _customerrepository;            
        }

        /// <summary>
        /// Add new customer document
        /// </summary>
        /// <param name="customerdocument"></param>
        public void AddNewCustomerDocument(CustomerDocument customerdocument, ServiceHeader serviceHeader)
        {
            if (customerdocument == null)
                throw new ArgumentNullException("null object");

            try
            {
                customerdocumentrepository.Insert(customerdocument, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        /// <summary>
        /// Find Customer document given an Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public CustomerDocument FindCustomerDocumentById(Guid Id)
        {
            var findcustomerdocumentbyid = customerdocumentrepository.GetById(Id);

            return findcustomerdocumentbyid;
        }

        /// <summary>
        /// Find Customer Document given the customer id and document type.
        /// </summary>
        /// <param name="doctype"></param>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public CustomerDocument FindCustomerDocumentByDocumentTypeAndCustomerId(Enumerations.CustomerDocumentTypes doctype, Guid CustomerId)
        {
            var findcustomerdocument = customerdocumentrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.DocumentType == doctype && x.CustomerId == CustomerId);

            return findcustomerdocument.FirstOrDefault();
        }

        public IList<CustomerDocumentView> GetAllCustomerDocuments(Guid CustomerId)
        {
            if (CustomerId == Guid.Empty)
            {
                throw new ArgumentNullException("Customer Id cannot be null");
            }

            var customerdocuemntviewlist = new List<CustomerDocumentView>();
            var c = (from a in datacontext.CustomerDocuments
                     where a.CustomerId == CustomerId
                     select new { a.CreatedDate, a.Customer, a.CustomerId, a.DocumentType, a.Id}).ToList();
            foreach (var document in c)
            {
                customerdocuemntviewlist.Add(new CustomerDocumentView 
                { 
                    Customer = document.Customer,
                    Id = document.Id,
                    CustomerId = document.CustomerId,
                    DocumentType = document.DocumentType,
                    CreatedDate = document.CreatedDate

                });
            }

            return customerdocuemntviewlist;
                        
            //var findcustomerdocuments = customerdocumentrepository.GetAll().Where(x => x.CustomerId == CustomerId);

            //return findcustomerdocuments.ToList();
        }

        public bool VerifyAllCustomerDocumentExistence(Guid customerId)
        {
            if (customerId == Guid.Empty) throw new ArgumentNullException("Customer id cannot be null");

            var getkrapin = customerdocumentrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CustomerId == customerId && x.DocumentType == Enumerations.CustomerDocumentTypes.KRAPIN).FirstOrDefault();
            var getid = customerdocumentrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CustomerId == customerId && x.DocumentType == Enumerations.CustomerDocumentTypes.IDCopy).FirstOrDefault();
            var getpassport = customerdocumentrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CustomerId == customerId && x.DocumentType == Enumerations.CustomerDocumentTypes.PassportPhoto).FirstOrDefault();
            var getsigniture = customerdocumentrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CustomerId == customerId && x.DocumentType == Enumerations.CustomerDocumentTypes.Signature).FirstOrDefault();

            if (getkrapin == null || getid == null || getpassport == null && getsigniture == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    public class CustomerDocumentView
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        public Enumerations.CustomerDocumentTypes DocumentType { get; set; }

        public string DocumentTypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.CustomerDocumentTypes), DocumentType) ? EnumHelper.GetDescription((Enumerations.CustomerDocumentTypes)DocumentType) : string.Empty;
            }
        }

        public DateTime CreatedDate { get; set; }
        
    }   
}

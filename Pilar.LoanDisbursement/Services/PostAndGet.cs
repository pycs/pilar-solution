﻿using Crosscutting.NetFramework.Logging;
using Infrastructure.Crosscutting.NetFramework;
using Newtonsoft.Json;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.Disbursement;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.IO;
using System.Messaging;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace Pilar.LoanDisbursement.Services
{
    [Export(typeof(IPlugin))]
    public class PostAndGet : IPlugin
    {
        private MessageQueue _messageQueue;
        private readonly ILoanProcessing _iloanprocessing;
        private readonly string _queuePath;
        string CompanyId = ConfigurationManager.AppSettings["CompanyId"].ToString();
        string spsusername = ConfigurationManager.AppSettings["SPSUsername"].ToString();
        string spsapiendpoint = ConfigurationManager.AppSettings["SPSAPIEndPoint"].ToString();
        string spspassword = ConfigurationManager.AppSettings["SPSPassword"].ToString();
        string resultcode = ConfigurationManager.AppSettings["resultcode"].ToString();
        string responcecode = ConfigurationManager.AppSettings["responcecode"].ToString();
        private readonly DataContext datacontext = new DataContext();
        private readonly IMessageQueueService _messageQueueService;
        SPSGetResponse spsresponce = new SPSGetResponse();

        [ImportingConstructor]
        public PostAndGet()
        {
            _iloanprocessing = new LoanProcessing();
            _messageQueueService = new MessageQueueService();
            _queuePath = ConfigurationManager.AppSettings["SPSQueuePath"];
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{71ED714C-25CA-4987-8ABF-604572A13FD7}"); }
        }

        public string Description
        {
            get { return "POST GET"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                if (!MessageQueue.Exists(_queuePath))
                    throw new ArgumentException("Queue does not exist at specified path", "_queuePath");
                else
                {
                    // initialize message queue
                    _messageQueue = new MessageQueue(_queuePath);

                    // enable the AppSpecific field in the messages
                    _messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                    // set the formatter to binary.
                    _messageQueue.Formatter = new BinaryMessageFormatter();

                    // set an event to listen for new messages.
                    _messageQueue.ReceiveCompleted += MessageQueue_ReceiveCompleted;
                    
                    // start listening
                    _messageQueue.BeginReceive();
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
        }

        public void Exit()
        {
            _messageQueue.Close();
        }

        #region Event Handlers
        private void MessageQueue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {                
                // retrieve message category
                var messageCategory = (Enumerations.MessageCategory)e.Message.AppSpecific;
                
                #region POST
                if (messageCategory == Enumerations.MessageCategory.SPSPostRequest)
                {
                    var recordId = (Guid)e.Message.Body;
                    var pendingpost = _iloanprocessing.FindLoanRequestById(recordId);

                        if(pendingpost != null)
                        {
                            try
                            {
                                var order = new Order();
                                if (pendingpost.Type == Enumerations.EFTType.AccountToMobile)
                                {
                                    order = new Order()
                                    {
                                        Type = (int)Enumerations.EFTType.AccountToMobile,
                                        CompanyId = Guid.Parse(CompanyId),
                                        Remarks = "Loan Disbursement"
                                    };
                                    order.OrderLines = new List<OrderLine>();
                                    order.OrderLines.Add(new OrderLine
                                    {
                                        Payee = pendingpost.CustomerAccount.Customer.FullName,
                                        PrimaryAccountNumber = pendingpost.PrimaryAccountNumber,
                                        Amount = pendingpost.DisbursedAmount,
                                        MCCMNC = (int)Enumerations.MobileNetworkCode.KE_Safaricom,
                                        Reference = pendingpost.LoanReferenceNumber,
                                        SystemTraceAuditNumber = string.Concat(pendingpost.SystemTraceAuditNumber)
                                    });
                                }

                                else if (pendingpost.Type == Enumerations.EFTType.BusinessToBusiness)
                                {
                                    order = new Order()
                                    {
                                        Type = (int)Enumerations.EFTType.BusinessToBusiness,
                                        CompanyId = Guid.Parse(CompanyId),
                                        Remarks = "Loan Disbursement"
                                    };

                                    order.OrderLines = new List<OrderLine>();

                                    order.OrderLines.Add(new OrderLine
                                    {
                                        Payee = pendingpost.CustomerAccount.Customer.FullName,
                                        Type = (int)Enumerations.B2BTransactionType.PayBill,
                                        PrimaryAccountNumber = pendingpost.BankCode,
                                        Amount = pendingpost.DisbursedAmount,                                        
                                        Reference = pendingpost.PrimaryAccountNumber,
                                        SystemTraceAuditNumber = string.Concat(pendingpost.SystemTraceAuditNumber)
                                    });
                                }

                                string jsonString = JsonConvert.SerializeObject(order);
                                var jsonData = Encoding.UTF8.GetBytes(jsonString);
                                HttpWebRequest webRequest = WebRequest.Create(spsapiendpoint) as HttpWebRequest;
                                if (webRequest == null)
                                    throw new NullReferenceException("the request is not a web request");
                                webRequest.Method = "POST";
                                webRequest.ContentType = "application/json";
                                webRequest.ContentLength = jsonData.Length;

                                webRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(spsusername + ":" + spspassword)));

                                using (Stream requestStream = webRequest.GetRequestStream())
                                {
                                    requestStream.Write(jsonData, 0, jsonData.Length);
                                    requestStream.Close();
                                }

                                HttpWebResponse webReponse = webRequest.GetResponse() as HttpWebResponse;
                                if (webReponse.StatusCode == HttpStatusCode.OK)
                                    using (var receiveStream = webReponse.GetResponseStream())
                                    {
                                        using (var readstream = new StreamReader(receiveStream, new UTF8Encoding()))
                                        {
                                            pendingpost.ThirdPartyTransactionId = readstream.ReadToEnd();
                                            pendingpost.HttpPostResponseCode = (int)webReponse.StatusCode;
                                            pendingpost.ProcessingStatus = (int)Enumerations.ProcessingStatus.Submitted;
                                            _iloanprocessing.UpdateLoanRequest(pendingpost);
                                        }
                                    }
                            }

                            catch(WebException ex)
                            {
                                var response = ex.Response as HttpWebResponse;
                                pendingpost.HttpPostResponseCode = (int)response.StatusCode;
                                pendingpost.ProcessingStatus = (int)Enumerations.ProcessingStatus.Submitted;
                                _iloanprocessing.UpdateLoanRequest(pendingpost);
                                LoggerFactory.CreateLog().LogInfo(ex.Message + ex.InnerException + ex.StackTrace);
                            }
                        }
                }
                #endregion

                #region GET
                
                else if (messageCategory == Enumerations.MessageCategory.SPSGetRequest)
                {
                    var recordId = (Guid)e.Message.Body;

                    var pendingpost = _iloanprocessing.FindLoanRequestById(recordId);

                    if (pendingpost.ThirdPartyTransactionId != null)
                    {
                        try
                        {
                            HttpWebRequest webRequest = WebRequest.Create(spsapiendpoint + pendingpost.ThirdPartyTransactionId) as HttpWebRequest;

                            if (webRequest == null)
                                throw new NullReferenceException("request is not a http request");

                            webRequest.Method = "GET";
                            webRequest.ContentType = "application/json";

                            webRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(spsusername + ":" + spspassword)));
                            HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;

                            if (webResponse.StatusCode == HttpStatusCode.OK)
                            {
                                using (var receiveStream = webResponse.GetResponseStream())
                                {
                                    using (var readStream = new StreamReader(receiveStream, new UTF8Encoding()))
                                    {
                                        var jsonResponse = readStream.ReadToEnd();

                                        var serializer = new JavaScriptSerializer();

                                        var order = serializer.Deserialize<Order>(jsonResponse);

                                        var findloanrequestbyid = datacontext.LoanRequest.Find(pendingpost.Id);
                                        //var entry = datacontext.Entry(findloanrequestbyid);
                                        //entry.State = EntityState.Modified;
                                        List<OrderLine> orderline = order.OrderLines;
                                        findloanrequestbyid.B2MResponseCode = orderline[0].B2MResponseCode;
                                        findloanrequestbyid.B2MResponseDesc = orderline[0].B2MResponseDesc;
                                        findloanrequestbyid.B2MResultCode = orderline[0].B2MResultCode;
                                        findloanrequestbyid.B2MResultDesc = orderline[0].B2MResultDesc;
                                        findloanrequestbyid.B2MTransactionID = orderline[0].B2MTransactionID;
                                        findloanrequestbyid.ChargePaidAccountAvailableFunds = orderline[0].ChargePaidAccountAvailableFunds;
                                        findloanrequestbyid.IsDelivered = order.IsDelivered;
                                        findloanrequestbyid.TypeDesc = orderline[0].TypeDesc;
                                        findloanrequestbyid.MCCMNCDesc = orderline[0].MCCMNCDesc;
                                        findloanrequestbyid.TransactionCreditParty = orderline[0].TransactionCreditParty;
                                        findloanrequestbyid.TransactionDateTime = orderline[0].TransactionDateTime;
                                        findloanrequestbyid.SPSStatus = orderline[0].Status;
                                        findloanrequestbyid.SPSStatusDesc = orderline[0].StatusDesc;
                                        findloanrequestbyid.UtilityAccountAvailableFunds = orderline[0].UtilityAccountAvailableFunds;
                                        findloanrequestbyid.WorkingAccountAvailableFunds = orderline[0].WorkingAccountAvailableFunds;
                                        findloanrequestbyid.HttpGetResponseCode = (int)webResponse.StatusCode;
                                        _messageQueueService.Send(_queuePath, findloanrequestbyid, Enumerations.MessageCategory.SPSGetResponse, MessagePriority.Normal);
                                        //datacontext.SaveChanges();
                                    }
                                }
                            }
                        }

                        catch(WebException ex)
                        {
                            //var responsecode = ex.Response as HttpWebResponse;
                            //pendingpost.HttpGetResponseCode = (int)responsecode.StatusCode;
                            //pendingpost.ProcessingStatus = (int)Enumerations.ProcessingStatus.Submitted;
                            //_iloanprocessing.UpdateLoanRequest(pendingpost);
                            LoggerFactory.CreateLog().LogInfo(ex.Message + ex.InnerException + ex.StackTrace);
                        }
                    }
                }
                #endregion

                #region SPS GET Response

                else if (messageCategory == Enumerations.MessageCategory.SPSGetResponse)
                {
                    try
                    {
                        var getloanrequest = (LoanRequest)e.Message.Body;
                        var findloanrequestbyid = _iloanprocessing.FindLoanRequestById(getloanrequest.Id);
                        findloanrequestbyid.B2MResponseCode = getloanrequest.B2MResponseCode;
                        findloanrequestbyid.B2MResponseDesc = getloanrequest.B2MResponseDesc;
                        findloanrequestbyid.B2MResultCode = getloanrequest.B2MResultCode;
                        findloanrequestbyid.B2MResultDesc = getloanrequest.B2MResultDesc;
                        findloanrequestbyid.B2MTransactionID = getloanrequest.B2MTransactionID;
                        findloanrequestbyid.ChargePaidAccountAvailableFunds = getloanrequest.ChargePaidAccountAvailableFunds;
                        findloanrequestbyid.IsDelivered = getloanrequest.IsDelivered;
                        findloanrequestbyid.TypeDesc = getloanrequest.TypeDesc;
                        findloanrequestbyid.MCCMNCDesc = getloanrequest.MCCMNCDesc;
                        findloanrequestbyid.TransactionCreditParty = getloanrequest.TransactionCreditParty;
                        findloanrequestbyid.TransactionDateTime = getloanrequest.TransactionDateTime;
                        findloanrequestbyid.SPSStatus = getloanrequest.SPSStatus;
                        findloanrequestbyid.SPSStatusDesc = getloanrequest.SPSStatusDesc;
                        findloanrequestbyid.HttpGetResponseCode = getloanrequest.HttpGetResponseCode;
                        if (findloanrequestbyid.B2MResponseCode == responcecode && findloanrequestbyid.B2MResultCode == resultcode)
                        {
                            findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Active;
                            findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Completed;
                        }
                        else if (getloanrequest.Status == (int)Enumerations.EFTStatus.Completed && (findloanrequestbyid.B2MResponseCode != responcecode || findloanrequestbyid.B2MResultCode != resultcode))
                        {
                            findloanrequestbyid.Status = (int)Enumerations.LoanStatus.FailedAtDisbursing;
                            findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Failed;
                        }
                        _iloanprocessing.UpdateLoanRequest(findloanrequestbyid);
                    }

                    catch (Exception ex)
                    {
                        LoggerFactory.CreateLog().LogInfo(ex.Message + ex.InnerException + ex.StackTrace);
                    }
                }
                #endregion
                
                #region Loan Request Transaction Reversal

                else if (messageCategory == Enumerations.MessageCategory.LoanTransactionsReversal)
                {
                    var recordId = (Guid)e.Message.Body;

                    var reversal = _iloanprocessing.FindLoanRequestById(recordId);

                    if (reversal != null)
                    {
                        using (var dataContext = new DataContext())
                        {                            
                            using (var transaction = datacontext.Database.BeginTransaction())
                            {
                                try
                                {                                    
                                    var findLoanTransactionHistoryByLoanRequestid = _iloanprocessing.FindLoanTransactionHistoryByLoanRequestId(reversal.Id);

                                    if (findLoanTransactionHistoryByLoanRequestid != null && findLoanTransactionHistoryByLoanRequestid.Count != 0)
                                    {
                                        int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                        foreach (var loanTransaction in findLoanTransactionHistoryByLoanRequestid)
                                        {
                                            if (loanTransaction.TransactionType == Enumerations.TransactionType.Credit)
                                            {
                                                LoanTransactionHistory debitCustomerSavings = new LoanTransactionHistory()
                                                {
                                                    LoanRequestId = reversal.Id,
                                                    ChartofAccountId = loanTransaction.ChartofAccountId,
                                                    ContraAccountId = loanTransaction.ContraAccountId,
                                                    TransactionType = Enumerations.TransactionType.Debit,
                                                    CustomersAccountId = loanTransaction.CustomersAccountId,
                                                    Description = "Reversal Loan Approval",
                                                    BranchId = loanTransaction.BranchId,
                                                    IsApproved = true,
                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Reversal,
                                                    CreatedBy = "",
                                                    TransactionAmount = loanTransaction.TransactionAmount,
                                                    TransactionBatchNumber = batchNo
                                                };

                                                datacontext.LoanTransactionHistory.Add(debitCustomerSavings);
                                            }

                                            else
                                            {
                                                LoanTransactionHistory creditLoanAccount = new LoanTransactionHistory()
                                                {
                                                    LoanRequestId = reversal.Id,
                                                    ChartofAccountId = loanTransaction.ChartofAccountId,
                                                    ContraAccountId = loanTransaction.ContraAccountId,
                                                    TransactionType = Enumerations.TransactionType.Credit,
                                                    CustomersAccountId = loanTransaction.CustomersAccountId,
                                                    Description = "Reversal Loan Approval",
                                                    BranchId = loanTransaction.BranchId,
                                                    CreatedBy = "System",
                                                    IsApproved = true,
                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Reversal,
                                                    TransactionAmount = loanTransaction.TransactionAmount,
                                                    TransactionBatchNumber = batchNo
                                                };

                                                datacontext.LoanTransactionHistory.Add(creditLoanAccount);
                                            }
                                        }

                                        reversal.Status = (int)Enumerations.LoanStatus.TransactionReversed;
                                        _iloanprocessing.UpdateLoanRequest(reversal);
                                    }

                                    datacontext.SaveChanges();
                                    transaction.Commit();
                                }

                                catch(Exception ex)
                                {
                                    transaction.Rollback();
                                    LoggerFactory.CreateLog().LogInfo("{0}->Process->ReceiveCompleted FAILED...ReceiveCompletedEventArgs={1}" + ex + Description + e);
                                }
                            }
                        }
                    }
                }

                else if (messageCategory == Enumerations.MessageCategory.CompleteLoanTransactionsReveral)
                {
                
                    var recordId = (Guid)e.Message.Body;

                    var reversal = _iloanprocessing.FindLoanRequestById(recordId);

                    if (reversal != null)
                    {
                        using (var dataContext = new DataContext())
                        {
                            using (var transaction = datacontext.Database.BeginTransaction())
                            {
                                try
                                {
                                    var findLoanRequestTransactions = _iloanprocessing.GetAllLoanRequestTransactionsByLoanRequestId(reversal.Id);

                                    if (findLoanRequestTransactions != null)
                                    {
                                        foreach (var trx in findLoanRequestTransactions)
                                        {
                                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();

                                            if (trx.TransactionCategory == (int)Enumerations.TransactionCategory.Approval)
                                            {
                                                if (trx.TransactionType == Enumerations.TransactionType.Credit)
                                                {
                                                    LoanTransactionHistory debitCustomerSavings = new LoanTransactionHistory()
                                                    {
                                                        LoanRequestId = reversal.Id,
                                                        ChartofAccountId = trx.ChartofAccountId,
                                                        ContraAccountId = trx.ContraAccountId,
                                                        TransactionType = Enumerations.TransactionType.Debit,
                                                        CustomersAccountId = trx.CustomersAccountId,
                                                        Description = "Loan Approval Reversal",
                                                        BranchId = trx.BranchId,
                                                        IsApproved = true,
                                                        TransactionCategory = (int)Enumerations.TransactionCategory.Reversal,
                                                        CreatedBy = "",
                                                        TransactionAmount = trx.TransactionAmount,
                                                        TransactionBatchNumber = batchNo
                                                    };

                                                    datacontext.LoanTransactionHistory.Add(debitCustomerSavings);
                                                }

                                                else
                                                {
                                                    LoanTransactionHistory creditLoanAccount = new LoanTransactionHistory()
                                                    {
                                                        LoanRequestId = reversal.Id,
                                                        ChartofAccountId = trx.ChartofAccountId,
                                                        ContraAccountId = trx.ContraAccountId,
                                                        TransactionType = Enumerations.TransactionType.Credit,
                                                        CustomersAccountId = trx.CustomersAccountId,
                                                        Description = "Loan Approval Reversal",
                                                        BranchId = trx.BranchId,
                                                        CreatedBy = "System",
                                                        IsApproved = true,
                                                        TransactionCategory = (int)Enumerations.TransactionCategory.Reversal,
                                                        TransactionAmount = trx.TransactionAmount,
                                                        TransactionBatchNumber = batchNo
                                                    };

                                                    datacontext.LoanTransactionHistory.Add(creditLoanAccount);
                                                }
                                            }
                                            else if (trx.TransactionCategory == (int)Enumerations.TransactionCategory.Disbursement)
                                            {
                                                int batchNo1 = RandomBatchNumberGenerator.GetRandomNumber();

                                                if (trx.TransactionType == Enumerations.TransactionType.Debit)
                                                {
                                                    LoanTransactionHistory debitmpesaglaccount = new LoanTransactionHistory()
                                                    {
                                                        LoanRequestId = trx.LoanRequestId,
                                                        ChartofAccountId = trx.ChartofAccountId,
                                                        ContraAccountId = trx.ContraAccountId,
                                                        TransactionType = Enumerations.TransactionType.Credit,
                                                        CustomersAccountId = trx.CustomersAccountId,
                                                        Description = "Loan Disbursement Reversal",
                                                        TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement,
                                                        IsApproved = true,
                                                        TransactionAmount = trx.TransactionAmount,
                                                        BranchId = trx.BranchId,
                                                        TransactionBatchNumber = batchNo1
                                                    };

                                                    datacontext.LoanTransactionHistory.Add(debitmpesaglaccount);
                                                }

                                                else
                                                {
                                                    LoanTransactionHistory creditsavingsaccount = new LoanTransactionHistory()
                                                    {
                                                        LoanRequestId = trx.LoanRequestId,
                                                        ChartofAccountId = trx.ChartofAccountId,
                                                        ContraAccountId = trx.ContraAccountId,
                                                        TransactionType = Enumerations.TransactionType.Debit,
                                                        CustomersAccountId = trx.CustomersAccountId,
                                                        Description = "Loan Disbursement Reversal",
                                                        IsApproved = true,
                                                        TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement,
                                                        TransactionAmount = trx.TransactionAmount,
                                                        BranchId = trx.BranchId,
                                                        TransactionBatchNumber = batchNo1
                                                    };

                                                    datacontext.LoanTransactionHistory.Add(creditsavingsaccount);
                                                }
                                            }
                                        }
                                    }

                                    reversal.Status = (int)Enumerations.LoanStatus.CompleteReversal;
                                    _iloanprocessing.UpdateLoanRequest(reversal);
                                    datacontext.SaveChanges();
                                    transaction.Commit();
                                }

                                catch (Exception ex)
                                {
                                    transaction.Rollback();
                                    LoggerFactory.CreateLog().LogInfo("{0}->Process->ReceiveCompleted FAILED...ReceiveCompletedEventArgs={1}" + ex + Description + e);
                                }
                            }
                        }
                    }
                }

                #endregion

                else
                {
                    LoggerFactory.CreateLog().LogInfo("Unknown message category");
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->Process->ReceiveCompleted FAILED...ReceiveCompletedEventArgs={1}" + ex + Description + e);
            }
            finally
            {
                // listen for the next message.
                _messageQueue.BeginReceive();
            }
        }
        #endregion
        #endregion
    }

    class SPSGetResponse
    {
        public Guid Id { get; set; }

        public int MCCMNC { get; set; }

        public string MCCMNCDesc { get; set; }

        public int Status { get; set; }

        public string StatusDesc { get; set; }

        public string B2MResponseCode { get; set; }

        public string B2MResponseDesc { get; set; }

        public string B2MResultCode { get; set; }

        public string B2MResultDesc { get; set; }

        public string B2MTransactionID { get; set; }

        public string TransactionDateTime { get; set; }

        public decimal WorkingAccountAvailableFunds { get; set; }

        public decimal UtilityAccountAvailableFunds { get; set; }

        public decimal ChargePaidAccountAvailableFunds { get; set; }

        public string TransactionCreditParty { get; set; }
    }
}

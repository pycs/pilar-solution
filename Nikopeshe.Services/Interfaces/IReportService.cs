﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface IReportService
    {
        void AddNewReport(Report report, ServiceHeader serviceHeader);

        void UpdateReport(Report report, ServiceHeader serviceHeader);

        IList<Report> GetAllReports();

        IList<Report> GetAllReportsPerRole(int RoleId);

        //IList<Report> GetAllReportsByRoleName(string roleName);

        IList<Report> GetAllReportsByRole(int RoleCategory);

        void DeleteReport(Report report, ServiceHeader serviceHeader);

        Report FindReportById(Guid Id);
    }
}

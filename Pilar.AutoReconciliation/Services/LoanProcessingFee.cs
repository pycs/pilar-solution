﻿using Pilar.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Quartz;
using Quartz.Impl;
using Pilar.AutoReconciliation.Configuration;
using System.Configuration;
using Crosscutting.NetFramework.Logging;

namespace Pilar.AutoReconciliation.Services
{
    [Export(typeof(IPlugin))]
    public class LoanProcessingFee : IPlugin
    {
        private readonly IScheduler _scheduler;

        public LoanProcessingFee()
        {
            // Get a reference to the scheduler
            var sf = new StdSchedulerFactory();

            // Get an instance of the Quartz.Net scheduler
            _scheduler = sf.GetScheduler();
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{2F962F9B-2460-4958-8B82-AE127EDDF3B0}"); }
        }

        public string Description
        {
            get { return "LOAN PROCESSING QEUEING JOB"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                // Start the scheduler if its in standby
                if (!_scheduler.IsStarted)
                    _scheduler.Start();

                // Define the Job to be scheduled
                var job = JobBuilder.Create<LoanProcessingFeeQueueingJob>()
                    .WithIdentity("LoanProcessingFeeQueueingJob", "Pilar")
                    .RequestRecovery()
                    .Build();

                // Associate a trigger with the Job
                var cronExpression = ConfigurationManager.AppSettings["LoanProcessingFeeQueueingJobCronExpression"];
                var trigger = (ICronTrigger)TriggerBuilder.Create()
                    .WithIdentity("LoanProcessingFeeQueueingJob", "Pilar")
                    .WithCronSchedule(cronExpression)
                    .StartAt(DateTime.UtcNow)
                    .WithPriority(1)
                    .Build();

                // Validate that the job doesn't already exist
                if (_scheduler.CheckExists(new JobKey("LoanProcessingFeeQueueingJob", "Pilar")))
                {
                    _scheduler.DeleteJob(new JobKey("LoanProcessingFeeQueueingJob", "Pilar"));
                }

                var schedule = _scheduler.ScheduleJob(job, trigger);

#if DEBUG
                //ExceptionLoggerFactory.CreateLog("{0}->DoWork..." +  Description);
#endif
            }
            catch (Exception ex)
            {
                //ExceptionLoggerFactory.CreateLog("{0}->DoWork..." + ex + Description);
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        public void Exit()
        {
            _scheduler.Shutdown();
        }

        #endregion
    }
}

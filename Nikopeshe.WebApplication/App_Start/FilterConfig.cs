﻿using Nikopeshe.WebApplication.Attributes;
using System.Web;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new CustomerApproveAccountAtribute());
            filters.Add(new ForcePasswordChangeAttribute());
            //filters.Add(new LockSystemAttribute());
            //filters.Add(new AuthorizeIPAddressAttribute());
        }
    }
}

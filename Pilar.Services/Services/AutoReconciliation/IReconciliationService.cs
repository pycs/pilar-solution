﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.AutoReconciliation
{
    public interface IReconciliationService
    {
        IList<Reconciliation> FindUnProcessedFilesByStatus(int Status, int Type);

        void UpdateFile(Guid Id, int Status);
    }
}

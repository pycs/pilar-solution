﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    [Serializable]
    public class CustomersAccount : BaseEntity
    {
        [Index("CustomerId")]
        [Display(Name = "Customer Name")]
        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; }
       
        [Display(Name = "Account Type")]
        [Index]
        public int AccountType { get; set; }

        [Display(Name = "Account Number")]
        [Index("AccountNumber", IsUnique = true)]
        [Column(TypeName = "VARCHAR")]
        [MaxLength(100)]
        public string AccountNumber { get; set; }

        [Index]
        public bool IsActive { get; set; }
    }
}

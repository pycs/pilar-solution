﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.WindowsService.LoanProcessing
{
    public class LoanStatusWatchdog : IJob
    {
        private DataContext datacontext = new DataContext();
        string resultcode = ConfigurationManager.AppSettings["resultcode"].ToString();
        string responcecode = ConfigurationManager.AppSettings["responcecode"].ToString();
        public void Execute(IJobExecutionContext context)
        {
            List<LoanRequest> markcompleteoanrequestactive = datacontext.LoanRequest.Where(x => x.B2MResponseCode == responcecode && x.B2MResultCode == resultcode && x.SPSStatus == (int)Enumerations.EFTStatus.Completed && x.Status != (int)Enumerations.LoanStatus.Cleared && x.Status != (int)Enumerations.LoanStatus.Active).Take(10).ToList();
            if(markcompleteoanrequestactive != null && markcompleteoanrequestactive.Any())
            {
                foreach(var loanrequest in markcompleteoanrequestactive)
                {
                    var findloanrequestdetails = datacontext.LoanRequest.Find(loanrequest.Id);
                    findloanrequestdetails.Status = (int)Enumerations.LoanStatus.Active;
                    var entry = datacontext.Entry(findloanrequestdetails);
                    entry.State = System.Data.Entity.EntityState.Modified;
                    datacontext.SaveChanges();
                }
            }
        }
    }
}

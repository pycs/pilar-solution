﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    public class SystemGeneralLedgerAccountMapping : BaseEntity
    {
        [Display(Name = "System General Ledger Account Code")]
        [Required]
        public Enumerations.SystemGeneralLedgerAccountCode SystemGeneralLedgerAccountCode { get; set; }

        [Display(Name = "System General Ledger Account")]
        public string SystemGeneralLedgerAccountDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.SystemGeneralLedgerAccountCode), SystemGeneralLedgerAccountCode) ? EnumHelper.GetDescription((Enumerations.SystemGeneralLedgerAccountCode)SystemGeneralLedgerAccountCode) : string.Empty;
            }
        }

        [Display(Name = "Chart of Accounts")]
        [Required]
        [Index]
        public Guid ChartofAccountId { get; set; }

        public virtual ChartofAccount ChartofAccount { get; set; }       

        [Display(Name="Is Enabled?")]
        [Index]
        public bool IsEnabled { get; set; }

        [NotMapped]
        public string Commands { get; set; }
    }
}

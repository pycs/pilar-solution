﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Data.Entity.Validation;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class PasswordRecoveryRequestService : IPasswordRecoveryRequestService
    {
        readonly IRepository<PasswordRecoveryRequest> passwordrecoveryrepository;

        string errorMessage = string.Empty;

        public PasswordRecoveryRequestService(IRepository<PasswordRecoveryRequest> _passwordrecoveryrepository)
        {
            this.passwordrecoveryrepository = _passwordrecoveryrepository;
        }

        public void AddNewPasswordRecoveryRequest(PasswordRecoveryRequest passwordrecoveryrequest, ServiceHeader serviceHeader)
        {
            if (passwordrecoveryrequest == null)
                throw new ArgumentNullException("null entity");

            try
            {
                passwordrecoveryrepository.Insert(passwordrecoveryrequest, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationerror in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} ErroMessage: {1}", validationerror.PropertyName, validationerror.ErrorMessage);
                    }
                }
                throw new Exception(errorMessage, ex);
            }
        }

        public PasswordRecoveryRequest GetPasswordRecoveryRequestByVerificationId(Guid Id)
        {
            return passwordrecoveryrepository.GetById(Id);
        }

        public void UpdatePasswordRecoveryRequest(PasswordRecoveryRequest passwordrecoveryrequest, ServiceHeader serviceHeader)
        {
            if(passwordrecoveryrequest == null)
                throw new ArgumentNullException("null entity");

            try
            {
                passwordrecoveryrepository.Update(passwordrecoveryrequest, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationerror in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} ErroMessage: {1}", validationerror.PropertyName, validationerror.ErrorMessage);
                    }
                }
                throw new Exception(errorMessage, ex);
            }
        }
    }
}

﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class LoanTransactionHistoryController : Controller
    {
        internal readonly ILoanTransactionHistoryService loantransactionhistory;
        internal readonly ILoanRequestService loanrequestservice;
        internal readonly ICustomerService customerService;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public LoanTransactionHistoryController(ILoanTransactionHistoryService _loantransctionHistory, ICustomerService _customerservice, ILoanRequestService _loanrequestservice)
        {
            if(_loanrequestservice == null || _loantransctionHistory == null || _customerservice == null)
                throw new ArgumentNullException("null service reference");

            this.loantransactionhistory = _loantransctionHistory;
            this.loanrequestservice = _loanrequestservice;
            this.customerService = _customerservice;
        }
        //
        // GET: /LoanTransactionHistory/
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getalltransactionhsitory(JQueryDataTablesModel datatablemodel)
        {
            int totalrecordcount = 0;

            int searchedrecordcount = 0;

            var sortAscending = datatablemodel.sSortDir_.First() == "asc" ? true : false;

            var sortedcolumns = (from s in datatablemodel.GetSortedColumns() select s.PropertyName).ToList();

            var pagecollection = loantransactionhistory.GetAllLoanTransactionsHistory();

            if(pagecollection != null)
            {
                totalrecordcount = pagecollection.Count();
                searchedrecordcount = !string.IsNullOrWhiteSpace(datatablemodel.sSearch) ? pagecollection.Count() : totalrecordcount;
            }

            var result = Extensions.ControllerExtensions.DataTablesJson(this, items: pagecollection, totalRecords: totalrecordcount, totalDisplayRecords: searchedrecordcount, sEcho: datatablemodel.sEcho);

            return result;
        }
        

        [HttpGet]
        public ActionResult AddNewLoanTransactionHistory()
        {
            var loanrequestlist = GetAllLoanRequest(Guid.Empty.ToString());
            ViewBag.LoanRequestList = loanrequestlist;

            return View();
        }

        [HttpPost]        
        public ActionResult AddNewLoanTransactionHistory(LoanTransactionHistory loanhistory)
        {
            var loanrequestlist = GetAllLoanRequest(Guid.Empty.ToString());
            ViewBag.LoanRequestList = loanrequestlist;

            if(ModelState.IsValid)
            {
                try
                {
                    loantransactionhistory.AddNewLoanHistory(loanhistory, serviceHeader);

                    return RedirectToAction("Index");
                }

                catch(Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(loanhistory);
                }
            }
            return View(loanhistory);
        }

        [Authorize]
        public ActionResult loanhistory()
        {             
            return View();
        }

        public JsonResult loanhistorybycustomer(JQueryDataTablesModel datatablemodel)
        {            
            var getcustomerdetailsbymobilenumber = customerService.FindCustomerByMobileNumber(User.Identity.Name);
            
            var pagecollection = loanrequestservice.GetAllCustomerLoanRequests(getcustomerdetailsbymobilenumber.Id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetCustomerLoanTransactionsHistory(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, LoanRequestList: pagecollection);


            return Json(new JQueryDataTablesResponse<LoanRequest>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<LoanRequest> GetCustomerLoanTransactionsHistory(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanRequest> LoanRequestList)
        {
            var GetLoanTransactionsHistoryList = LoanRequestList;

            totalRecordCount = GetLoanTransactionsHistoryList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetLoanTransactionsHistoryList = GetLoanTransactionsHistoryList.Where(c => c.LoanProduct.ProductName
                    .ToLower().Contains(searchString.ToLower()) || c.LoanReferenceNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetLoanTransactionsHistoryList.Count;

            IOrderedEnumerable<LoanRequest> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetLoanTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "LoanProduct.ProductName":
                        sortedClients = sortedClients == null ? GetLoanTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName);
                        break;

                    case "LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetLoanTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber);
                        break;

                    case "LoanAmount":
                        sortedClients = sortedClients == null ? GetLoanTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount);
                        break;

                    case "InterestAmount":
                        sortedClients = sortedClients == null ? GetLoanTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount);
                        break;

                    case "StatusDesc":
                        sortedClients = sortedClients == null ? GetLoanTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc);
                        break;

                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }   

        [Authorize]
        public ActionResult customerloantransactionhistory(Guid Id)
        {
            ViewBag.Id = Id;

            return View();
        }

        public JsonResult getcustomerloantransactionhistory(Guid Id, JQueryDataTablesModel datatablemodel)
        {                        
            var pagecollection = loantransactionhistory.GetAllCustomerTransationHistory(Id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetAllCustomerLoanTransactionsHistory(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, LoanTransactionsHistoryList: pagecollection);


            return Json(new JQueryDataTablesResponse<LoanTransactionHistory>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<LoanTransactionHistory> GetAllCustomerLoanTransactionsHistory(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanTransactionHistory> LoanTransactionsHistoryList)
        {
            var GetLoanTransactionsHistoryList = LoanTransactionsHistoryList;

            totalRecordCount = GetLoanTransactionsHistoryList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetLoanTransactionsHistoryList = GetLoanTransactionsHistoryList.Where(c => c.LoanRequest.CustomerAccount.Customer.FullName
                    .ToLower().Contains(searchString.ToLower()) || c.LoanRequest.LoanReferenceNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetLoanTransactionsHistoryList.Count;

            IOrderedEnumerable<LoanTransactionHistory> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetLoanTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "CustomerAccount.Customer.FullName":
                        sortedClients = sortedClients == null ? GetLoanTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.CustomerAccount.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.CustomerAccount.Customer.FullName);
                        break;

                    case "TransactionTypeDesc":
                        sortedClients = sortedClients == null ? GetLoanTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.TransactionTypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransactionTypeDesc);
                        break;

                    case "LoanRequest.LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetLoanTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.LoanReferenceNumber);
                        break;

                    case "TransactionAmount":
                        sortedClients = sortedClients == null ? GetLoanTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.TransactionAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransactionAmount);
                        break;

                    case "Description":
                        sortedClients = sortedClients == null ? GetLoanTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.Description)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Description);
                        break;

                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [NonAction]
        public List<SelectListItem> GetAllLoanRequest(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultItem = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Loan Request...", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultItem);

            var otherItems = loanrequestservice.GetAllLoanRequests();

            foreach (var item in otherItems)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.CustomerLoanRequest, Value = item.Id.ToString("D") });

            return SelectList;
        }
	}
}
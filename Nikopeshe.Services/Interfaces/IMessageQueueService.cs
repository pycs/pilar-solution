﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface IMessageQueueService
    {
        string Send(string queuePath, object data, Enumerations.MessageCategory messageCategory, MessagePriority priority);
    }
}

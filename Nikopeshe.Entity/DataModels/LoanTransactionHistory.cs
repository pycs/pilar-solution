﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{
    public class LoanTransactionHistory : BaseEntity
    {
        [Display(Name = "Loan Request")]
        [Index("ID_LoanRequestId")]
        public Guid? LoanRequestId { get; set; }

        public virtual LoanRequest LoanRequest { get; set; }

        [Index]
        public Guid? BranchId { get; set; }

        public virtual Branch Branch { get; set; }

        /// <summary>
        /// Debit or Credit
        /// </summary>
        [Index("TransactionType")]
        [Index]
        [Display(Name = "Transaction Type")]
        public Enumerations.TransactionType TransactionType { get; set; }

        public string TransactionTypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.TransactionType), TransactionType) ? EnumHelper.GetDescription((Enumerations.TransactionType)TransactionType) : string.Empty;
            }
        }

        public Enumerations.RepaymentType RepaymentType { get; set; }

        public string RepaymentTypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.RepaymentType), RepaymentType) ? EnumHelper.GetDescription((Enumerations.RepaymentType)RepaymentType) : string.Empty;
            }
        }
        /// <summary>
        /// Principle amount
        /// </summary>
        ///
        [Display(Name = "Transaction Amount")]
        [Required]
        [Index]
        public Decimal TransactionAmount { get; set; }
     
        [Display(Name = "Loan GL Account")]
        [Index("LoanGLAccount")]
        public Guid? ChartofAccountId { get; set; }

        public virtual ChartofAccount ChartofAccount { get; set; }

        [Display(Name = "Contra Account")]
        [Index]
        public Guid ContraAccountId { get; set; }

        public virtual ChartofAccount ContraAccountChartofAccount { get; set; }

        [Display(Name = "Customers Account")]
        [Index("CustomerAccount")]
        public Guid? CustomersAccountId { get; set; }

        public virtual CustomersAccount CustomersAccount { get; set; }
       
        [Display(Name = "Transaction Category")]
        [Index]
        public int TransactionCategory { get; set; }

        public string TransactionCategoryDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.TransactionCategory), TransactionCategory) ? EnumHelper.GetDescription((Enumerations.TransactionCategory)TransactionCategory) : string.Empty;
            }
        }

        public Guid? C2BTransactionId { get; set; }

        public C2BTransaction C2BTransaction { get; set; }

        /// <summary>
        /// Transaction Description
        /// </summary>
        [Display(Name = "Remarks")]
        [MaxLength(256)]
        public string Description { get; set; }

        [Display(Name = "Created By")]
        [MaxLength(100)]
        public string CreatedBy { get; set; }

        [Display(Name = "Approved By")]
        [MaxLength(100)]
        public string ApprovedBy { get; set; }

        [Display(Name = "Is Approved?")]
        [Index]
        public bool IsApproved { get; set; }

        [NotMapped]
        public string Commands { get; set; }

        [NotMapped]
        public string LoanReferenceNo { get; set; }

        public decimal TotalTransactionAmount { get; set; }

        [Display(Name="Batch No.")]
        public int TransactionBatchNumber { get; set; }
    }
}

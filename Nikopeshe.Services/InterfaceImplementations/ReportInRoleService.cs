﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class ReportInRoleService : IReportInRoleService
    {
        private IRepository<ReportInRoles> reportinrolerepository;

        public string ErrorMessage = string.Empty;

        public ReportInRoleService(IRepository<ReportInRoles> _reportinrolerepository)
        {
            this.reportinrolerepository = _reportinrolerepository;
        }

        public void AddNewReportInRole(ReportInRoles reportInRole, ServiceHeader serviceHeader)
        {
            if (reportInRole == null)
                throw new ArgumentNullException("null entity");

            reportinrolerepository.Insert(reportInRole, serviceHeader);
        }

        public IList<ReportInRoles> GetAllReportsInRolesByRoleCategory(int RoleCategory)
        {
            var findReportInRolesByRoleCategory = reportinrolerepository.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.RoleCategory == RoleCategory);

            return findReportInRolesByRoleCategory.ToList();
        }

        public IList<ReportInRoles> GetAllReportsInCategory()
        {
            var findallreportinroles = reportinrolerepository.GetAll(IsolationLevel.ReadUncommitted);

            return findallreportinroles.ToList();
        }

        public ReportInRoles FindReportInRolesByReportId(Guid ReportId)
        {
            var findallreportinroles = reportinrolerepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.ReportId == ReportId).FirstOrDefault();

            return findallreportinroles;
        }
    }
}

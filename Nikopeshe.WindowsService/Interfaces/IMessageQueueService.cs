﻿using Nikopeshe.Entity.DataModels;
using System.Messaging;

namespace Nikopeshe.WindowsService
{
    public interface IMessageQueueService
    {
        string Send(string queuePath, object data, Enumerations.MessageCategory messageCategory, MessagePriority priority);
    }
}

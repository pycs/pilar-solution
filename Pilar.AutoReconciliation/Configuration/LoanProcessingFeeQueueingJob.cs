﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.AutoReconciliation;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.AutoReconciliation.Configuration
{
    public class LoanProcessingFeeQueueingJob : IJob
    {
        private readonly IMessageQueueService _messageQueueService;
        private readonly IReconciliationService _reconService;
        private readonly string _messageRelayQueuePath;

        public LoanProcessingFeeQueueingJob()
        {
            _messageQueueService = new MessageQueueService();
            _reconService = new ReconciliationService();
            _messageRelayQueuePath = ConfigurationManager.AppSettings["ReconQueuePath"];
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                // 1. Retrieve messages whose DLR status is UnKnown
                var findPendingFile = _reconService.FindUnProcessedFilesByStatus((int)Enumerations.FileProcessingStatus.Pending, (int)Enumerations.UploadFileCategory.ProcessingFee);

                // 2. Send the messages to msmq - Normal priority
                if (findPendingFile != null && findPendingFile.Any())
                {
                    foreach (var item in findPendingFile) 
                    {
                        _messageQueueService.Send(_messageRelayQueuePath, item.Id, Enumerations.MessageCategory.LoanProcessingFee, MessagePriority.AboveNormal);    
                        _reconService.UpdateFile(item.Id, (int)Enumerations.FileProcessingStatus.Processed);
                    }                                             
                }
            }

            catch (Exception ex)
            {
                //ExceptionLoggerFactory.CreateLog(ex.ToString());
                LoggerFactory.CreateLog().LogInfo("LoanProcessingFeeQueueingJob.Execute..." + ex);
            }
        }
    }
}

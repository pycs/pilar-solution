﻿using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nikopeshe.WcfWebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class EmailAlertWebService
    {
        private IRepository<EmailAlert> EmailAlertRepository;

        public EmailAlertWebService(IRepository<EmailAlert> _emailalertrepository)
        {
            this.EmailAlertRepository = _emailalertrepository;
        }       

        public IList<EmailAlert> FindEmailAlertByDRLStatus(int status)
        {
            if (string.IsNullOrWhiteSpace(status.ToString()))
                throw new ArgumentNullException("null entity");

            var results = EmailAlertRepository.GetAll().Where(x => x.MailMessageDLRStatus == status);

            var count = results.Count();

            return results.ToList();
        }
    }
}
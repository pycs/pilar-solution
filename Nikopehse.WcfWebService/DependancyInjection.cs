﻿using Nikopeshe.WcfWebService.InterfaceImplementation;
using Nikopeshe.WcfWebService;
using Nikopeshe.WcfWebService.Interfaces;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nikopeshe.WcfWebService
{
    public class DependancyInjection : NinjectModule
    {
        public override void Load()
        {
            //Injects the constructors of all DI-ed objects 
            //with a LinqToSQL implementation of IRepository
            Bind<IEmailAlertService>().To<MailAlert>();
        }
    }
}
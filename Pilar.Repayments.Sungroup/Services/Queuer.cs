﻿using Crosscutting.NetFramework.Logging;
using Pilar.Repayments.Sungroup.Configuration;
using Pilar.Services.Services;
using Quartz;
using Quartz.Impl;
using System;
using System.ComponentModel.Composition;
using System.Configuration;

namespace Pilar.Repayments.Sungroup.Services
{
    [Export(typeof(IPlugin))]
    public class Queuer : IPlugin
    {
        private readonly IScheduler _scheduler;

        public Queuer()
        {
            // Get a reference to the scheduler
            var sf = new StdSchedulerFactory();

            // Get an instance of the Quartz.Net scheduler
            _scheduler = sf.GetScheduler();
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{C990A51B-1795-4114-8FF8-6F80A33A8E62}"); }
        }

        public string Description
        {
            get { return "C2B PENDING TRANSACTIONS QUEUER"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                // Start the scheduler if its in standby
                if (!_scheduler.IsStarted)
                    _scheduler.Start();

                // Define the Job to be scheduled
                var job = JobBuilder.Create<IPNQueuer>()
                    .WithIdentity("IPNConfirmationQueueingJob", "Pilar")
                    .RequestRecovery()
                    .Build();

                // Associate a trigger with the Job
                var cronExpression = ConfigurationManager.AppSettings["C2BConfirmationQueueingJobCronExpression"];
                var trigger = (ICronTrigger)TriggerBuilder.Create()
                    .WithIdentity("IPNConfirmationQueueingJob", "Pilar")
                    .WithCronSchedule(cronExpression)
                    .StartAt(DateTime.UtcNow)
                    .WithPriority(1)
                    .Build();

                // Validate that the job doesn't already exist
                if (_scheduler.CheckExists(new JobKey("IPNConfirmationQueueingJob", "Pilar")))
                {
                    _scheduler.DeleteJob(new JobKey("IPNConfirmationQueueingJob", "Pilar"));
                }

                var schedule = _scheduler.ScheduleJob(job, trigger);

#if DEBUG
                //Logger.CreateLog("Job '{0}' scheduled for '{1}'" +"EmailQueueingJob" + schedule.ToString("r"));
#endif
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        public void Exit()
        {
            _scheduler.Shutdown();
        }

        #endregion
    }
}

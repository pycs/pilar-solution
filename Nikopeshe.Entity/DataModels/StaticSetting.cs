﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{
    public class StaticSetting : BaseEntity
    {
        [Display(Name = "Key")]
        [Required]
        [Column(TypeName = "VARCHAR")]
        [Index]
        [MaxLength(255)]
        public string Key { get; set; }

        [Display(Name = "Value")]
        [Required]
        [Column(TypeName = "VARCHAR")]
        [Index]
        [MaxLength(255)]
        public string Value { get; set; }

        [Display(Name = "Is Locked?")]                
        [Index]
        public bool IsLocked { get; set; }

        [NotMapped]
        public string Commands { get; set; }
    }
}

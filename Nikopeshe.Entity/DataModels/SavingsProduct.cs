﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    public class SavingsProduct : BaseEntity
    {
        [Index("ChartofAccountId")]
        public Guid ChartofAccountId { get; set; }

        public virtual ChartofAccount ChartofAccount { get; set; }

        [Display(Name = "Product Name")]
        [Required]
        [MaxLength(100)]
        public string ProductName { get; set; }

        [Display(Name="Product Code")]        
        [Column(TypeName = "VARCHAR")]
        [Index("Product Code")]
        [MaxLength(100)]
        public string ProductCode { get; set; }

        [Display(Name = "Is Enabled?")]
        [Required]
        [Index]
        public bool IsEnabled { get; set; }

        [Index]
        [Display(Name="Is Default?")]
        public bool IsDefault { get; set; }

        [NotMapped]
        public string Commands { get; set; }
    }
}

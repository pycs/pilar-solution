﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface IC2BTransactionService
    {
        void AddNewC2BTransaction(C2BTransaction c2bmapping, ServiceHeader serviceHeader);

        void UpdateC2BTransaction(C2BTransaction c2bmapping, ServiceHeader serviceHeader);

        C2BTransaction FindC2BTransactionById(Guid Id);

        IList<C2BTransaction> GetAllC2BTransaction();

        C2BTransaction FindC2BTransactionByTransactionId(string TransactionId);

        C2BTransaction FindC2BTransactionByThirdPartyTransID(string ThirdPartyTransID);

        IList<C2BTransaction> GetC2bTransactionsByStatus(int Status);

        IList<C2BTransaction> GetUnkownAndSuspenseC2BTransactions(int StatusA, int statusB, int StatusC);

        C2BTransaction FindC2BTransactionByMpesaReceiptCode(string MpesaReceiptCode);

        PageCollectionInfo<C2BTransaction> FindC2BTransactionByIdAndFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        PageCollectionInfo<C2BTransaction> FindC2BTransactionByStatusAndFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        PageCollectionInfo<C2BTransaction> FindSuspenseAndUnknownTransactionsFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);
    }
}

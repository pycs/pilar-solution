﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.InterfaceImplementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface ICustomerDocumentService
    {
        /// <summary>
        /// Add new customer document
        /// </summary>
        /// <param name="customerdocument"></param>
        void AddNewCustomerDocument(CustomerDocument customerdocument, ServiceHeader serviceHeader);

        /// <summary>
        /// Find Customer Document Given Document Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        CustomerDocument FindCustomerDocumentById(Guid Id);

        /// <summary>
        /// Find Customer Document Given Document Type and Document Id
        /// </summary>
        /// <param name="doctype"></param>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        CustomerDocument FindCustomerDocumentByDocumentTypeAndCustomerId(Enumerations.CustomerDocumentTypes doctype, Guid CustomerId);

        /// <summary>
        /// Get all customer documents
        /// </summary>
        /// <returns></returns>
        IList<CustomerDocumentView> GetAllCustomerDocuments(Guid CustomerId);

        /// <summary>
        /// Verify all Customer document existence
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        bool VerifyAllCustomerDocumentExistence(Guid customerId);
    }
}

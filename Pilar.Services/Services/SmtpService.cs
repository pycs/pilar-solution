﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services
{
    [Export(typeof(ISmtpService))]
    public class SmtpService : ISmtpService
    {
        private readonly Hashtable _attachments;

        public SmtpService()
        {
            _attachments = new Hashtable();
        }

        public void AddAttachment(string filePath)
        {
            _attachments.Add(_attachments.Count + 1, filePath);
        }

        public void SendEmail(MailMessage mailMessage)
        {
            using (SmtpClient client = new SmtpClient())
            {
                client.ServicePoint.MaxIdleTime = 2;

                client.ServicePoint.ConnectionLimit = 1;

                client.Send(mailMessage);
            }
        }

        public void SendEmail(MailAddress from, MailAddressCollection to, string subject, string body, bool isBodyHtml)
        {
            MailMessage mailMessage = new MailMessage();

            mailMessage.From = from;

            foreach (MailAddress mailAddress in to)
                mailMessage.To.Add(mailAddress);

            foreach (DictionaryEntry attachment in _attachments)
                mailMessage.Attachments.Add(new Attachment(attachment.Value.ToString()));

            mailMessage.Subject = subject.Replace('\r', ' ').Replace('\n', ' ');

            mailMessage.Body = body;

            mailMessage.IsBodyHtml = isBodyHtml;

            SendEmail(mailMessage);
        }

        public void SendEmail(string from, string to, string subject, string body, bool isBodyHtml)
        {
            MailAddressCollection mailAddressCollectionTo = new MailAddressCollection();

            MailAddress mailAddressFrom = new MailAddress(from);

            string[] strToAddresses = to.Replace("; ", ";").Split(char.Parse(";"));

            for (int intIndex = 0; intIndex < strToAddresses.Length; intIndex++)
                if (!string.IsNullOrWhiteSpace(strToAddresses[intIndex]))
                    mailAddressCollectionTo.Add(new MailAddress(strToAddresses[intIndex]));

            SendEmail(mailAddressFrom, mailAddressCollectionTo, subject, body, isBodyHtml);
        }

        public void SendEmail(MailAddress from, MailAddressCollection to, MailAddressCollection cc, string subject, string body, bool isBodyHtml)
        {
            MailMessage mailMessage = new MailMessage();

            mailMessage.From = from;

            foreach (MailAddress mailAddress in to)
                mailMessage.To.Add(mailAddress);

            foreach (MailAddress mailAddress in cc)
                mailMessage.CC.Add(mailAddress);

            foreach (DictionaryEntry attachment in _attachments)
                mailMessage.Attachments.Add(new Attachment(attachment.Value.ToString()));

            mailMessage.Subject = subject;

            mailMessage.Body = body;

            mailMessage.IsBodyHtml = isBodyHtml;

            SendEmail(mailMessage);
        }

        public void SendEmail(string from, string to, string cc, string subject, string body, bool isBodyHtml)
        {
            MailAddressCollection mailAddressCollectionTo = new MailAddressCollection();

            MailAddress mailAddressFrom = new MailAddress(from);

            string[] strToAddresses = to.Replace("; ", ";").Split(char.Parse(";"));

            for (int intIndex = 0; intIndex < strToAddresses.Length; intIndex++)
                if (!string.IsNullOrWhiteSpace(strToAddresses[intIndex]))
                    mailAddressCollectionTo.Add(new MailAddress(strToAddresses[intIndex]));

            MailAddressCollection mailAddressCollectionCc = new MailAddressCollection();

            MailAddress mailAddressCc = new MailAddress(from);

            string[] strCCAddresses = cc.Replace("; ", ";").Split(char.Parse(";"));

            for (int intIndex = 0; intIndex < strCCAddresses.Length; intIndex++)
                if (!string.IsNullOrWhiteSpace(strCCAddresses[intIndex]))
                    mailAddressCollectionCc.Add(new MailAddress(strCCAddresses[intIndex]));

            SendEmail(mailAddressFrom, mailAddressCollectionTo, mailAddressCollectionCc, subject, body, isBodyHtml);
        }
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class LoanTransactionHistoryService : ILoanTransactionHistoryService
    {
        private IRepository<LoanTransactionHistory> loantransactionhistoryrepository;

        private IRepository<LoanRequest> loanrequestrepository;

        private IRepository<CustomersAccount> customeraccountrepository;

        string errorMessage = string.Empty;
       
        public LoanTransactionHistoryService(IRepository<LoanTransactionHistory> _loantransactionhistoryrepository,IRepository<CustomersAccount> _customeraccountrepository, IRepository<LoanRequest> _loanrequestrepository)
        {
            if(_loanrequestrepository == null || _loantransactionhistoryrepository == null)
                throw new ArgumentNullException("null repository");

            this.loantransactionhistoryrepository = _loantransactionhistoryrepository;
            this.loanrequestrepository = _loanrequestrepository;
            this.customeraccountrepository = _customeraccountrepository;
        }

        /// <summary>
        /// add new loan transaction history
        /// </summary>
        /// <param name="loanhistory"></param>
        public void AddNewLoanHistory(LoanTransactionHistory loanhistory, ServiceHeader serviceHeader)
        {
            try
            {
                if (loanhistory == null)
                    throw new ArgumentNullException("null entity");

                loantransactionhistoryrepository.Insert(loanhistory, serviceHeader);
            }

            catch(DbEntityValidationException ex)
            {
                foreach(var validationErrors in ex.EntityValidationErrors)
                {
                    foreach(var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} ErroMessage: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw new Exception(errorMessage, ex);
            }            
        }

        public void UpdateLoanTransactionHistory(LoanTransactionHistory loanhistory, ServiceHeader serviceHeader)
        {
            try
            {
                if(loanhistory == null)
                    throw new ArgumentNullException("null entity");

                loantransactionhistoryrepository.Update(loanhistory, serviceHeader);
            }

            catch(DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} ErroMessage: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw new Exception(errorMessage, ex);
            }
        }

        public IList<LoanTransactionHistory> GetAllLoanTransactionsHistory()
        {
            return loantransactionhistoryrepository.GetAll(IsolationLevel.ReadCommitted).ToList();
        }

        public LoanTransactionHistory GetLoanTransactionsHistoryById(Guid Id)
        {
            return loantransactionhistoryrepository.GetById(Id);
        }

        public LoanTransactionHistory GetLoanTransactionsByLoanRequestId(Guid LoanRequestId)
        {
            return loantransactionhistoryrepository.GetAll(IsolationLevel.ReadCommitted).FirstOrDefault(x => x.LoanRequestId == LoanRequestId);
        }

        /// <summary>
        /// Get all customer transactions for active loan
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public IList<LoanTransactionHistory> GetAllCustomerTransationHistory(Guid CustomerId)
        {
            var getloanrequestscustomer = loanrequestrepository.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.CustomerAccountId == CustomerId && x.Status != (int)Enumerations.LoanStatus.Uncleared);

            var getloanrequestId = getloanrequestscustomer.Select(x => x.Id).FirstOrDefault();

            return loantransactionhistoryrepository.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.LoanRequestId == getloanrequestId).ToList();
        }

        /// <summary>
        /// returns total principle amount per loan request and total interest per loan request
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="TransactionType"></param>
        /// <returns></returns>
        public Tuple<Decimal, Decimal> SumofAllTransactionsPerTransactionType(Guid CustomerId, int TransactionType)
        {
            var getloanrequestscustomer = loanrequestrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CustomerAccountId == CustomerId && x.Status == (int)Enumerations.LoanStatus.Uncleared);

            Guid loanrequestid = getloanrequestscustomer.Select(x => x.Id).FirstOrDefault();

            var getalltransationsperloanrequest = loantransactionhistoryrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.LoanRequestId == loanrequestid && (int)x.TransactionType == TransactionType);

            var transactionamount = getalltransationsperloanrequest.Select(x => x.TransactionAmount).Sum();            

            return new Tuple<decimal, decimal>(transactionamount, 0m);
        }

        public IList<LoanTransactionHistory> GetAllTransactionsByType(int type)
        {
            return loantransactionhistoryrepository.GetAll(IsolationLevel.ReadUncommitted).Where(x => (int)x.TransactionType == type).ToList();
        }

        public IList<LoanTransactionHistory> GetLoanTransactionHistoryByLoanRequestId(Guid LoanRequestId)
        {
            var findloanrequestbyid = loanrequestrepository.GetById(LoanRequestId);

            var findcustomersavingsaccount = customeraccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CustomerId == findloanrequestbyid.CustomerAccount.CustomerId && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
            var accountstatement = loantransactionhistoryrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.LoanRequestId == LoanRequestId
                && x.TransactionType == Enumerations.TransactionType.Credit && x.TransactionCategory != (int)Enumerations.TransactionCategory.Approval && x.CustomersAccountId == findcustomersavingsaccount.Id);

            return accountstatement.ToList();           
        }

        public IList<LoanTransactionHistory> GetAllTransactionsByTransactionTypeAndTransactionCategory(int TransactionType, int TransactionCategory, IsolationLevel isolationLevel)
        {
            var loantransactions = loantransactionhistoryrepository.GetAll(isolationLevel).Where(x => (int)x.TransactionType == TransactionType
                && x.TransactionCategory == TransactionCategory && x.CustomersAccount.AccountType == (int)Enumerations.ProductCode.Loan).OrderByDescending(x =>x.CreatedDate).Take(10).ToList();

            var repaymenttransactions = loantransactions.ToList();

            return repaymenttransactions.ToList();
        }

        public IList<LoanTransactionHistory> GetLoanTransactionHistoryByLoanRequestIdAndCustomerAccountId(Guid LoanRequestId, Guid CustomerAccountId, bool IsLoanMigrated)
        {
            var gettransactions = new List<LoanTransactionHistory>();

            if (IsLoanMigrated == true)
            {
                gettransactions = loantransactionhistoryrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.LoanRequestId == LoanRequestId && x.IsApproved == true
               && x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment &&
               x.TransactionType == Enumerations.TransactionType.Debit && x.CustomersAccountId == CustomerAccountId
               && x.CustomersAccount.AccountType == (int)Enumerations.ProductCode.Savings).ToList();
            }

            else
            {
                gettransactions = loantransactionhistoryrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => (int)x.TransactionType == (int)Enumerations.TransactionType.Debit
                 && x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment &&
                 x.CustomersAccount.AccountType == (int)Enumerations.ProductCode.Loan && x.LoanRequestId == LoanRequestId).ToList();                
            }
          
           return gettransactions;
        }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.DTO
{
    public class PageCollectionInfo<T>
    {
        /// <summary>
        /// Current Page items
        /// </summary>
        public List<T> PageCollection { get; set; }

        /// <summary>
        /// Count of all items in store
        /// </summary>
        public int ItemsCount { get; set; }

        ///// <summary>
        ///// Balance B/F in G/L Account
        ///// </summary>
        //public decimal BalanceBroughtFoward { get; set; }

        ///// <summary>
        ///// Balance C/F in G/L Account
        ///// </summary>
        //public decimal BalanceCarriedForward { get; set; }

        ///// <summary>
        ///// Total Debits in G/L Account
        ///// </summary>
        //public decimal TotalDebits { get; set; }

        ///// <summary>
        ///// Total Credits in G/L Account
        ///// </summary>
        //public decimal TotalCredits { get; set; }

        ///// <summary>
        ///// Total Apportioned in Batch
        ///// </summary>
        //public decimal TotalApportioned { get; set; }

        ///// <summary>
        ///// Total Shortage in Batch
        ///// </summary>
        //public decimal TotalShortage { get; set; }
    }
}

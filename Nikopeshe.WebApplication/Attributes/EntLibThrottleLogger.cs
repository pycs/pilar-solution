﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebApiThrottle;

namespace Nikopeshe.WebApplication.Attributes
{
    public class EntLibThrottleLogger : IThrottleLogger
    {
        private readonly IEmailAlertService _emailAlertService;
        //ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public EntLibThrottleLogger()
        {
            _emailAlertService = DependencyResolver.Current.GetService<IEmailAlertService>();
        }

        public void Log(ThrottleLogEntry entry)
        {
            LoggerFactory.CreateLog().LogInfo("WebApiThrottle: {0} Request {1} from {2} has been throttled (blocked), quota {3}/{4} exceeded by {5}",
                entry.LogDate,
                entry.RequestId,
                entry.ClientIp,
                entry.RateLimit,
                entry.RateLimitPeriod,
                entry.TotalRequests);

            var emailAlertDTO = new EmailAlert
            {
                MailMessageTo = "felix.ngugi@pycs.co.ke",                
                MailMessageSubject = string.Format("WebApiThrottle ({0})", entry.ClientIp),
                MailMessageBody = string.Format("{0} Request {1} from {2} has been throttled (blocked), quota {3}/{4} exceeded by {5}", entry.LogDate, entry.RequestId, entry.ClientIp, entry.RateLimit, entry.RateLimitPeriod, entry.TotalRequests),
                MailMessageOrigin = (int)Nikopeshe.Entity.DataModels.Enumerations.MessageOrigin.Within,
                MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending
            };

            _emailAlertService.AddNewEmailAlert(emailAlertDTO, null);
        }        
    }
}
﻿using Nikopeshe.Entity.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    [Serializable]
    public class LoanRequest : BaseEntity
    {
        public LoanRequest()
        {
            IsMigrated = false;
            AcceptTermsAndConditions = true;
        }
        //This is the Customer Account Id for account type as loan
        [Display(Name ="Customer Name")]
        [Index("Id_CustomerAccountId")]               
        public Guid CustomerAccountId { get; set; }
        
        public virtual CustomersAccount CustomerAccount { get; set; }        

        /// <summary>
        /// Loan product Id
        /// </summary>
        [Display(Name="Loan Type")]
        [Index("ID_LoanProductId")]
        public Guid LoanProductId { get; set; }

        [Index]
        public Guid BranchId { get; set; }

        public virtual Branch Branch { get; set; }

        [NotMapped]
        [Display(Name="Customer Name")]
        public Guid CustomerId { get; set; }
        
        public string CustomerLoanRequest
        {
            get
            {
                return string.Format("{0}, Mobile Number => {1}, Loan Ref No. => {2}", CustomerAccount.Customer.FullName, CustomerAccount.Customer.MobileNumber, LoanReferenceNumber);
            }
        }

        public string CustomerIdNumberLoanRefMobileNumber
        {
            get
            {
                return string.Format("{0}, Loan Ref No => {1}, Mobile Number => {2}", CustomerAccount.Customer.IDNumber, LoanReferenceNumber, CustomerAccount.Customer.MobileNumber);
            }
        }
        
        public virtual LoanProduct LoanProduct { get; set; }

        [Display(Name="Loan Officer")]
        [Index]
        public Guid? RelationshipManagerId { get; set; }

        public virtual RelationshipManager RelationshipManager { get; set; }
        /// <summary>
        /// Principle Amount
        /// </summary>
        [Display(Name = "Loan Amount")]
        [Required(ErrorMessage = "The loan amount is required.")]
        [Index("Amount")]
        [DisplayFormat(DataFormatString = "{0:0,0}", ApplyFormatInEditMode = false)]
        public Decimal LoanAmount { get; set; }
        
        /// <summary>
        /// Disbursed Amount
        /// </summary>
        [Display(Name = "Disbursed Amount")]
        [Required(ErrorMessage = "The loan amount is required.")]
        [Index("Disbursed Amount")]
        [DisplayFormat(DataFormatString = "{0:0,0}", ApplyFormatInEditMode = false)]
        public Decimal DisbursedAmount { get; set; }

        /// <summary>
        /// loan reference number
        /// </summary>
        [Display(Name="Loan Ref. No.")]
        [Index("Ref No")]
        [Column(TypeName = "VARCHAR")]
        [MaxLength(100)]
        public string LoanReferenceNumber { get; set; }

        /// <summary>
        /// Loan start date
        /// </summary>
        [Display(Name="Loan Start Date")]
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime LoanStartDate { get; set; }

        /// <summary>
        /// Loan Payment end date
        /// </summary>
        [Display(Name = "Loan End Date")]
        [Required]
        [DataType(DataType.Date)]        
        public DateTime LoanEndDate { get; set; }

        /// <summary>
        /// Loan Status [Pending, Paid, Rejected]
        /// </summary>
        [Display(Name="Loan Status")]
        [Required]
        [Index("Status")]
        public int Status { get; set; }        

        public string StatusDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.LoanStatus), Status) ? EnumHelper.GetDescription((Enumerations.LoanStatus)Status) : string.Empty;
            }
        }
        
        /// <summary>
        /// Computed Interest amount from principle amount
        /// </summary>
        [Display(Name = "Interest Amount")]
        [Required]        
        [DisplayFormat(DataFormatString = "{0:0,0}")]
        public decimal InterestAmount { get; set; }

        public decimal TotalLoanAmount
        {
            get
            {
                return LoanAmount + InterestAmount;
            }
        }
        [Display(Name = "Remarks")]
        [MaxLength(100)]
        public string Remarks { get; set; }

        [Display(Name = "Bank Account Number / Mobile Number")]
        [MaxLength(100)]        
        [Required]
        [Column(TypeName = "VARCHAR")]
        [Index("PrimaryAccountNumber")]
        public string PrimaryAccountNumber { get; set; }

        [Index]
        [Display(Name="How would you like to receive money?")]
        public Enumerations.EFTType Type { get; set; }

        [MaxLength(50)]
        public string TypeDesc { get; set; }
        
        [Display(Name="Disbursement Method")]
        public string DisbursementMethod
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.EFTType), Type) ? EnumHelper.GetDescription((Enumerations.EFTType)Type) : string.Empty;
            }
        }
        public bool IsDelivered { get; set; }

        [MaxLength(10)]
        public string BankCode { get; set; }

        [Index]
        public int MCCMNC { get; set; }

        [MaxLength(50)]
        public string MCCMNCDesc { get; set; }

        [Index]
        public int SPSStatus { get; set; }

        [MaxLength(50)]
        public string SPSStatusDesc { get; set; }

        [Index("STAN", IsUnique = false)]
        public Guid SystemTraceAuditNumber { get; set; }

        [Column(TypeName = "VARCHAR")]
        [Index]
        [MaxLength(20)]
        public string B2MResponseCode { get; set; }

        [MaxLength(100)]
        public string B2MResponseDesc { get; set; }

        [Column(TypeName = "VARCHAR")]
        [Index]
        [MaxLength(20)]
        public string B2MResultCode { get; set; }

        [MaxLength(256)]
        public string B2MResultDesc { get; set; }

        [Index]
        [Column(TypeName = "VARCHAR")]
        [MaxLength(100)]
        public string ThirdPartyTransactionId { get; set; }
        
        [Column(TypeName = "VARCHAR")]
        [MaxLength(100)]
        public string B2MTransactionID { get; set; }

        [MaxLength(100)]
        public string TransactionDateTime { get; set; }

        public decimal WorkingAccountAvailableFunds { get; set; }

        public decimal UtilityAccountAvailableFunds { get; set; }

        public decimal ChargePaidAccountAvailableFunds { get; set; }

        [MaxLength(200)]
        public string TransactionCreditParty { get; set; }

        [Display(Name = "Next Payment Date (MM/dd/yyyy)")]        
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? NextPaymentDate { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? ApprovedDate { get; set; }

        [Display(Name="Disbursement Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? DisbursementDate { get; set; }

        [Index]
        public int ProcessingStatus { get; set; }

        public string ProcessingStatusDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.ProcessingStatus), SPSStatus) ? EnumHelper.GetDescription((Enumerations.ProcessingStatus)SPSStatus) : string.Empty;
            }
        }

        [NotMapped]
        public string Commands { get; set; }

        [NotMapped]
        [Display(Name="Bank Name")]
        public string ApplicantBankCode { get; set; }

        [NotMapped]
        [Display(Name="Branch Name")]        
        public string ApplicantBranchCode { get; set; }

        [Display(Name = "Accept Terms And Conditions")]
        [NotMapped]
        public bool AcceptTermsAndConditions { get; set; }

        [Display(Name="Http Get Response Code")]
        public int HttpGetResponseCode { get; set; }

        [Display(Name="Http Post Response Code")]
        public int HttpPostResponseCode { get; set; }      

        [Display(Name="Negotiated Installments")]
        public int NegotiatedInstallments { get; set; }

        public string RegisteredBy { get; set; }

        public string ApprovedBy { get; set; }

        public string DisbursedBy { get; set; }

        [Display(Name="Outstanding Loan Balance")]
        public decimal OutstandingLoanBalance { get; set; }

        [NotMapped]
        public decimal LoanBalance { get; set; }

        public bool IsMigrated { get; set; }

        public string RejectionRemarks { get; set; }

        public string ReversalRemarks { get; set; }

        [Display(Name="Interest Due Today")]
        public Decimal? InterestDueToday { get; set; }

        public DateTime? ScheduledRepaymentDate { get; set; }

        public int LoanType { get; set; }

        public string LoanTypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.LoanType), LoanType) ? EnumHelper.GetDescription((Enumerations.LoanType)LoanType) : string.Empty;
            }
        }

        public virtual ICollection<Guarantor> Guarantors { get; set; }

        public virtual ICollection<Collateral> Collaterals { get; set; }
    }     
}

﻿using Nikopeshe.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.GraduatedScale
{
    public class GraduatedScaleService : IGraduatedScaleService
    {
        DataContext dataContext = new DataContext();

        public List<Nikopeshe.Entity.DataModels.GraduatedScale> FindGraduatedScaleUsingRangeAndChargeId(decimal amount, Guid ChargeId)
        {
            var findgradutedscale = (from a in dataContext.GraduatedScale
                                     where a.ChargeId == ChargeId && (a.LowerLimit <= amount && a.UpperLimit >= amount)
                                     select a);

            return findgradutedscale.ToList();
        }
    }
}

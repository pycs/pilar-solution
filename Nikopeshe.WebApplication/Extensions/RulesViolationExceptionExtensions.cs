﻿using Infrastructure.Crosscutting.NetFramework.Utils;
using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Extensions
{
    public static class RulesViolationExceptionExtensions
    {
        public static void CopyTo(this RulesException ex, ModelStateDictionary modelState)
        {
            CopyTo(ex, modelState, null);
        }

        public static void CopyTo(this RulesException ex, ModelStateDictionary modelState, string prefix)
        {
            prefix = string.IsNullOrEmpty(prefix) ? "" : prefix + ".";

            foreach (var ruleViolation in ex.Errors)
            {
                string key = string.Empty;

                if (ruleViolation.PropertyExpression is Expression<Func<string>>)
                    key = ruleViolation.GetProperty();
                else key = ExpressionHelper.GetExpressionText(ruleViolation.PropertyExpression);

                modelState.AddModelError(prefix + key, ruleViolation.Message);
            }
        }
    }
}
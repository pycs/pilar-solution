﻿using Nikopeshe.Data;
using Nikopeshe.Services.Specifications;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

namespace Nikopeshe.Services
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll(IsolationLevel isolationLevel);

        TEntity GetById(object id);

        void Insert(TEntity entity, ServiceHeader serviceHeader);

        void Update(TEntity entity, ServiceHeader serviceHeader);

        void Delete(TEntity entity, ServiceHeader serviceHeader);

        PagedCollection<TEntity> AllMatchingPaged(ISpecification<TEntity> specification, int pageIndex, int pageSize, List<string> sortFields, bool ascending, params Expression<Func<TEntity, object>>[] children);
    }
}

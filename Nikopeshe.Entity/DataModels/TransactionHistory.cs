﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    public class TransactionHistory : BaseEntity
    {       
        [Index("CustomerID")]
        [Display(Name="Customer Name")]
        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        [Index("Amount")]
        [Display(Name="Amount")]
        public Decimal Amount { get; set; }

        /// <summary>
        /// Credit or Debit
        /// </summary>
        ///
        [Index("TransactionType")]
        [Display(Name="Transaction Type")]
        public Enumerations.TransactionType TransactionType { get; set; }

        public string TransactionTypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.TransactionType), TransactionType) ? EnumHelper.GetDescription((Enumerations.TransactionType)TransactionType) : string.Empty;
            }
        }
        
        public DateTime TransactionDateTime { get; set; }

        /// <summary>
        /// Airtime, Bills etc
        /// </summary>
        public Enumerations.Type Type { get; set; }

        public string TypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.Type), Type) ? EnumHelper.GetDescription((Enumerations.Type)TransactionType) : string.Empty;
            }
        }

        [NotMapped]
        public string Commands { get; set; }

        public static ValidationResult CheckType(string value, ValidationContext context)
        {
            var bindingModel = context.ObjectInstance as TransactionHistory;
            if (bindingModel == null)
                throw new NotSupportedException("ObjectInstance must be TransactionHistory");

            Enumerations.Type typeAirtime = Enumerations.Type.Airtime;
            Enumerations.Type typePaybill = Enumerations.Type.Paybill;
            if (string.IsNullOrWhiteSpace(value) || !Enum.TryParse<Enumerations.Type>(value, out typeAirtime) || !Enum.TryParse<Enumerations.Type>(value, out typePaybill))
                return new ValidationResult("UnKnown Type.");

            return ValidationResult.Success;
        }

        public static ValidationResult CheckTransactionType(string value, ValidationContext context)
        {
            var bindingModel = context.ObjectInstance as TransactionHistory;
            if (bindingModel == null)
                throw new NotSupportedException("ObjectInstance must be TransactionHistory");

            Enumerations.TransactionType cr = Enumerations.TransactionType.Credit;
            Enumerations.TransactionType dr = Enumerations.TransactionType.Debit;
            if (string.IsNullOrWhiteSpace(value) || !Enum.TryParse<Enumerations.TransactionType>(value, out cr) || !Enum.TryParse<Enumerations.TransactionType>(value, out dr))
                return new ValidationResult("UnKnown Transaction Type.");

            return ValidationResult.Success;
        }

    }
}

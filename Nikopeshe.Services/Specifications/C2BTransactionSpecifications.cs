﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nikopeshe.Services.Specifications
{
    public static class C2BTransactionSpecifications
    {
        public static ISpecification<C2BTransaction> EFTOrderLinesByEFTOrder(Guid id, string text)
        {
            Specification<C2BTransaction> specification = new DirectSpecification<C2BTransaction>(c => c.Id == id);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var bankAccountNumberSpec = new DirectSpecification<C2BTransaction>(c => c.MSISDN.Contains(text));

                var nameSpec = new DirectSpecification<C2BTransaction>(c => c.BillRefNumber.Contains(text));

                var referenceSpec = new DirectSpecification<C2BTransaction>(c => c.TransID.Contains(text));

                specification &= (bankAccountNumberSpec | nameSpec | referenceSpec);
            }

            return specification;
        }

        public static Specification<C2BTransaction> DefaultSpec(string text)
        {
            Specification<C2BTransaction> specification = new TrueSpecification<C2BTransaction>();

            if (!String.IsNullOrWhiteSpace(text))
            {
                var companyNameSpec = new DirectSpecification<C2BTransaction>(c => c.MSISDN.Contains(text));

                var bankAccountNumberSpec = new DirectSpecification<C2BTransaction>(c => c.BillRefNumber.Contains(text));

                var remarksSpec = new DirectSpecification<C2BTransaction>(c => c.TransID.Contains(text));

                var mpesacode = new DirectSpecification<C2BTransaction>(c => c.TransID.Contains(text));

                specification &= (companyNameSpec | bankAccountNumberSpec | remarksSpec | mpesacode);
            }

            return specification;
        }

        public static ISpecification<C2BTransaction> FindC2BTransactionsByStatus(string text)
        {
            Specification<C2BTransaction> specification = new DirectSpecification<C2BTransaction>(c => c.Status == (int)Enumerations.C2BTransactionStatus.Processed || c.Status == (int)Enumerations.C2BTransactionStatus.JournalProcessed);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var msisdn = new DirectSpecification<C2BTransaction>(c => c.MSISDN.Contains(text));

                var billreferenceNo = new DirectSpecification<C2BTransaction>(c => c.BillRefNumber.Contains(text));

                var mpesacode = new DirectSpecification<C2BTransaction>(c => c.TransID.Contains(text));

                specification &= (msisdn | billreferenceNo | mpesacode);
            }

            return specification;
        }

        public static ISpecification<C2BTransaction> FindSuspenseAndUnknownTransaction(string text)
        {
            Specification<C2BTransaction> specification = new DirectSpecification<C2BTransaction>(c => c.Status == (int)Enumerations.C2BTransactionStatus.Suspense || c.Status == (int)Enumerations.C2BTransactionStatus.Unknown || c.Status == (int)Enumerations.C2BTransactionStatus.Waiting || c.Status == (int)Enumerations.C2BTransactionStatus.Reversed || c.Status == (int)Enumerations.C2BTransactionStatus.Reverse);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var msisdn = new DirectSpecification<C2BTransaction>(c => c.MSISDN.Contains(text));

                var billreferenceNo = new DirectSpecification<C2BTransaction>(c => c.BillRefNumber.Contains(text));

                var mpesacode = new DirectSpecification<C2BTransaction>(c => c.TransID.Contains(text));

                specification &= (msisdn | billreferenceNo | mpesacode);
            }

            return specification;
        }

        public static ISpecification<C2BTransaction> FindConsumerToBusinessTransactionByStatuses(string text)
        {
            Specification<C2BTransaction> specification = new DirectSpecification<C2BTransaction>(c => c.Status == (int)Enumerations.C2BTransactionStatus.Suspense || c.Status == (int)Enumerations.C2BTransactionStatus.Unknown || c.Status == (int)Enumerations.C2BTransactionStatus.Waiting || c.Status == (int)Enumerations.C2BTransactionStatus.Reversed || c.Status == (int)Enumerations.C2BTransactionStatus.Reverse);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var msisdn = new DirectSpecification<C2BTransaction>(c => c.MSISDN.Contains(text));

                var billreferenceNo = new DirectSpecification<C2BTransaction>(c => c.BillRefNumber.Contains(text));

                var mpesacode = new DirectSpecification<C2BTransaction>(c => c.TransID.Contains(text));

                specification &= (msisdn | billreferenceNo | mpesacode);
            }

            return specification;
        }
    }
}
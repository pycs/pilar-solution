﻿using Crosscutting.NetFramework.Logging;
using Infrastructure.Crosscutting.NetFramework;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MvcContrib.Pagination;
using Nikopeshe.Data;
using Nikopeshe.Data.Mappings;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.InterfaceImplementations;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using Nikopeshe.WebApplication.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
namespace Nikopeshe.WebApplication.Controllers
{        
    [Authorize]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class CustomerController : BaseController
    {
        internal readonly ICustomerService customerservice;
        internal readonly ILoanRequestService loanrequestservice;
        internal readonly ILoanTransactionHistoryService loantransactionhistory;
        internal readonly ISystemGeneralLedgerAccountMappingService glMappingService;
        internal readonly ISavingsProductService savingProductService;
        internal readonly DomainAuditTrailService domainaudittrail;
        internal readonly IEmailAlertService emailalerservice;
        internal readonly IUserInfoService userInfoService;
        internal readonly ICustomerAccountService customeraccountservice;
        internal readonly IBranchService branchservice;
        internal readonly IStaticSettingService staticSettingService;
        internal readonly IRelationshipManagerService relationshipManagerService;
        internal ITextAlertService textalertservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        private string TextSignature = ConfigurationManager.AppSettings["TextSignature"].ToString();
        private string ApplicationName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        public CustomerController(ICustomerService _customerservice, ILoanTransactionHistoryService _loantransactionhistory, 
            ILoanRequestService _loanrequestservice, DomainAuditTrailService _domainaudittrail, IEmailAlertService _emailalerservice,
            ICustomerAccountService _customeraccountservice, IBranchService _branchservice, ITextAlertService _textalertservice,
            IStaticSettingService _staticSettingService, ISystemGeneralLedgerAccountMappingService _glMappingService,
            ISavingsProductService _savingProductService, IUserInfoService _userInfoService, IRelationshipManagerService _relationshipManagerService)
        {
            if(_customeraccountservice == null || _loanrequestservice == null || _domainaudittrail == null || _customerservice == null
                || _emailalerservice == null || _loantransactionhistory == null || _branchservice == null || _textalertservice == null)
                throw new ArgumentNullException("null service reference");

            this.customeraccountservice = _customeraccountservice;
            this.loanrequestservice = _loanrequestservice;
            this.domainaudittrail = _domainaudittrail;
            this.customerservice = _customerservice;
            this.emailalerservice = _emailalerservice;
            this.loantransactionhistory = _loantransactionhistory;
            this.branchservice = _branchservice;
            this.userInfoService = _userInfoService;
            this.textalertservice = _textalertservice;
            this.staticSettingService = _staticSettingService;
            this.glMappingService = _glMappingService;
            this.savingProductService = _savingProductService;
            this.relationshipManagerService = _relationshipManagerService;
        }       
      
        /// <summary>
        /// Get all customers
        /// </summary>
        /// <returns></returns>
        /// 
        [Authorize(Roles = "Administrator, LoanOfficer")]
        public ActionResult Index()
        {           
            return View();
        }       

        public JsonResult getallcustomers(JQueryDataTablesModel datatablemodel)
        {
            var findCurrentUserId = User.Identity.Name;
            var pagecollection = new List<Customer>();
            if (findCurrentUserId != null)
            {
                var findCurrentUserInfo = userInfoService.FindUserByUserName(findCurrentUserId);
                if (findCurrentUserInfo != null)
                {
                    if (findCurrentUserInfo.Branch.BranchCode == "000")
                    {
                        pagecollection = customerservice.GetAllCustomers().ToList();  
                    }
                    else
                    {
                        pagecollection = customerservice.GetCustomersByBranchId(findCurrentUserInfo.BranchId).ToList();
                    }
                }
                else
                {
                    pagecollection = customerservice.GetAllCustomers().ToList();  
                }
            }            

            int totalrecordcount = 0;

            int searchrecordcount = 0;

            var transactionList = GetAllCustomer(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalrecordcount, searchRecordCount: out searchrecordcount, searchString: datatablemodel.sSearch, CustomerList: pagecollection);


            return Json(new JQueryDataTablesResponse<Customer>(items: transactionList,
                totalRecords: totalrecordcount,
                totalDisplayRecords: searchrecordcount,
                sEcho: datatablemodel.sEcho));
                            
       }

        public static IList<Customer> GetAllCustomer(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<Customer> CustomerList)
        {
            var GetCustomersList = CustomerList;

            totalRecordCount = GetCustomersList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomersList = GetCustomersList.Where(c => c.FullName
                    .ToLower().Contains(searchString.ToLower())
                    || c.IDNumber.ToLower().Contains(searchString.ToLower()) || c.MobileNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetCustomersList.Count;

            IOrderedEnumerable<Customer> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "FullName":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.FullName);
                        break;

                    case "MobileNumber":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.MobileNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.MobileNumber);
                        break;

                    case "IDNumber":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.IDNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IDNumber);
                        break;

                    case "Branch.BranchName":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName);
                        break;

                    case "IncomeRange":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.IncomeRange)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IncomeRange);
                        break;

                    case "IsEnabled":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }
       
        //
        // GET: /Loanrequest/       
        [Authorize]
        public ActionResult loanrequestlist()
        {
            return View();
        }

        public JsonResult getallloanrequestspercustomer(JQueryDataTablesModel datatablemodel)
        {
            var username = User.Identity.Name;
            var allloanrequestpagecollection = loanrequestservice.GetAllLoanRequests();
            var getuserid = customerservice.FindCustomerByMobileNumber(username);

            var pagecollection = loanrequestservice.GetAllCustomerActiveLoanRequests(getuserid.Id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetCustomerLoanRequest(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, CustomerLoanRequestList: pagecollection);


            return Json(new JQueryDataTablesResponse<LoanRequest>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));

        }

        public static IList<LoanRequest> GetCustomerLoanRequest(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanRequest> CustomerLoanRequestList)
        {
            var GetLoanRequestList = CustomerLoanRequestList;

            totalRecordCount = GetLoanRequestList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetLoanRequestList = GetLoanRequestList.Where(c => c.LoanProduct.ProductName
                    .ToLower().Contains(searchString.ToLower())
                    || c.CustomerAccount.Customer.FullName.ToLower().Contains(searchString.ToLower()) || c.LoanReferenceNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetLoanRequestList.Count;

            IOrderedEnumerable<LoanRequest> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "CustomerAccount.Customer.FullName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName);
                        break;

                    case "LoanProduct.ProductName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName);
                        break;

                    case "LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber);
                        break;

                    case "LoanAmount":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount);
                        break;

                    case "InterestAmount":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount);
                        break;

                    case "StatusDesc":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult CustomerRegistration()
        {
            ViewBag.IncomeRange = IncomeRangeSelectList();

            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();
            
            ViewBag.GenderCheckList = genderInfoCheckList();
            var currentUser = User.Identity.Name;
            var findCurrentUserDetails = userInfoService.FindUserByUserName(currentUser);
            Guid branchId = Guid.Empty;
            if (findCurrentUserDetails != null)
            {
                if (findCurrentUserDetails.Branch.BranchCode == "000")
                {
                    branchId = Guid.Empty;
                }
                else
                {
                    branchId = findCurrentUserDetails.BranchId;
                }
            }
            else
            {
                branchId = Guid.Empty;
            }

            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());

            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(Guid.Empty.ToString(), branchId);

            var customer = new Customer
            {
                DateofBirth = DateTime.Now
            };
            return View(customer);
        }

        [HttpPost]        
        public ActionResult CustomerRegistration(string DateofBirth, [Bind(Exclude = "DateofBirth")]Customer customer)
        {
            ViewBag.IncomeRange = IncomeRangeSelectList();
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());        
            string mobilenumber = customer.MobileNumber;
            var currentUser = User.Identity.Name;
            Guid branchId = Guid.Empty;
            var findCurrentUserDetails = userInfoService.FindUserByUserName(currentUser);
        
            if (findCurrentUserDetails != null)
            {
                if (findCurrentUserDetails.Branch.BranchCode == "000")
                {
                    branchId = Guid.Empty;
                }
                else
                {
                    branchId = findCurrentUserDetails.BranchId;
                }
            }
            else
            {
                branchId = Guid.Empty;
            }
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(Guid.Empty.ToString(), branchId);
            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();
            ViewBag.GenderCheckList = genderInfoCheckList();
            if (string.IsNullOrWhiteSpace(DateofBirth))
            {
                ModelState.AddModelError(string.Empty, "Date of Birth is required.");
                return View();
            }
            if(customer.BranchId == Guid.Empty)
            {
                ModelState.AddModelError(string.Empty, "Branch cannot be null.");
                return View(customer);
            }

            if (string.IsNullOrWhiteSpace(string.Concat(customer.CustomerType)))
            {
                ModelState.AddModelError(string.Empty, "Customer Type cannot be null.");
                return View(customer);
            }
            DateTime dDate;

            if (DateTime.TryParseExact(DateofBirth, "dd/MM/yyyy", null, DateTimeStyles.None, out dDate) == true)
            {
                customer.DateofBirth = DateTime.ParseExact(DateofBirth, "dd/MM/yyyy", null);
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid Date of Birth format.");
                return View(customer);
            }
            customer.ApprovalCode = GenerateRandomCustomerApprovalCode(6);
            bool checkduplicatecustomer;
            bool iscustomerunderage;
            string nextcustomernumber = customerservice.NextCustomerNumber();
            customer.CustomerNumber = nextcustomernumber;            
            string nextcustomeraccountnumber = customerservice.NextCustomerSavingAccountNumber(nextcustomernumber);
            CustomersAccount customersaccount = new CustomersAccount();
            if (ModelState.IsValid)
            {
                customer.IsApproved = true;
                customer.IsEnabled = true;
                customer.FirstName = customer.FirstName.ToUpper();
                customer.MiddleName = customer.MiddleName.ToUpper();
                customer.LastName = customer.LastName.ToUpper();
                customer.RegisteredBy = HttpContext.User.Identity.Name;
                customer.CustomerRegistrationType = Enumerations.CustomerRegistrationType.WalkIn;
                string password = Membership.GeneratePassword(8, 2);
                customersaccount.CustomerId = customer.Id;
                customersaccount.AccountType = (int)Enumerations.ProductCode.Savings;
                customersaccount.AccountNumber = nextcustomeraccountnumber;
                customersaccount.IsActive = true;
                checkduplicatecustomer = customerservice.CheckDuplicateCustomer(customer.MobileNumber, customer.IDNumber);
                iscustomerunderage = customerservice.ValidateCustomerAge(customer.DateofBirth);
                try
                {
                    if (checkduplicatecustomer)
                    {
                        ModelState.AddModelError(string.Empty, string.Format("Customer with Mobile Number {0} or Id Number {1} already exists", customer.MobileNumber, customer.IDNumber));

                        return View(customer);
                    }
                    
                    if (!iscustomerunderage)
                    {
                        ModelState.AddModelError(string.Empty, string.Format("You must be 18 years and above."));

                        return View(customer);
                    }
                   
                    else if (customer.IncomeRange.Equals("Select Income Range"))
                    {
                        ModelState.AddModelError(string.Empty, string.Format("Income range is required."));

                        return View(customer);
                    }
                    var findmembershipuser = Membership.GetUser(customer.MobileNumber);
                    if (checkduplicatecustomer == false && findmembershipuser == null)
                    {                        
                        var findStaticSetting = staticSettingService.FindStaticSettingByKey("CREATEMEMBERSHIPACCOUNT");
                        
                        if (findStaticSetting.Value == "1")
                        {
                            MembershipCreateStatus status;
                            MembershipUser createuser = Membership.CreateUser(customer.MobileNumber, password, customer.EmailAddress, "Where were you when you first heard about 9/11?", "School", true, out status);
                            Roles.AddUserToRole(customer.MobileNumber, "Customer");
                            string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                            string loginurl = "<a href='" + mvcclientendposint + "'/Account/Login> click here</a>";

                            if (status == MembershipCreateStatus.Success)
                            {
                                //Create email alert and insert in the database
                                StringBuilder bodyBuilder = new StringBuilder();
                                bodyBuilder.Append("Dear Sir/Madam,");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.Append(string.Format("Welcome to {0}. Below you will find your login details.", DefaultSettings.Instance.ApplicationDisplayName));
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.Append("Username: " + customer.MobileNumber);
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.Append("Password: " + password);
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.Append(string.Format("Continue to approve your account and borrow {0}.", loginurl));
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");

                                var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
                                bodyBuilder.Append(emailSignature);

                                var emailAlertDTO = new EmailAlert
                                {
                                    MailMessageFrom = DefaultSettings.Instance.Email,
                                    MailMessageTo = string.Format("{0}", customer.EmailAddress),
                                    MailMessageSubject = string.Format("Login Details - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                    MailMessageBody = string.Format("{0}", bodyBuilder),
                                    MailMessageIsBodyHtml = true,
                                    MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                    MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                    MailMessageSecurityCritical = false,
                                    CreatedBy = "System",
                                    MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                };

                                emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                                NewTextAlert(customer.FullName, customer.MobileNumber, customer.MobileNumber, password, customer.ApprovalCode);
                                TempData["Message"] = "Account created successfully. An email/text message has been sent to you with your approval code. Provide the code after login to approve your account.";
                            }

                            switch (status)
                            {
                                case MembershipCreateStatus.Success:
                                    ModelState.AddModelError(string.Empty, "User Account created successfully");

                                    break;
                                case MembershipCreateStatus.DuplicateUserName:
                                    ModelState.AddModelError(string.Empty, "There already exists a user with this username.");
                                    break;

                                case MembershipCreateStatus.DuplicateEmail:
                                    ModelState.AddModelError(string.Empty, "There already exists a user with this email address.");
                                    break;
                                case MembershipCreateStatus.InvalidEmail:
                                    ModelState.AddModelError(string.Empty, "The email address you provided in invalid.");
                                    break;
                                case MembershipCreateStatus.InvalidAnswer:
                                    ModelState.AddModelError(string.Empty, "The security answer is invalid.");
                                    break;
                                case MembershipCreateStatus.InvalidPassword:
                                    ModelState.AddModelError(string.Empty, "The password you provided is invalid. It must be seven characters long and have at least one non-alphanumeric character.");

                                    break;
                                default:
                                    ModelState.AddModelError(string.Empty, status.ToString());
                                    break;
                            }
                        }

                        var findStaticSettingByKey = staticSettingService.FindStaticSettingByKey("CUSTOMERJOININGFEE");
                        //Post Joining fee to Income GL
                        if (customer.JoiningFee == true)
                        {
                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                            var findCustomerJoiningFeeGL = glMappingService.FindSystemGeneralLedgerAccountMappingsByGLCode((int)Enumerations.SystemGeneralLedgerAccountCode.CustomerJoiningFee);
                            var findDefaultSavingProduct = savingProductService.FindDefaultSavingsProduct();
                            #region Dr Saving GL, Cr Joining Fee GL
                            LoanTransactionHistory drSavingGL = new LoanTransactionHistory()
                            {                                
                                TransactionType = Enumerations.TransactionType.Debit,
                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                TransactionAmount = decimal.Parse(findStaticSettingByKey.Value),                                
                                TotalTransactionAmount = decimal.Parse(findStaticSettingByKey.Value),
                                ChartofAccountId = findDefaultSavingProduct.ChartofAccountId,
                                ContraAccountId = findCustomerJoiningFeeGL.ChartofAccountId,
                                CustomersAccountId = customersaccount.Id,
                                Description = "Customer Joining Fee",
                                CreatedBy = User.Identity.Name,
                                IsApproved = true,
                                ApprovedBy = string.Empty,
                                TransactionCategory = (int)Enumerations.TransactionCategory.CustomerJoiningFee,
                                BranchId = customer.BranchId,
                                TransactionBatchNumber = batchNo
                            };
                            
                            #endregion

                            #region Cr Saving GL, Dr Joining Fee GL
                            LoanTransactionHistory crJoiningFeeGL = new LoanTransactionHistory()
                            {                                
                                TransactionType = Enumerations.TransactionType.Credit,
                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                TransactionAmount = decimal.Parse(findStaticSettingByKey.Value),                                
                                TotalTransactionAmount = decimal.Parse(findStaticSettingByKey.Value),
                                ChartofAccountId = findCustomerJoiningFeeGL.ChartofAccountId,
                                ContraAccountId = findDefaultSavingProduct.ChartofAccountId,
                                CustomersAccountId = customersaccount.Id,
                                Description = "Customer Joining Fee",
                                CreatedBy = User.Identity.Name,
                                IsApproved = true,
                                ApprovedBy = string.Empty,
                                TransactionCategory = (int)Enumerations.TransactionCategory.CustomerJoiningFee,
                                BranchId = customer.BranchId,
                                TransactionBatchNumber = batchNo
                            };
                            using (var dataContext = new DataContext())
                            {
                                using (var transaction = dataContext.Database.BeginTransaction())
                                {
                                    try
                                    {
                                        customerservice.AddNewCustomer(customer, serviceHeader);
                                        customeraccountservice.AddNewCustomersAccount(customersaccount, serviceHeader);
                                        loantransactionhistory.AddNewLoanHistory(drSavingGL, serviceHeader);
                                        loantransactionhistory.AddNewLoanHistory(crJoiningFeeGL, serviceHeader);                                        
                                        transaction.Commit();
                                    }

                                    catch(Exception ex)
                                    {
                                        transaction.Rollback();
                                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                    }
                                }
                            }
                            
                            #endregion
                        }

                        else
                        {
                            customerservice.AddNewCustomer(customer, serviceHeader);
                            customeraccountservice.AddNewCustomersAccount(customersaccount, serviceHeader);
                        }
                    }
                        
                    else
                    {                        
                        ModelState.AddModelError(string.Empty, "An error has occurred while processing. Make sure the mobile number is unique.");
                        return View(customer);
                    }
                    
                    return RedirectToAction("Successful", "Customer");
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(customer);
                }
            }
            return View(customer);
        }

        private void NewTextAlert(string RecipientName, string RecepientNumber, string Username, string Password, string AuthorizationCode)
        {
            //Create email alert and insert in the database
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append(string.Format("Dear {0} ", RecipientName));
            bodyBuilder.Append(string.Format("Welcome to {0}. Your Username - {1}, Password - {2} ", ApplicationName, Username, Password));
            bodyBuilder.Append(TextSignature);
            bodyBuilder.Append("Thank you.");
            var textalert = new TextAlert
            {
                TextMessageRecipient = string.Format("{0}", RecepientNumber),
                TextMessageBody = string.Format("{0}", bodyBuilder),
                MaskedTextMessageBody = "",
                TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                TextMessageSecurityCritical = false,
                CreatedBy = User.Identity.Name,
                TextMessageSendRetry = 3
            };

            textalertservice.AddNewTextAlert(textalert, serviceHeader);
        }

        /// <summary>
        /// User account creation success page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Successful()
        {
            return View();
        }  
      
        [HttpGet]
        [Authorize]
        public ActionResult editcustomer(Guid Id)
        {
            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();
            ViewBag.GenderCheckList = genderInfoCheckList();
            ViewBag.IncomeRange = IncomeRangeSelectList();
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            var findcustomer = customerservice.FindCustomerById(Id);
            var currentUser = User.Identity.Name;
            var findCurrentUserDetials = userInfoService.FindUserByUserName(currentUser);
            Guid branchId = Guid.Empty;
            if (findCurrentUserDetials != null)
            {
                if (findCurrentUserDetials.Branch.BranchCode == "000")
                {
                    branchId = Guid.Empty;
                }
                else
                {
                    branchId = findCurrentUserDetials.BranchId;
                }
            }
            else
            {
                branchId = Guid.Empty;
            }
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(findcustomer.RelationshipManagerId.ToString(), branchId);
            var mobilenumber = findcustomer.MobileNumber;
            var idnumber = findcustomer.IDNumber;
            ViewBag.Mobilenumber = mobilenumber;
            ViewBag.IdNumber = idnumber;            
            return View(findcustomer);
        }

        [Authorize]        
        public ActionResult editcustomer(Customer customer)
        {
            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();
            ViewBag.GenderCheckList = genderInfoCheckList();
            ViewBag.IncomeRange = IncomeRangeSelectList();
            var currentUser = User.Identity.Name;
            var findCurrentUserDetials = userInfoService.FindUserByUserName(currentUser);
            var findcustomer = customerservice.FindCustomerById(customer.Id);
            Guid branchId = Guid.Empty;
            if (findCurrentUserDetials != null)
            {
                if (findCurrentUserDetials.Branch.BranchCode == "000")
                {
                    branchId = Guid.Empty;
                }
                else
                {
                    branchId = findCurrentUserDetials.BranchId;
                }
            }
            else
            {
                branchId = Guid.Empty;
            }
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(findcustomer.RelationshipManagerId.ToString(), branchId);
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            if (ModelState.IsValid)
            {                
                var mobilenumber = findcustomer.MobileNumber;
                var idnumber = findcustomer.IDNumber;
                findcustomer.FirstName = customer.FirstName.ToUpper();
                findcustomer.LastName = customer.LastName.ToUpper();
                findcustomer.EmailAddress = customer.EmailAddress;
                findcustomer.Occupation = customer.Occupation;                
                findcustomer.Gender = customer.Gender;
                findcustomer.BranchId = customer.BranchId;
                findcustomer.MaritalStatus = customer.MaritalStatus;
                findcustomer.IncomeRange = customer.IncomeRange;
                findcustomer.MiddleName = customer.MiddleName.ToUpper();
                findcustomer.MobileNumber = customer.MobileNumber;
                findcustomer.IDNumber = customer.IDNumber;
                findcustomer.RelationshipManagerId = customer.RelationshipManagerId;
                findcustomer.KRAPIN = customer.KRAPIN;
                ViewBag.Mobilenumber = mobilenumber;
                ViewBag.IdNumber = idnumber;
                if (customer.BranchId == Guid.Empty)
                {
                    ModelState.AddModelError(string.Empty, "Branch cannot be null.");
                    return View();
                }

                bool checkduplicatecustomer = customerservice.CheckDuplicateCustomer(customer.MobileNumber, customer.IDNumber);
                if (customer.BranchId == Guid.Empty)
                {
                    ModelState.AddModelError(string.Empty, "Branch cannot be null.");
                    return View(customer);
                }

                if (string.IsNullOrWhiteSpace(string.Concat(customer.CustomerType)))
                {
                    ModelState.AddModelError(string.Empty, "Customer Type cannot be null.");
                    return View(customer);
                }
               
                bool iscustomerunderage = customerservice.ValidateCustomerAge(customer.DateofBirth);
                if (!iscustomerunderage)
                {
                    ModelState.AddModelError(string.Empty, string.Format("You must be 18 years and above."));

                    return View(customer);
                }

                else if (customer.IncomeRange.Equals("Select Income Range"))
                {
                    ModelState.AddModelError(string.Empty, string.Format("Income range is required."));

                    return View(customer);
                }

                try
                {
                    customerservice.UpdateCustomer(findcustomer, serviceHeader);
                    if (User.IsInRole("Customer"))
                    {
                        return RedirectToAction("CustomerProfile");
                    }

                    else
                    {
                        return RedirectToAction("Index");
                    }
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                }
            }
            return View(customer);
        }

        [Authorize]
        public ActionResult customerdetails(Guid Id)
        {
            var findcutomerbyid = customerservice.FindCustomerById(Id);

            return View(findcutomerbyid);
        }

        public ActionResult CustomerProfile()
        {
            var customerdetials = customerservice.FindCustomerByMobileNumber(User.Identity.Name);

            return View(customerdetials);
        }
        [Authorize]
        public ActionResult customer()
        {
            return View();
        }

        public JsonResult getcustomerbyid(JQueryDataTablesModel datatablemodel)
        {            
            int totalrecordcount = 0;

            int searchedrecordcount = 0;

            var sortAscending = datatablemodel.sSortDir_.First() == "asc" ? true : false;

            var sortedcolumns = (from s in datatablemodel.GetSortedColumns() select s.PropertyName).ToList();

            var pagecollection = customerservice.GetCustomerByMobileNumber(User.Identity.Name);

            if (pagecollection != null)
            {
                totalrecordcount = pagecollection.Count();

                searchedrecordcount = !string.IsNullOrWhiteSpace(datatablemodel.sSearch) ? pagecollection.Count() : totalrecordcount;
            }

            var result = Extensions.ControllerExtensions.DataTablesJson(this, items: pagecollection, totalRecords: totalrecordcount, totalDisplayRecords: searchedrecordcount, sEcho: datatablemodel.sEcho);

            return result;
        }   
    
        [HttpGet]
        [Authorize]
        public ActionResult ApproveCustomer()
        {
            return View();
        }

        [HttpPost]        
        public ActionResult ApproveCustomer(CustomerVerification customerverification)
        {
            try
            {  
                if(ModelState.IsValid)
                {
                    Customer findcustomerbyapprovalcode = customerservice.FindCustomerByApprovalCode(customerverification.ApprovalCode); 

                    if(findcustomerbyapprovalcode == null)
                    {
                        ModelState.AddModelError(string.Empty, "Verification Code is incorrect, please try again.");
                        return View(customerverification);
                    }

                    else
                    {
                        findcustomerbyapprovalcode.IsApproved = true;
                        customerservice.UpdateCustomer(findcustomerbyapprovalcode, serviceHeader);

                        return RedirectToAction("Dashboard", "Customer");
                    }
                }                
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }            
            return View(customerverification);
        }        
       

        [HttpGet]
        [Authorize]
        public ActionResult Dashboard()
        {
            var paybillnumber = ConfigurationManager.AppSettings["PaybillNumber"].ToString();
            ViewBag.PaybillNumber = paybillnumber;
            return View();
        }

        public ActionResult CustomerLookUpView(int? page)
        {
            var pagecollection = customerservice.GetAllCustomers().ToArray();
            ViewBag.Users = pagecollection.AsPagination(page ?? 1, 10);
            ViewBag.CustomerList = pagecollection;
            return PartialView("_CustomerLoopUp");
        }

        public ActionResult AccountStatement()
        {
            return View();
        }

        public ActionResult AccountStatementView(string id)
        {            
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException("id cannot be null");
            }

            string customerName = string.Empty;
            var findCustomerById = customerservice.FindCustomerById(Guid.Parse(id));
            if (findCustomerById != null)
            {
                customerName = findCustomerById.FullNamemobileNumber;
            }

            ViewBag.Id = id;
            ViewBag.CustomerName = customerName;
            return View();            
        }

        public JsonResult GetCustomerAccountStatementView(string id, JQueryDataTablesModel dataTableModel)
        {
            if(string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException("id cannot be null");
            }
            var customer = new Customer();
            var customerId = Guid.Parse(id);
            if(customerId != Guid.Empty)
            {
                customer = customerservice.FindCustomerById(customerId);
            }
            var pagecollection = customerservice.CustomerAccountStatement(id, customer.CreatedDate, DateTime.Now);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetCustomerAccountBalance(startIndex: dataTableModel.iDisplayStart,
                pageSize: dataTableModel.iDisplayLength, sortedColumns: dataTableModel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: dataTableModel.sSearch, accountStatement: pagecollection);


            return Json(new JQueryDataTablesResponse<CustomerAccountStatement>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: dataTableModel.sEcho));
        }

        public static IList<CustomerAccountStatement> GetCustomerAccountBalance(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<CustomerAccountStatement> accountStatement)
        {
            var GetaccountStatmentList = accountStatement;

            totalRecordCount = GetaccountStatmentList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetaccountStatmentList = GetaccountStatmentList.Where(c => c.AccountName.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetaccountStatmentList.Count;

            IOrderedEnumerable<CustomerAccountStatement> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "TrxDate":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.TrxDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TrxDate);
                        break;

                    case "AccountName":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.AccountName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.AccountName);
                        break;

                    case "credit":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.credit)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.credit);
                        break;

                    case "debit":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.debit)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.debit);
                        break;

                    case "id":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.id)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.id);
                        break;

                    case "IntBalance":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.IntBalance)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IntBalance);
                        break;

                    case "InterestCR":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.InterestCR)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestCR);
                        break;

                    case "InterestDR":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.InterestDR)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestDR);
                        break;

                    case "PrimaryDescription":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.PrimaryDescription)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.PrimaryDescription);
                        break;

                    case "Product":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.Product)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Product);
                        break;

                    case "reference":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.reference)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.reference);
                        break;

                    case "RunningTotal":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.RunningTotal)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.RunningTotal);
                        break;

                    case "SecondaryDescription":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.SecondaryDescription)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.SecondaryDescription);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }


        public ActionResult CustomerSavingsAccountTransations()
        {
            return View();
        }

        /// <summary>
        /// Random approval cod generator
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private string GenerateRandomCustomerApprovalCode(int length)
        {
            string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[length];
            Random rd = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }

        [NonAction]
        public List<SelectListItem> BranchesSelectList(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultvalue = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Branch", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultvalue);

            var otherlist = branchservice.GetAllBranches().Where(x => x.IsEnabled == true);

            foreach (var item in otherlist)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.BranchName, Value = item.Id.ToString("D") });

            return SelectList;
        }

        [NonAction]
        public List<SelectListItem> GetAllRelationshipInList(string selectedValue, Guid BranchId)
        {
            List<SelectListItem> returnList = new List<SelectListItem>();
            if (string.IsNullOrWhiteSpace(selectedValue))
            {
                selectedValue = Guid.Empty.ToString();
            }
            var findAllRM = new List<RelationshipManager>();

            var defaultRM = new SelectListItem { Selected = (selectedValue == Guid.Empty.ToString("D")), Text = "Select Relationship Manager", Value = Guid.Empty.ToString("D") };

            returnList.Add(defaultRM);

            if (BranchId == Guid.Empty)
            {
                findAllRM = relationshipManagerService.GetAllRelationshipManagers().ToList();
            }
            else
            {
                findAllRM = relationshipManagerService.FindRelationshipManagerByBranchId(BranchId);
            }              

            foreach (var RM in findAllRM)
                returnList.Add(new SelectListItem { Selected = RM.Id == Guid.Parse(selectedValue), Text = RM.RMFullName, Value = RM.Id.ToString("D") });

            return returnList;
        }

        [NonAction]
        public List<SelectListItem> GetAllRelationshipManagersByBranchIdAndRelationshipManagerId(string selectedValue, Guid BranchId)
        {
            List<SelectListItem> returnList = new List<SelectListItem>();

            var findAllRM = new List<RelationshipManager>();

            var defaultRM = new SelectListItem { Selected = (selectedValue == Guid.Empty.ToString("D")), Text = "Select Relationship Manager", Value = Guid.Empty.ToString("D") };

            returnList.Add(defaultRM);
            var rmId = Guid.Parse(selectedValue);
            findAllRM = relationshipManagerService.FindRelationshipManagerByBranchIdAndRelationshipManagerId(BranchId, rmId).ToList();

            foreach (var RM in findAllRM)
                returnList.Add(new SelectListItem { Selected = RM.Id == Guid.Parse(selectedValue), Text = RM.RMFullName, Value = RM.Id.ToString("D") });

            return returnList;
        }

        [HttpGet]
        public ActionResult TransferCustomers()
        {
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(Guid.Empty.ToString(), Guid.Empty);
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            return View();
        }

        [HttpPost]
        public ActionResult TransferCustomers([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Customer> customer, [Bind(Prefix = "xId")]string targetBranchId, string Name2)
        {
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(Guid.Empty.ToString(), Guid.Empty);
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            return View();                
        }

        public ActionResult FilterMenuCustomization_Read([DataSourceRequest] DataSourceRequest request)
        {            
            return Json(customerservice.GetAllCustomers().ToDataSourceResult(request));
        }

        public ActionResult FilterMenuCustomization_RMs()
        {
            var rmList = relationshipManagerService.GetAllRelationshipManagers();
            return Json(rmList.Select(e => e.LastName).Distinct(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilterMenuCustomization_Branch()
        {
            var rmList = branchservice.GetAllBranches();
            return Json(rmList.Select(e => e.BranchName).Distinct(), JsonRequestBehavior.AllowGet);
        } 
	}    
}
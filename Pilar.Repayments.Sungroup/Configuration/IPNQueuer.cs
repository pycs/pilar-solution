﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.IPNService;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Repayments.Sungroup.Configuration
{
    public class IPNQueuer : IJob
    {
        private readonly IMessageQueueService _messageQueueService;
        private readonly IIPNService _iipnservice;
        private readonly string _messageRelayQueuePath;

        public IPNQueuer()
        {
            _messageQueueService = new MessageQueueService();
            _iipnservice = new IPNService();
            _messageRelayQueuePath = ConfigurationManager.AppSettings["C2BQueuePath"];
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var findpendingapprovalc2b = _iipnservice.FindC2BTransactionByStatus((int)Enumerations.C2BTransactionStatus.Pending).Take(100);

                if (findpendingapprovalc2b != null && findpendingapprovalc2b.Any())
                    foreach (var item in findpendingapprovalc2b)
                    {
                        _messageQueueService.Send(_messageRelayQueuePath, item.Id, Enumerations.MessageCategory.C2BConfirmation, MessagePriority.High);
                        item.Status = (int)Enumerations.C2BTransactionStatus.Queued;
                        _iipnservice.UpdateC2BTransaction(item);
                    }
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("IPNConfirmationQueueingJob.Execute..." + ex);
            }
        }
    }
}

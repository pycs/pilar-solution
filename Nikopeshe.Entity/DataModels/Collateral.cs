﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Entity.DataModels
{
    public class Collateral : BaseEntity
    {
        [Index]
        public Guid? LoanRequestId { get; set; }

        public virtual LoanRequest LoanRequest { get; set; }

        [Display(Name = "Description")]
        [Column(TypeName = "VARCHAR")]
        public string Description { get; set; }

        [Display(Name = "Amount")]
        public decimal Amount { get; set; }

    }
}

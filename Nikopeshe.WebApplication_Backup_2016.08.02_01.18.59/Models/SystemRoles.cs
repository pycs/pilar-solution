﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nikopeshe.WebApplication.Models
{
    public class SystemRoles
    {
        public string RoleName { get; set; }

        public string Actions { get; set; }
    }
}
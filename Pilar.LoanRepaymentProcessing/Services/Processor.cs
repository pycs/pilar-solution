﻿using Crosscutting.NetFramework.Logging;
using Infrastructure.Crosscutting.NetFramework;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.Charge;
using Pilar.Services.Services.CustomerService;
using Pilar.Services.Services.GraduatedScale;
using Pilar.Services.Services.IPNService;
using Pilar.Services.Services.Repayments;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Messaging;

namespace Pilar.LoanRepaymentProcessing.Services
{
    [Export(typeof(IPlugin))]
    public class Processor : IPlugin
    {
        private MessageQueue _messageQueue;
        private readonly string _queuePath;
        private readonly IChargeService chargeService;
        private readonly IRepayment irepayment;      
        private readonly IIPNService _iipnservice;
        private readonly ICustomerService _customerservice;
        private readonly IGraduatedScaleService graduatedScaleService;
        string emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
        [ImportingConstructor]
        public Processor()
        {
            _queuePath = ConfigurationManager.AppSettings["LoanProcessingQueuePath"];
            irepayment = new Repayment();
            _iipnservice = new IPNService();
            chargeService = new ChargeService();
            graduatedScaleService = new GraduatedScaleService();
            _customerservice = new CustomerService();
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{124768CE-BFB6-47F9-8ECD-89B536F0F038}"); }
        }

        public string Description
        {
            get { return "LOANREPAYMENT.PROCESSOR"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                if (!MessageQueue.Exists(_queuePath))
                    throw new ArgumentException("Queue does not exist at specified path", "_queuePath");
                else
                {
                    // initialize message queue
                    _messageQueue = new MessageQueue(_queuePath);

                    // enable the AppSpecific field in the messages
                    _messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                    // set the formatter to binary.
                    _messageQueue.Formatter = new BinaryMessageFormatter();

                    // set an event to listen for new messages.
                    _messageQueue.ReceiveCompleted += MessageQueue_ReceiveCompleted;

                    // start listening
                    _messageQueue.BeginReceive();
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        public void Exit()
        {
            _messageQueue.Close();
        }

        #endregion

        #region Event Handlers
        private void MessageQueue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                // retrieve message category
                var messageCategory = (Nikopeshe.Entity.DataModels.Enumerations.MessageCategory)e.Message.AppSpecific;
                var recordId = (Guid)e.Message.Body;                 
                switch (messageCategory)
                {
                    case Nikopeshe.Entity.DataModels.Enumerations.MessageCategory.LoanProcessing:

                        #region Loan Processing
                        using (var datacontext = new DataContext())
                        {
                            using (var transaction = datacontext.Database.BeginTransaction())
                            {
                                try
                                {
                                    var findLoanRequestById = datacontext.LoanRequest.Find(recordId);
                                    if ((findLoanRequestById != null && findLoanRequestById.NextPaymentDate.Value.Date == DateTime.Today.Date) || findLoanRequestById.LoanProduct.IsMicroLoan == true)
                                    {
                                        //find m-pesa repayment gl account id 
                                        var findmpesarepaymentaccountid = datacontext.SystemGeneralLedgerAccountMapping.Where(x => x.SystemGeneralLedgerAccountCode == Enumerations.SystemGeneralLedgerAccountCode.MPesaRepaymentAccount).FirstOrDefault();
                                        var findmpessuspenseaccountid = datacontext.SystemGeneralLedgerAccountMapping.Where(x => x.SystemGeneralLedgerAccountCode == Enumerations.SystemGeneralLedgerAccountCode.MPesaSuspsnseAccount).FirstOrDefault();
                                        var finddefaultsavingaccountid = datacontext.SavingsProduct.Where(x => x.IsDefault == true).FirstOrDefault();
                                        var findcustomerbymobilenumber = _customerservice.FindCustomerByMobileNumber(findLoanRequestById.PrimaryAccountNumber);
                                        var findStaticSetting = datacontext.StaticSetting.Where(x => x.Key == "CREDITSAVINGSACCOUNT").FirstOrDefault();
#if DEBUG                                        
                                        LoggerFactory.CreateLog().LogInfo("M-Pesa Suspense GL Account " + findmpessuspenseaccountid.ToString());
                                        LoggerFactory.CreateLog().LogInfo("M-Pesa Repayment GL " + findmpesarepaymentaccountid.ToString());
                                        LoggerFactory.CreateLog().LogInfo("Default Savings Account Id " + finddefaultsavingaccountid.ToString());
                                        LoggerFactory.CreateLog().LogInfo("Static Settings " + findStaticSetting.Value);                                        
#endif
                                        try
                                        {
                                            decimal expectedinterestamount = 0m;                                            
                                            decimal receivedInterestAmount = 0m;
                                            decimal expectedRecurringCharges = 0m;
                                            decimal receivedRecurringCharges = 0m;
                                            decimal principalPerInstallment = 0m;
                                            decimal totalPrincipal = 0m;
                                            decimal totalRecurringCharges = 0m;
                                            decimal TransactionAmount = 0m;
                                            decimal Amount = 0m;
                                            decimal totalloanamount = 0m;
                                            decimal totalTransactionAmount = 0m;
                                            decimal findCustomerSavingsAccountBalance = 0m;
                                            int principalRecoveryMode = (int)findLoanRequestById.LoanProduct.PrincipalRecoveryMethod;
                                            //find customer using mobile number

                                            var findcustomersavingsaccountid = datacontext.CustomersAccount.Where(x => x.CustomerId == findLoanRequestById.CustomerAccount.CustomerId && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();

                                            if (findcustomersavingsaccountid != null)
                                            {
                                                var customerAccountDetails = _iipnservice.CustomerSavingsAccountBalance(findcustomersavingsaccountid.Id);
                                                findCustomerSavingsAccountBalance = customerAccountDetails.AccountBalance;

                                                if(findCustomerSavingsAccountBalance > 0m)
                                                {                                                    
                                                    var PaidPrincipalInterestCharges = _iipnservice.PaidPrincipalPaidInterestPaidCharges(findLoanRequestById.Id, findcustomersavingsaccountid.Id, findLoanRequestById.LoanProductId);
                                                    var ExpectedTotalPrincipalInterestCharges = _iipnservice.ExpectedTotalPrincipalExpectedTotalInterestExpectedTotalCharges(findLoanRequestById.Id);                                                    
                                                    var findloanproductbyid = datacontext.LoanProduct.Find(findLoanRequestById.LoanProductId);
                                                    var interesrratepermonth = findloanproductbyid.InterestValue / 12;
                                                    TransactionAmount = findCustomerSavingsAccountBalance;

                                                    totalPrincipal = (ExpectedTotalPrincipalInterestCharges.Item1 + ExpectedTotalPrincipalInterestCharges.Item2 + ExpectedTotalPrincipalInterestCharges.Item3) / findLoanRequestById.NegotiatedInstallments;
                                                    if (TransactionAmount > totalPrincipal)
                                                    {
                                                        Amount = totalPrincipal;
                                                    }
                                                    else
                                                    {
                                                        Amount = TransactionAmount;
                                                    }
                                                    totalTransactionAmount = totalPrincipal;
                                                    #region Recover Interest
                                                    if (findloanproductbyid.InterestChargeType == (int)Enumerations.InterestChargeType.Fixed)
                                                    {
                                                        expectedinterestamount = findloanproductbyid.InterestValue;
                                                        principalPerInstallment = findLoanRequestById.LoanAmount / findLoanRequestById.NegotiatedInstallments;
                                                    }
                                                    else if (findloanproductbyid.InterestChargeType == (int)Enumerations.InterestChargeType.Percentage)
                                                    {
                                                        expectedinterestamount = findLoanRequestById.InterestAmount / findLoanRequestById.NegotiatedInstallments;
                                                        principalPerInstallment = findLoanRequestById.LoanAmount / findLoanRequestById.NegotiatedInstallments;
                                                    }

                                                    if (PaidPrincipalInterestCharges.Item2 <= ExpectedTotalPrincipalInterestCharges.Item2 && TransactionAmount > 0m)
                                                    {
                                                        decimal interestBalance = ExpectedTotalPrincipalInterestCharges.Item2 - PaidPrincipalInterestCharges.Item2;

                                                        if (TransactionAmount >= expectedinterestamount)
                                                        {
                                                            if (interestBalance >= expectedinterestamount)
                                                            {
                                                                receivedInterestAmount = expectedinterestamount;
                                                            }
                                                            else
                                                            {
                                                                receivedInterestAmount = interestBalance;
                                                            }
                                                        }

                                                        else
                                                        {
                                                            receivedInterestAmount = TransactionAmount;
                                                        }

                                                        TransactionAmount = TransactionAmount - receivedInterestAmount;
                                                        int batchNo = RandomBatchNumberGenerator.GetRandomNumber();

                                                        if (receivedInterestAmount > 0)
                                                        {
                                                            LoanTransactionHistory debitsavingsgl = new LoanTransactionHistory()
                                                            {
                                                                LoanRequestId = findLoanRequestById.Id,
                                                                TransactionType = Enumerations.TransactionType.Debit,
                                                                RepaymentType = (int)Enumerations.RepaymentType.MPesa,
                                                                TransactionAmount = receivedInterestAmount,
                                                                TotalTransactionAmount = totalTransactionAmount,
                                                                ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                ContraAccountId = Guid.Parse(findLoanRequestById.LoanProduct.InterestGLAccountId.ToString()),
                                                                CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                Description = string.Format("Interest Liquidation"),
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                BranchId = findLoanRequestById.BranchId,
                                                                TransactionBatchNumber = batchNo
                                                            };

                                                            LoanTransactionHistory creditincomegl = new LoanTransactionHistory()
                                                            {
                                                                LoanRequestId = findLoanRequestById.Id,
                                                                TransactionType = Enumerations.TransactionType.Credit,
                                                                RepaymentType = (int)Enumerations.RepaymentType.MPesa,
                                                                TransactionAmount = receivedInterestAmount,
                                                                TotalTransactionAmount = totalTransactionAmount,
                                                                ChartofAccountId = findLoanRequestById.LoanProduct.InterestGLAccountId,
                                                                ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                CustomersAccountId = findLoanRequestById.CustomerAccountId,
                                                                Description = string.Format("Interest Liquidation"),
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                BranchId = findLoanRequestById.BranchId,
                                                                TransactionBatchNumber = batchNo
                                                            };

                                                            datacontext.LoanTransactionHistory.Add(debitsavingsgl);
                                                            datacontext.LoanTransactionHistory.Add(creditincomegl);
                                                        }
                                                    }
                                                    #endregion

                                                    #region Recover Recurring Charges
                                                    if (PaidPrincipalInterestCharges.Item3 < ExpectedTotalPrincipalInterestCharges.Item3)
                                                    {
                                                        var findProductRecurringCharges = _iipnservice.GetRecurringLoanProductChargeByProductId(findLoanRequestById.LoanProductId);
                                                        if (findProductRecurringCharges.Count != 0 && findProductRecurringCharges.Any())
                                                        {
                                                            foreach (var recurringCharge in findProductRecurringCharges)
                                                            {
                                                                var findRecurringCharges = chargeService.FindChargeById(recurringCharge.ChargeId);
                                                                if (findProductRecurringCharges != null)
                                                                {
                                                                    var findGraduatedScaleByChargeId = graduatedScaleService.FindGraduatedScaleUsingRangeAndChargeId(Amount, findRecurringCharges.Id);
                                                                    if (findGraduatedScaleByChargeId != null)
                                                                    {
                                                                        foreach (var graduatedScale in findGraduatedScaleByChargeId)
                                                                        {
                                                                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                                            var chargeType = graduatedScale.ChargeType;
                                                                            if (chargeType == Enumerations.ChargeTypes.FixedAmount)
                                                                            {
                                                                                expectedRecurringCharges = graduatedScale.Value;
                                                                                totalRecurringCharges = graduatedScale.Value * findLoanRequestById.NegotiatedInstallments;
                                                                            }

                                                                            else
                                                                            {
                                                                                expectedRecurringCharges = (graduatedScale.Value * Amount) / 100;
                                                                                totalRecurringCharges = ((graduatedScale.Value * Amount) / 100) * findLoanRequestById.NegotiatedInstallments;
                                                                            }

                                                                            if (TransactionAmount >= expectedRecurringCharges)
                                                                            {
                                                                                receivedRecurringCharges = expectedRecurringCharges;
                                                                            }
                                                                            else
                                                                            {
                                                                                receivedRecurringCharges = TransactionAmount;
                                                                            }
                                                                            //TransactionAmount = TransactionAmount - receivedRecurringCharges;

                                                                            if (receivedRecurringCharges > 0m && TransactionAmount > 0m)
                                                                            {
                                                                                LoanTransactionHistory postChargeDr = new LoanTransactionHistory()
                                                                                {
                                                                                    LoanRequestId = findLoanRequestById.Id,
                                                                                    TransactionType = Enumerations.TransactionType.Debit,
                                                                                    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                                    TransactionAmount = receivedRecurringCharges,
                                                                                    TotalTransactionAmount = totalTransactionAmount,
                                                                                    ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                                    ContraAccountId = graduatedScale.Charge.ChartofAccountId,
                                                                                    CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                                    Description = string.Format("Charge Payment"),
                                                                                    CreatedBy = "System",
                                                                                    IsApproved = true,
                                                                                    ApprovedBy = "System",
                                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                                    BranchId = findLoanRequestById.BranchId,
                                                                                    TransactionBatchNumber = batchNo
                                                                                };
                                                                                datacontext.LoanTransactionHistory.Add(postChargeDr);

                                                                                LoanTransactionHistory postChargeCr = new LoanTransactionHistory()
                                                                                {
                                                                                    LoanRequestId = findLoanRequestById.Id,
                                                                                    TransactionType = Enumerations.TransactionType.Credit,
                                                                                    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                                    TransactionAmount = receivedRecurringCharges,
                                                                                    TotalTransactionAmount = totalTransactionAmount,
                                                                                    ChartofAccountId = graduatedScale.Charge.ChartofAccountId,
                                                                                    ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                                    CustomersAccountId = findLoanRequestById.CustomerAccountId,
                                                                                    Description = string.Format("Charge Payment"),
                                                                                    CreatedBy = "System",
                                                                                    IsApproved = true,
                                                                                    ApprovedBy = "System",
                                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                                    BranchId = findLoanRequestById.BranchId,
                                                                                    TransactionBatchNumber = batchNo
                                                                                };
                                                                                datacontext.LoanTransactionHistory.Add(postChargeCr);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion

                                                    

                                                    #region Recover Expected Principal Amount
                                                    if (PaidPrincipalInterestCharges.Item1 < ExpectedTotalPrincipalInterestCharges.Item1)
                                                    {
                                                        if (principalRecoveryMode == (int)Enumerations.PrincipalRecoveryMethod.PerInstallment)
                                                        {
                                                            if (TransactionAmount < principalPerInstallment)
                                                            {
                                                                principalPerInstallment = TransactionAmount;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            principalPerInstallment = TransactionAmount;
                                                        }

                                                        if (principalPerInstallment > 0)
                                                        {
                                                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                            LoanTransactionHistory debitsavingsgl = new LoanTransactionHistory()
                                                            {
                                                                LoanRequestId = findLoanRequestById.Id,
                                                                TransactionType = Enumerations.TransactionType.Debit,
                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                TransactionAmount = principalPerInstallment,
                                                                TotalTransactionAmount = totalTransactionAmount,
                                                                ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                ContraAccountId = Guid.Parse(findloanproductbyid.LoanGLAccountId.ToString()),
                                                                CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                Description = string.Format("Loan Liquidation"),
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                BranchId = findLoanRequestById.BranchId,
                                                                TransactionBatchNumber = batchNo
                                                            };
                                                            datacontext.LoanTransactionHistory.Add(debitsavingsgl);

                                                            LoanTransactionHistory creditloangl = new LoanTransactionHistory()
                                                            {
                                                                LoanRequestId = findLoanRequestById.Id,
                                                                TransactionType = Enumerations.TransactionType.Credit,
                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                TransactionAmount = principalPerInstallment,
                                                                TotalTransactionAmount = totalTransactionAmount,
                                                                ChartofAccountId = findloanproductbyid.LoanGLAccountId,
                                                                ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                CustomersAccountId = findLoanRequestById.CustomerAccountId,
                                                                Description = string.Format("Loan Liquidation"),
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                BranchId = findLoanRequestById.BranchId,
                                                                TransactionBatchNumber = batchNo
                                                            };
                                                            datacontext.LoanTransactionHistory.Add(creditloangl);
                                                        }
                                                    }
                                                    #endregion

                                                    var frequency = findloanproductbyid.PaymentFrequencyType;
                                                    decimal expectedPrincipal = ExpectedTotalPrincipalInterestCharges.Item1 / findLoanRequestById.NegotiatedInstallments;
                                                    decimal expectedInstallmentAmount = expectedPrincipal + expectedRecurringCharges + expectedinterestamount;
                                                    decimal receivedInstallmentAmount = principalPerInstallment + receivedRecurringCharges + receivedInterestAmount;
                                                    if (receivedInstallmentAmount >= expectedInstallmentAmount)
                                                    {
                                                        if (frequency == Enumerations.PaymentFrequency.Weekly)
                                                        {
                                                            findLoanRequestById.NextPaymentDate = findLoanRequestById.NextPaymentDate.Value.AddDays(7);                                                            
                                                        }
                                                        else
                                                        {
                                                            findLoanRequestById.NextPaymentDate = findLoanRequestById.NextPaymentDate.Value.AddMonths(1);                                                            
                                                        }                                                                                                                
                                                    }

                                                    var updateloan = datacontext.Entry(findLoanRequestById);
                                                    updateloan.State = EntityState.Modified;
                                                    
                                                    #region Check if loan has been paid in full
                                                    var findloantransactionsbyloanrequestid = new List<LoanTransactionHistory>();
                                                    findloantransactionsbyloanrequestid = datacontext.LoanTransactionHistory.Where(x => x.LoanRequestId == findLoanRequestById.Id
                                                    && x.IsApproved == true && x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment
                                                    && x.TransactionType == Enumerations.TransactionType.Credit && x.CustomersAccountId == findLoanRequestById.CustomerAccountId && x.ChartofAccountId == findloanproductbyid.LoanGLAccountId).ToList();
                                                    var totalpaidamount = 0m;
                                                    
                                                    if (principalRecoveryMode == (int)Enumerations.PrincipalRecoveryMethod.PerInstallment)
                                                    {
                                                        totalpaidamount = PaidPrincipalInterestCharges.Item1 + receivedRecurringCharges + receivedInterestAmount + principalPerInstallment + PaidPrincipalInterestCharges.Item2 + PaidPrincipalInterestCharges.Item3;
                                                        totalloanamount = findLoanRequestById.LoanAmount + findLoanRequestById.InterestAmount + totalRecurringCharges;
                                                    }
                                                    else
                                                    {
                                                        totalpaidamount = PaidPrincipalInterestCharges.Item1 + principalPerInstallment;
                                                        totalloanamount = findLoanRequestById.LoanAmount;
                                                    }
                                                    
                                                    var totalbalance = (totalloanamount) - totalpaidamount;

                                                    if (totalpaidamount >= totalloanamount && totalbalance <= 0)
                                                    {
                                                        findLoanRequestById.Status = (int)Enumerations.LoanStatus.Cleared;
                                                        var updateloanstatus = datacontext.Entry(findLoanRequestById);
                                                        updateloanstatus.State = EntityState.Modified;
                                                    }

                                                    #endregion
                                                }
                                            }
                                        }
                                        catch(Exception ex)
                                        {
                                            LoggerFactory.CreateLog().LogError("{0} -> MessageQueue_ReceiveCompleted...", ex);
                                        }
                                    }

                                    datacontext.SaveChanges();
                                    transaction.Commit();
                                }

                                catch (Exception ex)
                                {
                                    transaction.Rollback();
                                    LoggerFactory.CreateLog().LogError("{0} -> MessageQueue_ReceiveCompleted...", ex);
                                }

                                finally
                                {
                                    _messageQueue.BeginReceive();
                                }
                            }
                        }
                        #endregion

                        break;
                }
            }

            catch(Exception ex)
            {
                LoggerFactory.CreateLog().LogError("{0} -> MessageQueue_ReceiveCompleted... ", ex);
            }
        }
    }                        
}
        #endregion
﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Nikopeshe.Services.InterfaceImplementations;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class DashboardController : Controller
    {
        private readonly IDashBoardService dashboardservice;
        private readonly ILoanRequestService loanrequestservice;
        private readonly IGroupService groupservice;
        private readonly ICustomerService customerservice;
        private readonly UserInfoService userInfo;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public DashboardController(IDashBoardService _dashboardservice,
            ILoanRequestService _loanrequestservice, IGroupService _groupservice, ICustomerService _customerservice, UserInfoService _userInfo)
        {
            if(_dashboardservice == null || _loanrequestservice == null)
                throw new ArgumentNullException("null service reference");

            this.dashboardservice = _dashboardservice;
            this.loanrequestservice = _loanrequestservice;
            this.groupservice = _groupservice;
            this.customerservice = _customerservice;
            this.userInfo = _userInfo;
        }       

        public ActionResult Dashboard()
        {
            var getCurrentUser = User.Identity.Name;
            var findUserInfo = userInfo.FindUserByUserName(getCurrentUser);
            decimal totalactiveloansamount = 0m;
            decimal totalInterestAmount = 0m;            
            int duetodayloans = 0;      
            int totalActiveLoanCount = 0;      
            if (findUserInfo != null && findUserInfo.Branch.BranchCode != "000")
            {
                Tuple<decimal, decimal, int, int> results = loanrequestservice.SumAllActiveLoansAmountTotalInterest(findUserInfo.BranchId);                
                totalactiveloansamount = results.Item1;
                totalActiveLoanCount = results.Item3;
                duetodayloans = results.Item4;
                totalInterestAmount = results.Item2;
            }
            else
            {
                Tuple<decimal, decimal, int, int> results = loanrequestservice.SumAllActiveLoansAmountTotalInterest(Guid.Empty);                
                totalactiveloansamount = results.Item1;
                totalActiveLoanCount = results.Item3;
                duetodayloans = results.Item4;
                totalInterestAmount = results.Item2;
            }

            var distinctGroupCountyCodes = groupservice.GetDistinctCountyCodes();
            var GroupCounties = new List<string>();
            var NumberOfGroupsPerCounty = new List<double>();
            foreach (string countyCode in distinctGroupCountyCodes)
            {
                BaseController baseC = new BaseController();
                int count = groupservice.FindGroupCountByCountyCode(countyCode);
                string countyName = baseC.GetCountyName(countyCode);
                GroupCounties.Add(countyName);
                NumberOfGroupsPerCounty.Add(count);
            }

            var distinctCustomerCountyCodes = customerservice.GetDistinctCountyCodes();
            var CustomerCounties = new List<string>();
            var NumberOfCustomersPerCounty = new List<double>();
            foreach (string countyCode in distinctCustomerCountyCodes)
            {
                if(string.IsNullOrEmpty(countyCode)){
                
                }
                else
                {
                    BaseController baseC = new BaseController();
                    int count = customerservice.FindCustomerCountByCountyCode(countyCode);
                    string countyName = baseC.GetCountyName(countyCode);
                    CustomerCounties.Add(countyName);
                    NumberOfCustomersPerCounty.Add(count);
                }
            }

            ViewBag.ActiveLoanCount = totalActiveLoanCount;
            ViewBag.LoanDueToday = duetodayloans;
            ViewBag.TotalActiveLoansAmount = totalactiveloansamount;
            ViewBag.InterestCount = totalInterestAmount;
            ViewBag.TotalGroup = groupservice.GetAllGroup(IsolationLevel.ReadUncommitted).Count();
            ViewBag.TotalCustomer = customerservice.GetAllCustomers(IsolationLevel.ReadUncommitted).Where(c => c.CustomerType == Enumerations.CustomerTypes.Individual).Count();
            ViewBag.GroupCountiesArray = GroupCounties.ToArray();
            ViewBag.NumberOfGroups = NumberOfGroupsPerCounty.ToArray();
            ViewBag.CustomerCountiesArray = CustomerCounties.ToArray();
            ViewBag.NumberOfCustomers = NumberOfCustomersPerCounty.ToArray();

            if (NumberOfGroupsPerCounty.Count == 0)
            {
                ViewBag.MaxGroup = 5;
            }
            else
            {
                if (NumberOfGroupsPerCounty.Max() > 0)
                {
                    ViewBag.MaxGroup = (Math.Round(NumberOfGroupsPerCounty.Max() / 10, MidpointRounding.AwayFromZero) * 10) + 10;
                }
                else
                {
                    ViewBag.MaxGroup = 50;
                }
            }
            ViewBag.MaxCustomer = (Math.Round(NumberOfCustomersPerCounty.Max() / 10, MidpointRounding.AwayFromZero) * 10) + 50;
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DashBoard model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.IsPublished = true;
                    dashboardservice.AddNewDashBoard(model, serviceHeader);
                }

                return RedirectToAction("Index", "Application");
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View();
            }
        }        
    }

    public class DashboardContent
    {
        public string Title { get; set; }
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Content { get; set; }
        public bool IsPublished { get; set; }
    }
}
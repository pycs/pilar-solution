﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Pilar.Services.Services;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.EmailDispatcher.Configuration
{
    public class EmailQueueingJob : IJob
    {
        private readonly IMessageQueueService _messageQueueService;
        private readonly IEmailAlertService _emailAlertService;
        private readonly string _messageRelayQueuePath;
        private readonly DataContext datacontext = new DataContext();

        public EmailQueueingJob()
        {
            _messageQueueService = new MessageQueueService();
            _emailAlertService = new EmailAlertService();
            _messageRelayQueuePath = ConfigurationManager.AppSettings["MessageRelayQueuePath"];
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                // 1. Retrieve messages whose DLR status is UnKnown
                var emailAlertsWithDLRStatusPending = _emailAlertService.FindEmailAlertByDRLStatus(4);
                
                // 2. Send the messages to msmq - Normal priority
                if (emailAlertsWithDLRStatusPending != null && emailAlertsWithDLRStatusPending.Any())
                {
                    foreach (var item in emailAlertsWithDLRStatusPending)
                        if (item.MailMessageSendRetry == 0 && !string.IsNullOrWhiteSpace(item.MailMessageTo))
                            _messageQueueService.Send(_messageRelayQueuePath, item.Id, Enumerations.MessageCategory.EmailAlert, (MessagePriority)item.MailMessagePriority);
                }
            }

            catch (Exception ex)
            {
                //ExceptionLoggerFactory.CreateLog(ex.ToString());
                LoggerFactory.CreateLog().LogInfo("EmailQueueingJob.Execute..." + ex);
            }
        }
    }
}

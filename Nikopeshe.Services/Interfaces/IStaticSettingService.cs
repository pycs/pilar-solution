﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data;

namespace Nikopeshe.Services.Interfaces
{
    public interface IStaticSettingService
    {
        void AddNewStaticSetting(StaticSetting staticsetting, ServiceHeader serviceHeader);

        void UpdateStaticSetting(StaticSetting staticsetting, ServiceHeader serviceHeader);

        IList<StaticSetting> GetAllStaticSettings(IsolationLevel isolationLevel);

        StaticSetting FindStaticSettingById(Guid Id);

        StaticSetting FindStaticSettingByKey(string statickey);
    }
}

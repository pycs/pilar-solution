﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class BranchService : IBranchService
    {
        private IRepository<Branch> branchrepository;

        public string ErrorMessage = string.Empty;

        public BranchService(IRepository<Branch> _branchrepository)
        {
            this.branchrepository = _branchrepository;
        }

        public void AddNewBranch(Branch branch, ServiceHeader serviceHeader)
        {
            if (branch == null)
                throw new ArgumentNullException("entity cannot be null");

            try
            {
                branchrepository.Insert(branch, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public void Update(Branch branch, ServiceHeader serviceHeader)
        {
            if (branch == null)
                throw new ArgumentNullException("entity cannot be null");

            try
            {
                branchrepository.Update(branch, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public Branch FindBranchById(Guid Id)
        {
            if (Id == Guid.Empty)
                throw new ArgumentNullException("Id cannot be null");

            var findbranchbyid = branchrepository.GetById(Id);

            return findbranchbyid;
        }

        public IList<Branch> GetAllBranches()
        {
            var findallbranches = branchrepository.GetAll(IsolationLevel.ReadUncommitted);

            return findallbranches.ToList();
        }

        public Branch FindBranchByBranchCode(string branchcode)
        {
            if (string.IsNullOrWhiteSpace(string.Concat(branchcode)))
                throw new ArgumentNullException("branch code cannot be null.");

            var findbranchbybranchcode = branchrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.BranchCode == branchcode).FirstOrDefault();

            return findbranchbybranchcode;
        }

        public Branch FindBranchByC2BNumber(string C2BNumber)
        {
            if(string.IsNullOrWhiteSpace(C2BNumber))
            {
                C2BNumber = "00000";
            }
            if (string.IsNullOrWhiteSpace(C2BNumber))
            {
                throw new ArgumentNullException("C2B Number cannot be null.");                
            }

            var findBranchByC2BNumber = branchrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.C2BNumber == C2BNumber).FirstOrDefault();

            return findBranchByC2BNumber;
        }
    }
}

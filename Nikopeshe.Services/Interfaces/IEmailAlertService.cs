﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;

namespace Nikopeshe.Services.Interfaces
{
    public interface IEmailAlertService
    {
        void AddNewEmailAlert(EmailAlert emailalert, ServiceHeader serviceHeader);

        IList<EmailAlert> FindEmailAlertByDRLStatus(int status);

        void UpdateEmailAlertDRLStatus(Guid Id, int status, ServiceHeader serviceHeader);
    }
}

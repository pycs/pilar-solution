﻿using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.DTO;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.Services.Specifications;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class DomainAuditTrailService : IDomainAuditTrail
    {
        private readonly IRepository<DomainAudit> domainaudittrailrepository;

        public DomainAuditTrailService(Repository<DomainAudit> _domainaudittrailrepository)
        {
            if(_domainaudittrailrepository == null)
                throw new ArgumentNullException("null repository");

            this.domainaudittrailrepository = _domainaudittrailrepository;
        }        

        public IList<DomainAudit> GetAllDomainAudotTrail()
        {
            var domainaudittrail = domainaudittrailrepository.GetAll(IsolationLevel.ReadUncommitted);

            return domainaudittrail.ToList();
        }

        public PageCollectionInfo<DomainAudit> FindDomainAuditFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = DomainAuditSpecifications.DefaultSpec(searchString);

            ISpecification<DomainAudit> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var orderLinePagedCollection = domainaudittrailrepository.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (orderLinePagedCollection != null)
            {
                var pageCollection = orderLinePagedCollection.Items.ToList();

                var totalRecordCount = orderLinePagedCollection.TotalItems;

                return new PageCollectionInfo<DomainAudit> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }
    }    
}

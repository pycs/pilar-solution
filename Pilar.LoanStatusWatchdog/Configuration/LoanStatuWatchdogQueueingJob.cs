﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Quartz;
using System.Configuration;
using System.Linq;

namespace Pilar.LoanStatusWatchdog.Configuration
{
    public class LoanStatuWatchdogQueueingJob : IJob
    {
        private readonly IMessageQueueService _messageQueueService;
        private readonly string _loanstatuswatchdogQueuePath;
        private DataContext datacontext = new DataContext();
        private readonly string _mpesaresponcecode;
        private readonly string _mpesaresultcode;

        public LoanStatuWatchdogQueueingJob()
        {
            _loanstatuswatchdogQueuePath = ConfigurationManager.AppSettings["LoanStatusWatchDogQueuePath"];
            _mpesaresponcecode = ConfigurationManager.AppSettings["responcecode"];
            _mpesaresultcode = ConfigurationManager.AppSettings["resultcode"];
            _messageQueueService = new MessageQueueService();
        }

        public void Execute(IJobExecutionContext context)
        {
            var markloanactive = datacontext.LoanRequest.Where(x => x.B2MResultCode == _mpesaresultcode && x.SPSStatus == (int)Enumerations.EFTStatus.Completed && x.Status != (int)Enumerations.LoanStatus.Cleared && x.Status != (int)Enumerations.LoanStatus.Active).Take(10).ToList();
            if(markloanactive != null && markloanactive.Any())
                foreach(var item in markloanactive)
                {
                    _messageQueueService.Send(_loanstatuswatchdogQueuePath, item.Id, Enumerations.MessageCategory.LoanStatusWatchDog, System.Messaging.MessagePriority.Highest);
                }
        }
    }
}

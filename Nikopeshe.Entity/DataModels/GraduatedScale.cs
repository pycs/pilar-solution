﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    public class GraduatedScale : BaseEntity
    {
        [Display(Name="Charge Name")]
        [Index]
        [Required]
        public Guid ChargeId { get; set; }

        public virtual Charge Charge { get; set; }

        [Display(Name="Lower Limit")]
        [Index]
        [Required]
        public decimal LowerLimit { get; set; }

        [Display(Name="Upper Limit")]
        [Index]
        [Required]
        public decimal UpperLimit { get; set; }
        
        [Required]
        [Display(Name = "ChargeType")]
        public Enumerations.ChargeTypes ChargeType { get; set; }

        public string ChargeTypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.ChargeTypes), ChargeType) ? EnumHelper.GetDescription((Enumerations.ChargeTypes)ChargeType) : string.Empty;
            }
        }
        
        [NotMapped]
        public string Commands { get; set; }
        
        [Required]
        [Index]
        [Display(Name="Value")]
        public decimal Value { get; set; }
    }
}

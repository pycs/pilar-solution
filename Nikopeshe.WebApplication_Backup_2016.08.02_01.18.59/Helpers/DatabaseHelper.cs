﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Data.Mappings;
using Nikopeshe.Entity.DataModels;
using System;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Management;
using System.Web.Profile;
using System.Web.Security;

namespace Nikopeshe.WebApplication.Helpers
{
    public static class DatabaseHelper
    {        
        public static void ConfigureApplicationDatabase()
        {
            try
            {
                var databaseMigrator = new DbMigrator(new Nikopeshe.Data.Migrations.Configuration());

                databaseMigrator.Update();
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
        }

        public static void ConfigureMembershipDatabase()
        {
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["PilarDefaultConnnection"].ConnectionString;

                var connectionStringBuilder = new SqlConnectionStringBuilder(connectionString);

                SqlServices.Install(connectionStringBuilder.InitialCatalog, SqlFeatures.All, connectionString);

                if (!Roles.RoleExists("Administrator"))
                    Roles.CreateRole("Administrator");

                if (!Roles.RoleExists("LoanOfficer"))
                    Roles.CreateRole("LoanOfficer");

                if (!Roles.RoleExists("Customer"))
                    Roles.CreateRole("Customer");

                if (!Roles.RoleExists("LoanManager"))
                    Roles.CreateRole("LoanManager");

                if (!Roles.RoleExists("FinanceManager"))
                    Roles.CreateRole("FinanceManager");

                if (Membership.GetUser(DefaultSettings.Instance.RootUser, true) == null)
                {
                    MembershipCreateStatus result;
                    Membership.CreateUser(DefaultSettings.Instance.RootUser, DefaultSettings.Instance.RootPassword, DefaultSettings.Instance.Email, DefaultSettings.Instance.PasswordQuestion, DefaultSettings.Instance.PasswordAnswer, true, out result);

                    switch (result)
                    {
                        case MembershipCreateStatus.DuplicateEmail:
                            break;
                        case MembershipCreateStatus.DuplicateProviderUserKey:
                            break;
                        case MembershipCreateStatus.DuplicateUserName:
                            break;
                        case MembershipCreateStatus.InvalidAnswer:
                            break;
                        case MembershipCreateStatus.InvalidEmail:
                            break;
                        case MembershipCreateStatus.InvalidPassword:
                            break;
                        case MembershipCreateStatus.InvalidProviderUserKey:
                            break;
                        case MembershipCreateStatus.InvalidQuestion:
                            break;
                        case MembershipCreateStatus.InvalidUserName:
                            break;
                        case MembershipCreateStatus.ProviderError:
                            break;
                        case MembershipCreateStatus.Success:
                            Roles.AddUserToRole(DefaultSettings.Instance.RootUser, "Administrator");

                            ProfileBase profile = ProfileBase.Create(DefaultSettings.Instance.RootUser);
                            profile["FirstName"] = DefaultSettings.Instance.FirstName;
                            profile["LastName"] = DefaultSettings.Instance.LastName;
                            profile.Save();
                            break;
                        case MembershipCreateStatus.UserRejected:
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
        }

        public static void CreateDefaultBranch()
        {
            DataContext datacontext = new DataContext();
            var branch = new Branch()
            {
                BranchCode = "000",
                BranchName = "Head Office",
                IsEnabled = true,
                Type = (int)Enumerations.BranchType.Branch
            };

            var findheadoffice = datacontext.Branch.Where(x => x.BranchCode == "000").ToList();
            if (findheadoffice.Count == 0)
            {
                datacontext.Branch.Add(branch);
                datacontext.SaveChanges();
            }
        }

        public static void StaticSettings()
        {
            DataContext datacontext = new DataContext();
            var ENFORCE_CUSTOMER_DOCUMENTS_AFTER_REGISTRATION = datacontext.StaticSetting.Where(x => x.Key == "ENFORCE_CUSTOMER_DOCUMENTS_AFTER_REGISTRATION").ToList();

            var ENFORCE_LOAN_DOCUMENTS_BEFORE_LOAN_APPROVAL = datacontext.StaticSetting.Where(x => x.Key == "ENFORCE_LOAN_DOCUMENTS_BEFORE_LOAN_APPROVAL").ToList();

            var ENFORCE_CUSTOMER_AND_LOAN_DOCUMENTS_BEFORE_LOAN_APPPROVAL = datacontext.StaticSetting.Where(x => x.Key == "ENFORCE_CUSTOMER_AND_LOAN_DOCUMENTS_BEFORE_LOAN_APPPROVAL").ToList();

            var LOAN_REMINDER_DUE_DAYS = datacontext.StaticSetting.Where(x => x.Key == "LOAN_REMINDER_DUE_DAYS").ToList();

            var CREATEMEMBERSHIPACCOUNT = datacontext.StaticSetting.Where(x => x.Key == "CREATEMEMBERSHIPACCOUNT").ToList();

            var CUSTOMERJOININGFEE = datacontext.StaticSetting.Where(x => x.Key == "CUSTOMERJOININGFEE").ToList();

            var ENABLELOCKSYSTEM = datacontext.StaticSetting.Where(x => x.Key == "ENABLELOCKSYSTEM").ToList();

            var OPENINGTIME = datacontext.StaticSetting.Where(x => x.Key == "OPENINGTIME").ToList();

            var CLOSINGTIME = datacontext.StaticSetting.Where(x => x.Key == "CLOSINGTIME").ToList();

            var CREDITSAVINGSACCOUNT = datacontext.StaticSetting.Where(x => x.Key == "CREDITSAVINGSACCOUNT").ToList();

            if (ENFORCE_CUSTOMER_DOCUMENTS_AFTER_REGISTRATION.Count == 0)
            {
                StaticSetting setttings = new StaticSetting()
                {
                    Key = "ENFORCE_CUSTOMER_DOCUMENTS_AFTER_REGISTRATION",
                    Value = "0",
                    IsLocked = false
                };
                datacontext.StaticSetting.Add(setttings);
                datacontext.SaveChanges();
            }
            if (ENFORCE_LOAN_DOCUMENTS_BEFORE_LOAN_APPROVAL.Count == 0)
            {
                StaticSetting setttings = new StaticSetting()
                {
                    Key = "ENFORCE_LOAN_DOCUMENTS_BEFORE_LOAN_APPROVAL",
                    Value = "0",
                    IsLocked = false
                };
                datacontext.StaticSetting.Add(setttings);
                datacontext.SaveChanges();
            }
            if (ENFORCE_CUSTOMER_AND_LOAN_DOCUMENTS_BEFORE_LOAN_APPPROVAL.Count == 0)
            {
                StaticSetting setttings = new StaticSetting()
                {
                    Key = "ENFORCE_CUSTOMER_AND_LOAN_DOCUMENTS_BEFORE_LOAN_APPPROVAL",
                    Value = "1",
                    IsLocked = false
                };
                datacontext.StaticSetting.Add(setttings);
                datacontext.SaveChanges();
            }

            if (LOAN_REMINDER_DUE_DAYS.Count == 0)
            {
                StaticSetting setttings = new StaticSetting()
                {
                    Key = "LOAN_REMINDER_DUE_DAYS",
                    Value = "2",
                    IsLocked = false
                };
                datacontext.StaticSetting.Add(setttings);
                datacontext.SaveChanges();
            }

            if (CUSTOMERJOININGFEE.Count == 0)
            {
                StaticSetting setttings = new StaticSetting()
                {
                    Key = "CUSTOMERJOININGFEE",
                    Value = "150",
                    IsLocked = false
                };
                datacontext.StaticSetting.Add(setttings);
                datacontext.SaveChanges();
            }

            if (CREATEMEMBERSHIPACCOUNT.Count == 0)
            {
                StaticSetting setttings = new StaticSetting()
                {
                    Key = "CREATEMEMBERSHIPACCOUNT",
                    Value = "0",
                    IsLocked = false
                };
                datacontext.StaticSetting.Add(setttings);
                datacontext.SaveChanges();
            }

            if (ENABLELOCKSYSTEM.Count == 0)
            {
                StaticSetting setttings = new StaticSetting()
                {
                    Key = "ENABLELOCKSYSTEM",
                    Value = "0",
                    IsLocked = false
                };
                datacontext.StaticSetting.Add(setttings);
                datacontext.SaveChanges();
            }

            if (OPENINGTIME.Count == 0)
            {
                StaticSetting setttings = new StaticSetting()
                {
                    Key = "OPENINGTIME",
                    Value = "08:00:00.000",
                    IsLocked = false
                };
                datacontext.StaticSetting.Add(setttings);
                datacontext.SaveChanges();
            }

            if (CLOSINGTIME.Count == 0)
            {
                StaticSetting setttings = new StaticSetting()
                {
                    Key = "CLOSINGTIME",
                    Value = "17:00:00.000",
                    IsLocked = false
                };
                datacontext.StaticSetting.Add(setttings);
                datacontext.SaveChanges();
            }

            if (CREDITSAVINGSACCOUNT.Count == 0)
            {
                StaticSetting setttings = new StaticSetting()
                {
                    Key = "CREDITSAVINGSACCOUNT",
                    Value = "0",
                    IsLocked = false
                };
                datacontext.StaticSetting.Add(setttings);
                datacontext.SaveChanges();
            }
        }

        public static void InitializeChartOfAccounts()
        {
            DataContext dataContext = new DataContext();

            var FindAssetGL = dataContext.ChartofAccount.Where(x => x.AccountCode == 1000000).ToList();

            var FindLiabilityGL = dataContext.ChartofAccount.Where(x => x.AccountCode == 2000000).ToList();

            var FindEquityGl = dataContext.ChartofAccount.Where(x => x.AccountCode == 3000000).ToList();

            var FindIncome = dataContext.ChartofAccount.Where(x => x.AccountCode == 4000000).ToList();

            var FindExpenseGL = dataContext.ChartofAccount.Where(x => x.AccountCode == 5000000).ToList();

            if (FindAssetGL.Count == 0)
            {
                ChartofAccount chartOfAccount = new ChartofAccount()
                {
                    Id = Guid.NewGuid(),
                    ChartOfAccountType = Enumerations.ChartOfAccountType.Asset,
                    AccountCode = (int)Enumerations.ChartOfAccountType.Asset,
                    ChartOfAccountCategory = (int)Enumerations.ChartOfAccountCategory.HeaderAccount,
                    AccountName = "Asset",
                    IsActive = true,
                    Depth = 0,
                    CreatedDate = DateTime.Now                   
                };
                dataContext.ChartofAccount.Add(chartOfAccount);
                dataContext.SaveChanges();
            }

            if (FindLiabilityGL.Count == 0)
            {
                ChartofAccount chartOfAccount = new ChartofAccount()
                {
                    Id = Guid.NewGuid(),
                    ChartOfAccountType = Enumerations.ChartOfAccountType.Liability,
                    AccountCode = (int)Enumerations.ChartOfAccountType.Liability,
                    ChartOfAccountCategory = (int)Enumerations.ChartOfAccountCategory.HeaderAccount,
                    AccountName = "Liability",
                    IsActive = true,
                    Depth = 0,
                    CreatedDate = DateTime.Now
                };
                dataContext.ChartofAccount.Add(chartOfAccount);
                dataContext.SaveChanges();
            }

            if (FindEquityGl.Count == 0)
            {
                ChartofAccount chartOfAccount = new ChartofAccount()
                {
                    Id = Guid.NewGuid(),
                    ChartOfAccountType = Enumerations.ChartOfAccountType.Equity,
                    AccountCode = (int)Enumerations.ChartOfAccountType.Equity,
                    ChartOfAccountCategory = (int)Enumerations.ChartOfAccountCategory.HeaderAccount,
                    AccountName = "Equity",
                    IsActive = true,
                    Depth = 0,
                    CreatedDate = DateTime.Now
                };
                dataContext.ChartofAccount.Add(chartOfAccount);
                dataContext.SaveChanges();
            }

            if (FindIncome.Count == 0)
            {
                ChartofAccount chartOfAccount = new ChartofAccount()
                {
                    Id = Guid.NewGuid(),
                    ChartOfAccountType = Enumerations.ChartOfAccountType.Income,
                    AccountCode = (int)Enumerations.ChartOfAccountType.Income,
                    ChartOfAccountCategory = (int)Enumerations.ChartOfAccountCategory.HeaderAccount,
                    AccountName = "Income",
                    IsActive = true,
                    Depth = 0,
                    CreatedDate = DateTime.Now
                };
                dataContext.ChartofAccount.Add(chartOfAccount);
                dataContext.SaveChanges();
            }

            if (FindExpenseGL.Count == 0)
            {
                ChartofAccount chartOfAccount = new ChartofAccount()
                {
                    Id = Guid.NewGuid(),
                    ChartOfAccountType = Enumerations.ChartOfAccountType.Expense,
                    AccountCode = (int)Enumerations.ChartOfAccountType.Expense,
                    ChartOfAccountCategory = (int)Enumerations.ChartOfAccountCategory.HeaderAccount,
                    AccountName = "Expense",
                    IsActive = true,
                    Depth = 0,
                    CreatedDate = DateTime.Now
                };
                dataContext.ChartofAccount.Add(chartOfAccount);
                dataContext.SaveChanges();
            }
        }
    }
}
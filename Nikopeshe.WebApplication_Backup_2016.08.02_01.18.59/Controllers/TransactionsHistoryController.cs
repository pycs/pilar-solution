﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize(Roles = "LoanOfficer, Administrator, LoanManager")]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class TransactionsHistoryController : Controller
    {
        private readonly ITransactionHistoryService transactionhistoryservice;
        private readonly ICustomerService customerservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public TransactionsHistoryController(ITransactionHistoryService _transactionhistoryservice, ICustomerService _customerservice)
        {
            if(_transactionhistoryservice == null || _customerservice == null)
                throw new ArgumentNullException("null service reference");

            this.transactionhistoryservice = _transactionhistoryservice;
            this.customerservice = _customerservice;
        }

        //
        // GET: /TransactionsHistory/        
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getalltransactionhistory(JQueryDataTablesModel datatablemodel)
        {            
            var pagecollection = transactionhistoryservice.GetAllLoanTransactionsHistory();

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var transactionhistoryList = GetAllTransactionsHistory(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, TransactionsHistoryList: pagecollection);


            return Json(new JQueryDataTablesResponse<TransactionHistory>(items: transactionhistoryList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<TransactionHistory> GetAllTransactionsHistory(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<TransactionHistory> TransactionsHistoryList)
        {
            var GetTransactionsHistoryList = TransactionsHistoryList;

            totalRecordCount = GetTransactionsHistoryList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetTransactionsHistoryList = GetTransactionsHistoryList.Where(c => c.Customer.FullName
                    .ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetTransactionsHistoryList.Count;

            IOrderedEnumerable<TransactionHistory> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "Customer.FullName":
                        sortedClients = sortedClients == null ? GetTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Customer.FullName);
                        break;

                    case "Amount":
                        sortedClients = sortedClients == null ? GetTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.Amount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Amount);
                        break;

                    case "TransactionTypeDesc":
                        sortedClients = sortedClients == null ? GetTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.TransactionTypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransactionTypeDesc);
                        break;

                    case "TransactionDateTime":
                        sortedClients = sortedClients == null ? GetTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.TransactionDateTime)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransactionDateTime);
                        break;

                    case "TypeDesc":
                        sortedClients = sortedClients == null ? GetTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.TypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TypeDesc);
                        break;                   
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult addtransactionhistory()
        {
            ViewBag.Customerlist = GetCustomerlist(Guid.Empty.ToString());

            return View();
        }

        [HttpPost]        
        public ActionResult addtransactionhistory(TransactionHistory transactionhistory)
        {
            ViewBag.Customerlist = GetCustomerlist(Guid.Empty.ToString());            

            transactionhistory.TransactionDateTime = DateTime.Now;

            if(transactionhistory.CustomerId == Guid.Empty)
            {
                ModelState.AddModelError(string.Empty, "Please select a customer.");
                return View(transactionhistory);
            }

            if(string.Concat(transactionhistory.Type) == "0" || string.Concat(transactionhistory.TransactionType) == "0")
            {
                ModelState.AddModelError(string.Empty, "Transaction Type or Type cannot be null");
                return View(transactionhistory);
            }

            if(ModelState.IsValid)
            {
                try
                {                    
                    transactionhistoryservice.AddNewTransactionHistory(transactionhistory, serviceHeader);

                    return RedirectToAction("Index");
                }
                catch(Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(transactionhistory);
                }
            }
            return View(transactionhistory);
        }

        [HttpGet]
        public ActionResult customertransactionhistory(Guid Id)
        {            
            ViewBag.CustomerId = Id;

            return View();
        }

        public JsonResult Getcustomertransactionhistory(Guid id, JQueryDataTablesModel datatablemodel)
        {           
            var pagecollection = transactionhistoryservice.GetAllCustomerTransationHistoryByCustomerId(id);
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var transactionhistoryList = GetAllCustomerTransactionsHistory(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, TransactionsHistoryList: pagecollection);


            return Json(new JQueryDataTablesResponse<TransactionHistory>(items: transactionhistoryList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<TransactionHistory> GetAllCustomerTransactionsHistory(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<TransactionHistory> TransactionsHistoryList)
        {
            var GetTransactionsHistoryList = TransactionsHistoryList;

            totalRecordCount = GetTransactionsHistoryList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetTransactionsHistoryList = GetTransactionsHistoryList.Where(c => c.Customer.FullName
                    .ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetTransactionsHistoryList.Count;

            IOrderedEnumerable<TransactionHistory> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "Customer.FullName":
                        sortedClients = sortedClients == null ? GetTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Customer.FullName);
                        break;

                    case "Amount":
                        sortedClients = sortedClients == null ? GetTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.Amount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Amount);
                        break;

                    case "TransactionTypeDesc":
                        sortedClients = sortedClients == null ? GetTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.TransactionTypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransactionTypeDesc);
                        break;

                    case "TransactionDateTime":
                        sortedClients = sortedClients == null ? GetTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.TransactionDateTime)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransactionDateTime);
                        break;

                    case "TypeDesc":
                        sortedClients = sortedClients == null ? GetTransactionsHistoryList.CustomSort(sortedColumn.Direction, cust => cust.TypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TypeDesc);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }


        [NonAction]
        public List<SelectListItem> GetCustomerlist(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultItem = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Customer", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultItem);

            var otherItems = customerservice.GetAllCustomers().Where(x => x.IsEnabled == true && x.IsApproved == true);

            foreach (var item in otherItems)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.FullNamemobileNumber, Value = item.Id.ToString("D") });

            return SelectList;
        }              
	}
}
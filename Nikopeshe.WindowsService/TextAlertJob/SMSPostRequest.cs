﻿using Newtonsoft.Json;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Quartz;
using System;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace Nikopeshe.WindowsService.TextAlertJob
{
    public class AfricasTalkingGatewayException : Exception
    {
        public AfricasTalkingGatewayException(string message)
            : base(message) { }
    }

    public class SMSPostRequest : IJob
    {
        // Specify your login credentials
        string AfricasTakingAPIKey = ConfigurationManager.AppSettings["AfricasTalkingAPIKey"].ToString();
        string AfricasTakingUserName = ConfigurationManager.AppSettings["AfricasTalkingUsername"].ToString();
        string AfricasTakingAPIEndPoint = ConfigurationManager.AppSettings["SMS_URLString"].ToString();
        
        DataContext datacontext = new DataContext();

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                // Create a new instance of our gateway class
                AfricasTalkingGateway gateway = new AfricasTalkingGateway(AfricasTakingUserName, AfricasTakingAPIKey);

                var findpendingtextalerts = datacontext.TextAlert.Where(x => x.TextMessageDLRStatus == (int)Enumerations.DLRStatus.Pending).Take(1).ToList();
                if (findpendingtextalerts != null && findpendingtextalerts.Any())
                {
                    foreach (var pendingtext in findpendingtextalerts)
                    {
                        string recipients = pendingtext.TextMessageRecipient;
                        string message = pendingtext.TextMessageBody;
                        dynamic results = gateway.sendMessage(recipients, message);
                        foreach (dynamic result in results)
                        {

                            var number = result["number"];
                            var status = result["status"].ToString();                      
                            var updatetextalert = datacontext.TextAlert.Find(pendingtext.Id);
                            var entry = datacontext.Entry(updatetextalert);
                            entry.State = EntityState.Modified;
                            if (status.ToLower() == "Success".ToLower())
                            {
                                updatetextalert.ATMessageId = result["messageId"];
                                updatetextalert.Cost = result["cost"];
                                updatetextalert.Status = status; // status is either "Success" or "error message"
                                updatetextalert.TextMessageDLRStatus = (int)Enumerations.DLRStatus.Delivered;
                                datacontext.SaveChanges();
                            }

                            else
                            {
                                updatetextalert.TextMessageDLRStatus = (int)Enumerations.DLRStatus.Failed;
                                datacontext.SaveChanges();
                            }            
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }           
        }       
        
    }

    public class TextAlertDefination
    {
        public string username { get; set; }
        public string to { get; set; }
        public string message { get; set; }
    }   
}

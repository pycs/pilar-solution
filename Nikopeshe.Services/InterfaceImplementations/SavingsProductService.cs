﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class SavingsProductService : ISavingsProductService
    {
        readonly IRepository<SavingsProduct> savingsaccountrepository;

        string ErrorMessage = string.Empty;

        public SavingsProductService(IRepository<SavingsProduct> _savingsaccountrepository)
        {
            if(_savingsaccountrepository == null)
                throw new ArgumentNullException("null repository");

            this.savingsaccountrepository = _savingsaccountrepository;
        }

        public void AddNewSavingsProduct(SavingsProduct savingsproduct, ServiceHeader serviceHeader)
        {            
            try
            {
                if (savingsproduct == null)
                    throw new ArgumentNullException("entity cannot be null");

                savingsaccountrepository.Insert(savingsproduct, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public void UpdateSavingProduct(SavingsProduct savingsproduct, ServiceHeader serviceHeader)
        {
            if (savingsproduct == null)
                throw new ArgumentNullException("entity cannot be null");

            try
            {
                savingsaccountrepository.Update(savingsproduct, serviceHeader);
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public SavingsProduct FindSavingsProductById(Guid Id)
        {
            if (Id == Guid.Empty)
                throw new ArgumentNullException("Guid cannot be null");

            var findsavingsaccountbyid = savingsaccountrepository.GetById(Id);

            return findsavingsaccountbyid;
        }

        public SavingsProduct FindSavingsChartofAccountsId(Guid chartofaccountid)
        {
            if(chartofaccountid == Guid.Empty)
                throw new ArgumentNullException("Guid cannot be null");

            var findsavingsaccountbychartofaccountid = savingsaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.ChartofAccountId == chartofaccountid);

            return findsavingsaccountbychartofaccountid.FirstOrDefault();           
        }

        public IList<SavingsProduct> GetAllSavingsAccountProducts(IsolationLevel isolationLevel)
        {
            var getallsavingsproduct = savingsaccountrepository.GetAll(isolationLevel);

            return getallsavingsproduct.ToList();
        }

        public SavingsProduct FindDefaultSavingsProduct()
        {
            var finddefaultsavingproduct = savingsaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.IsDefault == true);

            return finddefaultsavingproduct.FirstOrDefault();
        }

        public IList<SavingsProduct> DefaultSavingsProductCount()
        {
            var finddefaultsavingproduct = savingsaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.IsDefault == true);

            return finddefaultsavingproduct.ToList();
        }

        public string NextSavingProductCode()
        {
            string generatenextsavingproductcode = string.Empty;

            var findlastissuedsavingsproductcode = savingsaccountrepository.GetAll(IsolationLevel.ReadCommitted).OrderByDescending(x => x.CreatedDate).FirstOrDefault();

            if (findlastissuedsavingsproductcode == null)
            {
                generatenextsavingproductcode = string.Concat((int)Enumerations.ProductCode.Savings);
            }
            else
            {

                generatenextsavingproductcode = string.Format("{0}", int.Parse(findlastissuedsavingsproductcode.ProductCode) + 1);
            }
            return generatenextsavingproductcode.ToString();
        }       
    }
}

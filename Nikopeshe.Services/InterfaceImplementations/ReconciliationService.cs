﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class ReconciliationService : IReconciliationService
    {
        private readonly IRepository<Reconciliation> reconciliationRepository;
        public string errorMessage = string.Empty;
        public ReconciliationService(IRepository<Reconciliation> _reconciliationRepository)
        {
            this.reconciliationRepository = _reconciliationRepository;
        }

        public void Add(Reconciliation reconFile, ServiceHeader serviceHeader)
        {
            if (reconFile == null)
                throw new ArgumentNullException("entity cannot be null");

            try
            {
                reconciliationRepository.Insert(reconFile, serviceHeader);
            }

            catch(DbEntityValidationException ex)
            {
                foreach(var validationErrors in ex.EntityValidationErrors)
                {
                    foreach(var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} ErrorMessage:{1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw new Exception(errorMessage, ex);            
            }
        }

        public IList<Reconciliation> FindAllReconciliationFiles()
        {
            return reconciliationRepository.GetAll(IsolationLevel.ReadUncommitted).ToList();
        }

        public IList<Reconciliation> FindAllReconciliationFilesByStatus(int Status)
        {
            var findReconFilesbyStatus = reconciliationRepository.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.FileStatus == Status);

            return findReconFilesbyStatus.ToList();
        }
    }
}

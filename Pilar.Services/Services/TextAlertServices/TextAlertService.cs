﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.TextAlertServices
{
    public class TextAlertService : ITextAlertService
    {
        private string errorMessage = string.Empty;
        private DataContext datacontext = new DataContext();
        public void AddNewTextAlert(TextAlert textalert)
        {
            try
            {
                if (textalert == null)
                    throw new ArgumentNullException("entity cannot be null");

                datacontext.TextAlert.Add(textalert);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
        }

        public IList<TextAlert> FindTextAlertByDRLStatus(int status)
        {
            if (string.IsNullOrWhiteSpace(status.ToString()))
                throw new ArgumentNullException("null entity");

            var results = datacontext.TextAlert.Where(x => x.TextMessageDLRStatus == status);

            return results.ToList();
        }

        public void UpdateTextAlertDRLStatus(Guid Id, int Status)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Id.ToString()) || string.IsNullOrWhiteSpace(Status.ToString()))

                    throw new ArgumentNullException("empty value(s)");

                var findrecordbyId = datacontext.TextAlert.Find(Id);

                findrecordbyId.TextMessageDLRStatus = Status;

                var entry = datacontext.Entry(findrecordbyId);
                entry.State = EntityState.Modified;
                datacontext.SaveChanges();
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
        }

        public void UpdateTextAlertDRLStatus(Guid Id, int TextMessageDLRStatus, string ATMessageId, string Cost, string Status)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Id.ToString()) || string.IsNullOrWhiteSpace(Status.ToString()))

                    throw new ArgumentNullException("empty value(s)");

                var findrecordbyId = datacontext.TextAlert.Find(Id);

                findrecordbyId.TextMessageDLRStatus = TextMessageDLRStatus;
                findrecordbyId.ATMessageId = ATMessageId;
                findrecordbyId.Cost = Cost;
                findrecordbyId.Status = Status;
                var entry = datacontext.Entry(findrecordbyId);
                entry.State = EntityState.Modified;
                datacontext.SaveChanges();
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
        }

        public TextAlert FindTextAlertById(Guid Id)
        {
            var findtextalertbyid = datacontext.TextAlert.Find(Id);

            return findtextalertbyid;
        }
    }
}

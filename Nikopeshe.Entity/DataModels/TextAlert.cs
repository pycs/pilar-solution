﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    public class TextAlert : BaseEntity
    {       
        [Display(Name = "Recipient")]
        [Required]
        [Column(TypeName = "VARCHAR")]
        [Index("Recipient")]
        [MaxLength(20)]
        public string TextMessageRecipient { get; set; }

        [Display(Name = "Message")]
        [Required]        
        [Index("TextMessageBody")]
        [Column(TypeName = "VARCHAR")]
        [MaxLength(899)]
        public string TextMessageBody { get; set; }

        [Display(Name = "Message")]
        [MaxLength(899)]
        public string MaskedTextMessageBody { get; set; }

        [Display(Name = "DLR Status")]
        [Index("DLRStatus")]
        public int TextMessageDLRStatus { get; set; }

        [Display(Name = "DLR Status")]
        public string TextMessageDLRStatusDescription
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.DLRStatus), TextMessageDLRStatus) ? EnumHelper.GetDescription((Enumerations.DLRStatus)TextMessageDLRStatus) : string.Empty;
            }
        }
      
        [Display(Name = "Reference")]
        [MaxLength(100)]
        public string TextMessageReference { get; set; }

        [Display(Name = "Origin")]
        public int TextMessageOrigin { get; set; }

        [Display(Name = "Origin")]
        public string TextMessageOriginDescription
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.MessageOrigin), TextMessageOrigin) ? EnumHelper.GetDescription((Enumerations.MessageOrigin)TextMessageOrigin) : string.Empty;
            }
        }

        [Display(Name = "Priority")]
        public int TextMessagePriority { get; set; }

        [Display(Name = "Send Retry")]
        public int TextMessageSendRetry { get; set; }

        [Display(Name = "Security Critical")]
        public bool TextMessageSecurityCritical { get; set; }
        
        [Display(Name = "Created By")]
        [MaxLength(100)]
        public string CreatedBy { get; set; }

        [MaxLength(100)]
        public string ATMessageId { get; set; }

        [MaxLength(100)]
        public string Cost { get; set; }

        [MaxLength(100)]
        public string Status { get; set; }
    }
}

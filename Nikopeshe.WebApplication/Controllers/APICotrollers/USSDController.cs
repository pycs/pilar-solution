﻿using Crosscutting.NetFramework.Logging;
using Infrastructure.Crosscutting.NetFramework;
using LazyCache;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using Newtonsoft.Json;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Utilities;
using Stateless;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Nikopeshe.WebApplication.Controllers.APICotrollers
{
    public class USSDController : ApiController
    {
        private readonly IAppCache _appCache;
        private string mobileNumber = string.Empty;
        
        public USSDController()
        {

        }
        public USSDController(IAppCache appCache)
        {
            Guard.ArgumentNotNull(appCache, "appCache");
            
            this._appCache = appCache;
            
        }

        public Dictionary<string, string> GetHTTPResponseHeaders(string Url)
        {
            Dictionary<string, string> HeaderList = new Dictionary<string, string>();

            WebRequest WebRequestObject = HttpWebRequest.Create(Url);
            WebResponse ResponseObject = WebRequestObject.GetResponse();

            foreach (string HeaderKey in ResponseObject.Headers)
                HeaderList.Add(HeaderKey, ResponseObject.Headers[HeaderKey]);

            ResponseObject.Close();

            return HeaderList;
        }

         // GET api/ussd
        public HttpResponseMessage Get()
        {
            try
            {
                // TODO: { DefaultCacheDuration = 60 * 5 /*5minutes*/}
                var loanRequestUSSD = _appCache.GetOrAdd<LoanRequestUSSD>(Request.GetQueryString("SESSION_ID"), () => { return new LoanRequestUSSD(Request.GetQueryString("ORIG")); });

                loanRequestUSSD.Transition(Request.GetQueryString("USSD_PARAMS"));

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(string.Format("{0}", loanRequestUSSD));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                return response;
            }

            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(string.Format("END {0}", "Sorry, there was a technical error. Please try again later."));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                return response;
            }
        }

        // POST api/ussd
        public HttpResponseMessage Post(Params value)
        {
            //LoggerFactory.CreateLog().LogInfo("serviceCode => {0}, phoneNumber => {1}, sessionId => {2}, text => {3}", value.serviceCode, value.phoneNumber, value.sessionId, value.text);
            try
            {
                string result = string.Empty;
                if (value.text != null)
                {
                    if (value.text.Contains('*'))
                    {
                        string[] stringValues = value.text.Split('*');

                        result = stringValues[stringValues.Length - 1];
                    }

                    else
                    {
                        result = value.text;
                    }
                }
                else
                {
                    result = "";
                }

                var mobileNumber = value.phoneNumber.TrimStart('+');
                //mobileNumber = value.phoneNumber;

                // TODO: { DefaultCacheDuration = 60 * 5 /*5minutes*/}

                var loanRequestUSSD = _appCache.GetOrAdd<LoanRequestUSSD>(value.sessionId, () => { return new LoanRequestUSSD(mobileNumber); });
                loanRequestUSSD.Transition(result);

                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(string.Format("{0}", loanRequestUSSD));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                return response;
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(string.Format("END {0}", "Sorry, a technical error has occurred. Please try again later."));
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
                return response;
            }
        }
    }

    public class LoanRequestUSSD
    {
        public string MSISDN { get; set; }

        public string PIN { get; set; }

        public string NewPIN { get; set; }

        public string ReceiverPartyIdentifier { get; set; }

        public string TransactionAmount { get; set; }

        public string TransactionAccountReference { get; set; }

        public string AcceptMessage { get; set; }

        public Partner PartnerName { get; set; }

        public string LoanBalanceRepaymentType { get; set; }

        public string OnlineCheckoutURL { get; set; }

        public decimal PartnerInterestChargeRate { get; set; }

        public States CurrentState { get; protected set; }

        public enum States
        {
            Initial,
            LoanRequest,
            LoanBalance,
            ChangePIN,
            Accepted,
            Declined,
            Terminated,
            PayLoan
        }

        public enum Triggers
        {
            Initial = 33,
            LoanRequest = 1,
            LoanBalance = 2,
            ChangePIN = 3,
            PayLoan = 4,
            Accept = 98,
            Decline = 99,
            Terminate = -1,
        }

        protected StateMachine<States, Triggers> Machine { get; set; }

        protected StateMachine<States, Triggers>.TriggerWithParameters<int> ParameterizedAcceptTrigger;

        protected Customer customer { get; set; }

        protected CustomersAccount customerAccount { get; set; }

        protected string PaybillNumber { get; set; }

        DataContext _dataContext = new DataContext();

        protected List<LoanRequest> loanRequest = new List<LoanRequest>();

        protected LoanProduct loanproduct { get; set; }

        protected decimal LoanBalance { get; set; }

        protected LoanRequestUSSD()
        {
            Machine = new StateMachine<States, Triggers>(() => CurrentState, s => CurrentState = s);

            ParameterizedAcceptTrigger = Machine.SetTriggerParameters<int>(Triggers.Accept);

            Machine.Configure(States.Initial)
               .Permit(Triggers.LoanBalance, States.LoanBalance)
               .Permit(Triggers.LoanRequest, States.LoanRequest)
               .Permit(Triggers.ChangePIN, States.ChangePIN)
               .Permit(Triggers.PayLoan, States.PayLoan)
               .PermitReentry(Triggers.Initial)
               .Permit(Triggers.Terminate, States.Terminated)
               .Permit(Triggers.Decline, States.Declined).PermitReentry(Triggers.Decline);

            Machine.Configure(States.LoanBalance)
                .SubstateOf(States.Initial)
                .PermitIf(Triggers.Accept, States.Accepted, () => !string.IsNullOrWhiteSpace(ReceiverPartyIdentifier) && !string.IsNullOrWhiteSpace(TransactionAmount) && !string.IsNullOrWhiteSpace(PIN))
                .Permit(Triggers.Decline, States.Declined);

            Machine.Configure(States.LoanRequest)
                .SubstateOf(States.Initial)
                .PermitReentry(Triggers.LoanRequest)
                .Permit(Triggers.Accept, States.Accepted)
                .Permit(Triggers.Decline, States.Declined);

            Machine.Configure(States.PayLoan)
                .SubstateOf(States.Initial)
                .PermitReentry(Triggers.PayLoan)
                .Permit(Triggers.Accept, States.Accepted)
                .Permit(Triggers.Decline, States.Declined);

            Machine.Configure(States.ChangePIN)
               .SubstateOf(States.Initial)
               .PermitReentry(Triggers.ChangePIN)
               .PermitIf(Triggers.Accept, States.Accepted, () => !string.IsNullOrWhiteSpace(PIN) && !string.IsNullOrWhiteSpace(NewPIN))
               .Permit(Triggers.Decline, States.Declined);

            Machine.Configure(States.Terminated)
                .SubstateOf(States.Initial)
                .PermitReentry(Triggers.Terminate)
                .PermitIf(Triggers.Accept, States.Accepted, () => !string.IsNullOrWhiteSpace(ReceiverPartyIdentifier) && !string.IsNullOrWhiteSpace(TransactionAmount) && !string.IsNullOrWhiteSpace(TransactionAccountReference))
                .Permit(Triggers.Decline, States.Declined);

            Machine.Configure(States.Declined)
                .Permit(Triggers.Initial, States.Initial)
                .PermitReentry(Triggers.Terminate);

            Machine.Configure(States.Accepted)
                .Permit(Triggers.Initial, States.Initial)
                .Permit(Triggers.Decline, States.Declined)
                .OnEntryFrom(ParameterizedAcceptTrigger, (currentState) =>
                {
                    switch ((States)currentState)
                    {
                        case States.LoanBalance:
                            // commit check balance
                            AcceptMessage = "Loan Account Balance";
                            break;

                        case States.PayLoan:
                            AcceptMessage = string.Format("Enter your Bonga PIN in the next screen to complete. Don't have your service PIN? Just create it in the next screen.");
                            var onlineCheckOutParam = new OnlineCheckoutParams
                            {
                                msisdn = MSISDN,
                                merchantId = PartnerName.MerchantId,
                                passKey = PartnerName.PassKey,
                                transactionAmount = TransactionAmount,
                                referenceId = MSISDN
                            };
                            JavaScriptSerializer json = new JavaScriptSerializer();
                            string output = json.Serialize(onlineCheckOutParam);
                            var jsonData = Encoding.UTF8.GetBytes(output);
                            HttpWebRequest webRequest = WebRequest.Create(OnlineCheckoutURL) as HttpWebRequest;
                            if (webRequest == null)
                                throw new NullReferenceException("the request is not a web request");
                            webRequest.Method = "POST";
                            webRequest.ContentType = "application/json";
                            webRequest.ContentLength = jsonData.Length;
                            
                            using (Stream requestStream = webRequest.GetRequestStream())
                            {
                                requestStream.Write(jsonData, 0, jsonData.Length);
                                requestStream.Close();
                            }

                            HttpWebResponse webReponse = webRequest.GetResponse() as HttpWebResponse;
                            
                            if (webReponse.StatusCode == HttpStatusCode.OK)
                            {
                                using (var receiveStream = webReponse.GetResponseStream())
                                {
                                    using (var readstream = new StreamReader(receiveStream, new UTF8Encoding()))
                                    {
                                        LoggerFactory.CreateLog().LogInfo("Transaction Completed for MSISDN => {0}", MSISDN);
                                    }
                                }
                            }
                                    
                            break;
                        case States.LoanRequest:

                            // commit loan request                                                        
                            
                            string status = string.Empty;

                            using (var transaction = _dataContext.Database.BeginTransaction())
                            {
                                var results = (from a in _dataContext.Customer
                                                             join b in _dataContext.CustomersAccount on a.Id equals b.CustomerId
                                                             join c in _dataContext.LoanRequest on b.Id equals c.CustomerAccountId
                                                             where a.Id == customer.Id
                                                             select c).ToList();

                                var countunclearedloans = results.Where(x => x.Status != (int)Enumerations.LoanStatus.Cleared && x.Status != (int)Enumerations.LoanStatus.Rejected && x.Status != (int)Enumerations.LoanStatus.CustomerRejected && x.Status != (int)Enumerations.LoanStatus.Review && x.Status != (int)Enumerations.LoanStatus.TransactionReversed).Count();

                                if (countunclearedloans == 0)
                                {
                                    var generatenextcustomerloanaccountnumber = string.Empty;
                                    var findlastcustomerloanaccountnumber = _dataContext.CustomersAccount.Where(x => x.CustomerId == customer.Id && x.AccountType == (int)Enumerations.ProductCode.Loan).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                                    if (findlastcustomerloanaccountnumber == null)
                                    {
                                        generatenextcustomerloanaccountnumber = string.Format("{0}{1}{2}", (int)Enumerations.ProductCode.Loan, customer.CustomerNumber, "001");
                                    }

                                    else
                                    {
                                        string lastaccountnumber = findlastcustomerloanaccountnumber.AccountNumber;
                                        string x = (lastaccountnumber.Length > 3) ? lastaccountnumber.Substring(lastaccountnumber.Length - 3, 3) : lastaccountnumber;
                                        generatenextcustomerloanaccountnumber = string.Format("{0}{1}00{2}", (int)Enumerations.ProductCode.Loan, customer.CustomerNumber, int.Parse(x) + 1);
                                    }

                                    Guid customerLoanAccountId = Guid.NewGuid();
                                    CustomersAccount customersaccount = new CustomersAccount()
                                    {
                                        AccountNumber = generatenextcustomerloanaccountnumber,
                                        IsActive = true,
                                        AccountType = (int)Enumerations.ProductCode.Loan,
                                        CustomerId = customer.Id,
                                        Id = customerLoanAccountId,
                                    };
                                    
                                    _dataContext.CustomersAccount.Add(customersaccount);

                                    //get default micro loan
                                    int installments = 0;
                                    decimal interest = 0m;
                                    if (loanproduct.PaymentFrequencyType == Enumerations.PaymentFrequency.Monthly)
                                    {
                                        installments = loanproduct.Term;
                                    }
                                    else if (loanproduct.PaymentFrequencyType == Enumerations.PaymentFrequency.Weekly)
                                    {
                                        installments = loanproduct.Term * 4;
                                    }

                                    if (PartnerInterestChargeRate <= 0m)
                                    {
                                        var interestratepermonth = loanproduct.InterestValue / 12;
                                        interest = (interestratepermonth * loanproduct.Term * Convert.ToDecimal(TransactionAmount)) / 100;
                                    }

                                    else
                                    {
                                        var interestratepermonth = PartnerInterestChargeRate / 12;
                                        interest = (interestratepermonth * loanproduct.Term * Convert.ToDecimal(TransactionAmount)) / 100;
                                    }

                                    var loanRequestId = Guid.NewGuid();
                                    var nextPaymentDate = DateTime.Today.AddMonths(1);
                                    var createnewloanRequest = new LoanRequest()
                                    {
                                        Id = loanRequestId,
                                        BranchId = customer.BranchId,
                                        CreatedDate = DateTime.Now,
                                        CustomerAccountId = customerLoanAccountId,
                                        DisbursedAmount = Convert.ToDecimal(TransactionAmount),
                                        DisbursementDate = DateTime.Now,
                                        InterestAmount = interest,
                                        LoanAmount = Convert.ToDecimal(TransactionAmount),
                                        LoanReferenceNumber = GenerateRandomLoanReferenceNumber(8),
                                        LoanStartDate = DateTime.Now,
                                        LoanEndDate = DateTime.Now,
                                        LoanType = (int)Enumerations.LoanType.Individual,
                                        MCCMNC = (int)Enumerations.MobileNetworkCode.KE_Safaricom,
                                        NegotiatedInstallments = 1,
                                        NextPaymentDate = nextPaymentDate,
                                        PrimaryAccountNumber = MSISDN,
                                        ProcessingStatus = (int)Enumerations.ProcessingStatus.Disburse,
                                        RegisteredBy = string.Format("USSDUSER>>{0}", customer.FullName),
                                        RelationshipManagerId = customer.RelationshipManagerId,
                                        Status = (int)Enumerations.LoanStatus.Disbursed,
                                        SystemTraceAuditNumber = loanRequestId,
                                        Type = Enumerations.EFTType.AccountToMobile,
                                        LoanProductId = loanproduct.Id
                                    };

                                    _dataContext.LoanRequest.Add(createnewloanRequest);

                                    var headOfficeBranch = _dataContext.Branch.Where(x => x.BranchCode == "000").Select(x => x.C2BNumber).FirstOrDefault();
                                    var totalLoanAmount = (Convert.ToDecimal(TransactionAmount) + interest);
                                    //Create email alert and insert in the database
                                    StringBuilder textbodyBuilder = new StringBuilder();
                                    textbodyBuilder.Append(string.Format("Dear {0}, we have sent a total of KSH {1} via M-Pesa.", customer.FirstName, TransactionAmount));
                                    textbodyBuilder.Append(string.Format("Your repayment of KSH {0} is due to Paybill {1} by {2}", totalLoanAmount, PaybillNumber, nextPaymentDate.ToString("dd/MM/yyyy")));
                                    var textalert = new TextAlert
                                    {
                                        TextMessageRecipient = string.Format("{0}", MSISDN),
                                        TextMessageBody = string.Format("{0}", textbodyBuilder),
                                        TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                        TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                        TextMessageSecurityCritical = false,
                                        TextMessageSendRetry = 3
                                    };

                                    _dataContext.TextAlert.Add(textalert);

                                    #region Debit Loan Transaction History Table
                                    var defaultsavingproductglaccount = _dataContext.SavingsProduct.Where(x => x.IsDefault == true).FirstOrDefault();
                                    
                                    int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                    LoanTransactionHistory debitloanaccount = new LoanTransactionHistory()
                                    {
                                        LoanRequestId = loanRequestId,
                                        ChartofAccountId = loanproduct.LoanGLAccountId,
                                        ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                        TransactionType = Enumerations.TransactionType.Debit,
                                        CustomersAccountId = customerLoanAccountId,
                                        Description = "Loan Request",
                                        CreatedBy = string.Format("USSDUSER>>{0}", customer.FullName),
                                        IsApproved = true,
                                        TransactionCategory = (int)Enumerations.TransactionCategory.Approval,
                                        TransactionAmount = Convert.ToDecimal(TransactionAmount),
                                        BranchId = customer.BranchId,
                                        TransactionBatchNumber = batchNo
                                    };

                                    _dataContext.LoanTransactionHistory.Add(debitloanaccount);
                                    #endregion

                                    #region Credit Loan Transaction History
                                    var customerSavingAccountId = _dataContext.CustomersAccount.Where(x => x.CustomerId == customer.Id && x.AccountType == (int)Enumerations.ProductCode.Savings).Select(x => x.Id).FirstOrDefault();
                                    var mpesaRepaymentAccount = _dataContext.SystemGeneralLedgerAccountMapping.Where(x => x.SystemGeneralLedgerAccountCode == Enumerations.SystemGeneralLedgerAccountCode.MPesaSettlementAccount).Select(x => x.ChartofAccountId).FirstOrDefault();
                                    LoanTransactionHistory creditsavingglaccount = new LoanTransactionHistory()
                                    {
                                        LoanRequestId = loanRequestId,
                                        ChartofAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                        ContraAccountId = loanproduct.LoanGLAccountId,
                                        TransactionType = Enumerations.TransactionType.Credit,
                                        CustomersAccountId = customerSavingAccountId,
                                        Description = "Loan Request",
                                        IsApproved = true,
                                        TransactionCategory = (int)Enumerations.TransactionCategory.Approval,
                                        CreatedBy = string.Format("USSDUSER>>{0}", customer.FullName),
                                        TransactionAmount = Convert.ToDecimal(TransactionAmount),
                                        BranchId = customer.BranchId,
                                        TransactionBatchNumber = batchNo
                                    };
                                    _dataContext.LoanTransactionHistory.Add(creditsavingglaccount);
                                    #endregion

                                    #region Debit Loan Transaction History
                                    int batchNo1 = RandomBatchNumberGenerator.GetRandomNumber();
                                    LoanTransactionHistory debitsavingsaccount = new LoanTransactionHistory()
                                    {
                                        LoanRequestId = loanRequestId,
                                        ChartofAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                        ContraAccountId = mpesaRepaymentAccount,
                                        TransactionType = Enumerations.TransactionType.Debit,
                                        CustomersAccountId = customerSavingAccountId,
                                        Description = "Loan Request",
                                        IsApproved = true,
                                        TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement,
                                        CreatedBy = string.Format("USSDUSER>>{0}", customer.FullName),
                                        TransactionAmount = Convert.ToDecimal(TransactionAmount),
                                        BranchId = customer.BranchId,
                                        TransactionBatchNumber = batchNo1
                                    };
                                    _dataContext.LoanTransactionHistory.Add(debitsavingsaccount);
                                    #endregion

                                    #region Credit Loan Transaction History
                                    LoanTransactionHistory creditmpesaglaccount = new LoanTransactionHistory()
                                    {
                                        LoanRequestId = loanRequestId,
                                        ChartofAccountId = mpesaRepaymentAccount,
                                        ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                        TransactionType = Enumerations.TransactionType.Credit,
                                        CustomersAccountId = customerSavingAccountId,
                                        Description = "Loan Request",
                                        TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement,
                                        IsApproved = true,
                                        CreatedBy = string.Format("USSDUSER>>{0}", customer.FullName),
                                        TransactionAmount = Convert.ToDecimal(TransactionAmount),
                                        BranchId = customer.BranchId,
                                        TransactionBatchNumber = batchNo1
                                    };
                                    _dataContext.LoanTransactionHistory.Add(creditmpesaglaccount);
                                    #endregion

                                    if (_dataContext.SaveChanges() > 0)
                                    {
                                        AcceptMessage = "Loan request received successfully.";
                                        transaction.Commit();
                                        TransactionAmount = null;
                                        loanRequest = new List<LoanRequest>();
                                        PIN = string.Empty;
                                        NewPIN = string.Empty;
                                    }
                                    else
                                    {
                                        AcceptMessage = "An error has occurred while processing your request. Please try again later.";
                                    }
                                }
                                else
                                {
                                    AcceptMessage = "Sorry, you have an uncleared loan. Clear it first and try again later.";
                                }
                            }
                                   
                            break;
                        
                        case States.ChangePIN:
                            //commit change pin
                            AcceptMessage = "Your PIN has been changed!";

                            var newPasswordHash = PasswordHash.CreateHash(NewPIN);

                            var findCustomer = _dataContext.Customer.Where(x => x.Id == customer.Id).FirstOrDefault();

                            if (findCustomer != null)
                            {
                                findCustomer.PIN = newPasswordHash;

                                var entry = _dataContext.Entry(findCustomer);
                                entry.State = EntityState.Modified;
                                _dataContext.SaveChanges();
                            }
                            PIN = string.Empty;
                            NewPIN = string.Empty;
                            break;
                    }
                }); 
        }

        public LoanRequestUSSD(string msisdn)
            : this()
        {
            Seed(msisdn);
        }

        private void Seed(string msisdn)
        {
            if (!string.IsNullOrWhiteSpace(msisdn))
            {
                MSISDN = msisdn;

                customer = _dataContext.Customer.Where(x => x.MobileNumber == msisdn).FirstOrDefault();
                loanproduct = _dataContext.LoanProduct.Where(x => x.IsMicroLoan == true).FirstOrDefault();
                OnlineCheckoutURL = ConfigurationManager.AppSettings["OnlineCheckoutURL"].ToString();
                PaybillNumber = _dataContext.Branch.Where(x => x.BranchCode.Equals("000")).Select(x => x.C2BNumber).FirstOrDefault();
                if (customer == null)
                    Machine.Fire(Triggers.Terminate);
                else
                {
                    customerAccount = _dataContext.CustomersAccount.Where(x => x.CustomerId == customer.Id && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
                    var idStartDate = new SqlParameter { ParameterName = "@CustomerAccountID", Value = customerAccount.Id };

                    var results = _dataContext.Database.SqlQuery<CustomerAccountInfo>("sp_Customer_Savings_Loan_AccountBalance @CustomerAccountID", idStartDate).Cast<CustomerAccountInfo>().First();

                    LoanBalance = results.TotalAmountDue;

                    PartnerName = customer.Partner;
                    PartnerInterestChargeRate = customer.Partner.InterestChargeRate;
                    loanRequest = (from a in _dataContext.Customer
                                   join b in _dataContext.CustomersAccount on a.Id equals b.CustomerId
                                   join c in _dataContext.LoanRequest on b.Id equals c.CustomerAccountId
                                   where a.Id == customer.Id
                                   select c).ToList();

                }
            }
        }

        public void Transition(string input)
        {
            var trigger = Triggers.Initial;

            if (Enum.TryParse(input, out trigger))
            {
                if (Enum.IsDefined(typeof(Triggers), trigger))
                {
                    if (Machine.CanFire(trigger))
                    {
                        switch (trigger)
                        {
                            case Triggers.Accept:
                                Machine.Fire(ParameterizedAcceptTrigger, (int)CurrentState);
                                break;
                            case Triggers.Decline:
                                ReceiverPartyIdentifier = string.Empty;
                                TransactionAmount = string.Empty;
                                PIN = string.Empty;
                                NewPIN = string.Empty;
                                Machine.Fire(trigger);
                                break;
                            default:
                                Machine.Fire(trigger);
                                break;
                        }
                    }
                }
                else CaptureInput(input);
            }
            else CaptureInput(input);
        }

        private void CaptureInput(string input)
        {
            switch (CurrentState)
            {
                case States.ChangePIN:
                    if (string.IsNullOrWhiteSpace(PIN) && PasswordHash.ValidatePassword(input, customer.PIN))
                        PIN = input;
                    else if (string.IsNullOrWhiteSpace(NewPIN) && Regex.IsMatch(input, "^[0-9]{4}$"))
                        NewPIN = input;
                    break;
                case States.LoanRequest:
                    if (string.IsNullOrWhiteSpace(TransactionAmount) && input.Is<decimal>())
                        TransactionAmount = input;                    
                    else if (string.IsNullOrWhiteSpace(PIN) && PasswordHash.ValidatePassword(input, customer.PIN))
                        PIN = input;
                    break;
                case States.LoanBalance:
                    if (string.IsNullOrWhiteSpace(PIN) && PasswordHash.ValidatePassword(input, customer.PIN))
                        PIN = input;
                    break;
                case States.PayLoan:
                    if (string.IsNullOrWhiteSpace(LoanBalanceRepaymentType))
                        LoanBalanceRepaymentType = input;
                    else if (string.IsNullOrWhiteSpace(TransactionAmount) && input.Is<decimal>())
                        TransactionAmount = input;                    
                    //else if (string.IsNullOrWhiteSpace(PIN) && PasswordHash.ValidatePassword(input, customer.PIN))
                    //    PIN = input;
                    break; 

                default:
                    break;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            var customer = _dataContext.Customer.Where(x => x.MobileNumber == MSISDN).FirstOrDefault();
            var countunclearedloans = loanRequest.Where(x => x.Status != (int)Enumerations.LoanStatus.Cleared && x.Status != (int)Enumerations.LoanStatus.Rejected && x.Status != (int)Enumerations.LoanStatus.CustomerRejected && x.Status != (int)Enumerations.LoanStatus.Review && x.Status != (int)Enumerations.LoanStatus.TransactionReversed).Count();
            switch (CurrentState)
            {
                case States.Initial:

                    if (customer != null)
                    {
                        if (countunclearedloans == 0)
                        {
                            sb.Append("CON ");
                            sb.AppendLine(string.Format("Welcome {0} of {1}, qualified amount Ksh {2}", customer.FullName, PartnerName.PartnerName, customer.PreQualifiedAmount));
                            sb.AppendLine("1. Request Loan");
                            sb.AppendLine("2. Loan Balance");
                            sb.AppendLine("3. Change PIN");
                            
                        }

                        else
                        {
                            sb.Append("CON ");
                            sb.AppendLine(string.Format("Welcome {0} of {1}, your loan balance is Ksh {2}. Repay using Paybill No. {3}", customer.FullName, PartnerName.PartnerName, LoanBalance, PaybillNumber));
                            sb.AppendLine("4. Pay Loan");
                        }
                    }
                    else
                    {
                        sb.Append("END ");
                        sb.AppendLine("Sorry, you are not registered for this service.");
                    }

                    break;

                case States.Accepted:
                    sb.Append("END ");
                    sb.AppendLine(AcceptMessage);
                    //sb.AppendLine("33. Main Menu");
                    //sb.AppendLine("99. Exit");
                    break;

                case States.Declined:
                    sb.Append("END ");
                    sb.AppendLine("Thank you!");                    
                    break;

                case States.LoanBalance:
                    decimal accountBalance = LoanAccountBalance(customer.Id);
                    sb.Append("CON ");
                    sb.AppendLine("Loan Balance");
                    sb.AppendLine("Your loan balance is KES " + accountBalance);
                    sb.AppendLine("33. Main Menu");
                    sb.AppendLine("99. Exit");
                    break;

                case States.PayLoan:

                    if (customer != null)
                    {
                        ReceiverPartyIdentifier = customer.MobileNumber;
                        decimal amount = Convert.ToDecimal(TransactionAmount);
                        if (string.IsNullOrWhiteSpace(LoanBalanceRepaymentType))
                        {
                            sb.Append("CON ");
                            sb.AppendLine("Loan Repayment Options:");
                            sb.AppendLine("8. Full Payment");
                            sb.AppendLine("9. Part Payment");
                        }
                        else if (LoanBalanceRepaymentType.Equals("8"))
                        {
                            TransactionAmount = string.Concat(LoanBalance);
                            if (!string.IsNullOrWhiteSpace(TransactionAmount))
                            {
                                sb.Append("CON ");
                                sb.AppendLine("Confirm:");
                                sb.AppendLine(string.Format("Pay {0} for account {1}?", FormatAmount(TransactionAmount), ReceiverPartyIdentifier));
                                sb.AppendLine("98. Accept");
                                sb.AppendLine("99. Decline");
                            }
                        }
                        else if (LoanBalanceRepaymentType.Equals("9"))
                        {
                            if (string.IsNullOrWhiteSpace(TransactionAmount))
                            {
                                sb.Append("CON ");
                                sb.AppendLine("Enter Amount:");
                            }
                            
                            else
                            {
                                sb.Append("CON ");
                                sb.AppendLine("Confirm:");
                                sb.AppendLine(string.Format("Pay {0} for account {1}?", FormatAmount(TransactionAmount), ReceiverPartyIdentifier));
                                sb.AppendLine("98. Accept");
                                sb.AppendLine("99. Decline");
                            }
                        }
                        else
                        {
                            sb.Append("CON ");
                            sb.AppendLine("Loan Repayment Options:");
                            sb.AppendLine("8. Full Payment");
                            sb.AppendLine("9. Part Payment");
                        }
                    }

                    break;
                case States.LoanRequest:
                    if (customer != null)
                    {
                        ReceiverPartyIdentifier = customer.MobileNumber;
                        decimal transAmount = Convert.ToDecimal(TransactionAmount);
                        if (string.IsNullOrWhiteSpace(TransactionAmount))
                        {
                            sb.Append("CON ");
                            sb.AppendLine("Enter Loan Amount:");
                        }
                        else if (transAmount > customer.PreQualifiedAmount)
                        {
                            TransactionAmount = string.Empty;
                            sb.Append("CON ");
                            sb.AppendLine("Amount entered greater than qualified amount.");
                            sb.AppendLine("Enter Amount:");
                        }
                        else if (transAmount > loanproduct.MaximumAmount || transAmount < loanproduct.MinimumAmount)
                        {
                            TransactionAmount = string.Empty;
                            sb.Append("CON ");
                            sb.AppendLine(string.Format("Amount must be within {0} - {1}", loanproduct.MinimumAmount, loanproduct.MaximumAmount));
                            sb.AppendLine("Enter Amount:");
                        }

                        else if (string.IsNullOrWhiteSpace(PIN))
                        {
                            sb.Append("CON ");
                            sb.AppendLine("Enter PIN:");
                        }
                        else
                        {
                            sb.Append("CON ");
                            sb.AppendLine("Confirm:");
                            sb.AppendLine(string.Format("Borrow {0} to {1}?", FormatAmount(TransactionAmount), ReceiverPartyIdentifier));
                            sb.AppendLine("98. Accept");
                            sb.AppendLine("99. Decline");
                        }
                    }
                    break;

                case States.ChangePIN:
                    if (string.IsNullOrWhiteSpace(PIN))
                    {
                        sb.Append("CON ");
                        sb.AppendLine("Enter Current PIN:");
                    }
                    else if (string.IsNullOrWhiteSpace(NewPIN))
                    {
                        sb.Append("CON ");
                        sb.AppendLine("Enter New PIN:");
                    }
                    else
                    {
                        sb.Append("CON ");
                        sb.AppendLine("Change PIN:");
                        sb.AppendLine(string.Format("Change PIN from {0} to {1}?", PIN, NewPIN));
                        sb.AppendLine("98. Accept");
                        sb.AppendLine("99. Decline");
                    }
                    break;

                case States.Terminated:
                    sb.Append("END ");
                    sb.AppendLine("Sorry, you are not registered for this service.");
                    break;

                default:
                    sb.Append("END ");
                    sb.AppendLine("Invalid selection!");
                    break;
            }

            return sb.ToString();
        }

        static string FormatAmount(string amount)
        {
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.CurrencySymbol = "KES ";

            var result = 0m;

            if (!string.IsNullOrWhiteSpace(amount))
                decimal.TryParse(amount, out result);

            return string.Format(nfi, "{0:C}", result);
        }

        private decimal LoanAccountBalance(Guid customerId)
        {
            decimal loanAccountBalance = 0m;

            var customerAccountId = _dataContext.CustomersAccount.Where(x => x.CustomerId == customerId && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();

            if(customerAccountId != null)
            {
                var idStartDate = new SqlParameter { ParameterName = "@CustomerAccountID", Value = customerAccountId.Id };

                var results = _dataContext.Database.SqlQuery<CustomerAccountInfo>("sp_Customer_Savings_Loan_AccountBalance @CustomerAccountID", idStartDate).Cast< CustomerAccountInfo>().First();

                loanAccountBalance = results.TotalAmountDue;
            }

            return loanAccountBalance;
        }
              
        public string GenerateRandomLoanReferenceNumber(int length)
        {
            string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[length];
            Random rd = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }
    }

    public static class Extensions
    {
        public static bool Is<T>(this string s)
        {
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));

            try
            {
                object val = converter.ConvertFromInvariantString(s);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }

    public class Params
    {
        public string serviceCode { get; set; }

        public string sessionId { get; set; }

        public string phoneNumber { get; set; }

        public string text { get; set; }
    }

    public class OnlineCheckoutParams
    {
        //Customer mobile number
        public string msisdn { get; set; }

        //Paybill or Till number
        public string merchantId { get; set; }

        //Unique Merchant password provided by safaricom
        public string passKey { get; set; }

        //Transaction Amount
        public string transactionAmount { get; set; }

        //Accoubt
        public string referenceId { get; set; }
    }
}
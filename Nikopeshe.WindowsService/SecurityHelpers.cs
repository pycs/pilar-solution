﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Nikopeshe.WindowsService
{
    public class SecurityHelpers
    {
        public static void ProtectConfigurationFile()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConfigurationSection connStrings = config.GetSection("connectionStrings");

            //Define the Dpapi provider name.
            string provider = "DataProtectionConfigurationProvider";

            //Get the section to protect - connStrings.
            //ConfigurationSection connStrings = config.ConnectionStrings;

            if (connStrings != null)
            {
                if (!connStrings.SectionInformation.IsProtected)
                {
                    if (!connStrings.ElementInformation.IsLocked)
                    {
                        //Protect the section.
                        connStrings.SectionInformation.ProtectSection(provider);
                        connStrings.SectionInformation.ForceSave = true;
                        config.Save(ConfigurationSaveMode.Full);
                    }
                }
            }
        }

        public static void UnProtectConfigurationFile()
        {
            // Get the application configuration file.
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConfigurationSection connStrings = config.GetSection("connectionStrings");
            // Get the section to unprotect - connStrings.
            //ConfigurationSection connStrings = config.ConnectionStrings;

            if (connStrings != null)
            {
                if (connStrings.SectionInformation.IsProtected)
                {
                    if (!connStrings.ElementInformation.IsLocked)
                    {
                        // Unprotect the section.
                        connStrings.SectionInformation.UnprotectSection();

                        connStrings.SectionInformation.ForceSave = true;
                        config.Save(ConfigurationSaveMode.Full);
                    }
                }
            }

            // Get the section to unprotect - appSettings.
            ConfigurationSection appSettings = config.GetSection("appSettings");

            if (appSettings != null)
            {
                if (appSettings.SectionInformation.IsProtected)
                {
                    if (!appSettings.ElementInformation.IsLocked)
                    {
                        // Unprotect the section.
                        appSettings.SectionInformation.UnprotectSection();

                        appSettings.SectionInformation.ForceSave = true;
                        config.Save(ConfigurationSaveMode.Full);
                    }
                }
            }
        }
    }
}

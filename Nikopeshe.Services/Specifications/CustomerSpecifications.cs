﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Specifications
{
    public class CustomerSpecifications
    {
        public static ISpecification<Customer> FindCustomerById(Guid id, string text)
        {
            Specification<Customer> specification = new DirectSpecification<Customer>(c => c.Id == id);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var bankAccountNumberSpec = new DirectSpecification<Customer>(c => c.MobileNumber.Contains(text));

                var nameSpec = new DirectSpecification<Customer>(c => c.IDNumber.Contains(text));                

                specification &= (bankAccountNumberSpec | nameSpec );
            }

            return specification;
        }

        public static Specification<Customer> DefaultSpec(string text)
        {
            Specification<Customer> specification = new DirectSpecification<Customer>(c => c.CustomerType == Enumerations.CustomerTypes.Individual);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var companyNameSpec = new DirectSpecification<Customer>(c => c.IDNumber.Contains(text));

                var bankAccountNumberSpec = new DirectSpecification<Customer>(c => c.MobileNumber.Contains(text));

                //var remarksSpec = new DirectSpecification<Customer>(c => c.FullName.Contains(text));

                specification &= (companyNameSpec | bankAccountNumberSpec );
            }

            return specification;
        }

        public static ISpecification<Customer> FindCustomerByBranchId(Guid id, string text)
        {            
            Specification<Customer> specification = new DirectSpecification<Customer>(c => c.BranchId == id && c.CustomerType == Enumerations.CustomerTypes.Individual);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var bankAccountNumberSpec = new DirectSpecification<Customer>(c => c.MobileNumber.Contains(text));

                var nameSpec = new DirectSpecification<Customer>(c => c.IDNumber.Contains(text));

                specification &= (bankAccountNumberSpec | nameSpec);
            }

            return specification;
        }
    }
}

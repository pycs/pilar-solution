﻿using Nikopeshe.Data;
using Nikopeshe.Entity;
using Nikopeshe.Services.Specifications;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Nikopeshe.Services
{
    public class Repository<TEntity> : IDisposable, IRepository<TEntity> where TEntity : BaseEntity
    {
        private IDataContext _datacontext;   

        private IDbSet<TEntity> _entity
        {
            get { return this._datacontext.Set<TEntity>(); }
        }

        public Repository(DataContext context)
        {
            this._datacontext = context;           
        }

        public IQueryable<TEntity> GetAll(IsolationLevel isolationLevel)
        {
            using (var context = new DataContext())
            {
                using (var dbContextTransaction = context.Database.BeginTransaction(isolationLevel))
                {
                    try
                    {
                        return _entity.AsQueryable();
                    }

                    catch
                    {

                    }
                }
            }
            return null;
        }
        
        public TEntity GetById(object Id)
        {
            return _entity.Find(Id);
        }

        public void Update(TEntity entity, ServiceHeader serviceHeader)
        {
            if (entity == null)
                throw new ArgumentNullException("null entity");

            
            this._datacontext.SaveChanges(serviceHeader);
        }

        public void Insert(TEntity entity, ServiceHeader serviceHeader)
        {
            if (entity == null)
                throw new ArgumentNullException("null entity");

            _entity.Add(entity);
            this._datacontext.SaveChanges(serviceHeader);
        }

        public void Delete(TEntity entity, ServiceHeader serviceHeader)
        {
            if (entity == null)
                throw new ArgumentNullException("null entity");

            _entity.Remove(entity);
            this._datacontext.SaveChanges(serviceHeader);
        }
       
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }                   

        public virtual void Dispose(bool disposing)
        {
            if(disposing)
            {
                if(this._datacontext != null)
                {
                    this._datacontext.Dispose();
                    this._datacontext = null;                    
                }
            }
        }

        public IEnumerable<TEntity> AllMatching(ISpecification<TEntity> specification, params Expression<Func<TEntity, object>>[] children)
        {
            IQueryable<TEntity> items = null;

            var objectSet = _datacontext.Set<TEntity>();

            if (children != null && children.Any())
            {
                IQueryable<TEntity> entitySet = null;

                Array.ForEach(children, child =>
                {
                    entitySet = (entitySet ?? objectSet).Include(child);
                });

                items = entitySet.Where(specification.SatisfiedBy());
            }
            else items = objectSet.Where(specification.SatisfiedBy());

            return items;
        }

        public PagedCollection<TEntity> AllMatchingPaged(ISpecification<TEntity> specification, int pageIndex, int pageSize, List<string> sortFields, bool ascending, params Expression<Func<TEntity, object>>[] children)
        {
            IQueryable<TEntity> items = null;

            var totalItems = 0;

            var objectSet = _datacontext.Set<TEntity>();

            if (children != null && children.Any())
            {
                IQueryable<TEntity> entitySet = null;

                Array.ForEach(children, child =>
                {
                    entitySet = (entitySet ?? objectSet).Include(child);
                });

                items = entitySet.Where(specification.SatisfiedBy());
            }
            else items = objectSet.Where(specification.SatisfiedBy());

            totalItems = items.Count();

            if (pageSize != 0)
            {
                if (sortFields != null && sortFields.Any())
                {
                    sortFields.ForEach(s => items = ExpressionTreeSerializationUtils.CallOrderBy(items, s, ascending));

                    items = items.Skip(/*pageSize */ pageIndex); // NB: orderby must be called before skip(..)
                }

                items = items.Take(pageSize);
            }

            return new PagedCollection<TEntity> { PageIndex = pageIndex, PageSize = pageSize, Items = items.AsEnumerable(), TotalItems = totalItems };
        }
    }
}

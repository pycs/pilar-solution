﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Data
{
    public class CustomerAccountStatement
    {
        public string AccountName { get; set; }

        public string Product { get; set; }

        public string id { get; set; }

        public DateTime TrxDate { get; set; }

        public string PrimaryDescription { get; set; }

        public string SecondaryDescription { get; set; }

        public string reference { get; set; }

        public decimal debit { get; set; }

        public decimal credit { get; set; }

        public decimal RunningTotal { get; set; }

        public decimal InterestDR { get; set; }

        public decimal InterestCR { get; set; }

        public decimal IntBalance { get; set; }
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface ILoanRequestChargeService
    {
        void AddLoanRequestCharge(LoanRequestCharge loanrequestcharge, ServiceHeader serviceHeader);

        IList<LoanRequestCharge> FindAllLoanRequestChargesByLoanRequestId(Guid LoanRequestId);
    }
}

﻿using Infrastructure.Crosscutting.NetFramework.Utils;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.DTO;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.Services.Specifications;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;

namespace Nikopeshe.Services.InterfaceImplementations
{    
    public class CustomerService : ICustomerService
    {
        private IRepository<Customer> customerrepository;

        private IRepository<LoanRequest> loanrequestrepository;

        private IRepository<CustomersAccount> customeraccountrepository;

        private static readonly object _lockObj = new object();

        string ErrorMessage = string.Empty;
        public DataContext dataContext = new DataContext();
        public CustomerService(IRepository<Customer> _customerrepository, IRepository<LoanRequest> _loanrequestrepository, IRepository<CustomersAccount> _customeraccountrepository)
        {
            if (_customeraccountrepository == null || _customerrepository == null || _loanrequestrepository == null)
                throw new ArgumentNullException("null repository");

            this.customerrepository = _customerrepository;
            this.loanrequestrepository = _loanrequestrepository;
            this.customeraccountrepository = _customeraccountrepository;
        }

        public void AddNewCustomer(Customer customer, ServiceHeader serviceHeader)
        {
            try
            {
                if (customer == null)
                    throw new ArgumentNullException("null entity");                

                customerrepository.Insert(customer, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }           
        }

        public bool AddNewCustomerTest(Customer customer, ServiceHeader serviceHeader)
        {
            bool result = false;
            try
            {                
                if (customer != null)
                    throw new InvalidOperationException("null entity");

                customerrepository.Insert(customer, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }

            return result;
        }

        public IList<Customer> GetAllCustomers(IsolationLevel isolationLevel)
        {
            return customerrepository.GetAll(isolationLevel).ToList();
        }

        public IList<Customer> GetCustomerByMobileNumber(string mobilenumber)
        {
            return customerrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.MobileNumber == mobilenumber).ToList();
        }

        public void UpdateCustomer(Customer customer, ServiceHeader serviceHeader)
        {
            if (customer == null)
                throw new ArgumentNullException("null entity");
            
            customerrepository.Update(customer, serviceHeader);
        }

        public void DeleteCustomer(Customer customer, ServiceHeader serviceHeader)
        {
            if (customer == null)
                throw new ArgumentNullException("null entity");

            customerrepository.Delete(customer, serviceHeader);
        }

        public Customer FindCustomerById(Guid CustomerId)
        {
            if (CustomerId == Guid.Empty)
                throw new ArgumentNullException("null entity");

            return customerrepository.GetById(CustomerId);            
        }

        public bool CheckDuplicateCustomer(string mobilenumber, string idnumber)
        {
            if (string.IsNullOrWhiteSpace(idnumber))
                throw new ArgumentNullException("values cannot be null");

            if (string.IsNullOrWhiteSpace(mobilenumber))
                mobilenumber = "";

            var count = customerrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.MobileNumber == mobilenumber || x.IDNumber == idnumber).Count();

            if (count >= 1)
                return true;

            return false;
        }

        public bool ValidateGroupRole(Enumerations.GroupRoles groupRole)
        {
            var count = 0;
            if(groupRole != Enumerations.GroupRoles.Member)
            {
            count = customerrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.GroupRole == groupRole).Count();
            }
            if (count >= 1)
                return true;

            return false;
        }

        public Customer FindCustomerByMobileNumber(string mobilenumber)
        {
            if (string.IsNullOrWhiteSpace(mobilenumber))
                throw new ArgumentNullException("string cannot be null or white space");

            return customerrepository.GetAll(IsolationLevel.ReadCommitted).FirstOrDefault(x => x.MobileNumber == mobilenumber);
        }

        public Customer FindCustomerByIdNumber(string IdNumber)
        {
            if (string.IsNullOrWhiteSpace(IdNumber))
                throw new ArgumentNullException("string cannot be null or white space");

            return customerrepository.GetAll(IsolationLevel.ReadCommitted).FirstOrDefault(x => x.IDNumber == IdNumber);
        }       

        public bool ValidateCustomerAge(DateTime dateofbirth)
        {
            DateTime today = DateTime.Today;
            int age = today.Year - dateofbirth.Year;
            if (age >= 18)
                return true;

            return false;
        }   
   
        public int GetCustomerEnabledNotificationMode(Guid CustomerId)
        {
            if (string.IsNullOrWhiteSpace(CustomerId.ToString()))
                throw new ArgumentNullException("Customer id cannot be null");

            int result = 0;

            Customer GetCustomerById = customerrepository.GetById(CustomerId);

            if (GetCustomerById.EmailAlertsEnabled)
                result =  1;

            if (GetCustomerById.SMSAlertsEnabled)
                result = 2;

            if (GetCustomerById.SMSAlertsEnabled && GetCustomerById.EmailAlertsEnabled)
                result = 3;

            return result;
        }

        public Customer FindCustomerByApprovalCode(string ApprovalCode)
        {
            if (string.IsNullOrWhiteSpace(ApprovalCode))
                throw new ArgumentNullException("string cannot be null");

            var FindCustomerByApprovalCode = customerrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.ApprovalCode == ApprovalCode);

            return FindCustomerByApprovalCode.FirstOrDefault();
        }

        public string NextCustomerNumber()
        {
            var nextCustomerNumber = 0;
            lock (_lockObj)
            {
                var findlastcustomernumber = customerrepository.GetAll(IsolationLevel.ReadCommitted).OrderByDescending(x => x.CreatedDate).Select(x => x.CustomerNumber).FirstOrDefault();

                if (findlastcustomernumber == null)
                {
                    nextCustomerNumber = 1;
                }

                else
                {
                    nextCustomerNumber = int.Parse(findlastcustomernumber) + 1;
                }
            }

            return nextCustomerNumber.ToString("0000000");
        }

        public string NextCustomerSavingAccountNumber(string customernumber)
        {
            var generatenextcustomeraccountnumber = string.Empty;
            lock (_lockObj)
            {
                var savingsproductcode = string.Concat((int)Enumerations.ProductCode.Savings);
                var findlastcustomerccountnumber = customeraccountrepository.GetAll(IsolationLevel.ReadCommitted).OrderByDescending(x => x.CreatedDate).FirstOrDefault();

                generatenextcustomeraccountnumber = string.Format("{0}{1}{2}", savingsproductcode, customernumber, "001");

            }
            return generatenextcustomeraccountnumber.ToString();
        }

        public string NextCustomerLoanAccountNumber(string CustomerNumber, Guid CustomerId, string loanproductcode)
        {
            var generatenextcustomerloanaccountnumber = string.Empty;
            lock (_lockObj)
            {
                var findlastcustomerloanaccountnumber = customeraccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CustomerId == CustomerId && x.AccountType == (int)Enumerations.ProductCode.Loan).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                if (findlastcustomerloanaccountnumber == null)
                {
                    generatenextcustomerloanaccountnumber = string.Format("{0}{1}{2}", loanproductcode, CustomerNumber, "001");
                }

                else
                {
                    string lastaccountnumber = findlastcustomerloanaccountnumber.AccountNumber;
                    string x = (lastaccountnumber.Length > 3) ? lastaccountnumber.Substring(lastaccountnumber.Length - 3, 3) : lastaccountnumber;
                    generatenextcustomerloanaccountnumber = string.Format("{0}{1}00{2}", loanproductcode, CustomerNumber, int.Parse(x) + 1);
                }
            }

            return generatenextcustomerloanaccountnumber.ToString();
        }

        public string NextCustomerArrearsAccountNumber(string CustomerNumber, Guid CustomerId)
        {
            var generatenextcustomerarrearsaccountnumber = string.Empty;
            lock (_lockObj)
            {
                var findlastcustomerloanaccountnumber = customeraccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CustomerId == CustomerId && x.AccountType == (int)Enumerations.ProductCode.Loan).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                var loanproductcode = string.Concat((int)Enumerations.ProductCode.Arrears);
                if (findlastcustomerloanaccountnumber == null)
                {
                    generatenextcustomerarrearsaccountnumber = string.Format("00{0}{1}{2}", loanproductcode, CustomerNumber, "001");
                }

                else
                {
                    string lastaccountnumber = findlastcustomerloanaccountnumber.AccountNumber;
                    string x = (lastaccountnumber.Length > 3) ? lastaccountnumber.Substring(lastaccountnumber.Length - 3, 3) : lastaccountnumber;
                    generatenextcustomerarrearsaccountnumber = string.Format("00{0}{1}00{2}", loanproductcode, CustomerNumber, int.Parse(x) + 1);
                }
            }

            return generatenextcustomerarrearsaccountnumber.ToString();
        }

        public IList<Customer> GetCustomersByBranchId(Guid BranchId, IsolationLevel isolationLevel)
        {
            if (BranchId == Guid.Empty)
            {
                throw new ArgumentNullException("Branch id cannot be null");
            }

            var findCustomerByBranchId = customerrepository.GetAll(isolationLevel).Where(x => x.BranchId == BranchId);

            return findCustomerByBranchId.ToList();
        }

        public IList<CustomerAccountStatement> CustomerAccountStatement(string customerId, DateTime startDate, DateTime endDate)
        {
            if (string.IsNullOrWhiteSpace(customerId))
                throw new ArgumentNullException("customer id cannot be null");

            var idStartDate = new SqlParameter { ParameterName = "StartDate", Value = startDate.Date };
            var idEndDate = new SqlParameter { ParameterName = "EndDate", Value = endDate };
            var idCustomerId = new SqlParameter { ParameterName = "CustomerID", Value = customerId };

            var results = dataContext.Database.SqlQuery<CustomerAccountStatement>("sp_PilarCustomerAccountStatementByCustomerID @StartDate, @EndDate, @CustomerID", idStartDate, idEndDate, idCustomerId).ToList<CustomerAccountStatement>();

            return  results;
        }

        public PageCollectionInfo<Customer> FindCustomersFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = CustomerSpecifications.DefaultSpec(searchString);

            ISpecification<Customer> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var orderLinePagedCollection = customerrepository.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (orderLinePagedCollection != null)
            {
                var pageCollection = orderLinePagedCollection.Items.ToList();

                var totalRecordCount = orderLinePagedCollection.TotalItems;

                return new PageCollectionInfo<Customer> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }

        public IList<string> GetDistinctCountyCodes()
        {
            var findCustomerCountByCountyCode = customerrepository.GetAll(IsolationLevel.ReadCommitted).GroupBy(x => x.CountyCode).Select(x => x.FirstOrDefault()).ToList();

            var distinctCountyCodes = new List<string>();

            foreach (Customer customer in findCustomerCountByCountyCode)
            {
                distinctCountyCodes.Add(customer.CountyCode);
            }

            return distinctCountyCodes;
        }

        public int FindCustomerCountByCountyCode(string CountyCode)
        {
            if (string.IsNullOrWhiteSpace(CountyCode))
                throw new ArgumentNullException("CountyCode");

            var findCustomerCountByCountyCode = customerrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CountyCode == CountyCode).Count();

            return findCustomerCountByCountyCode;
        }

        public PageCollectionInfo<Customer> FindCustomersByBranchIdFilterInPage(Guid id, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = CustomerSpecifications.FindCustomerByBranchId(id, searchString);

            ISpecification<Customer> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var orderLinePagedCollection = customerrepository.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (orderLinePagedCollection != null)
            {
                var pageCollection = orderLinePagedCollection.Items.ToList();

                var totalRecordCount = orderLinePagedCollection.TotalItems;

                return new PageCollectionInfo<Customer> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }
    }
}

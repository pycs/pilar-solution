﻿using Microsoft.Practices.Unity.Utility;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.WindowsService.Interfaces;
using System;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Messaging;

namespace Nikopeshe.WindowsService.EmailDispatcher.Services
{
    public class Dispatcher
    {
        private MessageQueue _messageQueue;
        private DataContext datacontext = new DataContext();
        private readonly string _queuePath;
        private readonly ISmtpService _smtpService;
        
        public Dispatcher()
        {                                    
            _smtpService = new SmtpService();

            _queuePath = ConfigurationManager.AppSettings["MessageRelayQueuePath"];
        }

        public void DoWork(params string[] args)
        {
            try
            {
                if (!MessageQueue.Exists(_queuePath))
                    throw new ArgumentException("Queue does not exist at specified path", "_queuePath");
                else
                {
                    using (_messageQueue = new MessageQueue(_queuePath))
                    {
                        // enable the AppSpecific field in the messages
                        _messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                        // set the formatter to binary.
                        _messageQueue.Formatter = new BinaryMessageFormatter();

                        // set an event to listen for new messages.
                        _messageQueue.ReceiveCompleted += new ReceiveCompletedEventHandler(MessageQueue_ReceiveCompleted);

                        // Delete all messages from the queue.
                        _messageQueue.Purge();

                        // start listening
                        _messageQueue.BeginReceive();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }

        #region Event Handlers

        private void MessageQueue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                // retrieve sms alert id
                var recordId = (Guid)e.Message.Body;

                // retrieve message category
                var messageCategory = (Enumerations.MessageCategory)e.Message.AppSpecific;

                switch (messageCategory)
                {
                    case Enumerations.MessageCategory.EmailAlert:

                        #region email

                        var emailAlert = datacontext.EmailAlert.Find(recordId);

                        if (emailAlert != null)
                        {
                            switch ((Enumerations.DLRStatus)emailAlert.MailMessageDLRStatus)
                            {
                                case Enumerations.DLRStatus.UnKnown:
                                case Enumerations.DLRStatus.Pending:

                                    try
                                    {
                                        if (!string.IsNullOrWhiteSpace(emailAlert.MailMessageCC))
                                            _smtpService.SendEmail(emailAlert.MailMessageFrom, emailAlert.MailMessageTo, emailAlert.MailMessageCC, emailAlert.MailMessageSubject, emailAlert.MailMessageBody, emailAlert.MailMessageIsBodyHtml);
                                        else _smtpService.SendEmail(emailAlert.MailMessageFrom, emailAlert.MailMessageTo, emailAlert.MailMessageSubject, emailAlert.MailMessageBody, emailAlert.MailMessageIsBodyHtml);

                                        emailAlert.MailMessageDLRStatus = (int)Enumerations.DLRStatus.Delivered;
                                        emailAlert.MailMessageSendRetry = 1;
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
                                    }

                                    break;
                                default:
                                    break;
                            }
                        }

                        #endregion

                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }

            // listen for the next message.
            _messageQueue.BeginReceive();
        }

        #endregion
    }
}

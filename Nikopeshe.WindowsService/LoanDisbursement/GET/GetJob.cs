﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Nikopeshe.WindowsService.LoanDisbursement.GET
{
    public class GetJob : IJob
    {
        string CompanyId = ConfigurationManager.AppSettings["CompanyId"].ToString();
        string spsusername = ConfigurationManager.AppSettings["SPSUsername"].ToString();
        string spsapiendpoint = ConfigurationManager.AppSettings["SPSAPIEndPoint"].ToString();
        string spspassword = ConfigurationManager.AppSettings["SPSPassword"].ToString();
        string resultcode = ConfigurationManager.AppSettings["resultcode"].ToString();
        string responcecode = ConfigurationManager.AppSettings["responcecode"].ToString();

        DataContext datacontext = new DataContext();

        public void Execute(IJobExecutionContext context)
        {
            try
            {
               IList<LoanRequest> submittedloanrequests = datacontext.LoanRequest.Where(x => x.SPSStatus != (int)Enumerations.EFTStatus.Completed && x.SPSStatus != (int)Enumerations.EFTStatus.Cached && x.SPSStatus != (int)Enumerations.EFTStatus.Failed && x.SPSStatus != (int)Enumerations.EFTStatus.Rejected).ToList();
                if(submittedloanrequests != null && submittedloanrequests.Any())
                {
                    foreach (var submittedloanrequest in submittedloanrequests)
                    {

                        if(submittedloanrequest.ThirdPartyTransactionId != null)
                        {
                            HttpWebRequest webRequest = WebRequest.Create(spsapiendpoint + submittedloanrequest.ThirdPartyTransactionId) as HttpWebRequest;

                            if (webRequest == null)
                                throw new NullReferenceException("request is not a http request");

                            webRequest.Method = "GET";
                            webRequest.ContentType = "application/json";

                            webRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(spsusername + ":" + spspassword)));
                            HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;

                            if (webResponse.StatusCode == HttpStatusCode.OK)
                            {
                                using (var receiveStream = webResponse.GetResponseStream())
                                {
                                    using (var readStream = new StreamReader(receiveStream, new UTF8Encoding()))
                                    {
                                        var jsonResponse = readStream.ReadToEnd();

                                        var serializer = new JavaScriptSerializer();

                                        var order = serializer.Deserialize<Order>(jsonResponse);

                                        var findloanrequestbyid = datacontext.LoanRequest.Find(submittedloanrequest.Id);
                                        var entry = datacontext.Entry(findloanrequestbyid);
                                        entry.State = EntityState.Modified;
                                        List<OrderLine> orderline = order.OrderLines;
                                        findloanrequestbyid.B2MResponseCode = orderline[0].B2MResponseCode;
                                        findloanrequestbyid.B2MResponseDesc = orderline[0].B2MResponseDesc;
                                        findloanrequestbyid.B2MResultCode = orderline[0].B2MResultCode;
                                        findloanrequestbyid.B2MResultDesc = orderline[0].B2MResultDesc;
                                        findloanrequestbyid.B2MTransactionID = orderline[0].B2MTransactionID;
                                        findloanrequestbyid.ChargePaidAccountAvailableFunds = orderline[0].ChargePaidAccountAvailableFunds;
                                        findloanrequestbyid.IsDelivered = order.IsDelivered;
                                        findloanrequestbyid.TypeDesc = orderline[0].TypeDesc;
                                        findloanrequestbyid.MCCMNCDesc = orderline[0].MCCMNCDesc;
                                        findloanrequestbyid.TransactionCreditParty = orderline[0].TransactionCreditParty;
                                        findloanrequestbyid.TransactionDateTime = orderline[0].TransactionDateTime;
                                        findloanrequestbyid.SPSStatus = orderline[0].Status;
                                        findloanrequestbyid.SPSStatusDesc = orderline[0].StatusDesc;
                                        findloanrequestbyid.UtilityAccountAvailableFunds = orderline[0].UtilityAccountAvailableFunds;
                                        findloanrequestbyid.WorkingAccountAvailableFunds = orderline[0].WorkingAccountAvailableFunds;
                                        if (findloanrequestbyid.B2MResponseCode == responcecode && findloanrequestbyid.B2MResultCode == resultcode)
                                        {
                                            findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Active;
                                            findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Completed;
                                        }

                                        datacontext.SaveChanges();
                                    }
                                }
                            }
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }
    }
}

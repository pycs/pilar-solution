﻿using Nikopeshe.Infrastructure;
using Nikopeshe.Services.InterfaceImplementations;
using Nikopeshe.Services.Interfaces;
using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace NikopesheWindowsService
{
    public partial class MainService : ServiceBase
    {
        ITaskScheduler scheduler;

        public MainService()
        {
            InitializeComponent();
        }
        
        protected override void OnStart(string[] args)
        {
            try
            {
                var kernel = new StandardKernel();
                kernel.Load(Assembly.GetExecutingAssembly());
                kernel.Bind<IC2BTransactionService>().To<C2BTransactionService>();

                scheduler = new TaskScheduler();
                //scheduler.EmailQueueJobDefination();
                //scheduler.SPSPostJobDefination();
                //scheduler.SPSGetJobDefination();
                //scheduler.MpesaRepayment();
                scheduler.C2BConfirmation();
                //scheduler.LoanStatusWatchdogJob();
            }

            catch (ReflectionTypeLoadException ex)
            {
                Logger.CreateLog(ex.Message + ex.LoaderExceptions + ex.StackTrace + ex.Source);
            }

            catch (Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace + ex.Source);
            }
            
        }

        protected override void OnStop()
        {
            if (scheduler != null)
            {
                scheduler.Stop();
            }
        }

        public void StartDebugging(string[] args)
        {
            OnStart(args);
        }

        private static void ConfigureFactories()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) =>
            {
                return true;
            };
        }
    }
}

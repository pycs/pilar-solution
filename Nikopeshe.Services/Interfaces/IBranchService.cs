﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface IBranchService
    {
        void AddNewBranch(Branch branch, ServiceHeader serviceHeader);

        void Update(Branch branch, ServiceHeader serviceHeader);

        Branch FindBranchById(Guid Id);

        IList<Branch> GetAllBranches();

        Branch FindBranchByBranchCode(string branchcode);

        Branch FindBranchByC2BNumber(string C2BNumber);
    }
}

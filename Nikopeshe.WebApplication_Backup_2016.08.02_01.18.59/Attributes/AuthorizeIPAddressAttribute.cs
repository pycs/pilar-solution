﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data.Mappings;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Attributes
{
    public class AuthorizeIPAddressAttribute : ActionFilterAttribute
    {
        //ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public AuthorizeIPAddressAttribute()
        {
            _emailAlertService = DependencyResolver.Current.GetService<IEmailAlertService>();
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string ipAddress = HttpContext.Current.Request.UserHostAddress;

            if (!IsIpAddressAllowed(ipAddress.Trim()))
            {
                context.Result = new HttpStatusCodeResult(403);
                Log(ipAddress);
            }

            base.OnActionExecuting(context);
        }

        private bool IsIpAddressAllowed(string IpAddress)
        {
            if (!string.IsNullOrWhiteSpace(IpAddress))
            {
                string[] addresses = Convert.ToString(ConfigurationManager.AppSettings["AllowedIPAddresses"]).Split(',');
                return addresses.Where(a => a.Trim().Equals(IpAddress, StringComparison.InvariantCultureIgnoreCase)).Any();
            }
            return false;
        }

        private readonly IEmailAlertService _emailAlertService;        

        public void Log(string IPAddress)
        {
            LoggerFactory.CreateLog().LogInfo("WebApiThrottle: Request from {0} has been throttled (blocked)", IPAddress);                
            var emailAlertDTO = new EmailAlert
            {
                MailMessageTo = "felix.ngugi@pycs.co.ke",   
                MailMessageCC = "raphael@pycs.co.ke",
                MailMessageSubject = string.Format("WebApiThrottle ({0})", IPAddress),
                MailMessageBody = string.Format("Request from {0} has been throttled (blocked", IPAddress),
                MailMessageOrigin = (int)Nikopeshe.Entity.DataModels.Enumerations.MessageOrigin.Within,
                MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                MailMessageFrom = DefaultSettings.Instance.Email
            };

            _emailAlertService.AddNewEmailAlert(emailAlertDTO, null);
        }
    }
}
﻿using Crosscutting.NetFramework.Logging;
using MvcSiteMapProvider;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using Nikopeshe.WebApplication.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize(Roles = "Administrator")]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class ProductsController : BaseController
    {
        private readonly ILoanProductService loanproductservice;
        private readonly IChartofAccountService chartofaccountservice;
        private readonly IChargesService chargeservice;
        private readonly ISavingsProductService savingsproductservice;
        private readonly ILoanProductsChargeService loanproductchargeservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public ProductsController(ILoanProductService _loanproductservice, 
            ILoanProductsChargeService _loanproductchargeservice,
            IChartofAccountService _chartofaccountservice,
            IChargesService _chargeservice, ISavingsProductService _savingsproductservice)
        {
            if(_chartofaccountservice == null || _loanproductservice == null 
                || _chargeservice == null || _loanproductchargeservice == null)
                throw new ArgumentNullException("null service reference");

            this.loanproductservice = _loanproductservice;
            this.chargeservice = _chargeservice;
            this.loanproductchargeservice = _loanproductchargeservice;
            this.chartofaccountservice = _chartofaccountservice;
            this.savingsproductservice = _savingsproductservice;
        }

        // GET: Productcs
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Loanproduct/
        public ActionResult LoanProducts()
        {
            return View();
        }

        public JsonResult getallloanproducservice(JQueryDataTablesModel datatablemodel)
        {
            var pagecollection = loanproductservice.GetAllLoanProducts();

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetAllLoanProduct(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, LoanProductList: pagecollection);


            return Json(new JQueryDataTablesResponse<LoanProduct>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<LoanProduct> GetAllLoanProduct(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanProduct> LoanProductList)
        {
            var GetLoanproductList = LoanProductList;

            totalRecordCount = GetLoanproductList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetLoanproductList = LoanProductList.Where(c => c.ProductName
                    .ToLower().Contains(searchString.ToLower()) || c.ProductCode.ToString().ToLower().Contains(searchString)).ToList();
            }

            searchRecordCount = GetLoanproductList.Count;

            IOrderedEnumerable<LoanProduct> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "ProductName":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.ProductName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ProductName);
                        break;

                    case "ProductCode":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.ProductCode)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ProductCode);
                        break;

                    case "InterestRate":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.InterestValue)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestValue);
                        break;

                    case "Term":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.Term)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Term);
                        break;

                    case "MaximumAmount":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.MaximumAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.MaximumAmount);
                        break;

                    case "MinimumAmount":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.MinimumAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.MinimumAmount);
                        break;

                    case "DefaultLoanAmount":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.DefaultLoanAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.DefaultLoanAmount);
                        break;

                    case "IsEnabled":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult AddNewLoanProduct()
        {
            var ChargeSelectList = ChargesSelectList(Guid.Empty.ToString());
            ViewBag.GLAccountSelectList = AssetAccountTypeGLSelectList(Guid.Empty.ToString());
            ViewBag.IncomeGLAccount = IncomeAccountTypeGLSelectList(Guid.Empty.ToString());
            // Get selected charge
            var charges = GetLoanProductCharges(null);
            ViewBag.InterestChargeTypeCheckList = InterestChargeType(null);
            // Setup view data            
            ViewBag.CheckBoxList = charges;
            return View();
        }

        [HttpPost]        
        public ActionResult AddNewLoanProduct(LoanProduct loanproduct)
        {
            // Get selected charge
            var charges = GetLoanProductCharges(null);
            ViewBag.InterestChargeTypeCheckList = InterestChargeType(null);
            // Setup view data
            ViewBag.CheckBoxList = charges;
            ViewBag.ChargeSelectList = ChargesSelectList(Guid.Empty.ToString());
            ViewBag.IncomeGLAccount = IncomeAccountTypeGLSelectList(Guid.Empty.ToString());
            ViewBag.GLAccountSelectList = AssetAccountTypeGLSelectList(Guid.Empty.ToString());

            if (ModelState.IsValid)
            {
                try
                {
                    if (loanproduct.MinimumAmount > loanproduct.MaximumAmount)
                    {
                        ModelState.AddModelError(loanproduct.MinimumAmount.ToString(), "Minimum Amount cannot be greater than Maximum Amount");
                        return View(loanproduct);
                    }

                    else if (loanproduct.DefaultLoanAmount > loanproduct.MaximumAmount)
                    {
                        ModelState.AddModelError(loanproduct.DefaultLoanAmount.ToString(), "Default Amount cannot be greater than Maximum Amount");
                        return View(loanproduct);
                    }

                    else if (loanproduct.DefaultLoanAmount < loanproduct.MinimumAmount)
                    {
                        ModelState.AddModelError(loanproduct.DefaultLoanAmount.ToString(), "Default Amount cannot be less than Minimum Amount");
                        return View(loanproduct);
                    }

                    else if (loanproduct.MaximumAmount < loanproduct.MinimumAmount)
                    {
                        ModelState.AddModelError(loanproduct.DefaultLoanAmount.ToString(), "Maximum Amount cannot be less than Minimum Amount");
                        return View(loanproduct);
                    }

                    if (loanproduct.IsDefault == true)
                    {
                        var finddefaultloancount = loanproductservice.FindDefaultLoanProductCount();
                        if (finddefaultloancount >= 1)
                        {
                            ModelState.AddModelError(loanproduct.DefaultLoanAmount.ToString(), "Default Loan Product cannot be more than one.");
                            return View(loanproduct);
                        }
                    }                    

                    if (loanproduct.LoanGLAccountId == Guid.Empty || loanproduct.InterestGLAccountId == Guid.Empty)
                    {
                        ModelState.AddModelError(string.Empty, "Principal Account GL or Interest Account GL cannot be null.");
                        return View(loanproduct);
                    }
                    if (string.IsNullOrWhiteSpace(string.Concat(loanproduct.PaymentFrequencyType)))
                    {
                        ModelState.AddModelError(string.Empty, "You must select loan product payment frequency.");
                        return View(loanproduct);
                    }
                    string nextproductcode = loanproductservice.NextLoanProductCode();
                    loanproduct.ProductCode = nextproductcode;
                    loanproductservice.AddNewLoanProduct(loanproduct, serviceHeader);
                    var getselectedcharges = Request.Form["Charge"];
                    if (getselectedcharges != null)
                    {
                        string[] selectedvalues = getselectedcharges.Split(',');
                        for (int i = 0; i < selectedvalues.Length; i++)
                        {
                            LoanProductsCharge loanproductcharges = new LoanProductsCharge();
                            var ChargeId = Guid.Parse(selectedvalues[i].Trim());
                            loanproductcharges.ChargeId = ChargeId;
                            loanproductcharges.LoanProductId = loanproduct.Id;
                            loanproductcharges.IsEnabled = true;
                            loanproductchargeservice.AddNewLoanProductsCharge(loanproductcharges, serviceHeader);
                        }
                    }
                    return RedirectToAction("loanproducts");
                }

                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(loanproduct);
                }
            }

            return View(loanproduct);
        }

        [HttpGet]
        public ActionResult editproduct(Guid Id)
        {
            var findproductbyid = loanproductservice.FindLoanProductById(Id);
            var findLoanProductCharges = loanproductchargeservice.GetAllLoanProductCharges(findproductbyid.Id).FirstOrDefault();
            // Get selected charge
            var charges = GetLoanProductCharges(findLoanProductCharges);
            ViewBag.InterestChargeTypeCheckList = InterestChargeType(findproductbyid);
            // Setup view data
            ViewBag.IncomeGLAccount = IncomeAccountTypeGLSelectList(Guid.Empty.ToString());
            ViewBag.CheckBoxList = charges;
            
            

            ViewBag.GLAccountSelectList = AssetAccountTypeGLSelectList(Guid.Empty.ToString());
            ViewBag.IncomeGLAccount = IncomeAccountTypeGLSelectList(Guid.Empty.ToString());

            ViewBag.Productname = findproductbyid.ProductName;

            return View(findproductbyid);
        }

        [HttpPost]        
        public ActionResult editproduct(LoanProduct loanproduct)
        {
            //find loan product
            var findLoanproduct = loanproductservice.FindLoanProductById(loanproduct.Id);
            if (findLoanproduct == null)
            {
                return View(loanproduct);
            }
            var findLoanProductCharges = loanproductchargeservice.GetAllLoanProductCharges(findLoanproduct.Id).FirstOrDefault();
            // Get selected charge
            var charges = GetLoanProductCharges(findLoanProductCharges);
            ViewBag.InterestChargeTypeCheckList = InterestChargeType(loanproduct);
            // Setup view data
            ViewBag.IncomeGLAccount = IncomeAccountTypeGLSelectList(Guid.Empty.ToString());
            ViewBag.CheckBoxList = charges;
            
            loanproduct.CreatedDate = DateTime.Now;

            ViewBag.GLAccountSelectList = AssetAccountTypeGLSelectList(Guid.Empty.ToString());
            ViewBag.IncomeGLAccount = IncomeAccountTypeGLSelectList(Guid.Empty.ToString());

            if (ModelState.IsValid)
            {
                try
                {
                    if (loanproduct.IsDefault == true)
                    {                       
                        var finddefaultloancount = loanproductservice.FindDefaultLoanProductCount();
                        if (finddefaultloancount == 1)
                        {
                            var finddefaultproduct = loanproductservice.FindDefaultLoanProduct();
                            //update current default and set editing product to default
                            finddefaultproduct.IsDefault = false;
                            loanproductservice.UpdateLoanProduct(finddefaultproduct, serviceHeader);
                        }                        
                    } 

                    if (loanproduct.MinimumAmount > loanproduct.MaximumAmount)
                    {
                        ModelState.AddModelError(loanproduct.MinimumAmount.ToString(), "Minimum Amount cannot be greater than Maximum Amount");
                        return View(loanproduct);
                    }

                    else if (loanproduct.DefaultLoanAmount > loanproduct.MaximumAmount)
                    {
                        ModelState.AddModelError(loanproduct.DefaultLoanAmount.ToString(), "Default Amount cannot be greater than Maximum Amount");
                        return View(loanproduct);
                    }

                    else if (loanproduct.DefaultLoanAmount < loanproduct.MinimumAmount)
                    {
                        ModelState.AddModelError(loanproduct.DefaultLoanAmount.ToString(), "Default Amount cannot be less than Minimum Amount");
                        return View(loanproduct);
                    }

                    else if (loanproduct.MaximumAmount < loanproduct.MinimumAmount)
                    {
                        ModelState.AddModelError(loanproduct.DefaultLoanAmount.ToString(), "Maximum Amount cannot be less than Minimum Amount");
                        return View(loanproduct);
                    }

                    if (loanproduct.LoanGLAccountId == Guid.Empty || loanproduct.InterestGLAccountId == Guid.Empty)
                    {
                        ModelState.AddModelError(string.Empty, "Principal Account GL or Interest Account GL cannot be null.");
                        return View(loanproduct);
                    }

                    LoanProduct _loanproduct = loanproductservice.FindLoanProductById(loanproduct.Id);
                    _loanproduct.InterestValue = loanproduct.InterestValue;
                    _loanproduct.IsEnabled = loanproduct.IsEnabled;
                    _loanproduct.MaximumAmount = loanproduct.MaximumAmount;
                    _loanproduct.MinimumAmount = loanproduct.MinimumAmount;
                    _loanproduct.InterestGLAccountId = loanproduct.InterestGLAccountId;
                    _loanproduct.DefaultLoanAmount = loanproduct.DefaultLoanAmount;
                    _loanproduct.LoanGLAccountId = loanproduct.LoanGLAccountId;
                    _loanproduct.PaymentFrequencyType = loanproduct.PaymentFrequencyType;
                    _loanproduct.ProductName = loanproduct.ProductName;
                    _loanproduct.Term = loanproduct.Term;
                    _loanproduct.InterestChargeType = loanproduct.InterestChargeType;
                    _loanproduct.IsDefault = loanproduct.IsDefault;                    
                    loanproductservice.UpdateLoanProduct(_loanproduct, serviceHeader);
                    var getselectedcharges = Request.Form["Charge"];

                    var findallloanproductcharges = loanproductchargeservice.GetAllLoanProductCharges(loanproduct.Id);
                    if (findallloanproductcharges != null && findallloanproductcharges.Any())
                    {
                        foreach (var loanproductcharge in findallloanproductcharges)
                        {
                            var findloanproductchargebyid = loanproductchargeservice.FindLoanProductsChargeById(loanproductcharge.Id);
                            loanproductchargeservice.DeleteLoanProductCharge(findloanproductchargebyid, serviceHeader);
                        }
                    }
                    if (getselectedcharges != null)
                    {
                        string[] selectedvalues = getselectedcharges.Split(',');
                        for (int i = 0; i < selectedvalues.Length; i++)
                        {
                            LoanProductsCharge loanproductcharges = new LoanProductsCharge();
                            var ChargeId = Guid.Parse(selectedvalues[i].Trim());
                            loanproductcharges.ChargeId = ChargeId;
                            loanproductcharges.LoanProductId = loanproduct.Id;
                            loanproductcharges.IsEnabled = true;
                            loanproductchargeservice.AddNewLoanProductsCharge(loanproductcharges, serviceHeader);
                        }
                    }

                    return RedirectToAction("loanproducts");
                }

                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(loanproduct);
                }
            }
            return View(loanproduct);
        }

        [MvcSiteMapNode(Title = "productdetails", ParentKey = "Id2")]
        public ActionResult productdetails(Guid id)
        {
            var findloanproduct = loanproductservice.FindLoanProductById(id);

            ViewBag.Productname = findloanproduct.ProductName;

            return View(findloanproduct);
        }

        [HttpGet]
        public ActionResult deleteproduct(Guid Id)
        {
            var findproductbyid = loanproductservice.FindLoanProductById(Id);
            ViewBag.Productname = findproductbyid.ProductName;
            return View(findproductbyid);
        }

        [ActionName("deleteproduct")]
        [HttpPost]        
        public ActionResult deleteloanproduct(Guid Id)
        {
            LoanProduct loanproduct = loanproductservice.FindLoanProductById(Id);

            loanproduct.IsEnabled = false;

            loanproductservice.UpdateLoanProduct(loanproduct, serviceHeader);

            return RedirectToAction("Index");
        }

        // GET: LoanProductsCharge
        public ActionResult LoanProductCharges(Guid id)
        {
            ViewBag.Id = id;
            var findloanproductbyid = loanproductservice.FindLoanProductById(id);
            ViewBag.LoanProductName = findloanproductbyid.ProductName;
            return View();
        }

        public JsonResult GetAllLoanProductCharges(Guid id, JQueryDataTablesModel datatablemodel)
        {
            var pagecollection = loanproductchargeservice.GetAllLoanProductCharges(id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetAllLoanProductChargeList(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, LoanProductChargeList: pagecollection);


            return Json(new JQueryDataTablesResponse<LoanProductsCharge>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public IList<LoanProductsCharge> GetAllLoanProductChargeList(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanProductsCharge> LoanProductChargeList)
        {
            var GetLoanproductList = LoanProductChargeList;

            totalRecordCount = GetLoanproductList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetLoanproductList = LoanProductChargeList.Where(c => c.LoanProduct.ProductName
                    .ToLower().Contains(searchString.ToLower())
                    || c.Charges.ChargeName.ToLower().Contains(searchString.ToLower())
                    ).ToList();
            }

            searchRecordCount = GetLoanproductList.Count;

            IOrderedEnumerable<LoanProductsCharge> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "LoanProduct.ProductName":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName);
                        break;

                    case "Charges.ChargeName":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.Charges.ChargeName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Charges.ChargeName);
                        break;

                    case "IsEnabled":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [NonAction]
        public List<SelectListItem> AssetAccountTypeGLSelectList(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultvalue = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select GL Account", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultvalue);

            var otherlist = chartofaccountservice.GetChartOfAccountsByTypeAndCategory((int)Enumerations.ChartOfAccountType.Asset, (int)Enumerations.ChartOfAccountCategory.DetailAccount);

            foreach (var item in otherlist)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.ChartOfAccountDescriptionByCategory, Value = item.Id.ToString("D") });

            return SelectList;
        }

        [NonAction]
        public List<SelectListItem> IncomeAccountTypeGLSelectList(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultvalue = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select GL Account", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultvalue);

            var otherlist = chartofaccountservice.GetChartOfAccountsByTypeAndCategory((int)Enumerations.ChartOfAccountType.Income, (int)Enumerations.ChartOfAccountCategory.DetailAccount);

            foreach (var item in otherlist)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.ChartOfAccountDescriptionByCategory, Value = item.Id.ToString("D") });

            return SelectList;
        }

        [NonAction]
        public List<SelectListItem> ChargesSelectList(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultvalue = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select GL Account", Value = Guid.Empty.ToString("D"), Disabled = true };

            SelectList.Add(defaultvalue);

            var otherlist = chargeservice.GetAllCharges().Where(x => x.IsEnabled == true);

            foreach (var item in otherlist)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.ChargeName, Value = item.Id.ToString("D") });

            return SelectList;
        }

        List<CheckBoxListInfo> GetLoanProductCharges(LoanProductsCharge loanProductCharge)
        {
            List<CheckBoxListInfo> loanProductList = new List<CheckBoxListInfo>();

            var getallcharges = chargeservice.GetAllCharges().Where(x => x.IsEnabled == true);
            var findLoanProductCharge = loanproductchargeservice.GetAllLoanProductCharges(loanProductCharge.LoanProductId);
            foreach (var item in getallcharges)
            {
                bool IsChecked = false;
                var findLoanProduct = loanproductchargeservice.FindLoanProductChargeByChargeId(item.Id);
                if (findLoanProduct != null)
                {
                    IsChecked = true;
                }
                loanProductList.Add(new CheckBoxListInfo(item.Id.ToString(), item.ChargeName, IsChecked));               
            }
            return loanProductList;
        }

        // GET: SavingsProduct
        public ActionResult SavingsProduct()
        {
            return View();
        }

        public JsonResult GetAllSavingsProduct(JQueryDataTablesModel datatablemodel)
        {
            var totalrecordcount = 0;

            var searchedrecordcount = 0;

            var pagecollection = savingsproductservice.GetAllSavingsAccountProducts();

            var loanrequestList = GetAllSavingsProductList(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalrecordcount, searchRecordCount: out searchedrecordcount, searchString: datatablemodel.sSearch, SavingsProductList: pagecollection);


            return Json(new JQueryDataTablesResponse<SavingsProduct>(items: loanrequestList,
                totalRecords: totalrecordcount,
                totalDisplayRecords: searchedrecordcount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<SavingsProduct> GetAllSavingsProductList(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<SavingsProduct> SavingsProductList)
        {
            var GetSavingsProductList = SavingsProductList;

            totalRecordCount = GetSavingsProductList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetSavingsProductList = SavingsProductList.Where(c => c.ProductName
                    .ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetSavingsProductList.Count;

            IOrderedEnumerable<SavingsProduct> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetSavingsProductList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "ChartofAccount.AccountName":
                        sortedClients = sortedClients == null ? GetSavingsProductList.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.AccountName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.AccountName);
                        break;

                    case "ProductName":
                        sortedClients = sortedClients == null ? GetSavingsProductList.CustomSort(sortedColumn.Direction, cust => cust.ProductName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ProductName);
                        break;

                    case "ProductCode":
                        sortedClients = sortedClients == null ? GetSavingsProductList.CustomSort(sortedColumn.Direction, cust => cust.ProductCode)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ProductCode);
                        break;

                    case "IsDefault":
                        sortedClients = sortedClients == null ? GetSavingsProductList.CustomSort(sortedColumn.Direction, cust => cust.IsDefault)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsDefault);
                        break;

                    case "IsEnabled":
                        sortedClients = sortedClients == null ? GetSavingsProductList.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult CreateNewSavingsProduct()
        {
            ViewBag.LiabilityChartofAccounts = GetLiabilityAccountTypes(Guid.Empty.ToString());

            return View();
        }

        [HttpPost]        
        public ActionResult CreateNewSavingsProduct(SavingsProduct savingproduct)
        {
            ViewBag.LiabilityChartofAccounts = GetLiabilityAccountTypes(Guid.Empty.ToString());

            try
            {
                if (ModelState.IsValid)
                {
                    string nextproductcode = savingsproductservice.NextSavingProductCode();
                    savingproduct.ProductCode = nextproductcode;
                    if (savingproduct.ChartofAccountId == Guid.Empty)
                    {
                        ModelState.AddModelError(string.Empty, "Chart of Chart Account cannot be null.");
                        return View(savingproduct);
                    }
                    if (savingproduct.IsDefault == true)
                    {
                        var defaultsavingsproductcount = savingsproductservice.DefaultSavingsProductCount();
                        if (defaultsavingsproductcount.Count() <= 0)
                        {
                            savingsproductservice.AddNewSavingsProduct(savingproduct, serviceHeader);
                            return RedirectToAction("Index");
                        }

                        else
                        {
                            ModelState.AddModelError(string.Empty, "Sorry, there is already a default savings product.");
                            return View(savingproduct);
                        }
                    }
                    else
                    {
                        savingsproductservice.AddNewSavingsProduct(savingproduct, serviceHeader);
                        return RedirectToAction("savingsproduct");
                    }
                }
                return View(savingproduct);
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View(savingproduct);
            }
        }

        public ActionResult SavingProductDetails(Guid Id)
        {
            var findsavingproductbyid = savingsproductservice.FindSavingsProductById(Id);

            ViewBag.SavingsProductname = findsavingproductbyid.ProductName;

            return View(findsavingproductbyid);
        }

        [HttpGet]
        public ActionResult EditSavingProduct(Guid Id)
        {
            var findsavingsproductbyid = savingsproductservice.FindSavingsProductById(Id);

            ViewBag.LiabilityChartofAccounts = GetLiabilityAccountTypes(Guid.Empty.ToString());

            return View(findsavingsproductbyid);
        }
        
        [ValidateAntiForgeryToken]
        public ActionResult EditSavingProduct(SavingsProduct savingproduct)
        {
            ViewBag.LiabilityChartofAccounts = GetLiabilityAccountTypes(Guid.Empty.ToString());

            if (ModelState.IsValid)
            {
                try
                {
                    if (savingproduct.ChartofAccountId == Guid.Empty)
                    {
                        ModelState.AddModelError(string.Empty, "Chart of Chart Account cannot be null.");
                        return View(savingproduct);
                    }
                    var defaultsavingsproductcount = savingsproductservice.DefaultSavingsProductCount();
                    var findsavingsproductbyid = savingsproductservice.FindSavingsProductById(savingproduct.Id);
                    if (savingproduct.IsDefault == true)
                    {
                        if (defaultsavingsproductcount.Count() == 1)
                        {
                            var finddefaultsavingproduct = savingsproductservice.FindDefaultSavingsProduct();
                            finddefaultsavingproduct.IsDefault = false;
                            savingsproductservice.UpdateSavingProduct(finddefaultsavingproduct, serviceHeader);                            
                        }

                        findsavingsproductbyid.ChartofAccountId = savingproduct.ChartofAccountId;
                        findsavingsproductbyid.ProductName = savingproduct.ProductName;
                        findsavingsproductbyid.IsEnabled = savingproduct.IsEnabled;
                        findsavingsproductbyid.IsDefault = savingproduct.IsDefault;
                        savingsproductservice.UpdateSavingProduct(findsavingsproductbyid, serviceHeader);
                    }
                    return RedirectToAction("savingsproduct");
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(savingproduct);
                }
            }
            return View(savingproduct);
        }


        [NonAction]
        public List<SelectListItem> GetLiabilityAccountTypes(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultaccount = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Chart of Account", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultaccount);

            var otherliabilityaccounts = chartofaccountservice.GetChartOfAccountsByTypeAndCategory((int)Enumerations.ChartOfAccountType.Liability, (int)Enumerations.ChartOfAccountCategory.DetailAccount);

            foreach (var item in otherliabilityaccounts)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.ChartofAccountDescription, Value = item.Id.ToString("D") });

            return SelectList;
        }
    }
}
﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface IPartnerService
    {
        void AddNewPartner(Partner partner, ServiceHeader serviceHeader);

        void UpdatePartner(Partner partner, ServiceHeader serviceHeader);

        PageCollectionInfo<Partner> FindPartnersFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        PageCollectionInfo<Partner> FindCustomersByPartnerIdFilterInPage(Guid id, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        IList<Partner> GetAllPartners(IsolationLevel isolationLevel);
    }
}

﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    public class CustomerDocument : BaseEntity
    {
        [Index]
        [Display(Name="Customer Name")]
        public Guid CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        [Index]
        [Display(Name="Document Type")]
        public Enumerations.CustomerDocumentTypes DocumentType { get; set; }

        public string DocumentTypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.CustomerDocumentTypes), DocumentType) ? EnumHelper.GetDescription((Enumerations.CustomerDocumentTypes)DocumentType) : string.Empty;
            }
        }
        
        public byte[] Image { get; set; }

    }
}

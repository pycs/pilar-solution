﻿using Crosscutting.NetFramework.Logging;
using Infrastructure.Crosscutting.NetFramework;
using Nikopeshe.Data;
using Nikopeshe.Data.Mappings;
using Nikopeshe.Entity;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.DTO;
using Nikopeshe.Services.InterfaceImplementations;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize(Roles = "FinanceManager")]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class FinanceManagerController : BaseController
    {
        private readonly ILoanRequestService loanrequestService;
        private readonly ICustomerService customerservice;
        private readonly IEmailAlertService emailalerservice;
        private readonly ITextAlertService textalertservice;
        private readonly ISystemGeneralLedgerAccountMappingService systemglmappingservice;
        private readonly ISavingsProductService savingsproductservice;
        private readonly ICustomerAccountService customeraccountservice;
        private readonly ILoanProductService loanproductservice;
        private readonly ILoanTransactionHistoryService loantransactionhistoryservice;
        private readonly IChartofAccountService chartofaccountservice;
        private readonly ILoanProductsChargeService loanproductcharges;
        private readonly IUserInfoService userInfo;
        private readonly IGraduatedScaleService graduatedscaleservice;
        private readonly ILoanRequestChargeService loanrequestchargeservice;
        private readonly IStaticSettingService staticsettings;        
        private readonly IC2BTransactionService c2bTransactionService;
        private readonly ILoanRequestDocumentsService loanrequestdocumentservice;
        private readonly ICustomerDocumentService customerdocumentservice;
        private readonly IBranchService branchService;
        private readonly IReconciliationService reconService;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        private string TextSignature = ConfigurationManager.AppSettings["TextSignature"].ToString();
        public FinanceManagerController(ILoanRequestService _loanrequestservice, ICustomerService _customerservice,
            ILoanProductService _loanproductservice, ISystemGeneralLedgerAccountMappingService _systemglmappingservice,
            ISavingsProductService _savingsproductservice, IChartofAccountService _chartofaccountservice,
            ICustomerAccountService _customeraccountservice, IEmailAlertService _emailalerservice,
            ITextAlertService _textalertservice, ILoanTransactionHistoryService _loantransactionhistoryservice,
            ILoanProductsChargeService _loanproductcharges, IGraduatedScaleService _graduatedscaleservice,
            ILoanRequestChargeService _loanrequestchargeservice, IStaticSettingService _staticsettings,
            ILoanRequestDocumentsService _loanrequestdocumentservice, ICustomerDocumentService _customerdocumentservice,
            IUserInfoService _userInfo, IC2BTransactionService _c2bTransactionService, IBranchService _branchService,
            IReconciliationService _reconService)
        {
            if(_loanproductservice == null || _emailalerservice == null || _systemglmappingservice == null
                || _savingsproductservice == null || _chartofaccountservice == null || _textalertservice == null
                || _customeraccountservice == null || _loanrequestservice == null || _customerservice == null 
                || _loantransactionhistoryservice == null || _loanproductcharges == null || _graduatedscaleservice == null
                || _loanrequestchargeservice == null || _customerdocumentservice == null)
                throw new ArgumentNullException("null service reference");

            this.loanproductservice = _loanproductservice;
            this.emailalerservice = _emailalerservice;
            this.systemglmappingservice = _systemglmappingservice;
            this.savingsproductservice = _savingsproductservice;
            this.chartofaccountservice = _chartofaccountservice;
            this.textalertservice = _textalertservice;
            this.customeraccountservice = _customeraccountservice;
            this.loanrequestService = _loanrequestservice;
            this.customerservice = _customerservice;
            this.loantransactionhistoryservice = _loantransactionhistoryservice;
            this.loanproductcharges = _loanproductcharges;
            this.graduatedscaleservice = _graduatedscaleservice;
            this.branchService = _branchService;
            this.loanrequestchargeservice = _loanrequestchargeservice;
            this.staticsettings = _staticsettings;
            this.loanrequestdocumentservice = _loanrequestdocumentservice;
            this.customerdocumentservice = _customerdocumentservice;
            this.userInfo = _userInfo;
            this.c2bTransactionService = _c2bTransactionService;
            this.reconService = _reconService;
        }
       
        // GET: LoanManager
        public ActionResult Index(int? page)
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetPendingLoanRequestFilterInPage(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc" ? true : false;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = new PageCollectionInfo<LoanRequest>();

            pageCollectionInfo = loanrequestService.FindPendingDisbursementListFilterInPage((int)Enumerations.LoanStatus.Approved, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, sortAscending);

            int totalRecordCount = 0;

            int searchRecordCount = 0;


            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;
            }

            return this.DataTablesJson(items: pageCollectionInfo.PageCollection, totalRecords: totalRecordCount, totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
        }

        [HttpPost]
        public JsonResult LoanTransactionsList(JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc" ? true : false;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = loanrequestService.FindLoanRequestFilterInPage(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, sortAscending);

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;
            }

            return this.DataTablesJson(items: pageCollectionInfo.PageCollection, totalRecords: totalRecordCount, totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
        }
       
        public JsonResult GetAllLoanRequest(JQueryDataTablesModel datatablemodel)
        {
            var allloanrequestpagecollection = loanrequestService.GetAllLoanRequestByStatus((int)Enumerations.LoanStatus.Approved, IsolationLevel.ReadUncommitted).ToList();

            //if (id == 1000)
            //{
            //    allloanrequestpagecollection = loanrequestService.GetAllLoanRequests().ToList();
            //}
            //else
            //{
            //    allloanrequestpagecollection = loanrequestService.GetAllLoanRequests().Where(x => x.Status == id).ToList();
            //}
                
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetAllLoanRequest(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, LoanRequestList: allloanrequestpagecollection);


            var results = Json(new JQueryDataTablesResponse<LoanRequest>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));

            return results;
        }

        public static IList<LoanRequest> GetAllLoanRequest(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanRequest> LoanRequestList)
        {
            var GetLoanRequestList = LoanRequestList;

            totalRecordCount = GetLoanRequestList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetLoanRequestList = GetLoanRequestList.Where(c => c.CustomerAccount.Customer.IDNumber
                    .ToLower().Contains(searchString.ToLower())
                    || c.LoanReferenceNumber.ToLower().Contains(searchString.ToLower())
                    || c.CustomerAccount.Customer.MobileNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetLoanRequestList.Count;

            IOrderedEnumerable<LoanRequest> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "CustomerAccount.Customer.FullName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName);
                        break;

                    case "CustomerAccount.Customer.IDNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.IDNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.IDNumber);
                        break;

                    case "CustomerAccount.Customer.MobileNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.MobileNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.MobileNumber);
                        break;

                    case "LoanProduct.ProductName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName);
                        break;

                    case "LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber);
                        break;

                    case "LoanAmount":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount);
                        break;

                    case "InterestAmount":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount);
                        break;

                    case "StatusDesc":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc);
                        break;

                    case "Branch.BranchName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <returns></returns>
        /// 
        [Authorize]
        public ActionResult CustomerList()
        {
            return View();
        }

        public JsonResult getallcustomers(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var findCurrentUserId = User.Identity.Name;

            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc" ? true : false;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = customerservice.FindCustomersFilterInPage(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, sortAscending);
            
            int totalRecordCount = 0;

            int searchRecordCount = 0;


            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;
            }

            return this.DataTablesJson(items: pageCollectionInfo.PageCollection, totalRecords: totalRecordCount, totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);

        }

        public static IList<Customer> GetAllCustomer(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<Customer> CustomerList)
        {
            var GetCustomersList = CustomerList;

            totalRecordCount = GetCustomersList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomersList = GetCustomersList.Where(c => c.FullName
                    .ToLower().Contains(searchString.ToLower())
                    || c.IDNumber.ToLower().Contains(searchString.ToLower()) || c.MobileNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetCustomersList.Count;

            IOrderedEnumerable<Customer> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "FullName":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.FullName);
                        break;

                    case "MobileNumber":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.MobileNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.MobileNumber);
                        break;

                    case "IDNumber":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.IDNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IDNumber);
                        break;

                    case "Branch.BranchName":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName);
                        break;

                    case "IncomeRange":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.IncomeRange)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IncomeRange);
                        break;

                    case "IsEnabled":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        [Authorize(Roles = "FinanceManager, Administrator")]
        public ActionResult DisburseLoan(Guid Id)
        {
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(Id);
            
            return View(findloanrequestbyid);
        }

        [HttpPost]
        [ActionName("DisburseLoan")]
        public ActionResult DisburseLoan(LoanRequest loanrequest)
        {
            LoanTransactionHistory loantransactionhistory = new LoanTransactionHistory();
                                
            decimal totalcharges = 0;            
            string paybillNumber = string.Empty;
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(loanrequest.Id);            
            if (findloanrequestbyid == null)
            {
                ModelState.AddModelError(string.Empty, "An expected error has occurred.");
                return View(loanrequest);
            }
            
            if (findloanrequestbyid.Status == (int)Enumerations.LoanStatus.Approved)
            {
                var FindCustomerAccountByCustomerIdandAccountType = customeraccountservice.FindCustomerByCustomerIdandAccountType(findloanrequestbyid.CustomerAccount.CustomerId, (int)Enumerations.ProductCode.Savings);
                var defaultsavingproductglaccount = savingsproductservice.FindDefaultSavingsProduct();
                decimal findCustomerSavingsAccountBalance = customeraccountservice.FindCustomerAccountBalanceByProductCode(FindCustomerAccountByCustomerIdandAccountType.Id, defaultsavingproductglaccount.ChartofAccountId, (int)Enumerations.ProductCode.Savings);
                if ((findCustomerSavingsAccountBalance - findloanrequestbyid.LoanAmount) >= int.Parse(ConfigurationManager.AppSettings["LoanProcessingFee"]))
                {
                    using (var datacontext = new DataContext())
                    {
                        using (var transaction = datacontext.Database.BeginTransaction())
                        {
                            try
                            {
                                var MpesaGL = systemglmappingservice.FindSystemGeneralLedgerAccountMappingsByGLCode((int)Enumerations.SystemGeneralLedgerAccountCode.MPesaSettlementAccount);

                                if (MpesaGL == null)
                                {
                                    ModelState.AddModelError(string.Empty, "Failed. There is no M-Pesa Settlement GL Account available.");
                                    return View(findloanrequestbyid);
                                }
                                try
                                {
                                    var findloanproductcharges = loanproductcharges.GetAllLoanProductCharges(findloanrequestbyid.LoanProductId, System.Data.IsolationLevel.ReadCommitted).Where(x => x.Charges.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.UpFront);
                                    if (findloanproductcharges != null && findloanproductcharges.Any())
                                    {
                                        foreach (var charge in findloanproductcharges)
                                        {
                                            var findgraduatedscalebychargeid = graduatedscaleservice.FindGraduatedScaleUsingRangeAndChargeId(findloanrequestbyid.LoanAmount, charge.ChargeId);
                                            if (findgraduatedscalebychargeid != null && findgraduatedscalebychargeid.Any())
                                            {
                                                foreach (var graduatedscale in findgraduatedscalebychargeid)
                                                {
                                                    var findloanamountrangeingradutedscale = graduatedscaleservice.FindGraduatedScaleUsingRangeAndChargeId(findloanrequestbyid.LoanAmount, graduatedscale.ChargeId);
                                                    var customersavingsaccountid = FindCustomerAccountByCustomerIdandAccountType.Id.ToString();
                                                    if (graduatedscale.ChargeType == Enumerations.ChargeTypes.FixedAmount)
                                                    {
                                                        if (findloanamountrangeingradutedscale != null)
                                                        {
                                                            var fixedchargetypeamount = graduatedscale.Value;

                                                            LoanRequestCharges(findloanrequestbyid.Id, charge.Charges.ChartofAccountId, graduatedscale.Id, fixedchargetypeamount);
                                                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                            #region Debit
                                                            LoanTransactionHistory postchargesdebit = new LoanTransactionHistory()
                                                            {
                                                                LoanRequestId = findloanrequestbyid.Id,
                                                                ChartofAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                                                ContraAccountId = charge.Charges.ChartofAccountId,
                                                                TransactionType = Enumerations.TransactionType.Debit,
                                                                CustomersAccountId = Guid.Parse(customersavingsaccountid),
                                                                Description = findloanrequestbyid.Remarks,
                                                                IsApproved = true,
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.LoanProcessingFee,
                                                                CreatedBy = User.Identity.Name,
                                                                TransactionAmount = fixedchargetypeamount,
                                                                BranchId = findloanrequestbyid.BranchId,
                                                                TransactionBatchNumber = batchNo
                                                            };

                                                            loantransactionhistoryservice.AddNewLoanHistory(postchargesdebit, serviceHeader);
                                                            #endregion

                                                            #region Credit
                                                            LoanTransactionHistory postchargescredit = new LoanTransactionHistory()
                                                            {
                                                                LoanRequestId = findloanrequestbyid.Id,
                                                                ChartofAccountId = charge.Charges.ChartofAccountId,
                                                                ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                                                TransactionType = Enumerations.TransactionType.Credit,
                                                                CustomersAccountId = Guid.Parse(customersavingsaccountid),
                                                                Description = findloanrequestbyid.Remarks,
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.LoanProcessingFee,
                                                                CreatedBy = User.Identity.Name,
                                                                IsApproved = true,
                                                                TransactionAmount = fixedchargetypeamount,
                                                                BranchId = findloanrequestbyid.BranchId, TransactionBatchNumber = batchNo
                                                            };

                                                            loantransactionhistoryservice.AddNewLoanHistory(postchargescredit, serviceHeader);
                                                            #endregion
                                                        }
                                                    }

                                                    else if (graduatedscale.ChargeType == Enumerations.ChargeTypes.Percentage)
                                                    {
                                                        if (findloanamountrangeingradutedscale != null)
                                                        {
                                                            var percetagechargetypeamount = ((graduatedscale.Value) * findloanrequestbyid.LoanAmount) / 100;
                                                            LoanRequestCharges(findloanrequestbyid.Id, charge.Charges.ChartofAccountId, graduatedscale.Id, percetagechargetypeamount);
                                                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                            #region Debit
                                                            LoanTransactionHistory postchargesdebit = new LoanTransactionHistory()
                                                            {
                                                                LoanRequestId = findloanrequestbyid.Id,
                                                                ChartofAccountId = MpesaGL.ChartofAccountId,
                                                                ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                                                TransactionType = Enumerations.TransactionType.Debit,
                                                                CustomersAccountId = Guid.Parse(customersavingsaccountid),
                                                                Description = findloanrequestbyid.Remarks,
                                                                IsApproved = true,
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.LoanProcessingFee,
                                                                CreatedBy = User.Identity.Name,
                                                                TransactionAmount = percetagechargetypeamount,
                                                                BranchId = findloanrequestbyid.BranchId,
                                                                TransactionBatchNumber = batchNo
                                                            };

                                                            loantransactionhistoryservice.AddNewLoanHistory(postchargesdebit, serviceHeader);
                                                            #endregion

                                                            #region Credit
                                                            LoanTransactionHistory postchargescredit = new LoanTransactionHistory()
                                                            {
                                                                LoanRequestId = findloanrequestbyid.Id,
                                                                ChartofAccountId = MpesaGL.ChartofAccountId,
                                                                ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                                                TransactionType = Enumerations.TransactionType.Credit,
                                                                CustomersAccountId = Guid.Parse(customersavingsaccountid),
                                                                Description = findloanrequestbyid.Remarks,
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.LoanProcessingFee,
                                                                CreatedBy = User.Identity.Name,
                                                                IsApproved = true,
                                                                TransactionAmount = percetagechargetypeamount,
                                                                BranchId = findloanrequestbyid.BranchId,
                                                                TransactionBatchNumber = batchNo
                                                            };
                                                            loantransactionhistoryservice.AddNewLoanHistory(postchargescredit, serviceHeader);
                                                            #endregion
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ModelState.AddModelError(string.Empty, "Failed while trying to determine product charge type. Contact your system administrator for assistance.");
                                                        return View(findloanrequestbyid);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #region Debit Loan Transaction History Table
                                    var customeraccountid = FindCustomerAccountByCustomerIdandAccountType.Id.ToString();
                                    int batchNo1 = RandomBatchNumberGenerator.GetRandomNumber();
                                    LoanTransactionHistory debitsavingsaccount = new LoanTransactionHistory()
                                    {
                                        LoanRequestId = findloanrequestbyid.Id,
                                        ChartofAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                        ContraAccountId = MpesaGL.ChartofAccountId,
                                        TransactionType = Enumerations.TransactionType.Debit,
                                        CustomersAccountId = Guid.Parse(customeraccountid),
                                        Description = findloanrequestbyid.Remarks,
                                        IsApproved = true,
                                        TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement,
                                        CreatedBy = User.Identity.Name,
                                        TransactionAmount = findloanrequestbyid.LoanAmount,
                                        BranchId = findloanrequestbyid.BranchId,
                                        TransactionBatchNumber = batchNo1
                                    };

                                    #endregion

                                    #region Credit Loan Transaction History Table
                                    LoanTransactionHistory creditmpesaglaccount = new LoanTransactionHistory()
                                    {
                                        LoanRequestId = findloanrequestbyid.Id,
                                        ChartofAccountId = MpesaGL.ChartofAccountId,
                                        ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                        TransactionType = Enumerations.TransactionType.Credit,
                                        CustomersAccountId = Guid.Parse(customeraccountid),
                                        Description = findloanrequestbyid.Remarks,
                                        TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement,
                                        CreatedBy = User.Identity.Name,
                                        IsApproved = true,
                                        TransactionAmount = findloanrequestbyid.LoanAmount,
                                        BranchId = findloanrequestbyid.BranchId,
                                        TransactionBatchNumber = batchNo1
                                    };

                                    #endregion

                                    var findloanrequestcharge = loanrequestchargeservice.FindAllLoanRequestChargesByLoanRequestId(findloanrequestbyid.Id);
                                    if (findloanrequestcharge != null && findloanrequestcharge.Any())
                                    {
                                        totalcharges = findloanrequestcharge.Sum(x => x.TransactionAmount);
                                    }
                                    if (findloanrequestbyid.Type == Enumerations.EFTType.Cheque)
                                    {
                                        findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Active;
                                        findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Completed;
                                    }
                                    else
                                    {
                                        findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Disbursed;
                                        findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Disburse;
                                    }

                                    var findLoanProductById = loanproductservice.FindLoanProductById(findloanrequestbyid.LoanProductId);
                                    if (findLoanProductById.PaymentFrequencyType == Enumerations.PaymentFrequency.Monthly)
                                    {
                                        findloanrequestbyid.NextPaymentDate = DateTime.Now.AddMonths(1);
                                        findloanrequestbyid.ScheduledRepaymentDate = DateTime.Now.AddMonths(1);
                                    }

                                    else
                                    {
                                        findloanrequestbyid.NextPaymentDate = DateTime.Now.AddDays(7);
                                        findloanrequestbyid.ScheduledRepaymentDate = DateTime.Now.AddDays(7);
                                    }

                                    findloanrequestbyid.DisbursedAmount = findloanrequestbyid.LoanAmount;
                                    findloanrequestbyid.DisbursementDate = DateTime.Now;
                                    int addDays = findloanrequestbyid.NegotiatedInstallments * (int)findloanrequestbyid.LoanProduct.PaymentFrequencyType;
                                    findloanrequestbyid.LoanEndDate = DateTime.Now.AddDays(addDays);
                                    findloanrequestbyid.DisbursedBy = Request.RequestContext.HttpContext.User.Identity.Name;

                                    loanrequestService.UpdateLoanRequest(findloanrequestbyid, serviceHeader);
                                    loantransactionhistoryservice.AddNewLoanHistory(creditmpesaglaccount, serviceHeader);
                                    loantransactionhistoryservice.AddNewLoanHistory(debitsavingsaccount, serviceHeader);
                                }
                                catch (Exception ex)
                                {
                                    ModelState.AddModelError(string.Empty, "An error has occurred while processing your request.");
                                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                    return View(findloanrequestbyid);
                                }
                                transaction.Commit();
                            }

                            catch (Exception ex)
                            {
                                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                transaction.Rollback();
                                ModelState.AddModelError(string.Empty, "An error has occurred while processing your request.");
                                return View(findloanrequestbyid);
                            }
                        }
                    }
                }
                else
                {
                    decimal savingAccountBalance = 0m;
                    savingAccountBalance = findCustomerSavingsAccountBalance - findloanrequestbyid.LoanAmount;
                    string errorMessage = string.Format("Sorry but savings account has insufficient funds to facilitate loan processing fee. The available balance is KES {0}", savingAccountBalance);
                    ModelState.AddModelError(string.Empty, errorMessage);
                    return View(findloanrequestbyid);
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "This loan has already been disbursed.");
                return View(findloanrequestbyid);
            }
            return RedirectToAction("index");
        }

        [HttpGet]
        [Authorize(Roles = "FinanceManager, Administrator")]
        public ActionResult RejectLoan(Guid Id)
        {
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(Id);
           
            return View(findloanrequestbyid);
        }

        [ActionName("rejectloan")]
        [HttpPost]        
        public ActionResult RejectCustomerLoanRequest(LoanRequest loanrequest)
        {
            try
            {                
                var findloanrequestbyid = loanrequestService.GetLoanRequestById(loanrequest.Id);

                if (string.IsNullOrWhiteSpace(loanrequest.RejectionRemarks))
                {
                    ModelState.AddModelError(string.Empty, "You must provide rejection remarks.");
                    return View(findloanrequestbyid);
                }

                if(findloanrequestbyid.Status == (int)Enumerations.LoanStatus.Approved)
                {
                    findloanrequestbyid.Remarks = loanrequest.Remarks;
                    findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Review;
                    findloanrequestbyid.ApprovedBy = User.Identity.Name;
                    findloanrequestbyid.ApprovedDate = DateTime.Now;
                    loanrequestService.UpdateLoanRequest(findloanrequestbyid, serviceHeader);
                    if (!string.IsNullOrWhiteSpace(findloanrequestbyid.ApprovedBy))
                    {
                        var findUserInfo = userInfo.FindUserByUserName(findloanrequestbyid.ApprovedBy);
                        StringBuilder bodyBuilder = new StringBuilder();
                        bodyBuilder.Append(string.Format("Dear {0} ", findUserInfo.FirstName));
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.Append(string.Format("The loan with reference number-{0} has been sent for review. Reason: {1}", findloanrequestbyid.LoanReferenceNumber, loanrequest.RejectionRemarks));
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.AppendLine("<br />");

                        var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
                        bodyBuilder.Append(emailSignature);

                        var emailAlertDTO = new EmailAlert
                        {
                            MailMessageFrom = DefaultSettings.Instance.Email,
                            MailMessageTo = string.Format("{0}", findUserInfo.Email),
                            MailMessageSubject = string.Format("Loan Request Rejected - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                            MailMessageBody = string.Format("{0}", bodyBuilder),
                            MailMessageIsBodyHtml = true,
                            MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                            MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                            MailMessageSecurityCritical = false,
                            CreatedBy = "System",
                            MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                        };
                        emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                    }                    
                }
                
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, "An error has occurred while processing your request.");
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);        
            }
            return View(loanrequest);
        }

        #region Notifications
        private static EmailAlert EmailNotification(LoanRequest findloanrequestbyid)
        {
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append(string.Format("Dear {0}", findloanrequestbyid.CustomerAccount.Customer.FullName));
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.Append(string.Format("Your loan of KES {0} with reference number-{1} has been approved.", findloanrequestbyid.LoanAmount, findloanrequestbyid.LoanReferenceNumber));
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.AppendLine("<br />");

            var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
            bodyBuilder.Append(emailSignature);

            var emailAlertDTO = new EmailAlert
            {
                MailMessageFrom = DefaultSettings.Instance.Email,
                MailMessageTo = string.Format("{0}", findloanrequestbyid.CustomerAccount.Customer.EmailAddress),
                MailMessageSubject = string.Format("Loan Request Approved - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                MailMessageBody = string.Format("{0}", bodyBuilder),
                MailMessageIsBodyHtml = true,
                MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                MailMessageSecurityCritical = false,
                CreatedBy = "System",
                MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
            };
            return emailAlertDTO;
        }

        private void NewTextAlertOnApproval(string RecipientName, string RecepientNumber, string LoanRefNo, decimal LoanAmount)
        {
            //Create email alert and insert in the database
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append(string.Format("Dear {0} ", RecipientName));
            bodyBuilder.Append(string.Format("Your loan of KES {0} with reference number-{1} has been approved.", LoanAmount, LoanRefNo));

            var textalert = new TextAlert
            {
                TextMessageRecipient = string.Format("{0}", RecepientNumber),
                TextMessageBody = string.Format("{0}", bodyBuilder),
                MaskedTextMessageBody = "",
                TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                TextMessageSecurityCritical = false,
                CreatedBy = User.Identity.Name,
                TextMessageSendRetry = 3
            };

            textalertservice.AddNewTextAlert(textalert, serviceHeader);
        }
        #endregion

        private void LoanRequestCharges(Guid LoanRequestId, Guid ChartofAccountsId, Guid GraduatedScaleId, decimal amount)
        {
            LoanRequestCharge loanrequestcharge = new LoanRequestCharge();
            loanrequestcharge.GraduatedScaleId = GraduatedScaleId;
            loanrequestcharge.ChartofAccountId = ChartofAccountsId;
            loanrequestcharge.LoanRequestId = LoanRequestId;
            loanrequestcharge.TransactionAmount = amount;
            loanrequestchargeservice.AddLoanRequestCharge(loanrequestcharge, serviceHeader);
        }

        [HttpGet]
        [Authorize(Roles = "FinanceManager")]
        public ActionResult JournalVoucher(Guid id)
        {            
            var findC2BTransactionById = c2bTransactionService.FindC2BTransactionById(id);
            var customer = new Customer();
            var branch = new Branch();
            var customerId = Guid.Empty;
            var BranchName = string.Empty;
            var CustomerName = string.Empty;
            var customerMobileNumber = string.Empty;
            if (findC2BTransactionById != null)
            {
                if (!string.IsNullOrWhiteSpace(findC2BTransactionById.MSISDN))
                {
                    customer = customerservice.FindCustomerByMobileNumber(findC2BTransactionById.MSISDN);
                    if (customer != null)
                    {
                        customerId = customer.Id;
                        CustomerName = customer.FullName;
                        customerMobileNumber = customer.MobileNumber;
                    }
                    branch = branchService.FindBranchByC2BNumber(findC2BTransactionById.BusinessShortCode);
                    BranchName = branch.BranchName;
                }
            }

            ViewBag.Customername = CustomerName;
            ViewBag.CustomerSelectlist = GetCustomerlist(customerId.ToString());
            var findSuspenseAccount = systemglmappingservice.FindSystemGeneralLedgerAccountMappingsByGLCode((int)Enumerations.SystemGeneralLedgerAccountCode.MPesaSuspsnseAccount);
            var journalVoucher = new Journal()
            {
                C2BTransactionId = findC2BTransactionById.Id,
                TransactionAmount = findC2BTransactionById.TransAmount,
                C2BStatus = findC2BTransactionById.Status,
                CustomerId = customerId,
                SuspenseAccountName = findSuspenseAccount.ChartofAccount.AccountName,
                SuspenseAccountChartofAccountId = findSuspenseAccount.ChartofAccountId,
                MpesaReceiptCode = findC2BTransactionById.TransID,
                BranchName = branch.BranchName,
                CustomerName = CustomerName,
                CustomerMobileNumber = customerMobileNumber
            };
          
            return View(journalVoucher);
        }

        [HttpPost]
        public ActionResult JournalVoucher(Journal journalVoucher)
        {
            ViewBag.Customername = journalVoucher.CustomerName;
            ViewBag.CustomerSelectlist = GetCustomerlist(journalVoucher.CustomerId.ToString());
            if (string.IsNullOrWhiteSpace(journalVoucher.CustomerMobileNumber))
            {
                ModelState.AddModelError(string.Empty, "Customer cannot be null");
                return View(journalVoucher);
            }

            if (ModelState.IsValid && journalVoucher.C2BTransactionId != null && journalVoucher.CustomerId != null && journalVoucher.SuspenseAccountChartofAccountId != null)
            {
                var findC2BTransactionById = c2bTransactionService.FindC2BTransactionById(journalVoucher.C2BTransactionId);
                var findDefaultSavingsAccount = savingsproductservice.FindDefaultSavingsProduct();
                var findCustomerById = customerservice.FindCustomerByMobileNumber(journalVoucher.CustomerMobileNumber);
                var findMpesaRepaymentGL = systemglmappingservice.FindSystemGeneralLedgerAccountMappingsByGLCode((int)Enumerations.SystemGeneralLedgerAccountCode.MPesaRepaymentAccount);                
                if (findCustomerById != null)
                {
                    var findCustomerSavingAccount = customeraccountservice.FindCustomerByCustomerIdandAccountType(findCustomerById.Id, (int)Enumerations.ProductCode.Savings);
                    try
                    {
                        if (findC2BTransactionById != null)
                        {
                            if (findC2BTransactionById.Status == (int)Enumerations.C2BTransactionStatus.Suspense)
                            {
                                if (journalVoucher.CustomerId != null && findCustomerSavingAccount.Id != null && findCustomerById != null)
                                {
                                    int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                    LoanTransactionHistory drMPesaSuspense = new LoanTransactionHistory
                                    {
                                        ChartofAccountId = journalVoucher.SuspenseAccountChartofAccountId,
                                        ContraAccountId = findDefaultSavingsAccount.ChartofAccountId,
                                        TransactionType = Enumerations.TransactionType.Debit,
                                        CustomersAccountId = findCustomerSavingAccount.Id,
                                        Description = "Manual Posting" + journalVoucher.MpesaReceiptCode,
                                        IsApproved = true,
                                        TransactionCategory = (int)Enumerations.TransactionCategory.ManualEntry,
                                        CreatedBy = User.Identity.Name,
                                        TransactionAmount = journalVoucher.TransactionAmount,
                                        BranchId = findCustomerById.BranchId,
                                        C2BTransactionId = journalVoucher.C2BTransactionId,
                                        TransactionBatchNumber = batchNo
                                    };
                                    loantransactionhistoryservice.AddNewLoanHistory(drMPesaSuspense, serviceHeader);
                                    LoanTransactionHistory crSavingsAccount = new LoanTransactionHistory
                                    {
                                        ChartofAccountId = findDefaultSavingsAccount.ChartofAccountId,
                                        ContraAccountId = journalVoucher.SuspenseAccountChartofAccountId,
                                        TransactionType = Enumerations.TransactionType.Credit,
                                        CustomersAccountId = findCustomerSavingAccount.Id,
                                        Description = "Manual Posting" + journalVoucher.MpesaReceiptCode,
                                        IsApproved = true,
                                        TransactionCategory = (int)Enumerations.TransactionCategory.ManualEntry,
                                        CreatedBy = User.Identity.Name,
                                        TransactionAmount = journalVoucher.TransactionAmount,
                                        BranchId = findCustomerById.BranchId,
                                        C2BTransactionId = journalVoucher.C2BTransactionId,
                                        TransactionBatchNumber = batchNo
                                    };
                                    loantransactionhistoryservice.AddNewLoanHistory(crSavingsAccount, serviceHeader);

                                    //Update C2B Notification to processed
                                    findC2BTransactionById.Status = (int)Enumerations.C2BTransactionStatus.JournalProcessed;
                                    c2bTransactionService.UpdateC2BTransaction(findC2BTransactionById, serviceHeader);
                                }
                                else
                                {
                                    ModelState.AddModelError(string.Empty, "Customer cannot be null.");
                                    return View(journalVoucher);
                                }
                            }
                            else if ((findC2BTransactionById.Status == (int)Enumerations.C2BTransactionStatus.Unknown || findC2BTransactionById.Status == (int)Enumerations.C2BTransactionStatus.Reversed || findC2BTransactionById.Status == (int)Enumerations.C2BTransactionStatus.PendingReconciliation || findC2BTransactionById.Status == (int)Enumerations.C2BTransactionStatus.Waiting) && findCustomerById != null)
                            {
                                if (journalVoucher.CustomerId != null && findCustomerSavingAccount.Id != null)
                                {
                                    int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                    LoanTransactionHistory drMPesaRepayment = new LoanTransactionHistory
                                    {
                                        ChartofAccountId = findMpesaRepaymentGL.ChartofAccountId,
                                        ContraAccountId = findDefaultSavingsAccount.ChartofAccountId,
                                        TransactionType = Enumerations.TransactionType.Debit,
                                        CustomersAccountId = findCustomerSavingAccount.Id,
                                        Description = "Manual Posting" + journalVoucher.MpesaReceiptCode,
                                        IsApproved = true,
                                        TransactionCategory = (int)Enumerations.TransactionCategory.ManualEntry,
                                        CreatedBy = User.Identity.Name,
                                        TransactionAmount = journalVoucher.TransactionAmount,
                                        BranchId = findCustomerById.BranchId,
                                        C2BTransactionId = journalVoucher.C2BTransactionId,
                                        TransactionBatchNumber = batchNo
                                    };
                                    loantransactionhistoryservice.AddNewLoanHistory(drMPesaRepayment, serviceHeader);
                                    LoanTransactionHistory crSavingsAccount = new LoanTransactionHistory
                                    {
                                        ChartofAccountId = findDefaultSavingsAccount.ChartofAccountId,
                                        ContraAccountId = findMpesaRepaymentGL.ChartofAccountId,
                                        TransactionType = Enumerations.TransactionType.Credit,
                                        CustomersAccountId = findCustomerSavingAccount.Id,
                                        Description = "Manual Posting" + journalVoucher.MpesaReceiptCode,
                                        IsApproved = true,
                                        TransactionCategory = (int)Enumerations.TransactionCategory.ManualEntry,
                                        CreatedBy = User.Identity.Name,
                                        TransactionAmount = journalVoucher.TransactionAmount,
                                        BranchId = findCustomerById.BranchId,
                                        C2BTransactionId = journalVoucher.C2BTransactionId,
                                        TransactionBatchNumber = batchNo
                                    };
                                    loantransactionhistoryservice.AddNewLoanHistory(crSavingsAccount, serviceHeader);

                                    //Update C2B Notification to processed
                                    findC2BTransactionById.Status = (int)Enumerations.C2BTransactionStatus.JournalProcessed;
                                    c2bTransactionService.UpdateC2BTransaction(findC2BTransactionById, serviceHeader);
                                }
                                else
                                {
                                    ModelState.AddModelError(string.Empty, "Customer cannot be null.");
                                    return View(journalVoucher);
                                }
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                        return View();
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Customer not found. Please Register this customer and try again.");
                    return View(journalVoucher);
                }
            }
            return RedirectToAction("index", "c2bnotification");

        }

        public ActionResult CustomerSearch()
        {
            return PartialView("_CustomerSearch");
        }

        public JsonResult CustomerLookUpDetails(JQueryDataTablesModel datatablemodel)
        {
            var pagecollection = customerservice.GetAllCustomers(IsolationLevel.ReadUncommitted);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var repaymentList = CustomerLookUpDetailsList(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, RepaymentList: pagecollection);


            return Json(new JQueryDataTablesResponse<Customer>(items: repaymentList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        [HttpGet]
        public ActionResult ReverseLoan(Guid id)
        {
            var loanRequest = loanrequestService.GetLoanRequestById(id);

            return View(loanRequest);
        }

        [HttpPost]
        public ActionResult ReverseLoan(LoanRequest loanrequest)
        {
            if (loanrequest != null)
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(loanrequest.ReversalRemarks))
                    {
                        var findLoanrequestbyId = loanrequestService.GetLoanRequestById(loanrequest.Id);

                        if (findLoanrequestbyId != null)
                        {
                            findLoanrequestbyId.Status = (int)Enumerations.LoanStatus.CompleteReversal;
                            loanrequestService.UpdateLoanRequest(findLoanrequestbyId, serviceHeader);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Reversal remarks cannot be null.");
                        return View(loanrequest);
                    }
                }

                catch
                {
                    return View(loanrequest);
                }
            }
            return RedirectToAction("Index");
        }

        public static IList<Customer> CustomerLookUpDetailsList(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<Customer> RepaymentList)
        {
            var GetAllTRepaymentsList = RepaymentList;

            totalRecordCount = GetAllTRepaymentsList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetAllTRepaymentsList = GetAllTRepaymentsList.Where(c => c.FullName
                    .ToLower().Contains(searchString.ToLower()) ||
                    c.MobileNumber.ToLower().Contains(searchString.ToLower()) || c.IDNumber.ToLower().Contains(searchString)).ToList();
            }

            searchRecordCount = GetAllTRepaymentsList.Count;

            IOrderedEnumerable<Customer> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "FullName":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.FullName);
                        break;

                    case "MobileNumber":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.MobileNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.MobileNumber);
                        break;

                    case "IDNumber":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.IDNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IDNumber);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [NonAction]
        public List<SelectListItem> GetCustomerlist(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultcustomer = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Customer", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultcustomer);

            var othercustomers = customerservice.GetAllCustomers(IsolationLevel.ReadUncommitted).Where(x => x.IsEnabled == true);

            if (string.IsNullOrWhiteSpace(selectedvalue))
            {
                selectedvalue = Guid.Empty.ToString();
            }
            foreach (var item in othercustomers)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.MobileNumberFullName, Value = item.Id.ToString("D") });

            return SelectList;
        }

        [HttpGet]
        public ActionResult JournalVoucherEntry()
        {
            List<SelectListItem> li = new List<SelectListItem>();

            li.Add(new SelectListItem { Text = "Please Select", Value = "0" });
            li.Add(new SelectListItem { Text = "Debit GL Account", Value = "1" });
            li.Add(new SelectListItem { Text = "Debit Customer Account", Value = "2" });            
            ViewData["TransactionType"] = li;

            List<SelectListItem> voucherTransactionType = new List<SelectListItem>();
            voucherTransactionType.Add(new SelectListItem { Text = "Please Select", Value = "0" });
            voucherTransactionType.Add(new SelectListItem { Text = "GL Account", Value= "1" });
            voucherTransactionType.Add(new SelectListItem { Text = "Customer Account", Value = "2"});

            ViewData["TransactionType2"] = voucherTransactionType;
            return View();
        }

        [HttpPost]
        public ActionResult JournalVoucherEntry(JournalVoucher journalVoucher)
        {            
            #region Rebinding after posting
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Please Select", Value = "0" });
            li.Add(new SelectListItem { Text = "Debit GL Account", Value = "1" });
            li.Add(new SelectListItem { Text = "Debit Customer Account", Value = "2" });            
            ViewData["TransactionType"] = li;
            List<SelectListItem> voucherTransactionType = new List<SelectListItem>();
            voucherTransactionType.Add(new SelectListItem { Text = "Please Select", Value = "0" });
            voucherTransactionType.Add(new SelectListItem { Text = "GL Account", Value = "1" });
            voucherTransactionType.Add(new SelectListItem { Text = "Customer Account", Value = "2" });
            ViewData["TransactionType2"] = voucherTransactionType;
            #endregion
            var findDefaultSavingsAccount = savingsproductservice.FindDefaultSavingsProduct();
            if (ModelState.IsValid)
            {
                using (var dataContext = new DataContext())
                {
                    using (var transaction = dataContext.Database.BeginTransaction())
                    {
                        try
                        {
                            if (journalVoucher.TransactionType != 0 && journalVoucher.TransactionType2 != 0)
                            {
                                if (journalVoucher.TransactionType == 1 && journalVoucher.TransactionType2 == 1)
                                {
                                    int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                    //Debit GL Account Credit -> GL When Journal.ReturnType = 1 -> Chart Of Account Id OtherWise Customer Id
                                    LoanTransactionHistory transactionTypeDebit1 = new LoanTransactionHistory
                                    {
                                        TransactionType = Enumerations.TransactionType.Debit,
                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                        TransactionAmount = journalVoucher.PrincipalAmount,
                                        TotalTransactionAmount = journalVoucher.PrincipalAmount,
                                        ChartofAccountId = journalVoucher.ReturnValue1,
                                        ContraAccountId = journalVoucher.ReturnValue2,
                                        //CustomersAccountId = findcustomersavingsaccountid.Id,
                                        Description = string.Format("{0} {1}", journalVoucher.PrimaryDescription, journalVoucher.SecondaryDescription),
                                        CreatedBy = "System",
                                        IsApproved = true,
                                        ApprovedBy = "System",
                                        TransactionCategory = (int)Enumerations.TransactionCategory.ManualEntry,
                                        TransactionBatchNumber = batchNo
                                    };
                                    dataContext.LoanTransactionHistory.Add(transactionTypeDebit1);

                                    LoanTransactionHistory transactionTypeCredit1 = new LoanTransactionHistory
                                    {
                                        TransactionType = Enumerations.TransactionType.Credit,
                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                        TransactionAmount = journalVoucher.PrincipalAmount,
                                        TotalTransactionAmount = journalVoucher.PrincipalAmount,
                                        ChartofAccountId = journalVoucher.ReturnValue2,
                                        ContraAccountId = journalVoucher.ReturnValue1,
                                        //CustomersAccountId = findcustomersavingsaccountid.Id,
                                        Description = string.Format("{0} {1}", journalVoucher.PrimaryDescription, journalVoucher.SecondaryDescription),
                                        CreatedBy = "System",
                                        IsApproved = true,
                                        ApprovedBy = "System",
                                        TransactionCategory = (int)Enumerations.TransactionCategory.ManualEntry,
                                        TransactionBatchNumber = batchNo
                                    };
                                    dataContext.LoanTransactionHistory.Add(transactionTypeCredit1);
                                }

                                    //Debit Customer Account
                                else if (journalVoucher.TransactionType == 1 && journalVoucher.TransactionType2 == 2)
                                {
                                    int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                    var findCustomerSavingId = customeraccountservice.FindCustomerByCustomerIdandAccountType(journalVoucher.ReturnValue2, (int)Enumerations.ProductCode.Savings);
                                    LoanTransactionHistory transactionTypeDebit2 = new LoanTransactionHistory
                                    {
                                        TransactionType = Enumerations.TransactionType.Debit,
                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                        TransactionAmount = journalVoucher.PrincipalAmount,
                                        TotalTransactionAmount = journalVoucher.PrincipalAmount,
                                        ChartofAccountId = journalVoucher.ReturnValue1,
                                        ContraAccountId = findDefaultSavingsAccount.ChartofAccountId,
                                        CustomersAccountId = findCustomerSavingId.Id,
                                        Description = string.Format("{0} {1}", journalVoucher.PrimaryDescription, journalVoucher.SecondaryDescription),
                                        CreatedBy = "System",
                                        IsApproved = true,
                                        ApprovedBy = "System",
                                        TransactionCategory = (int)Enumerations.TransactionCategory.ManualEntry,
                                        TransactionBatchNumber = batchNo
                                    };
                                    dataContext.LoanTransactionHistory.Add(transactionTypeDebit2);

                                    LoanTransactionHistory transactionTypeCredit2 = new LoanTransactionHistory
                                    {
                                        TransactionType = Enumerations.TransactionType.Credit,
                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                        TransactionAmount = journalVoucher.PrincipalAmount,
                                        TotalTransactionAmount = journalVoucher.PrincipalAmount,
                                        ChartofAccountId = findDefaultSavingsAccount.ChartofAccountId,
                                        ContraAccountId = journalVoucher.ReturnValue1,
                                        CustomersAccountId = findCustomerSavingId.Id,
                                        Description = string.Format("{0} {1}", journalVoucher.PrimaryDescription, journalVoucher.SecondaryDescription),
                                        CreatedBy = "System",
                                        IsApproved = true,
                                        ApprovedBy = "System",
                                        TransactionCategory = (int)Enumerations.TransactionCategory.ManualEntry,
                                        TransactionBatchNumber = batchNo
                                    };
                                    dataContext.LoanTransactionHistory.Add(transactionTypeCredit2);
                                }

                                else if (journalVoucher.TransactionType == 2 && journalVoucher.TransactionType2 == 1)
                                {
                                    int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                    var findCustomerSavingId = customeraccountservice.FindCustomerByCustomerIdandAccountType(journalVoucher.ReturnValue1, (int)Enumerations.ProductCode.Savings);
                                    LoanTransactionHistory transactionTypeDebit2 = new LoanTransactionHistory
                                    {
                                        TransactionType = Enumerations.TransactionType.Debit,
                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                        TransactionAmount = journalVoucher.PrincipalAmount,
                                        TotalTransactionAmount = journalVoucher.PrincipalAmount,
                                        ChartofAccountId = findDefaultSavingsAccount.ChartofAccountId,
                                        ContraAccountId = journalVoucher.ReturnValue2,
                                        CustomersAccountId = findCustomerSavingId.Id,
                                        Description = string.Format("{0} {1}", journalVoucher.PrimaryDescription, journalVoucher.SecondaryDescription),
                                        CreatedBy = "System",
                                        IsApproved = true,
                                        ApprovedBy = "System",
                                        TransactionCategory = (int)Enumerations.TransactionCategory.ManualEntry,
                                        TransactionBatchNumber = batchNo
                                    };
                                    dataContext.LoanTransactionHistory.Add(transactionTypeDebit2);

                                    LoanTransactionHistory transactionTypeCredit2 = new LoanTransactionHistory
                                    {
                                        TransactionType = Enumerations.TransactionType.Credit,
                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                        TransactionAmount = journalVoucher.PrincipalAmount,
                                        TotalTransactionAmount = journalVoucher.PrincipalAmount,
                                        ChartofAccountId = journalVoucher.ReturnValue2,
                                        ContraAccountId = findDefaultSavingsAccount.ChartofAccountId,
                                        CustomersAccountId = findCustomerSavingId.Id,
                                        Description = string.Format("{0} {1}", journalVoucher.PrimaryDescription, journalVoucher.SecondaryDescription),
                                        CreatedBy = "System",
                                        IsApproved = true,
                                        ApprovedBy = "System",
                                        TransactionCategory = (int)Enumerations.TransactionCategory.ManualEntry,
                                        TransactionBatchNumber = batchNo
                                    };
                                    dataContext.LoanTransactionHistory.Add(transactionTypeCredit2);
                                }

                                else if (journalVoucher.TransactionType == 2 && journalVoucher.TransactionType2 == 2)
                                {
                                    var findCustomerSavingId = customeraccountservice.FindCustomerByCustomerIdandAccountType(journalVoucher.ReturnValue1, (int)Enumerations.ProductCode.Savings);
                                    var findCustomerSavingId1 = customeraccountservice.FindCustomerByCustomerIdandAccountType(journalVoucher.ReturnValue2, (int)Enumerations.ProductCode.Savings);
                                    int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                    LoanTransactionHistory transactionTypeDebit2 = new LoanTransactionHistory
                                    {
                                        TransactionType = Enumerations.TransactionType.Debit,
                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                        TransactionAmount = journalVoucher.PrincipalAmount,
                                        TotalTransactionAmount = journalVoucher.PrincipalAmount,
                                        ChartofAccountId = findDefaultSavingsAccount.ChartofAccountId,
                                        ContraAccountId = findDefaultSavingsAccount.ChartofAccountId,
                                        CustomersAccountId = findCustomerSavingId.Id,
                                        Description = string.Format("{0} {1}", journalVoucher.PrimaryDescription, journalVoucher.SecondaryDescription),
                                        CreatedBy = "System",
                                        IsApproved = true,
                                        ApprovedBy = "System",
                                        TransactionCategory = (int)Enumerations.TransactionCategory.ManualEntry,
                                        TransactionBatchNumber = batchNo
                                    };
                                    dataContext.LoanTransactionHistory.Add(transactionTypeDebit2);

                                    LoanTransactionHistory transactionTypeCredit2 = new LoanTransactionHistory
                                    {
                                        TransactionType = Enumerations.TransactionType.Credit,
                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                        TransactionAmount = journalVoucher.PrincipalAmount,
                                        TotalTransactionAmount = journalVoucher.PrincipalAmount,
                                        ChartofAccountId = findDefaultSavingsAccount.ChartofAccountId,
                                        ContraAccountId = findDefaultSavingsAccount.ChartofAccountId,
                                        CustomersAccountId = findCustomerSavingId1.Id,
                                        Description = string.Format("{0} {1}", journalVoucher.PrimaryDescription, journalVoucher.SecondaryDescription),
                                        CreatedBy = "System",
                                        IsApproved = true,
                                        ApprovedBy = "System",
                                        TransactionCategory = (int)Enumerations.TransactionCategory.ManualEntry,
                                        TransactionBatchNumber = batchNo
                                    };
                                    dataContext.LoanTransactionHistory.Add(transactionTypeCredit2);
                                }
                            }
                            dataContext.SaveChanges(serviceHeader);
                            transaction.Commit();
                        }

                        catch (Exception ex)
                        {
                            transaction.Commit();
                            LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                        }
                    }
                }
                ModelState.AddModelError(string.Empty, "Journal Created Successfully");
                return View();
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Failed");
            }
            return View();
        }

        public JsonResult GetStates(string id)
        {
            List<SelectListItem> states = new List<SelectListItem>();
            switch (id)
            {
                case "2":
                    var findAllAvailableCustomers = customerservice.GetAllCustomers(IsolationLevel.ReadUncommitted);
                    if (findAllAvailableCustomers != null)
                    {
                        foreach (var customer in findAllAvailableCustomers)
                        {
                            states.Add(new SelectListItem { Text = customer.MobileNumberFullName, Value = customer.Id.ToString() });
                        }
                    }
                    break;
                case "1":                
                    var findChartOfAccounts = chartofaccountservice.GetChartofAccountByAccountCategory((int)Enumerations.ChartOfAccountCategory.DetailAccount);
                    if (findChartOfAccounts != null)
                    {
                        foreach (var chartofaccount in findChartOfAccounts)
                        {
                            states.Add(new SelectListItem { Text = chartofaccount.ChartOfAccountDescriptionByCategory, Value = chartofaccount.Id.ToString() });
                        }
                    }
                    break;
            }
            return Json(new SelectList(states, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult GetCity(string id)
        {
            List<SelectListItem> types = new List<SelectListItem>();
            switch (id)
            {
                case "2":
                    var findAllAvailableCustomers = customerservice.GetAllCustomers(IsolationLevel.ReadUncommitted);
                    if (findAllAvailableCustomers != null)
                    {
                        foreach (var customer in findAllAvailableCustomers)
                        {
                            types.Add(new SelectListItem { Text = customer.MobileNumberFullName, Value = customer.Id.ToString() });
                        }
                    }
                    break;
                case "1":
                    var findChartOfAccounts = chartofaccountservice.GetChartofAccountByAccountCategory((int)Enumerations.ChartOfAccountCategory.DetailAccount);
                    if (findChartOfAccounts != null)
                    {
                        foreach (var chartofaccount in findChartOfAccounts)
                        {
                            types.Add(new SelectListItem { Text = chartofaccount.ChartOfAccountDescriptionByCategory, Value = chartofaccount.Id.ToString() });
                        }
                    }
                    break;
            }

            return Json(new SelectList(types, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerSearchArray(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                id = string.Empty;
            }

            var findAllCustomers = customerservice.GetAllCustomers(IsolationLevel.ReadCommitted).Where(x => x.MobileNumber.ToLower().StartsWith(id.ToLower()));
            
            return Json(findAllCustomers, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerSearchArray2()
        {
            var findAllCustomers = customerservice.GetAllCustomers(IsolationLevel.ReadUncommitted).Select(x => x.MobileNumber).ToArray();

            return Json(findAllCustomers, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UploadFile()
        {
            ViewBag.FileUploadCategory = FileUploadCategoryCheckBoxList();
           return View();
        }

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase excelFileBlob, string FileType, Reconciliation reconFile)
        {
            try
            {
                ViewBag.FileUploadCategory = FileUploadCategoryCheckBoxList();
                if (ModelState.IsValid)
                {
                    if (excelFileBlob == null)                        
                    {                        
                        ModelState.AddModelError(string.Empty, "Please select a file to upload");
                        return View(reconFile);
                    }
                        
                    else if (string.IsNullOrWhiteSpace(excelFileBlob.FileName) || excelFileBlob.ContentLength == 0)
                    {
                        ModelState.AddModelError(string.Empty, "File Name is not specified or Content Length is zero");
                        return View(reconFile);
                    }
                        
                    else
                    {
                        switch (excelFileBlob.ContentType)
                        {
                            case "application/vnd.ms-excel":
                            case "application/vnd.xls":
                            case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                                break;
                            default:
                                ModelState.AddModelError(string.Empty, string.Format("Document has to be an xls or xlsx file. Content type reported as: {0}", excelFileBlob.ContentType));
                                break;
                        }
                    }
                    
                    if (string.IsNullOrWhiteSpace(reconFile.Remarks))
                    {
                        ModelState.AddModelError(string.Empty, "Narrative must be specified");
                        return View(reconFile);
                    }

                    if(string.IsNullOrWhiteSpace(FileType))
                    {
                        ModelState.AddModelError(string.Empty, "File type must be specified");
                        return View(reconFile);
                    }
                    
                    else
                    {
                        reconFile.FileType = (int)((Enumerations.UploadFileCategory)Enum.Parse(typeof(Enumerations.UploadFileCategory), FileType));
                    }
                    switch (excelFileBlob.ContentType)
                    {
                        case "application/vnd.ms-excel":
                        case "application/vnd.xls":
                            reconFile.FileContentType = (int)Enumerations.FileContentType.applicationvndmsexcel;
                            break;
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                            reconFile.FileContentType = (int)Enumerations.FileContentType.applicationvndopenxmlformatsofficedocumentspreadsheetmlsheet;
                            break;
                        default:
                            break;
                    }
                    string savedFileName = Path.Combine(string.Format(@"{0}Content\uploads\", AppDomain.CurrentDomain.BaseDirectory), string.Format("{0}_{1}", Guid.NewGuid(), Path.GetFileName(excelFileBlob.FileName)));
                    excelFileBlob.SaveAs(savedFileName);
                    reconFile.FilePath = savedFileName;
                    reconFile.FileName = Path.GetFileName(excelFileBlob.FileName);
                    reconFile.FileStatus = (int)Enumerations.FileProcessingStatus.Pending;
                    reconService.Add(reconFile, serviceHeader);
                }
                return RedirectToAction("UploadFileSucess");
            }


            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
            ModelState.AddModelError(string.Empty, "An error has occured.");
            return View(reconFile);
        }

        public ActionResult UploadFileSucess()
        {
            return View();
        }

        public static Tuple<List<C2BTransaction>, List<string>> ParseOrderLineItemsFileWithNPOI(string fileName)
        {
            List<C2BTransaction> lineItems = null;

            List<string> builder = new List<string>();

            using (FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                HSSFWorkbook hssfWorkbook = new HSSFWorkbook(file);

                if (hssfWorkbook != null)
                {
                    ISheet sheet = hssfWorkbook.GetSheetAt(0);

                    if (sheet != null)
                    {
                        IEnumerator rows = sheet.GetRowEnumerator();

                        if (rows != null)
                        {
                            rows.MoveNext(); // skip header row

                            lineItems = new List<C2BTransaction>();

                            int counter = default(int);

                            while (rows.MoveNext())
                            {
                                counter += 1;

                                IRow row = (HSSFRow)rows.Current;

                                ICell cell1Value = row.GetCell(0);  // CustomerName
                                ICell cell2Value = row.GetCell(1);  // CustomerMobileNumber
                                ICell cell3Value = row.GetCell(2);  // Amount
                                ICell cell4Value = row.GetCell(3);  // M-PesaReceiptCode                                
                                ICell cell5Value = row.GetCell(4);  // Paybill
                                ICell cell6Value = row.GetCell(5);  // Reference

                                object col1Value = cell1Value != null ? cell1Value.ToString() : null;
                                object col2Value = cell2Value != null ? cell2Value.ToString() : null;
                                object col3Value = cell3Value != null ? cell3Value.ToString() : null;
                                object col4Value = cell4Value != null ? cell4Value.ToString() : null;
                                object col5Value = cell5Value != null ? cell5Value.ToString() : null;
                                object col6Value = cell6Value != null ? cell6Value.ToString() : null;

                                if ((col1Value != null) && (col2Value != null && col2Value.ToString().Length <= 12) && (col3Value != null && col3Value.ToString().StringIsAllNumeric()) && (col4Value != null) && (col5Value != null && col5Value.ToString().Length == 6) && (col6Value != null && col6Value != null))
                                {
                                    try
                                    {                                        
                                        if (col2Value.ToString().Length < 12)
                                            builder.Add(string.Format("Row {0} is invalid: {1},{2},{3},{4},{5},{6} - Mobile Number must be 12 characters", (counter + 1), col1Value, col2Value, col3Value, col4Value, col5Value, col6Value));
                                        else
                                        {                                            
                                            lineItems.Add(
                                                new C2BTransaction
                                                {
                                                    Id = Guid.NewGuid(),
                                                    KYCInfo_FirstName = col1Value.ToString(),
                                                    BusinessShortCode = col5Value.ToString(),
                                                    BillRefNumber = col6Value.ToString(),                                                    
                                                    CreatedDate = DateTime.Now,
                                                    InvoiceNumber = string.Empty,
                                                    MSISDN = col2Value.ToString(),
                                                    TransAmount = decimal.Parse(col3Value.ToString()),
                                                    Result = "Manual Upload",
                                                    ThirdPartyTransID= string.Empty,
                                                    TransactionId = Guid.NewGuid(),
                                                    Status = (int)Enumerations.C2BTransactionStatus.Unknown,
                                                    TransID = cell4Value.ToString(),
                                                });
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        builder.Add(ex.Message);
                                    }
                                }
                                else
                                {
                                    builder.Add(string.Format("Row {0} is invalid: {1},{2},{3},{4},{5},{6}", (counter + 1), col1Value, col2Value, col3Value, col4Value, col5Value, col6Value));
                                }
                            }
                        }
                    }
                }
            }

            return new Tuple<List<C2BTransaction>, List<string>>(lineItems, builder);
        }

        public static Tuple<List<C2BTransaction>, List<string>> ParseOrderLineItemsFileWithEPPlus(string fileName)
        {
            List<C2BTransaction> lineItems = null;

            List<string> builder = new List<string>();

            // Get the file we are going to process
            var existingFile = new FileInfo(fileName);

            // Open and read the XlSX file.
            using (var package = new ExcelPackage(existingFile))
            {
                // Get the work book in the file
                ExcelWorkbook workBook = package.Workbook;

                if (workBook != null)
                {
                    if (workBook.Worksheets.Count > 0)
                    {
                        // Get the first worksheet
                        ExcelWorksheet currentWorksheet = workBook.Worksheets.First();

                        lineItems = new List<C2BTransaction>();

                        // read each row from the start of the data to the end of the spreadsheet.
                        for (int rowNumber = 2; rowNumber <= currentWorksheet.Dimension.End.Row; rowNumber++) // skip header row
                        {
                            object col1Value = currentWorksheet.Cells[rowNumber, 1].Value; // Customer Name
                            object col2Value = currentWorksheet.Cells[rowNumber, 2].Value; // CustomerMobile Number
                            object col3Value = currentWorksheet.Cells[rowNumber, 3].Value; // Amount
                            object col4Value = currentWorksheet.Cells[rowNumber, 4].Value; // M-pesa Code
                            object col5Value = currentWorksheet.Cells[rowNumber, 5].Value; // Paybill
                            object col6Value = currentWorksheet.Cells[rowNumber, 6].Value; // Reference

                            if ((col1Value != null) && (col2Value != null && col2Value.ToString().Length <= 12) && (col3Value != null && col3Value.ToString().StringIsAllNumeric()) && (col4Value != null) && (col5Value != null && col5Value.ToString().Length == 6) && (col6Value != null && col6Value != null))
                            {
                                try
                                {
                                    if (col2Value.ToString().Length < 12)
                                        builder.Add(string.Format("Row {0} is invalid: {1},{2},{3},{4},{5},{6} - Mobile number has to be 12 digits.", rowNumber, col1Value, col2Value, col3Value, col4Value, col5Value, col6Value));
                                    else
                                    {
                                        lineItems.Add(
                                                new C2BTransaction
                                                {
                                                    Id = Guid.NewGuid(),
                                                    KYCInfo_FirstName = col1Value.ToString(),
                                                    BusinessShortCode = col5Value.ToString(),
                                                    BillRefNumber = col6Value.ToString(),                                                    
                                                    TransAmount = decimal.Parse(col3Value.ToString()),
                                                    CreatedDate = DateTime.Now,
                                                    InvoiceNumber = string.Empty,
                                                    MSISDN = col2Value.ToString(),
                                                    Result = "Manual Upload",
                                                    ThirdPartyTransID = string.Empty,
                                                    TransactionId = Guid.NewGuid(),
                                                    Status = (int)Enumerations.C2BTransactionStatus.Unknown,
                                                    TransID = col4Value.ToString(),
                                                });
                                    }
                                }
                                catch (Exception ex)
                                {
                                    builder.Add(ex.Message);
                                }
                            }
                            else
                            {
                                builder.Add(string.Format("Row {0} is invalid: {1},{2},{3},{4},{5},{6}", rowNumber, col1Value, col2Value, col3Value, col4Value, col5Value, col6Value));
                            }
                        }
                    }
                }
            }

            return new Tuple<List<C2BTransaction>, List<string>>(lineItems, builder);
        }
    }
}
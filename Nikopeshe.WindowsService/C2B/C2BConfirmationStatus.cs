﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Messaging;
using System.Net;

namespace Nikopeshe.WindowsService.C2B
{
    public class C2BConfirmationStatus : IJob
    {
        private DataContext datacontext = new DataContext();

        string CompanyId = ConfigurationManager.AppSettings["CompanyId"].ToString();
        string spsusername = ConfigurationManager.AppSettings["SPSUsername"].ToString();
        string spsapiendpoint = ConfigurationManager.AppSettings["SPSAPIEndPoint"].ToString();
        string spspassword = ConfigurationManager.AppSettings["SPSPassword"].ToString();               
       
        C2BTransaction _c2b = new C2BTransaction();

        public void Execute(IJobExecutionContext context)        
        {
            try
            {         
                using(datacontext)
                {                    
                    IList<Nikopeshe.Entity.DataModels.C2BTransaction> findpendingc2b = datacontext.C2BTransaction.Where(x => x.Status == (int)Enumerations.C2BTransactionStatus.Pending).Take(10).ToList();
                    if (findpendingc2b != null && findpendingc2b.Any())
                    {
                        foreach (var c2b in findpendingc2b)
                        {
                            _c2b = c2b;

                            HttpWebRequest webRequest = WebRequest.Create(spsapiendpoint + c2b.TransactionId) as HttpWebRequest;

                            if (webRequest == null)
                                throw new NullReferenceException("request is not a http request");

                            webRequest.Method = "GET";
                            webRequest.ContentType = "application/json";

                            webRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(spsusername + ":" + spspassword)));
                            HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;

                            if (webResponse.StatusCode == HttpStatusCode.OK)
                            {
                                C2BTransaction c2bconfirmation = datacontext.C2BTransaction.Find(c2b.Id);
                                if (c2bconfirmation.Status == (int)Enumerations.C2BTransactionStatus.Pending)
                                {
                                    c2bconfirmation.Status = (int)Enumerations.C2BTransactionStatus.Approved;
                                    var entry = datacontext.Entry(c2bconfirmation);
                                    entry.State = EntityState.Modified;
                                    datacontext.SaveChanges();
                                }
                            }
                        }
                    }
                }                
            }

            catch (WebException ex)
            {
                var response = ex.Response as HttpWebResponse;
                if (response.StatusCode == HttpStatusCode.Forbidden)
                {
                    C2BTransaction c2bconfirmation = datacontext.C2BTransaction.Find(_c2b.Id);
                    c2bconfirmation.Status = (int)Enumerations.C2BTransactionStatus.Rejected;
                    var entry = datacontext.Entry(c2bconfirmation);
                    entry.State = EntityState.Modified;
                    datacontext.SaveChanges();
                    Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
                }
                else
                {
                    Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
                }
            }

            catch (Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }       
    }
}

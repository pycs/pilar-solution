﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.AutoReconciliation
{
    public class ReconciliationService : IReconciliationService
    {
        private DataContext datacontext = new DataContext();
        private string errorMessage = string.Empty;
        public IList<Reconciliation> FindUnProcessedFilesByStatus(int Status, int Type)
        {
            var findUnprocessedFilesByStatus = datacontext.Reconciliation.Where(x => x.FileStatus == Status && x.FileType == Type);

            return findUnprocessedFilesByStatus.ToList();
        }

        public void UpdateFile(Guid Id, int Status)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Id.ToString()) || string.IsNullOrWhiteSpace(Status.ToString()))

                    throw new ArgumentNullException("empty value(s)");

                var findrecordbyId = datacontext.Reconciliation.Find(Id);

                findrecordbyId.FileStatus = Status;

                var entry = datacontext.Entry(findrecordbyId);
                entry.State = EntityState.Modified;
                datacontext.SaveChanges();
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
        }
    }
}

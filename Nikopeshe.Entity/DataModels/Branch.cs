﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    [Serializable]
    public class Branch : BaseEntity
    {
        [Display(Name="Branch Name")]
        [Required]
        [MaxLength(100)]
        public string BranchName { get; set; }

        [Index("BranchCode", IsUnique = true)]
        [Required]
        [Column(TypeName = "VARCHAR")]
        [Display(Name="Branch Code")]
        public string BranchCode { get; set; }

        public string BranchNameBranchCode
        {
            get
            {
                return string.Format("Branch Name => {0}, Branch Code => {1}", BranchName, BranchCode);
            }
        }
        [Required]
        [Index]
        [Display(Name="Is Enabled?")]
        public bool IsEnabled { get; set; }

        [NotMapped]
        public string Commands { get; set; }

        [Display(Name="Type")]
        public int Type { get; set; }

        public string TypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.BranchType), Type) ? EnumHelper.GetDescription((Enumerations.BranchType)Type) : string.Empty;
            }
        }

        [Display(Name="C2B Number")]
        public string C2BNumber { get; set; }

        [Display(Name="Location")]
        public string Location { get; set; }

    }
}

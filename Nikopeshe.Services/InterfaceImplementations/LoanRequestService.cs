﻿using Nikopeshe.Data;
using Nikopeshe.Entity;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.DTO;
using Nikopeshe.Services.Extensions;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.Services.Specifications;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class LoanRequestService : ILoanRequestService
    {
        private readonly IRepository<LoanRequest> loanrequestreposirory;        
        private readonly IRepository<LoanProduct> loanproductrepository;
        private readonly IRepository<Branch> branchService;
        private readonly IRepository<CustomersAccount> customeraccountrepository;
        private readonly IRepository<LoanTransactionHistory> loantransactionrepository;
        private readonly IRepository<LoanProductsCharge> loanProductCharge;
        private readonly IRepository<SystemGeneralLedgerAccountMapping> systemglmappings;
        private readonly IRepository<GraduatedScale> graduatedScalerepository;
        private readonly IRepository<Charge> chargeRepository;
        DataContext datacontext = new DataContext();
        string errorMessage = string.Empty;

        public LoanRequestService(IRepository<LoanRequest> _loanrequestrepository, 
            IRepository<LoanTransactionHistory> _loantransactionrepository,
            IRepository<LoanProduct> _loanproductrepository, IRepository<CustomersAccount> _customeraccountrepository,
            IRepository<SystemGeneralLedgerAccountMapping> _systemglmappings,
            IRepository<LoanProductsCharge> _loanProductCharge, IRepository<GraduatedScale> _graduatedScalerepository,
            IRepository<Charge> _chargeRepository, IRepository<Branch> _branchService)
        {
            if(_loanproductrepository == null || _loanrequestrepository == null || _loantransactionrepository == null)
                throw new ArgumentNullException("null repository");

            this.loanproductrepository = _loanproductrepository;
            this.loantransactionrepository = _loantransactionrepository;
            this.loanrequestreposirory = _loanrequestrepository;
            this.customeraccountrepository = _customeraccountrepository;
            this.systemglmappings = _systemglmappings;
            this.chargeRepository = _chargeRepository;
            this.loanProductCharge = _loanProductCharge;
            this.graduatedScalerepository = _graduatedScalerepository;
            this.branchService = _branchService;
        }

        /// <summary>
        /// Add new loan request
        /// </summary>
        /// <param name="loanrequest"></param>
        public void AddNewLoanRequest(LoanRequest loanrequest, ServiceHeader serviceHeader)
        {
            try
            {
                if (loanrequest == null)
                    throw new ArgumentNullException("entity cannot be null");

                loanrequestreposirory.Insert(loanrequest, serviceHeader);
            }

            catch(DbEntityValidationException ex)
            {
                foreach(var validationErrors in ex.EntityValidationErrors)
                {
                    foreach(var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} ErrorMessage:{1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw new Exception(errorMessage, ex);
            }
        }

        /// <summary>
        /// Update loan request
        /// </summary>
        /// <param name="loanrequest"></param>
        public void UpdateLoanRequest(LoanRequest loanrequest, ServiceHeader serviceHeader)
        {
            try
            {
                if (loanrequest == null)
                    throw new ArgumentNullException("entity cannot be null");

                loanrequestreposirory.Update(loanrequest, serviceHeader);
            }

            catch(DbEntityValidationException ex)
            {
                foreach(var validationErrors in ex.EntityValidationErrors)
                {
                    foreach(var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} ErrorMessage{1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw new Exception(errorMessage, ex);
            }
        }

        public IList<LoanRequest> GetAllLoanRequests()
        {
            return loanrequestreposirory.GetAll(IsolationLevel.ReadUncommitted).ToList();
        }

        /// <summary>
        /// Get All Loan Request
        /// </summary>
        /// <returns></returns>
        public IList<LoanRequestList> GetAllLoanRequestsWithBalance()
        {
            bool IsMigrated = false;
            var loanrequests = loanrequestreposirory.GetAll(IsolationLevel.ReadCommitted).ToList();
            var returnlist = new List<LoanRequestList>();
            var loanbalance = 0m;
            var recurringCharge = 0m;
            decimal LoanAmount = 0m;
            decimal InterestAmount = 0m;
            foreach (var list in loanrequests)
            {
                IsMigrated = list.IsMigrated;
                var loantransactionhistory = loantransactionrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.LoanRequestId == list.Id && x.IsApproved == true
                && x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment &&
                x.TransactionType == Enumerations.TransactionType.Credit && x.CustomersAccount.AccountType == (int)Enumerations.ProductCode.Loan).ToList();
                var findLoanProductRecurringCharges = loanProductCharge.GetAll(IsolationLevel.ReadCommitted).Where(x => x.LoanProductId == list.LoanProductId && x.Charges.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment).ToList();
                if (findLoanProductRecurringCharges != null && findLoanProductRecurringCharges.Any())
                {
                    foreach (var loanCharge in findLoanProductRecurringCharges)
                    {
                        var findgradutedscale = (from a in datacontext.GraduatedScale
                                                 where a.ChargeId == loanCharge.ChargeId && (a.LowerLimit <= list.LoanAmount && a.UpperLimit >= list.LoanAmount && a.Charge.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment)
                                                 select a).ToList();
                        if (findgradutedscale != null)
                        {
                            foreach (var graduatedScale in findgradutedscale)
                            {
                                if (graduatedScale.ChargeType == Enumerations.ChargeTypes.FixedAmount)
                                {
                                    recurringCharge = graduatedScale.Value * list.NegotiatedInstallments;
                                }
                                else
                                {
                                    recurringCharge = (graduatedScale.Value / 100) * list.NegotiatedInstallments * list.LoanAmount;
                                }
                            }
                        }
                    }
                }
                if (IsMigrated == true)
                {
                    LoanAmount = list.OutstandingLoanBalance;
                    InterestAmount = 0m;
                }
                else
                {
                    LoanAmount = list.LoanAmount;
                    InterestAmount = list.InterestAmount;
                }
                decimal allpaidtrxamount = loantransactionhistory.Sum(x => x.TransactionAmount);
                decimal paidamount = allpaidtrxamount;
                loanbalance = (LoanAmount + InterestAmount) - paidamount + recurringCharge;
                returnlist.Add(new LoanRequestList
                {
                    Id = list.Id,
                    CreatedDate = list.CreatedDate,
                    CustomerName = list.CustomerAccount.Customer.FullName,
                    CustomerIdNumber = list.CustomerAccount.Customer.IDNumber,
                    CustomerMobileNumber = list.CustomerAccount.Customer.MobileNumber,
                    ProductName = list.LoanProduct.ProductName,
                    LoanReferenceNo = list.LoanReferenceNumber,
                    LoanAmount = LoanAmount,
                    InterestAmount = InterestAmount + recurringCharge,
                    Status = list.Status,
                    StatusDesc = list.StatusDesc,
                    LoanBalance = loanbalance,
                    BranchName = list.Branch.BranchName,
                    Branch = list.Branch
                });
            }

            return returnlist;
        }

        /// <summary>
        /// Get loan request by loan request id
        /// </summary>
        /// <param name="LoanRequestId"></param>
        /// <returns></returns>
        public LoanRequest GetLoanRequestById(Guid LoanRequestId)
        {
            try
            {
                if (LoanRequestId == Guid.Empty)
                    throw new ArgumentNullException("invalid loan request id");

                return loanrequestreposirory.GetById(LoanRequestId);
            }
                

            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }            
        }

        /// <summary>
        /// Get loan request by customer account id
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public IList<LoanRequest> GetLoanRequestByCustomerAccountId(Guid CustomerId)
        {            
            try
            {
                if (CustomerId == Guid.Empty)
                    throw new ArgumentNullException("invalid customer id");

                //var h = from a in datacontext.Customer join b in datacontext.CustomersAccount on a.Id equals b.CustomerId select a;
                var results = (from a in datacontext.Customer join b in datacontext.CustomersAccount on a.Id equals b.CustomerId
                                               join c in datacontext.LoanRequest on b.Id equals c.CustomerAccountId where a.Id == CustomerId select c).ToList();

                return results.ToList();                
                //return loanrequestreposirory.GetAll().Where(x => x.CustomerAccountId == CustomerAccountId).ToList();
            }

            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Get loan request by customer id
        /// </summary>
        /// <param name="loanrequestid"></param>
        /// <returns></returns>
        public LoanRequest GetLoanRequestByLoanProductId(Guid loanrequestid)
        {
            try
            {
                if (loanrequestid != Guid.Empty)
                    throw new ArgumentNullException("invalid customer id");

                return loanrequestreposirory.GetAll(IsolationLevel.ReadUncommitted).FirstOrDefault(x => x.LoanProductId == loanrequestid);
            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// get loan request by status
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public IList<LoanRequest> GetAllLoanRequestByStatus(int status, IsolationLevel isolationLevel)
        {
            try
            {
                return loanrequestreposirory.GetAll(isolationLevel).Where(x => x.Status == status).ToList();
            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Get all customer loan requests
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public IList<LoanRequest> GetAllCustomerLoanRequests(Guid CustomerId)
        {
            var results = (from a in datacontext.Customer
                           join b in datacontext.CustomersAccount on a.Id equals b.CustomerId
                           join c in datacontext.LoanRequest on b.Id equals c.CustomerAccountId
                           where a.Id == CustomerId
                           select c).ToList();

            return results.ToList();         
            //return loanrequestreposirory.GetAll().Where(x => x.CustomerAccountId == CustomerId).ToList();
        }

        /// <summary>
        /// Get all customer loan requests
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public IList<LoanRequest> GetAllCustomerActiveLoanRequests(Guid CustomerId)
        {
            var results = (from a in datacontext.Customer
                           join b in datacontext.CustomersAccount on a.Id equals b.CustomerId
                           join c in datacontext.LoanRequest on b.Id equals c.CustomerAccountId
                           where a.Id == CustomerId && c.Status != (int)Enumerations.LoanStatus.CustomerRejected && c.Status != (int)Enumerations.LoanStatus.Rejected && c.Status != (int)Enumerations.LoanStatus.Cleared 
                           select c).ToList();

            return results.ToList();                            
        }

        public int GetCustomerLoanStatus(Guid CustomerId, int status)
        {
            var getloanrequest = loanrequestreposirory.GetAll(IsolationLevel.ReadCommitted).Where(x => x.Status == status && x.CustomerAccountId == CustomerId);

            return getloanrequest.Select(x => x.Status).FirstOrDefault();
        }

        public IList<LoanRequest> GetAllCustomerLoanRequestWithPendingApprovedDibursedActiveStates(Guid CustomerId, Guid LoanProductId)
        {
            var results = (from a in datacontext.Customer
                           join b in datacontext.CustomersAccount on a.Id equals b.CustomerId
                           join c in datacontext.LoanRequest on b.Id equals c.CustomerAccountId
                           where a.Id == CustomerId && c.Status == (int)Enumerations.LoanStatus.Pending || c.Status == (int)Enumerations.LoanStatus.Approved || c.Status == (int)Enumerations.LoanStatus.Disbursed || c.Status == (int)Enumerations.LoanStatus.Active
                           select c).ToList();

            var filterresults = results.Where(x => x.LoanProductId == LoanProductId);

            return filterresults.ToList();
        }

        /// <summary>
        /// Get loan maximum, minimum and defaulf loan amount
        /// </summary>
        /// <param name="loanproductid"></param>
        /// <returns></returns>
        public Tuple<Decimal, Decimal, Decimal> GetMaxMinDefaultLoanAmount(Guid loanproductid)
        {
            if (loanproductid == Guid.Empty)
            {
                throw new Exception("guid cannot be null");
            }
            var getloanproductbyid = loanproductrepository.GetById(loanproductid);

            return new Tuple<decimal, decimal, decimal>(getloanproductbyid.MaximumAmount, getloanproductbyid.MinimumAmount, getloanproductbyid.DefaultLoanAmount);
        }

        /// <summary>
        /// compute interest amount given principle amount
        /// </summary>
        /// <param name="interestrate"></param>
        /// <param name="principleamount"></param>
        /// <returns></returns>
        public LoanRequest FindLoanRequestByReferenceNumber(string referencenumber)
        {
            var findloanrequest = loanrequestreposirory.GetAll(IsolationLevel.ReadCommitted).FirstOrDefault(s => s.LoanReferenceNumber == referencenumber);

            return findloanrequest;
        }    
   
        public int CountLoanRequestsByStatus(int status)
        {
            var findloanrequestbystatus = loanrequestreposirory.GetAll(IsolationLevel.ReadCommitted).Where(x => x.Status == status);

            return findloanrequestbystatus.Count();
        }

        public Tuple<decimal, decimal, int, int> SumAllActiveLoansAmountTotalInterest(Guid branchId)
        {            
            var fixedAmountCharge = 0m;
            var percentageAmountCharge = 0m;
            decimal loanRepaymentTotalAmount = 0m;
            decimal interestRepaymentTotalAmount = 0m;
            int duetodayloans = 0;
            int activeTotalLoanCount = 0;
            var findBranchById = branchService.GetById(branchId);
            var getallactiveloan = new List<LoanRequest>();
            if (findBranchById != null)
            {
                if (findBranchById.BranchCode == "000")
                {
                    getallactiveloan = loanrequestreposirory.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.Status == (int)Enumerations.LoanStatus.Active).ToList();
                }
                else
                {
                    getallactiveloan = loanrequestreposirory.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.Status == (int)Enumerations.LoanStatus.Active && x.BranchId == branchId).ToList();            
                }               
            }
            else
            {
                getallactiveloan = loanrequestreposirory.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.Status == (int)Enumerations.LoanStatus.Active).ToList();
            }

            activeTotalLoanCount = getallactiveloan.Count();
            duetodayloans = getallactiveloan.Where(x => x.NextPaymentDate.Value.Date == DateTime.Today.Date).Count();
            //Find all per installment charge per loan product
            //if (getallactiveloan != null && getallactiveloan.Any())
            //{
            //    foreach (var loanProduct in getallactiveloan)
            //    {
            //        var findLoanProductRecurringCharges = loanProductCharge.GetAll().Where(x => x.LoanProductId == loanProduct.LoanProductId && x.Charges.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment).ToList();
            //        if (findLoanProductRecurringCharges != null && findLoanProductRecurringCharges.Any())
            //        {
            //            foreach (var loanCharge in findLoanProductRecurringCharges)
            //            {
            //                var findgradutedscale = (from a in datacontext.GraduatedScale
            //                                         where a.ChargeId == loanCharge.ChargeId && (a.LowerLimit <= loanProduct.LoanAmount && a.UpperLimit >= loanProduct.LoanAmount && a.Charge.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment)
            //                                         select a).ToList();
            //                if (findgradutedscale != null)
            //                {
            //                    foreach (var graduatedScale in findgradutedscale)
            //                    {
            //                        if (graduatedScale.ChargeType == Enumerations.ChargeTypes.FixedAmount)
            //                        {
            //                            fixedAmountCharge = graduatedScale.Value * loanProduct.NegotiatedInstallments + fixedAmountCharge;
            //                        }
            //                        else
            //                        {
            //                            percentageAmountCharge = (graduatedScale.Value / 100) * loanProduct.NegotiatedInstallments * loanProduct.LoanAmount + percentageAmountCharge;
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}

            var sumLoanAmount = getallactiveloan.Sum(x => x.LoanAmount);
            var sumInterestAmount = getallactiveloan.Sum(x => x.InterestAmount) + fixedAmountCharge + percentageAmountCharge;
            
            //var wantedLoanProducts = getallactiveloan.Select(x => x.LoanProductId).ToList();
            
            //if (findBranchById != null)
            //{
            //    if (findBranchById.BranchCode == "000")
            //    {
            //        foreach (var loanProduct in wantedLoanProducts)
            //        {
            //            var findLoanProductById = loanproductrepository.GetById(loanProduct);
            //            var loanRepayments = loantransactionrepository.GetAll().Where(x => x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment
            //                && x.TransactionType == Enumerations.TransactionType.Credit && x.CustomersAccount.AccountType == (int)Enumerations.ProductCode.Loan
            //                && x.LoanRequest.Status == (int)Enumerations.LoanStatus.Active && x.ChartofAccountId == findLoanProductById.LoanGLAccountId).ToList<LoanTransactionHistory>();

            //            var interestRepayments = loantransactionrepository.GetAll().Where(x => x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment
            //                && x.TransactionType == Enumerations.TransactionType.Credit && x.CustomersAccount.AccountType == (int)Enumerations.ProductCode.Loan
            //                && x.LoanRequest.Status == (int)Enumerations.LoanStatus.Active && x.ChartofAccountId == findLoanProductById.InterestGLAccountId).ToList<LoanTransactionHistory>();
            //            loanRepaymentTotalAmount = loanRepayments.Sum(x => x.TransactionAmount);
            //            interestRepaymentTotalAmount = interestRepayments.Sum(x => x.TransactionAmount);
            //        }
            //    }
            //    else
            //    {
            //        foreach (var loanProduct in wantedLoanProducts)
            //        {
            //            var findLoanProductById = loanproductrepository.GetById(loanProduct);
            //            var loanRepayments = loantransactionrepository.GetAll().Where(x => x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment
            //                && x.TransactionType == Enumerations.TransactionType.Credit && x.CustomersAccount.AccountType == (int)Enumerations.ProductCode.Loan
            //                && x.LoanRequest.Status == (int)Enumerations.LoanStatus.Active && x.BranchId == branchId && x.ChartofAccountId == findLoanProductById.LoanGLAccountId).ToList<LoanTransactionHistory>();

            //            var interestRepayments = loantransactionrepository.GetAll().Where(x => x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment
            //                && x.TransactionType == Enumerations.TransactionType.Credit && x.CustomersAccount.AccountType == (int)Enumerations.ProductCode.Loan
            //                && x.LoanRequest.Status == (int)Enumerations.LoanStatus.Active && x.BranchId == branchId && x.ChartofAccountId == findLoanProductById.InterestGLAccountId).ToList<LoanTransactionHistory>();
            //            loanRepaymentTotalAmount = loanRepayments.Sum(x => x.TransactionAmount);
            //            interestRepaymentTotalAmount = interestRepayments.Sum(x => x.TransactionAmount);
            //        }
            //    }
            //}
            //else
            //{
            //    foreach (var loanProduct in wantedLoanProducts)
            //    {
            //        var findLoanProductById = loanproductrepository.GetById(loanProduct);
            //        var loanRepayments = loantransactionrepository.GetAll().Where(x => x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment
            //            && x.TransactionType == Enumerations.TransactionType.Credit && x.CustomersAccount.AccountType == (int)Enumerations.ProductCode.Loan
            //            && x.LoanRequest.Status == (int)Enumerations.LoanStatus.Active && x.ChartofAccountId == findLoanProductById.LoanGLAccountId).ToList();

            //        var interestRepayments = loantransactionrepository.GetAll().Where(x => x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment
            //            && x.TransactionType == Enumerations.TransactionType.Credit && x.CustomersAccount.AccountType == (int)Enumerations.ProductCode.Loan
            //            && x.LoanRequest.Status == (int)Enumerations.LoanStatus.Active && x.ChartofAccountId == findLoanProductById.InterestGLAccountId).ToList();
            //        loanRepaymentTotalAmount = loanRepayments.Sum(x => x.TransactionAmount);
            //        interestRepaymentTotalAmount = interestRepayments.Sum(x => x.TransactionAmount);
            //    }
            //}

            decimal resultLoanAmount = sumLoanAmount - loanRepaymentTotalAmount;
            decimal resultInterestAmount = sumInterestAmount - interestRepaymentTotalAmount;
            return new Tuple<decimal, decimal, int, int>(resultLoanAmount, resultInterestAmount, activeTotalLoanCount, duetodayloans);
        }

        public IList<LoanRequest> GetCustomerPendingLoanRequest(Guid CustomerId)
        {
            var results = (from a in datacontext.Customer
                           join b in datacontext.CustomersAccount on a.Id equals b.CustomerId
                           join c in datacontext.LoanRequest on b.Id equals c.CustomerAccountId
                           where a.Id == CustomerId && c.Status == (int)Enumerations.LoanStatus.Pending
                           select c).ToList();

            return results;
        }

        public IList<CustomerLookUpDetails> CustomerLookUpDetails()
        {
            var customerdetails = new List<CustomerLookUpDetails>();
            var getallactiveloans = loanrequestreposirory.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.Status == (int)Enumerations.LoanStatus.Active);

            foreach (var activeloan in getallactiveloans)
            {                                
                customerdetails.Add(new CustomerLookUpDetails
                {
                    CustomerName = activeloan.CustomerAccount.Customer.FullName,
                    CustomerMobileNumber = activeloan.CustomerAccount.Customer.MobileNumber,
                    LoanAmount = activeloan.LoanAmount + activeloan.InterestAmount,
                    LoanRequestId = activeloan.Id,
                    CustomeIdNumber = activeloan.CustomerAccount.Customer.IDNumber,
                    LoanReferenceNumber = activeloan.LoanReferenceNumber,
                    NextPaymentDate = activeloan.NextPaymentDate,
                    CustomerId = activeloan.CustomerAccount.CustomerId               
                });
            }
            return customerdetails;
        }

        public Tuple<decimal, decimal> LoanRequestInterestAndCharges(Guid LoanRequestId)
        {
            decimal percentageAmountCharge = 0m;
            decimal fixedAmountCharge = 0m;
            if (LoanRequestId == Guid.Empty)
            {
                throw new ArgumentNullException("loan request Id cannot be null.");
            }

            var findLoanRequestById = loanrequestreposirory.GetById(LoanRequestId);
            if (findLoanRequestById != null)
            {
                var findLoanProduct = loanproductrepository.GetById(findLoanRequestById.LoanProductId);
                if (findLoanProduct != null)
                {
                    var findLoanProductRecurringCharges = loanProductCharge.GetAll(IsolationLevel.ReadCommitted).Where(x => x.LoanProductId == findLoanProduct.Id && x.Charges.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment).ToList();
                    foreach (var loanCharge in findLoanProductRecurringCharges)
                    {
                        var findgradutedscale = (from a in datacontext.GraduatedScale
                                                 where a.ChargeId == loanCharge.ChargeId && (a.LowerLimit <= findLoanRequestById.LoanAmount && a.UpperLimit >= findLoanRequestById.LoanAmount && a.Charge.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment)
                                                 select a).ToList();
                        if (findgradutedscale != null)
                        {
                            foreach (var graduatedScale in findgradutedscale)
                            {
                                if (graduatedScale.ChargeType == Enumerations.ChargeTypes.FixedAmount)
                                {
                                    fixedAmountCharge = graduatedScale.Value * findLoanRequestById.NegotiatedInstallments + fixedAmountCharge;
                                }
                                else
                                {
                                    percentageAmountCharge = (graduatedScale.Value / 100) * findLoanRequestById.NegotiatedInstallments * findLoanRequestById.LoanAmount + percentageAmountCharge;
                                }
                            }
                        }
                    }
                }
            }

            var charge = fixedAmountCharge + percentageAmountCharge;
            var inter = findLoanRequestById.InterestAmount;
            return new Tuple<decimal, decimal>(charge, inter);
        }

        public IList<LoanRequestList> GetAllLoanRequestsWithBalanceByStatus(int Status)
        {
            bool IsMigrated = false;
            var loanrequests = loanrequestreposirory.GetAll(IsolationLevel.ReadCommitted).Where(x => x.Status == Status).ToList();
            var returnlist = new List<LoanRequestList>();
            var loanbalance = 0m;
            var recurringCharge = 0m;
            decimal LoanAmount = 0m;
            decimal InterestAmount = 0m;
            foreach (var list in loanrequests)
            {
                IsMigrated = list.IsMigrated;
                var loantransactionhistory = loantransactionrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.LoanRequestId == list.Id && x.IsApproved == true
                && x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment &&
                x.TransactionType == Enumerations.TransactionType.Credit && x.CustomersAccount.AccountType == (int)Enumerations.ProductCode.Loan).ToList();
                var findLoanProductRecurringCharges = loanProductCharge.GetAll(IsolationLevel.ReadCommitted).Where(x => x.LoanProductId == list.LoanProductId && x.Charges.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment).ToList();
                if (findLoanProductRecurringCharges != null && findLoanProductRecurringCharges.Any())
                {
                    foreach (var loanCharge in findLoanProductRecurringCharges)
                    {
                        var findgradutedscale = (from a in datacontext.GraduatedScale
                                                 where a.ChargeId == loanCharge.ChargeId && (a.LowerLimit <= list.LoanAmount && a.UpperLimit >= list.LoanAmount && a.Charge.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment)
                                                 select a).ToList();
                        if (findgradutedscale != null)
                        {
                            foreach (var graduatedScale in findgradutedscale)
                            {
                                if (graduatedScale.ChargeType == Enumerations.ChargeTypes.FixedAmount)
                                {
                                    recurringCharge = graduatedScale.Value * list.NegotiatedInstallments;
                                }
                                else
                                {
                                    recurringCharge = (graduatedScale.Value / 100) * list.NegotiatedInstallments * list.LoanAmount;
                                }
                            }
                        }
                    }
                }
                if (IsMigrated == true)
                {
                    LoanAmount = list.OutstandingLoanBalance;
                    InterestAmount = 0m;
                }
                else
                {
                    LoanAmount = list.LoanAmount;
                    InterestAmount = list.InterestAmount;
                }
                decimal allpaidtrxamount = loantransactionhistory.Sum(x => x.TransactionAmount);
                decimal paidamount = allpaidtrxamount;
                loanbalance = (LoanAmount + InterestAmount) - paidamount + recurringCharge;
                returnlist.Add(new LoanRequestList
                {
                    Id = list.Id,
                    CreatedDate = list.CreatedDate,
                    CustomerName = list.CustomerAccount.Customer.FullName,
                    CustomerIdNumber = list.CustomerAccount.Customer.IDNumber,
                    CustomerMobileNumber = list.CustomerAccount.Customer.MobileNumber,
                    ProductName = list.LoanProduct.ProductName,
                    LoanReferenceNo = list.LoanReferenceNumber,
                    LoanAmount = LoanAmount,
                    InterestAmount = InterestAmount + recurringCharge,
                    Status = list.Status,
                    StatusDesc = list.StatusDesc,
                    LoanBalance = loanbalance,
                    BranchName = list.Branch.BranchName,
                    Branch = list.Branch
                });
            }

            return returnlist;
        }

        public PageCollectionInfo<LoanRequest> FindLoanRequestFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = LoanRequestSpecification.DefaultSpec(searchString);

            ISpecification<LoanRequest> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var orderLinePagedCollection = loanrequestreposirory.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (orderLinePagedCollection != null)
            {
                var pageCollection = orderLinePagedCollection.Items.ToList();

                var totalRecordCount = orderLinePagedCollection.TotalItems;

                return new PageCollectionInfo<LoanRequest> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }

        public PageCollectionInfo<LoanRequest> GetCustomerLookupDetailsByFilterInPage(int status, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = LoanRequestSpecification.CustomerLookUpDetailsSpecifications(status, searchString);

            ISpecification<LoanRequest> spec = filter;

            var sortFields = new List<string> { "NextPaymentDate" };

            var customerLookUpDetails = loanrequestreposirory.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (customerLookUpDetails != null)
            {
                var pageCollection = customerLookUpDetails.Items.ToList();

                var totalRecordCount = customerLookUpDetails.TotalItems;

                return new PageCollectionInfo<LoanRequest> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else
                return null;
        }

        public PageCollectionInfo<LoanRequest> FindLoanRequestByBranchFilterInPage(Guid branchId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = LoanRequestSpecification.LoanRequestByBranchId(branchId, searchString);

            ISpecification<LoanRequest> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var loanRequestPagedCollection = loanrequestreposirory.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (loanRequestPagedCollection != null)
            {
                var pageCollection = loanRequestPagedCollection.Items.ToList();

                var totalRecordCount = loanRequestPagedCollection.TotalItems;

                return new PageCollectionInfo<LoanRequest> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }

        public PageCollectionInfo<LoanRequest> FindLoanRequestByGroupFilterInPage(Guid groupId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = LoanRequestSpecification.LoanRequestByGroupId(groupId, searchString);

            ISpecification<LoanRequest> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var loanRequestPagedCollection = loanrequestreposirory.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (loanRequestPagedCollection != null)
            {
                var pageCollection = loanRequestPagedCollection.Items.ToList();

                var totalRecordCount = loanRequestPagedCollection.TotalItems;

                return new PageCollectionInfo<LoanRequest> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }

        public PageCollectionInfo<LoanRequest> FindDisbursementListFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = LoanRequestSpecification.LoanDisbursementList(searchString);

            ISpecification<LoanRequest> spec = filter;

            var sortFields = new List<string> { "DisbursementDate" };

            var loanRequestPagedCollection = loanrequestreposirory.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (loanRequestPagedCollection != null)
            {
                var pageCollection = loanRequestPagedCollection.Items.ToList();

                var totalRecordCount = loanRequestPagedCollection.TotalItems;

                return new PageCollectionInfo<LoanRequest> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }


        public PageCollectionInfo<LoanRequest> FindPendingDisbursementListFilterInPage(int status,int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = LoanRequestSpecification.PendingDisbursementList(status,searchString);

            ISpecification<LoanRequest> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var loanRequestPagedCollection = loanrequestreposirory.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (loanRequestPagedCollection != null)
            {
                var pageCollection = loanRequestPagedCollection.Items.ToList();

                var totalRecordCount = loanRequestPagedCollection.TotalItems;

                return new PageCollectionInfo<LoanRequest> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }
    }

    public class LoanRequestList
    {
        public Guid Id { get; set; }

        public DateTime CreatedDate { get; set; }

        public string CustomerName { get; set; }

        public string CustomerIdNumber { get; set; }

        public string CustomerMobileNumber { get; set; }

        public string ProductName { get; set; }

        public string LoanReferenceNo { get; set; }

        public decimal LoanAmount { get; set; }

        public decimal InterestAmount { get; set; }

        public int Status { get; set; }

        public string StatusDesc { get; set; }

        public string Commands { get; set; }

        public decimal LoanBalance { get; set; }

        public decimal RecurringCharges { get; set; }

        public virtual Branch Branch { get; set; }

        public string BranchName { get; set; }
    }
}

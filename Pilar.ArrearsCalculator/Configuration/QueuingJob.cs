﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.ArrearsCalculator;
using Quartz;
using System;
using System.Configuration;
using System.Messaging;

namespace Pilar.ArrearsCalculator.Configuration
{
    public class QueuingJob : IJob
    {
        private readonly IMessageQueueService _messageQueueService;        
        private readonly string _messageRelayQueuePath;
        private readonly ICalculator icalculatorService;
        private readonly DataContext datacontext = new DataContext();

        public QueuingJob()
        {
            icalculatorService = new Calculator();
            _messageQueueService = new MessageQueueService();
            _messageRelayQueuePath = ConfigurationManager.AppSettings["ArrearsQueuePath"];
        }
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var loanInArrears = icalculatorService.SPResultsList();
                if (loanInArrears.Count != 0)
                {
                    foreach (var item in loanInArrears)
                    {
                        _messageQueueService.Send(_messageRelayQueuePath, item, Enumerations.MessageCategory.Arrears, MessagePriority.AboveNormal);
                    }
                }
            }

            catch(Exception ex)
            {
                LoggerFactory.CreateLog().LogError("Error -> ", ex);
            }
        }
    }
}

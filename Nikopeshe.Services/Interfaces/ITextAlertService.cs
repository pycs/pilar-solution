﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;

namespace Nikopeshe.Services.Interfaces
{
    public interface ITextAlertService
    {
        void AddNewTextAlert(TextAlert emailalert, ServiceHeader serviceHeader);

        IList<TextAlert> FindTextAlertByDRLStatus(int status);

        void UpdateTextAlertDRLStatus(Guid Id, int status, ServiceHeader serviceHeader);
    }
}

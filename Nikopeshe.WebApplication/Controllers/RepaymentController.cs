﻿using Crosscutting.NetFramework.Logging;
using Infrastructure.Crosscutting.NetFramework;
using Nikopeshe.Data;
using Nikopeshe.Entity;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    [Authorize]
    public class RepaymentController : Controller
    {
        private readonly ILoanTransactionHistoryService loantransactionservice;
        private readonly ILoanRequestService loanrequestservice;
        private readonly ILoanProductsChargeService loanProductCharge;
        private readonly ICustomerAccountService customerAccountService;
        private readonly ISavingsProductService savingsProductService;
        private readonly ISystemGeneralLedgerAccountMappingService systemtraceuditnumberservice;
        private readonly ILoanProductService loanproductservice;
        private readonly IChargesService chargeService;
        private readonly IGraduatedScaleService graduatedScaleService;
        private readonly IEmailAlertService emailAlertService;
        private readonly ITextAlertService textAlertService;
        private readonly ICustomerService customerservice;
        private string TextSignature = ConfigurationManager.AppSettings["TextSignature"].ToString();
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public RepaymentController(ILoanTransactionHistoryService _loantransactionservice, 
            ICustomerService _customerservice, ILoanRequestService _loanerquestservice,
            ILoanProductService _loanproductservice,
            ISystemGeneralLedgerAccountMappingService _systemtraceuditnumberservice,
            ISavingsProductService _savingsProductService,
            ICustomerAccountService _customerAccountService,
            IEmailAlertService _emailAlertService,
            ITextAlertService _textAlertService,
            ILoanProductsChargeService _loanProductCharge,
            IGraduatedScaleService _graduatedScaleService,
            IChargesService _chargeService)
        {
            if(_loanerquestservice == null || _loanproductservice == null || _customerservice == null || _loantransactionservice == null)
                throw new ArgumentNullException("null service reference");

            this.loantransactionservice = _loantransactionservice;
            this.customerservice = _customerservice;
            this.loanproductservice = _loanproductservice;
            this.loanrequestservice = _loanerquestservice;
            this.systemtraceuditnumberservice = _systemtraceuditnumberservice;
            this.savingsProductService = _savingsProductService;
            this.customerAccountService = _customerAccountService;
            this.emailAlertService = _emailAlertService;
            this.textAlertService = _textAlertService;
            this.loanProductCharge = _loanProductCharge;
            this.graduatedScaleService = _graduatedScaleService;
            this.chargeService = _chargeService;
        }

        public ActionResult Payments()
        {
            return View();
        }
        //
        // GET: /Repayment/
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAllRepayments(JQueryDataTablesModel datatablemodel)
        {
            int transactiontype = (int)Enumerations.TransactionType.Credit;

            var pagecollection = loantransactionservice.GetAllTransactionsByTransactionTypeAndTransactionCategory(transactiontype, (int)Enumerations.TransactionCategory.Repayment, IsolationLevel.ReadUncommitted);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var repaymentList = GetAllTRepayments(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, RepaymentList: pagecollection);


            return Json(new JQueryDataTablesResponse<LoanTransactionHistory>(items: repaymentList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<LoanTransactionHistory> GetAllTRepayments(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanTransactionHistory> RepaymentList)
        {
            var GetAllTRepaymentsList = RepaymentList;

            totalRecordCount = GetAllTRepaymentsList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetAllTRepaymentsList = GetAllTRepaymentsList.Where(c => c.LoanRequest.CustomerAccount.Customer.FullName
                    .ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetAllTRepaymentsList.Count;

            IOrderedEnumerable<LoanTransactionHistory> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "LoanRequest.CustomerAccount.Customer.FullName":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.CustomerAccount.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.CustomerAccount.Customer.FullName);
                        break;

                    case "ContraAccountChartofAccount.AccountName":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.ContraAccountChartofAccount.AccountName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ContraAccountChartofAccount.AccountName);
                        break;

                    case "LoanChartofAccountGL.ChartofAccountDescription":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.ChartofAccountDescription)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.ChartofAccountDescription);
                        break;

                    case "TransactionTypeDesc":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.TransactionTypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransactionTypeDesc);
                        break;

                    case "ChartofAccount.ChartofAccountDescription":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.ChartofAccountDescription)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.ChartofAccountDescription);
                        break;

                    case "IsApproved":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.IsApproved)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsApproved);
                        break;

                    case "ApprovedBy":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.ApprovedBy)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ApprovedBy);
                        break;

                    case "TransactionAmount":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.TransactionAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransactionAmount);
                        break;

                    case "CreatedBy":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.CreatedBy)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedBy);
                        break;

                    case "Description":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.Description)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Description);
                        break;  
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult loanrepayment(Guid? id)
        {                                               
            decimal loanamount = 0m;
            if (id != null)
            {
                Guid LoanRequestId = Guid.Parse(id.ToString());
                var findloanrequestbyreferencenumber = loanrequestservice.GetLoanRequestById(LoanRequestId);                
                var finddefaultsavingaccountid = savingsProductService.FindDefaultSavingsProduct();               
                var findcustomersavingsaccountid = customerAccountService.GetAllCustomersAccount(IsolationLevel.ReadCommitted).Where(x => x.CustomerId == findloanrequestbyreferencenumber.CustomerAccount.CustomerId && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
                CustomerAccountInfo customerAccountInfo = customerAccountService.GetCustomerAccountDetailsByCustomerSavingsAccountId(findcustomersavingsaccountid.Id);
                decimal installmentamount = customerAccountInfo.TotalLoanAmount / findloanrequestbyreferencenumber.NegotiatedInstallments;
                decimal totalrepayment = customerAccountInfo.TotalLoanAmountPaid;
                ViewBag.CustomerMobileNumber = findcustomersavingsaccountid.Customer.MobileNumber;
                ViewBag.CustomerFullName = findcustomersavingsaccountid.Customer.FullName;
                ViewBag.LoanRefNo = findloanrequestbyreferencenumber.LoanReferenceNumber;
                loanamount = customerAccountInfo.TotalLoanAmount;
                var paidamount = customerAccountInfo.PrincipalPaid;
                var paidInterest = customerAccountInfo.InterestPaid;
                ViewBag.LoanAmount = string.Concat(loanamount);
                var balance = customerAccountInfo.TotalAmountDue;
                ViewBag.PaidAmount = string.Concat(totalrepayment);
                ViewBag.Balance = string.Concat(balance);
                ViewBag.SavingsAccountBalance = string.Concat(customerAccountInfo.AccountBalance);
                ViewBag.NextPaymentDate = findloanrequestbyreferencenumber.NextPaymentDate.Value.ToShortDateString();
                ViewBag.InstallmentAmount = string.Concat(installmentamount);
            }
            
            return View();         
        }
        
        [HttpPost]        
        public ActionResult loanrepayment(Guid? id, [Bind(Exclude = "LoanReferenceNo")]LoanTransactionHistory loantransactionhistory)
        {
            decimal expectedinterestamount = 0m;            
            decimal principalamount = 0m;
            decimal totalLoanAmount = 0m;
            decimal expectedRecurringCharges = 0m;
            decimal receivedRecurringCharges = 0m;
            decimal totalInterestAmount = 0m;
            decimal principalPerInstallment = 0m;
            decimal receivedInterestAmount = 0m;
            decimal loanamount = 0m;
            decimal paidamount = 0m;
            decimal paidInterest = 0m;
            decimal TransactionAmount = loantransactionhistory.TransactionAmount;
            if (id == null)
            {
                ModelState.AddModelError(string.Empty, "An error has occurred while processing. Please try again.");
                return View("loanrepayment");
            }

            if (TransactionAmount <= 0)
            {
                ModelState.AddModelError(string.Empty, "Transaction amount must be greater than 0.");
                return View(loantransactionhistory);
            }
            CustomersAccount findcustomersavingsaccountid = new CustomersAccount();
            var findloanrequest = new LoanRequest();
            var finddefaultsavingaccountid = savingsProductService.FindDefaultSavingsProduct();               
            Guid LoanRequestId = Guid.Parse(id.ToString());
            findloanrequest = loanrequestservice.GetLoanRequestById(LoanRequestId);
            finddefaultsavingaccountid = savingsProductService.FindDefaultSavingsProduct();
            findcustomersavingsaccountid = customerAccountService.GetAllCustomersAccount(IsolationLevel.ReadCommitted).Where(x => x.CustomerId == findloanrequest.CustomerAccount.CustomerId && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
            CustomerAccountInfo customerAccountInfo = customerAccountService.GetCustomerAccountDetailsByCustomerSavingsAccountId(findcustomersavingsaccountid.Id);
            decimal findCustomerSavingsAccountBalance = customerAccountInfo.AccountBalance;
            if (id != null)
            {
                decimal installmentamount = customerAccountInfo.TotalLoanAmount / findloanrequest.NegotiatedInstallments;
                decimal totalrepayment = customerAccountInfo.TotalLoanAmountPaid;
                ViewBag.CustomerMobileNumber = findcustomersavingsaccountid.Customer.MobileNumber;
                ViewBag.CustomerFullName = findcustomersavingsaccountid.Customer.FullName;
                ViewBag.LoanRefNo = findloanrequest.LoanReferenceNumber;
                loanamount = customerAccountInfo.TotalLoanAmount;
                paidamount = customerAccountInfo.PrincipalPaid;
                paidInterest = customerAccountInfo.InterestPaid;
                ViewBag.LoanAmount = string.Concat(loanamount);
                var balance = customerAccountInfo.TotalAmountDue;
                ViewBag.PaidAmount = string.Concat(totalrepayment);
                ViewBag.Balance = string.Concat(balance);
                ViewBag.SavingsAccountBalance = string.Concat(customerAccountInfo.AccountBalance);
                ViewBag.NextPaymentDate = findloanrequest.NextPaymentDate;
            }
            
            var getloanproductfromloanhistory = loanproductservice.FindLoanProductById(findloanrequest.LoanProductId);            

            loantransactionhistory.TransactionType = Enumerations.TransactionType.Credit;

            if (findCustomerSavingsAccountBalance < loantransactionhistory.TransactionAmount)
            {
                ModelState.AddModelError(string.Empty, "Insufficient Balance. The available balance is KES" + findCustomerSavingsAccountBalance);
                return View(loantransactionhistory);
            }
            if(ModelState.IsValid)
            {
                try
                {                                     
                    if (findCustomerSavingsAccountBalance >= loantransactionhistory.TransactionAmount)
                    {                       
                        var interesrratepermonth = findloanrequest.LoanProduct.InterestValue / 12;

                        expectedinterestamount = customerAccountInfo.InterestDue;
                        principalPerInstallment = customerAccountInfo.PrincipalDue;
                        #region Dr Savings GL, Cr Income GL

                        if (TransactionAmount > findCustomerSavingsAccountBalance)
                        {
                            TransactionAmount = findCustomerSavingsAccountBalance;
                        }

                        if (TransactionAmount >= expectedinterestamount)
                        {
                            receivedInterestAmount = expectedinterestamount;
                        }

                        else
                        {
                            receivedInterestAmount = TransactionAmount;
                        }

                        TransactionAmount = TransactionAmount - receivedInterestAmount;

                        if (receivedInterestAmount > 0)
                        {
                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                            LoanTransactionHistory creditincomegl = new LoanTransactionHistory()
                            {
                                LoanRequestId = LoanRequestId,
                                TransactionType = Enumerations.TransactionType.Debit,
                                RepaymentType = loantransactionhistory.RepaymentType,
                                TransactionAmount = receivedInterestAmount,
                                TotalTransactionAmount = loantransactionhistory.TransactionAmount,
                                ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                ContraAccountId = findloanrequest.LoanProduct.InterestGLAccountId,
                                CustomersAccountId = findcustomersavingsaccountid.Id,
                                Description = "Interest Liquidation " + loantransactionhistory.Description,
                                CreatedBy = User.Identity.Name,
                                IsApproved = true,
                                ApprovedBy = string.Empty,
                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                BranchId = findloanrequest.BranchId,
                                TransactionBatchNumber = batchNo
                            };

                            loantransactionservice.AddNewLoanHistory(creditincomegl, serviceHeader);

                        #endregion

                            #region Dr Income GL, Dr Saving GL

                            LoanTransactionHistory debitincomegl = new LoanTransactionHistory()
                            {
                                LoanRequestId = LoanRequestId,
                                TransactionType = Enumerations.TransactionType.Credit,
                                RepaymentType = loantransactionhistory.RepaymentType,
                                TransactionAmount = receivedInterestAmount,
                                TotalTransactionAmount = loantransactionhistory.TransactionAmount,
                                ChartofAccountId = findloanrequest.LoanProduct.InterestGLAccountId,
                                ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                CustomersAccountId = findloanrequest.CustomerAccountId,
                                Description = "Interest Liquidation " + loantransactionhistory.Description,
                                CreatedBy = User.Identity.Name,
                                IsApproved = true,
                                ApprovedBy = string.Empty,
                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                BranchId = findloanrequest.BranchId,
                                TransactionBatchNumber = batchNo
                            };

                            loantransactionservice.AddNewLoanHistory(debitincomegl, serviceHeader);
                        }

                            #endregion

                        var findProductRecurringCharges = loanProductCharge.GetRecurringLoanProductChargeByProductId(findloanrequest.LoanProductId, IsolationLevel.ReadCommitted);
                        if (findProductRecurringCharges.Count != 0 && findProductRecurringCharges.Any())
                        {
                            foreach (var recurringCharge in findProductRecurringCharges)
                            {
                                var findRecurringCharges = chargeService.FindChargeById(recurringCharge.ChargeId);
                                if (findProductRecurringCharges != null)
                                {
                                    var findGraduatedScaleByChargeId = graduatedScaleService.FindGraduatedScaleUsingRangeAndChargeId(loantransactionhistory.TransactionAmount, findRecurringCharges.Id);
                                    if (findGraduatedScaleByChargeId != null)
                                    {
                                        foreach (var graduatedScale in findGraduatedScaleByChargeId)
                                        {
                                            var chargeType = graduatedScale.ChargeType;
                                            if (chargeType == Enumerations.ChargeTypes.FixedAmount)
                                            {
                                                expectedRecurringCharges = graduatedScale.Value;
                                            }

                                            else
                                            {
                                                expectedRecurringCharges = (graduatedScale.Value * loantransactionhistory.TransactionAmount) / 100;
                                            }

                                            if (TransactionAmount >= expectedRecurringCharges)
                                            {
                                                receivedRecurringCharges = expectedRecurringCharges;
                                            }
                                            else
                                            {
                                                receivedRecurringCharges = TransactionAmount;
                                            }
                                            TransactionAmount = TransactionAmount - receivedRecurringCharges;
                                            if (receivedRecurringCharges > 0m)
                                            {
                                                #region Dr Savings GL, Cr Charge GL
                                                int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                LoanTransactionHistory postChargeDr = new LoanTransactionHistory()
                                                {
                                                    LoanRequestId = LoanRequestId,
                                                    TransactionType = Enumerations.TransactionType.Debit,
                                                    RepaymentType = loantransactionhistory.RepaymentType,
                                                    TransactionAmount = receivedRecurringCharges,
                                                    TotalTransactionAmount = loantransactionhistory.TransactionAmount,
                                                    ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                    ContraAccountId = graduatedScale.Charge.ChartofAccountId,
                                                    CustomersAccountId = findcustomersavingsaccountid.Id,
                                                    Description = "Charge Liquidation " + loantransactionhistory.Description,
                                                    CreatedBy = User.Identity.Name,
                                                    IsApproved = true,
                                                    ApprovedBy = string.Empty,
                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                    BranchId = findloanrequest.BranchId,
                                                    TransactionBatchNumber = batchNo
                                                };

                                                loantransactionservice.AddNewLoanHistory(postChargeDr, serviceHeader);

                                                #endregion

                                                #region Cr Charge GL, Dr Saving GL

                                                LoanTransactionHistory postChargeCr = new LoanTransactionHistory()
                                                {
                                                    LoanRequestId = LoanRequestId,
                                                    TransactionType = Enumerations.TransactionType.Credit,
                                                    RepaymentType = loantransactionhistory.RepaymentType,
                                                    TransactionAmount = receivedRecurringCharges,
                                                    TotalTransactionAmount = loantransactionhistory.TransactionAmount,
                                                    ChartofAccountId = graduatedScale.Charge.ChartofAccountId,
                                                    ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                    CustomersAccountId = findloanrequest.CustomerAccountId,
                                                    Description = "Charge Liquidation " + loantransactionhistory.Description,
                                                    CreatedBy = User.Identity.Name,
                                                    IsApproved = true,
                                                    ApprovedBy = string.Empty,
                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                    BranchId = findloanrequest.BranchId,
                                                    TransactionBatchNumber = batchNo
                                                };

                                                loantransactionservice.AddNewLoanHistory(postChargeCr, serviceHeader);

                                                #endregion
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        principalamount = loantransactionhistory.TransactionAmount - receivedInterestAmount - receivedRecurringCharges;
                        totalLoanAmount = findloanrequest.LoanAmount;
                        totalInterestAmount = findloanrequest.InterestAmount;

                        decimal receivedPrincipal = 0m;

                        if (principalamount >= principalPerInstallment)
                        {
                            receivedPrincipal = principalPerInstallment;
                        }

                        else
                        {
                            receivedPrincipal = principalamount;
                        }
                        if (receivedPrincipal > 0m)
                        {
                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                            #region  Debit Savings GL
                            LoanTransactionHistory debitsavingsgl = new LoanTransactionHistory()
                            {
                                LoanRequestId = LoanRequestId,
                                TransactionType = Enumerations.TransactionType.Debit,
                                RepaymentType = loantransactionhistory.RepaymentType,
                                TransactionAmount = receivedPrincipal,
                                TotalTransactionAmount = loantransactionhistory.TransactionAmount,
                                ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                ContraAccountId = Guid.Parse(findloanrequest.LoanProduct.LoanGLAccountId.ToString()),
                                CustomersAccountId = findcustomersavingsaccountid.Id,
                                Description = "Principal Liquidation " + loantransactionhistory.Description,
                                CreatedBy = User.Identity.Name,
                                IsApproved = true,
                                ApprovedBy = string.Empty,
                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                BranchId = findloanrequest.BranchId,
                                TransactionBatchNumber = batchNo
                            };

                            loantransactionservice.AddNewLoanHistory(debitsavingsgl, serviceHeader);
                            #endregion

                            #region  Credit Loan GL
                            LoanTransactionHistory creditsavingsgl = new LoanTransactionHistory()
                            {
                                LoanRequestId = LoanRequestId,
                                TransactionType = Enumerations.TransactionType.Credit,
                                RepaymentType = loantransactionhistory.RepaymentType,
                                TransactionAmount = receivedPrincipal,
                                TotalTransactionAmount = loantransactionhistory.TransactionAmount,
                                ChartofAccountId = findloanrequest.LoanProduct.LoanGLAccountId,
                                ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                CustomersAccountId = findloanrequest.CustomerAccountId,
                                Description = "Principal Liquidation " + loantransactionhistory.Description,
                                CreatedBy = User.Identity.Name,
                                IsApproved = true,
                                ApprovedBy = string.Empty,
                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                BranchId = findloanrequest.BranchId,
                                TransactionBatchNumber = batchNo
                            };

                            loantransactionservice.AddNewLoanHistory(creditsavingsgl, serviceHeader);
                            #endregion
                        }

                        var frequency = getloanproductfromloanhistory.PaymentFrequencyType;
                        decimal expectedInstallmentAmount = principalPerInstallment + expectedRecurringCharges + expectedinterestamount;
                        decimal receivedInstallmentAmount = loantransactionhistory.TransactionAmount;
                        if (receivedInstallmentAmount >= expectedInstallmentAmount)
                        {
                            if (frequency == Enumerations.PaymentFrequency.Weekly)
                            {
                                findloanrequest.NextPaymentDate = findloanrequest.NextPaymentDate.Value.AddDays(7);
                            }
                            else
                            {
                                findloanrequest.NextPaymentDate = findloanrequest.NextPaymentDate.Value.AddMonths(1);
                            }
                        }

                        loanrequestservice.UpdateLoanRequest(findloanrequest, serviceHeader);                       
                    }                
                }

                catch(Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(loantransactionhistory);                    
                }
                return RedirectToAction("Index");
            }
            return View(loantransactionhistory);
        }

        public ActionResult approveloanrepayment(Guid Id)
        {
            var findloanrepaymentbyid = loantransactionservice.GetLoanTransactionsHistoryById(Id);

            return View(findloanrepaymentbyid);
        }

        [ActionName("approveloanrepayment")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult _approveloanrepayment(LoanTransactionHistory loantransactionhistory)
        {
            var findloanrepaymentbyid = loantransactionservice.GetLoanTransactionsHistoryById(loantransactionhistory.Id);            
           
            try
            {
                if (!findloanrepaymentbyid.CreatedBy.Equals(User.Identity.Name))
                {
                    findloanrepaymentbyid.ApprovedBy = User.Identity.Name;
                    findloanrepaymentbyid.Description = loantransactionhistory.Description;
                    findloanrepaymentbyid.IsApproved = true;
                    loantransactionservice.UpdateLoanTransactionHistory(loantransactionhistory, serviceHeader);

                }
               
                return RedirectToAction("Index");
            }

            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
            return View(findloanrepaymentbyid);
        }

        [NonAction]
        public List<SelectListItem> GetCustomerlist(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultcustomerselectlist = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Customer <--Type Customer Id Number to filter-->", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultcustomerselectlist);

            var otherRMItems = loanrequestservice.GetAllLoanRequests().Where(x => x.CustomerAccount.Customer.IsEnabled == true && x.Status == (int)Enumerations.LoanStatus.Active);          

            var id = otherRMItems.Select(x => x.Id);

            foreach (var item in otherRMItems)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.CustomerIdNumberLoanRefMobileNumber, Value = item.Id.ToString("D") });

            return SelectList;
        }

        [HttpGet]
        public JsonResult GetVendors(string term )
        {
            var finduserbyidno = customerservice.GetAllCustomers(IsolationLevel.ReadUncommitted).Where(x => x.IDNumber.Contains(term.ToUpper().Trim()));

            var modelData = finduserbyidno.Select(m => new SelectListItem()
            {
                Text = m.IDNumber,
                Value = m.Id.ToString(),
                Selected = m.IsApproved,
            });

            return Json(modelData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewLyubomir()
        {
            return PartialView("_CustomerLoopUp");
        }

        [HttpPost]
        public ActionResult Lyubomir()
        {
            return RedirectToAction("Index");
        }

        public ActionResult CustomerLookUp(int? page)
        {
            return View();
        }

        public JsonResult CustomerLookUpDetails(JQueryDataTablesModel jQueryDataTablesModel)
        {
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc" ? true : false;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = loanrequestservice.GetCustomerLookupDetailsByFilterInPage((int)Enumerations.LoanStatus.Active, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, sortAscending);

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;
            }

            return this.DataTablesJson(items: pageCollectionInfo.PageCollection, totalRecords: totalRecordCount, totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
        }

        public static IList<CustomerLookUpDetails> CustomerLookUpDetailsList(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<CustomerLookUpDetails> RepaymentList)
        {
            var GetAllTRepaymentsList = RepaymentList;

            totalRecordCount = GetAllTRepaymentsList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetAllTRepaymentsList = GetAllTRepaymentsList.Where(c => c.CustomerName
                    .ToLower().Contains(searchString.ToLower()) ||
                    c.CustomerMobileNumber.ToLower().Contains(searchString.ToLower())
                    || c.LoanReferenceNumber.ToLower().Contains(searchString.ToLower()) || c.CustomeIdNumber.ToLower().Contains(searchString)).ToList();
            }

            searchRecordCount = GetAllTRepaymentsList.Count;

            IOrderedEnumerable<CustomerLookUpDetails> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CustomerName":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.CustomerName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerName);
                        break;

                    case "CustomerMobileNumber":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.CustomerMobileNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerMobileNumber);
                        break;

                    case "LoanAmount":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount);
                        break;

                    case "LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber);
                        break;

                    case "CustomeIdNumber":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.CustomeIdNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomeIdNumber);
                        break;

                    case "LoanRequestId":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.LoanRequestId.ToString())
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanRequestId.ToString());
                        break;

                    case "LoanBalance":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.LoanBalance)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanBalance);
                        break;

                    case "NextPaymentDate":
                        sortedClients = sortedClients == null ? GetAllTRepaymentsList.CustomSort(sortedColumn.Direction, cust => cust.NextPaymentDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.NextPaymentDate);
                        break;                    
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }               

        [HttpGet]
        public ActionResult CustomerSearch()
        {
            return View();
        }
        
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CustomerSearch(CustomerLookUpView lookupview)
        {
            return RedirectToAction("loanrepayment", lookupview);
        }

        [HttpGet]
        public ActionResult Adjust(Guid id)
        {
            var findLoanTransactionHistoryById = loantransactionservice.GetLoanTransactionsHistoryById(id);
            return View(findLoanTransactionHistoryById);
        }

        [HttpPost]
        public ActionResult Adjust(LoanTransactionHistory loanTransactions)
        {
            return View();
        }
	}

    public class CustomerLookUpView
    {
        public string CustomerFullName { get; set; }
        public string CustomerMobileNumber { get; set; }
        public decimal LoanAmount { get; set; }
        public string LoanReferenceNumber { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal Balance { get; set; }
        public string NextPaymentDate { get; set; }
    }
}
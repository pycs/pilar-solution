﻿using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services;
using Nikopeshe.WcfWebService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nikopeshe.WcfWebService.InterfaceImplementation
{
    public class MailAlert : IEmailAlertService
    {
        readonly IRepository<EmailAlert> emailalertrepository;

        string errorMessage = string.Empty;

        public MailAlert(IRepository<EmailAlert> _emailalertrepository)
        {
            this.emailalertrepository = _emailalertrepository;
        }

        public IList<EmailAlert> FindEmailAlertByDRLStatus(int status)
        {
            if (string.IsNullOrWhiteSpace(status.ToString()))
                throw new ArgumentNullException("null entity");

            var results = emailalertrepository.GetAll().Where(x => x.MailMessageDLRStatus == status);

            var count = results.Count();

            return results.ToList();
        }
    }
}
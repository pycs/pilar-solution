﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.InterfaceImplementations;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class SelfRegisteredController : Controller
    {
        private readonly ICustomerDocumentService customerdocumentservice;
        private readonly ICustomerService customerservice;
        private readonly ILoanRequestService loanrequestservice;
        private readonly ILoanRequestDocumentsService loanrequestdocumentservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public SelfRegisteredController(ICustomerDocumentService _customerdocumentservice, ILoanRequestService _loanrequestservice, ICustomerService _customerservice, ILoanRequestDocumentsService _loanrequestdocumentservice)
        {
            this.loanrequestdocumentservice = _loanrequestdocumentservice;
            this.loanrequestservice = _loanrequestservice;
            this.customerdocumentservice = _customerdocumentservice;
            this.customerservice = _customerservice;
        }
        // GET: SelfRegistered
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UploadFile()
        {            
            return View();
        }

        /// <summary>
        /// Upload Customer Document
        /// </summary>
        /// <returns></returns>
        ///
        [HttpPost]        
        public ActionResult UploadFile(CustomerDocument customerdocument, HttpPostedFileBase PostedFile)
        {
            var getuserid = customerservice.FindCustomerByMobileNumber(User.Identity.Name);

            if (getuserid.Id == Guid.Empty)
            {
                ModelState.AddModelError(string.Empty, "Unexpected error has occurred.");
                return View(customerdocument);
            }

            if (customerdocument.DocumentType == 0)
            {
                ModelState.AddModelError(string.Empty, "Document Type cannot be null.");
                return View(customerdocument);
            }
            customerdocument.CustomerId = getuserid.Id;
            if (ModelState.IsValid)
            {
                try
                {
                    if (PostedFile != null && PostedFile.ContentLength > 0)
                    {
                        // extract only the filename
                        var fileName = Path.GetFileName(PostedFile.FileName);
                        // store the file inside ~/App_Data/uploads folder
                        var path = Path.Combine(Server.MapPath("~/Content/Uploads"), fileName);
                        var getfileextension = Path.GetExtension(path);
                        if (getfileextension != ".pdf")
                        {
                            ModelState.AddModelError(string.Empty, "Wrong file type. The file must have .pdf extension.");
                            return View(customerdocument);
                        }

                        PostedFile.SaveAs(path);
                        var fileData = new MemoryStream();
                        PostedFile.InputStream.CopyTo(fileData);
                        customerdocument.Image = fileData.ToArray();
                        customerdocumentservice.AddNewCustomerDocument(customerdocument, serviceHeader);
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "You must select a file.");
                        return View(customerdocument);
                    }
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(customerdocument);
                }
            }
            return RedirectToAction("UploadSuccess");
        }

        [HttpGet]
        public ActionResult UploadLoanApplicationDocuments()
        {
            var findcustomer = customerservice.FindCustomerByMobileNumber(User.Identity.Name);
            ViewBag.PendingLoan = GetAllCustomerLoans(findcustomer.Id.ToString());
            return View();
        }

        /// <summary>
        /// Upload Loan Request Document
        /// </summary>
        /// <param name="loanrequestdocument"></param>
        /// <returns></returns>
        [HttpPost]        
        public ActionResult UploadLoanApplicationDocuments(LoanRequestDocument loanrequestdocument, HttpPostedFileBase PostedFile, FormCollection collection)
        {
            var findcustomer = customerservice.FindCustomerByMobileNumber(User.Identity.Name);
            ViewBag.PendingLoan = GetAllCustomerLoans(findcustomer.Id.ToString());
                     
            if (loanrequestdocument.LoanRequestId == Guid.Empty)
            {
                ModelState.AddModelError(string.Empty, "Loan Request is null.");
                return View(loanrequestdocument);
            }

            if (loanrequestdocument.DocumentType == 0)
            {
                ModelState.AddModelError(string.Empty, "Document Type cannot be null.");
                return View(loanrequestdocument);
            }
            try
            {
                if (PostedFile != null && PostedFile.ContentLength > 0)
                {
                    // extract only the filename
                    var fileName = Path.GetFileName(PostedFile.FileName);
                    // store the file inside ~/App_Data/uploads folder
                    var path = Path.Combine(Server.MapPath("~/Content/Uploads"), fileName);
                    var getfileextension = Path.GetExtension(path);
                    if (getfileextension != ".pdf")
                    {
                        ModelState.AddModelError(string.Empty, "Wrong file type. The file must have .pdf extension");
                        return View(loanrequestdocument);
                    }

                    PostedFile.SaveAs(path);
                    var fileData = new MemoryStream();
                    PostedFile.InputStream.CopyTo(fileData);
                    loanrequestdocument.Image = fileData.ToArray();
                    loanrequestdocumentservice.AddNewLoanRequestDocuments(loanrequestdocument, serviceHeader);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "You must select a file.");
                    return View(loanrequestdocument);
                }
                return RedirectToAction("UploadSuccess");
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View(loanrequestdocument);
            }
        }

        public ActionResult CustomerDocuments()
        {
            var currentuser = User.Identity.Name;
            var findcustomerbymobilenumber = customerservice.FindCustomerByMobileNumber(currentuser);
            ViewBag.Id = findcustomerbymobilenumber.Id;
            ViewBag.CustomerId = findcustomerbymobilenumber.Id;
            ViewBag.CustomerName = findcustomerbymobilenumber.FullName;
            return View();
        }

        public JsonResult GetCustomerDocuments(JQueryDataTablesModel datatablemodel, Guid Id)
        {
            var customerdocuments = customerdocumentservice.GetAllCustomerDocuments(Id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetAllCustomerDocument(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, CustomerDocumentList: customerdocuments);


            var results = Json(new JQueryDataTablesResponse<CustomerDocumentView>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));

            results.MaxJsonLength = Int32.MaxValue;

            return results;
        }

        public static IList<CustomerDocumentView> GetAllCustomerDocument(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<CustomerDocumentView> CustomerDocumentList)
        {
            var GetCustomerDocumentList = CustomerDocumentList;

            totalRecordCount = GetCustomerDocumentList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomerDocumentList = CustomerDocumentList.Where(c => c.Customer.IDNumber
                    .ToLower().Contains(searchString.ToLower())
                    || c.Customer.MobileNumber.ToLower().Contains(searchString.ToLower())
                    ).ToList();
            }

            searchRecordCount = GetCustomerDocumentList.Count;

            IOrderedEnumerable<CustomerDocumentView> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "Customer.FullName":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Customer.FullName);
                        break;

                    case "Customer.IDNumber":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.Customer.IDNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Customer.IDNumber);
                        break;

                    case "Customer.MobileNumber":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.Customer.MobileNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Customer.MobileNumber);
                        break;

                    //case "DocumentTypeDesc":
                    //    sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.DocumentTypeDesc)
                    //        : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.DocumentTypeDesc);
                    //    break;                    
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }


        public ActionResult UploadSuccess()
        {
            return View();
        }

        [NonAction]
        public List<SelectListItem> GetAllCustomerLoans(string selectedValue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultItem = new SelectListItem { Selected = (string.Concat(selectedValue) == string.Empty), Text = "Select Loan", Value = string.Empty };

            SelectList.Add(defaultItem);

            var otherItems = loanrequestservice.GetCustomerPendingLoanRequest(Guid.Parse(selectedValue));

            if (otherItems != null)
            {
                foreach (var item in otherItems)
                {
                    SelectList.Add(new SelectListItem { Selected = item.Id.ToString() == selectedValue, Text = item.CustomerIdNumberLoanRefMobileNumber, Value = item.Id.ToString() });
                }
            }
            return SelectList;
        }
    }
}
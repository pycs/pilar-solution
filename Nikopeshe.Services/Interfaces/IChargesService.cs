﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface IChargesService
    {
        void AddNewCharge(Charge charge, ServiceHeader serviceHeader);

        void UpdateCharge(Charge charge, ServiceHeader serviceHeader);

        Charge FindChargeById(Guid Id);

        IList<Charge> GetAllCharges();
    }
}

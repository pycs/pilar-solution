﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface ILoanTransactionHistoryService
    {
        void AddNewLoanHistory(LoanTransactionHistory loanhistory, ServiceHeader serviceHeader);

        void UpdateLoanTransactionHistory(LoanTransactionHistory loanhistory, ServiceHeader serviceHeader);

        IList<LoanTransactionHistory> GetAllLoanTransactionsHistory();

        LoanTransactionHistory GetLoanTransactionsHistoryById(Guid Id);

        IList<LoanTransactionHistory> GetLoanTransactionHistoryByLoanRequestId(Guid CustomerId);

        LoanTransactionHistory GetLoanTransactionsByLoanRequestId(Guid LoanRequestId);

        IList<LoanTransactionHistory> GetAllCustomerTransationHistory(Guid CustomerId);

        Tuple<Decimal, Decimal> SumofAllTransactionsPerTransactionType(Guid CustomerId, int TransactionType);

        IList<LoanTransactionHistory> GetAllTransactionsByType(int type);

        IList<LoanTransactionHistory> GetAllTransactionsByTransactionTypeAndTransactionCategory(int TransactionType, int TransactionCategory, IsolationLevel isolationLevel);

        IList<LoanTransactionHistory> GetLoanTransactionHistoryByLoanRequestIdAndCustomerAccountId(Guid LoanRequestId, Guid CustomerAccountId, bool IsLoanMigrated);
    }
}

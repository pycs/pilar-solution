﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class GuarantorService : IGuarantorService
    {
        private IRepository<Guarantor> guarantorRepository;

        string ErrorMessage = string.Empty;
        public GuarantorService(IRepository<Guarantor> _guarantorRepository)
        {
            if (_guarantorRepository == null)
                throw new ArgumentNullException("null repository");

            this.guarantorRepository = _guarantorRepository;
        }

        public void AddNewGuarantor(Guarantor guarantor, ServiceHeader serviceHeader)
        {
            try
            {
                if (guarantor == null)
                    throw new ArgumentNullException("null entity");

                guarantorRepository.Insert(guarantor, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            } 
        }
    }
}

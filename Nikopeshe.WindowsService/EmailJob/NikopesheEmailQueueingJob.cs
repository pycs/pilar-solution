﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.WindowsService.EmailJob
{
    public class NikopesheEmailQueueingJob : IJob
    {
        string _queuePath = ConfigurationManager.AppSettings["MessageRelayQueuePath"];

        public enum MessageType
        {
            MESSAGE_TYPE_EMAIL_ALERT = 0,            
        };

        private static MessageQueue _messageQueue = null;
        private static object lockObject = new object();        

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                if (!MessageQueue.Exists(_queuePath))
                    throw new ArgumentException("Queue does not exist at specified path", "_queuePath");
                else
                {
                    // initialize message queue
                    _messageQueue = new MessageQueue(_queuePath);

                    // enable the AppSpecific field in the messages
                    _messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                    // set the formatter to binary.
                    _messageQueue.Formatter = new BinaryMessageFormatter();

                    // set an event to listen for new messages.
                    _messageQueue.ReceiveCompleted += MessageQueue_ReceiveCompleted;

                    // Delete all messages from the queue.
                    _messageQueue.Purge();

                    // start listening
                    _messageQueue.BeginReceive();
                }
            }

            catch(Exception ex)
            {
                // Handle Message Queuing exceptions.
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }            

        }

        static void MessageQueue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                // retrieve sms alert id
                var recordId = (EmailAlert)e.Message.Body;

                // retrieve message category
                var messageCategory = (Nikopeshe.Entity.DataModels.Enumerations.MessageCategory)e.Message.AppSpecific;
            }

            catch(Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }

        /// <summary>
        /// Find all pending email
        /// </summary>
        /// <returns></returns>
        public IList<EmailAlert> FindEmailsWithPendingDRLStatus()
        {
            DataContext datacontext = new DataContext();

            int numberofemails = int.Parse(ConfigurationManager.AppSettings["EmailNumberofItems"]);

            var pendingemails = datacontext.EmailAlert.Where(x => x.MailMessageDLRStatus == (int)Enumerations.DLRStatus.Pending).Take(numberofemails);

            return pendingemails.ToList();
        }
    }
}

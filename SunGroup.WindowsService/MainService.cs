﻿using Crosscutting.NetFramework.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SunGroup.WindowsService
{
    public partial class MainService : ServiceBase
    {
        PluginProvider pluginProvider = null;

        public MainService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                ConfigureFactories();

                LoggerFactory.CreateLog().LogInfo("Starting...");

                pluginProvider = new PluginProvider();

                pluginProvider.Initialize();

                LoggerFactory.CreateLog().LogInfo("Available Plugins -> {0}", pluginProvider.AvailablePlugins);

                if (pluginProvider.AvailablePlugins != 0)
                {
                    pluginProvider.SignalDoWork(args);
                }
            }
            catch (Exception ex)
            {
                if (ex is System.Reflection.ReflectionTypeLoadException)
                {
                    var typeLoadException = ex as System.Reflection.ReflectionTypeLoadException;

                    var loaderExceptions = typeLoadException.LoaderExceptions;

                    if (loaderExceptions != null)
                    {
                        foreach (var item in loaderExceptions)
                        {
                            LoggerFactory.CreateLog().LogInfo("Pilar.WindowsService..." + item);
                        }
                    }
                }
                else LoggerFactory.CreateLog().LogInfo("Pilar.WindowsService..." + ex);
            }
        }

        protected override void OnStop()
        {
            LoggerFactory.CreateLog().LogInfo("Stopping...");

            LoggerFactory.CreateLog().LogInfo("Available Plugins -> {0} - ", pluginProvider.AvailablePlugins);

            if (pluginProvider != null && pluginProvider.AvailablePlugins != 0)
            {
                pluginProvider.SignalExit();
            }
        }

        public void StartDebugging(string[] args)
        {
            OnStart(args);
        }

        private static void ConfigureFactories()
        {
            LoggerFactory.SetCurrent(new EntLibLogFactory());

            System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) =>
            {
                return true;
            };
        }
    }
}

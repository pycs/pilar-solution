﻿using Crosscutting.NetFramework.Logging;
using Microsoft.Reporting.WebForms;
using Nikopeshe.Data;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using Nikopeshe.WebApplication.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class ReportController : BaseController
    {
        // GET: Reports
        private string TargetReportServerURL = string.Empty;
        private string SSRSReportPassword = string.Empty;
        private string SSRSReportUsername = string.Empty;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        private readonly IReportService reportService;
        private readonly IReportInRoleService reportInRoleService;
        public ReportController(IReportService _reportService, IReportInRoleService _reportInRoleService)
        {
            TargetReportServerURL = ConfigurationManager.AppSettings["ReportTargetServerURL"].ToString();
            SSRSReportPassword = ConfigurationManager.AppSettings["SSRSReportPassword"].ToString();
            SSRSReportUsername = ConfigurationManager.AppSettings["SSRSReportUsername"].ToString();
            reportService = _reportService;
            reportInRoleService = _reportInRoleService;
        }
        public ActionResult Index()
        {
            var getRolesInList = GetUserRoleInfoList(true);

            return View(getRolesInList);
        }

        public JsonResult GetAllReports(JQueryDataTablesModel datatablemodel)
        {
            var findCurrentUser = User.Identity.Name.ToString();
            var findCurrentUserRole = Roles.GetRolesForUser(findCurrentUser);
            var findReportByGivenCategoryName = new List<Guid>();
            if (Roles.IsUserInRole("Administrator"))
            {
                findReportByGivenCategoryName = reportInRoleService.GetAllReportsInCategory().Select(x => x.ReportId).Distinct().ToList();
            }
            else
            {               
                findReportByGivenCategoryName = reportInRoleService.GetAllReportsInCategory().Where(x => x.RoleDescription == findCurrentUserRole[0].ToString()).Select(x => x.ReportId).ToList();
            }

            var pagecollection = new List<Nikopeshe.Entity.DataModels.Report>();

            foreach (var reportId in findReportByGivenCategoryName)
            {
                var findreportById = reportService.FindReportById(reportId);
                pagecollection.Add(new Entity.DataModels.Report
                {
                    Commands = findreportById.Commands,
                    CreatedDate = findreportById.CreatedDate,
                    FolderName = findreportById.FolderName,
                    Id = findreportById.Id,
                    IsEnabled = findreportById.IsEnabled,
                    ReportName = findreportById.ReportName,
                    RoleCategory = findreportById.RoleCategory
                });
            }

            int totalrecordcount = 0;

            int searchrecordcount = 0;

            var reportList = GetAllReportsList(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalrecordcount, searchRecordCount: out searchrecordcount, searchString: datatablemodel.sSearch, reportList: pagecollection);

            return Json(new JQueryDataTablesResponse<Nikopeshe.Entity.DataModels.Report>(items: reportList,
                totalRecords: totalrecordcount,
                totalDisplayRecords: searchrecordcount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<Nikopeshe.Entity.DataModels.Report> GetAllReportsList(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<Nikopeshe.Entity.DataModels.Report> reportList)
        {
            var GetReportList = reportList;

            totalRecordCount = GetReportList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetReportList = GetReportList.Where(c => c.ReportName.ToLower().Contains(searchString.ToLower())
                    || c.FolderName.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetReportList.Count;

            IOrderedEnumerable<Nikopeshe.Entity.DataModels.Report> sortedClients = null;

            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetReportList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "ReportName":
                        sortedClients = sortedClients == null ? GetReportList.CustomSort(sortedColumn.Direction, cust => cust.ReportName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ReportName);
                        break;

                    case "IsParameterized":
                        sortedClients = sortedClients == null ? GetReportList.CustomSort(sortedColumn.Direction, cust => cust.IsParameterized)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsParameterized);
                        break;

                    case "ParameterName":
                        sortedClients = sortedClients == null ? GetReportList.CustomSort(sortedColumn.Direction, cust => cust.ParameterName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ParameterName);
                        break;

                    case "ReportPath":
                        sortedClients = sortedClients == null ? GetReportList.CustomSort(sortedColumn.Direction, cust => cust.ReportPath)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ReportPath);
                        break;

                    case "Description":
                        sortedClients = sortedClients == null ? GetReportList.CustomSort(sortedColumn.Direction, cust => cust.Description)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Description);
                        break;

                    case "FolderName":
                        sortedClients = sortedClients == null ? GetReportList.CustomSort(sortedColumn.Direction, cust => cust.FolderName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.FolderName);
                        break;

                    case "IsEnabled":
                        sortedClients = sortedClients == null ? GetReportList.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled);
                        break;
                }
            }
            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }
        
        public ActionResult ViewReport(Guid Id)
        {
            var findReportById = reportService.FindReportById(Id);
            if (findReportById != null)
            {
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Remote;
                reportViewer.SizeToReportContent = true;
                reportViewer.Width = Unit.Percentage(100);
                //if(!string.IsNullOrWhiteSpace(SSRSReportPassword) && !string.IsNullOrWhiteSpace(SSRSReportUsername))
                //{
                //    IReportServerCredentials irsc = new ReportServerCredentials(SSRSReportUsername, SSRSReportPassword, "app.houseofpayments.com");
                //    reportViewer.ServerReport.ReportServerCredentials = irsc;
                //}
                                     
                reportViewer.ShowCredentialPrompts = true;
                reportViewer.Height = Unit.Percentage(100);
                reportViewer.Width = Unit.Percentage(100);
                reportViewer.Enabled = true;
                reportViewer.ShowPrintButton = true;
                reportViewer.ZoomMode = ZoomMode.FullPage;
                reportViewer.KeepSessionAlive = true;                
                reportViewer.ServerReport.ReportPath = "/" + findReportById.FolderName + "/" + findReportById.ReportName;
                reportViewer.ShowBackButton = true;
                reportViewer.ServerReport.ReportServerUrl = new Uri(TargetReportServerURL);
                ViewBag.ReportViewer = reportViewer;

            }

            else
            {
                ModelState.AddModelError(string.Empty, "No report found.");
                return View();
            }
            
            return View();
        }

        public ActionResult report(Guid Id)
        {
            var findReportById = reportService.FindReportById(Id);
            if (findReportById != null)
            {
                ReportViewer reportViewer = new ReportViewer();
                reportViewer.ProcessingMode = ProcessingMode.Remote;
                reportViewer.SizeToReportContent = true;                              
                reportViewer.ShowCredentialPrompts = true;
                reportViewer.Width = Unit.Percentage(99);
                reportViewer.Height = Unit.Pixel(1000);
                reportViewer.Enabled = true;
                reportViewer.ShowPrintButton = true;
                reportViewer.ZoomMode = ZoomMode.PageWidth;
                reportViewer.AsyncRendering = true;
                reportViewer.KeepSessionAlive = true;
                reportViewer.ServerReport.ReportPath = "/" + findReportById.FolderName + "/" + findReportById.ReportName;
                reportViewer.ShowBackButton = true;
                reportViewer.ServerReport.ReportServerUrl = new Uri(TargetReportServerURL);
                ViewBag.ReportViewer = reportViewer;

            }

            else
            {
                ModelState.AddModelError(string.Empty, "No report found.");
                return View();
            }

            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.Roles = roleList();

            return View();           
        }        

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult Create(Nikopeshe.Entity.DataModels.Report report)
        {
            ViewBag.Roles = GetUserRoleInfo(string.Empty);

            if(ModelState.IsValid)
            {
                try
                {
                    var selectedRoles = Request.Form["RoleCategory"];                    
                    if (report.RoleCategory == 0)
                    {
                        ModelState.AddModelError(string.Empty, "You must select a role.");
                        return View(report);
                    }

                    else
                    {
                        if (selectedRoles != null)
                        {
                            if (report.IsParameterized == true)
                            {
                                if (!string.IsNullOrWhiteSpace(report.ParameterName))
                                {
                                    report.ReportPath = string.Format("ReportServer/Pages/ReportViewer.aspx?%2f{0}%2f{1}&rs:Command=Render&{2}=", report.FolderName, report.ReportName, report.ParameterName);
                                }
                                else
                                {
                                    ModelState.AddModelError(string.Empty, "If this report is parameterized, the parameter name cannot be null.");
                                    return View(report);
                                }
                            }
                            else
                            {
                                report.ReportPath = string.Format("ReportServer/Pages/ReportViewer.aspx?%2f{0}%2f{1}&rs:Command=Render", report.FolderName, report.ReportName);
                            }
                            reportService.AddNewReport(report, serviceHeader);

                            string[] selectedvalues = selectedRoles.Split(',');
                            for (int i = 0; i < selectedvalues.Length; i++)
                            {
                                Nikopeshe.Entity.DataModels.ReportInRoles _report = new Nikopeshe.Entity.DataModels.ReportInRoles();                                
                                var roleCategory = int.Parse(selectedvalues[i].Trim());
                                _report.RoleCategory = roleCategory;
                                _report.ReportId = report.Id;
                                reportInRoleService.AddNewReportInRole(_report, serviceHeader);
                            }                            
                        }                        
                    }
                }

                catch(Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    ModelState.AddModelError(string.Empty, "An error has occurred while creating a report.");
                    return View(report);
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult rolereports(string id)
        {
            var returnList = new List<ReportInRoleDetails>();
            if (!string.IsNullOrWhiteSpace(id))
            {
                var findReportsInRoles = new List<Guid>();
                if (id.ToLower() == ("Administrator").ToLower())
                {
                    findReportsInRoles = reportInRoleService.GetAllReportsInCategory().Select(x => x.ReportId).Distinct().ToList();
                }
                else
                {
                    findReportsInRoles = reportInRoleService.GetAllReportsInCategory().Where(x => x.RoleDescription.ToLower() == id.ToLower()).Select(x => x.ReportId).Distinct().ToList();
                }

                if (findReportsInRoles.Any() && findReportsInRoles != null)
                {
                    foreach (var reportInRoles in findReportsInRoles)
                    {
                        var findReporInRoleById = reportService.FindReportById(reportInRoles);
                        if (findReporInRoleById != null)
                        {
                            returnList.Add(new ReportInRoleDetails
                            {
                                ReportId = findReporInRoleById.Id,
                                ReportName = findReporInRoleById.ReportName,
                                FolderName = findReporInRoleById.FolderName,
                                IsEnabled = findReporInRoleById.IsEnabled,
                            });
                        }                        
                    }
                }
            }

            var getRolesInList = returnList;

            return View(getRolesInList);
        }

        [HttpGet]
        public ActionResult Update(Guid Id)
        {
            var findReportById = reportService.FindReportById(Id);
            //var rolesInfo = GetUserRoleInfo(findReportById.);
            return View();
        }

        [HttpPost]
        [ActionName("Update")]
        public ActionResult Update(Nikopeshe.Entity.DataModels.Report report)
        {
            return View();
        }       

        private ReportParameter[] GetParametersServer()
        {
            //ReportParameter p1 = new ReportParameter("ShowBingMaps", "Visible");
            ReportParameter p2 = new ReportParameter("ShowAll", "True");
            return new ReportParameter[] { p2 };
        }        
    }
}
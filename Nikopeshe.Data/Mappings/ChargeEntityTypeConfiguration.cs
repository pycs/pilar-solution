﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Data.Mappings
{
    public class ChargeEntityTypeConfiguration : EntityTypeConfiguration<Charge>
    {
        public ChargeEntityTypeConfiguration()
        {
            this.HasKey(x => x.Id);
            this.ToTable(string.Format("{0}Charges", DefaultSettings.Instance.TablePrefix));
        }
    }
}

﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.Disbursement;
using Quartz;
using System;
using System.Configuration;
using System.Messaging;

namespace Pilar.Notifier.Configuration
{
    public class NotifierQueuingJob : IJob
    {
        private readonly ILoanProcessing loanprocessing;
        private string notifierqueuepath;        
        private readonly IMessageQueueService _messageQueueService;
        private DataContext dataContext = new DataContext();

        public NotifierQueuingJob()
        {
            loanprocessing = new LoanProcessing();
            _messageQueueService = new MessageQueueService();
            notifierqueuepath = ConfigurationManager.AppSettings["NotifierQueuePath"];            
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var dueloans = loanprocessing.FindLoanRequestDueGivenDays();

                if (dueloans != null)
                {
                    foreach (var dueloan in dueloans)
                    {
                        _messageQueueService.Send(notifierqueuepath, dueloan.Id, Enumerations.MessageCategory.EmailAlert, MessagePriority.Normal);
                    }
                }
            }

            catch(Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("NotifierQueuingJob.Execute => ", ex);
            }
        }
    }
}

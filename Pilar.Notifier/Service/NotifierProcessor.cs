﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data.Mappings;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.CustomerService;
using Pilar.Services.Services.Disbursement;
using Pilar.Services.Services.TextAlerts;
using System;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Messaging;
using System.Text;

namespace Pilar.Notifier.Service
{
    [Export(typeof(IPlugin))]
    public class NotifierProcessor : IPlugin
    {
        private string notifierqueuepath;
        private readonly ILoanProcessing _iloanprocessing;
        private readonly IEmailAlertService emailAlertService;
        private readonly ICustomerService _customerService;
        private ITextAlertService textAlertService;
        private MessageQueue _messageQueue;
        private string paybillNumber = string.Empty;
        private string TextSignature = ConfigurationManager.AppSettings["TextSignature"].ToString();
        [ImportingConstructor]
        public NotifierProcessor()
        {
            notifierqueuepath = ConfigurationManager.AppSettings["NotifierQueuePath"];
            paybillNumber = ConfigurationManager.AppSettings["PaybillNumber"];
            textAlertService = new TextAlertService();
            _iloanprocessing = new LoanProcessing();
            emailAlertService = new EmailAlertService();
            _customerService = new CustomerService();
        }
        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{851FDC83-FCEE-481D-B0EF-C01F358F8BD3}"); }
        }

        public string Description
        {
            get { return "NOTIFIER PROCESSOR"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                if (!MessageQueue.Exists(notifierqueuepath))
                    throw new ArgumentException("Queue does not exist at specified path", "_queuePath");
                else
                {
                    // initialize message queue
                    _messageQueue = new MessageQueue(notifierqueuepath);

                    // enable the AppSpecific field in the messages
                    _messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                    // set the formatter to binary.
                    _messageQueue.Formatter = new BinaryMessageFormatter();

                    // set an event to listen for new messages.
                    _messageQueue.ReceiveCompleted += MessageQueue_ReceiveCompleted;

                    // start listening
                    _messageQueue.BeginReceive();
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        public void Exit()
        {
            _messageQueue.Close();
        }

        #region Event Handlers
        private void MessageQueue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                // retrieve record id
                var recordId = (Guid)e.Message.Body;
                decimal dueInstallmentAmount = 0m;
                decimal InstallmentAmount = 0m;
                // retrieve message category
                var messageCategory = (Enumerations.MessageCategory)e.Message.AppSpecific;
                var loanrequest = _iloanprocessing.FindLoanRequestById(recordId);

                if (loanrequest != null)
                {
                    InstallmentAmount = (loanrequest.LoanAmount + loanrequest.InterestAmount) / loanrequest.NegotiatedInstallments;
                    var findCustomerByMobileNumber = _customerService.FindCustomerByMobileNumber(loanrequest.PrimaryAccountNumber);
                    string paybill = loanrequest.CustomerAccount.Customer.Branch.C2BNumber;
                    if (findCustomerByMobileNumber != null)
                    {
                        decimal loanBalance = _customerService.CustomerLoanBalance(findCustomerByMobileNumber.Id);

                        if (loanBalance >= InstallmentAmount)
                        {
                            dueInstallmentAmount = InstallmentAmount;
                        }
                        else
                        {
                            dueInstallmentAmount = loanBalance;
                        }
                        //Create email alert and insert in the database
                        StringBuilder textbodyBuilder = new StringBuilder();
                        textbodyBuilder.Append(string.Format("Dear {0}, ", loanrequest.CustomerAccount.Customer.FirstName));
                        if (loanrequest.NextPaymentDate.Value.Date == DateTime.Today.Date)
                        {
                            textbodyBuilder.Append(string.Format("your loan repayment is due today. Please pay KSH{0} to PayBill {1}. Pay on time to increase your loan limit.", dueInstallmentAmount, paybill));
                        }

                        else
                        {
                            textbodyBuilder.Append(string.Format("your loan repayment is due {0}. Please pay KSH{1} to PayBill {2}. Pay on time to increase your loan limit.", loanrequest.NextPaymentDate.Value.ToString("dd/MM/yyyy"), dueInstallmentAmount, paybill));
                        }
                        
                        textbodyBuilder.Append(TextSignature);
                        var textalert = new TextAlert
                        {
                            TextMessageRecipient = string.Format("{0}", loanrequest.CustomerAccount.Customer.MobileNumber),
                            TextMessageBody = string.Format("{0}", textbodyBuilder),
                            MaskedTextMessageBody = "",
                            TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                            TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                            TextMessageSecurityCritical = false,
                            CreatedBy = "",
                            TextMessageSendRetry = 3
                        };

                        textAlertService.CreateNewTextAlert(textalert); 
                    }
                }
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->Process->ReceiveCompleted FAILED...ReceiveCompletedEventArgs={1}" + ex + Description + e);
            }

            finally
            {
                // listen for the next message.
                _messageQueue.BeginReceive();
            }   
        }
        #endregion
        #endregion
    }
}

﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Data.Mappings;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.Charge;
using Pilar.Services.Services.CustomerService;
using Pilar.Services.Services.IPNService;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Messaging;
using System.Net;
using System.Text;
using Pilar.Services.Services.GraduatedScale;

namespace Pilar.IPN.Confirmation.Services
{
    [Export(typeof(IPlugin))]
    public class IPNConfirmationAndProcessing : IPlugin
    {
        private MessageQueue _messageQueue;
        private IIPNService _iipnservice;
        private IChargeService chargeService;
        private ICustomerService _customerservice;        
        private readonly string _queuePath;
        private IGraduatedScaleService graduatedScaleService;
        string CompanyId = ConfigurationManager.AppSettings["CompanyId"].ToString();
        private string TextSignature = ConfigurationManager.AppSettings["TextSignature"].ToString();
        string spsusername = ConfigurationManager.AppSettings["SPSUsername"].ToString();
        string spsapiendpoint = ConfigurationManager.AppSettings["SPSAPIEndPoint"].ToString();        
        string spspassword = ConfigurationManager.AppSettings["SPSPassword"].ToString();
        string recipientEmailAddress = ConfigurationManager.AppSettings["NotificationsEmailAddress"].ToString();
        string emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
        string failedC2BEmailAddress = ConfigurationManager.AppSettings["C2BFailedApprovalEmailAddress"].ToString();
        string failedC2BEmailAddressCC = ConfigurationManager.AppSettings["C2BFailedApprovalEmailAddressCC"].ToString();
        
        private decimal totalloanamount = 0m;

        [ImportingConstructor]
        public IPNConfirmationAndProcessing()
        {
            _iipnservice = new IPNService();
            _customerservice = new CustomerService();
            chargeService = new ChargeService();
            graduatedScaleService = new GraduatedScaleService();
            _queuePath = ConfigurationManager.AppSettings["C2BQueuePath"];
        }
        
        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{78DF89F5-96DA-4381-97E5-3AA14A7C11AB}"); }
        }

        public string Description
        {
            get { return "C2B TRANSACTION CONFIRMATION"; }
        }

                
        public void DoWork(params string[] args)
        {
            try
            {
                if (!MessageQueue.Exists(_queuePath))
                    throw new ArgumentException("Queue does not exist at specified path", "_queuePath");
                else
                {
                    // initialize message queue
                    _messageQueue = new MessageQueue(_queuePath, QueueAccessMode.Receive);

                    // initialize message queue
                    _messageQueue = new MessageQueue(_queuePath);

                    // enable the AppSpecific field in the messages
                    _messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                    // set the formatter to binary.
                    _messageQueue.Formatter = new BinaryMessageFormatter();

                    // set an event to listen for new messages.
                    _messageQueue.ReceiveCompleted += MessageQueue_ReceiveCompleted;
                   
                    // start listening
                    _messageQueue.BeginReceive();
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        public void Exit()
        {
            _messageQueue.ReceiveCompleted -= MessageQueue_ReceiveCompleted;

            _messageQueue.Close();
        }

        #endregion

        #region Event Handlers
        private void MessageQueue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                var recordId = (Guid)e.Message.Body;

#if DEBUG
                LoggerFactory.CreateLog().LogInfo(recordId.ToString());
#endif
                // retrieve message category
                var messageCategory = (Enumerations.MessageCategory)e.Message.AppSpecific;

                switch (messageCategory)
                {
                    case Enumerations.MessageCategory.C2BConfirmation:

                        #region c2b notification
                        
                            using(var datacontext = new DataContext())
                            {
                                var c2bnotification = datacontext.C2BTransaction.Find(recordId);
#if DEBUG
                                LoggerFactory.CreateLog().LogInfo(c2bnotification.StatusDesc + "record Id - " + c2bnotification.Id);
#endif
                                if (c2bnotification != null)
                                {
                                    using (var transaction = datacontext.Database.BeginTransaction())
                                    {
                                        try
                                        {
                                            #region Approve C2B Notification
                                            if (c2bnotification.Status == (int)Enumerations.C2BTransactionStatus.Pending || c2bnotification.Status == (int)Enumerations.C2BTransactionStatus.Queued)
                                            {
                                                try
                                                {
                                                    HttpWebRequest webRequest = WebRequest.Create(spsapiendpoint + c2bnotification.TransactionId) as HttpWebRequest;
                                                    if (webRequest == null)
                                                        throw new NullReferenceException("request is not a http request");

                                                    webRequest.Method = "GET";
                                                    webRequest.ContentType = "application/json";

                                                    webRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(spsusername + ":" + spspassword)));
                                                    HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;
#if DEBUG
                                                    LoggerFactory.CreateLog().LogInfo(webResponse.StatusDescription);
#endif
                                                    if (webResponse.StatusCode == HttpStatusCode.OK)
                                                    {
                                                        if (c2bnotification.Status == (int)Enumerations.C2BTransactionStatus.Queued || c2bnotification.Status == (int)Enumerations.C2BTransactionStatus.Pending)
                                                        {
                                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Approved;
                                                            c2bnotification.Result = "Approval Successful";
                                                            var entity = datacontext.Entry(c2bnotification);
                                                            entity.State = System.Data.Entity.EntityState.Modified;
#if DEBUG
                                                            LoggerFactory.CreateLog().LogInfo("End " + webResponse.StatusCode.ToString());
#endif
                                                        }
                                                    }
                                                }

                                                catch (WebException ex)
                                                {
                                                    LoggerFactory.CreateLog().LogInfo("[Approval] - MessageQueue_ReceiveCompleted.C2BTransactionConfirmation", ex.Message + ex.InnerException + ex.StackTrace);
                                                }
                                            }
                                            #endregion


                                            else if (c2bnotification.Status == (int)Enumerations.C2BTransactionStatus.Approved || c2bnotification.Status == (int)Enumerations.C2BTransactionStatus.PostQueued)
                                            {
                                                //find m-pesa repayment gl account id 
                                                var findmpesarepaymentaccountid = datacontext.SystemGeneralLedgerAccountMapping.Where(x => x.SystemGeneralLedgerAccountCode == Enumerations.SystemGeneralLedgerAccountCode.MPesaRepaymentAccount).FirstOrDefault();
                                                var findmpessuspenseaccountid = datacontext.SystemGeneralLedgerAccountMapping.Where(x => x.SystemGeneralLedgerAccountCode == Enumerations.SystemGeneralLedgerAccountCode.MPesaSuspsnseAccount).FirstOrDefault();
                                                var finddefaultsavingaccountid = datacontext.SavingsProduct.Where(x => x.IsDefault == true).FirstOrDefault();
                                                var findLoanTransactionHistoryByC2BId = _iipnservice.FindLoanTransactionHistoryByC2BTransactionId(c2bnotification.Id);
                                                var findStaticSetting = datacontext.StaticSetting.Where(x => x.Key == "CREDITSAVINGSACCOUNT").FirstOrDefault();
#if DEBUG
                                                LoggerFactory.CreateLog().LogInfo("C2B Id " + c2bnotification.Id.ToString());
                                                LoggerFactory.CreateLog().LogInfo("M-Pesa Suspense GL Account " + findmpessuspenseaccountid.ToString());
                                                LoggerFactory.CreateLog().LogInfo("M-Pesa Repayment GL " + findmpesarepaymentaccountid.ToString());
                                                LoggerFactory.CreateLog().LogInfo("Default Savings Account Id " + finddefaultsavingaccountid.ToString());
                                                LoggerFactory.CreateLog().LogInfo("Static Settings " + findStaticSetting.Value);
                                                LoggerFactory.CreateLog().LogInfo("C2B Id from Loan Transaction Table" + findLoanTransactionHistoryByC2BId.Id.ToString());
#endif
                                                if (findLoanTransactionHistoryByC2BId == null)
                                                {
                                                    try
                                                    {
                                                        #region Posting
                                                        bool IsLoanMigrated = false;
                                                        decimal expectedinterestamount = 0m;
                                                        decimal principalamount = 0m;
                                                        decimal receivedInterestAmount = 0m;
                                                        decimal expectedRecurringCharges = 0m;
                                                        decimal receivedRecurringCharges = 0m;
                                                        decimal principalPerInstallment = 0m;
                                                        decimal totalRecurringCharges = 0m;
                                                        decimal TransactionAmount = 0m;
                                                        decimal findCustomerSavingsAccountBalance = 0m;
                                                        //find customer using mobile number
                                                        var findcustomerbymobilenumber = _customerservice.FindCustomerByMobileNumber(c2bnotification.MSISDN);

                                                        if (findcustomerbymobilenumber != null)
                                                        {
                                                            var findcustomersavingsaccountid = datacontext.CustomersAccount.Where(x => x.CustomerId == findcustomerbymobilenumber.Id && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
#if DEBUG
                                                            LoggerFactory.CreateLog().LogInfo(findcustomersavingsaccountid.ToString());
#endif
                                                            if (findcustomerbymobilenumber != null && findcustomersavingsaccountid != null)
                                                            {
                                                                //find customer accounts by customer id
                                                                var findcustomersaccountbyid = datacontext.CustomersAccount.Where(x => x.CustomerId == findcustomerbymobilenumber.Id);

                                                                //find active loan
                                                                var findcustomeractiveloanbycustomeraccountid = (from a in datacontext.Customer
                                                                                                                 join b in datacontext.CustomersAccount
                                                                                                                     on a.Id equals b.CustomerId
                                                                                                                 join c in datacontext.LoanRequest on b.Id equals c.CustomerAccountId
                                                                                                                 where c.Status == (int)Enumerations.LoanStatus.Active && a.Id == findcustomerbymobilenumber.Id
                                                                                                                 select c).FirstOrDefault();


                                                                if (findcustomeractiveloanbycustomeraccountid != null)
                                                                {
                                                                    IsLoanMigrated = findcustomeractiveloanbycustomeraccountid.IsMigrated;
                                                                    var PaidPrincipalInterestCharges = _iipnservice.PaidPrincipalPaidInterestPaidCharges(findcustomeractiveloanbycustomeraccountid.Id, findcustomersavingsaccountid.Id, findcustomeractiveloanbycustomeraccountid.LoanProductId);
                                                                    var ExpectedTotalPrincipalInterestCharges = _iipnservice.ExpectedTotalPrincipalExpectedTotalInterestExpectedTotalCharges(findcustomeractiveloanbycustomeraccountid.Id);
                                                                    findCustomerSavingsAccountBalance = _iipnservice.CustomerSavingsAccountBalance(findcustomersavingsaccountid.Id, finddefaultsavingaccountid.ChartofAccountId);
                                                                    var findloanproductbyid = datacontext.LoanProduct.Find(findcustomeractiveloanbycustomeraccountid.LoanProductId);
                                                                    var interesrratepermonth = findloanproductbyid.InterestValue / 12;
                                                                    TransactionAmount = c2bnotification.TransAmount + findCustomerSavingsAccountBalance;
                                                                    if (IsLoanMigrated == true)
                                                                    {
                                                                        expectedinterestamount = 0m;
                                                                        principalamount = c2bnotification.TransAmount;
                                                                        totalloanamount = findcustomeractiveloanbycustomeraccountid.OutstandingLoanBalance;
                                                                    }
                                                                    else
                                                                    {
                                                                        #region Dr M-Pesa Repayment GL, Cr Savings GL
                                                                        LoanTransactionHistory loantransactionhistory = new LoanTransactionHistory()
                                                                        {
                                                                            LoanRequestId = findcustomeractiveloanbycustomeraccountid.Id,
                                                                            TransactionType = Enumerations.TransactionType.Debit,
                                                                            RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                            TransactionAmount = c2bnotification.TransAmount,
                                                                            TotalTransactionAmount = c2bnotification.TransAmount,
                                                                            C2BTransactionId = c2bnotification.Id,
                                                                            ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                            ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                            CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                            Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                                            CreatedBy = "System",
                                                                            IsApproved = true,
                                                                            ApprovedBy = "System",
                                                                            TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                            BranchId = findcustomeractiveloanbycustomeraccountid.BranchId
                                                                        };
                                                                        datacontext.LoanTransactionHistory.Add(loantransactionhistory);

                                                                        LoanTransactionHistory loantransaction = new LoanTransactionHistory()
                                                                        {
                                                                            LoanRequestId = findcustomeractiveloanbycustomeraccountid.Id,
                                                                            TransactionType = Enumerations.TransactionType.Credit,
                                                                            RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                            C2BTransactionId = c2bnotification.Id,
                                                                            TransactionAmount = c2bnotification.TransAmount,
                                                                            TotalTransactionAmount = c2bnotification.TransAmount,
                                                                            ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                            ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                            CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                            Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                                            CreatedBy = "System",
                                                                            IsApproved = true,
                                                                            ApprovedBy = "System",
                                                                            TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                            BranchId = findcustomeractiveloanbycustomeraccountid.BranchId
                                                                        };
                                                                        datacontext.LoanTransactionHistory.Add(loantransaction);
                                                                        #endregion


                                                                        if (findloanproductbyid.InterestChargeType == (int)Enumerations.InterestChargeType.Fixed)
                                                                        {
                                                                            expectedinterestamount = findloanproductbyid.InterestValue;
                                                                            principalPerInstallment = findcustomeractiveloanbycustomeraccountid.LoanAmount / findcustomeractiveloanbycustomeraccountid.NegotiatedInstallments;
                                                                        }
                                                                        else
                                                                        {
                                                                            expectedinterestamount = c2bnotification.TransAmount * (interesrratepermonth / 100) * findcustomeractiveloanbycustomeraccountid.NegotiatedInstallments / 12;
                                                                            principalPerInstallment = findcustomeractiveloanbycustomeraccountid.LoanAmount / findcustomeractiveloanbycustomeraccountid.NegotiatedInstallments;
                                                                        }

                                                                        #region Recover Interest
                                                                        if (IsLoanMigrated == false)
                                                                        {
                                                                            if (PaidPrincipalInterestCharges.Item2 < ExpectedTotalPrincipalInterestCharges.Item2 && TransactionAmount > 0m)
                                                                            {
                                                                                if (TransactionAmount >= expectedinterestamount)
                                                                                {
                                                                                    receivedInterestAmount = expectedinterestamount;
                                                                                }

                                                                                else
                                                                                {
                                                                                    receivedInterestAmount = TransactionAmount;
                                                                                }

                                                                                TransactionAmount = TransactionAmount - receivedInterestAmount;

                                                                                LoanTransactionHistory creditincomegl = new LoanTransactionHistory()
                                                                                {
                                                                                    LoanRequestId = findcustomeractiveloanbycustomeraccountid.Id,
                                                                                    TransactionType = Enumerations.TransactionType.Debit,
                                                                                    RepaymentType = loantransactionhistory.RepaymentType,
                                                                                    TransactionAmount = receivedInterestAmount,
                                                                                    C2BTransactionId = c2bnotification.Id,
                                                                                    TotalTransactionAmount = loantransactionhistory.TransactionAmount,
                                                                                    ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                                    ContraAccountId = Guid.Parse(findcustomeractiveloanbycustomeraccountid.LoanProduct.InterestGLAccountId.ToString()),
                                                                                    CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                                    Description = loantransactionhistory.Description,
                                                                                    CreatedBy = "System",
                                                                                    IsApproved = true,
                                                                                    ApprovedBy = "System",
                                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                                    BranchId = findcustomeractiveloanbycustomeraccountid.BranchId
                                                                                };

                                                                                LoanTransactionHistory debitincomegl = new LoanTransactionHistory()
                                                                                {
                                                                                    LoanRequestId = findcustomeractiveloanbycustomeraccountid.Id,
                                                                                    TransactionType = Enumerations.TransactionType.Credit,
                                                                                    RepaymentType = loantransactionhistory.RepaymentType,
                                                                                    TransactionAmount = receivedInterestAmount,
                                                                                    C2BTransactionId = c2bnotification.Id,
                                                                                    TotalTransactionAmount = loantransactionhistory.TransactionAmount,
                                                                                    ChartofAccountId = findcustomeractiveloanbycustomeraccountid.LoanProduct.InterestGLAccountId,
                                                                                    ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                                    CustomersAccountId = findcustomeractiveloanbycustomeraccountid.CustomerAccountId,
                                                                                    Description = loantransactionhistory.Description,
                                                                                    CreatedBy = "System",
                                                                                    IsApproved = true,
                                                                                    ApprovedBy = "System",
                                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                                    BranchId = findcustomeractiveloanbycustomeraccountid.BranchId
                                                                                };

                                                                                datacontext.LoanTransactionHistory.Add(debitincomegl);
                                                                                datacontext.LoanTransactionHistory.Add(creditincomegl);
                                                                            }
                                                                        }
                                                                        #endregion

                                                                        #region Recover Recurring Charges
                                                                        if (PaidPrincipalInterestCharges.Item3 < ExpectedTotalPrincipalInterestCharges.Item3)
                                                                        {
                                                                            var findProductRecurringCharges = _iipnservice.GetRecurringLoanProductChargeByProductId(findcustomeractiveloanbycustomeraccountid.LoanProductId);
                                                                            if (findProductRecurringCharges.Count != 0 && findProductRecurringCharges.Any())
                                                                            {
                                                                                foreach (var recurringCharge in findProductRecurringCharges)
                                                                                {
                                                                                    var findRecurringCharges = chargeService.FindChargeById(recurringCharge.ChargeId);
                                                                                    if (findProductRecurringCharges != null)
                                                                                    {
                                                                                        var findGraduatedScaleByChargeId = graduatedScaleService.FindGraduatedScaleUsingRangeAndChargeId(c2bnotification.TransAmount, findRecurringCharges.Id);
                                                                                        if (findGraduatedScaleByChargeId != null)
                                                                                        {
                                                                                            foreach (var graduatedScale in findGraduatedScaleByChargeId)
                                                                                            {
                                                                                                var chargeType = graduatedScale.ChargeType;
                                                                                                if (chargeType == Enumerations.ChargeTypes.FixedAmount)
                                                                                                {
                                                                                                    expectedRecurringCharges = graduatedScale.Value;
                                                                                                    totalRecurringCharges = graduatedScale.Value * findcustomeractiveloanbycustomeraccountid.NegotiatedInstallments;
                                                                                                }

                                                                                                else
                                                                                                {
                                                                                                    expectedRecurringCharges = (graduatedScale.Value * c2bnotification.TransAmount) / 100;
                                                                                                    totalRecurringCharges = ((graduatedScale.Value * c2bnotification.TransAmount) / 100) * findcustomeractiveloanbycustomeraccountid.NegotiatedInstallments;
                                                                                                }

                                                                                                if (TransactionAmount >= expectedRecurringCharges)
                                                                                                {
                                                                                                    receivedRecurringCharges = expectedRecurringCharges;
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    receivedRecurringCharges = TransactionAmount;
                                                                                                }
                                                                                                TransactionAmount = TransactionAmount - receivedRecurringCharges;

                                                                                                if (receivedRecurringCharges > 0m && TransactionAmount > 0m)
                                                                                                {
                                                                                                    LoanTransactionHistory postChargeDr = new LoanTransactionHistory()
                                                                                                    {
                                                                                                        LoanRequestId = findcustomeractiveloanbycustomeraccountid.Id,
                                                                                                        TransactionType = Enumerations.TransactionType.Debit,
                                                                                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                                                        TransactionAmount = receivedRecurringCharges,
                                                                                                        TotalTransactionAmount = c2bnotification.TransAmount,
                                                                                                        ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                                                        ContraAccountId = graduatedScale.Charge.ChartofAccountId,
                                                                                                        CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                                                        Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                                                                        CreatedBy = "System",
                                                                                                        IsApproved = true,
                                                                                                        ApprovedBy = "System",
                                                                                                        TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                                                        BranchId = findcustomeractiveloanbycustomeraccountid.BranchId,
                                                                                                        C2BTransactionId = c2bnotification.Id
                                                                                                    };
                                                                                                    datacontext.LoanTransactionHistory.Add(postChargeDr);

                                                                                                    LoanTransactionHistory postChargeCr = new LoanTransactionHistory()
                                                                                                    {
                                                                                                        LoanRequestId = findcustomeractiveloanbycustomeraccountid.Id,
                                                                                                        TransactionType = Enumerations.TransactionType.Credit,
                                                                                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                                                        TransactionAmount = receivedRecurringCharges,
                                                                                                        TotalTransactionAmount = c2bnotification.TransAmount,
                                                                                                        ChartofAccountId = graduatedScale.Charge.ChartofAccountId,
                                                                                                        ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                                                        CustomersAccountId = findcustomeractiveloanbycustomeraccountid.CustomerAccountId,
                                                                                                        Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                                                                        CreatedBy = "System",
                                                                                                        IsApproved = true,
                                                                                                        ApprovedBy = "System",
                                                                                                        TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                                                        BranchId = findcustomeractiveloanbycustomeraccountid.BranchId,
                                                                                                        C2BTransactionId = c2bnotification.Id
                                                                                                    };
                                                                                                    datacontext.LoanTransactionHistory.Add(postChargeCr);
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        #endregion

                                                                        totalloanamount = findcustomeractiveloanbycustomeraccountid.LoanAmount + findcustomeractiveloanbycustomeraccountid.InterestAmount + totalRecurringCharges;
                                                                    }

                                                                    #region Recover Expected Principal Amount
                                                                    if (PaidPrincipalInterestCharges.Item1 < ExpectedTotalPrincipalInterestCharges.Item1)
                                                                    {
                                                                        if (TransactionAmount < principalPerInstallment)
                                                                        {
                                                                            principalPerInstallment = TransactionAmount;
                                                                        }

                                                                        if (principalPerInstallment > 0)
                                                                        {
                                                                            LoanTransactionHistory debitsavingsgl = new LoanTransactionHistory()
                                                                            {
                                                                                LoanRequestId = findcustomeractiveloanbycustomeraccountid.Id,
                                                                                TransactionType = Enumerations.TransactionType.Debit,
                                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                                TransactionAmount = principalPerInstallment,
                                                                                C2BTransactionId = c2bnotification.Id,
                                                                                TotalTransactionAmount = c2bnotification.TransAmount,
                                                                                ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                                ContraAccountId = Guid.Parse(findloanproductbyid.LoanGLAccountId.ToString()),
                                                                                CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                                Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                                                CreatedBy = "System",
                                                                                IsApproved = true,
                                                                                ApprovedBy = "System",
                                                                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                                BranchId = findcustomeractiveloanbycustomeraccountid.BranchId
                                                                            };
                                                                            datacontext.LoanTransactionHistory.Add(debitsavingsgl);

                                                                            LoanTransactionHistory creditloangl = new LoanTransactionHistory()
                                                                            {
                                                                                LoanRequestId = findcustomeractiveloanbycustomeraccountid.Id,
                                                                                TransactionType = Enumerations.TransactionType.Credit,
                                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                                TransactionAmount = principalPerInstallment,
                                                                                C2BTransactionId = c2bnotification.Id,
                                                                                TotalTransactionAmount = c2bnotification.TransAmount,
                                                                                ChartofAccountId = findloanproductbyid.LoanGLAccountId,
                                                                                ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                                CustomersAccountId = findcustomeractiveloanbycustomeraccountid.CustomerAccountId,
                                                                                Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                                                CreatedBy = "System",
                                                                                IsApproved = true,
                                                                                ApprovedBy = "System",
                                                                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                                BranchId = findcustomeractiveloanbycustomeraccountid.BranchId
                                                                            };
                                                                            datacontext.LoanTransactionHistory.Add(creditloangl);
                                                                        }
                                                                    }
                                                                    #endregion
                                                                    #endregion
                                                                    var frequency = findloanproductbyid.PaymentFrequencyType;
                                                                    decimal expectedPrincipal = ExpectedTotalPrincipalInterestCharges.Item1 / findcustomeractiveloanbycustomeraccountid.NegotiatedInstallments;
                                                                    decimal expectedInstallmentAmount = expectedPrincipal + expectedRecurringCharges + expectedinterestamount;
                                                                    decimal receivedInstallmentAmount = principalPerInstallment + receivedRecurringCharges + receivedInterestAmount;
                                                                    if (receivedInstallmentAmount >= expectedInstallmentAmount)
                                                                    {
                                                                        if (frequency == Enumerations.PaymentFrequency.Weekly)
                                                                        {
                                                                            findcustomeractiveloanbycustomeraccountid.NextPaymentDate = findcustomeractiveloanbycustomeraccountid.NextPaymentDate.Value.AddDays(7);
                                                                        }
                                                                        else
                                                                        {
                                                                            findcustomeractiveloanbycustomeraccountid.NextPaymentDate = findcustomeractiveloanbycustomeraccountid.NextPaymentDate.Value.AddMonths(1);
                                                                        }
                                                                    }

                                                                    var updateloan = datacontext.Entry(findcustomeractiveloanbycustomeraccountid);
                                                                    updateloan.State = EntityState.Modified;

                                                                    c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                                                    var entry = datacontext.Entry(c2bnotification);
                                                                    entry.State = EntityState.Modified;
                                                                    //datacontext.SaveChanges();                                                        


                                                                    #region Check if loan has been paid in full
                                                                    var findloantransactionsbyloanrequestid = new List<LoanTransactionHistory>();

                                                                    if (findcustomeractiveloanbycustomeraccountid.IsMigrated == true)
                                                                    {
                                                                        findloantransactionsbyloanrequestid = datacontext.LoanTransactionHistory.Where(x => x.LoanRequestId == findcustomeractiveloanbycustomeraccountid.Id && x.IsApproved == true
                                                                        && x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment &&
                                                                        x.TransactionType == Enumerations.TransactionType.Debit && x.CustomersAccountId == findcustomersavingsaccountid.Id
                                                                        && x.CustomersAccount.AccountType == (int)Enumerations.ProductCode.Savings).ToList();
                                                                    }

                                                                    else
                                                                    {
                                                                        findloantransactionsbyloanrequestid = datacontext.LoanTransactionHistory.Where(x => x.LoanRequestId == findcustomeractiveloanbycustomeraccountid.Id
                                                                        && x.IsApproved == true && x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment
                                                                        && x.TransactionType == Enumerations.TransactionType.Credit && x.CustomersAccountId == findcustomeractiveloanbycustomeraccountid.CustomerAccountId && x.ChartofAccountId == findloanproductbyid.LoanGLAccountId).ToList();
                                                                    }

                                                                    //var totalpricipalamount = findloantransactionsbyloanrequestid.Sum(x => x.TransactionAmount);
                                                                    var totalpaidamount = PaidPrincipalInterestCharges.Item1 + receivedRecurringCharges + receivedInterestAmount + principalPerInstallment + PaidPrincipalInterestCharges.Item2 + PaidPrincipalInterestCharges.Item3;
                                                                    var totalbalance = (totalloanamount) - totalpaidamount;

                                                                    if (totalpaidamount >= totalloanamount && totalbalance <= 0)
                                                                    {
                                                                        findcustomeractiveloanbycustomeraccountid.Status = (int)Enumerations.LoanStatus.Cleared;
                                                                        var updateloanstatus = datacontext.Entry(findcustomeractiveloanbycustomeraccountid);
                                                                        updateloanstatus.State = EntityState.Modified;
                                                                    }
                                                                    #endregion

                                                                    StringBuilder bodyBuilder = new StringBuilder();
                                                                    StringBuilder textBodyBuilder = new StringBuilder();
                                                                    bodyBuilder.Append(string.Format("Dear {0}", findcustomerbymobilenumber.FullName));
                                                                    bodyBuilder.AppendLine("<br />");
                                                                    textBodyBuilder.Append(string.Format("Dear {0} ", findcustomerbymobilenumber.FirstName));
                                                                    string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                                                                    bodyBuilder.AppendLine("<br />");
                                                                    string borrowloanlink = "<a href='" + mvcclientendposint + "'/Loanrequest/newloanrequest> click here</a>";
                                                                    if (totalpaidamount >= totalloanamount && totalbalance <= 0)
                                                                    {
                                                                        bodyBuilder.Append(string.Format("Your loan repayment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                                        bodyBuilder.AppendLine("<br />");
                                                                        bodyBuilder.AppendLine("<br />");
                                                                        bodyBuilder.AppendLine(string.Format("Your loan has been cleared. To apply for a new loan {0}", borrowloanlink));
                                                                        bodyBuilder.AppendLine("<br />");
                                                                        bodyBuilder.AppendLine("<br />");
                                                                        textBodyBuilder.Append(string.Format("Your loan repayment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                                        textBodyBuilder.AppendLine(string.Format("The loan has been cleared."));
                                                                    }
                                                                    else
                                                                    {
                                                                        bodyBuilder.Append(string.Format("Your loan repayment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                                        //bodyBuilder.AppendLine("<br />");
                                                                        //bodyBuilder.AppendLine("<br />");
                                                                        //bodyBuilder.AppendLine(string.Format("Your outstanding loan balance is KES{0}", totalbalance));                                                            
                                                                        bodyBuilder.AppendLine("<br />");
                                                                        bodyBuilder.AppendLine("<br />");
                                                                        textBodyBuilder.Append(string.Format("Your loan repayment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                                        //textBodyBuilder.AppendLine(string.Format("Balance: KES{0}", totalbalance));                                                            
                                                                    }
                                                                    bodyBuilder.AppendLine("Thank you for being our valued client.");
                                                                    bodyBuilder.AppendLine("<br />");
                                                                    bodyBuilder.AppendLine("<br />");
                                                                    textBodyBuilder.Append(TextSignature);
                                                                    textBodyBuilder.Append(" Thank you.");
                                                                    bodyBuilder.Append(emailSignature);

                                                                    var emailAlertDTO = new EmailAlert
                                                                    {
                                                                        MailMessageFrom = DefaultSettings.Instance.Email,
                                                                        MailMessageTo = string.Format("{0}", findcustomerbymobilenumber.EmailAddress),
                                                                        MailMessageSubject = string.Format("Loan Repayment - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                                                        MailMessageBody = string.Format("{0}", bodyBuilder),
                                                                        MailMessageIsBodyHtml = true,
                                                                        MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                        MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                        MailMessageSecurityCritical = false,
                                                                        CreatedBy = "System",
                                                                        MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                                                    };
                                                                    datacontext.EmailAlert.Add(emailAlertDTO);
                                                                    //datacontext.SaveChanges();

                                                                    var textalert = new TextAlert
                                                                    {
                                                                        TextMessageRecipient = string.Format("{0}", findcustomerbymobilenumber.MobileNumber),
                                                                        TextMessageBody = string.Format("{0}", textBodyBuilder),
                                                                        MaskedTextMessageBody = "",
                                                                        TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                        TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                        TextMessageSecurityCritical = false,
                                                                        CreatedBy = "",
                                                                        TextMessageSendRetry = 3
                                                                    };
                                                                    datacontext.TextAlert.Add(textalert);
                                                                    
                                                                    c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                                                    var entity = datacontext.Entry(c2bnotification);
                                                                    entity.State = System.Data.Entity.EntityState.Modified;
                                                                }

                                                                else if (findcustomeractiveloanbycustomeraccountid == null && findStaticSetting.Value == "1")
                                                                {
                                                                    #region Credit Customer Savings Account and mark C2B as suspense
                                                                    var debitMpesaRepaymentGL = new LoanTransactionHistory
                                                                    {
                                                                        TransactionType = Enumerations.TransactionType.Debit,
                                                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                        TransactionAmount = c2bnotification.TransAmount,
                                                                        TotalTransactionAmount = c2bnotification.TransAmount,
                                                                        C2BTransactionId = c2bnotification.Id,
                                                                        ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                        ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                        CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                        Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                                        CreatedBy = "System",
                                                                        IsApproved = true,
                                                                        ApprovedBy = "System",
                                                                        TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                                        BranchId = findcustomerbymobilenumber.BranchId
                                                                    };
                                                                    datacontext.LoanTransactionHistory.Add(debitMpesaRepaymentGL);

                                                                    var creditCustomerSavingAccount = new LoanTransactionHistory
                                                                    {
                                                                        TransactionType = Enumerations.TransactionType.Credit,
                                                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                        C2BTransactionId = c2bnotification.Id,
                                                                        TransactionAmount = c2bnotification.TransAmount,
                                                                        TotalTransactionAmount = c2bnotification.TransAmount,
                                                                        ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                        ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                        CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                        Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                                        CreatedBy = "System",
                                                                        IsApproved = true,
                                                                        ApprovedBy = "System",
                                                                        TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                                        BranchId = findcustomerbymobilenumber.BranchId
                                                                    };
                                                                    datacontext.LoanTransactionHistory.Add(creditCustomerSavingAccount);

                                                                    c2bnotification.Result = string.Format("Could not match active loan with provided mobile number {0}", c2bnotification.MSISDN);
                                                                    c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Suspense;
                                                                    var entry = datacontext.Entry(c2bnotification);
                                                                    entry.State = EntityState.Modified;
                                                                    
                                                                    #endregion
                                                                }

                                                                else
                                                                {
                                                                    #region Mark C2B notification as unknown
                                                                    c2bnotification.Result = string.Format("Could not match active loan with provided mobile number {0}", c2bnotification.MSISDN);
                                                                    c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Unknown;
                                                                    var entry = datacontext.Entry(c2bnotification);
                                                                    entry.State = EntityState.Modified;
                                                                    
                                                                    #endregion
                                                                }

                                                            }

                                                            else
                                                            {
                                                                #region mark C2B transaction as unknown

                                                                //POST to M-Pesa Suspense account
                                                                var debitMpesaRepaymentGL = new LoanTransactionHistory
                                                                {
                                                                    TransactionType = Enumerations.TransactionType.Debit,
                                                                    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                    TransactionAmount = c2bnotification.TransAmount,
                                                                    TotalTransactionAmount = c2bnotification.TransAmount,
                                                                    C2BTransactionId = c2bnotification.Id,
                                                                    ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                    ContraAccountId = findmpessuspenseaccountid.ChartofAccountId,
                                                                    Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                                    CreatedBy = "System",
                                                                    IsApproved = true,
                                                                    ApprovedBy = "System",
                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit
                                                                };
                                                                datacontext.LoanTransactionHistory.Add(debitMpesaRepaymentGL);

                                                                var creditMPesaSuspenseAccount = new LoanTransactionHistory
                                                                {
                                                                    TransactionType = Enumerations.TransactionType.Credit,
                                                                    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                    C2BTransactionId = c2bnotification.Id,
                                                                    TransactionAmount = c2bnotification.TransAmount,
                                                                    TotalTransactionAmount = c2bnotification.TransAmount,
                                                                    ChartofAccountId = findmpessuspenseaccountid.ChartofAccountId,
                                                                    ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                    Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                                    CreatedBy = "System",
                                                                    IsApproved = true,
                                                                    ApprovedBy = "System",
                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit
                                                                };
                                                                datacontext.LoanTransactionHistory.Add(creditMPesaSuspenseAccount);


                                                                c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Suspense;
                                                                c2bnotification.Result = string.Format("Could not find customer with provided mobile number {0}", c2bnotification.MSISDN);
                                                                var entry = datacontext.Entry(c2bnotification);
                                                                entry.State = EntityState.Modified;
                                                                
                                                                #endregion
                                                            }
                                                        }

                                                        else
                                                        {
                                                            #region mark C2B transaction as unknown

                                                            //POST to M-Pesa Suspense account
                                                            var debitMpesaRepaymentGL = new LoanTransactionHistory
                                                            {
                                                                TransactionType = Enumerations.TransactionType.Debit,
                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                TransactionAmount = c2bnotification.TransAmount,
                                                                TotalTransactionAmount = c2bnotification.TransAmount,
                                                                C2BTransactionId = c2bnotification.Id,
                                                                ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                ContraAccountId = findmpessuspenseaccountid.ChartofAccountId,
                                                                Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit
                                                            };
                                                            datacontext.LoanTransactionHistory.Add(debitMpesaRepaymentGL);

                                                            var creditMPesaSuspenseAccount = new LoanTransactionHistory
                                                            {
                                                                TransactionType = Enumerations.TransactionType.Credit,
                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                C2BTransactionId = c2bnotification.Id,
                                                                TransactionAmount = c2bnotification.TransAmount,
                                                                TotalTransactionAmount = c2bnotification.TransAmount,
                                                                ChartofAccountId = findmpessuspenseaccountid.ChartofAccountId,
                                                                ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit
                                                            };
                                                            datacontext.LoanTransactionHistory.Add(creditMPesaSuspenseAccount);

                                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Suspense;
                                                            c2bnotification.Result = string.Format("Could not find customer with provided mobile number {0}", c2bnotification.MSISDN);
                                                            var entry = datacontext.Entry(c2bnotification);
                                                            entry.State = EntityState.Modified;                                                           
                                                            #endregion
                                                        }
                        #endregion
                                                    }

                                                    #region Catch Exception
                                                    catch (Exception ex)
                                                    {                                                        
                                                        LoggerFactory.CreateLog().LogInfo("[Processing - ]" + ex.Message + ex.InnerException + ex.StackTrace);
                                                    }
                                                    #endregion
                                                }

                                                else
                                                {
                                                    c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Duplicate;
                                                    _iipnservice.UpdateC2BTransaction(c2bnotification);
                                                }

                                            }

                                            datacontext.SaveChanges();
                                            transaction.Commit();
                                            //transaction.Dispose();
                                            //datacontext.Database.Connection.Dispose();
                                        }

                                        catch (Exception ex)
                                        {
                                            LoggerFactory.CreateLog().LogInfo("[Processing - ]" + ex.Message + ex.InnerException + ex.StackTrace);
                                            transaction.Rollback();
                                            //transaction.Dispose();
                                            //datacontext.Database.Connection.Dispose();
                                        }
                                    }
                                }
                            }                        

                        #endregion

                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("[Catch - Processing - ]{0}->Process->ReceiveCompleted FAILED...ReceiveCompletedEventArgs={1}" + ex.Message + Description + ex.InnerException + ex.StackTrace, e);
            }

            finally
            {
                // listen for the next message.
                _messageQueue.BeginReceive();
            }
        }             
    }
}

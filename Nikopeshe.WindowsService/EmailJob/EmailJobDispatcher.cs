﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.WindowsService.EmailJob
{
    public class EmailJobDispatcher
    {
        string ErrorMessage = string.Empty;

        DataContext datacontext = new DataContext();

        public void SendEmail(string from, string to, string subject, string mailbody, bool IsBodyHtml)
        {
            try
            {
                MailMessage mail = new MailMessage();

                var smpt = ConfigurationManager.AppSettings["smtpserver"];

                var port = ConfigurationManager.AppSettings["port"];

                var username = ConfigurationManager.AppSettings["Username"];

                var password = ConfigurationManager.AppSettings["password"];

                var enablessl = ConfigurationManager.AppSettings["enablessl"];

                SmtpClient SmtpServer = new SmtpClient(smpt);

                mail.From = new MailAddress(from);
                mail.To.Add(to);
                mail.Subject = subject;
                mail.IsBodyHtml = IsBodyHtml;
                mail.Body = mailbody;
                mail.Subject = subject.Replace('\r', ' ').Replace('\n', ' ');

                SmtpServer.Port = int.Parse(port);
                SmtpServer.Credentials = new System.Net.NetworkCredential(username, password);
                SmtpServer.EnableSsl = bool.Parse(enablessl);

                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }

        public void UpdateEmailAlert(Guid Id, int status)
        {
            try
            {
                var findemailalertbyid = datacontext.EmailAlert.Find(Id);               
                var entry = datacontext.Entry(findemailalertbyid);
                entry.State = EntityState.Modified;
                findemailalertbyid.MailMessageDLRStatus = status;                
                datacontext.SaveChanges();
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }

                Logger.CreateLog(ErrorMessage + ex.Message + ex.InnerException + ex.StackTrace);                
            }    
        }
    }
}

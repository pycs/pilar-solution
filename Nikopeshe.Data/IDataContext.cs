﻿using Nikopeshe.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Data
{
    public interface IDataContext
    {
        IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity;
        int SaveChanges(ServiceHeader serviceHeader);
        int SaveChanges();
        void Dispose();
    }
}

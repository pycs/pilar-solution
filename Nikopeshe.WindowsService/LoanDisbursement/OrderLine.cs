﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.WindowsService.LoanDisbursement
{
    public class OrderLine
    {
        public string TypeDesc { get; set; }

        public string Payee { get; set; }

        public string PrimaryAccountNumber { get; set; }

        public decimal Amount { get; set; }

        public string BankCode { get; set; }

        public int MCCMNC { get; set; }

        public string MCCMNCDesc { get; set; }

        public string Reference { get; set; }

        public string SystemTraceAuditNumber { get; set; }

        public int Status { get; set; }

        public string StatusDesc { get; set; }

        public string B2MResponseCode { get; set; }

        public string B2MResponseDesc { get; set; }

        public string B2MResultCode { get; set; }

        public string B2MResultDesc { get; set; }
        
        public string B2MTransactionID { get; set; }
        
        public string TransactionDateTime { get; set; }
        
        public decimal WorkingAccountAvailableFunds { get; set; }
        
        public decimal UtilityAccountAvailableFunds { get; set; }
        
        public decimal ChargePaidAccountAvailableFunds { get; set; }
        
        public string TransactionCreditParty { get; set; }
    }
}

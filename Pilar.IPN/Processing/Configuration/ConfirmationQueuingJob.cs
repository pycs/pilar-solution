﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.IPNService;
using Quartz;
using System;
using System.Configuration;
using System.Linq;
using System.Messaging;

namespace Pilar.IPN.Processing.Configuration
{
    public class ConfirmationQueuingJob : IJob
    {
        private readonly IMessageQueueService _messageQueueService;
        private readonly IIPNService _iipnservice;        
        private readonly string _messageRelayQueuePath;

        public ConfirmationQueuingJob()
        {
            _messageQueueService = new MessageQueueService();
            _iipnservice = new IPNService();
            _messageRelayQueuePath = ConfigurationManager.AppSettings["C2BQueuePath"];
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                //find approved c2b transactions
                var findapprovedc2b = _iipnservice.FindC2BTransactionByStatus((int)Enumerations.C2BTransactionStatus.Approved);

                if(findapprovedc2b != null && findapprovedc2b.Any())
                    foreach(var item in findapprovedc2b)
                    {
                        _messageQueueService.Send(_messageRelayQueuePath, item.Id, Enumerations.MessageCategory.C2BConfirmation, MessagePriority.Normal);
                        item.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                        _iipnservice.UpdateC2BTransaction(item);
                    }
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }
    }
}

﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize(Roles = "Administrator")]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class RelationshipManagerController : Controller
    {
        private readonly IRelationshipManagerService relationshipManagerService;
        private readonly IBranchService branchService;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public RelationshipManagerController(IRelationshipManagerService _relationshipManagerService, IBranchService _branchService)
        {
            this.relationshipManagerService = _relationshipManagerService;
            this.branchService = _branchService;
        }
        // GET: RelationshipManager
        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {            
            return View();
        }

        public JsonResult FindAllRelationshipManagers(JQueryDataTablesModel datatableModel)
        {
            var findAllRms = relationshipManagerService.GetAllRelationshipManagers();
            int totalRecordCount = 0;
            int totalSearchCount = 0;
            var RmList = GetAllRelationshipManagerInList(startIndex: datatableModel.iDisplayStart, pageSize: datatableModel.iDisplayLength,
                sortedColumns: datatableModel.GetSortedColumns(), totalRecordCount: out totalRecordCount, searchRecordCount: out totalSearchCount, searchString: datatableModel.sSearch,
                RMList: findAllRms);


            var results = Json(new JQueryDataTablesResponse<RelationshipManager>(items: RmList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: totalSearchCount,
                sEcho: datatableModel.sEcho));

            return results;
        }

        public static IList<RelationshipManager> GetAllRelationshipManagerInList(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<RelationshipManager> RMList)
        {
            var GetRMList = RMList;

            totalRecordCount = GetRMList.Count;

            if (string.IsNullOrWhiteSpace(searchString))
            {
                GetRMList = GetRMList.Where(c => c.FirstName.ToLower().Contains(searchString.ToLower()) ||
                    c.LastName.ToLower().Contains(searchString.ToLower()) || 
                    c.IDNumber.ToLower().Contains(searchString.ToLower()) ||
                    c.MobileNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }
                searchRecordCount = GetRMList.Count;

                IOrderedEnumerable<RelationshipManager> sortedRMs = null;

                foreach (var sortedColumn in sortedColumns)
                {
                    switch (sortedColumn.PropertyName)
                    {
                        case "CreatedDate":
                            sortedRMs = sortedRMs == null ? GetRMList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedRMs.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                            break;

                        case "FirstName":
                            sortedRMs = sortedRMs == null ? GetRMList.CustomSort(sortedColumn.Direction, cust => cust.FirstName)
                            : sortedRMs.CustomSort(sortedColumn.Direction, cust => cust.FirstName);
                            break;

                        case "LastName":
                            sortedRMs = sortedRMs == null ? GetRMList.CustomSort(sortedColumn.Direction, cust => cust.LastName)
                                : sortedRMs.CustomSort(sortedColumn.Direction, cust => cust.LastName);
                            break;

                        case"IDNumber":
                            sortedRMs = sortedRMs == null ? GetRMList.CustomSort(sortedColumn.Direction, cust => cust.IDNumber)
                                : sortedRMs.CustomSort(sortedColumn.Direction, cust => cust.IDNumber);
                            break;

                        case "MobileNumber":
                            sortedRMs = sortedRMs == null ? GetRMList.CustomSort(sortedColumn.Direction, cust => cust.MobileNumber)
                                : sortedRMs.CustomSort(sortedColumn.Direction, cust => cust.MobileNumber);
                            break;

                        case "EmailAddress":
                            sortedRMs = sortedRMs == null ? GetRMList.CustomSort(sortedColumn.Direction, cust => cust.EmailAddress)
                                : sortedRMs.CustomSort(sortedColumn.Direction, cust => cust.EmailAddress);
                            break;

                        case "Branch.BranchName":
                            sortedRMs = sortedRMs == null ? GetRMList.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName)
                                : sortedRMs.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName);
                            break;

                        case "IsEnabled":
                            sortedRMs = sortedRMs == null ? GetRMList.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled)
                                : sortedRMs.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled);
                            break;
                    }
                }
                return sortedRMs.Skip(startIndex).Take(pageSize).ToList();
        }

        // GET: RelationshipManager/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: RelationshipManager/Create
        [Authorize(Roles="Administrator")]
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.branchedSelectList = GetBranchSelectList(Guid.Empty.ToString());

            return View();
        }

        // POST: RelationshipManager/Create
        [HttpPost]        
        public ActionResult Create(RelationshipManager relationshipmanager)
        {
            ViewBag.branchedSelectList = GetBranchSelectList(Guid.Empty.ToString());
            try
            {
                if (ModelState.IsValid)
                {
                    if (relationshipmanager.BranchId == Guid.Empty)
                    {
                        ModelState.AddModelError(string.Empty, "Branch cannot be null.");
                        return View(relationshipmanager);
                    }

                    relationshipManagerService.AddNewRelationshipManager(relationshipmanager, serviceHeader);
                }

                else
                {
                    return View(relationshipmanager);
                }

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {                
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View();
            }
        }

        // GET: RelationshipManager/Edit/5
        public ActionResult Edit(Guid id)
        {
            ViewBag.branchedSelectList = GetBranchSelectList(Guid.Empty.ToString());
            var findrelationshipManagerById = relationshipManagerService.FindRelationshipManagerById(id);

            if (findrelationshipManagerById != null)
                return View(findrelationshipManagerById);

            return View();
        }

        // POST: RelationshipManager/Edit/5
        [HttpPost]
        public ActionResult Edit(RelationshipManager relationshipManager)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (relationshipManager.BranchId == Guid.Empty)
                    {
                        ModelState.AddModelError(string.Empty, "Branch cannot be null");
                        return View(relationshipManager);
                    }

                    var findRMById = relationshipManagerService.FindRelationshipManagerById(relationshipManager.Id);
                    if(findRMById == null)
                    {
                        ModelState.AddModelError(string.Empty, "An expected error has occurred");
                        return View(relationshipManager);
                    }

                    findRMById.BranchId = relationshipManager.BranchId;
                    findRMById.EmailAddress = relationshipManager.EmailAddress;
                    findRMById.FirstName = relationshipManager.FirstName;
                    findRMById.IDNumber = relationshipManager.IDNumber;
                    findRMById.IsEnabled = relationshipManager.IsEnabled;
                    findRMById.LastName = relationshipManager.LastName;
                    findRMById.MobileNumber = relationshipManager.MobileNumber;
                    
                    //Update the record
                    relationshipManagerService.UpdateRelationshipManager(findRMById, serviceHeader);
                }
                else
                {
                    return View();
                }
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ViewBag.branchedSelectList = GetBranchSelectList(Guid.Empty.ToString());
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View();
            }
        }

        // GET: RelationshipManager/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: RelationshipManager/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [NonAction]
        public IList<SelectListItem> GetBranchSelectList(string selectedValue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultBranch = new SelectListItem { Selected = (selectedValue == Guid.Empty.ToString("D")), Text = "Select Branch", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultBranch);

            var otherBranchesList = branchService.GetAllBranches().Where(x => x.IsEnabled == true);

            foreach (var item in otherBranchesList)
            {
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedValue)), Text = item.BranchName, Value = item.Id.ToString("D") });
            }

            return SelectList;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Extensions
{
    public static class EnumerationHelper
    {
        private static object _syncObj = new object();

        static Dictionary<Enum, string> _enumDescriptionCache = new Dictionary<Enum, string>();

        public static string GetDescription(Enum value)
        {
            if (value == null) return string.Empty;

            lock (_syncObj)
            {
                if (!_enumDescriptionCache.ContainsKey(value))
                {
                    var description = (from m in value.GetType().GetMember(value.ToString())
                                       let attr = (DescriptionAttribute)m.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault()
                                       select attr == null ? value.ToString() : attr.Description).FirstOrDefault();

                    _enumDescriptionCache.Add(value, description);
                }
            }

            return _enumDescriptionCache[value];
        }
    }
}
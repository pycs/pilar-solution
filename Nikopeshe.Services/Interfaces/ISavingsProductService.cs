﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data;

namespace Nikopeshe.Services.Interfaces
{
    public interface ISavingsProductService
    {
        void AddNewSavingsProduct(SavingsProduct savingsproduct, ServiceHeader serviceHeader);

        void UpdateSavingProduct(SavingsProduct savingsproduct, ServiceHeader serviceHeader);

        SavingsProduct FindSavingsProductById(Guid Id);

        SavingsProduct FindSavingsChartofAccountsId(Guid chartofaccountid);

        IList<SavingsProduct> GetAllSavingsAccountProducts(IsolationLevel isolationLevel);

        SavingsProduct FindDefaultSavingsProduct();

        IList<SavingsProduct> DefaultSavingsProductCount();

        string NextSavingProductCode();        
    }
}

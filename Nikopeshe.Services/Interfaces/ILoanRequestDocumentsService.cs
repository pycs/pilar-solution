﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.InterfaceImplementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface ILoanRequestDocumentsService
    {
        void AddNewLoanRequestDocuments(LoanRequestDocument loanrequestdocuments, ServiceHeader serviceHeader);

        LoanRequestDocument FindLoanRequestDocumentById(Guid Id);

        LoanRequestDocument FindLoanRequestDocumentByLoanRequestIdAndDocumentType(Guid LoanRequestId, Enumerations.LoanRequestDocumentTypes doctype);

        IList<LoanRequestDocument> GetAllLoanRequestDocuments();

        IList<Nikopeshe.Services.InterfaceImplementations.LoanRequestDocumentsService.LoanRequestDocumentView> GetAllLoanRequestDocumentsByLoanRequestId(Guid LoanRequestId);

        bool VerifyLoanRequestDocument(Guid LoanRequestId);
    }
}

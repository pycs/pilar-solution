[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Nikopeshe.WebApplication.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Nikopeshe.WebApplication.App_Start.NinjectWebCommon), "Stop")]

namespace Nikopeshe.WebApplication.App_Start
{
    using Crosscutting.NetFramework.Logging;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Nikopeshe.Data;
    using Nikopeshe.Services;
    using Nikopeshe.Services.InterfaceImplementations;
    using Nikopeshe.Services.Interfaces;
    using Ninject;
    using Ninject.Web.Common;
    using System;
    using System.Web;
    using System.Web.Http;    

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                // Install our Ninject-based IDependencyResolver into the Web API config
                GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            ConfigureFactories();

            kernel.Bind<IDataContext>().To<DataContext>();
            kernel.Bind(typeof(IRepository<>)).To(typeof(Repository<>));
            kernel.Bind<ICustomerService>().To<CustomerService>();
            kernel.Bind<ILoanProductService>().To<LoanProductService>();
            kernel.Bind<ILoanRequestService>().To<LoanRequestService>();
            kernel.Bind<ILoanTransactionHistoryService>().To<LoanTransactionHistoryService>();
            kernel.Bind<ITransactionHistoryService>().To<TransactionHistoryService>();
            kernel.Bind<IChartofAccountService>().To<ChartofAccountService>();
            kernel.Bind<IEmailAlertService>().To<EmailAlertService>();
            kernel.Bind<ITextAlertService>().To<TextAlertService>();
            kernel.Bind<IUserInfoService>().To<UserInfoService>();
            kernel.Bind<IDashBoardService>().To<DashBoardService>();
            kernel.Bind<ICustomerAccountService>().To<CustomerAccountService>();
            kernel.Bind<ISavingsProductService>().To<SavingsProductService>();
            kernel.Bind<ISystemGeneralLedgerAccountMappingService>().To<SystemGeneralLedgerAccountMappingService>();
            kernel.Bind<IC2BTransactionService>().To<C2BTransactionService>();
            kernel.Bind<IDomainAuditTrail>().To<DomainAuditTrailService>();
            kernel.Bind<ICustomerDocumentService>().To<CustomerDocumentService>();
            kernel.Bind<ILoanRequestDocumentsService>().To<LoanRequestDocumentsService>();
            kernel.Bind<IBranchService>().To<BranchService>();
            kernel.Bind<IChargesService>().To<ChargesService>();
            kernel.Bind<IGraduatedScaleService>().To<GraduatedScaleService>();
            kernel.Bind<ILoanProductsChargeService>().To<LoanProductsChargeService>();
            kernel.Bind<ILoanRequestChargeService>().To<LoanRequestChargeService>();
            kernel.Bind<IStaticSettingService>().To<StaticSettingService>();
            kernel.Bind<IPasswordRecoveryRequestService>().To<PasswordRecoveryRequestService>();
            kernel.Bind<IReportService>().To<ReportService>();
            kernel.Bind<IReportInRoleService>().To<ReportInRoleService>();
            kernel.Bind<IRelationshipManagerService>().To<RelationshipManagerService>();
            kernel.Bind<IReconciliationService>().To<ReconciliationService>();
        }

        static void ConfigureFactories()
        {
            LoggerFactory.SetCurrent(new EntLibLogFactory());            
        }
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class ChargesService : IChargesService
    {
        private IRepository<Charge> chargerepository;

        private string ErrorMessage = string.Empty;

        public ChargesService(IRepository<Charge> _chargerepository)
        {
            if (_chargerepository == null)
                throw new ArgumentNullException("repository cannot be null.");

                this.chargerepository = _chargerepository;
        }

        public void AddNewCharge(Charge charge, ServiceHeader serviceHeader)
        {
            if (charge == null)
                throw new ArgumentNullException("entity cannot be null");

            try 
            {                                
                chargerepository.Insert(charge, serviceHeader);
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public void UpdateCharge(Charge charge, ServiceHeader serviceHeader)
        {
            if (charge == null)
                throw new ArgumentNullException("entity cannot be null");

            try
            {
                chargerepository.Update(charge, serviceHeader);
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public Charge FindChargeById(Guid Id)
        {
            var findchargebyid = chargerepository.GetById(Id);

            return findchargebyid;
        }

        public IList<Charge> GetAllCharges()
        {
            var getallcharges = chargerepository.GetAll(IsolationLevel.ReadUncommitted);

            return getallcharges.ToList();
        }
    }
}

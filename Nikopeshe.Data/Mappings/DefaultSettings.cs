﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Data.Mappings
{
    public sealed class DefaultSettings
    {
        private static readonly object _lockObj = new object();

        private DefaultSettings() { }

        private static DefaultSettings instance;
        public static DefaultSettings Instance
        {
            get
            {
                lock (_lockObj)
                {
                    if (instance == null)
                        instance = new DefaultSettings();

                    /*
                     * Only vendor should know this :-)
                     * 
                     * paolo izmoto 09.01.2012 
                     * 
                     * todo: find a better way of handling this...
                     */
                    instance.RootUser = "su";
                    instance.RootPassword = "H@rd2Cr@k!";
                    instance.AuditUser = "auditor";
                    instance.AuditPassword = "Ch@11enge";
                    instance.Password = "yeknod!";
                    instance.PasswordQuestion = "Where were you when you first heard about 9/11?";
                    instance.PasswordAnswer = "School";
                    instance.PasswordExpiryPeriod = 14;
                    instance.Email = "application@pycs.co.ke";
                    instance.FirstName = "Super";
                    instance.LastName = "User";
                    instance.TablePrefix = "pilar_";
                    instance.ApplicationName = "/PilarApplication";
                    instance.ApplicationDisplayName = "Fast Credit";

                    instance.ApplicationCopyright = string.Format("Copyright © 2012-2016, Pi Consulting Services® and/or its affiliates.{0}All rights reserved.", Environment.NewLine);
                }

                return instance;
            }
        }

        public string RootUser { get; set; }

        public string RootPassword { get; private set; }

        public string AuditUser { get; private set; }

        public string AuditPassword { get; private set; }

        public string Password { get; private set; }

        public string PasswordQuestion { get; private set; }

        public string PasswordAnswer { get; private set; }

        public int PasswordExpiryPeriod { get; private set; }

        public string Email { get; private set; }

        public string ApplicationName { get; private set; }

        public string ApplicationDisplayName { get; private set; }

        public string ApplicationCopyright { get; private set; }

        public string TablePrefix { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }
    }
}

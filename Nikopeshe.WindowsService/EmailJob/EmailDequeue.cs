﻿using Nikopeshe.Entity.DataModels;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.WindowsService.EmailJob
{
    public class EmailDequeue : IJob
    {
        public void Execute(IJobExecutionContext context)
        {            
            var queuename = ConfigurationManager.AppSettings["MessageRelayQueuePath"].ToString();
            // Connect to the a queue on the local computer.
            MessageQueue myQueue = new MessageQueue(queuename);

            EmailJobDispatcher maildispatcher = new EmailJobDispatcher();

            // Set the formatter to indicate body contains an Order.
            myQueue.Formatter = new XmlMessageFormatter(new Type[] { typeof(EmailAlert) });

            try
            {
                // Receive and format the message. 
                Message myMessage = myQueue.Receive();
                EmailAlert emails = (EmailAlert)myMessage.Body;
                maildispatcher.SendEmail(emails.MailMessageFrom, emails.MailMessageTo, emails.MailMessageSubject, emails.MailMessageBody, emails.MailMessageIsBodyHtml);
                maildispatcher.UpdateEmailAlert(emails.Id, (int)Enumerations.DLRStatus.Delivered);
            }

            catch (MessageQueueException ex)
            {
                // Handle Message Queuing exceptions.
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }

            // Handle invalid serialization format.
            catch (InvalidOperationException ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }

            catch (Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }
    }
}

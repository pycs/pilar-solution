﻿using System.ComponentModel.DataAnnotations;

namespace Nikopeshe.Entity.DataModels
{
    public class PasswordRecoveryRequest : BaseEntity
    {        
        [Display(Name="Mobile Number")]
        [Required]
        public string UserName { get; set; }

        public bool IsVerified { get; set; }
    }
}

﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Content.PM;

namespace Pilar.AndroidApplication
{
	[Activity (Label = "Login" , Icon = "@drawable/BCPhoneicon",Theme = "@style/AppBaseTheme")]					
	public class LoginActivity : Activity
	{
		EditText password;
		EditText Username;
		Button LoginButton;
		const string UserId = "bumpcash";
		const string Userpassword = "bumpcash";
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.Login);
			LoginButton = FindViewById<Button> (Resource.Id.login);
			Username = FindViewById<EditText> (Resource.Id.username);
			password = FindViewById<EditText> (Resource.Id.password);
			LoginButton.Click += (sender, e) => { 
				//checking whether values are null
				if(Username.Text.Length == 0)
				{
					Toast.MakeText (this, "Username Required.", ToastLength.Long).Show ();
				}
				else if (password.Text.Length == 0)
				{
					Toast.MakeText (this, "Password Required.", ToastLength.Long).Show ();
				}
				else{
					if(Username.Text.ToString() == UserId && password.Text.ToString() == Userpassword)
					{
                        //var sendmoney = new Intent (this, typeof(SendMoney));
                        //StartActivity (sendmoney);
					}
					else{
						Toast.MakeText (this, "Wrong Username or Password. Try again", ToastLength.Short).Show ();
					}
				}
			};
			var forgotpassword = FindViewById<TextView> (Resource.Id.forgotpassword);
            //forgotpassword.Click += (sender, e) => {           
            //    AlertDialog.Builder alert = new AlertDialog.Builder (this);
            //    alert.SetView(LayoutInflater.Inflate(Resource.Layout.input_dialog, null));
            //    var EmailReset = FindViewById<EditText>(Resource.Id.emailreset);
            //    alert.SetTitle("Reset Password");
            //    alert.SetPositiveButton("Reset Password", (senderAlert, args) =>
            //        Toast.MakeText (this, "An email with instructions has been sent", ToastLength.Long).Show ());
            //    alert.SetNegativeButton ("Cancel", (senderAlert, args) => Toast.MakeText (this, "Cancelled", ToastLength.Long).Show ());
            //    alert.Create().Show();
            //};

		}

	}
}


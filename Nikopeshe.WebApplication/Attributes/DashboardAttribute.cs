﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;


namespace Nikopeshe.WebApplication.Attributes
{
    public class DashboardAttribute : FilterAttribute
    {
        private DataContext datacontext = new DataContext();

        public IList<LoanRequest> GetAllPendingLoanRequest()
        {
            var getpendingloanrequest = datacontext.LoanRequest.Where(x => x.Status == (int)Enumerations.LoanStatus.Pending);

            return getpendingloanrequest.ToList();
        }

        public IList<LoanRequest> GetPaymentDueToday()
        {
            var getpaymentdueyoday = datacontext.LoanRequest.Where(x => x.NextPaymentDate == DateTime.Today);

            return getpaymentdueyoday.ToList();
        }

        public IList<LoanRequest> GetActiveLoanRequest()
        {
            var getactiveloanrequest = datacontext.LoanRequest.Where(x => x.Status == (int)Enumerations.LoanStatus.Active);

            return getactiveloanrequest.ToList();
        }        
    }
}
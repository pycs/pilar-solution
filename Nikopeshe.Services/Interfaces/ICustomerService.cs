﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface ICustomerService
    {
        /// <summary>
        /// creating new customer
        /// </summary>
        /// <param name="customer"></param>
        void AddNewCustomer(Customer customer, ServiceHeader serviceHeader);


        bool AddNewCustomerTest(Customer customer, ServiceHeader serviceHeader);
        /// <summary>
        /// Get List of all customers available
        /// </summary>
        /// <returns></returns>
        IList<Customer> GetAllCustomers(IsolationLevel isolationLevel);

        IList<Customer> GetCustomersByBranchId(Guid BranchId, IsolationLevel isolationLevel);
        /// <summary>
        /// Get customer by mobile number
        /// </summary>
        /// <returns></returns>
        IList<Customer> GetCustomerByMobileNumber(string mobilenumber);

        /// <summary>
        /// update customer
        /// </summary>
        /// <param name="customer"></param>
        void UpdateCustomer(Customer customer, ServiceHeader serviceHeader);

        /// <summary>
        /// Delete customer
        /// </summary>
        /// <param name="customer"></param>
        void DeleteCustomer(Customer customer, ServiceHeader serviceHeader);

        /// <summary>
        /// Find customer using Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Customer FindCustomerById(Guid Id);

        /// <summary>
        /// Check using mobile number or Id Number if the customer already exist
        /// </summary>
        /// <param name="MobileNumber"></param>
        /// <param name="IdNumber"></param>
        /// <returns></returns>
        bool CheckDuplicateCustomer(string MobileNumber, string IdNumber);

        /// <summary>
        /// Find customer by mobile number
        /// </summary>
        /// <param name="MobileNumber"></param>
        /// <returns></returns>
        Customer FindCustomerByMobileNumber(string MobileNumber);

        /// <summary>
        /// Find Customer by Id Number
        /// </summary>
        /// <param name="IdNumber"></param>
        /// <returns></returns>
        Customer FindCustomerByIdNumber(string IdNumber);       

        /// <summary>
        /// Validating the customer age
        /// </summary>
        /// <param name="dateofbirth"></param>
        /// <returns></returns>
        bool ValidateCustomerAge(DateTime dateofbirth);

        /// <summary>
        /// Validating the customer age
        /// </summary>
        /// <param name="groupRole"></param>
        /// <returns></returns>
        bool ValidateGroupRole(Enumerations.GroupRoles groupRole);

        /// <summary>
        /// find what mode of notificationt the customer is enable for.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        int GetCustomerEnabledNotificationMode(Guid CustomerId);

        /// <summary>
        /// Find Customer by Approval code
        /// </summary>
        /// <param name="ApprovalCode"></param>
        /// <returns></returns>
        Customer FindCustomerByApprovalCode(string ApprovalCode);

        /// <summary>
        /// Calculate the next customer number;
        /// </summary>
        /// <returns></returns>
        string NextCustomerNumber();

        /// <summary>
        /// Next Customer savings account number
        /// </summary>
        /// <param name="customernumber"></param>
        /// <returns></returns>
        string NextCustomerSavingAccountNumber(string customernumber);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CustomerNumber"></param>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        string NextCustomerLoanAccountNumber(string CustomerNumber, Guid CustomerId, string loanproductcode);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CustomerNumber"></param>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        string NextCustomerArrearsAccountNumber(string CustomerNumber, Guid CustomerId);

        IList<string> GetDistinctCountyCodes();

        int FindCustomerCountByCountyCode(string CountyCode);

        IList<CustomerAccountStatement> CustomerAccountStatement(string customerId, DateTime startDate, DateTime endDate);

        PageCollectionInfo<Customer> FindCustomersFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        PageCollectionInfo<Customer> FindCustomersByBranchIdFilterInPage(Guid id, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);
    }
}

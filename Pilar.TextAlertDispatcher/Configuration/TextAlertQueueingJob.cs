﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.TextAlertServices;
using Quartz;
using System;
using System.Configuration;
using System.Linq;
using System.Messaging;

namespace Pilar.TextAlertDispatcher.Configuration
{
    public class TextAlertQueueingJob : IJob
    {
        private readonly IMessageQueueService _messageQueueService;        
        private readonly string _textRelayQueuePath;
        private ITextAlertService _textAlertService;        

        public TextAlertQueueingJob()
        {
            _textAlertService = new TextAlertService();
            _messageQueueService = new MessageQueueService();
            _textRelayQueuePath = ConfigurationManager.AppSettings["TextRelayQueuePath"];
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                // 1. find all pending text alerts
                var textAlertsWithDLRStatusPending = _textAlertService.FindTextAlertByDRLStatus((int)Enumerations.DLRStatus.Pending);
                
                // 2. Send all pending text alert to queue
                if(textAlertsWithDLRStatusPending != null && textAlertsWithDLRStatusPending.Any())
                    foreach(var item in textAlertsWithDLRStatusPending)
                        _messageQueueService.Send(_textRelayQueuePath, item.Id, Enumerations.MessageCategory.SMSAlert, (MessagePriority)item.TextMessagePriority);
                            
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class TextAlertService : ITextAlertService
    {
        readonly IRepository<TextAlert> textalertrepository;

        string errorMessage = string.Empty;

        public TextAlertService(IRepository<TextAlert> _textalertrepository)
        {
            if(_textalertrepository == null)
                throw new ArgumentNullException("null repository");

            this.textalertrepository = _textalertrepository;
        }

        public void AddNewTextAlert(TextAlert textalert, ServiceHeader serviceHeader)
        {            
            try
            {
                if (textalert == null)
                    throw new ArgumentNullException("entity cannot be null");

                textalertrepository.Insert(textalert, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(errorMessage, ex);
            }            
        }

        public IList<TextAlert> FindTextAlertByDRLStatus(int status)
        {
            if (string.IsNullOrWhiteSpace(status.ToString()))
                throw new ArgumentNullException("null entity");

            var results = textalertrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.TextMessageDLRStatus == status);

            return results.ToList();
        }

        public void UpdateTextAlertDRLStatus(Guid Id, int Status, ServiceHeader serviceHeader)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Id.ToString()) || string.IsNullOrWhiteSpace(Status.ToString()))

                    throw new ArgumentNullException("empty value(s)");

                var findrecordbyId = textalertrepository.GetById(Id);

                findrecordbyId.TextMessageDLRStatus = Status;

                textalertrepository.Update(findrecordbyId, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(errorMessage, ex);
            }    
        }
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data;

namespace Nikopeshe.Services.Interfaces
{
    public interface IGraduatedScaleService
    {
        void AddNewGraduatedScale(GraduatedScale graduatedscale, ServiceHeader serviceHeader);

        void UpdateGraduatedScale(GraduatedScale graduatedscale, ServiceHeader serviceHeader);

        GraduatedScale FindGraduatedScaleById(Guid Id);

        GraduatedScale FindGraduatedScaleByChargeId(Guid ChargeId);

        IList<GraduatedScale> GetAllGraduatedScales(IsolationLevel isolationLevel);

        List<GraduatedScale> FindGraduatedScaleUsingRangeAndChargeId(decimal amount, Guid ChargeId);

        IList<GraduatedScale> GetAllGraduatedScalesByChargeId(Guid ChargeId, IsolationLevel isolationLevel);

        GraduatedScale ValidateGraduatedScale(Guid ChargeId, decimal LowerLimit, decimal UpperLimit);

        IList<GraduatedScale> CountGraduatedScaleWithChargeAndRange(Guid ChargeId, decimal LowerLimit, decimal UpperLimit);
    }
}

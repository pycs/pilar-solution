﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using OfficeOpenXml;
using Pilar.Services.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Messaging;

namespace Pilar.AutoReconciliation.Services
{
    [Export(typeof(IPlugin))]
    public class Processor : IPlugin
    {
        private MessageQueue _messageQueue;        
        private readonly string _queuePath;

        private readonly ISmtpService _smtpService;

        [ImportingConstructor]
        public Processor()
        {
            _queuePath = ConfigurationManager.AppSettings["ReconQueuePath"];
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{984B68B3-5C5C-4507-AFB5-90B8ED2D3CDE}"); }
        }

        public string Description
        {
            get { return "AUTORECON.PROCESSOR"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                if (!MessageQueue.Exists(_queuePath))
                    throw new ArgumentException("Queue does not exist at specified path", "_queuePath");
                else
                {
                    // initialize message queue
                    _messageQueue = new MessageQueue(_queuePath);

                    // enable the AppSpecific field in the messages
                    _messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                    // set the formatter to binary.
                    _messageQueue.Formatter = new BinaryMessageFormatter();

                    // set an event to listen for new messages.
                    _messageQueue.ReceiveCompleted += MessageQueue_ReceiveCompleted;

                    // start listening
                    _messageQueue.BeginReceive();
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        #endregion

        #region Event Handlers

        private void MessageQueue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                // retrieve sms alert id
                var recordId = (Guid)e.Message.Body;

                // retrieve message category
                var messageCategory = (Enumerations.MessageCategory)e.Message.AppSpecific;

                switch (messageCategory)
                {
                    #region Loan Processing Fee

                    case Enumerations.MessageCategory.AutoRecon:
                        using (var dataContext = new DataContext())
                        {
                            using (var transaction = dataContext.Database.BeginTransaction())
                            {
                                try
                                {

                                    var findUprocessedFileById = dataContext.Reconciliation.Find(recordId);
                                    if (findUprocessedFileById != null && (findUprocessedFileById.FilePath != null || string.IsNullOrWhiteSpace(findUprocessedFileById.FilePath)))
                                    {
                                        if(findUprocessedFileById.FileContentType == (int)Enumerations.FileContentType.applicationvndmsexcel)
                                        {
                                            var parsingResult = ParseOrderLineItemsFileWithNPOI(findUprocessedFileById.FilePath);

                                            var reconFileItems = parsingResult.Item1.ToList();

                                            if (reconFileItems != null && reconFileItems.Any())
                                            {
                                                foreach (var fileItem in reconFileItems)
                                                {
                                                    var findC2BByTransId = dataContext.C2BTransaction.Where(x => x.TransID == fileItem.TransID).FirstOrDefault();
                                                    if (findUprocessedFileById == null)
                                                    {
                                                        dataContext.C2BTransaction.Add(fileItem);
                                                    }
                                                }
                                            }
                                            
                                        }
                                        else if (findUprocessedFileById.FileContentType == (int)Enumerations.FileContentType.applicationvndopenxmlformatsofficedocumentspreadsheetmlsheet)
                                        {
                                            var parsingResult2 = ParseOrderLineItemsFileWithEPPlus(findUprocessedFileById.FilePath);
                                            var reconFileItems2 = parsingResult2.Item1.ToList();

                                            if (reconFileItems2 != null && reconFileItems2.Any())
                                            {
                                                foreach (var fileItem in reconFileItems2)
                                                {
                                                    var findC2BByTransId = dataContext.C2BTransaction.Where(x => x.TransID == fileItem.TransID).FirstOrDefault();
                                                    if (findC2BByTransId == null)
                                                    {
                                                        dataContext.C2BTransaction.Add(fileItem);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    dataContext.SaveChanges();
                                    transaction.Commit();
                                }

                                catch (Exception ex)
                                {
                                    transaction.Rollback();
                                    LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
                                }
                            }
                        }
                        break;

                    #endregion

                    #region Auto Reconciliation

                    case Enumerations.MessageCategory.LoanProcessingFee:
                        using (var dataContext = new DataContext())
                        {
                            using (var transaction = dataContext.Database.BeginTransaction())
                            {
                                try
                                {

                                    var findUprocessedFileById = dataContext.Reconciliation.Find(recordId);
                                    if (findUprocessedFileById != null && (findUprocessedFileById.FilePath != null || string.IsNullOrWhiteSpace(findUprocessedFileById.FilePath)))
                                    {
                                        if (findUprocessedFileById.FileContentType == (int)Enumerations.FileContentType.applicationvndmsexcel)
                                        {
                                            var parsingResult = ParseOrderLineItemsFileWithNPOI(findUprocessedFileById.FilePath);

                                            var reconFileItems = parsingResult.Item1.ToList();

                                            if (reconFileItems != null && reconFileItems.Any())
                                            {
                                                foreach (var fileItem in reconFileItems)
                                                {
                                                    var findC2BByTransId = dataContext.C2BTransaction.Where(x => x.TransID == fileItem.TransID).FirstOrDefault();
                                                    if (findC2BByTransId == null)
                                                    {
                                                        dataContext.C2BTransaction.Add(fileItem);
                                                    }
                                                }
                                            }

                                        }
                                        else if (findUprocessedFileById.FileContentType == (int)Enumerations.FileContentType.applicationvndopenxmlformatsofficedocumentspreadsheetmlsheet)
                                        {
                                            var parsingResult2 = ParseOrderLineItemsFileWithEPPlus(findUprocessedFileById.FilePath);
                                            var reconFileItems2 = parsingResult2.Item1.ToList();

                                            if (reconFileItems2 != null && reconFileItems2.Any())
                                            {
                                                foreach (var fileItem in reconFileItems2)
                                                {
                                                    var findC2BByTransId = dataContext.C2BTransaction.Where(x => x.TransID == fileItem.TransID).FirstOrDefault();
                                                    if (findC2BByTransId == null)
                                                    {
                                                        dataContext.C2BTransaction.Add(fileItem);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    dataContext.SaveChanges();
                                    transaction.Commit();
                                }

                                catch (Exception ex)
                                {
                                    transaction.Rollback();
                                    LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
                                }
                            }
                        }
                        break;

                    #endregion
                }
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }

            finally
            {
                _messageQueue.BeginReceive();
            }
        }

        public void Exit()
        {
            _messageQueue.Close();
        }
        #endregion

        #region Helpers

        #region Parse Items File With NPOI
        public static Tuple<List<C2BTransaction>, List<string>> ParseOrderLineItemsFileWithNPOI(string fileName)
        {
            List<C2BTransaction> lineItems = null;

            List<string> builder = new List<string>();

            using (FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                HSSFWorkbook hssfWorkbook = new HSSFWorkbook(file);

                if (hssfWorkbook != null)
                {
                    ISheet sheet = hssfWorkbook.GetSheetAt(0);

                    if (sheet != null)
                    {
                        IEnumerator rows = sheet.GetRowEnumerator();

                        if (rows != null)
                        {
                            rows.MoveNext(); // skip header row

                            lineItems = new List<C2BTransaction>();

                            int counter = default(int);

                            while (rows.MoveNext())
                            {
                                counter += 1;

                                IRow row = (HSSFRow)rows.Current;

                                ICell cell1Value = row.GetCell(0);  // Receipt No.
                                ICell cell2Value = row.GetCell(1);  // Completion Time
                                ICell cell3Value = row.GetCell(2);  // Initiation Time
                                ICell cell4Value = row.GetCell(3);  // Details                                
                                ICell cell5Value = row.GetCell(4);  // TransactionStatus
                                ICell cell6Value = row.GetCell(5);  // PaidIn
                                ICell cell7Value = row.GetCell(6);  // Withdrawn
                                ICell cell8Value = row.GetCell(7);  //Balance
                                ICell cell9Value = row.GetCell(8);  //BalanceConfirmed
                                ICell cell10Value = row.GetCell(9);  //ReasonType
                                ICell cell11Value = row.GetCell(10); //OtherPartyInfo
                                ICell cell12Value = row.GetCell(11); //LinkedTransactionID
                                ICell cell13Value = row.GetCell(12); //ACNo

                                object col1Value = cell1Value != null ? cell1Value.ToString() : null;
                                object col2Value = cell2Value != null ? cell2Value.ToString() : null;
                                object col3Value = cell3Value != null ? cell3Value.ToString() : null;
                                object col4Value = cell4Value != null ? cell4Value.ToString() : null;
                                object col5Value = cell5Value != null ? cell5Value.ToString() : null;
                                object col6Value = cell6Value != null ? cell6Value.ToString() : null;
                                object col7Value = cell7Value != null ? cell7Value.ToString() : null;
                                object col8Value = cell8Value != null ? cell8Value.ToString() : null;
                                object col9Value = cell9Value != null ? cell9Value.ToString() : null;
                                object col10Value = cell10Value != null ? cell10Value.ToString() : null;
                                object col11Value = cell1Value != null ? cell11Value.ToString() : null;
                                object col12Value = cell2Value != null ? cell12Value.ToString() : null;
                                object col13Value = cell3Value != null ? cell13Value.ToString() : null;

                                if (col1Value != null && col2Value != null && col3Value != null && col4Value != null && col6Value != null && col13Value != null && col11Value != null)
                                {
                                    try
                                    {
                                        lineItems.Add(
                                                new C2BTransaction
                                                {
                                                    Id = Guid.NewGuid(),
                                                    KYCInfo_FirstName = col11Value.ToString(),
                                                    BusinessShortCode = "850150",
                                                    BillRefNumber = cell13Value.ToString(),
                                                    CreatedDate = DateTime.Now,
                                                    InvoiceNumber = string.Empty,
                                                    MSISDN = col11Value.ToString(),
                                                    TransAmount = decimal.Parse(col6Value.ToString()),
                                                    Result = "Manual Upload",
                                                    ThirdPartyTransID = string.Empty,
                                                    TransactionId = Guid.NewGuid(),
                                                    Status = (int)Enumerations.C2BTransactionStatus.Approved,
                                                    TransID = col1Value.ToString(),
                                                });
                                    }
                                    catch (Exception ex)
                                    {
                                        builder.Add(ex.Message);
                                    }
                                }
                                else
                                {
                                    builder.Add(string.Format("Row {0} is invalid: {1},{2},{3},{4},{5},{6}", (counter + 1), col1Value, col2Value, col3Value, col4Value, col5Value, col6Value));
                                }
                            }
                        }
                    }
                }
            }

            return new Tuple<List<C2BTransaction>, List<string>>(lineItems, builder);
        }
        #endregion

        #region Parse Items File With EPPlus
        public static Tuple<List<C2BTransaction>, List<string>> ParseOrderLineItemsFileWithEPPlus(string fileName)
        {
            List<C2BTransaction> lineItems = null;

            List<string> builder = new List<string>();

            // Get the file we are going to process
            var existingFile = new FileInfo(fileName);

            // Open and read the XlSX file.
            using (var package = new ExcelPackage(existingFile))
            {
                // Get the work book in the file
                ExcelWorkbook workBook = package.Workbook;

                if (workBook != null)
                {
                    if (workBook.Worksheets.Count > 0)
                    {
                        // Get the first worksheet
                        ExcelWorksheet currentWorksheet = workBook.Worksheets.First();

                        lineItems = new List<C2BTransaction>();

                        // read each row from the start of the data to the end of the spreadsheet.
                        for (int rowNumber = 2; rowNumber <= currentWorksheet.Dimension.End.Row; rowNumber++) // skip header row
                        {
                            object col1Value = currentWorksheet.Cells[rowNumber, 1].Value; // MPesaReceiptNo
                            object col2Value = currentWorksheet.Cells[rowNumber, 2].Value; // CompletionTime
                            object col3Value = currentWorksheet.Cells[rowNumber, 3].Value; // InitiationTime
                            object col4Value = currentWorksheet.Cells[rowNumber, 4].Value; // Details
                            object col5Value = currentWorksheet.Cells[rowNumber, 5].Value; // TransactionStatus
                            object col6Value = currentWorksheet.Cells[rowNumber, 6].Value; // PaidIn 
                            object col7Value = currentWorksheet.Cells[rowNumber, 7].Value; // Withdrawn 
                            object col8Value = currentWorksheet.Cells[rowNumber, 8].Value; // Balance  
                            object col9Value = currentWorksheet.Cells[rowNumber, 9].Value; // BalanceConfirmed  
                            object col10Value = currentWorksheet.Cells[rowNumber, 10].Value; // ReasonType  
                            object col11Value = currentWorksheet.Cells[rowNumber, 11].Value; // OtherPartyInfo  
                            object col12Value = currentWorksheet.Cells[rowNumber, 12].Value; // LinkedTransactionID  
                            object col13Value = currentWorksheet.Cells[rowNumber, 13].Value; // ACNo  

                            if (col1Value != null && col2Value != null && col3Value != null && col4Value != null && col6Value != null && col11Value != null && col13Value != null)
                            {
                                try
                                {
                                    if (col2Value.ToString().Length < 12)
                                        builder.Add(string.Format("Row {0} is invalid: {1},{2},{3},{4},{5},{6} - Mobile number has to be 12 digits.", rowNumber, col1Value, col2Value, col3Value, col4Value, col5Value, col6Value));
                                    else
                                    {                                        
                                        string mobilenumber = col11Value.ToString().Substring(0, 12);
                                        lineItems.Add(
                                                new C2BTransaction
                                                {
                                                    Id = Guid.NewGuid(),
                                                    KYCInfo_FirstName = col11Value.ToString(),
                                                    BusinessShortCode ="850150",
                                                    BillRefNumber = col13Value.ToString(),                                                    
                                                    TransAmount = decimal.Parse(col6Value.ToString()),
                                                    CreatedDate = DateTime.Now,
                                                    InvoiceNumber = string.Empty,
                                                    MSISDN = mobilenumber,
                                                    Result = "Manual Upload",
                                                    ThirdPartyTransID = string.Empty,                                                    
                                                    TransactionId = Guid.NewGuid(),
                                                    Status = (int)Enumerations.C2BTransactionStatus.Approved,
                                                    TransID = col1Value.ToString(),
                                                });
                                    }
                                }
                                catch (Exception ex)
                                {
                                    builder.Add(ex.Message);
                                }
                            }
                            else
                            {
                                builder.Add(string.Format("Row {0} is invalid: {1},{2},{3},{4},{5},{6}", rowNumber, col1Value, col2Value, col3Value, col4Value, col5Value, col6Value));
                            }
                        }
                    }
                }
            }

            return new Tuple<List<C2BTransaction>, List<string>>(lineItems, builder);
        }
        #endregion

        #endregion
    }

    public static class StringExtension
    {
        public static bool StringIsAllNumeric(this string s)
        {
            var result = default(bool);

            if (!string.IsNullOrWhiteSpace(s))
            {
                s = s.Trim();

                result = !s.ToCharArray().Any(c => Char.IsLetter(c) || Char.IsWhiteSpace(c) || Char.IsSymbol(c) || Char.IsSeparator(c) || Char.IsPunctuation(c) || Char.IsControl(c));
            }

            return result;
        }
    }

    public class ReconciliationFileFields
    {
        public string MPesaReceiptNo { get; set; }

        public string CompletionTime { get; set; }

        public string InitiationTime { get; set; }

        public string Details { get; set; }

        public string TransactionStatus { get; set;}

        public string PaidIn { get; set; }

        public string Withdrawn { get; set; }

        public string Balance { get; set; }

        public string BalanceConfirmed { get; set; }

        public string ReasonType { get; set; }

        public string OtherPartyInfo { get; set; }

        public string LinkedTransactionID { get; set; }

        public string ACNo { get; set; }
    }
}

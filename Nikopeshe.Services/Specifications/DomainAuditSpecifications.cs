﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Specifications
{
    public class DomainAuditSpecifications
    {
        public static ISpecification<DomainAudit> EFTOrderLinesByEFTOrder(Guid id, string text)
        {
            Specification<DomainAudit> specification = new DirectSpecification<DomainAudit>(c => c.Id == id);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var recordId = new DirectSpecification<DomainAudit>(c => c.RecordID.Contains(text));

                var username = new DirectSpecification<DomainAudit>(c => c.ApplicationUserName.Contains(text));

                var eventType = new DirectSpecification<DomainAudit>(c => c.EventType.Contains(text));

                specification &= (recordId | username | eventType);
            }

            return specification;
        }

        public static Specification<DomainAudit> DefaultSpec(string text)
        {
            Specification<DomainAudit> specification = new TrueSpecification<DomainAudit>();

            if (!String.IsNullOrWhiteSpace(text))
            {
                var recordId = new DirectSpecification<DomainAudit>(c => c.RecordID.Contains(text));

                var username = new DirectSpecification<DomainAudit>(c => c.ApplicationUserName.Contains(text));

                var eventType = new DirectSpecification<DomainAudit>(c => c.EventType.Contains(text));

                specification &= (recordId | username | eventType);
            }

            return specification;
        }
    }
}

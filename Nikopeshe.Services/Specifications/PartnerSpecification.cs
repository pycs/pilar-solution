﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Specifications
{
    public class PartnerSpecification
    {
        public static ISpecification<Partner> FindPartnerById(Guid id, string text)
        {
            Specification<Partner> specification = new DirectSpecification<Partner>(c => c.Id == id);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var bankAccountNumberSpec = new DirectSpecification<Partner>(c => c.AccountNumber.Contains(text));

                var bankMobileNumberSpec = new DirectSpecification<Partner>(c => c.MobileNumber.Contains(text));

                var nameSpec = new DirectSpecification<Partner>(c => c.PaybillNumber.Contains(text));

                specification &= (bankAccountNumberSpec | nameSpec);
            }

            return specification;
        }

        public static Specification<Partner> DefaultSpec(string text)
        {
            Specification<Partner> specification = new TrueSpecification<Partner>();

            if (!String.IsNullOrWhiteSpace(text))
            {
                var companyNameSpec = new DirectSpecification<Partner>(c => c.MobileNumber.Contains(text));

                var bankAccountNumberSpec = new DirectSpecification<Partner>(c => c.MobileNumber.Contains(text));

                //var remarksSpec = new DirectSpecification<Customer>(c => c.FullName.Contains(text));

                specification &= (companyNameSpec | bankAccountNumberSpec);
            }

            return specification;
        }       
    }
}

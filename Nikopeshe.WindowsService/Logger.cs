﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.WindowsService
{
    public class Logger : IDisposable
    {
        private static object lockObject = new object();

        public static void CreateLog(string errorMessage)
        {
            lock(lockObject)
            {
                string path = ConfigurationManager.AppSettings["ErrorLogPath"];
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine("Time stamp:" + DateTime.Now);
                    writer.WriteLine(string.Format(errorMessage, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                    writer.Close();
                    writer.Dispose();
                }
            }           
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }        
    }
}

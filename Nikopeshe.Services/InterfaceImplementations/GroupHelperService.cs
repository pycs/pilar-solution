﻿using Infrastructure.Crosscutting.NetFramework.Utils;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class GroupHelperService : IGroupHelperService
    {
        private IRepository<GroupHelper> grouphelperrepository;

        public GroupHelperService(IRepository<GroupHelper> _grouphelperrepository)
        {
            if (_grouphelperrepository == null)
                throw new ArgumentNullException("null repository");

            this.grouphelperrepository = _grouphelperrepository;
        }


        public IList<GroupHelper> GetAllGroupHelpers(IsolationLevel isolationLevel)
        {
            return grouphelperrepository.GetAll(isolationLevel).ToList();
        }

        public GroupHelper FindGroupHelperById(Guid Id)
        {
            if (Id == Guid.Empty)
                throw new ArgumentNullException("null entity");

            return grouphelperrepository.GetById(Id); 
        }

        public IList<GroupHelper> GetGroupHelpersByCountyCode(int countyCode)
        {
            return grouphelperrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CountyCode == countyCode).ToList();
        }

    }
}

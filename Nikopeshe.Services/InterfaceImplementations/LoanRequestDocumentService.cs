﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Entity.Helpers;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class LoanRequestDocumentsService : ILoanRequestDocumentsService
    {
        private readonly IRepository<LoanRequestDocument> loanrequestdocumentservice;
        private readonly DataContext datacontext = new DataContext();
        private readonly IRepository<LoanRequest> loanrequestservice;

        public string ErrorMessage = string.Empty;

        public LoanRequestDocumentsService(IRepository<LoanRequestDocument> _loanrequestdocumentservice, IRepository<LoanRequest> _loanrequestservice)
        {
            this.loanrequestdocumentservice = _loanrequestdocumentservice;
            this.loanrequestservice = _loanrequestservice;
        }

        public void AddNewLoanRequestDocuments(LoanRequestDocument loanrequestdocuments, ServiceHeader serviceHeader)
        {
            if (loanrequestdocuments == null)
                throw new ArgumentNullException("entity cannot be null");

            try
            {
                loanrequestdocumentservice.Insert(loanrequestdocuments, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public LoanRequestDocument FindLoanRequestDocumentById(Guid Id)
        {
            if (Id == Guid.Empty)
                throw new ArgumentNullException("guid cannot be null");

            var findloanrequestdocument = loanrequestdocumentservice.GetById(Id);

            return findloanrequestdocument;
        }

        public LoanRequestDocument FindLoanRequestDocumentByLoanRequestIdAndDocumentType(Guid LoanRequestId, Enumerations.LoanRequestDocumentTypes doctype)
        {
            if (LoanRequestId == Guid.Empty)
                throw new ArgumentNullException("guid cannot be null");

            var findloanrequestdocumentbyloanrequestidanddocumenttype = loanrequestdocumentservice.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.LoanRequestId == LoanRequestId && x.DocumentType == doctype);

            return findloanrequestdocumentbyloanrequestidanddocumenttype.FirstOrDefault();
        }

        public IList<LoanRequestDocument> GetAllLoanRequestDocuments()
        {
            var getallloanrequestdocuments = loanrequestdocumentservice.GetAll(IsolationLevel.ReadUncommitted);

            return getallloanrequestdocuments.ToList();
        }

        public IList<LoanRequestDocumentView> GetAllLoanRequestDocumentsByLoanRequestId(Guid LoanRequestId)
        {            
            var loanrequestviewlist = new List<LoanRequestDocumentView>();

            var loanrequestdocumentview = (from a in datacontext.LoanRequestDocument
                                           where a.LoanRequestId == LoanRequestId
                                           select new { a.Id, a.CreatedDate, a.LoanRequest, a.LoanRequestId, a.DocumentType }).ToList();

            foreach (var document in loanrequestdocumentview)
            {
                loanrequestviewlist.Add(new LoanRequestDocumentView
                {
                    Id = document.Id,
                    LoanRequestId = document.LoanRequestId,
                    LoanRequest = document.LoanRequest,                    
                    DocumentType = document.DocumentType,
                    CreatedDate = document.CreatedDate

                });
            }
            return loanrequestviewlist;            
        }

        public bool VerifyLoanRequestDocument(Guid LoanRequestId)
        {
            if (LoanRequestId == Guid.Empty) throw new ArgumentNullException("Loan request Id cannot be null");

            var getloanapplicationform = loanrequestdocumentservice.GetAll(IsolationLevel.ReadCommitted).Where(x => x.LoanRequestId == LoanRequestId && x.DocumentType == Enumerations.LoanRequestDocumentTypes.LoanApplicationForm);
            var getbankstatement = loanrequestdocumentservice.GetAll(IsolationLevel.ReadCommitted).Where(x => x.LoanRequestId == LoanRequestId && x.DocumentType == Enumerations.LoanRequestDocumentTypes.BankStatement);
            var getpsyslip = loanrequestdocumentservice.GetAll(IsolationLevel.ReadCommitted).Where(x => x.LoanRequestId == LoanRequestId && x.DocumentType == Enumerations.LoanRequestDocumentTypes.Payslip);

            if (getbankstatement == null || getloanapplicationform == null || getpsyslip == null)
            {
                return false;
            }

            else
            {
                return true;
            }
        }

        public class LoanRequestDocumentView
        {
            public Guid Id { get; set; }

            public Guid LoanRequestId { get; set; }

            public DateTime CreatedDate { get; set; }

            public virtual LoanRequest LoanRequest { get; set; }

            public Guid CustomerId { get; set; }

            public Enumerations.LoanRequestDocumentTypes DocumentType { get; set; }

            public string DocumentTypeDesc
            {
                get
                {
                    return Enum.IsDefined(typeof(Enumerations.LoanRequestDocumentTypes), DocumentType) ? EnumHelper.GetDescription((Enumerations.LoanRequestDocumentTypes)DocumentType) : string.Empty;
                }
            }
        }
    }
}

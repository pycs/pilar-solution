﻿using Nikopeshe.WindowsService.C2B;
using Nikopeshe.WindowsService.EmailJob;
using Nikopeshe.WindowsService.LoanDisbursement.GET;
using Nikopeshe.WindowsService.LoanDisbursement.POST;
using Nikopeshe.WindowsService.LoanProcessing;
using Nikopeshe.WindowsService.TextAlertJob;
using Quartz;
using Quartz.Impl;
using System;
using System.Configuration;

namespace Nikopeshe.WindowsService
{
    public class TaskScheduler : ITaskScheduler
    {
        private IScheduler _scheduler;

        public string EmailQueueingJobCronExpression = ConfigurationManager.AppSettings["EmailQueueingJobCronExpression"];

        public string LoanProcessingJobCronExpressionDequeue = ConfigurationManager.AppSettings["LoanProcessingJobCronExpressionDequeue"];

        public string LoanProcessingJobCronExpressionQueue = ConfigurationManager.AppSettings["LoanProcessingJobCronExpressionQueue"];

        public string SPSQueueCronExpression = ConfigurationManager.AppSettings["SPSQueueCronExpression"];

        public string SPSDequeueCronExpression = ConfigurationManager.AppSettings["SPSDequeueCronExpression"];

        public TaskScheduler()
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            _scheduler = schedulerFactory.GetScheduler();
        }
        public string Name
        {
            get { return GetType().Name; }
        }

        public void EmailQueueJobDefination()
        {
            var cronexpression = ConfigurationManager.AppSettings["EmailQueueingJobCronExpression"];
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            _scheduler = schedulerFactory.GetScheduler();

            // Define the Job to be scheduled
            var job = JobBuilder.Create<EmailQueueingJob>()
                .WithIdentity("EmailQueueingJob", "EmailQueuingGroup")
                .RequestRecovery()
                .Build();

            // Associate a trigger with the Job            
            // Trigger the job to run now, and then every 40 seconds
            ITrigger trigger = TriggerBuilder.Create()
             .WithIdentity("EmailQueueingJob", "EmailQueuingGroup")
             .WithCronSchedule(cronexpression)
                    .StartAt(DateTime.UtcNow)
                    .WithPriority(1)
                    .Build();

            // Tell quartz to schedule the job using our trigger
            _scheduler.ScheduleJob(job, trigger);
            _scheduler.Start();
        }

        public void Stop()
        {
            _scheduler.Shutdown();
        }       

        public void SPSPostJobDefination()
        {
            var cronExpression = ConfigurationManager.AppSettings["SPSPostCronExpression"];

            // define the job and tie it to our HelloJob class
            IJobDetail job = JobBuilder.Create<PostJob>()
                .WithIdentity("spsgetqueueing", "spsgetqueueing") // name "myJob", group "group1"
                .Build();

            // Trigger the job to run now, and then every 40 seconds
            ITrigger trigger = TriggerBuilder.Create()
                .StartNow()
                .WithIdentity("spsgetqueueing", "spsgetqueueing")
                .WithCronSchedule(cronExpression)
                    .StartAt(DateTime.UtcNow)
                    .WithPriority(1)
                    .Build();

            // Tell quartz to schedule the job using our trigger
            _scheduler.ScheduleJob(job, trigger);
            _scheduler.Start();
        }

        public void SPSGetJobDefination()
        {
            var cronExpression = ConfigurationManager.AppSettings["SPSGetCronExpression"];

            // define the job and tie it to our HelloJob class
            IJobDetail job = JobBuilder.Create<GetJob>()
                .WithIdentity("spsgetdequeueing", "spsgetdequeueing") // name "myJob", group "group1"
                .Build();

            // Trigger the job to run now, and then every 40 seconds
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("spsgetdequeueing", "spsgetdequeueing")
                .WithCronSchedule(cronExpression)
                    .StartAt(DateTime.UtcNow)
                    .WithPriority(1)
                    .Build();

            // Tell quartz to schedule the job using our trigger
            _scheduler.ScheduleJob(job, trigger);
            _scheduler.Start();
        }

        public void MpesaRepayment()
        {
            var cronExpression = ConfigurationManager.AppSettings["MPesaRepaymentCronExpression"];

            // define the job and tie it to our HelloJob class
            IJobDetail job = JobBuilder.Create<MPesaRepayment>()
                .WithIdentity("mpesarepaymentdequeueing", "mpesarepaymentdequeueing") // name "myJob", group "group1"
                .Build();

            // Trigger the job to run now, and then every 40 seconds
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("mpesarepaymentdequeueing", "mpesarepaymentgetdequeueing")
                .WithCronSchedule(cronExpression)
                    .StartAt(DateTime.UtcNow)
                    .WithPriority(1)
                    .Build();

            // Tell quartz to schedule the job using our trigger
            _scheduler.ScheduleJob(job, trigger);
            _scheduler.Start();
        }

        public void C2BConfirmation()
        {
            var cronExpression = ConfigurationManager.AppSettings["C2BConfirmation"];

            // define the job and tie it to our HelloJob class
            IJobDetail job = JobBuilder.Create<C2BConfirmationStatus>()
                .WithIdentity("c2bstatus", "c2bstatus") // name "myJob", group "group1"
                .Build();

            // Trigger the job to run now, and then every 40 seconds
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("c2bstatus", "c2bstatus")
                .WithCronSchedule(cronExpression)
                    .StartAt(DateTime.UtcNow)
                    .WithPriority(1)
                    .Build();

            // Tell quartz to schedule the job using our trigger
            _scheduler.ScheduleJob(job, trigger);
            _scheduler.Start();
        }

        public void LoanStatusWatchdogJob()
        {
            var cronExpression = ConfigurationManager.AppSettings["LoanWatchDogJobExpression"];

            // define the job and tie it to our HelloJob class
            IJobDetail job = JobBuilder.Create<LoanStatusWatchdog>()
                .WithIdentity("loanStatuswatchdogjob", "loanStatuswatchdogjob") // name "myJob", group "group1"
                .Build();

            // Trigger the job to run now, and then every 40 seconds
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("loanStatuswatchdogjob", "loanStatuswatchdogjob")
                .WithCronSchedule(cronExpression)
                    .StartAt(DateTime.UtcNow)
                    .WithPriority(1)
                    .Build();

            // Tell quartz to schedule the job using our trigger
            _scheduler.ScheduleJob(job, trigger);
            _scheduler.Start();
        }

        public void TextAlertJob()
        {
            var cronExpression = ConfigurationManager.AppSettings["TextAlertCronExpression"];

            // define the job and tie it to our HelloJob class
            IJobDetail job = JobBuilder.Create<SMSPostRequest>()
                .WithIdentity("textalertjob", "textalertjob") // name "myJob", group "group1"
                .Build();

            // Trigger the job to run now, and then every 40 seconds
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("textalertjob", "textalertjob")
                .WithCronSchedule(cronExpression)
                    .StartAt(DateTime.UtcNow)
                    .WithPriority(1)
                    .Build();

            // Tell quartz to schedule the job using our trigger
            _scheduler.ScheduleJob(job, trigger);
            _scheduler.Start();
        }        
    }
}

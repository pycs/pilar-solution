﻿using Nikopeshe.Services.InterfaceImplementations;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WindowsService.EmailJob;
using Nikopeshe.WindowsService.LoanDisbursement.GET;
using Nikopeshe.WindowsService.LoanDisbursement.POST;
using Ninject;
using Quartz;
using Quartz.Impl;
using System;
using System.Configuration;
using System.Reflection;
using System.ServiceProcess;

namespace Nikopeshe.WindowsService
{
    public partial class MainService : ServiceBase
    {     
        ITaskScheduler scheduler;

        public MainService()
        {            
            InitializeComponent();            
        }
 
        protected override void OnStart(string[] args)
        {
            try
            {
                var configCode = 17140;

                int.TryParse(ConfigurationManager.AppSettings["ConfigCode"], out configCode);

                if (configCode == 17140)
                {
                    SecurityHelpers.ProtectConfigurationFile();
                }
                else if (configCode == 14170)
                {
                    SecurityHelpers.UnProtectConfigurationFile();
                }               

                scheduler = new TaskScheduler();
                scheduler.EmailQueueJobDefination();
                scheduler.SPSPostJobDefination();
                scheduler.SPSGetJobDefination();
                scheduler.MpesaRepayment();
                scheduler.C2BConfirmation();
                scheduler.LoanStatusWatchdogJob();
                //scheduler.TextAlertJob();
            }

            catch (ReflectionTypeLoadException ex)
            {
                Logger.CreateLog(ex.Message + ex.LoaderExceptions + ex.StackTrace + ex.Source);
            }

            catch(Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace + ex.Source);
            }
            
        }
 
        protected override void OnStop()
        {
            if (scheduler != null)
            {
                scheduler.Stop();
            }
        }

        public void StartDebugging(string[] args)
        {
            OnStart(args);
        }

        private static void ConfigureFactories()
        {            
            System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) =>
            {
                return true;
            };
        }

    }
}

﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Nikopeshe.WebApplication.Startup))]
namespace Nikopeshe.WebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

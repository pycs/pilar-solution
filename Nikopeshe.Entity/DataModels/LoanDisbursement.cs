﻿using System;

namespace Nikopeshe.Entity.DataModels
{    
    public class LoanDisbursement
    {
        public int Type { get; set; }

        public string TypeDesc { get; set; }

        public string Payee { get; set; }

        public string Remarks { get; set; }

        public bool IsDelivered { get; set; }

        public string PrimaryAccountNumber { get; set; }

        public string ThirdPartyTransactionId { get; set; }

        public decimal Amount { get; set; }

        public string BankCode { get; set; }

        public int MCCMNC { get; set; }

        public string MCCMNCDesc { get; set; }

        public string Reference { get; set; }

        public Guid SystemTraceAuditNumber { get; set; }

        public Guid LoanRequestId { get; set; }

        public LoanRequest LoanRequest { get; set; }

        public int Status { get; set; }

        public string StatusDesc { get; set; }

        public string B2MResponseCode { get; set; }

        public string B2MResponseDesc { get; set; }

        public string B2MResultCode { get; set; }

        public string B2MResultDesc { get; set; }

        public string B2MTransactionID { get; set; }

        public string TransactionDateTime { get; set; }

        public decimal WorkingAccountAvailableFunds { get; set; }

        public decimal UtilityAccountAvailableFunds { get; set; }

        public decimal ChargePaidAccountAvailableFunds { get; set; }

        public string TransactionCreditParty { get; set; }

        public int ProcessingStatus { get; set; }
    }
}

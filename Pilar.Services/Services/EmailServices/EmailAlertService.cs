﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services
{
    public class EmailAlertService : IEmailAlertService
    {
        private readonly DataContext datacontext = new DataContext();

        public string errorMessage = string.Empty;

        public EmailAlert FindEmailAlertById(Guid Id)
        {
            var findemailalertbyId = datacontext.EmailAlert.Find(Id);

            return findemailalertbyId;
        }

        public List<EmailAlert> FindEmailAlertByDRLStatus(int status)
        {
            var findemailalertsbystatus = datacontext.EmailAlert.Where(x => x.MailMessageDLRStatus == status && !x.MailMessageTo.Equals("N/A"));

            return findemailalertsbystatus.ToList();
        }

        public void UpdateEmailAlertDRLStatus(Guid Id, int Status)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Id.ToString()) || string.IsNullOrWhiteSpace(Status.ToString()))

                    throw new ArgumentNullException("empty value(s)");

                var findrecordbyId = datacontext.EmailAlert.Find(Id);

                findrecordbyId.MailMessageDLRStatus = Status;

                var entry = datacontext.Entry(findrecordbyId);
                entry.State = EntityState.Modified;                
                datacontext.SaveChanges();
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
        }

        public bool CreateNewEmailAlert(EmailAlert emailAlert)
        {
            datacontext.EmailAlert.Add(emailAlert);

            return datacontext.SaveChanges() >= 0;
        }
    }
}

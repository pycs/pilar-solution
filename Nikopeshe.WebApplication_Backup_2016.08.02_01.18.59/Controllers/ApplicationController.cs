﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize(Roles = "Administrator")]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class ApplicationController : Controller
    {
        private readonly IDashBoardService dashboardservice;
        private readonly IDomainAuditTrail audittrailservice;
        private readonly IC2BTransactionService c2bservice;
        private readonly IEmailAlertService emailservice;
        private readonly ILoanRequestService loanservice;
        private readonly IStaticSettingService staticsettingservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public ApplicationController(IDashBoardService _dashboardservice, IDomainAuditTrail _audittrailservice,
            IC2BTransactionService _c2bservice, IEmailAlertService _emailservice, ILoanRequestService _loanservice,
            IStaticSettingService _staticsettingsservice)
        {
            if(_dashboardservice == null || _audittrailservice == null || _c2bservice == null || _emailservice == null 
                || _loanservice == null || _staticsettingsservice == null)
                throw new ArgumentNullException("null service reference");

            this.dashboardservice = _dashboardservice;
            this.audittrailservice = _audittrailservice;
            this.emailservice = _emailservice;
            this.c2bservice = _c2bservice;
            this.loanservice = _loanservice;
            this.staticsettingservice = _staticsettingsservice;
        }


        // GET: Application
        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public ActionResult AuditLogs()
        {
            return View();
        }

        
        [HttpPost]
        public JsonResult GetAuditLogs(JQueryDataTablesModel datatablemodel)
        {          
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var systemauditlogs = audittrailservice.GetAllDomainAudotTrail();

            var audits = GetAllAuditLogDetails(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, AuditLogList: systemauditlogs);

            var results = this.Json(new JQueryDataTablesResponse<DomainAudit>(items: audits,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho), JsonRequestBehavior.AllowGet);

            return results;

        }

        public IList<DomainAudit> GetAllAuditLogDetails(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<DomainAudit> AuditLogList)
        {
            var GetAllAuditList = AuditLogList;

            totalRecordCount = GetAllAuditList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetAllAuditList = GetAllAuditList.Where(c => c.RecordID.ToString()
                    .ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetAllAuditList.Count;

            IOrderedEnumerable<DomainAudit> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "EventDate":
                        sortedClients = sortedClients == null ? GetAllAuditList.CustomSort(sortedColumn.Direction, cust => cust.EventDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.EventDate);
                        break;

                    case "EventType":
                        sortedClients = sortedClients == null ? GetAllAuditList.CustomSort(sortedColumn.Direction, cust => cust.EventType)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.EventType);
                        break;

                    case "TableName":
                        sortedClients = sortedClients == null ? GetAllAuditList.CustomSort(sortedColumn.Direction, cust => cust.TableName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TableName);
                        break;

                    case "RecordID":
                        sortedClients = sortedClients == null ? GetAllAuditList.CustomSort(sortedColumn.Direction, cust => cust.RecordID)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.RecordID);
                        break;

                    case "AdditionalNarration":
                        sortedClients = sortedClients == null ? GetAllAuditList.CustomSort(sortedColumn.Direction, cust => cust.AdditionalNarration)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.AdditionalNarration);
                        break;

                    case "ApplicationUserName":
                        sortedClients = sortedClients == null ? GetAllAuditList.CustomSort(sortedColumn.Direction, cust => cust.ApplicationUserName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ApplicationUserName);
                        break;

                    case "EnvironmentMachineName":
                        sortedClients = sortedClients == null ? GetAllAuditList.CustomSort(sortedColumn.Direction, cust => cust.EnvironmentMachineName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.EnvironmentMachineName);
                        break;

                    case "EnvironmentDomainName":
                        sortedClients = sortedClients == null ? GetAllAuditList.CustomSort(sortedColumn.Direction, cust => cust.EnvironmentDomainName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.EnvironmentDomainName);
                        break;

                    case "EnvironmentOSVersion":
                        sortedClients = sortedClients == null ? GetAllAuditList.CustomSort(sortedColumn.Direction, cust => cust.EnvironmentOSVersion)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.EnvironmentOSVersion);
                        break;

                    case "EnvironmentMACAddress":
                        sortedClients = sortedClients == null ? GetAllAuditList.CustomSort(sortedColumn.Direction, cust => cust.EnvironmentMACAddress)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.EnvironmentMACAddress);
                        break;

                    case "EnvironmentMotherboardSerialNumber":
                        sortedClients = sortedClients == null ? GetAllAuditList.CustomSort(sortedColumn.Direction, cust => cust.EnvironmentMotherboardSerialNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.EnvironmentMotherboardSerialNumber);
                        break;

                    case "EnvironmentProcessorId":
                        sortedClients = sortedClients == null ? GetAllAuditList.CustomSort(sortedColumn.Direction, cust => cust.EnvironmentProcessorId)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.EnvironmentProcessorId);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        public ActionResult ServiceMonitor()
        {
            var findnumberofpendingemails = emailservice.FindEmailAlertByDRLStatus((int)Enumerations.DLRStatus.Pending).Count;
            var findnumberofqueuedemails = emailservice.FindEmailAlertByDRLStatus((int)Enumerations.DLRStatus.Queued).Count;
            var findpendingc2bforconfirmation = c2bservice.GetC2bTransactionsByStatus((int)Enumerations.C2BTransactionStatus.Pending).Count;
            var findpendingprocessingc2btransaction = c2bservice.GetC2bTransactionsByStatus((int)Enumerations.C2BTransactionStatus.Approved).Count;            
            ViewBag.PendingConfirmationC2B = findpendingc2bforconfirmation;
            ViewBag.PendingEmails = findnumberofpendingemails;
            ViewBag.PendingProcessingC2B = findpendingprocessingc2btransaction;
            ViewBag.QueuedEmails = findnumberofqueuedemails;

            return View();
        }

        [HttpGet]
        public ActionResult Settings()
        {
            return View();
        }

        public JsonResult GetAllSettings(JQueryDataTablesModel datatablemodel)
        {
            var pagecollection = staticsettingservice.GetAllStaticSettings();

            int totalrecordcount = 0;

            int searchrecordcount = 0;

            var settingsList = GetSystemSettingToList(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalrecordcount, searchRecordCount: out searchrecordcount, searchString: datatablemodel.sSearch, StaticSettingList: pagecollection);

            return Json(new JQueryDataTablesResponse<StaticSetting>(items: settingsList,
                totalRecords: totalrecordcount,
                totalDisplayRecords: searchrecordcount,
                sEcho: datatablemodel.sEcho));
        }

        public IList<StaticSetting> GetSystemSettingToList(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<StaticSetting> StaticSettingList)
        {
            var getallstaticsetting = StaticSettingList;

            totalRecordCount = getallstaticsetting.Count();

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                getallstaticsetting = getallstaticsetting.Where(c => c.Key
                    .ToLower().Contains(searchString.ToLower())
                    || c.Value.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = getallstaticsetting.Count();

            IOrderedEnumerable<StaticSetting> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? getallstaticsetting.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "IsLocked":
                        sortedClients = sortedClients == null ? getallstaticsetting.CustomSort(sortedColumn.Direction, cust => cust.IsLocked)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsLocked);
                        break;

                    case "SettingsKey":
                        sortedClients = sortedClients == null ? getallstaticsetting.CustomSort(sortedColumn.Direction, cust => cust.Key)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Key);
                        break;

                    case "Value":
                        sortedClients = sortedClients == null ? getallstaticsetting.CustomSort(sortedColumn.Direction, cust => cust.Value)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Value);
                        break;                    
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        ///
        [HttpGet]
        public ActionResult EditSettings(Guid Id)
        {
            var findstaticsettingsbyid = staticsettingservice.FindStaticSettingById(Id);

            return View(findstaticsettingsbyid);
        }

        [HttpPost]        
        public ActionResult EditSettings(StaticSetting staticsettings)
        {
            if (staticsettings == null)
                throw new ArgumentNullException("entity cannot be null.");

            if (ModelState.IsValid)
            {
                var findstaticsetting = staticsettingservice.FindStaticSettingById(staticsettings.Id);
                try
                {
                    findstaticsetting.IsLocked = staticsettings.IsLocked;
                    findstaticsetting.Key = staticsettings.Key;
                    findstaticsetting.Value = staticsettings.Value;
                    staticsettingservice.UpdateStaticSetting(staticsettings, serviceHeader);
                    return View("Settings");
                }
                    
                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(staticsettings);
                }
            }

            return View(staticsettings);
        }
    }   
}
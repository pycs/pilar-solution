﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Nikopeshe.WebApplication.Extensions
{
    public class DateTimeTryParse
    {
        public static bool DateTimeTryParseExact(string s, out DateTime date)
        {
            string[] formats = { "dd/MM/yyyy", "dd/M/yyyy", "d/M/yyyy", "d/MM/yyyy", "dd/MM/yy", "dd/M/yy", "d/M/yy", "d/MM/yy", 
                                 "MM/dd/yyyy", "M/dd/yyyy", "M/d/yyyy", "MM/d/yyyy", "MM/dd/yy", "M/dd/yy", "M/d/yy", "MM/d/yy",
                                 "yyyy/MM/dd", "yyyy/M/dd", "yyyy/M/d", "yyyy/MM/d", "yy/MM/dd", "yy/M/dd", "yy/M/d", "yy/d/MM"};

            var dateParts = s.Split(new char[] { ' ' });

            return DateTime.TryParseExact(dateParts[0], formats, System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
        }
    }
}
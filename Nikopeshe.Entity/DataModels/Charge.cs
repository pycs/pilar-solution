﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    public class Charge : BaseEntity
    {
        [Display(Name = "Charge Name")]
        [MaxLength(100)]
        public string ChargeName { get; set; }

        [Display(Name = "Is Enabled?")]
        [Index]
        public bool IsEnabled { get; set; }

        [Display(Name="Chart of Accounts")]
        public Guid ChartofAccountId { get; set; }

        public virtual ChartofAccount ChartofAccount { get; set; }

        [NotMapped]
        public string Commands { get; set; }

        [Display(Name="Is Charge Recurring?")]
        public bool IsRecurring { get; set; }

        public int ChargeRecoveryMethod { get; set; }

        public string ChargeRecoveryMethodDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.ChargeRecoveryMethod), ChargeRecoveryMethod) ? EnumHelper.GetDescription((Enumerations.ChargeRecoveryMethod)ChargeRecoveryMethod) : string.Empty;
            }
        }
    }
}

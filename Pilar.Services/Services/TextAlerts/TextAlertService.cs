﻿
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
namespace Pilar.Services.Services.TextAlerts
{
    public class TextAlertService : ITextAlertService
    {
        private DataContext datacontext = new DataContext();

        public bool CreateNewTextAlert(TextAlert textAlert)
        {
            datacontext.TextAlert.Add(textAlert);

            return datacontext.SaveChanges() >= 0;
        }
    }
}

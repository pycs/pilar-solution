﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class CustomerGroupRelationshipService : ICustomerGroupRelationshipService
    {
        private string ErrorMessage = string.Empty;

        private IRepository<CustomerGroupRelationship> customerGroupRelationRepository;

        public CustomerGroupRelationshipService(IRepository<CustomerGroupRelationship> _customerGroupRelationshipService)
        {
            this.customerGroupRelationRepository = _customerGroupRelationshipService;
        }

        public void AddNewCustomerGroupRelationship(CustomerGroupRelationship customerGroup, ServiceHeader serviceHeader)
        {
            if (customerGroup == null)
                throw new ArgumentNullException("customer Group cannot be null");

            try
            {                
                customerGroupRelationRepository.Insert(customerGroup, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public void UpdateCustomerGroupRelationship(CustomerGroupRelationship customerGroup, ServiceHeader serviceHeader)
        {
            if (customerGroup == null)
                throw new ArgumentNullException("customer Group cannot be null");

            try
            {
                customerGroupRelationRepository.Update(customerGroup, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public void DeleteCustomerGroupRelationship(Guid Id, ServiceHeader serviceHeader)
        {
            if (Id == Guid.Empty)
            {
                throw new ArgumentNullException("Id cannot be null");
            }

            try
            {
                var findCustomerGroupById = customerGroupRelationRepository.GetById(Id);

                if (findCustomerGroupById == null)
                {
                    customerGroupRelationRepository.Delete(findCustomerGroupById, serviceHeader);
                }

            }

            catch(Exception ex)
            {
                throw ex;
            }
        }

        public CustomerGroupRelationship FindCustomerGroupRelationshipById(Guid Id)
        {
            if (Id == Guid.Empty)
            {
                throw new ArgumentNullException("id cannot be null");
            }

            var findCustomerGroupRelationsById = customerGroupRelationRepository.GetById(Id);

            return findCustomerGroupRelationsById;
        }

        public IList<CustomerGroupRelationship> GetCustomerGroupRelationshipByGroupId(Guid GroupId)
        {
            if (GroupId == Guid.Empty)
            {
                throw new ArgumentNullException("GroupId cannot be null");
            }

            var findCustomerByGroupId = customerGroupRelationRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.GroupId == GroupId);

            return findCustomerByGroupId.ToList();
        }
    }
}

﻿using Nikopeshe.Entity.DataModels;
using System.Data.Entity.ModelConfiguration;

namespace Nikopeshe.Data.Mappings
{
    public class RelationshipManagerEntityTypeMapping : EntityTypeConfiguration<RelationshipManager>
    {
        public RelationshipManagerEntityTypeMapping()
        {
            this.HasKey(x => x.Id);
            this.ToTable(string.Format("{0}RelationshipManagers", DefaultSettings.Instance.TablePrefix));
        }      
    }
}

﻿using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using Android.Content.PM;
using Java.Lang;
using System.Windows.Input;

namespace Pilar.AndroidApplication
{
    [Activity(Label = "Pilar", MainLauncher = true, Icon = "@drawable/icon", Theme="@style/CustomActionBarTheme", ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);
            // Set our view from the "main" layout resource
            // create events for buttons

            var LoginButton = FindViewById<Button>(Resource.Id.login);
            LoginButton.Click += (sender, e) =>
            {

                //OnStart ();
                var login = new Intent(this, typeof(LoginActivity));
                StartActivity(login);

                //ErrorLogger.CreateErrorLog("Login");
            };
            //open save account information

            var RegisterButton = FindViewById<Button>(Resource.Id.registerButton);
            RegisterButton.Click += (sender, e) =>
            {

                var signup = new Intent(this, typeof(RegisterActivity));
                StartActivity(signup);

                //ErrorLogger.CreateErrorLog("Signup");
            };
        }

    }
}


﻿using Nikopeshe.Entity.DataModels;

namespace Pilar.Services.Services.TextAlerts
{
    public interface ITextAlertService
    {
        bool CreateNewTextAlert(TextAlert textAlert);
    }
}

﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Nikopeshe.Services.InterfaceImplementations;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Web.Http;
using System.Xml;

namespace Nikopeshe.WebApplication.Controllers.APICotrollers
{
    public class C2BController : ApiController
    {
        readonly IMessageQueueService _messageQueueService;
        private string _messageRelayQueuePath = string.Empty;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public C2BController()
        {

        }

        public C2BController(IMessageQueueService messageQueueService)
        {
            this._messageQueueService = new MessageQueueService();
            _messageRelayQueuePath = ConfigurationManager.AppSettings["C2BQueuePath"];
        }
        [AuthorizeIPAddress]
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);
                    XmlElement element = xmlDoc.DocumentElement;
                    XmlNodeList TransID = xmlDoc.SelectNodes("/");
                    XmlNodeList XmlDocNodes = xmlDoc.SelectNodes("/InstantPaymentNotification");
                    C2BTransaction c2btrx = new C2BTransaction();
                    string TransactionId = element.GetAttribute("Id");
                    foreach (XmlNode node in XmlDocNodes)
                    {
                        string MSISDN = node["MSISDN"].InnerText.ToString();
                        c2btrx.MSISDN = MSISDN;
                        c2btrx.BusinessShortCode = node["BusinessShortCode"].InnerText.ToString();
                        c2btrx.InvoiceNumber = node["InvoiceNumber"].InnerText.ToString();
                        c2btrx.TransID = node["TransID"].InnerText.ToString();
                        c2btrx.TransAmount = decimal.Parse(node["TransAmount"].InnerText.ToString());
                        c2btrx.ThirdPartyTransID = node["ThirdPartyTransID"].InnerText.ToString();
                        c2btrx.TransTime = node["TransTime"].InnerText.ToString();
                        c2btrx.BillRefNumber = node["BillRefNumber"].InnerText.ToString().Trim();
                        c2btrx.TransactionId = Guid.Parse(TransactionId);
                        c2btrx.Result = "Notification Received Successfully.";

                        XmlNodeList kycnode = xmlDoc.SelectNodes("/InstantPaymentNotification/KYCInfoList/KYCInfo");
                        var i = 0;
                        var nodecount = kycnode.Count;
                        if (nodecount == 0)
                        {

                        }
                        else if (nodecount == 1)
                        {
                            for (i = 0; i <= 1 - 1; i++)
                            {
                                c2btrx.KYCInfo_FirstName = kycnode[i].ChildNodes.Item(1).InnerText.ToString();
                            }
                        }
                        else if (nodecount == 2)
                        {
                            for (i = 0; i <= 1 - 1; i++)
                            {
                                c2btrx.KYCInfo_FirstName = kycnode[i].ChildNodes.Item(1).InnerText.ToString();
                                c2btrx.KYCInfo_LastName = kycnode[i + 1].ChildNodes.Item(1).InnerText.ToString();
                            }
                        }

                        else
                        {
                            for (i = 0; i <= 1 - 1; i++)
                            {
                                c2btrx.KYCInfo_FirstName = kycnode[i].ChildNodes.Item(1).InnerText.ToString();
                                c2btrx.KYCInfo_LastName = kycnode[i + 1].ChildNodes.Item(1).InnerText.ToString();
                                c2btrx.KYCInfo_MiddleName = kycnode[i + 2].ChildNodes.Item(1).InnerText.ToString();
                            }
                        }
                    }

                    
                    _messageQueueService.Send(_messageRelayQueuePath, c2btrx, Enumerations.MessageCategory.C2BNotificationSync, System.Messaging.MessagePriority.High);
                }

                catch (Exception ex)
                {
                    var error = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(string.Format("Error occurred.")),
                        ReasonPhrase = "Failed."
                    };

                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    throw new HttpResponseException(error);
                }

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(string.Format("Notification Received Successfully."));
                response.ReasonPhrase = "SUCCESS.";
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }
    }
}
﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data;

namespace Nikopeshe.Services.Interfaces
{
    public interface ICustomerAccountService
    {
        void AddNewCustomersAccount(CustomersAccount customersaccount, ServiceHeader serviceHeader);

        void UpdateCustomersAccount(CustomersAccount customersaccount, ServiceHeader serviceHeader);

        CustomersAccount FindCustomersAccountById(Guid Id);

        IList<CustomersAccount> GetAllCustomersAccount(IsolationLevel isolationLevel);

        CustomersAccount FindCustomersAccountByAccountNumber(string AccountNumber);

        IList<CustomersAccount> GetCustomersAccountByAccountType(int AccountType);

        CustomersAccount FindCustomersAccountByCustomerId(Guid CustomerId);

        CustomersAccount FindCustomerByCustomerIdandAccountType(Guid CustomerId, int AccountType);

        IList<CustomersAccount> GetAllCustomerAccountNumbers(Guid CustomerId, IsolationLevel isolationLevel);

        decimal FindCustomerAccountBalanceByProductCode(Guid CustomerAccountId, Guid targetProductId, int ProductCode);

        CustomersAccount GetCustomerAccountNumberByAccountType(Guid? CustomerId, int AccountType);

        CustomerAccountInfo GetCustomerAccountDetailsByCustomerSavingsAccountId(Guid SavingsAccountId);
    }

    public class CustomerAccountInfo
    {
        public decimal AccountBalance { get; set; }

        public decimal PrincipalPaid { get; set; }

        public decimal InterestPaid { get; set; }

        public decimal PrincipalDue { get; set; }

        public decimal InterestDue { get; set; }

        public decimal TotalAmountDue { get; set; }

        public decimal TotalLoanAmount { get; set; }

        public decimal TotalLoanAmountPaid { get; set; }
    }
}

﻿using Microsoft.Practices.Unity.Utility;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WindowsService.Interfaces;
using Quartz;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.WindowsService.EmailJob
{
    public class EmailQueueingJob : IJob
    {
        private MessageQueue _messageQueue;

        private readonly ISmtpService _smtpService;
        
        public EmailQueueingJob()
        {
            _smtpService = new SmtpService();            
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {                
                IList<EmailAlert> pendingemails = FindEmailsWithPendingDRLStatus();

                EmailJobDispatcher dispatcher = new EmailJobDispatcher();

                string QueueName = ConfigurationManager.AppSettings["MessageRelayQueuePath"].ToString();
                _messageQueue = new MessageQueue(QueueName, true, false, QueueAccessMode.Peek);
                if (!MessageQueue.Exists(QueueName))
                {
                    Exception ex = new Exception();
                    Logger.CreateLog(ex.Message);
                }
                else
                {
                    _messageQueue = new MessageQueue(QueueName);                    
                }

                using (_messageQueue)
                {
                    _messageQueue.Formatter = new XmlMessageFormatter();

                    _messageQueue.ReceiveCompleted += new ReceiveCompletedEventHandler(queue_ReceiveCompleted);

                    // Start listening.
                    _messageQueue.BeginReceive();
                    using(MessageQueueTransaction mqt = new MessageQueueTransaction())
                    {
                        foreach (var pendingemail in pendingemails)
                        {
                            //send the email to queue
                            _messageQueue.Send(pendingemail, mqt);
                            //update email record to queued
                            dispatcher.UpdateEmailAlert(pendingemail.Id, (int)Enumerations.DLRStatus.Queued);
                        } 
                    }                   
                }
                               
            }

            catch(Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }
            
        }

        public void queue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            EmailJobDispatcher maildispatcher = new EmailJobDispatcher();

            // Set the formatter to indicate body contains an Order.
            _messageQueue.Formatter = new XmlMessageFormatter(new Type[] { typeof(EmailAlert) });

            try
            {
                // Receive and format the message. 
                Message myMessage = _messageQueue.Receive();
                EmailAlert emailAlert = (EmailAlert)myMessage.Body;
                if (!string.IsNullOrWhiteSpace(emailAlert.MailMessageCC))
                    _smtpService.SendEmail(emailAlert.MailMessageFrom, emailAlert.MailMessageTo, emailAlert.MailMessageCC, emailAlert.MailMessageSubject, emailAlert.MailMessageBody, emailAlert.MailMessageIsBodyHtml);
                else _smtpService.SendEmail(emailAlert.MailMessageFrom, emailAlert.MailMessageTo, emailAlert.MailMessageSubject, emailAlert.MailMessageBody, emailAlert.MailMessageIsBodyHtml);
                //maildispatcher.SendEmail(emailAlert.MailMessageFrom, emailAlert.MailMessageTo, emailAlert.MailMessageSubject, emailAlert.MailMessageBody, emailAlert.MailMessageIsBodyHtml);
                maildispatcher.UpdateEmailAlert(emailAlert.Id, (int)Enumerations.DLRStatus.Delivered);
            }

            catch (MessageQueueException ex)
            {
                // Handle Message Queuing exceptions.
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }

            // Handle invalid serialization format.
            catch (InvalidOperationException ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }

            catch (Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }

        /// <summary>
        /// Find all pending email
        /// </summary>
        /// <returns></returns>
        public IList<EmailAlert> FindEmailsWithPendingDRLStatus()
        {
            DataContext datacontext = new DataContext();

            int numberofemails = int.Parse(ConfigurationManager.AppSettings["EmailNumberofItems"]);

            var pendingemails = datacontext.EmailAlert.Where(x => x.MailMessageDLRStatus == (int)Enumerations.DLRStatus.Pending).Take(20);

            return pendingemails.ToList();
        }
    }
}

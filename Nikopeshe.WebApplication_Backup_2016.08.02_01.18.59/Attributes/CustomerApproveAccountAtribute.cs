﻿using Nikopeshe.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Nikopeshe.WebApplication.Attributes
{
    public class CustomerApproveAccountAtribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException("filterContext");

            IPrincipal user = filterContext.HttpContext.User;

            DataContext datacontext = new DataContext();

            if (user != null && user.Identity.IsAuthenticated)
            {
                bool role = Roles.IsUserInRole("Customer");

                // has the customer approved account
                if (role)
                {
                    var findcustomerbymobilenumber = datacontext.Customer.Where(x => x.MobileNumber == user.Identity.Name).FirstOrDefault();
                    if (findcustomerbymobilenumber == null)
                        throw new ArgumentException("could not find the specified customer..");

                    if (!findcustomerbymobilenumber.IsApproved)
                    {
                        UrlHelper urlHelper = new UrlHelper(filterContext.RequestContext);

                        filterContext.Result = new RedirectResult(urlHelper.Action("ApproveCustomer", "Account"));
                    }

                    else if (findcustomerbymobilenumber.CustomerRegistrationType == Entity.DataModels.Enumerations.CustomerRegistrationType.WalkIn)
                    {
                        MembershipUser membershipUser = Membership.GetUser();

                        if (membershipUser.LastPasswordChangedDate == membershipUser.CreationDate)
                        {
                            UrlHelper urlHelper = new UrlHelper(filterContext.RequestContext);

                            filterContext.Result = new RedirectResult(urlHelper.Action("ChangePassword", "Account"));
                        }
                    }
                }
            }
        }
    }
}
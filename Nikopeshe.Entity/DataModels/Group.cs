﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{
    public class Group : BaseEntity
    {
        [Required]
        [Display(Name="Group Name")]
        public string GroupName { get; set; }

        [Display(Name="Registration Number")]
        public string RegistrationNumber { get; set; }
        
        [Display(Name="Date Of Registration")]
        public DateTime? DateOfRegistration { get; set; }

        [Display(Name = "Govt Date of Registration")]
        public DateTime? GovtDateOfRegistration { get; set; }

        public string GroupNumberGroupName
        {
            get
            {
                return GroupNumber + " => " + GroupName;
            }
        }

        [Display(Name = "Current Year of License")]
        [MaxLength(4)]
        [Column(TypeName = "VARCHAR")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Invalid Current Year of License. Must be 0-9")]
        public string CurrentYearOfLicence { get; set; }

        [Display(Name="P.O. BOX Number")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "P.O Box Number Code. Must be 0-9")]
        public string PoBox { get; set; }

        [Display(Name="Postal Code")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Invalid Postal Code. Must be 0-9")]
        public string PostalCode { get; set; }

        [Display(Name = "Mobile Number / Mpesa Number")]
        [StringLength(12, MinimumLength = 12)]
        [Column(TypeName = "VARCHAR")]
        [RegularExpression(@"^254(?:[0-9]??){6,14}[0-9]$", ErrorMessage = "The mobile number should start with country code 254 followed by phone number")] 
        public string TelephoneNumber { get; set; }

        [Required]
        [Display(Name = "Main Activity")]
        public Guid ProfessionId { get; set; }

        public virtual Profession Profession { get; set; }

        public virtual Customer Customer { get; set; }

        public Guid? CustomerId { get; set; }

        [Index]
        [Display(Name = "Branch / County Name")]
        public Guid? BranchId { get; set; }

        public virtual Branch Branch { get; set; }

        [Required]
        [Display(Name = "Dealing In")]
        public string DealingIn { get; set; }

        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [Display(Name = "Election Type")]
        [Range(1, int.MaxValue, ErrorMessage = "Election Type cannot be null.")]
        public Nikopeshe.Entity.DataModels.Enumerations.Elections ElectionType  { get; set; }

        public string ElectionsTypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.Elections), ElectionType) ? EnumHelper.GetDescription((Enumerations.Elections)ElectionType) : string.Empty;
            }
        }

        [Display(Name="Bank Name")]
        public string BankCode { get; set; }

        [Display(Name = "Branch Name")]
        public string BranchCode { get; set; }

        [Display(Name="Bank Account Number")]
        public string BankAccountNumber { get; set; }

        public string BankPaybillNumber { get; set; }

        [Display(Name = "County Name")]
        [Required]
        public string CountyCode { get; set; }

        public string CountyName { get; set; }

        [Required]
        [Display(Name = "Constituency Name")]
        public string ConstituencyCode { get; set; }

        public string ConstituencyName { get; set; }

        //[Required]
        [Display(Name = "Ward Name")]
        public string WardCode { get; set; }

        public string WardName { get; set; }

        [Required]
        [Display(Name = "Village/Estate")]
        public string Village { get; set; }

        [Required]
        [Display(Name = "Street/ Landmark")]
        public string Landmark { get; set; }

        [Display(Name = "Plot No. or Bldg Name")]
        public string PlotNumber { get; set; }
       
        public bool IsEnabled { get; set; }
        
        [Display(Name="Election Date")]        
        public DateTime? ElectionDate { get;set;}        

        [Display(Name="Meeting Frequency")]
        [Range(1, int.MaxValue, ErrorMessage = "Meeting Frequency cannot be null.")]
        public Nikopeshe.Entity.DataModels.Enumerations.MeetingFrequency MeetingFrequency { get; set; }

        [Display(Name = "Government Registration")]
        [Range(1, int.MaxValue, ErrorMessage = "Group Type cannot be null.")]
        public Enumerations.GroupType GroupType { get; set; }

        public string MeetingFrequencyDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.MeetingFrequency), MeetingFrequency) ? EnumHelper.GetDescription((Enumerations.MeetingFrequency)MeetingFrequency) : string.Empty;
            }
        }

        //[Display(Name="Meeting Location")]
        //[Required]
        //public string MeetingLocation { get; set; }

        [NotMapped]
        public string Commands { get; set; }

        public int RecordStatus { get; set; }

        public string RecordStatusDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.RecordStatus), RecordStatus) ? EnumHelper.GetDescription((Enumerations.RecordStatus)RecordStatus) : string.Empty;
            }
        }
        
        [Display(Name="Created By")]
        public string CreatedBy { get; set; }

        [Display(Name="Approved By")]
        public string ApprovedBy { get; set; }

        [Column(TypeName = "VARCHAR")]
        [Index("GroupNumber", IsUnique = true)]
        [Required]
        public string GroupNumber { get; set; }

        [Display(Name="Program Officer")]
        //[Required]
        public Guid? RelationshipManagerId { get; set; }

        public virtual RelationshipManager RelationshipManager { get; set; }
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.DTO;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.Services.Specifications;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class GroupService : IGroupService
    {
        private IRepository<Group> groupRepository;

        private IRepository<Customer> customerRepository;

        public string ErrorMessage = string.Empty;

        public GroupService(IRepository<Group> _groupRepository, IRepository<Customer> _customerRepository)
        {
            groupRepository = _groupRepository;
            this.customerRepository = _customerRepository;
        }

        public void CreateNewGroup(Group group, Customer customer, ServiceHeader serviceHeader)
        {
            if (group == null)
                throw new ArgumentNullException("group cannot be null");

            try
            {
                if (group.ProfessionId == Guid.Empty)
                {
                    throw new ArgumentNullException("Main Activity cannot be null");
                }
                if (group.ElectionType == 0)
                    throw new ArgumentNullException("Election Type cannot be null");

                if (group.MeetingFrequency == 0)
                    throw new ArgumentNullException("Meeting Frequency cannot be null");

                if (group.RelationshipManagerId == Guid.Empty)
                    throw new ArgumentNullException("Relationship Manager cannot be null");
                #region Group
                groupRepository.Insert(group, serviceHeader);
                #endregion    
            
                #region Create CustomerAccount
                #endregion
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public void UpdateGroup(Group group, ServiceHeader serviceHeader)
        {
            if (group == null)
                throw new ArgumentNullException("entity cannot be null");

            
                groupRepository.Update(group, serviceHeader);
            

            
        }

        public Group FindGroupById(Guid Id)
        {
            if (Id == null || Id == Guid.Empty)
                throw new ArgumentNullException("value cannot be null.");

            var group = groupRepository.GetById(Id);

            return group;
        }

        public IList<Group> GetAllGroup(IsolationLevel isolationLevel)
        {
            return groupRepository.GetAll(isolationLevel).ToList();
        }

        public Group FindGroupByRegistrationNumber(string regNumber)
        {

            var findGroup = groupRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.RegistrationNumber == regNumber);

            return findGroup.FirstOrDefault();
        }

        public Group FindGroupByGroupNumber(string GroupNumber)
        {
            if (string.IsNullOrWhiteSpace(GroupNumber))
                throw new ArgumentNullException("GroupNumber");

            var findGroupByGroupNumber = groupRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.GroupNumber == GroupNumber).FirstOrDefault();

            return findGroupByGroupNumber;
        }

        public Group FindGroupByCustomerId(Guid CustomerId)
        {
            if (CustomerId == Guid.Empty)
                throw new ArgumentNullException("CustomerId");

            var findGroupByCustomerId = groupRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CustomerId == CustomerId).FirstOrDefault();

            return findGroupByCustomerId;
        }

        public Group FindGroupCustomerAccountsIdByGroupId(Guid GroupId)
        {
            if (GroupId == Guid.Empty)
                throw new ArgumentNullException("GroupId");

            var FindGroupCustomerAccountsIdByGroupId = groupRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.Id == GroupId).FirstOrDefault();

            return FindGroupCustomerAccountsIdByGroupId;
        }

        public Group FindGroupByRM(Guid RMId)
        {
            if (RMId == Guid.Empty)
                throw new ArgumentNullException("CustomerId");

            var findGroupByCustomerId = groupRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.RelationshipManagerId == RMId).FirstOrDefault();

            return findGroupByCustomerId;
        }

        public IList<string> GetDistinctCountyCodes()
        {
            var findGroupCountByCountyCode = groupRepository.GetAll(IsolationLevel.ReadCommitted).GroupBy(x => x.CountyCode).Select(x => x.FirstOrDefault()).ToList();

            var distinctCountyCodes = new List<string>();

            foreach(Group group in findGroupCountByCountyCode)
            {
                distinctCountyCodes.Add(group.CountyCode);
            }

            return distinctCountyCodes;
        }

        public int FindGroupCountByCountyCode(string CountyCode)
        {
            if (string.IsNullOrWhiteSpace(CountyCode))
                throw new ArgumentNullException("CountyCode");

            var findGroupCountByCountyCode = groupRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CountyCode == CountyCode).Count();

            return findGroupCountByCountyCode;
        }

        public bool CheckDuplicateGroupNumber(string groupNumber)
        {
            if (string.IsNullOrWhiteSpace(groupNumber))
                throw new ArgumentNullException("values cannot be null");

            var count = groupRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.GroupNumber == groupNumber).Count();

            if (count >= 1)
                return true;

            return false;
        }


        public PageCollectionInfo<Group> FindGroupsFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = GroupSpecification.DefaultSpec(searchString);

            ISpecification<Group> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var orderLinePagedCollection = groupRepository.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (orderLinePagedCollection != null)
            {
                var pageCollection = orderLinePagedCollection.Items.ToList();

                var totalRecordCount = orderLinePagedCollection.TotalItems;

                return new PageCollectionInfo<Group> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }


        public PageCollectionInfo<Group> FindGroupsByBranchIdFilterInPage(Guid id, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending) 
        {
            var filter = GroupSpecification.FindGroupByBranchId(id, searchString);

            ISpecification<Group> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var orderLinePagedCollection = groupRepository.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (orderLinePagedCollection != null)
            {
                var pageCollection = orderLinePagedCollection.Items.ToList();

                var totalRecordCount = orderLinePagedCollection.TotalItems;

                return new PageCollectionInfo<Group> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }
        
    }
}

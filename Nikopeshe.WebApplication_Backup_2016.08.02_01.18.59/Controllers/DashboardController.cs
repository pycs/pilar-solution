﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Nikopeshe.Services.InterfaceImplementations;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class DashboardController : Controller
    {
        private readonly IDashBoardService dashboardservice;
        private readonly ILoanRequestService loanrequestservice;
        private readonly UserInfoService userInfo;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public DashboardController(IDashBoardService _dashboardservice,
            ILoanRequestService _loanrequestservice, UserInfoService _userInfo)
        {
            if(_dashboardservice == null || _loanrequestservice == null)
                throw new ArgumentNullException("null service reference");

            this.dashboardservice = _dashboardservice;
            this.loanrequestservice = _loanrequestservice;
            this.userInfo = _userInfo;
        }       

        public ActionResult Dashboard()
        {
            var getCurrentUser = User.Identity.Name;
            var findUserInfo = userInfo.FindUserByUserName(getCurrentUser);
            decimal totalactiveloansamount = 0m;
            decimal totalInterestAmount = 0m;            
            int duetodayloans = 0;      
            int totalActiveLoanCount = 0;      
            if (findUserInfo != null && findUserInfo.Branch.BranchCode != "000")
            {
                Tuple<decimal, decimal, int, int> results = loanrequestservice.SumAllActiveLoansAmountTotalInterest(findUserInfo.BranchId);                
                totalactiveloansamount = results.Item1;
                totalActiveLoanCount = results.Item3;
                duetodayloans = results.Item4;
                totalInterestAmount = results.Item2;
            }
            else
            {
                Tuple<decimal, decimal, int, int> results = loanrequestservice.SumAllActiveLoansAmountTotalInterest(Guid.Empty);                
                totalactiveloansamount = results.Item1;
                totalActiveLoanCount = results.Item3;
                duetodayloans = results.Item4;
                totalInterestAmount = results.Item2;
            }

            ViewBag.ActiveLoanCount = totalActiveLoanCount;
            ViewBag.LoanDueToday = duetodayloans;
            ViewBag.TotalActiveLoansAmount = totalactiveloansamount;
            ViewBag.InterestCount = totalInterestAmount;
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DashBoard model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.IsPublished = true;
                    dashboardservice.AddNewDashBoard(model, serviceHeader);
                }

                return RedirectToAction("Index", "Application");
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View();
            }
        }
    }

    public class DashboardContent
    {
        public string Title { get; set; }
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Content { get; set; }
        public bool IsPublished { get; set; }
    }
}
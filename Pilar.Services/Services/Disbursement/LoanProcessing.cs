﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.Disbursement
{
    public class LoanProcessing : ILoanProcessing
    {
        private DataContext datacontext = new DataContext();

        public IList<LoanRequest> GetPendingDisbursmentLoanRequest()
        {
            var findloanrequestbyprocessingstatus = datacontext.LoanRequest.Where(x => x.Status == (int)Enumerations.LoanStatus.Disbursed && x.ProcessingStatus == (int)Enumerations.ProcessingStatus.Disburse).Take(10).ToList();

            return findloanrequestbyprocessingstatus.ToList();
        }

        public void UpdateLoanRequest(LoanRequest loanrequest)
        {
            var findloanrequestbyid = datacontext.LoanRequest.Find(loanrequest.Id);
            var entry = datacontext.Entry(findloanrequestbyid);
            entry.State = EntityState.Modified;            
            datacontext.SaveChanges();
        }

        public IList<LoanRequest> GetLoanDibursementWithPendingGetStatus()
        {
            IList<LoanRequest> submittedloanrequests = datacontext.LoanRequest.Where(x => x.SPSStatus != (int)Enumerations.EFTStatus.Completed 
                && x.SPSStatus != (int)Enumerations.EFTStatus.Cached && x.SPSStatus != (int)Enumerations.EFTStatus.Failed
                && x.SPSStatus != (int)Enumerations.EFTStatus.Rejected
                && x.ProcessingStatus == (int)Enumerations.ProcessingStatus.Submitted
                && x.Status == (int)Enumerations.LoanStatus.Disbursed || x.ProcessingStatus == (int)Enumerations.ProcessingStatus.QueryStatus || x.ProcessingStatus == (int)Enumerations.ProcessingStatus.Queued).ToList();

            return submittedloanrequests;
        }

        public LoanRequest FindLoanRequestById(Guid Id)
        {
            var findloanrequestbyid = datacontext.LoanRequest.Find(Id);

            return findloanrequestbyid;
        }

        public IList<LoanRequest> FindLoanRequestDueGivenDays()
        {
            var finddueloans = new List<LoanRequest>();
            DateTime dayToday = DateTime.Today;            
            var NoOfDays = datacontext.StaticSetting.Where(x => x.Key == "LOAN_REMINDER_DUE_DAYS").FirstOrDefault();
            int parseduedays = int.Parse(NoOfDays.Value.ToString());
            DateTime dueloans = dayToday.AddDays(parseduedays);
            if (parseduedays != 0)
            {                                
                finddueloans = datacontext.LoanRequest.Where(x => (x.NextPaymentDate >= DateTime.Today && x.NextPaymentDate <= dueloans) && x.Status == (int)Enumerations.LoanStatus.Active).ToList();
            }            

            return finddueloans.ToList();
        }

        public IList<LoanRequest> FindLoanRequestByStatus(int Status)
        {
            var findLoanRequestByStatus = datacontext.LoanRequest.Where(x => x.Status == Status).ToList();

            return findLoanRequestByStatus;
        }

        public IList<LoanTransactionHistory> FindLoanTransactionHistoryByLoanRequestId(Guid LoanRequestId)
        {
            if (LoanRequestId == Guid.Empty)
                throw new ArgumentNullException("id cannot be null");

            var findLoanTransactionByLoanRequestid = datacontext.LoanTransactionHistory.Where(x => x.LoanRequestId == LoanRequestId && x.TransactionCategory == (int)Enumerations.TransactionCategory.Approval).ToList();

            return findLoanTransactionByLoanRequestid;
        }

        public IList<LoanTransactionHistory> GetAllLoanRequestTransactionsByLoanRequestId(Guid LoanRequestId)
        {
            if (LoanRequestId == Guid.Empty)
                throw new ArgumentNullException("id cannot be null");

            var findAllLoanRequestTransactions = datacontext.LoanTransactionHistory.Where(x => x.LoanRequestId == LoanRequestId).ToList();

            return findAllLoanRequestTransactions;
        }
    }
}

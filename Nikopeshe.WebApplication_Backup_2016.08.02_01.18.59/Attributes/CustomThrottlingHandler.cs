﻿using Nikopeshe.Data.Mappings;
using System;
using System.Net.Http;
using System.Text;
using WebApiThrottle;

namespace Nikopeshe.WebApplication.Attributes
{
    public class CustomThrottlingHandler : ThrottlingHandler
    {
        protected override RequestIdentity SetIndentity(HttpRequestMessage request)
        {            
            var requestIdentity = new RequestIdentity()
            {
                ClientKey = "",
                ClientIp = base.GetClientIp(request).ToString(),
                Endpoint = request.RequestUri.AbsolutePath
            };

            return requestIdentity;
        }
    }
}
﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;

namespace Pilar.Services.Services
{
    public interface IEmailAlertService
    {
        List<EmailAlert> FindEmailAlertByDRLStatus(int status);

        void UpdateEmailAlertDRLStatus(Guid Id, int status);

        EmailAlert FindEmailAlertById(Guid Id);

        bool CreateNewEmailAlert(EmailAlert emailAlert);
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;

namespace Nikopeshe.Services.Interfaces
{
    public interface ISystemGeneralLedgerAccountMappingService
    {
        void AddNewSystemGeneralLedgerAccountMapping(SystemGeneralLedgerAccountMapping systemgeneralledgemapping, ServiceHeader serviceHeader);

        void UpdateSystemGeneralLedgerAccountMapping(SystemGeneralLedgerAccountMapping systemgeneralledgemapping, ServiceHeader serviceHeader);

        SystemGeneralLedgerAccountMapping FindSystemGeneralLedgerAccountMappingsByGLCode(int GLCode);

        SystemGeneralLedgerAccountMapping FindSystemGeneralLedgerAccountMappingsById(Guid id);

        IList<SystemGeneralLedgerAccountMapping> GetAllSystemGeneralLedgerAccountMappings();

    }
}
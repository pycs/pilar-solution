﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    [Serializable]
    public class ChartofAccount : BaseEntity
    {
        [Display(Name = "Parent Account")]
        [Index("Parent_Id")]
        public Guid? ParentId { get; set; }

        [Display(Name = "Parent")]
        public ChartofAccount Parent { get; set; }

        [Display(Name = "Account Type")]
        [CustomValidation(typeof(ChartofAccount), "CheckAccountType")]
        public Enumerations.ChartOfAccountType ChartOfAccountType { get; set; }

        public string ChartOfAccountTypeDescription
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.ChartOfAccountType), ChartOfAccountType) ? EnumHelper.GetDescription((Enumerations.ChartOfAccountType)ChartOfAccountType) : string.Empty;
            }
        }      

        [Display(Name = "Account Code")]
        [Index("AccountCode", IsUnique = true)]
        [CustomValidation(typeof(ChartofAccount), "CheckAccountCode", ErrorMessage = "Account Code must be equal to 7 digits")]
        public int AccountCode { get; set; }

        [Display(Name="Account Name")]
        [MaxLength(100)]
        public string AccountName { get; set; }

        [Display(Name = "Account Category")]
        [CustomValidation(typeof(ChartofAccount), "CheckAccountCategory")]
        public int ChartOfAccountCategory { get; set; }

        public int Depth { get; set; }

        public string ChartOfAccountCategoryDescription
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.ChartOfAccountCategory), ChartOfAccountCategory) ? EnumHelper.GetDescription((Enumerations.ChartOfAccountCategory)ChartOfAccountCategory) : string.Empty;
            }
        }

        public string ChartofAccountDescription
        {
            get
            {
                return AccountName + " - " + AccountCode;
            }
        }

        public string ChartOfAccountDescriptionByCategory
        {
            get
            {
                return AccountName + ", Category => " + ChartOfAccountCategoryDescription + ", Account Code => "+ ChartOfAccountType + "-" + AccountCode;
            }
        }

        [Display(Name="Is Enabled")]
        [Index("IsEnabled")]
        public bool IsActive { get; set; }

        [Display(Name = "Commands")]
        [NotMapped]
        public string Commands { get; set; }

        public static ValidationResult CheckAccountType(string value, ValidationContext context)
        {
            var bindingModel = context.ObjectInstance as ChartofAccount;
            if (bindingModel == null)
                throw new NotSupportedException("ObjectInstance must be ChartOfAccountDTO");

            Enumerations.ChartOfAccountType accountType = Enumerations.ChartOfAccountType.Asset;
            if (string.IsNullOrWhiteSpace(value) || !Enum.TryParse<Enumerations.ChartOfAccountType>(value, out accountType))
                return new ValidationResult("UnKnown Account Type.");

            return ValidationResult.Success;
        }

        public static ValidationResult CheckAccountCategory(string value, ValidationContext context)
        {
            var bindingModel = context.ObjectInstance as ChartofAccount;
            if (bindingModel == null)
                throw new NotSupportedException("ObjectInstance must be ChartOfAccountDTO");

            Enumerations.ChartOfAccountCategory accountCategory = Enumerations.ChartOfAccountCategory.DetailAccount;
            if (string.IsNullOrWhiteSpace(value) || !Enum.TryParse<Enumerations.ChartOfAccountCategory>(value, out accountCategory))
                return new ValidationResult("UnKnown Account Category.");

            return ValidationResult.Success;
        }

        public static ValidationResult CheckAccountCode(string value, ValidationContext context)
        {
            var bindingModel = context.ObjectInstance as ChartofAccount;
            if (bindingModel == null)
                throw new NotSupportedException("ObjectInstance must be ChartOfAccountDTO");

            if (string.IsNullOrWhiteSpace(value) || value.Length != string.Format("{0}", (int)Enumerations.ChartOfAccountType.Asset).Length)
                return new ValidationResult(string.Format("Account Code must be equal to {0} digits.", string.Format("{0}", (int)Enumerations.ChartOfAccountType.Asset).Length));

            return ValidationResult.Success;
        }
    }
}

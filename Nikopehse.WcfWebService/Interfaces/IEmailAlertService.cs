﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.WcfWebService.Interfaces
{
    public interface IEmailAlertService
    {
        IList<EmailAlert> FindEmailAlertByDRLStatus(int status);
    }
}

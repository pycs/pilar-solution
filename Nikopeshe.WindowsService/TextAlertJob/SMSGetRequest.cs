﻿using Nikopeshe.Data;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Nikopeshe.WindowsService.TextAlertJob
{
    class SMSGetRequest : IJob
    {
        string AfricasTakingAPIKey = ConfigurationManager.AppSettings["AfricasTalkingAPIKey"].ToString();
        string AfricasTakingUserName = ConfigurationManager.AppSettings["AfricasTalkingUsername"].ToString();
        string AfricasTakingAPIEndPoint = ConfigurationManager.AppSettings["SMS_URLString"].ToString();
        private JavaScriptSerializer serializer;
        DataContext datacontext = new DataContext();

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) =>
                {
                    return true;
                };

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(AfricasTakingAPIEndPoint);
                webRequest.Method = "GET";
                webRequest.Accept = "application/json";
                webRequest.Headers.Add("apiKey", AfricasTakingAPIKey);

                HttpWebResponse httpResponse = (HttpWebResponse)webRequest.GetResponse();
                var responseCode = (int)httpResponse.StatusCode;
                StreamReader webpageReader = new StreamReader(httpResponse.GetResponseStream());

                string response = webpageReader.ReadToEnd();

                if (responseCode == (int)HttpStatusCode.OK)
                {
                    dynamic json = serializer.DeserializeObject(response);
                    
                }


            }

            catch (Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }
    }
}

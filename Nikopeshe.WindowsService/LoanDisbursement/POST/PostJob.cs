﻿using Newtonsoft.Json;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.WindowsService.LoanDisbursement.POST
{
    public class PostJob : IJob
    {        
        private MessageQueue _messageQueue;
        LoanDisbursementManager manager = new LoanDisbursementManager();
        string CompanyId = ConfigurationManager.AppSettings["CompanyId"].ToString();
        string spsusername = ConfigurationManager.AppSettings["SPSUsername"].ToString();
        string spsapiendpoint = ConfigurationManager.AppSettings["SPSAPIEndPoint"].ToString();
        string spspassword = ConfigurationManager.AppSettings["SPSPassword"].ToString();
        private DataContext datacontext = new DataContext();
        Guid EntityId = Guid.Empty;
        List<Entity.DataModels.LoanRequest> findloanrequestbyprocessingstatus = new List<Entity.DataModels.LoanRequest>();
        Order order = new Order();
        public void Execute(IJobExecutionContext context)
         {
            try
            {
                findloanrequestbyprocessingstatus = datacontext.LoanRequest.Where(x => x.Status == (int)Enumerations.LoanStatus.Disbursed && x.ProcessingStatus == (int)Enumerations.ProcessingStatus.Disburse && x.SPSStatus == (int)Enumerations.EFTStatus.Pending).Take(10).ToList();
                if(findloanrequestbyprocessingstatus != null && findloanrequestbyprocessingstatus.Any())
                {
                    foreach (var pendingloanrequest in findloanrequestbyprocessingstatus)
                    {
                        EntityId = pendingloanrequest.Id;
                        if (pendingloanrequest.Type == Enumerations.EFTType.AccountToMobile)
                        {
                            order = new Order()
                            {
                                Type = (int)Enumerations.EFTType.AccountToMobile,
                                CompanyId = Guid.Parse(CompanyId),
                                Remarks = "Loan Disbursement"
                            };
                            order.OrderLines = new List<OrderLine>();
                            order.OrderLines.Add(new OrderLine
                            {
                                Payee = pendingloanrequest.CustomerAccount.Customer.FullName,
                                PrimaryAccountNumber = pendingloanrequest.PrimaryAccountNumber,
                                Amount = pendingloanrequest.DisbursedAmount,
                                MCCMNC = (int)Enumerations.MobileNetworkCode.KE_Safaricom,
                                Reference = pendingloanrequest.LoanReferenceNumber,
                                SystemTraceAuditNumber = string.Concat(pendingloanrequest.Id)
                            });                            
                        }

                        else if(pendingloanrequest.Type == Enumerations.EFTType.AccountToAccount)
                        {
                            order = new Order()
                            {
                                Type = (int)Enumerations.EFTType.AccountToAccount,
                                CompanyId = Guid.Parse(CompanyId),
                                Remarks = "Loan Disbursement"
                            };

                            order.OrderLines = new List<OrderLine>();

                            order.OrderLines.Add(new OrderLine
                            {
                                Payee = pendingloanrequest.CustomerAccount.Customer.FullName,
                                PrimaryAccountNumber = pendingloanrequest.PrimaryAccountNumber,
                                Amount = pendingloanrequest.DisbursedAmount,
                                BankCode = pendingloanrequest.BankCode,
                                Reference = pendingloanrequest.LoanReferenceNumber,
                                SystemTraceAuditNumber = string.Concat(pendingloanrequest.Id)
                            });
                        }
                        
                        string jsonString = JsonConvert.SerializeObject(order);
                        var jsonData = Encoding.UTF8.GetBytes(jsonString);
                        HttpWebRequest webRequest = WebRequest.Create(spsapiendpoint) as HttpWebRequest;
                        if (webRequest == null)
                            throw new NullReferenceException("the request is not a web request");
                        webRequest.Method = "POST";
                        webRequest.ContentType = "application/json";
                        webRequest.ContentLength = jsonData.Length;

                        webRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(spsusername + ":" + spspassword)));

                        using (Stream requestStream = webRequest.GetRequestStream())
                        {
                            requestStream.Write(jsonData, 0, jsonData.Length);
                            requestStream.Close();
                        }

                        HttpWebResponse webReponse = webRequest.GetResponse() as HttpWebResponse;
                        if (webReponse.StatusCode == HttpStatusCode.OK)
                            using (var receiveStream = webReponse.GetResponseStream())
                            {
                                using (var readstream = new StreamReader(receiveStream, new UTF8Encoding()))
                                {
                                    var findloanrequestbyid = datacontext.LoanRequest.Find(pendingloanrequest.Id);
                                    var entry = datacontext.Entry(findloanrequestbyid);
                                    entry.State = EntityState.Modified;
                                    findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Submitted;
                                    findloanrequestbyid.ThirdPartyTransactionId = readstream.ReadToEnd();
                                    datacontext.SaveChanges();
                                }
                            }
                    } 
                }                               
            }

            catch (WebException ex)
            {
                var response = ex.Response as HttpWebResponse;
                Logger.CreateLog(string.Format("Web Status code: {0}, Record Primary Key: {1}", response.StatusCode, EntityId.ToString()));
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }   
        }
    }
}

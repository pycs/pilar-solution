﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.WindowsService.LoanDisbursement;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.WindowsService.LoanDisbursement
{
    public class LoanDisbursementManager
    {
        private DataContext datacontext = new DataContext();

        string ErrorMessage = string.Empty;

        public IList<Nikopeshe.Entity.DataModels.LoanRequest> FindLoanRequestBySPSStatus()
        {
            var findloanrequestbyprocessingstatus = datacontext.LoanRequest.Where(x => x.Status == (int)Enumerations.LoanStatus.Disbursed && x.ProcessingStatus != (int)Enumerations.ProcessingStatus.Completed && x.SPSStatus != (int)Enumerations.EFTStatus.Completed || x.SPSStatus != (int)Enumerations.EFTStatus.Failed || x.SPSStatus != (int)Enumerations.EFTStatus.Rejected).Take(10);
            //var findloanrequestbyprocessingstatus = datacontext.LoanRequest.Where(x => x.Status == (int)Enumerations.LoanStatus.Disbursed && x.ProcessingStatus != (int)Enumerations.ProcessingStatus.Completed && x.SPSStatus != (int)Enumerations.EFTStatus.Completed || x.SPSStatus != (int)Enumerations.EFTStatus.Failed || x.SPSStatus != (int)Enumerations.EFTStatus.Rejected).Take(10);

            return findloanrequestbyprocessingstatus.ToList();

        }
        
        public IList<Nikopeshe.Entity.DataModels.LoanRequest> FindLoanRequestByProcessingStatus()
        {
            var findloanrequestbyprocessingstatus = datacontext.LoanRequest.Where(x => x.Status == (int)Enumerations.LoanStatus.Disbursed && x.ProcessingStatus == (int)Enumerations.ProcessingStatus.Disburse).Take(10);

            return findloanrequestbyprocessingstatus.ToList();

        }

        public IList<Nikopeshe.Entity.DataModels.LoanRequest> FindLoanRequestByProcessingStatus(int LoanStatus, int ProcessingStatusA, int ProcessingStatusB)
        {
            var findloanrequestbyprocessingstatus = datacontext.LoanRequest.Where((x => x.Status
                == LoanStatus && x.ProcessingStatus == ProcessingStatusA || x.ProcessingStatus == ProcessingStatusB)).Take(20);

            return findloanrequestbyprocessingstatus.ToList();

        }

        public void UpdateLoanRequest(Guid Id, int status)
        {
            try
            {
                var findloanrequestbyid = datacontext.LoanRequest.Find(Id);
                var entry = datacontext.Entry(findloanrequestbyid);
                entry.State = EntityState.Modified;
                findloanrequestbyid.ProcessingStatus = status;
                datacontext.SaveChanges();
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }

                Logger.CreateLog(ErrorMessage + ex.Message + ex.InnerException + ex.StackTrace);
            } 
        }

        public void UpdateLoanRequest(Guid Id, int status, string TransactionId)
        {
            try
            {
                var findloanrequestbyid = datacontext.LoanRequest.Find(Id);
                var entry = datacontext.Entry(findloanrequestbyid);
                entry.State = EntityState.Modified;
                findloanrequestbyid.ProcessingStatus = status;
                findloanrequestbyid.ThirdPartyTransactionId = TransactionId;
                datacontext.SaveChanges();
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }

                Logger.CreateLog(ErrorMessage + ex.Message + ex.InnerException + ex.StackTrace);
            }
        }

        public void UpdateLoanRequest(Guid Id, int status, List<OrderLine> orderline, Order order)
        {
            try
            {
                var findloanrequestbyid = datacontext.LoanRequest.Find(Id);
                var entry = datacontext.Entry(findloanrequestbyid);
                entry.State = EntityState.Modified;                
                findloanrequestbyid.B2MResponseCode = orderline[0].B2MResponseCode;
                findloanrequestbyid.B2MResponseDesc = orderline[0].B2MResponseDesc;
                findloanrequestbyid.B2MResultCode = orderline[0].B2MResultCode;
                findloanrequestbyid.B2MResultDesc = orderline[0].B2MResultDesc;
                findloanrequestbyid.B2MTransactionID = orderline[0].B2MTransactionID;                
                findloanrequestbyid.ChargePaidAccountAvailableFunds = orderline[0].ChargePaidAccountAvailableFunds;
                findloanrequestbyid.IsDelivered = order.IsDelivered;
                findloanrequestbyid.TypeDesc = orderline[0].TypeDesc;
                findloanrequestbyid.MCCMNCDesc = orderline[0].MCCMNCDesc;
                findloanrequestbyid.ProcessingStatus = status;
                findloanrequestbyid.TransactionCreditParty = orderline[0].TransactionCreditParty;
                findloanrequestbyid.TransactionDateTime = orderline[0].TransactionDateTime;                
                findloanrequestbyid.SPSStatus = orderline[0].Status;
                findloanrequestbyid.SPSStatusDesc = orderline[0].StatusDesc;
                findloanrequestbyid.UtilityAccountAvailableFunds = orderline[0].UtilityAccountAvailableFunds;
                findloanrequestbyid.WorkingAccountAvailableFunds = orderline[0].WorkingAccountAvailableFunds;
                if (findloanrequestbyid.B2MResponseCode == "0" && findloanrequestbyid.B2MResultCode == "0")
                {
                    findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Active;
                }

                datacontext.SaveChanges();
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }

                Logger.CreateLog(ErrorMessage + ex.Message + ex.InnerException + ex.StackTrace);
            }
        }
    }
}

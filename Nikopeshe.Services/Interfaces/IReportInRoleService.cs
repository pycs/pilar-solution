﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface IReportInRoleService
    {
        void AddNewReportInRole(ReportInRoles reportInRole, ServiceHeader serviceHeader);

        IList<ReportInRoles> GetAllReportsInRolesByRoleCategory(int RoleCategory);

        IList<ReportInRoles> GetAllReportsInCategory();

        ReportInRoles FindReportInRolesByReportId(Guid ReportId);
    }
}

﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Data.Mappings;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Models;
using System;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Text;
using System.Web.Http;
using System.Web.Security;

namespace Nikopeshe.WebApplication.Controllers.APICotrollers
{    
    public class RegistrationController : ApiController
    {
        internal readonly IEmailAlertService emailalerservice;
        internal readonly ITextAlertService textalertservice;
        internal readonly ICustomerService customerservice;
        internal readonly ICustomerAccountService customeraccountservice;
        internal readonly IUserInfoService userinfoservice;
        internal readonly IBranchService branchservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        private string ApplicationName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        public RegistrationController(IEmailAlertService _emailalerservice, ITextAlertService _textalertservice, 
            ICustomerAccountService _customeraccountservice, ICustomerService _customerservice,
            IUserInfoService _userinfoservice, IBranchService _branchservice)
        {
            if(_emailalerservice == null || _textalertservice == null || _customeraccountservice == null || _customerservice == null || _userinfoservice == null || _branchservice == null)
                throw new ArgumentNullException("null service reference");

            this.emailalerservice = _emailalerservice;
            this.userinfoservice = _userinfoservice;
            this.customeraccountservice = _customeraccountservice;
            this.customerservice = _customerservice;
            this.textalertservice = _textalertservice;
            this.branchservice = _branchservice;
        }

        // POST api/registration
        /// <summary>
        /// This method is used for registration.
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Http Response Message</returns>
        public HttpResponseMessage Post(RegistrationModel registration)
        {
            if (registration == null) throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.Forbidden, Content = new StringContent("POSTed value cannot be null!") });

            //Must validate inputs from the mobile app and api

            if (string.IsNullOrWhiteSpace(registration.FirstName) || string.IsNullOrWhiteSpace(registration.MiddleName) ||
                string.IsNullOrWhiteSpace(registration.LastName) || string.IsNullOrWhiteSpace(registration.MobileNumber) ||
                string.IsNullOrWhiteSpace(registration.IDNumber) || string.IsNullOrWhiteSpace(registration.EmailAddress) ||
                string.IsNullOrWhiteSpace(registration.PIN) || string.IsNullOrWhiteSpace(registration.Occupation) ||
                string.IsNullOrWhiteSpace(registration.IncomeRange) || registration.Gender == 0 || registration.MaritalStatus == 0 || registration.DateofBirth == null)
            {
                throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.Forbidden, Content = new StringContent("Mandatory field(s) cannot be null.") });
            }

            Customer customer = new Customer();
            MembershipCreateStatus status;
            customer.IsEnabled = true;
            var finddefaultbranch = branchservice.FindBranchByBranchCode("000");
            if (finddefaultbranch == null)
            {
                throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.InternalServerError, Content = new StringContent("Failed. Please again later.") });
            }
            string mobilenumber = registration.MobileNumber;
            customer.IsApproved = false;
            customer.CustomerRegistrationType = Enumerations.CustomerRegistrationType.SelfRegistered;
            customer.FirstName = registration.FirstName;
            customer.LastName = registration.LastName;
            customer.IDNumber = registration.IDNumber;
            customer.PIN = registration.PIN;
            customer.IncomeRange = registration.IncomeRange;
            customer.MiddleName = registration.MobileNumber;
            customer.MobileNumber = registration.MobileNumber;
            customer.EmailAddress = registration.EmailAddress;
            customer.Occupation = registration.Occupation;
            customer.Gender = (Enumerations.Gender)registration.Gender;
            customer.MaritalStatus = (Enumerations.MaritalStatus)registration.MaritalStatus;
            customer.BranchId = finddefaultbranch.Id;
            customer.CustomerType = Enumerations.CustomerTypes.Individual;
            customer.RegisteredBy = registration.MobileNumber;
            //customer.MobileNumber = mobilenumber.Substring(1);
            customer.ApprovalCode = GenerateRandomCustomerApprovalCode(6);
            bool checkduplicatecustomer;
            bool iscustomerunderage;
            string nextcustomernumber = customerservice.NextCustomerNumber();
            customer.CustomerNumber = nextcustomernumber;
            string nextcustomeraccountnumber = customerservice.NextCustomerSavingAccountNumber(nextcustomernumber);
            CustomersAccount customersaccount = new CustomersAccount();
            DateTime dDate;

            if (DateTime.TryParseExact(registration.DateofBirth, "dd/MM/yyyy", null, DateTimeStyles.None, out dDate) == true)
            {
                customer.DateofBirth = DateTime.ParseExact(registration.DateofBirth, "dd/MM/yyyy", null);
            }
            if (ModelState.IsValid)
            {

                customersaccount.CustomerId = customer.Id;
                customersaccount.AccountType = (int)Enumerations.ProductCode.Savings;
                customersaccount.AccountNumber = nextcustomeraccountnumber;
                customersaccount.IsActive = true;
                checkduplicatecustomer = customerservice.CheckDuplicateCustomer(registration.MobileNumber, registration.IDNumber);
                iscustomerunderage = customerservice.ValidateCustomerAge(customer.DateofBirth);
                if (checkduplicatecustomer)
                {
                    throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.Forbidden, Content = new StringContent(string.Format("Customer with Mobile Number {0} or Id Number {1} already exists", registration.MobileNumber, registration.IDNumber)) });
                }
                if (!iscustomerunderage)
                {
                    throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.Forbidden, Content = new StringContent(string.Format("You must be 18 years and above.")) });
                }

                try
                {                    
                    var findmembershipuser = Membership.GetUser(registration.MobileNumber);
                    if (checkduplicatecustomer == false && findmembershipuser == null)
                    {
                        using (var datacontext = new DataContext())
                        {
                            using (var transaction = datacontext.Database.BeginTransaction())
                            {
                                try
                                {

                                    MembershipUser createuser = Membership.CreateUser(registration.MobileNumber, registration.PIN, registration.EmailAddress, DefaultSettings.Instance.PasswordQuestion, DefaultSettings.Instance.PasswordAnswer, true, out status);
                                    string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                                    string loginurl = "<a href='" + mvcclientendposint + "'/Account/Login> click here</a>";
                                    switch (status)
                                    {
                                        case MembershipCreateStatus.Success:
                                            ModelState.AddModelError(string.Empty, "User Account created successfully");
                                            Roles.AddUserToRole(registration.MobileNumber, "Customer");
                                            customerservice.AddNewCustomer(customer, serviceHeader);
                                            customeraccountservice.AddNewCustomersAccount(customersaccount, serviceHeader);
                                            //Create email alert and insert in the database
                                            StringBuilder bodyBuilder = new StringBuilder();
                                            bodyBuilder.Append("Dear Sir/Madam,");
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.Append(string.Format("Welcome to {0}. Below you will find your login details.", DefaultSettings.Instance.ApplicationDisplayName));
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.Append("Username: " + registration.MobileNumber);
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.Append("Password: " + registration.PIN);
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.Append("Authorization Code : " + customer.ApprovalCode);
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.Append(string.Format("Continue to approve your account and borrow {0}.", loginurl));
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.AppendLine("<br />");

                                            var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
                                            bodyBuilder.Append(emailSignature);

                                            var emailAlertDTO = new EmailAlert
                                            {
                                                MailMessageFrom = DefaultSettings.Instance.Email,
                                                MailMessageTo = string.Format("{0}", customer.EmailAddress),
                                                MailMessageSubject = string.Format("Login Details - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                                MailMessageBody = string.Format("{0}", bodyBuilder),
                                                MailMessageIsBodyHtml = true,
                                                MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                MailMessageSecurityCritical = false,
                                                CreatedBy = "System",
                                                MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                            };

                                            emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                                            NewTextAlert(customer.FullName, customer.MobileNumber, customer.MobileNumber, customer.PIN, customer.ApprovalCode);                                            

                                            break;
                                        case MembershipCreateStatus.DuplicateUserName:
                                            ModelState.AddModelError(string.Empty, "There already exists a user with this username.");
                                            break;

                                        case MembershipCreateStatus.DuplicateEmail:
                                            ModelState.AddModelError(string.Empty, "There already exists a user with this email address.");
                                            break;
                                        case MembershipCreateStatus.InvalidEmail:
                                            ModelState.AddModelError(string.Empty, "The email address you provided in invalid.");
                                            break;
                                        case MembershipCreateStatus.InvalidAnswer:
                                            ModelState.AddModelError(string.Empty, "The security answer is invalid.");
                                            break;
                                        case MembershipCreateStatus.InvalidPassword:
                                            ModelState.AddModelError(string.Empty, "The password you provided is invalid. It must be seven characters long and have at least one non-alphanumeric character.");

                                            break;
                                        default:
                                            ModelState.AddModelError(string.Empty, status.ToString());
                                            break;
                                    }
                                    transaction.Commit();
                                }

                                catch (Exception ex)
                                {
                                    Roles.RemoveUserFromRole(customer.MobileNumber, "Customer");
                                    Membership.DeleteUser(customer.MobileNumber);
                                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                    transaction.Rollback();
                                }
                            }
                        }
                    }                    
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    HttpResponseMessage ex_response = Request.CreateResponse(HttpStatusCode.Forbidden);
                    ex_response.Content = new StringContent(string.Format("Internal Server Error."));
                    ex_response.ReasonPhrase = "Failed.";
                    return ex_response;
                }                
            }

            else
            {
                throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.Forbidden, Content = new StringContent("POSTed value(s) cannot be null.") });
            }

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(string.Format("Account Created Successfully."));
            response.ReasonPhrase = "SUCCESS.";
            return response;
        }

        /// <summary>
        /// Random approval code generator
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private string GenerateRandomCustomerApprovalCode(int length)
        {
            string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[length];
            Random rd = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }

        private void NewTextAlert(string RecipientName, string RecepientNumber, string Username, string Password, string AuthorizationCode)
        {
            //Create email alert and insert in the database
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append(string.Format("Dear {0} ", RecipientName));
            bodyBuilder.Append(string.Format("Welcome to {0}. Your Username - {1}, Password - {2}, AuthorizationCode - {3} ", ApplicationName, Username, Password, AuthorizationCode));
            bodyBuilder.Append("Go ahead, login, and apply for a loan.");
            var textalert = new TextAlert
            {
                TextMessageRecipient = string.Format("{0}", RecepientNumber),
                TextMessageBody = string.Format("{0}", bodyBuilder),
                MaskedTextMessageBody = "",
                TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                TextMessageSecurityCritical = false,
                CreatedBy = User.Identity.Name,
                TextMessageSendRetry = 3
            };

            textalertservice.AddNewTextAlert(textalert, serviceHeader);
        }
    }
}
﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class SystemGeneralLedgerAccountMappingService : ISystemGeneralLedgerAccountMappingService
    {
        private readonly IRepository<SystemGeneralLedgerAccountMapping> repository;

        string ErrorMessage = string.Empty;

        public SystemGeneralLedgerAccountMappingService(IRepository<SystemGeneralLedgerAccountMapping> _repository)
        {
            if(_repository == null)
                throw new ArgumentNullException("null repository");

            this.repository = _repository;
        }

        public void AddNewSystemGeneralLedgerAccountMapping(SystemGeneralLedgerAccountMapping systemgeneralledgemapping, ServiceHeader serviceHeader)
        {
            try
            {
                if (systemgeneralledgemapping == null)
                {
                    throw new ArgumentNullException("entity cannot be null");
                }
                var getallglmappings = repository.GetAll(IsolationLevel.ReadCommitted);
                foreach(var glmapping in getallglmappings)
                {
                    if(glmapping.SystemGeneralLedgerAccountCode == systemgeneralledgemapping.SystemGeneralLedgerAccountCode)
                    {
                        throw new ArgumentNullException("System GL Account Code has already been mapped.");
                    }
                }
                repository.Insert(systemgeneralledgemapping, serviceHeader);
            }

            catch(DbEntityValidationException ex)
            {
                foreach(var ValidationErrors in ex.EntityValidationErrors)
                {
                    foreach(var validationError in ValidationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} ErroMessage: {1}", validationError.PropertyName, validationError.ErrorMessage);

                    }
                }
                throw new Exception(ErrorMessage, ex);
            }            
        }

        public void UpdateSystemGeneralLedgerAccountMapping(SystemGeneralLedgerAccountMapping systemgeneralledgemapping, ServiceHeader serviceHeader)
        {
            try
            {
                if(systemgeneralledgemapping == null)
                    throw new ArgumentNullException("Entity cannot be null");

                repository.Update(systemgeneralledgemapping, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var ValidationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in ValidationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} ErroMessage: {1}", validationError.PropertyName, validationError.ErrorMessage);

                    }
                }
                throw new Exception(ErrorMessage, ex);
            }   
        }

        public SystemGeneralLedgerAccountMapping FindSystemGeneralLedgerAccountMappingsByGLCode(int GLCode)
        {
            if (string.IsNullOrWhiteSpace(string.Concat(GLCode)))
                throw new ArgumentNullException("GLCode cannot be null");

            var findsystemGLMapping = repository.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.SystemGeneralLedgerAccountCode == (Enumerations.SystemGeneralLedgerAccountCode)GLCode).FirstOrDefault();

            return findsystemGLMapping;
        }

        public SystemGeneralLedgerAccountMapping FindSystemGeneralLedgerAccountMappingsById(Guid id)
        {
            var findsystemGLbyId = repository.GetById(id);

            return findsystemGLbyId;
        }

        public IList<SystemGeneralLedgerAccountMapping> GetAllSystemGeneralLedgerAccountMappings()
        {
            var findall = repository.GetAll(IsolationLevel.ReadUncommitted);

            return findall.ToList();
        }
    }
}

﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.ArrearsCalculator
{
    public interface ICalculator
    {
        List<SPResult> SPResultsList();

        Guid FindCustomerSavingsAccount(Guid LoanRequestId);
    }
}

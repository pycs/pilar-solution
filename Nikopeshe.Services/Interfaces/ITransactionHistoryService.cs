﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;

namespace Nikopeshe.Services.Interfaces
{
    public interface ITransactionHistoryService
    {
        void AddNewTransactionHistory(TransactionHistory loanhistory, ServiceHeader serviceHeader);

        void UpdateTransactionHistory(TransactionHistory loanhistory, ServiceHeader serviceHeader);

        IList<TransactionHistory> GetAllLoanTransactionsHistory();

        TransactionHistory GetAllLoanTransactionsHistoryById(Guid Id);

        IList<TransactionHistory> GetAllCustomerTransationHistoryByCustomerId(Guid CustomerId);        
    }
}

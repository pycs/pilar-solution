﻿using Nikopeshe.WebApplication.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WebApiThrottle;

namespace Nikopeshe.WebApplication
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //config.MessageHandlers.Add(new CustomThrottlingHandler()
            //{
            //    Policy = ThrottlePolicy.FromStore(new PolicyConfigurationProvider()),
            //    Repository = new CacheRepository(),
            //    Logger = new EntLibThrottleLogger()            
            //});            
        }
    }
}

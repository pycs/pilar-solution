﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;

namespace Nikopeshe.Services.InterfaceImplementations
{
    [Export(typeof(IEmailAlertService))]
    public class EmailAlertService : IEmailAlertService
    {
        readonly IRepository<EmailAlert> emailalertrepository;

        string errorMessage = string.Empty;
        
        public EmailAlertService(IRepository<EmailAlert> _emailalertrepository)
        {
            if(_emailalertrepository == null)
                throw new ArgumentNullException("null repository");

            this.emailalertrepository = _emailalertrepository;
        }

        public void AddNewEmailAlert(EmailAlert emailalert, ServiceHeader serviceHeader)
        {            
            try
            {
                if (emailalert == null)
                    throw new ArgumentNullException("entity cannot be null");

                emailalertrepository.Insert(emailalert, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(errorMessage, ex);
            }            
        }

        public IList<EmailAlert> FindEmailAlertByDRLStatus(int status)
        {
            if (string.IsNullOrWhiteSpace(status.ToString()))
                throw new ArgumentNullException("null entity");

            var results = emailalertrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.MailMessageDLRStatus == status);

            var count = results.Count();

            return results.ToList();
        }

        public void UpdateEmailAlertDRLStatus(Guid Id, int Status, ServiceHeader serviceHeader)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Id.ToString()) || string.IsNullOrWhiteSpace(Status.ToString()))

                    throw new ArgumentNullException("empty value(s)");

                var findrecordbyId = emailalertrepository.GetById(Id);

                findrecordbyId.MailMessageDLRStatus = Status;

                emailalertrepository.Update(findrecordbyId, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(errorMessage, ex);
            }    
        }
    }
}

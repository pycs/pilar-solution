﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class ProfessionService : IProfessionService
    {
        private IRepository<Profession> professionRepository;

        private string ErrorMessage = string.Empty;

        public ProfessionService(IRepository<Profession> _professionRepository)
        {
            this.professionRepository = _professionRepository;
        }

        public void AddNewProfessio(Profession profession, ServiceHeader serviceHeader)
        {
            if (profession == null)
                throw new ArgumentNullException("entity cannot be null");

            try
            {
                professionRepository.Insert(profession, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public void UpdateProfession(Profession profession, ServiceHeader serviceHeader)
        {
            if (profession == null)
                throw new ArgumentNullException("entity cannot be null");

            try
            {
                professionRepository.Update(profession, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public void DeleteProfession(Profession profession, ServiceHeader serviceHeader)
        {
            if (profession == null)
                throw new ArgumentNullException("entity cannot be null");

            try
            {
                professionRepository.Delete(profession, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public IList<Profession> GetAllProfessions(IsolationLevel isolationLevel)
        {
            return professionRepository.GetAll(isolationLevel).ToList();
        }

        public Profession FindProfessionById(Guid Id)
        {
            var findById = professionRepository.GetById(Id);

            return findById;
        }
    }
}

﻿using Nikopeshe.WebApplication.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using LazyCache;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Caching;
using System.Web;
using System.Web.Http;
using System.Web.Http.Routing;
using WebApiThrottle;
using Nikopeshe.WebApplication.App_Start;
using Nikopeshe.Services.InterfaceImplementations;
using Nikopeshe.Services.Interfaces;

namespace Nikopeshe.WebApplication
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var container = new UnityContainer();

            container.RegisterType<IAppCache, CachingService>(new PerResolveLifetimeManager(), new InjectionConstructor(MemoryCache.Default));
            container.RegisterType<IMessageQueueService, MessageQueueService>();
            config.DependencyResolver = new UnityWebApiResolver(container);

            //config.MessageHandlers.Add(new CustomThrottlingHandler()
            //{
            //    Policy = ThrottlePolicy.FromStore(new PolicyConfigurationProvider()),
            //    Repository = new CacheRepository(),
            //    Logger = new EntLibThrottleLogger()            
            //});            
        }
    }
}

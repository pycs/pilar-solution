﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class DashBoardService : IDashBoardService
    {
        readonly IRepository<DashBoard> dashboardrepository;

        public DashBoardService(IRepository<DashBoard> _dashboardrepository)
        {
            if (_dashboardrepository == null)
                throw new ArgumentNullException("null repository");

            this.dashboardrepository = _dashboardrepository;
        }

        public void AddNewDashBoard(DashBoard dashboard, ServiceHeader serviceHeader)
        {
            if (dashboard == null)
                throw new ArgumentNullException("null entity");

            dashboardrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.IsPublished == true);            
            dashboardrepository.Insert(dashboard, serviceHeader);
        }

        public DashBoard FindPublishedDashboard()
        {
            var findpublisheddashboard = dashboardrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.IsPublished == true);

            return findpublisheddashboard.FirstOrDefault();
        }

        public IList<DashBoard> FindPublishedDashBoardList()
        {
            var findpublisheddashboard = dashboardrepository.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.IsPublished == true);

            return findpublisheddashboard.ToList();
        }
    }
}

﻿using Nikopeshe.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.ServiceModel;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Nikopeshe.WebApplication.Attributes
{
    public static class CustomHeaderUtility
    {
        public static ServiceHeader ReadHeader(OperationContext operationContext)
        {
            string CurrentUser = string.Empty;
            if(!string.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name.ToString()))
            {
                CurrentUser = HttpContext.Current.User.Identity.Name.ToString();
            }

            string remoteIPAddress = string.Empty;
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    remoteIPAddress = addresses[0];
                }                
            }
            else
            {
                remoteIPAddress = context.Request.ServerVariables["REMOTE_ADDR"];
            }
            
            var customData = new ServiceHeader 
            { 
                ApplicationUserName = CurrentUser, 
                EnvironmentDomainName = Environment.UserDomainName.ToString(), 
                EnvironmentMachineName = Environment.MachineName.ToString(), 
                EnvironmentOSVersion = Environment.OSVersion.ToString(), 
                EnvironmentUserName = Environment.UserName,    
                RemoteIPAddress = remoteIPAddress,
                
            };

            if (operationContext == null)
                return customData;

            var messageHeaders = operationContext.IncomingMessageHeaders;

            var headerPosition = messageHeaders.FindHeader(CustomHeaderNames.CustomHeaderName, CustomHeaderNames.CustomHeaderNamespace);

            if (headerPosition == -1)
                return customData;

            var content = messageHeaders.GetHeader<XmlNode[]>(headerPosition);

            var text = content[0].InnerText;

            var deserializer = new XmlSerializer(typeof(ServiceHeader));

            var textReader = new StringReader(text);

            customData = (ServiceHeader)deserializer.Deserialize(textReader);

            textReader.Close();

            return customData;
        }
    }
}
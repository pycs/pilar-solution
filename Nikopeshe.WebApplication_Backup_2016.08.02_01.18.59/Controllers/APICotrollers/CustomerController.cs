﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using System;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.Web.Http;
using System.Web.Security;

namespace Nikopeshe.WebApplication.Controllers.APICotrollers
{
    public class CustomerController : ApiController
    {
        internal readonly ICustomerService customerService;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
       
        public CustomerController(ICustomerService _customerService)
        {
            if(_customerService == null)
                throw new ArgumentNullException("null service reference");

            this.customerService = _customerService;
        }     

        public HttpResponseMessage Post(Customer customer)
        {
            customer.DateofBirth = DateTime.Now;

            bool duplicatecustomer;

            MembershipCreateStatus status;

            if(ModelState.IsValid)
            {
                duplicatecustomer = customerService.CheckDuplicateCustomer(customer.MobileNumber, customer.IDNumber);
                try
                {
                    if(duplicatecustomer)
                    {
                        var duplicatecustomerdetected = new HttpResponseMessage(HttpStatusCode.PreconditionFailed)
                        {
                            Content = new StringContent(string.Format("Customer with Mobile Number {0} or Id Number {1} already exists", customer.MobileNumber, customer.IDNumber)),
                            ReasonPhrase = "Failed"
                        };

                        return duplicatecustomerdetected;
                    }

                    customer.IsEnabled = true;
                    MembershipUser createuser = Membership.CreateUser(customer.MobileNumber, customer.PIN, customer.EmailAddress, "Where were you when you first heard about 9/11?", "School", true, out status);
                    customerService.AddNewCustomer(customer, serviceHeader);
                    switch (status)
                    {
                        case MembershipCreateStatus.Success:
                            ModelState.AddModelError(string.Empty, "User Account created successfully");
                            break;
                        case MembershipCreateStatus.DuplicateUserName:
                            ModelState.AddModelError(string.Empty, "There already exists a user with this username.");
                            break;

                        case MembershipCreateStatus.DuplicateEmail:
                            ModelState.AddModelError(string.Empty, "There already exists a user with this email address.");
                            break;
                        case MembershipCreateStatus.InvalidEmail:
                            ModelState.AddModelError(string.Empty, "There email address you provided in invalid.");
                            break;
                        case MembershipCreateStatus.InvalidAnswer:
                            ModelState.AddModelError(string.Empty, "There security answer was invalid.");
                            break;
                        case MembershipCreateStatus.InvalidPassword:
                            ModelState.AddModelError(string.Empty, "The password you provided is invalid. It must be seven characters long and have at least one non-alphanumeric character.");

                            break;
                        default:
                            ModelState.AddModelError(string.Empty, status.ToString());
                            break;
                    }
                }

                catch
                {
                    var error = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(string.Format("Validation error(s) occurred..")),
                        ReasonPhrase = "Failed."
                    };
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", error);
                    throw new HttpResponseException(error);
                }
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, status.ToString());                
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }
    }
}
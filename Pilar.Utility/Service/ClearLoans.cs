﻿using Crosscutting.NetFramework.Logging;
using Infrastructure.Crosscutting.NetFramework;
using Pilar.Services.Services;
using Pilar.Utility.Configuration;
using Quartz;
using Quartz.Impl;
using System;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Data.Entity;
using System.Linq;

namespace Pilar.Utility.Service
{
    [Export(typeof(IPlugin))]
    public class ClearLoans : IPlugin
    {
        private readonly IScheduler _scheduler;

        public ClearLoans()
        {
            // Get a reference to the scheduler
            var sf = new StdSchedulerFactory();

            // Get an instance of the Quartz.Net scheduler
            _scheduler = sf.GetScheduler();
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{A8B2E70A-725A-4BFF-9728-D51CA033825F}"); }
        }

        public string Description
        {
            get { return "CLEANLOANS"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                // Start the scheduler if its in standby
                if (!_scheduler.IsStarted)
                    _scheduler.Start();

                // Define the Job to be scheduled
                var job = JobBuilder.Create<ClearLoanJob>()
                    .WithIdentity("UtilityQueueingJob", "Pilar")
                    .RequestRecovery()
                    .Build();

                // Associate a trigger with the Job
                var cronExpression = ConfigurationManager.AppSettings["UtilityJobCronExpression"];
                var trigger = (ICronTrigger)TriggerBuilder.Create()
                    .WithIdentity("UtilityQueueingJob", "Pilar")
                    .WithCronSchedule(cronExpression)
                    .StartAt(DateTime.UtcNow)
                    .WithPriority(1)
                    .Build();

                // Validate that the job doesn't already exist
                if (_scheduler.CheckExists(new JobKey("UtilityQueueingJob", "Pilar")))
                {
                    _scheduler.DeleteJob(new JobKey("UtilityQueueingJob", "Pilar"));
                }

                var schedule = _scheduler.ScheduleJob(job, trigger);

#if DEBUG
                //ExceptionLoggerFactory.CreateLog("{0}->DoWork..." +  Description);
#endif
            }
            catch (Exception ex)
            {
                //ExceptionLoggerFactory.CreateLog("{0}->DoWork..." + ex + Description);
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        public void Exit()
        {
            _scheduler.Shutdown();
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Entity.DataModels
{
    public class Profession : BaseEntity
    {
        public string ProfessionName { get; set; }

        public string Description { get; set; }
    }
}

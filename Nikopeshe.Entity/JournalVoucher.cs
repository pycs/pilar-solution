﻿using Nikopeshe.Entity.DataModels;
using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;

namespace Nikopeshe.Entity
{
    public class JournalVoucher
    {
        [Required]
        public int TransactionType { get; set; }
        [Required]
        public Guid ReturnValue1 { get; set; }
        [Required]
        public Guid ReturnValue2 { get; set; }
        [Required]
        public int TransactionType2 { get; set; }
        [Required]
        public string PrimaryDescription { get; set; }
        [Required]
        public string SecondaryDescription { get; set; }
        [Required]
        public decimal PrincipalAmount { get; set; }        
    }

    public class Journal
    {
        public Guid C2BTransactionId { get; set; }
        public decimal TransactionAmount { get; set; }
        public int C2BStatus {get;set;}
        public Guid CustomerId {get;set;}
        public string SuspenseAccountName {get;set;}
        public Guid SuspenseAccountChartofAccountId {get;set;}
        public string MpesaReceiptCode{get;set;}
        public string BranchName {get;set;}
        public string CustomerName { get; set; }
        public string C2BStatusDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.C2BTransactionStatus), C2BStatus) ? EnumHelper.GetDescription((Enumerations.C2BTransactionStatus)C2BStatus) : string.Empty;
            }
        }

        public string CustomerMobileNumber { get; set; }

        public string CustomerNameCustomerMobileNumber
        {
            get
            {
                return CustomerName + " => " + CustomerMobileNumber;
            }
        }
    }
}

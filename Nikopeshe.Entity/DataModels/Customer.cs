﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    [Serializable]    
    public class Customer : BaseEntity
    {        
        [Display(Name="Customer Type")]        
        public Enumerations.CustomerTypes CustomerType { get; set; }

        public string CustomerTypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.CustomerTypes), CustomerType) ? EnumHelper.GetDescription((Enumerations.CustomerTypes)CustomerType) : string.Empty;
            }
        }

        [Range(1, int.MaxValue, ErrorMessage = "Title cannot be null.")]
        public Enumerations.Title Title { get; set; }

        public string TitleDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.Title), Title) ? EnumHelper.GetDescription((Enumerations.Title)Title) : string.Empty;
            }
        }

        [Display(Name = "Group Role")]
        public Enumerations.GroupRoles GroupRole { get; set; }

        public string GroupRoleDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.GroupRoles), GroupRole) ? EnumHelper.GetDescription((Enumerations.GroupRoles)GroupRole) : string.Empty;
            }
        }

        [Required]
        [Index]
        [Display(Name="Branch Name / County Name")]
        public Guid BranchId { get; set; }

        public virtual Branch Branch { get; set; }

        [Display(Name = "Customer Number")]        
        [Column(TypeName = "VARCHAR")]
        [Index("CustomerNumber", IsUnique = true)]
        [MaxLength(20)]
        public string CustomerNumber { get; set; }
        
        [Required]
        [Display(Name="First Name")]
        [MaxLength(100)]
        public string FirstName { get; set; }
        
        [MaxLength(100)]
        [Display(Name ="Middle Name")]
        public string MiddleName { get; set; }

        [Required]
        [MaxLength(100)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name= "Full Name")]
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public string FullNamemobileNumber
        {
            get
            {
                return FullName + " =>" + MobileNumber;
            }
        }

        public string MobileNumberFullName
        {
            get
            {
                return MobileNumber + " =>" + FullName;
            }
        }
       
        [Column(TypeName = "VARCHAR")]
        [StringLength(12, MinimumLength = 12)]        
        [Display(Name = "Mobile Number(2547********)")]
        [RegularExpression(@"^254(?:[0-9]??){6,14}[0-9]$", ErrorMessage = "The mobile number should start with country code 254 followed by phone number")]        
        public string MobileNumber { get; set; }

        [Display(Name ="ID Number")]  
        [Column(TypeName = "VARCHAR")]
        [Index("Id_Number")]
        public string IDNumber { get; set; }

        [Display(Name="KRA PIN")]
        [MaxLength(20)]
        public string KRAPIN { get; set; }

        [Display(Name = "Email Address")]        
        [Column(TypeName = "VARCHAR")]
        [Index]
        [MaxLength(100)]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [Display(Name="P.O. Box Number")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "P.O Box Number Code. Must be 0-9")]
        public string PoBoxNumber { get; set; }

        [Display(Name = "Postal Code")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Invalid Postal Code. Must be 0-9")]
        public string PostalCode { get; set; }

        [Required]
        [Display(Name="Date of Birth")]
        public DateTime DateofBirth { get; set; }

        [Display(Name="Dealing In")]        
        [MaxLength(100)]
        [Required]
        public string Occupation { get; set; }

        [Display(Name = "Income Range per month")]        
        [MaxLength(100)]
        public string IncomeRange { get; set; }

        /// <summary>
        /// AKA Password
        /// </summary>
        /// 
        
        public string PIN { get; set; }      

        [Column(TypeName = "VARCHAR")]
        [Index("Approval_Code")]
        [MaxLength(20)]
        public string ApprovalCode { get; set; }

        [Index("IsEnabled")]
        public bool IsEnabled { get; set; }

        [Index]
        [Display(Name = "Enable Email Alerts")]
        public bool EmailAlertsEnabled { get; set; }

        [Index]
        [Display(Name = "Enable SMS Alerts")]
        public bool SMSAlertsEnabled { get; set; }

        [NotMapped]
        public string Commands { get; set; }

        [Display(Name ="Is Approved?")]
        public bool IsApproved { get; set; }

        public Enumerations.CustomerRegistrationType CustomerRegistrationType { get; set; }

        public string CustomerRegistrationTypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.CustomerRegistrationType), CustomerRegistrationType) ? EnumHelper.GetDescription((Enumerations.CustomerRegistrationType)CustomerRegistrationType) : string.Empty;
            }
        }

        [Range(1, int.MaxValue, ErrorMessage = "Marital Status cannot be null.")]
        public Enumerations.MaritalStatus MaritalStatus { get; set; }

        public string MaritalStatusDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.MaritalStatus), MaritalStatus) ? EnumHelper.GetDescription((Enumerations.MaritalStatus)MaritalStatus) : string.Empty;
            }
        }

        [Range(1, int.MaxValue, ErrorMessage = "Gender cannot be null.")]
        public Enumerations.Gender Gender { get; set; }

        public string GenderDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.Gender), Gender) ? EnumHelper.GetDescription((Enumerations.Gender)Gender) : string.Empty;
            }
        }

        public string RegisteredBy { get; set; }

        public decimal PreQualifiedAmount { get; set; }

        [Display(Name="Charge Joining Fee")]
        public bool JoiningFee { get; set; }

        [Display(Name = "Loan / Program Officer")]
        public Guid? RelationshipManagerId { get; set; }

        public virtual RelationshipManager RelationshipManager { get; set; }

        [Display(Name = "Partner")]
        public Guid? PartnerId { get; set; }

        public virtual Partner Partner { get; set; }

        [Display(Name="Profession")]
        public Guid? ProfessionId { get; set; }

        public virtual Profession Profession { get; set; }     

        [Display(Name = "Spouse Name")]
        public string SpouseName { get; set; }

        [Display(Name = "Spouse ID No.")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Invalid Spouse ID Number. Must be 0-9")]
        public string SpouseIdNumber { get; set; }

        [RegularExpression(@"^254(?:[0-9]??){6,14}[0-9]$", ErrorMessage = "The mobile number should start with country code 254 followed by phone number")]
        [Display(Name = "Spouse Tel. No")]
        public string SpouseTelephoneNumber { get; set; }

        [Display(Name="Next Of Kin")]
        //[Required]
        public string NextOfKinName { get; set; }

        [Display(Name = "Next of Kin ID Number")]
        //[Required]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Invalid Next of Kin ID Number. Must be 0-9")]
        public string NextOfKinIDNumber { get; set; }

        [Display(Name = "Next of Kin Tel No")]
        //[Required]
        [RegularExpression(@"^254(?:[0-9]??){6,14}[0-9]$", ErrorMessage = "The mobile number should start with country code 254 followed by phone number")]
        public string NextOfKinTelNumber { get; set; }

        [Display(Name="Next of Kin Relationship")]
        //[Required]
        public string NextofKinRelationship { get; set; }

        [Display(Name="Group Name")]
        public Guid GroupId { get ;set;}

        [NotMapped]
        public Group Group { get; set; }

        [Display(Name="Bank Name")]
        public string BankCode { get; set; }

        [Display(Name="Bank Account Number")]
        public string BankAccountNumber { get; set; }

        [Display(Name = "County Name")]
        [Required]
        public string CountyCode { get; set; }

        public string CountyName { get; set; }

        [Display(Name = "Constituency Name")]
        [Required]
        public string ConstituencyCode { get; set; }

        public string ConstituencyName { get; set; }

        [Display(Name = "Ward Name")]
        //[Required]
        public string WardCode { get; set; }

        public string WardName { get; set; }

        [Display(Name = "Village/Estate")]
        public string Village { get; set; }

        [Display(Name = "Street/ Landmark")]
        public string Landmark { get; set; }

        [Display(Name = "Plot Number    ")]
        public string PlotNumber { get; set; }

        [NotMapped]
        [Display(Name="Add To A Group?")]
        public bool BelongsToAGroup { get; set; }

        public int RecordStatus { get; set; }

        [Display(Name="Group Member Number")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Invalid Member number. Only 0-9 allowed")]
        public string GroupMemberNumber { get; set; }

        public string RecordStatusDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.RecordStatus), RecordStatus) ? EnumHelper.GetDescription((Enumerations.RecordStatus)RecordStatus) : string.Empty;
            }
        }
    }
}

﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize(Roles = "Administrator, FinanceManager")]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class C2BNotificationController : Controller
    {
        private readonly IC2BTransactionService c2transactionservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public C2BNotificationController(IC2BTransactionService _c2transactionservice)
        {
            if(_c2transactionservice == null)
                throw new ArgumentNullException("null service reference");

            this.c2transactionservice = _c2transactionservice;
        }

        // GET: C2BNotification
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetC2BTransactions(int id, JQueryDataTablesModel datatablemodel)
        {            
            var pagecollection = new List<C2BTransaction>();
            if (id == 0)
            {
                pagecollection = c2transactionservice.GetAllC2BTransaction().Where(x => x.Status == (int)Enumerations.C2BTransactionStatus.Processed || x.Status == (int)Enumerations.C2BTransactionStatus.JournalProcessed).ToList();
            }
            else
            {
                pagecollection = c2transactionservice.GetAllC2BTransaction().Where(x => x.Status == (int)Enumerations.C2BTransactionStatus.Suspense || x.Status == (int)Enumerations.C2BTransactionStatus.Unknown || x.Status == (int)Enumerations.C2BTransactionStatus.Reversed || x.Status == (int)Enumerations.C2BTransactionStatus.PendingReconciliation).ToList();                
            }

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var C2BList = GetAllC2BTransactions(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, c2btransactionlist: pagecollection);


            return Json(new JQueryDataTablesResponse<C2BTransaction>(items: C2BList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public IList<C2BTransaction> GetAllC2BTransactions(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<C2BTransaction> c2btransactionlist)
        {
            var c2blist = c2btransactionlist;

            totalRecordCount = c2blist.Count();

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                c2blist = c2blist.Where(c => c.TransID.ToLower().Contains(searchString.ToLower()) 
                    || c.MSISDN.ToString().Contains(searchString.ToLower())                                        
                    || c.StatusDesc.ToString().Contains(searchString.ToLower())                     
                    || c.FullName.ToString().ToLower().Contains(searchString.ToLower())                                     
                    || c.BusinessShortCode.ToString().ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = c2blist.Count;

            IOrderedEnumerable<C2BTransaction> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? c2blist.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "BillRefNumber":
                        sortedClients = sortedClients == null ? c2blist.CustomSort(sortedColumn.Direction, cust => cust.BillRefNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.BillRefNumber);
                        break;

                    case "BusinessShortCode":
                        sortedClients = sortedClients == null ? c2blist.CustomSort(sortedColumn.Direction, cust => cust.BusinessShortCode)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.BusinessShortCode);
                        break;

                    case "InvoiceNumber":
                        sortedClients = sortedClients == null ? c2blist.CustomSort(sortedColumn.Direction, cust => cust.InvoiceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InvoiceNumber);
                        break;

                    case "FullName":
                        sortedClients = sortedClients == null ? c2blist.CustomSort(sortedColumn.Direction, cust => cust.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.FullName);
                        break;

                    case "MSISDN":
                        sortedClients = sortedClients == null ? c2blist.CustomSort(sortedColumn.Direction, cust => cust.MSISDN)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.MSISDN);
                        break;

                    case "StatusDesc":
                        sortedClients = sortedClients == null ? c2blist.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc);
                        break;

                    case "TransAmount":
                        sortedClients = sortedClients == null ? c2blist.CustomSort(sortedColumn.Direction, cust => cust.TransAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransAmount);
                        break;

                    case "TransID":
                        sortedClients = sortedClients == null ? c2blist.CustomSort(sortedColumn.Direction, cust => cust.TransID)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransID);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult Reverse(Guid id)
        {
            if (id == Guid.Empty)
                throw new ArgumentNullException("id cannot be null");

            C2BTransaction c2b = new C2BTransaction();
            var findC2BById = c2transactionservice.FindC2BTransactionById(id);

            if (findC2BById != null)
            {
                c2b = findC2BById;
            }

            return View(c2b);
        }

        [HttpPost]
        public ActionResult Reverse(C2BTransaction c2bTransaction)
        {
            try
            {
                if (c2bTransaction != null)
                {
                    var findC2BTransactionById = c2transactionservice.FindC2BTransactionById(c2bTransaction.Id);
                    if (findC2BTransactionById != null)
                    {
                        findC2BTransactionById.Status = (int)Enumerations.C2BTransactionStatus.Reverse;
                    }

                    c2transactionservice.UpdateC2BTransaction(findC2BTransactionById, serviceHeader);
                }
            }

            catch(Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View();
            }

            return View("Index");
        }
    }
}
﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nikopeshe.WebApplication.Extensions
{
    public static class ValidateLoanRequestDocumentAvailability
    {        
        public static bool LoanRequestDocumentValidation()
        {
            DataContext datacontext = new DataContext();

            var currentuser = HttpContext.Current.User.Identity;
            bool returnvalue = false;
            var findcustomerbymobilnumber = datacontext.Customer.Where(x => x.MobileNumber == currentuser.Name).FirstOrDefault();
            if (findcustomerbymobilnumber != null)
            {
                var findcustomeraloans = datacontext.LoanRequest.Where(x => x.CustomerAccount.CustomerId == findcustomerbymobilnumber.Id);
                if (findcustomeraloans != null && findcustomeraloans.Any())
                {
                    var filterwiththependingloan = findcustomeraloans.Where(x => x.Status == (int)Enumerations.LoanStatus.Pending).FirstOrDefault();
                    if (filterwiththependingloan != null)
                    {
                        var findbankstatment = datacontext.LoanRequestDocument.Where(x => x.LoanRequestId == filterwiththependingloan.Id && x.DocumentType == Enumerations.LoanRequestDocumentTypes.BankStatement).FirstOrDefault();
                        var findloanapplicationform = datacontext.LoanRequestDocument.Where(x => x.LoanRequestId == filterwiththependingloan.Id && x.DocumentType == Enumerations.LoanRequestDocumentTypes.LoanApplicationForm).FirstOrDefault();
                        var findpayslip = datacontext.LoanRequestDocument.Where(x => x.LoanRequestId == filterwiththependingloan.Id && x.DocumentType == Enumerations.LoanRequestDocumentTypes.Payslip).FirstOrDefault();

                        if (findbankstatment == null || findloanapplicationform == null || findpayslip == null)
                        {
                            returnvalue = true;
                        }
                        else
                        {
                            returnvalue = false;
                        }
                    }
                }
            }

            return returnvalue;
        }
    }
}
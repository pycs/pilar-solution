﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Entity
{
    public class LoanScheduleModel
    {
        public DateTime NextPaymentDate { get; set; }

        public decimal PrincipleAmount { get; set; }

        public decimal InterestAmount { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal OutstandingLoanBalance { get; set; }
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class StaticSettingService : IStaticSettingService
    {
        private IRepository<StaticSetting> staticsettingsrepository;

        private string ErrorMessage = string.Empty;

        public StaticSettingService(IRepository<StaticSetting> _staticsettingsrepository)
        {
            if (_staticsettingsrepository == null)
                throw new ArgumentNullException("static setting repository is null.");

            this.staticsettingsrepository = _staticsettingsrepository;
        }

        public void AddNewStaticSetting(StaticSetting staticsetting, ServiceHeader serviceHeader)
        {
            if (staticsetting == null)
                throw new ArgumentNullException("null entity");
            try
            {
                staticsettingsrepository.Insert(staticsetting, serviceHeader);
            }

            catch(DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }            
        }

        public void UpdateStaticSetting(StaticSetting staticsetting, ServiceHeader serviceHeader)
        {
            if (staticsetting == null)
                throw new ArgumentNullException("null entity");
            try
            {
                staticsettingsrepository.Update(staticsetting, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }   
        }

        public IList<StaticSetting> GetAllStaticSettings(IsolationLevel isolationLevel)
        {
            var getallstaticsettings = staticsettingsrepository.GetAll(isolationLevel);

            return getallstaticsettings.ToList();
        }

        public StaticSetting FindStaticSettingById(Guid Id)
        {
            if (Id == Guid.Empty)
                throw new ArgumentNullException("Guid cannot be null");

            return staticsettingsrepository.GetById(Id);            
        }

        public StaticSetting FindStaticSettingByKey(string statickey)
        {
            if (String.IsNullOrEmpty(statickey))
                throw new ArgumentNullException("Key cannot be null");

            return staticsettingsrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.Key == statickey).FirstOrDefault();
        }
    }
}

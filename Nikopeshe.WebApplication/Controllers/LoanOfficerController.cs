﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;
using NPOI.SS.UserModel;
using System.IO;
using NPOI.HSSF.UserModel;
using System.Collections;
using Nikopeshe.WebApplication.Models;
using Nikopeshe.Entity.Helpers;
using System.Web.Security;
using Infrastructure.Crosscutting.NetFramework.Utils;
using System.Data;
using Nikopeshe.Services.DTO;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize]
    public class LoanOfficerController : BaseController
    {
        private IGroupService groupService;
        internal readonly IBranchService branchservice;
        private readonly ICustomerService customerService;
        private readonly ICustomerGroupRelationshipService customerGroupRelationshipService;
        private readonly IProfessionService professionService;
        private readonly IRelationshipManagerService relationshipManagerService;
        private readonly ICustomerAccountService customerAccountService;
        private readonly ILoanProductService loanproductservice;
        private readonly ILoanRequestService loanrequestService;
        private readonly IUserInfoService userInfoService;
        private readonly IGroupHelperService groupHelperService;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public LoanOfficerController(IGroupService _groupService, ICustomerGroupRelationshipService _customerGroupRelationshipService
            , IProfessionService _professionService, ICustomerService _customerService,
            ICustomerAccountService _customerAccountService, IBranchService _branchservice, ILoanProductService _loanproductservice,
            IRelationshipManagerService _relationshipManagerService, IUserInfoService _userInfoService, ILoanRequestService _loanrequestService,
            IGroupHelperService _groupHelperService)
        {
            if (_groupService == null)
                throw new ArgumentNullException("_group Service");

            this.groupService = _groupService;
            this.customerGroupRelationshipService = _customerGroupRelationshipService;
            this.professionService = _professionService;
            this.customerService = _customerService;
            this.customerAccountService = _customerAccountService;
            this.branchservice = _branchservice;
            this.loanproductservice = _loanproductservice;
            this.relationshipManagerService = _relationshipManagerService;
            this.userInfoService = _userInfoService;
            this.loanrequestService = _loanrequestService;
            this.groupHelperService = _groupHelperService;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetGroupsFilterInPage(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc" ? true : false;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();

            var pageCollectionInfo = new PageCollectionInfo<Group>();
    
            pageCollectionInfo = groupService.FindGroupsFilterInPage(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, sortAscending);
            
            int totalRecordCount = 0;

            int searchRecordCount = 0;


            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;
            }

            return this.DataTablesJson(items: pageCollectionInfo.PageCollection, totalRecords: totalRecordCount, totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
        }

        public JsonResult GroupList(JQueryDataTablesModel datatablemodel)
        {           
            var pagecollection = groupService.GetAllGroup(IsolationLevel.ReadUncommitted);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var groupList = GetAllGroups(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, groupList: pagecollection);


            return Json(new JQueryDataTablesResponse<Group>(items: groupList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<Group> GetAllGroups(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<Group> groupList)
        {
            var GetGroupList = groupList;

            totalRecordCount = GetGroupList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetGroupList = GetGroupList.Where(c => c.GroupName
                    .ToLower().Contains(searchString.ToLower())
                    || c.RegistrationNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetGroupList.Count;

            IOrderedEnumerable<Group> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetGroupList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "GroupName":
                        sortedClients = sortedClients == null ? GetGroupList.CustomSort(sortedColumn.Direction, cust => cust.GroupName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.GroupName);
                        break;

                    case "RegistrationNumber":
                        sortedClients = sortedClients == null ? GetGroupList.CustomSort(sortedColumn.Direction, cust => cust.RegistrationNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.RegistrationNumber);
                        break;                    

                    case "TelephoneNumber":
                        sortedClients = sortedClients == null ? GetGroupList.CustomSort(sortedColumn.Direction, cust => cust.TelephoneNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TelephoneNumber);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }
        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.CountySelectList = GetCountysSelectList(string.Empty);
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            ViewBag.ConstituencySelectList = GetConstituencySelectList(string.Empty, string.Empty);
            ViewBag.CountyAssemblyWardSelectList = GetCountyAssemblyWardSelectList(string.Empty, string.Empty, string.Empty);
            return View();
        }

        [HttpPost]
        public ActionResult Create(Group group)
        {
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            ViewBag.CountySelectList = GetCountysSelectList(string.Empty);
            ViewBag.ConstituencySelectList = GetConstituencySelectList(string.Empty, string.Empty);
            ViewBag.CountyAssemblyWardSelectList = GetCountyAssemblyWardSelectList(string.Empty, string.Empty, string.Empty);
            if (ModelState.IsValid)
            {
                try
                {
                    //groupService.CreateNewGroup(group, serviceHeader);
                }

                catch(Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                }
            }
            return View();
        }

        public ActionResult GroupMembers(Guid Id)
        {
            ViewBag.Id = Id;

            return View();
        }

        public JsonResult GetGroupMembers(JQueryDataTablesModel datatablemodel, Guid Id)
        {                       
            var pagecollection = customerGroupRelationshipService.GetCustomerGroupRelationshipByGroupId(Id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var customerGroupRelationship = GetAllGroupMembers(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, groupMembersList: pagecollection);


            return Json(new JQueryDataTablesResponse<CustomerGroupRelationship>(items: customerGroupRelationship,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<CustomerGroupRelationship> GetAllGroupMembers(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<CustomerGroupRelationship> groupMembersList)
        {
            var GetGroupList = groupMembersList;

            totalRecordCount = GetGroupList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetGroupList = GetGroupList.Where(c => c.Group.GroupName
                    .ToLower().Contains(searchString.ToLower())
                    || c.Group.RegistrationNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetGroupList.Count;

            IOrderedEnumerable<CustomerGroupRelationship> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetGroupList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "Group.GroupName":
                        sortedClients = sortedClients == null ? GetGroupList.CustomSort(sortedColumn.Direction, cust => cust.Group.GroupName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Group.GroupName);
                        break;

                    case "Group.RegistrationNumber":
                        sortedClients = sortedClients == null ? GetGroupList.CustomSort(sortedColumn.Direction, cust => cust.Group.RegistrationNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Group.RegistrationNumber);
                        break;

                    case "Customer.FullName":
                        sortedClients = sortedClients == null ? GetGroupList.CustomSort(sortedColumn.Direction, cust => cust.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Customer.FullName);
                        break;

                    case "Customer.IDNumber":
                        sortedClients = sortedClients == null ? GetGroupList.CustomSort(sortedColumn.Direction, cust => cust.Customer.IDNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Customer.IDNumber);
                        break;

                    case "GroupMemberNo":
                        sortedClients = sortedClients == null ? GetGroupList.CustomSort(sortedColumn.Direction, cust => cust.GroupMemberNo)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.GroupMemberNo);
                        break;

                    case "GroupRoleDesc":
                        sortedClients = sortedClients == null ? GetGroupList.CustomSort(sortedColumn.Direction, cust => cust.GroupRoleDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.GroupRoleDesc);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult Edit(Guid Id)
        {
            var group = groupService.FindGroupById(Id);

            if (group != null)
            {
                return View(group);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult Edit(Group group)
        {
            if (ModelState.IsValid)
            {
                var findgroup = groupService.FindGroupById(group.Id);

                if (findgroup != null)
                {
                    groupService.UpdateGroup(findgroup, serviceHeader);
                }
            }

            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadConstituencyByCounty(string id)
        {
            var constituencyList = GetConstituencyList(id);

            var modelData = constituencyList.Select(m => new SelectListItem()
            {
                Text = m.Name,
                Value = m.Code,
            });

            return Json(modelData, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadCAWByConstituency(string countyId, string constituencyId)
        {
            var constituencyList = GetCountyAssemblyWardList(countyId, constituencyId);

            var modelData = constituencyList.Select(m => new SelectListItem()
            {
               
                Text = m.Name,
                Value = m.Code,

            });

            return Json(modelData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GroupRegistration()
        {
            ViewBag.CountySelectList = GetCountysSelectList(string.Empty);
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            ViewBag.ConstituencySelectList = GetConstituencySelectList(string.Empty, string.Empty);
            ViewBag.ProfessionSelectList = GetProfessionSelectList(string.Empty);
            ViewBag.SystemBranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            ViewBag.BranchesSelectList = GetBranchesSelectList(string.Empty, string.Empty);
            ViewBag.RelationshipManager = RelationshipManagerSelectList2(Guid.Empty.ToString(),Guid.Empty);
            ViewBag.CountyAssemblyWardSelectList = GetCountyAssemblyWardSelectList(string.Empty, string.Empty, string.Empty);
            return View();
        }

        [HttpPost]
        public ActionResult GroupRegistration(Group group)
        {
            ViewBag.CountySelectList = GetCountysSelectList(string.Empty);
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            ViewBag.SystemBranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            ViewBag.ConstituencySelectList = GetConstituencySelectList(string.Empty, string.Empty);
            ViewBag.ProfessionSelectList = GetProfessionSelectList(string.Empty);
            ViewBag.RelationshipManager = RelationshipManagerSelectList2(Guid.Empty.ToString(), (Guid)group.BranchId);
            //ViewBag.RelationshipManager = GetAllRelationshipInList(group.RelationshipManagerId.ToString(), group.BranchId);
            ViewBag.BranchesSelectList = GetBranchesSelectList(string.Empty, string.Empty);
            ViewBag.CountyAssemblyWardSelectList = GetCountyAssemblyWardSelectList(string.Empty, string.Empty, string.Empty);
            if (ModelState.IsValid)
            {
                try
                {
                    #region Customer
                    Guid CustomerId = Guid.NewGuid();
                    string nextCustomerNumber = customerService.NextCustomerNumber();
                    bool checkduplicatecustomer = customerService.CheckDuplicateCustomer(group.TelephoneNumber, group.GroupNumber);
                    bool checkduplicategroupnumber = groupService.CheckDuplicateGroupNumber(group.GroupNumber);
                    var rulesException = new RulesException();
                    //if (checkduplicatecustomer)
                    //{
                    //    rulesException.ErrorForModel(string.Format("Customer with Mobile Number {0} or Id Number {1} already exists", group.TelephoneNumber, group.RegistrationNumber));
                    //}

                    if (group.RelationshipManagerId == Guid.Empty)
                        rulesException.ErrorForModel("Program Officer is required.");

                    if (System.Text.RegularExpressions.Regex.IsMatch("^[a-z A-Z]+$", group.GroupName))
                        rulesException.ErrorForModel("Group Name cannot contain numerics");



                    if(checkduplicategroupnumber)
                    {
                        rulesException.ErrorForModel(string.Format("Group with Group Number {0} already exists", group.GroupNumber));
                    }
                    if (rulesException.Errors.Any())
                        throw rulesException;

                    string BranchId = group.BranchId.ToString();

                    if (string.IsNullOrWhiteSpace(group.RegistrationNumber))
                    {
                    group.RegistrationNumber = "";
                    }
                    if (string.IsNullOrWhiteSpace(group.DateOfRegistration.ToString()))
                    {
                        group.DateOfRegistration=DateTime.Parse("1/1/1970");
                    }

                    var customer = new Customer()
                    {
                        Id = CustomerId,
                        FirstName = group.GroupName,
                        MiddleName = group.GroupName,
                        LastName = group.GroupName,
                        BranchId = Guid.Parse(BranchId),
                        MobileNumber = group.TelephoneNumber,
                        IDNumber = group.RegistrationNumber,
                        DateofBirth = DateTime.Parse(group.DateOfRegistration.ToString()),
                        Occupation = group.DealingIn,
                        CustomerRegistrationType = Enumerations.CustomerRegistrationType.WalkIn,
                        MaritalStatus = Enumerations.MaritalStatus.Married,
                        Gender = Enumerations.Gender.Male,
                        NextOfKinIDNumber = "0000",
                        CustomerType = Enumerations.CustomerTypes.Group,
                        NextOfKinName = group.GroupName,
                        NextofKinRelationship = "Unknown",
                        NextOfKinTelNumber = group.TelephoneNumber,
                        CountyCode = group.CountyCode,
                        ConstituencyCode = group.ConstituencyCode,
                        RecordStatus = (int)Enumerations.RecordStatus.New,
                        Title = Enumerations.Title.Dr,
                        CustomerNumber = nextCustomerNumber,
                        PoBoxNumber = group.PoBox,
                        PostalCode = group.PostalCode,
                        IsApproved = true,
                        IsEnabled = true, 
                        GroupRole=Enumerations.GroupRoles.Member,
                    };

                    customerService.AddNewCustomer(customer, serviceHeader);

                    #endregion
                    
                    #region CustomerAcounts
                    var customersaccount = new CustomersAccount()
                    {
                        CustomerId = CustomerId,
                        AccountType = (int)Enumerations.ProductCode.Savings,
                        AccountNumber = customerService.NextCustomerSavingAccountNumber(nextCustomerNumber),
                        IsActive = true
                    };
                    customerAccountService.AddNewCustomersAccount(customersaccount, serviceHeader);
                    #endregion

                    #region Group
                    
                    group.CustomerId = CustomerId;
                    group.BankPaybillNumber = GetBankPaybillNumber(group.BankCode);
                    group.CreatedBy = User.Identity.Name;
                    group.GroupName = group.GroupName.ToUpper();
                    group.RecordStatus = (int)Enumerations.RecordStatus.New;
                    group.RegistrationNumber = group.RegistrationNumber.ToUpper();
                    groupService.CreateNewGroup(group, customer, serviceHeader);

                    #endregion        
            
                    return RedirectToAction("Index");
                }

                catch (RulesException ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    
                    ex.CopyTo(ModelState);
                    return View(group);
                }
            }

            return View(group);
        }

        [HttpGet]
        public ActionResult RegisterLoan(string Id)
        {
            var customerlist = GetCustomerlist(Guid.Empty.ToString());
            ViewBag.RelationshipManagersSelectList = RelationshipManagerSelectList(Guid.Empty.ToString());
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            //ViewBag.BranchesSelectList = GetBranchesSelectList(string.Empty, string.Empty);
            ViewBag.EFTTypes = EFTTypeInfo(null);
            var loanproductlist = Loanproductlist(Guid.Empty.ToString());
            ViewBag.CustomersList = customerlist;
            ViewBag.LoanProductsList = loanproductlist;
            string customerMobileNumber = string.Empty;
            string mobileNumber = string.Empty;
            string bankAccountNumber = string.Empty;
            if (string.IsNullOrWhiteSpace(Id))
            {
                Id = Guid.Empty.ToString();
                Guid customerId = Guid.Parse(Id);
            }
            else
            {
                var group = groupService.FindGroupByCustomerId(Guid.Parse(Id));
                Guid CustomerId = Guid.Parse(Id);
                var findCustomerById = customerService.FindCustomerById(CustomerId);
                mobileNumber = group.TelephoneNumber;
                bankAccountNumber = group.BankAccountNumber;
                customerMobileNumber = findCustomerById.MobileNumber;
            }
            var loanrequest = new LoanRequest()
            {
                NextPaymentDate = DateTime.Today,
                CustomerId = Guid.Parse(Id),
                PrimaryAccountNumber = customerMobileNumber
            };
            ViewBag.MobileNumber = mobileNumber;
            ViewBag.BankAccountNumber = bankAccountNumber;
            return View(loanrequest);
        }

        [HttpPost]
        public ActionResult RegisterLoan(LoanRequest loanrequest)
        {
            ViewBag.EFTTypes = EFTTypeInfo(null);
            var loanproductlist = Loanproductlist(Guid.Empty.ToString());
            ViewBag.RelationshipManagersSelectList = RelationshipManagerSelectList(Guid.Empty.ToString());
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            ViewBag.LoanProductsList = loanproductlist;
            var customerlist = GetCustomerlist(Guid.Empty.ToString());
            ViewBag.CustomersList = customerlist;
            try
            {                
                var selectedtype = Request.Form["Type"];
                var customerId = Request.Form["CustomerId"];
                string customerMobileNumber = string.Empty;
                string paybillNumber = string.Empty;
                var rulesException = new RulesException();

                var findGroupByCustomerId = groupService.FindGroupByCustomerId(loanrequest.CustomerId);

                if (string.IsNullOrWhiteSpace(findGroupByCustomerId.BankCode))
                    rulesException.ErrorForModel("Failed. If transfer mode is to bank account, bank details cannot be null.");

                if (findGroupByCustomerId == null)
                {
                    rulesException.ErrorForModel("Could not find the selected group.");
                }

                if (selectedtype == null)
                {
                    rulesException.ErrorForModel("You must select money transfer mode");
                    if (rulesException.Errors.Any())
                        throw rulesException;
                }
                if (loanrequest.LoanProductId == null || loanrequest.LoanProductId == Guid.Empty)
                {
                    rulesException.ErrorForModel("Loan Product cannot be null.");
                    if (rulesException.Errors.Any())
                        throw rulesException;
                }
                var efttype = int.Parse(selectedtype);

                if (loanrequest.LoanProductId == Guid.Empty)
                {
                    rulesException.ErrorForModel("You must select a loan product from the provided list.");               
                }

                if (loanrequest.RelationshipManagerId == Guid.Empty)
                {
                    rulesException.ErrorForModel("Loan Officer is required.");
                }

                if (loanrequest.CustomerId == Guid.Empty)
                {
                    rulesException.ErrorForModel("You must select a customer from the list.");
                }

                if (loanrequest.AcceptTermsAndConditions == false)
                {
                    rulesException.ErrorForModel("You must accept terms and conditions.");
                }

                UserInfo userInfo = userInfoService.FindUserByUserName(User.Identity.Name);
                if (userInfo == null)
                {
                    rulesException.ErrorForModel("An error has occurred while processing. Please contact system administrator for assistance.");
                    LoggerFactory.CreateLog().LogError("Error -> {0}", "No current user found while processing this loan request.");
                    if (rulesException.Errors.Any())
                        throw rulesException;
                }
                Tuple<decimal, decimal, decimal> amountvalues = loanrequestService.GetMaxMinDefaultLoanAmount(loanrequest.LoanProductId);

                Tuple<int, decimal> termandinterestrate = loanproductservice.GetLoanTermandInterestRateByProductId(loanrequest.LoanProductId);

                var findcustomerbycustomerid = customerService.FindCustomerById(loanrequest.CustomerId);
                if (findcustomerbycustomerid == null)
                {
                    rulesException.ErrorForModel("Failed while retrieving customer details. Customer doesn't exist.");
                    if (rulesException.Errors.Any())
                        throw rulesException;
                }

                loanrequest.LoanEndDate = DateTime.Today.AddMonths(termandinterestrate.Item1);

                string loanrefno = GenerateRandomLoanReferenceNumber(8);
                var findloanproductbyid = loanproductservice.FindLoanProductById(loanrequest.LoanProductId);
                loanrequest.LoanStartDate = DateTime.Now;
                loanrequest.BranchId = userInfo.BranchId;
                //loanrequest.NegotiatedInstallments = NegotiatedInstallments;
                if (loanrequest.NegotiatedInstallments == 0)
                {
                    if (findloanproductbyid.PaymentFrequencyType == Enumerations.PaymentFrequency.Monthly)
                    {
                        loanrequest.NegotiatedInstallments = termandinterestrate.Item1;
                    }
                    else if (findloanproductbyid.PaymentFrequencyType == Enumerations.PaymentFrequency.Weekly)
                    {
                        loanrequest.NegotiatedInstallments = termandinterestrate.Item1 * 4;
                    }

                }
                if (ModelState.IsValid)
                {

                    if (efttype == (int)Enumerations.EFTType.AccountToMobile)
                    {
                        System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"^254(?:[0-9]??){6,14}[0-9]$");
                        System.Text.RegularExpressions.Match match = regex.Match(loanrequest.PrimaryAccountNumber);
                        if (!match.Success)
                        {
                            rulesException.ErrorForModel("Failed. If transfer mode is to mobile, the Primary Account Number should start 254, followed by the national number");
                        }
                    }
                    else
                    {
                        loanrequest.ApplicantBankCode = findGroupByCustomerId.BankCode + findGroupByCustomerId.BranchCode;
                        if (string.IsNullOrWhiteSpace(loanrequest.ApplicantBankCode))
                        {
                            rulesException.ErrorForModel("Failed. If transfer mode is to bank account, you must select your Bank from the list.");
                        }
                        else
                        {
                            paybillNumber = GetBranchesPaybillList(findGroupByCustomerId.BankCode);
                            if (paybillNumber.Equals("0"))
                            {
                                rulesException.ErrorForModel("The system could not much a valid Paybill Number with the selected bank. Please select another bank or change the funds transfer method.");
                            }
                        }                    
                    }

                    if (rulesException.Errors.Any())
                        throw rulesException;

                    var findcustomeraccountnumbers = customerAccountService.GetAllCustomerAccountNumbers(loanrequest.CustomerId, IsolationLevel.ReadCommitted).ToArray();

                    var NextCustomerAccountLoanAccountNumber = customerService.NextCustomerLoanAccountNumber(findcustomerbycustomerid.CustomerNumber, findcustomerbycustomerid.Id, findloanproductbyid.ProductCode);
                    CustomersAccount customersaccount = new CustomersAccount();
                    customersaccount.AccountNumber = NextCustomerAccountLoanAccountNumber;
                    customersaccount.IsActive = true;
                    customersaccount.AccountType = (int)Enumerations.ProductCode.Loan;
                    customersaccount.CustomerId = findcustomerbycustomerid.Id;
                
                    var checkingoutstandingloan = loanrequestService.GetLoanRequestByCustomerAccountId(loanrequest.CustomerId);
                    int getnotificationenabledmode = customerService.GetCustomerEnabledNotificationMode(loanrequest.CustomerId);
                    var countunclearedloans = checkingoutstandingloan.Where(x => x.Status != (int)Enumerations.LoanStatus.Cleared && x.Status != (int)Enumerations.LoanStatus.Rejected && x.Status != (int)Enumerations.LoanStatus.CustomerRejected && x.Status != (int)Enumerations.LoanStatus.Review && x.Status != (int)Enumerations.LoanStatus.TransactionReversed).Count();
                    if (loanrequest.LoanAmount <= amountvalues.Item1 && loanrequest.LoanAmount >= amountvalues.Item2)
                    {
                        if (checkingoutstandingloan.Count() != 0)
                        {
                            if (countunclearedloans >= 1)
                            {
                                rulesException.ErrorForModel("Sorry, you have an uncleared loan. Clear it first and try again later.");
                                if (rulesException.Errors.Any())
                                    throw rulesException;
                            }
                            else
                            {
                                decimal interest = 0m;
                                if (findloanproductbyid.InterestChargeType == (int)Enumerations.InterestChargeType.Fixed)
                                {
                                    interest = loanrequest.NegotiatedInstallments * findloanproductbyid.InterestValue;
                                }
                                else
                                {
                                    var interestratepermonth = findloanproductbyid.InterestValue / 12;
                                    interest = (interestratepermonth * findloanproductbyid.Term * loanrequest.LoanAmount) / 100;
                                }

                                loanrequest.InterestAmount = interest;
                                loanrequest.InterestDueToday = interest / (int)findloanproductbyid.PaymentFrequencyType;
                                loanrequest.LoanReferenceNumber = loanrefno;
                                loanrequest.LoanType = (int)Enumerations.LoanType.Group;
                                loanrequest.CustomerAccountId = customersaccount.Id;
                                if (efttype == (int)Enumerations.EFTType.BusinessToBusiness)
                                {
                                    loanrequest.BankCode = paybillNumber;
                                    loanrequest.PrimaryAccountNumber = findGroupByCustomerId.BankAccountNumber;
                                    loanrequest.Type = Enumerations.EFTType.BusinessToBusiness;
                                }
                                else if (efttype == (int)Enumerations.EFTType.AccountToMobile)
                                {
                                    loanrequest.PrimaryAccountNumber = findGroupByCustomerId.TelephoneNumber;
                                    loanrequest.Type = Enumerations.EFTType.AccountToMobile;

                                }
                                //else if (efttype == (int)Enumerations.EFTType.Cheque)
                                //{
                                //    loanrequest.Type = Enumerations.EFTType.Cheque;
                                //}

                                loanrequest.Status = (int)Enumerations.LoanStatus.Pending;
                                loanrequest.Id = Guid.NewGuid();
                                customerAccountService.AddNewCustomersAccount(customersaccount, serviceHeader);
                                loanrequestService.AddNewLoanRequest(loanrequest, serviceHeader);
                                
                                return RedirectToAction("allloanrequest", "LoanRequest");
                            }
                        }

                        else
                        {
                            if (efttype == (int)Enumerations.EFTType.BusinessToBusiness)
                            {
                                loanrequest.BankCode = paybillNumber;
                                loanrequest.PrimaryAccountNumber = findGroupByCustomerId.BankAccountNumber;
                                loanrequest.Type = Enumerations.EFTType.BusinessToBusiness;
                            }
                            else if (efttype == (int)Enumerations.EFTType.AccountToMobile)
                            {
                                loanrequest.PrimaryAccountNumber = findGroupByCustomerId.TelephoneNumber;
                                loanrequest.Type = Enumerations.EFTType.AccountToMobile;

                            }   

                            decimal interest = 0m;
                            if (findloanproductbyid.InterestChargeType == (int)Enumerations.InterestChargeType.Fixed)
                            {
                                interest = loanrequest.NegotiatedInstallments * findloanproductbyid.InterestValue;
                            }
                            else
                            {
                                var interestratepermonth = findloanproductbyid.InterestValue / 12;
                                interest = (interestratepermonth * findloanproductbyid.Term * loanrequest.LoanAmount) / 100;
                            }

                            loanrequest.InterestAmount = interest;
                            loanrequest.InterestDueToday = interest / (int)findloanproductbyid.PaymentFrequencyType;
                            loanrequest.LoanReferenceNumber = loanrefno;
                            loanrequest.Status = (int)Enumerations.LoanStatus.Pending;
                            loanrequest.CustomerAccountId = customersaccount.Id;
                            loanrequest.LoanType = (int)Enumerations.LoanType.Individual;
                            using (var datacontext = new DataContext())
                            {
                                using (var transaction = datacontext.Database.BeginTransaction())
                                {
                                    try
                                    {
                                        customerAccountService.AddNewCustomersAccount(customersaccount, serviceHeader);
                                        loanrequestService.AddNewLoanRequest(loanrequest, serviceHeader);
                                        
                                    }

                                    catch (Exception ex)
                                    {
                                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                        return View(loanrequest);
                                    }
                                }
                            }

                            if (Roles.IsUserInRole("LoanOfficer"))
                            {
                                return RedirectToAction("loanrequestlist", "LoanRequest");
                            }
                            return RedirectToAction("allloanrequest", "LoanRequest");
                        }
                    }

                    else
                    {
                        string errormessage = string.Format("Minimum amount you can borrow is {0} and maximum amount {1}. Make sure loan amount is within the range", (amountvalues.Item2), (amountvalues.Item1));
                        ModelState.AddModelError(string.Empty, errormessage);
                        return View(loanrequest);
                    }
                }                
            }

            catch (RulesException ex)
            {
                ModelState.AddModelError(string.Empty, "Operation was unsuccessful, correct the errors and try again.");
                ex.CopyTo(ModelState);
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
            return View(loanrequest);
        }

        public ActionResult EditGroup(Guid Id)
        {

            var findGroup = groupService.FindGroupById(Id);
            ViewBag.CountySelectList = GetCountysSelectList(findGroup.CountyCode);
            ViewBag.BanksSelectList = GetBanksSelectList(findGroup.BankCode);
            ViewBag.ConstituencySelectList = GetConstituencySelectList(findGroup.CountyCode, findGroup.ConstituencyCode);
            ViewBag.ProfessionSelectList = GetProfessionSelectList(findGroup.ProfessionId.ToString());
            ViewBag.SystemBranchesSelectList = BranchesSelectList(findGroup.BranchId.ToString());
            ViewBag.BranchesSelectList = GetBranchesSelectList(findGroup.BankCode, findGroup.BranchCode);
            ViewBag.CountyAssemblyWardSelectList = GetCountyAssemblyWardSelectList(findGroup.CountyCode, findGroup.ConstituencyCode, findGroup.WardCode);
            ViewBag.RelationshipManager = RelationshipManagerSelectList2(findGroup.RelationshipManagerId.ToString(), (Guid)findGroup.BranchId);
            //ViewBag.GroupTypeSelectList= GroupType


            return View(findGroup);
        }

        [HttpPost]
        [ActionName("EditGroup")]
        public ActionResult EditGroupOnSubmit(Group group)
        {
            //        if (ModelState.IsValid)
            //{
            //    db.Entry(group).State = EntityState.Modified;
            //    db.SaveChanges();
            //    //return RedirectToAction("Index");
            //}
            //    return View(group);
            ViewBag.CountySelectList = GetCountysSelectList(group.CountyCode);
            ViewBag.BanksSelectList = GetBanksSelectList(group.BankCode);
            ViewBag.ConstituencySelectList = GetConstituencySelectList(group.CountyCode, group.ConstituencyCode);
            ViewBag.ProfessionSelectList = GetProfessionSelectList(group.ProfessionId.ToString());
            ViewBag.SystemBranchesSelectList = BranchesSelectList(group.BranchId.ToString());
            ViewBag.BranchesSelectList = GetBranchesSelectList(group.BankCode, group.BranchCode);
            ViewBag.CountyAssemblyWardSelectList = GetCountyAssemblyWardSelectList(group.CountyCode, group.ConstituencyCode, group.WardCode);
            ViewBag.RelationshipManager = RelationshipManagerSelectList2(group.RelationshipManagerId.ToString(), (Guid)group.BranchId);
            Group findGroup = groupService.FindGroupById(group.Id);
            if (ModelState.IsValid)
            {
                try
                {
                    #region Customer
                    var rulesException = new RulesException();
                    //Guid CustomerId = (Guid)findGroup.CustomerId;
                    if (findGroup == null)
                    {
                        rulesException.ErrorForModel("Cannot find group.");
                    }
                    var customerEdit = customerService.FindCustomerById((Guid)findGroup.CustomerId);
                    string nextCustomerNumber = customerEdit.CustomerNumber;



                    if (group.RelationshipManagerId == Guid.Empty)
                        rulesException.ErrorForModel("Program Officer is required.");

                    if (System.Text.RegularExpressions.Regex.IsMatch("^[a-z A-Z]+$", group.GroupName))
                        rulesException.ErrorForModel("Group Name cannot contain numerics");

                    if (rulesException.Errors.Any())
                        throw rulesException;

                    string BranchId = group.BranchId.ToString();

                    if (string.IsNullOrWhiteSpace(group.RegistrationNumber))
                    {
                        group.RegistrationNumber = "";
                    }
                    if (string.IsNullOrWhiteSpace(group.DateOfRegistration.ToString()))
                    {
                        group.DateOfRegistration = DateTime.Parse("1/1/1970");
                    }

                    //var customer = new Customer()
                    //{
                    //    Id = customerEdit.Id,
                    //    FirstName = group.GroupName,
                    //    MiddleName = group.GroupName,
                    //    LastName = group.GroupName,
                    //    BranchId = Guid.Parse(BranchId),
                    //    MobileNumber = group.TelephoneNumber,
                    //    IDNumber = group.RegistrationNumber,
                    //    DateofBirth = DateTime.Parse(group.DateOfRegistration.ToString()),
                    //    Occupation = group.DealingIn,
                    //    CustomerRegistrationType = Enumerations.CustomerRegistrationType.WalkIn,
                    //    MaritalStatus = Enumerations.MaritalStatus.Married,
                    //    Gender = Enumerations.Gender.Male,
                    //    NextOfKinIDNumber = "0000",
                    //    CustomerType = Enumerations.CustomerTypes.Group,
                    //    NextOfKinName = group.GroupName,
                    //    NextofKinRelationship = "Unknown",
                    //    NextOfKinTelNumber = group.TelephoneNumber,
                    //    CountyCode = group.CountyCode,
                    //    ConstituencyCode = group.ConstituencyCode,
                    //    RecordStatus = (int)Enumerations.RecordStatus.New,
                    //    Title = Enumerations.Title.Dr,
                    //    CustomerNumber = nextCustomerNumber,
                    //    PoBoxNumber = group.PoBox,
                    //    PostalCode = group.PostalCode,
                    //    IsApproved = true,
                    //    IsEnabled = true,
                    //};

                        customerEdit.FirstName = group.GroupName;
                        customerEdit.MiddleName = group.GroupName;
                        customerEdit.LastName = group.GroupName;
                        customerEdit.BranchId = Guid.Parse(BranchId);
                        customerEdit.MobileNumber = group.TelephoneNumber;
                        customerEdit.IDNumber = group.RegistrationNumber;
                        customerEdit.Occupation = group.DealingIn;
                        customerEdit.NextOfKinName = group.GroupName;
                        customerEdit.NextOfKinTelNumber = group.TelephoneNumber;
                        customerEdit.CountyCode = group.CountyCode;
                        customerEdit.ConstituencyCode = group.ConstituencyCode;
                        customerEdit.RecordStatus = (int)Enumerations.RecordStatus.Edited;
                        customerEdit.PoBoxNumber = group.PoBox;
                        customerEdit.PostalCode = group.PostalCode;
                        customerEdit.DateofBirth = DateTime.Parse(group.DateOfRegistration.ToString());

                    //customerService.AddNewCustomer(customer, serviceHeader);
                    customerService.UpdateCustomer(customerEdit, serviceHeader);

                    #endregion

                    #region CustomerAcounts

                    //var customersaccount = new CustomersAccount()
                    //{
                    //    CustomerId = CustomerId,
                    //    AccountType = (int)Enumerations.ProductCode.Savings,
                    //    //AccountNumber = customerService.NextCustomerSavingAccountNumber(nextCustomerNumber),
                    //    AccountNumber = customerAccountService.FindCustomersAccountByCustomerId(CustomerId).AccountNumber,
                    //    IsActive = true
                    //};
                    ////customerAccountService.AddNewCustomersAccount(customersaccount, serviceHeader);
                    //customerAccountService.UpdateCustomersAccount(customersaccount, serviceHeader);

                    #endregion

                    #region Group

                    findGroup.BankPaybillNumber = GetBankPaybillNumber(group.BankCode);
                    //group.CreatedBy = User.Identity.Name;
                    findGroup.GroupName = group.GroupName.ToUpper();
                    findGroup.RecordStatus = (int)Enumerations.RecordStatus.Edited;
                    findGroup.BankAccountNumber = group.BankAccountNumber;
                    findGroup.BankCode = group.BankCode;
                    findGroup.BankPaybillNumber = group.BankPaybillNumber;
                    findGroup.BranchCode = group.BranchCode;
                    findGroup.BranchId = group.BranchId;
                    findGroup.ConstituencyCode = group.ConstituencyCode;
                    findGroup.CountyCode = group.CountyCode;
                    findGroup.CurrentYearOfLicence = group.CurrentYearOfLicence;
                    findGroup.DealingIn = group.DealingIn;
                    findGroup.ElectionType = group.ElectionType;
                    findGroup.GroupNumber = group.GroupNumber;
                    findGroup.GroupType = group.GroupType;
                    findGroup.Landmark = group.Landmark;
                    findGroup.MeetingFrequency = group.MeetingFrequency;
                    findGroup.PlotNumber = group.PlotNumber;
                    findGroup.PoBox = group.PoBox;
                    findGroup.PostalCode = group.PostalCode;
                    findGroup.Profession = findGroup.Profession;
                    findGroup.ProfessionId = findGroup.ProfessionId;
                    findGroup.RegistrationNumber = group.RegistrationNumber.ToUpper();
                    findGroup.RelationshipManagerId = group.RelationshipManagerId;
                    findGroup.TelephoneNumber = group.TelephoneNumber;
                    findGroup.Village = group.Village;
                    findGroup.WardCode = group.WardCode;
                    findGroup.ProfessionId = group.ProfessionId;
                    findGroup.DateOfRegistration = group.DateOfRegistration;
                    findGroup.GovtDateOfRegistration = group.GovtDateOfRegistration;

                    groupService.UpdateGroup(findGroup, serviceHeader);

                    #endregion

                    //return RedirectToAction("EditSuccessful", "LoanOfficer");
                    return RedirectToAction("Index");
                }

                catch (RulesException ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);

                    ex.CopyTo(ModelState);
                    return View(group);
                }
            }

            return View(group);
        }

        public ActionResult EditSuccessful()
        {
            return View();
        }

        public ActionResult GroupSearchArray(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                id = string.Empty;
            }

            //var findAllCustomers = customerservice.GetAllCustomers(IsolationLevel.ReadUncommitted).Where(x => x.MobileNumber.ToLower().StartsWith(id.ToLower()));
            var findAllGroups = groupService.GetAllGroup(IsolationLevel.ReadUncommitted).Where(x => x.GroupNumber.ToLower().StartsWith(id.ToLower()));

            return Json(findAllGroups, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GroupHelperSearchArray(string GroupCode)
        {
            if (string.IsNullOrWhiteSpace(GroupCode))
            {
                GroupCode = string.Empty;
            }
            var findAllGroups = groupHelperService.GetAllGroupHelpers(IsolationLevel.ReadUncommitted).Where(x => x.GroupCode.ToLower().StartsWith(GroupCode.ToLower()));
            return Json(findAllGroups, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GroupHelperNameSearchArray(string GroupCode)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(GroupCode))
                {
                    GroupCode = string.Empty;
                }
                var findGroup = groupHelperService.GetAllGroupHelpers(IsolationLevel.ReadUncommitted).FirstOrDefault(x => x.GroupCode == GroupCode);

                return Content(findGroup.GroupName, "text/html");
            }
            catch(Exception e)
            {
                return Content("", "text/html");
            }
        }

        public ActionResult GroupLoanRequests(Guid Id)
        {
            ViewBag.Id = Id;

            var findcustomer = customerService.FindCustomerById(Id);

            ViewBag.Name = findcustomer.FullName;

            return View();
        }

        public JsonResult GroupLoanRequest(Guid Id, JQueryDataTablesModel datatablemodel)
        {
            var pageCollectionInfo = new PageCollectionInfo<LoanRequest>();
            var sortAscending = datatablemodel.sSortDir_.First() == "asc" ? true : false;
            var sortedColumns = (from s in datatablemodel.GetSortedColumns() select s.PropertyName).ToList();
            var AccountType = 200;
            var groupCustomerAccountId = customerAccountService.GetCustomerAccountNumberByAccountType(Id, AccountType).Id;
            pageCollectionInfo = loanrequestService.FindLoanRequestByGroupFilterInPage(groupCustomerAccountId, datatablemodel.iDisplayStart, datatablemodel.iDisplayLength, sortedColumns, datatablemodel.sSearch, sortAscending);
           

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(datatablemodel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;
            }

            return this.DataTablesJson(items: pageCollectionInfo.PageCollection, totalRecords: totalRecordCount, totalDisplayRecords: searchRecordCount, sEcho: datatablemodel.sEcho);
            
        }


        public static IList<LoanRequest> GroupLoanRequest(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanRequest> GroupLoanRequest)
        {
            var GetCustomerLoanRequest = GroupLoanRequest;

            totalRecordCount = GetCustomerLoanRequest.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomerLoanRequest = GetCustomerLoanRequest.Where(c => c.CustomerAccount.Customer.FullName
                    .ToLower().Contains(searchString.ToLower()) || c.LoanReferenceNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetCustomerLoanRequest.Count;

            IOrderedEnumerable<LoanRequest> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "LoanProduct.ProductName":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName);
                        break;

                    case "LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber);
                        break;

                    case "LoanAmount":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount);
                        break;

                    case "InterestAmount":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount);
                        break;

                    case "StatusDesc":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }
        #region Helpers
        [NonAction]
        protected List<SelectListItem> GetProfessionSelectList(string selectedValue)
        {
            List<SelectListItem> professionSelectList = new List<SelectListItem> { };

            var defaultProfessionItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = "Choose Main Activity", Value = string.Empty };

            professionSelectList.Add(defaultProfessionItem);

            var otherProfessionItems = professionService.GetAllProfessions(IsolationLevel.ReadCommitted);

            if (otherProfessionItems != null)
            {
                foreach (var item in otherProfessionItems)
                {
                    professionSelectList.Add(new SelectListItem { Selected = item.Id.ToString() == selectedValue, Text = item.ProfessionName, Value = item.Id.ToString() });
                }
            }
            return professionSelectList;
        }

        protected List<SelectListItem> GroupTypeSelectList(string selectedValue)
        {
            List<SelectListItem> groupTypeSelectList = new List<SelectListItem> { };

            var defaultGroupType = new SelectListItem { Selected = (selectedValue == string.Empty), Text = "Choose Group Type", Value = string.Empty };

            groupTypeSelectList.Add(defaultGroupType);

            var otherProfessionItems = professionService.GetAllProfessions(IsolationLevel.ReadCommitted);

            if (otherProfessionItems != null)
            {
                foreach (var item in otherProfessionItems)
                {
                    groupTypeSelectList.Add(new SelectListItem { Selected = item.Id.ToString() == selectedValue, Text = item.ProfessionName, Value = item.Id.ToString() });
                }
            }
            return groupTypeSelectList;
        } 

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadBranchesByBank(string id)
        {
            var branchList = GetBranchesList(id);

            var modelData = branchList.Select(m => new SelectListItem()
            {
                Text = m.BranchName,
                Value = m.Code,
            });

            return Json(modelData, JsonRequestBehavior.AllowGet);
        }

        

        [NonAction]
        public List<SelectListItem> BranchesSelectList(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultvalue = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Branch / County", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultvalue);

            var otherlist = branchservice.GetAllBranches().Where(x => x.IsEnabled == true);

            foreach (var item in otherlist)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.BranchName, Value = item.Id.ToString("D") });

            return SelectList;
        }
        //
        // GET: /Base/Error

        [NonAction]
        public List<SelectListItem> Loanproductlist(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultloan = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Loan Product", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultloan);

            var otherloanlist = loanproductservice.GetAllLoanProducts(IsolationLevel.ReadCommitted).Where(x => x.IsEnabled == true);

            foreach (var item in otherloanlist)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.LoanProductDescription, Value = item.Id.ToString("D") });

            return SelectList;
        }

        [NonAction]
        public List<SelectListItem> GetCustomerlist(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultcustomer = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Customer", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultcustomer);

            var othercustomers = customerService.GetAllCustomers(IsolationLevel.ReadUncommitted).Where(x => x.IsEnabled == true && x.IsApproved == true);

            if (string.IsNullOrWhiteSpace(selectedvalue))
            {
                selectedvalue = Guid.Empty.ToString();
            }
            foreach (var item in othercustomers)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.FullNamemobileNumber, Value = item.Id.ToString("D") });

            return SelectList;
        }

        public List<SelectListItem> RelationshipManagerSelectList(string selectdValue)
        {
            var currentUser = User.Identity.Name;
            var findCurrentUserInfo = userInfoService.FindUserByUserName(currentUser);
            List<SelectListItem> selectList = new List<SelectListItem>();

            var defaultRM = new SelectListItem { Selected = (selectdValue == Guid.Empty.ToString("D")), Text = "Select Relationship Manager", Value = Guid.Empty.ToString("D") };

            selectList.Add(defaultRM);
            if (findCurrentUserInfo != null)
            {
                var otherRM = relationshipManagerService.FindRelationshipManagerByBranchId(findCurrentUserInfo.BranchId).Where(x => x.IsEnabled == true);

                foreach (var item in otherRM)
                    selectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectdValue)), Text = item.RMFullName, Value = item.Id.ToString("D") });
            }


            return selectList;
        }

        public List<SelectListItem> RelationshipManagerSelectList2(string selectdValue, Guid BranchId)
        {
            //var currentUser = User.Identity.Name;
            //var findCurrentUserInfo = userInfoService.FindUserByUserName(currentUser);
            List<SelectListItem> selectList = new List<SelectListItem>();

            var defaultRM = new SelectListItem { Selected = (selectdValue == Guid.Empty.ToString("D")), Text = "Select Relationship Manager", Value = string.Empty };

            selectList.Add(defaultRM);
            //if (findCurrentUserInfo != null)
            //{
            var otherRM = relationshipManagerService.FindRelationshipManagerByBranchId(BranchId);
            if (otherRM != null)
            {
                foreach (var item in otherRM)
                    selectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectdValue)), Text = item.RMFullName, Value = item.Id.ToString() });
            }
            //}
            //(Guid)findGroup.BranchId

            return selectList;
        }

        public List<CheckBoxListInfo> EFTTypeInfo(LoanRequest loanRequest)
        {
            List<CheckBoxListInfo> eftInfo = new List<CheckBoxListInfo>();
            eftInfo.Add(new CheckBoxListInfo(((int)Enumerations.EFTType.AccountToMobile).ToString(), EnumHelper.GetDescription(Enumerations.EFTType.AccountToMobile), loanRequest != null ? loanRequest.Type == Enumerations.EFTType.AccountToMobile : true));
            eftInfo.Add(new CheckBoxListInfo(((int)Enumerations.EFTType.BusinessToBusiness).ToString(), EnumHelper.GetDescription(Enumerations.EFTType.BusinessToBusiness), loanRequest != null ? loanRequest.Type == Enumerations.EFTType.BusinessToBusiness : false));                        

            return eftInfo;
        }

        [NonAction]
        public List<TargetBank> GetBankPaybillList()
        {
            var bankCodes = new List<string>();

            var bankNames = new List<string>();

            var branchCodes = new List<string>();

            var payBills = new List<string>();

            string bankBranchesFileName = string.Format(@"{0}Content\uploads\BanksPayBillNumbers.xls", AppDomain.CurrentDomain.BaseDirectory);

            using (FileStream file = new FileStream(bankBranchesFileName, FileMode.Open, FileAccess.Read))
            {
                HSSFWorkbook hssfWorkbook = new HSSFWorkbook(file);

                if (hssfWorkbook != null)
                {
                    ISheet sheet = hssfWorkbook.GetSheetAt(0);

                    if (sheet != null)
                    {
                        IEnumerator rows = sheet.GetRowEnumerator();

                        if (rows != null)
                        {
                            rows.MoveNext(); // skip header row

                            int rowCounter = default(int);

                            while (rows.MoveNext())
                            {
                                IRow row = (HSSFRow)rows.Current;

                                ICell cell1Value = row.GetCell(0);  // BankCode
                                ICell cell2Value = row.GetCell(1);  // BankName
                                ICell cell3Value = row.GetCell(2);  // Status
                                ICell cell4Value = row.GetCell(3);  // PayBillNumber
                                ICell cell5Value = row.GetCell(4);  // KeyWord

                                object col1Value = cell1Value != null ? cell1Value.ToString() : null;
                                object col2Value = cell2Value != null ? cell2Value.ToString() : null;
                                object col3Value = cell3Value != null ? cell3Value.ToString() : null;
                                object col4Value = cell4Value != null ? cell4Value.ToString() : null;
                                object col5Value = cell5Value != null ? cell5Value.ToString() : null;

                                if (!bankCodes.Contains(col1Value.ToString().Trim()))
                                {
                                    bankCodes.Add(col1Value.ToString().Trim());
                                }

                                if (!bankNames.Contains(col2Value.ToString().Trim()))
                                    bankNames.Add(col2Value.ToString().Trim());

                                //if (!payBills.Contains(col4Value.ToString().Trim()))
                                payBills.Add(col4Value.ToString().Trim());

                                rowCounter += 1;
                            }
                        }
                    }
                }
            }

            #region branches

            var tupleList = new List<Tuple<long, long>>();

            #endregion

            #region banks

            var banksList = new List<TargetBank>();

            var bankCounter = 0;

            foreach (var bankName in bankNames)
            {
                var bank = new TargetBank { Code = bankCodes[bankCounter], Name = bankName, PayBillNumber = payBills[bankCounter] };

                banksList.Add(bank);

                bankCounter += 1;
            }

            #endregion

            return banksList.OrderBy(x => x.Name).ToList();
        }

        [NonAction]
        public string GetBranchesPaybillList(string bankCode)
        {
            var banks = GetBankPaybillList();

            if (banks != null)
            {
                var targetBank = (from b in banks
                                  where b.Code == bankCode
                                  select b).FirstOrDefault();

                if (targetBank != null)
                    return targetBank.PayBillNumber;
                else return null;
            }
            else return null;
        }
        #endregion
    }

    public class TargetBank
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string PayBillNumber { get; set; }
    }

    public class TargetBranch
    {
        public string Code { get; set; }

        public string PayBillNumber { get; set; }
    }
}
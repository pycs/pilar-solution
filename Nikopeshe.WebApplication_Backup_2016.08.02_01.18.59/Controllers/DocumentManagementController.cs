﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.InterfaceImplementations;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class DocumentManagementController : Controller
    {
        private readonly ICustomerDocumentService customerdocumentservice;
        private readonly ICustomerService customerservice;
        private readonly ILoanRequestService loanrequestservice;
        private readonly ILoanRequestDocumentsService loanrequestdocumentservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public DocumentManagementController(ICustomerDocumentService _customerdocumentservice, ILoanRequestService _loanrequestservice,ICustomerService _customerservice, ILoanRequestDocumentsService _loanrequestdocumentservice)
        {
            this.loanrequestdocumentservice = _loanrequestdocumentservice;
            this.loanrequestservice = _loanrequestservice;
            this.customerdocumentservice = _customerdocumentservice;
            this.customerservice = _customerservice;
        }
        // GET: DocumentManagement
      
        public FileResult View(Guid Id)
        {
            var document = customerdocumentservice.FindCustomerDocumentById(Id);

            return File(document.Image, "application/pdf");
        }

        public FileResult LoanRequestDocumentView(Guid Id)
        {
            var document = loanrequestdocumentservice.FindLoanRequestDocumentById(Id);

            return File(document.Image, "application/pdf");
        }

        public FileResult CustomerDocumentsView(Guid Id)
        {
            var document = customerdocumentservice.FindCustomerDocumentById(Id);

            return File(document.Image, "application/pdf");
        }

        public PartialViewResult PDFPartialView()
        {
            return PartialView();
        }

        [HttpGet]
        public ActionResult UploadFile(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException("Id is required.");
            }

            ViewBag.Id = id;
            var findcustomer = customerservice.FindCustomerById(id);
            if (findcustomer == null)
            {
                ModelState.AddModelError(string.Empty, "Failed while retrieving customers information.");
                return View();
            }
            ViewBag.CustomerName = findcustomer.FullName;
            ViewBag.MobileNumber = findcustomer.MobileNumber;
            ViewBag.IdNumber = findcustomer.IDNumber;

            //var customerlist = CustomerSelectList(Guid.Empty.ToString());
            //ViewBag.CustomerSelectList = customerlist;
            return View();
        }

        /// <summary>
        /// Upload Customer Document
        /// </summary>
        /// <returns></returns>
        ///
        [HttpPost]        
        public ActionResult UploadFile(Guid id, CustomerDocument customerdocument, HttpPostedFileBase PostedFile)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException("Id is required.");
            }

            ViewBag.Id = id;
            var findcustomer = customerservice.FindCustomerById(id);
            if (findcustomer == null)
            {
                ModelState.AddModelError(string.Empty, "Failed while retrieving customers information.");
                return View();
            }
            ViewBag.CustomerName = findcustomer.FullName;
            ViewBag.MobileNumber = findcustomer.MobileNumber;
            ViewBag.IdNumber = findcustomer.IDNumber;
            //var customerlist = CustomerSelectList(Guid.Empty.ToString());
            //ViewBag.CustomerSelectList = customerlist;                        

            //if (customerdocument.CustomerId == Guid.Empty)
            //{
            //    ModelState.AddModelError(string.Empty, "You must select a customer");
            //    return View(customerdocument);
            //}
            if (ModelState.IsValid)
            {
                try
                {                    
                    if(PostedFile != null && PostedFile.ContentLength > 0)
                    {
                        // extract only the fielname
                        var fileName = Path.GetFileName(PostedFile.FileName);
                        // store the file inside ~/App_Data/uploads folder
                        var path = Path.Combine(Server.MapPath("~/Content/Uploads"), fileName);
                        var getfileextension = Path.GetExtension(path);
                        if (getfileextension != ".pdf")
                        {
                            ModelState.AddModelError(string.Empty, "Wrong file type. The file must have .pdf extension");
                            return View(customerdocument);
                        }
                                                
                        PostedFile.SaveAs(path);
                        var fileData = new MemoryStream();
                        PostedFile.InputStream.CopyTo(fileData);
                        customerdocument.Image = fileData.ToArray();
                        customerdocument.CustomerId = findcustomer.Id;
                        customerdocument.Id = Guid.NewGuid();
                        customerdocumentservice.AddNewCustomerDocument(customerdocument, serviceHeader);
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "You must select a file.");
                        return View(customerdocument);
                    }                    
                }

                catch(Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(customerdocument);
                }
            }
            return View("UploadSuccess");
        }

        public ActionResult UploadSuccess()
        {
            return View();
        }

        public ActionResult CustomerDocuments(Guid Id)
        {
            ViewBag.Id = Id;
            var findcustomerr = customerservice.FindCustomerById(Id);
            ViewBag.CustomerId = findcustomerr.Id;
            ViewBag.CustomerName = findcustomerr.FullName;
            return View();
        }

        public JsonResult GetCustomerDocuments(JQueryDataTablesModel datatablemodel, Guid Id)
        {
            var customerdocuments = customerdocumentservice.GetAllCustomerDocuments(Id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetAllCustomerDocument(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, CustomerDocumentList: customerdocuments);


            var results = Json(new JQueryDataTablesResponse<CustomerDocumentView>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));

            results.MaxJsonLength = Int32.MaxValue;

            return results;
        }

        public static IList<CustomerDocumentView> GetAllCustomerDocument(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<CustomerDocumentView> CustomerDocumentList)
        {
            var GetCustomerDocumentList = CustomerDocumentList;

            totalRecordCount = GetCustomerDocumentList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomerDocumentList = CustomerDocumentList.Where(c => c.Customer.IDNumber
                    .ToLower().Contains(searchString.ToLower())
                    || c.Customer.MobileNumber.ToLower().Contains(searchString.ToLower())
                    ).ToList();            
            }

            searchRecordCount = GetCustomerDocumentList.Count;

            IOrderedEnumerable<CustomerDocumentView> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "Customer.FullName":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Customer.FullName);
                        break;

                    case "Customer.IDNumber":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.Customer.IDNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Customer.IDNumber);
                        break;

                    case "Customer.MobileNumber":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.Customer.MobileNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Customer.MobileNumber);
                        break;

                    //case "DocumentTypeDesc":
                    //    sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.DocumentTypeDesc)
                    //        : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.DocumentTypeDesc);
                    //    break;                    
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }


        public ActionResult LoanRequestDocuments(Guid id)
        {
            ViewBag.Id = id;
            var findloanrequestbyid = loanrequestservice.GetLoanRequestById(id);
            ViewBag.CustomerFullName = findloanrequestbyid.CustomerAccount.Customer.FullName;
            ViewBag.LoanRefNo = findloanrequestbyid.LoanReferenceNumber;            

            return View();
        }

        public JsonResult GetLoanRequestDocuments(JQueryDataTablesModel datatablemodel, Guid Id)
        {
            var customerdocuments = loanrequestdocumentservice.GetAllLoanRequestDocumentsByLoanRequestId(Id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetAllLoanRequestDocuments(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, CustomerLoanDocumentList: customerdocuments);


            var results = Json(new JQueryDataTablesResponse<LoanRequestDocumentsService.LoanRequestDocumentView>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));

            results.MaxJsonLength = Int32.MaxValue;

            return results;
        }

        public static IList<LoanRequestDocumentsService.LoanRequestDocumentView> GetAllLoanRequestDocuments(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanRequestDocumentsService.LoanRequestDocumentView> CustomerLoanDocumentList)
        {
            var GetCustomerDocumentList = CustomerLoanDocumentList;

            totalRecordCount = GetCustomerDocumentList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomerDocumentList = CustomerLoanDocumentList.Where(c => c.LoanRequest.LoanReferenceNumber
                    .ToLower().Contains(searchString.ToLower())
                    || c.LoanRequest.CustomerAccount.Customer.FullName.ToLower().Contains(searchString.ToLower())
                    ).ToList();
            }

            searchRecordCount = GetCustomerDocumentList.Count;

            IOrderedEnumerable<LoanRequestDocumentsService.LoanRequestDocumentView> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "LoanRequest.CustomerAccount.Customer.FullName.FullName":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.CustomerAccount.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.CustomerAccount.Customer.FullName);
                        break;

                    case "LoanRequest.LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.LoanReferenceNumber);
                        break;

                    case "DocumentTypeDesc":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.DocumentTypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.DocumentTypeDesc);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult UploadLoanApplicationDocuments(Guid id)
        {
            if (id == Guid.Empty)
            {
                ModelState.AddModelError(string.Empty, "Invalid ID.");
                return View();
            }
            var findloanrequestbyid = loanrequestservice.GetLoanRequestById(id);
            if (findloanrequestbyid == null)
            {
                ModelState.AddModelError(string.Empty, "Failed while retrieving customers information. Could not find the customer.");
                return View();
            }
            ViewBag.CustomerName = findloanrequestbyid.CustomerAccount.Customer.FullName;
            ViewBag.LoanAmount = string.Concat(findloanrequestbyid.LoanAmount);
            ViewBag.LoanReferenceNumber = findloanrequestbyid.LoanReferenceNumber;
            ViewBag.CustomerFullName = findloanrequestbyid.CustomerAccount.Customer.FullName;            

            var customerlist = CustomerSelectList(Guid.Empty.ToString());
            ViewBag.CustomerSelectList = customerlist;
            return View();
        }

        /// <summary>
        /// Upload Loan Request Document
        /// </summary>
        /// <param name="loanrequestdocument"></param>
        /// <returns></returns>
        [HttpPost]        
        public ActionResult UploadLoanApplicationDocuments(Guid id, LoanRequestDocument loanrequestdocument, HttpPostedFileBase PostedFile, FormCollection collection)
        {
            if (loanrequestdocument.Id == Guid.Empty)
            {
                ModelState.AddModelError(string.Empty, "Invalid ID.");
                return View();
            }
            var findloanrequestbyid = loanrequestservice.GetLoanRequestById(id);
            if (findloanrequestbyid == null)
            {
                ModelState.AddModelError(string.Empty, "Failed while retrieving customers information. Could not find the customer.");
                return View();
            }
            ViewBag.CustomerName = findloanrequestbyid.CustomerAccount.Customer.FullName;
            ViewBag.LoanAmount = string.Concat(findloanrequestbyid.LoanAmount);
            ViewBag.LoanReferenceNumber = findloanrequestbyid.LoanReferenceNumber;                        

            var customerlist = CustomerSelectList(Guid.Empty.ToString());
            ViewBag.CustomerSelectList = customerlist;
            
            try
            {                
                if (PostedFile != null && PostedFile.ContentLength > 0)
                {
                    // extract only the filename
                    var fileName = Path.GetFileName(PostedFile.FileName);
                    // store the file inside ~/App_Data/uploads folder
                    var path = Path.Combine(Server.MapPath("~/Content/Uploads"), fileName);
                    var getfileextension = Path.GetExtension(path);
                    if (getfileextension != ".pdf")
                    {
                        ModelState.AddModelError(string.Empty, "Wrong file type. The file must have .pdf extension");
                        return View(loanrequestdocument);
                    }

                    PostedFile.SaveAs(path);
                    var fileData = new MemoryStream();
                    PostedFile.InputStream.CopyTo(fileData);
                    loanrequestdocument.LoanRequestId = findloanrequestbyid.Id;
                    loanrequestdocument.Image = fileData.ToArray();
                    loanrequestdocument.Id = Guid.NewGuid();
                    loanrequestdocumentservice.AddNewLoanRequestDocuments(loanrequestdocument, serviceHeader);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "You must select a file.");
                    return View(loanrequestdocument);
                }
                return View("UploadSuccess");
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View(loanrequestdocument);
            }            
        }

        [NonAction]
        public List<SelectListItem> CustomerSelectList(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultcustomer = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Customer", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultcustomer);

            var othercustomerlist = customerservice.GetAllCustomers();

            foreach (var item in othercustomerlist)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.FullNamemobileNumber, Value = item.Id.ToString("D") });

            return SelectList;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult CustomerLoanSelectList(string id)
        {           
            var accountList = GetAllCustomerLoans(id);

            var modelData = accountList.Select(m => new SelectListItem()
            {
                Text = m.Text,
                Value = m.Value,
                Selected = m.Selected,
            });

            return Json(modelData, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public List<SelectListItem> GetAllCustomerLoans(string selectedValue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultItem = new SelectListItem { Selected = (string.Concat(selectedValue) == string.Empty), Text = "Select", Value = string.Empty };

            SelectList.Add(defaultItem);
            
            var otherItems = loanrequestservice.GetAllCustomerActiveLoanRequests(Guid.Parse(selectedValue));

            if (otherItems != null)
            {
                foreach (var item in otherItems)
                {
                    SelectList.Add(new SelectListItem { Selected = item.Id.ToString() == selectedValue, Text = item.CustomerIdNumberLoanRefMobileNumber, Value = item.Id.ToString() });
                }
            }
            return SelectList;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Entity.DataModels
{
    public class Partner : BaseEntity
    {
        [Display(Name = "Partner Name")]
        [Required]
        public string PartnerName { get; set; }

        [Display(Name="Contact Person")]
        public string ContactPerson { get; set; }

        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }

        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [Display(Name = "Commission")]
        public decimal Commission { get; set; }

        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Display(Name = "Paybill Number")]
        public string PaybillNumber { get; set; }

        [Display(Name = "Settlement Date")]
        public DateTime SettlementDate { get; set; }

        [Display(Name = "Interest Charge Rate")]
        public decimal InterestChargeRate { get; set; }

        public string Commands { get; set; }

        [Display(Name = "Merchant C2B Number")]
        public string MerchantId { get; set; }

        [Display(Name="Passkey")]
        public string PassKey { get; set; }
    }
}

﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;


namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize(Roles = "Administrator")]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class GraduatedScaleController : Controller
    {
        private readonly IGraduatedScaleService graduatesscaleservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        private readonly IChargesService chargeservice;

        public GraduatedScaleController(IGraduatedScaleService graduatesscaleservice, IChargesService _chargeservice)
        {
            if (_chargeservice == null || graduatesscaleservice == null)
                throw new ArgumentNullException("null service");

            this.graduatesscaleservice = graduatesscaleservice;
            this.chargeservice = _chargeservice;
        }
        // GET: GraduatedScale
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAllGraduatedScale(JQueryDataTablesModel datatablemodel)
        {
            var pagecollection = graduatesscaleservice.GetAllGraduatedScales(IsolationLevel.ReadUncommitted);

            int totalrecordcount = 0;

            int searchrecordcount = 0;

            var transactionList = GetAllGraduatedScaleList(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalrecordcount, searchRecordCount: out searchrecordcount, searchString: datatablemodel.sSearch, GraduatedScaleList: pagecollection);

            return Json(new JQueryDataTablesResponse<GraduatedScale>(items: transactionList,
                totalRecords: totalrecordcount,
                totalDisplayRecords: searchrecordcount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<GraduatedScale> GetAllGraduatedScaleList(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<GraduatedScale> GraduatedScaleList)
        {
            var GetCustomersList = GraduatedScaleList;

            totalRecordCount = GetCustomersList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomersList = GetCustomersList.Where(c => c.Charge.ChargeName
                    .ToLower().Contains(searchString.ToLower())
                    || c.Charge.ChartofAccount.ChartofAccountDescription.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetCustomersList.Count;

            IOrderedEnumerable<GraduatedScale> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "Charge.ChargeName":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.Charge.ChargeName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Charge.ChargeName);
                        break;

                    case "LowerLimit":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.LowerLimit)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LowerLimit);
                        break;

                    case "UpperLimit":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.UpperLimit)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.UpperLimit);
                        break;

                    case "ChargeTypeDesc":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.ChargeTypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChargeTypeDesc);
                        break;

                    case "Value":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.Value)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Value);
                        break;

                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult CreateNewGraduatedScale()
        {
            ViewBag.ChargeSelectList = ChargeSelectList(Guid.Empty.ToString());

            return View();
        }

        [HttpPost]        
        public ActionResult CreateNewGraduatedScale(GraduatedScale graduatedscale)
        {
            ViewBag.ChargeSelectList = ChargeSelectList(Guid.Empty.ToString());
            if (ModelState.IsValid)
            {
                try
                {
                    var findchargebyid = chargeservice.FindChargeById(graduatedscale.ChargeId);
                    GraduatedScale validategraduatedscale = graduatesscaleservice.ValidateGraduatedScale(graduatedscale.ChargeId, graduatedscale.LowerLimit, graduatedscale.UpperLimit);
                    if (validategraduatedscale == null)
                    {                                                                      
                        if (graduatedscale.ChargeId == Guid.Empty)
                        {
                            ModelState.AddModelError(string.Empty, "Charge cannot be null.");
                            return View(graduatedscale);
                        }

                        graduatesscaleservice.AddNewGraduatedScale(graduatedscale, serviceHeader); 
                        return RedirectToAction("Index");
                    }
                                        
                    else
                    {
                        ModelState.AddModelError(string.Empty, string.Format("There is a graduated scale of charge type {0} with {1} as Lower Limit and {2} as Upper Limit.", validategraduatedscale.Charge.ChargeName, validategraduatedscale.LowerLimit, validategraduatedscale.UpperLimit));
                        return View(graduatedscale);
                    }                    
                }

                catch(Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    
                    return View(graduatedscale);
                }
            }
            return View(graduatedscale);
        }

        [HttpGet]        
        public ActionResult UpdateGraduatedScale(Guid Id)
        {
            ViewBag.ChargeSelectList = ChargeSelectList(Guid.Empty.ToString());
            var findgraduatedscalebyid = graduatesscaleservice.FindGraduatedScaleById(Id);
            
            return View(findgraduatedscalebyid);
        }

        [HttpPost]        
        public ActionResult UpdateGraduatedScale(GraduatedScale graduatedscale)
        {
            ViewBag.ChargeSelectList = ChargeSelectList(Guid.Empty.ToString());
            var findgraduatedscalebyid = graduatesscaleservice.FindGraduatedScaleById(graduatedscale.Id);
            if (findgraduatedscalebyid == null)
            {
                ModelState.AddModelError(string.Empty, "Failed while retrieving graduated scale details.");
                return View(graduatedscale);
            }           
            
            if (ModelState.IsValid)
            {
                try
                {
                    if (graduatesscaleservice.CountGraduatedScaleWithChargeAndRange(findgraduatedscalebyid.ChargeId, findgraduatedscalebyid.LowerLimit, findgraduatedscalebyid.UpperLimit).Count() >= 1)
                    {                        
                        findgraduatedscalebyid.ChargeId = graduatedscale.ChargeId;
                        findgraduatedscalebyid.Value = graduatedscale.Value;
                        findgraduatedscalebyid.ChargeType = graduatedscale.ChargeType;
                        findgraduatedscalebyid.LowerLimit = graduatedscale.LowerLimit;
                        findgraduatedscalebyid.UpperLimit = graduatedscale.UpperLimit;
                        
                        if (graduatedscale.ChargeId == null)
                        {
                            ModelState.AddModelError(string.Empty, "Charge cannot be null.");
                            return View(graduatedscale);
                        }
                        
                        graduatesscaleservice.UpdateGraduatedScale(findgraduatedscalebyid, serviceHeader);
                        return RedirectToAction("Index");
                    }
                                        
                    else
                    {
                        ModelState.AddModelError(string.Empty, string.Format("There is a graduated scale of charge type{0} with {1} as Lower Limit and {2} as Upper Limit.", findgraduatedscalebyid.Charge.ChargeName, graduatedscale.LowerLimit, graduatedscale.UpperLimit));
                        return View(graduatedscale);
                    }                    
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(graduatedscale);
                }
            }
            return View();
        }      


        [NonAction]
        public List<SelectListItem> ChargeSelectList(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultvalue = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Charge", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultvalue);

            var otherlist = chargeservice.GetAllCharges().Where(x => x.IsEnabled == true);

            foreach (var item in otherlist)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.ChargeName, Value = item.Id.ToString("D") });

            return SelectList;
        }
    }
}
﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{
    public class ReportInRoles : BaseEntity
    {
        [Index]
        public Guid ReportId { get; set; }

        public virtual Report Report { get; set; }

        [Display(Name = "Authorized Role(s)")]
        [Required]
        public int RoleCategory { get; set; }

        [Display(Name = "Role Description")]
        public string RoleDescription
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.RoleCategory), RoleCategory) ? EnumHelper.GetDescription((Enumerations.RoleCategory)RoleCategory) : string.Empty;
            }
        }
    }
}

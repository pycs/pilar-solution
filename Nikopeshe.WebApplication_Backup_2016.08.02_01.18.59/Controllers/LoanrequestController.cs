﻿using Crosscutting.NetFramework.Logging;
using Infrastructure.Crosscutting.NetFramework;
using Nikopeshe.Data;
using Nikopeshe.Data.Mappings;
using Nikopeshe.Entity;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.InterfaceImplementations;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Security;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class LoanrequestController : BaseController
    {
        private readonly ILoanRequestService loanrequestService;
        private readonly ICustomerService customerservice;
        private readonly IEmailAlertService emailalerservice;
        private readonly ITextAlertService textalertservice;
        private readonly ISystemGeneralLedgerAccountMappingService systemglmappingservice;
        private readonly ISavingsProductService savingsproductservice;
        private readonly ICustomerAccountService customeraccountservice;
        private readonly ILoanProductService loanproductservice;
        private readonly ILoanTransactionHistoryService loantransactionhistoryservice;
        private readonly IChartofAccountService chartofaccountservice;
        private readonly ILoanProductsChargeService loanproductcharges;
        private readonly IUserInfoService userInfoService;
        private readonly IGraduatedScaleService graduatedscaleservice;
        private readonly ILoanRequestChargeService loanrequestchargeservice;
        private readonly ILoanProductsChargeService loanProductCharge;
        private readonly IStaticSettingService staticsettings;        
        private readonly ILoanRequestDocumentsService loanrequestdocumentservice;
        private readonly IRelationshipManagerService relationshipManagerService;
        private readonly ICustomerDocumentService customerdocumentservice;
        private readonly IBranchService branchService;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        private string TextSignature = ConfigurationManager.AppSettings["TextSignature"].ToString();
        public LoanrequestController(ILoanRequestService _loanrequestservice, ICustomerService _customerservice,
            ILoanProductService _loanproductservice, ISystemGeneralLedgerAccountMappingService _systemglmappingservice,
            ISavingsProductService _savingsproductservice, IChartofAccountService _chartofaccountservice,
            ICustomerAccountService _customeraccountservice, IEmailAlertService _emailalerservice,
            ITextAlertService _textalertservice, ILoanTransactionHistoryService _loantransactionhistoryservice,
            ILoanProductsChargeService _loanproductcharges, IGraduatedScaleService _graduatedscaleservice,
            ILoanRequestChargeService _loanrequestchargeservice, IStaticSettingService _staticsettings,            
            ILoanRequestDocumentsService _loanrequestdocumentservice, ICustomerDocumentService _customerdocumentservice,
            IUserInfoService _userInfoService, IBranchService _branchService, IRelationshipManagerService _relationshipManagerService)
        {
            if(_loanproductservice == null || _emailalerservice == null || _systemglmappingservice == null
                || _savingsproductservice == null || _chartofaccountservice == null || _textalertservice == null
                || _customeraccountservice == null || _loanrequestservice == null || _customerservice == null 
                || _loantransactionhistoryservice == null || _loanproductcharges == null || _graduatedscaleservice == null
                || _loanrequestchargeservice == null || _customerdocumentservice == null || _userInfoService == null || _branchService == null)
                throw new ArgumentNullException("null service reference");

            this.loanproductservice = _loanproductservice;
            this.emailalerservice = _emailalerservice;
            this.systemglmappingservice = _systemglmappingservice;
            this.savingsproductservice = _savingsproductservice;
            this.chartofaccountservice = _chartofaccountservice;
            this.textalertservice = _textalertservice;
            this.customeraccountservice = _customeraccountservice;
            this.loanrequestService = _loanrequestservice;
            this.customerservice = _customerservice;
            this.loantransactionhistoryservice = _loantransactionhistoryservice;
            this.loanproductcharges = _loanproductcharges;
            this.graduatedscaleservice = _graduatedscaleservice;
            this.loanrequestchargeservice = _loanrequestchargeservice;
            this.staticsettings = _staticsettings;
            this.loanrequestdocumentservice = _loanrequestdocumentservice;
            this.customerdocumentservice = _customerdocumentservice;
            this.userInfoService = _userInfoService;
            this.branchService = _branchService;
            this.relationshipManagerService = _relationshipManagerService;            
        }

        public IList<LoanScheduleModel> LoanScheduler(Guid loanrequestid)
        {
            decimal recurringCharges = 0m;
            bool IsLoanMigrated = false;
            decimal interestamount = 0m;
            DataContext datacontext = new DataContext();
            decimal principleamount= 0m;
            decimal recurringChargePerInstallment = 0;
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(loanrequestid);
            var term = findloanrequestbyid.NegotiatedInstallments;
            var getproduct = loanproductservice.FindLoanProductById(findloanrequestbyid.LoanProductId);
            var findLoanProductRecurringCharges = loanproductcharges.GetAllLoanProductCharges(findloanrequestbyid.LoanProductId).Where(x => x.LoanProductId == findloanrequestbyid.LoanProductId && x.Charges.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment).ToList();
            if (findLoanProductRecurringCharges != null && findLoanProductRecurringCharges.Any())
            {
                foreach (var loanCharge in findLoanProductRecurringCharges)
                {
                    var findgradutedscale = (from a in datacontext.GraduatedScale
                                             where a.ChargeId == loanCharge.ChargeId && (a.LowerLimit <= findloanrequestbyid.LoanAmount && a.UpperLimit >= findloanrequestbyid.LoanAmount && a.Charge.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment)
                                             select a).ToList();
                    if (findgradutedscale != null)
                    {
                        foreach (var graduatedScale in findgradutedscale)
                        {
                            if (graduatedScale.ChargeType == Enumerations.ChargeTypes.FixedAmount)
                            {
                                recurringCharges = graduatedScale.Value * findloanrequestbyid.NegotiatedInstallments;
                            }
                            else
                            {
                                recurringCharges = (graduatedScale.Value / 100) * findloanrequestbyid.NegotiatedInstallments * findloanrequestbyid.LoanAmount;
                            }
                        }
                    }
                }
            }
            
            List<LoanScheduleModel> loanschedule = new List<LoanScheduleModel>();
           
            IsLoanMigrated = findloanrequestbyid.IsMigrated;

            if(IsLoanMigrated == false)
            {
                interestamount = findloanrequestbyid.InterestAmount;
                principleamount = findloanrequestbyid.LoanAmount;
            }
            else
            {                
                principleamount = findloanrequestbyid.OutstandingLoanBalance;
            }
            
            decimal interperschedule = Math.Round(interestamount / term,2);
            if (recurringCharges != 0m)
            {
                recurringChargePerInstallment = recurringCharges / findloanrequestbyid.NegotiatedInstallments;
            }
            decimal totalInterPerInstallment = recurringChargePerInstallment + interperschedule;
            decimal hx = (totalInterPerInstallment) / term;
            decimal principleperschedule = Math.Round(principleamount / term, 2);

            decimal totalrepaymentamount = principleperschedule + totalInterPerInstallment;            

            decimal principalperschedule = 0;                      

            DateTime nextpaymentdate = DateTime.Now;
            
            if(findloanrequestbyid.DisbursementDate == null)
            {
                nextpaymentdate = findloanrequestbyid.LoanStartDate;
            }
            else
            {
                nextpaymentdate = DateTime.Parse(findloanrequestbyid.DisbursementDate.ToString());
            }
            decimal interestcharges = interestamount + recurringCharges;
            for (decimal y = term - 1; term > 0; term --)
            {                
                if (findloanrequestbyid.LoanProduct.PaymentFrequencyType == Enumerations.PaymentFrequency.Weekly)
                {
                    nextpaymentdate = nextpaymentdate.AddDays(7);                    
                }

                else
                {
                    nextpaymentdate = nextpaymentdate.AddMonths(1);
                }
                
                decimal olb = principleamount - principalperschedule + interestcharges;
                recurringCharges -= recurringChargePerInstallment;
                interestamount -= interperschedule;
                interestcharges -= totalInterPerInstallment;
                principalperschedule += principleperschedule;

                loanschedule.Add(new LoanScheduleModel() { NextPaymentDate = nextpaymentdate, TotalAmount = totalrepaymentamount, PrincipleAmount = principleperschedule, OutstandingLoanBalance = olb, InterestAmount = totalInterPerInstallment });                
            }

            return loanschedule;
        }
       

        [Authorize(Roles = "Administrator")]
        public ActionResult AllLoanRequest()
        {            
            return View();
        }
        
        public JsonResult GetAllLoanRequest(JQueryDataTablesModel datatablemodel)
        {
            var allloanrequestpagecollection = loanrequestService.GetAllLoanRequests();

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetAllLoanRequest(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, LoanRequestList: allloanrequestpagecollection);


            var results = Json(new JQueryDataTablesResponse<LoanRequest>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));

            return results;
        }


        public static IList<LoanRequest> GetAllLoanRequest(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanRequest> LoanRequestList)
        {
            var GetLoanRequestList = LoanRequestList;

            totalRecordCount = GetLoanRequestList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetLoanRequestList = GetLoanRequestList.Where(c => c.LoanProduct.ProductName
                    .ToLower().Contains(searchString.ToLower())
                    || c.LoanReferenceNumber.ToLower().Contains(searchString.ToLower()) 
                    || c.CustomerAccount.Customer.FullName.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetLoanRequestList.Count;

            IOrderedEnumerable<LoanRequest> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "CustomerAccount.Customer.FullName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName);
                        break;

                    case "CustomerAccount.Customer.IDNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.IDNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.IDNumber);
                        break;

                    case "CustomerAccount.Customer.MobileNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.MobileNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.MobileNumber);
                        break;

                    case "LoanProduct.ProductName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName);
                        break;

                    case "LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber);
                        break;

                    case "LoanAmount":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount);
                        break;

                    case "InterestAmount":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount);
                        break;

                    case "StatusDesc":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc);
                        break;

                    case "Branch.BranchName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName);
                        break;
                   
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        #region Loan Request List Loan Officer

        [Authorize(Roles = "LoanOfficer")]
        public ActionResult LoanRequestList()
        {
            return View();
        }

        public JsonResult GetAllLoanRequestList(JQueryDataTablesModel datatablemodel)
        {
            var allloanrequestpagecollection = new List<LoanRequest>();
            var CurrentUser = User.Identity.Name;
            var findCurrentUserDetails = userInfoService.FindUserByUserName(CurrentUser);
            if (findCurrentUserDetails != null)
            {
                allloanrequestpagecollection = loanrequestService.GetAllLoanRequests().Where(x => x.Branch.Id == findCurrentUserDetails.BranchId).ToList() ;
            }

            else
            {
                allloanrequestpagecollection = loanrequestService.GetAllLoanRequests().ToList();
            }            

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetAllLoanRequestList(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, LoanRequestList: allloanrequestpagecollection);


            var results = Json(new JQueryDataTablesResponse<LoanRequest>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));

            return results;
        }


        public static IList<LoanRequest> GetAllLoanRequestList(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanRequest> LoanRequestList)
        {
            var GetLoanRequestList = LoanRequestList;

            totalRecordCount = GetLoanRequestList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetLoanRequestList = GetLoanRequestList.Where(c => c.LoanProduct.ProductName
                    .ToLower().Contains(searchString.ToLower())
                    || c.LoanReferenceNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetLoanRequestList.Count;

            IOrderedEnumerable<LoanRequest> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "CustomerAccount.Customer.FullName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName);
                        break;

                    case "CustomerAccount.Customer.IDNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.IDNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.IDNumber);
                        break;

                    case "CustomerAccount.Customer.MobileNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.MobileNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.MobileNumber);
                        break;

                    case "LoanProduct.ProductName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName);
                        break;

                    case "LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber);
                        break;

                    case "LoanAmount":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount);
                        break;

                    case "InterestAmount":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount);
                        break;

                    case "StatusDesc":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc);
                        break;

                    case "Branch.BranchName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName);
                        break;
                }
            }


            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        #endregion

        [Authorize(Roles = "Administrator, LoanOfficer, FinanceManager")]
        public ActionResult LoanDisbursements()
        {
            return View();
        }

        public JsonResult LoanDisbursementList(JQueryDataTablesModel datatablemodel)
        {            
            var allloanrequestpagecollection = loanrequestService.GetAllLoanRequests().Where(x => x.Status != (int)Enumerations.LoanStatus.CustomerRejected || x.Status == (int)Enumerations.LoanStatus.Rejected).ToList();

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetAllLoanDisbursements(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, LoanRequestList: allloanrequestpagecollection);


            var results =  Json(new JQueryDataTablesResponse<LoanRequest>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));

            return results;
        }
        public static IList<LoanRequest> GetAllLoanDisbursements(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanRequest> LoanRequestList)
        {
            var GetLoanRequestList = LoanRequestList;

            totalRecordCount = GetLoanRequestList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetLoanRequestList = GetLoanRequestList.Where(c => c.LoanProduct.ProductName
                    .ToLower().Contains(searchString.ToLower())                                        
                    || c.LoanReferenceNumber.ToLower().Contains(searchString.ToLower())                                       
                    || c.B2MTransactionID.ToLower().Contains(searchString.ToLower())
                    ).ToList();
            }

            searchRecordCount = GetLoanRequestList.Count;

            IOrderedEnumerable<LoanRequest> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "CustomerAccount.Customer.FullName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName);
                        break;

                    case "CustomerAccount.Customer.MobileNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.MobileNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.MobileNumber);
                        break;
                   
                    case "LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber);
                        break;

                    case "LoanAmount":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount);
                        break;
                    
                    case "StatusDesc":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc);
                        break;

                    case "SPSStatusDesc":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.SPSStatusDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.SPSStatusDesc);
                        break;

                    case "B2MResponseDesc":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.B2MResponseDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.B2MResponseDesc);
                        break;

                    case "B2MResultDesc":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.B2MResultDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.B2MResultDesc);
                        break;                  

                    case "B2MTransactionID":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.B2MTransactionID)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.B2MTransactionID);
                        break;                   
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }
        [HttpGet]
        [Authorize]
        public ActionResult NewLoanRequest()
        {
            var username = User.Identity.Name;
            var getuserid = customerservice.FindCustomerByMobileNumber(username);
            var userid = getuserid.FullName;
            var customerlist = GetCustomerlist(Guid.Empty.ToString());
            var loanproductlist = DefaultLoanproductlist(Guid.Empty.ToString());
            ViewBag.CustomersList = customerlist;
            ViewBag.Username = userid;
            ViewBag.EFTTypes = EFTInfo(null);
            ViewBag.LoanProductsList = loanproductlist;
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            //ViewBag.BranchesSelectList = GetBranchesSelectList(string.Empty, string.Empty);
            var loanrequest = new LoanRequest()
            {
                NegotiatedInstallments = 0
            };
            return View(loanrequest);
        }

        [Authorize]        
        public ActionResult NewLoanRequest(int NegotiatedInstallments, [Bind(Exclude = "NegotiatedInstallments")]LoanRequest loanrequest)
        {
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            //ViewBag.BranchesSelectList = GetBranchesSelectList(string.Empty, string.Empty);
            var username = User.Identity.Name;
            var getuserid = customerservice.FindCustomerByMobileNumber(username);
            var userid = getuserid.FullName;
            var loanproductlist = DefaultLoanproductlist(Guid.Empty.ToString());
            ViewBag.LoanProductsList = loanproductlist;
            loanrequest.CustomerAccountId = getuserid.Id;
            ViewBag.Username = userid;
            ViewBag.EFTTypes = EFTInfo(null);
            var selectedtype = Request.Form["Type"];
            string paybillNumber = string.Empty;
            if (selectedtype == null)
            {
                ModelState.AddModelError(string.Empty, "You must select money transfer mode.");
                return View(loanrequest);
            }
            
            var efttype = int.Parse(selectedtype);
            
            if(loanrequest.LoanProductId == Guid.Empty)
            {
                ModelState.AddModelError(string.Empty, "You must select a product.");
                return View(loanrequest);
            }
            if (loanrequest.AcceptTermsAndConditions == false)
            {
                ModelState.AddModelError(string.Empty, "You must accept terms and conditions.");
                return View(loanrequest);
            }
            Branch branch = branchService.FindBranchByBranchCode("000");
            if (branch == null)
            {
                ModelState.AddModelError(string.Empty, "An error has occurred while processing. Please contact system administrator for assistance.");
                LoggerFactory.CreateLog().LogError("Error -> {0}", "No default branch found while processing this loan request.");
                return View(loanrequest);
            }
            
            Tuple<decimal, decimal, decimal> amountvalues = loanrequestService.GetMaxMinDefaultLoanAmount(loanrequest.LoanProductId);

            Tuple<int, decimal> termandinterestrate = loanproductservice.GetLoanTermandInterestRateByProductId(loanrequest.LoanProductId);
            loanrequest.BranchId = branch.Id;
            loanrequest.LoanEndDate = DateTime.Today.AddMonths(termandinterestrate.Item1);
                       
            string loanrefno = GenerateRandomLoanReferenceNumber(8);
            loanrequest.NegotiatedInstallments = NegotiatedInstallments;
            loanrequest.RegisteredBy = Request.RequestContext.HttpContext.User.Identity.Name;
            loanrequest.LoanStartDate = DateTime.Now;
            if(ModelState.IsValid)
            {
                if (NegotiatedInstallments == 0)
                {
                    loanrequest.NegotiatedInstallments = termandinterestrate.Item1;
                }
                
                if (efttype == (int)Enumerations.EFTType.AccountToMobile)
                {
                    Regex regex = new Regex(@"^254(?:[0-9]??){6,14}[0-9]$");
                    Match match = regex.Match(loanrequest.PrimaryAccountNumber);
                    if (!match.Success)
                    {
                        ModelState.AddModelError(string.Empty, "Failed. If transfer mode is to mobile, the Primary Account Number should start 254, followed by the national number");
                        return View(loanrequest);
                    }                    
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(loanrequest.ApplicantBankCode))
                    {
                        ModelState.AddModelError(string.Empty, "Failed. If transfer mode is to bank account, you must select your Bank from the list.");
                        return View(loanrequest);
                    }
                    else
                    {
                        paybillNumber = GetBranchesSelectList(loanrequest.ApplicantBankCode);
                        if (paybillNumber.Equals("0"))
                        {
                            ModelState.AddModelError(string.Empty, "The system could not much a valid Paybill Number with the selected bank. Please select another bank or change the funds transfer method.");
                            return View(loanrequest);
                        }
                    }
                }
                var findcustomeraccountnumbers = customeraccountservice.GetAllCustomerAccountNumbers(getuserid.Id).ToArray();

                var findloanproductbyid = loanproductservice.FindLoanProductById(loanrequest.LoanProductId);
                

                var NextCustomerAccountLoanAccountNumber = customerservice.NextCustomerLoanAccountNumber(getuserid.CustomerNumber, getuserid.Id, findloanproductbyid.ProductCode);
                CustomersAccount customersaccount = new CustomersAccount();
                customersaccount.AccountNumber = NextCustomerAccountLoanAccountNumber;
                customersaccount.IsActive = true;
                customersaccount.AccountType = (int)Enumerations.ProductCode.Loan;
                customersaccount.CustomerId = getuserid.Id;   
                try
                {                    
                    var checkingoutstandingloan = loanrequestService.GetLoanRequestByCustomerAccountId(getuserid.Id);                    

                    int getnotificationenabledmode = customerservice.GetCustomerEnabledNotificationMode(getuserid.Id);

                    var countunclearedloans = checkingoutstandingloan.Where(x => x.Status != (int)Enumerations.LoanStatus.Cleared && x.Status != (int)Enumerations.LoanStatus.Rejected && x.Status != (int)Enumerations.LoanStatus.CustomerRejected && x.Status != (int)Enumerations.LoanStatus.Review && x.Status != (int)Enumerations.LoanStatus.TransactionReversed).Count();
                    
                    if (loanrequest.LoanAmount <= amountvalues.Item1 && loanrequest.LoanAmount >= amountvalues.Item2)
                    {    
                        if(checkingoutstandingloan.Count() != 0 )
                        {
                            if (countunclearedloans >= 1)
                            {
                                ModelState.AddModelError(string.Empty, "Sorry, you have an uncleared loan. Clear it first and try again later.");
                                return View(loanrequest);
                            }
                            else
                            {                                                             
                                decimal interestpermonth = termandinterestrate.Item2 / 12;
                                loanrequest.InterestAmount = (interestpermonth * loanrequest.NegotiatedInstallments * loanrequest.LoanAmount) / 100;
                                
                                loanrequest.LoanReferenceNumber = loanrefno;
                                loanrequest.NextPaymentDate = DateTime.Today.AddMonths(1);
                                loanrequest.CustomerAccountId = customersaccount.Id;
                                if (efttype == (int)Enumerations.EFTType.BusinessToBusiness)
                                {
                                    loanrequest.BankCode = paybillNumber;
                                    loanrequest.Type = Enumerations.EFTType.BusinessToBusiness;
                                }
                                if (efttype == (int)Enumerations.EFTType.AccountToMobile)
                                {
                                    loanrequest.Type = Enumerations.EFTType.AccountToMobile;
                                }
                                else
                                {
                                    loanrequest.Type = Enumerations.EFTType.Cheque;
                                }
                                loanrequest.Status = (int)Enumerations.LoanStatus.Pending;
                                customeraccountservice.AddNewCustomersAccount(customersaccount, serviceHeader);
                                loanrequestService.AddNewLoanRequest(loanrequest, serviceHeader);
                                NewNotificationEmail(ConfigurationManager.AppSettings["NotificationsEmailAddress"].ToString(), getuserid.FullName);
                                if(getnotificationenabledmode == 1)
                                {
                                    NewEmailAlert(getuserid.FullName, loanrefno, getuserid.EmailAddress, loanrequest.LoanAmount);
                                }

                                else if(getnotificationenabledmode == 2)
                                {
                                    NewTextAlert(getuserid.FirstName, loanrefno, getuserid.MobileNumber, loanrequest.LoanAmount);
                                }
                                else if(getnotificationenabledmode == 3)
                                {
                                    NewEmailAlert(getuserid.FullName, loanrefno, getuserid.EmailAddress, loanrequest.LoanAmount);
                                    NewTextAlert(getuserid.FirstName, loanrefno, getuserid.MobileNumber, loanrequest.LoanAmount);
                                }

                                return RedirectToAction("loanrequestlist", "Customer");
                            }
                        }

                        else
                        {
                            decimal interestpermonth = termandinterestrate.Item2 / 12;

                            loanrequest.InterestAmount = (interestpermonth * loanrequest.NegotiatedInstallments * loanrequest.LoanAmount) / 100;
                            loanrequest.LoanReferenceNumber = loanrefno;
                            loanrequest.Status = (int)Enumerations.LoanStatus.Pending;
                            loanrequest.CustomerAccountId = customersaccount.Id;
                            if (efttype == (int)Enumerations.EFTType.BusinessToBusiness)
                            {
                                loanrequest.BankCode = paybillNumber;
                                loanrequest.Type = Enumerations.EFTType.BusinessToBusiness;
                            }
                            if (efttype == (int)Enumerations.EFTType.AccountToMobile)
                            {
                                loanrequest.Type = Enumerations.EFTType.AccountToMobile;
                            }
                            else
                            {
                                loanrequest.Type = Enumerations.EFTType.Cheque;
                            }
                            loanrequest.NextPaymentDate = DateTime.Today.AddMonths(1);
                            customeraccountservice.AddNewCustomersAccount(customersaccount, serviceHeader);
                            loanrequestService.AddNewLoanRequest(loanrequest, serviceHeader);
                            NewNotificationEmail(ConfigurationManager.AppSettings["NotificationsEmailAddress"].ToString(), getuserid.FullName);
                            if (getnotificationenabledmode == 1)
                            {
                                NewEmailAlert(getuserid.FullName, loanrefno, getuserid.EmailAddress, loanrequest.LoanAmount);
                            }

                            else if (getnotificationenabledmode == 2)
                            {
                                NewTextAlert(getuserid.FullName, loanrefno, getuserid.MobileNumber, loanrequest.LoanAmount);
                            }
                            else if (getnotificationenabledmode == 3)
                            {
                                NewEmailAlert(getuserid.FullName, loanrefno, getuserid.EmailAddress, loanrequest.LoanAmount);
                                NewTextAlert(getuserid.FirstName, loanrefno, getuserid.MobileNumber, loanrequest.LoanAmount);
                            }
                            return RedirectToAction("loanrequestlist", "Customer");
                        }                                                                      
                    }

                    else
                    {
                        string errormessage = string.Format("Minimum amount you can borrow is {0} and maximum amount {1}. Make sure loan amount is within the range", (amountvalues.Item2), (amountvalues.Item1));
                        ModelState.AddModelError(string.Empty, errormessage);
                        return View(loanrequest);
                    }
                    
                }

                catch(Exception ex)
                {
                    ModelState.AddModelError(string.Empty, "Operation was unsuccessful, correct the errors and try again.");
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                }
            }
            return View(loanrequest);
        }


        private void NewNotificationEmail(string recipientEmailAddress, string CustomerName)
        {
            //Create email alert and insert in the database
            string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
            string loginurl = "<a href='" + mvcclientendposint + "'/Account/Login> clicking here</a>";

            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append("Dear Sir/Madam");
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.Append("There is a loan request awaiting your approval. ");
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.Append(string.Format(" You can login to Pilar™ and approve by {0}.", loginurl));
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.AppendLine("<br />");

            var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
            bodyBuilder.Append(emailSignature);

            var emailAlertDTO = new EmailAlert
            {
                MailMessageFrom = DefaultSettings.Instance.Email,
                MailMessageTo = string.Format("{0}", recipientEmailAddress),
                MailMessageSubject = string.Format("Loan Approval Pending", CustomerName),
                MailMessageBody = string.Format("{0}", bodyBuilder),
                MailMessageIsBodyHtml = true,
                MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                MailMessageSecurityCritical = false,
                CreatedBy = User.Identity.Name,
                MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
            };

            emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
        }

        private void NewEmailAlert(string RecipientName, string loanrefno, string to, decimal loanamount)
        {
            //Create email alert and insert in the database
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append(string.Format("Dear {0}", RecipientName));
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.Append(string.Format("Your loan application of KES {0} with RefNo.-{1} has been received and is being processed.", loanamount, loanrefno));
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.Append(" You will receive a notification once processing is done.");
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.AppendLine("<br />");

            var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
            bodyBuilder.Append(emailSignature);

            var emailAlertDTO = new EmailAlert
            {
                MailMessageFrom = DefaultSettings.Instance.Email,
                MailMessageTo = string.Format("{0}", to),
                MailMessageSubject = string.Format("Loan Application - {0}",loanrefno),
                MailMessageBody = string.Format("{0}", bodyBuilder),
                MailMessageIsBodyHtml = true,
                MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                MailMessageSecurityCritical = false,
                CreatedBy = User.Identity.Name,
                MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
            };

            emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
        }

        private void NewTextAlert(string RecipientName, string loanrefno, string to, decimal loanamount)
        {
            //Create email alert and insert in the database
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append(string.Format("Dear {0}, ", RecipientName));
            bodyBuilder.Append(string.Format("Your loan application of KES {0} with RefNo.-{1} has been received and is being processed.",loanamount, loanrefno));
            bodyBuilder.Append(TextSignature);
            var textalert = new TextAlert
            {
                TextMessageRecipient = string.Format("{0}", to),
                TextMessageReference = string.Format("Loan Application - {0}", loanrefno),
                TextMessageBody = string.Format("{0}", bodyBuilder),
                MaskedTextMessageBody = "",
                TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                TextMessageSecurityCritical = false,
                CreatedBy = User.Identity.Name,
                TextMessageSendRetry = 3
            };

            textalertservice.AddNewTextAlert(textalert, serviceHeader);
        }

        [Authorize]
        public ActionResult LoanRequestDetails(Guid Id)
        {
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(Id);
            var chargesAndInterest = loanrequestService.LoanRequestInterestAndCharges(Id);
            decimal result = chargesAndInterest.Item1 + chargesAndInterest.Item2;
            ViewBag.Result = result.ToString("0.00");
            return View(findloanrequestbyid);
        }

        public ActionResult CustomerLoanRequest(Guid Id)
        {
            ViewBag.Id = Id;

            var findcustomer = customerservice.FindCustomerById(Id);

            ViewBag.Name = findcustomer.FullName;

            return View();
        }

        public JsonResult GetCustomerLoanRequest(Guid Id, JQueryDataTablesModel datatablemodel)
        {            
            var pagecollection = loanrequestService.GetAllCustomerLoanRequests(Id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var customerloanrequest = CustomerLoanRequest(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, CustomerLoanRequest: pagecollection);


            return Json(new JQueryDataTablesResponse<LoanRequest>(items: customerloanrequest,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<LoanRequest> CustomerLoanRequest(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanRequest> CustomerLoanRequest)
        {
            var GetCustomerLoanRequest = CustomerLoanRequest;

            totalRecordCount = GetCustomerLoanRequest.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomerLoanRequest = GetCustomerLoanRequest.Where(c => c.CustomerAccount.Customer.FullName
                    .ToLower().Contains(searchString.ToLower()) || c.LoanReferenceNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetCustomerLoanRequest.Count;

            IOrderedEnumerable<LoanRequest> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "LoanProduct.ProductName":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName);
                        break;

                    case "LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber);
                        break;

                    case "LoanAmount":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount);
                        break;

                    case "InterestAmount":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount);
                        break;

                    case "StatusDesc":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc);
                        break;                    
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        /// <summary>
        /// Approve loan request
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        [Authorize(Roles = "LoanOfficer, Administrator")]
        public ActionResult ApproveLoan(Guid Id)
        {
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(Id);                       

            return View(findloanrequestbyid);
        }

        [ActionName("approveloan")]
        [HttpPost]        
        public ActionResult ApproveCustomerLoanRequest(LoanRequest loanrequest)
        {
            var finddstaticbykey = staticsettings.FindStaticSettingByKey("ENFORCE_CUSTOMER_AND_LOAN_DOCUMENTS_BEFORE_LOAN_APPPROVAL");
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(loanrequest.Id);
            findloanrequestbyid.ApprovedBy = Request.RequestContext.HttpContext.User.Identity.Name;
            if (finddstaticbykey.Value == "1" && finddstaticbykey.IsLocked == false)
            {
                bool iscustomerdocumentavailable = customerdocumentservice.VerifyAllCustomerDocumentExistence(loanrequest.CustomerAccount.Customer.Id);
                bool isloanrequestdocumentavailable = loanrequestdocumentservice.VerifyLoanRequestDocument(loanrequest.Id);
                if (findloanrequestbyid.LoanAmount > 70000 && findloanrequestbyid.Type == Enumerations.EFTType.AccountToMobile)
                {
                    ModelState.AddModelError(string.Empty, "If transfer mode is to mobile account(M-Pesa), the loan amount must be below KSH70000.This is due to M-Pesa limit rule.");
                    return View(findloanrequestbyid);
                }
                if (iscustomerdocumentavailable && isloanrequestdocumentavailable)
                {
                    
                    if (findloanrequestbyid == null)
                    {
                        ModelState.AddModelError(string.Empty, "Failed while retrieving loan request information. No information was found.");
                        return View(loanrequest);
                    }

                    if (findloanrequestbyid.Status == (int)Enumerations.LoanStatus.Pending)
                    {
                        var findloanproduct = loanproductservice.FindLoanProductById(findloanrequestbyid.LoanProductId);

                        var FindCustomerAccountByCustomerIdandAccountType = customeraccountservice.FindCustomerByCustomerIdandAccountType(findloanrequestbyid.CustomerAccount.CustomerId, (int)Enumerations.ProductCode.Savings);

                        var findcustomeraccountbycustomerid = customeraccountservice.FindCustomersAccountByCustomerId(findloanrequestbyid.CustomerAccountId);

                        var findchartofaccount = chartofaccountservice.GetChartOfAccountsByTypeAndCategory((int)Enumerations.ChartOfAccountType.Asset, (int)Enumerations.ChartOfAccountCategory.DetailAccount);
                        var findcustomerbyid = customerservice.FindCustomerById(findloanrequestbyid.CustomerAccountId);
                        string customeraccountid = findloanrequestbyid.CustomerAccountId.ToString();
                        if (string.IsNullOrWhiteSpace(loanrequest.Remarks))
                        {
                            ModelState.AddModelError(string.Empty, "You must provide approval remarks.");
                            return View(findloanrequestbyid);
                        }

                        try
                        {
                            findloanrequestbyid.MCCMNC = (int)Enumerations.MobileNetworkCode.KE_Safaricom;
                            findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Pending;
                            findloanrequestbyid.Remarks = loanrequest.Remarks;
                            findloanrequestbyid.ApprovedBy = Request.RequestContext.HttpContext.User.Identity.Name;
                            findloanrequestbyid.ApprovedDate = DateTime.Today;
                            findloanrequestbyid.SystemTraceAuditNumber = findloanrequestbyid.Id;

                            #region Debit Loan Transaction History Table
                            var defaultsavingproductglaccount = savingsproductservice.FindDefaultSavingsProduct();
                            if (defaultsavingproductglaccount == null)
                            {
                                ModelState.AddModelError(string.Empty, "Failed. There is no default saving product.");
                                return View(findloanrequestbyid);
                            }
                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                            LoanTransactionHistory debitloanaccount = new LoanTransactionHistory()
                            {
                                LoanRequestId = findloanrequestbyid.Id,
                                ChartofAccountId = findloanproduct.LoanGLAccountId,
                                ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                TransactionType = Enumerations.TransactionType.Debit,
                                CustomersAccountId = findloanrequestbyid.CustomerAccountId,                                
                                Description = findloanrequestbyid.Remarks,
                                CreatedBy = User.Identity.Name,
                                IsApproved = true,
                                TransactionCategory = (int)Enumerations.TransactionCategory.Approval,
                                TransactionAmount = findloanrequestbyid.LoanAmount,
                                BranchId = findloanrequestbyid.BranchId,
                                TransactionBatchNumber = batchNo
                            };
                            
                            #endregion

                            #region Credit Loan Transaction History Table
                            string _customeraccountId = FindCustomerAccountByCustomerIdandAccountType.Id.ToString();
                            var id = findloanproduct.LoanGLAccountId.ToString();
                            LoanTransactionHistory creditsavingglaccount = new LoanTransactionHistory()
                            {
                                LoanRequestId = findloanrequestbyid.Id,
                                ChartofAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                ContraAccountId = Guid.Parse(id),
                                TransactionType = Enumerations.TransactionType.Credit,
                                CustomersAccountId = Guid.Parse(_customeraccountId),                                
                                Description = findloanrequestbyid.Remarks,
                                IsApproved = true,
                                TransactionCategory = (int)Enumerations.TransactionCategory.Approval,
                                CreatedBy = User.Identity.Name,
                                TransactionAmount = findloanrequestbyid.LoanAmount,
                                BranchId = findloanrequestbyid.BranchId,
                                TransactionBatchNumber = batchNo
                            };
                            
                            #endregion

                            findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Approved;
                            findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Pending;
                            using (DataContext datacontext = new DataContext())
                            {
                                using (var transaction = datacontext.Database.BeginTransaction())
                                {
                                    try
                                    {
                                        loanrequestService.UpdateLoanRequest(findloanrequestbyid, serviceHeader);
                                        loantransactionhistoryservice.AddNewLoanHistory(debitloanaccount, serviceHeader);
                                        loantransactionhistoryservice.AddNewLoanHistory(creditsavingglaccount, serviceHeader);
                                        var emailAlertDTO = EmailNotification(findloanrequestbyid);
                                        emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                                        NewTextAlertOnApproval(findloanrequestbyid.CustomerAccount.Customer.FullName, findloanrequestbyid.CustomerAccount.Customer.MobileNumber, findloanrequestbyid.LoanReferenceNumber, findloanrequestbyid.LoanAmount);
                                        transaction.Commit();
                                    }

                                    catch (Exception ex)
                                    {
                                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                        transaction.Rollback();
                                    }
                                }
                            }
                        }

                        catch (Exception ex)
                        {
                            ModelState.AddModelError(string.Empty, "An error has occurred while processing your request.");
                            LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                        }
                    }

                    return RedirectToAction("allloanrequest");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Failed. Some required documents are missing. Make sure customer and loan application documents are available before approving.");
                    return View(findloanrequestbyid);
                }
            }
            else
            {                
                if (findloanrequestbyid == null)
                {
                    ModelState.AddModelError(string.Empty, "Failed while retrieving loan request information. No information was found.");
                    return View(loanrequest);
                }

                if (findloanrequestbyid.LoanAmount > 70000 && findloanrequestbyid.Type == Enumerations.EFTType.AccountToMobile)
                {
                    ModelState.AddModelError(string.Empty, "If transfer mode is to mobile account(M-Pesa), the loan amount must be below KSH70000.This is due to M-Pesa limit rule.");
                    return View(findloanrequestbyid);
                }

                if (findloanrequestbyid.Status == (int)Enumerations.LoanStatus.Pending)
                {
                    var findloanproduct = loanproductservice.FindLoanProductById(findloanrequestbyid.LoanProductId);

                    var FindCustomerAccountByCustomerIdandAccountType = customeraccountservice.FindCustomerByCustomerIdandAccountType(findloanrequestbyid.CustomerAccount.CustomerId, (int)Enumerations.ProductCode.Savings);

                    var findcustomeraccountbycustomerid = customeraccountservice.FindCustomersAccountByCustomerId(findloanrequestbyid.CustomerAccountId);

                    var findchartofaccount = chartofaccountservice.GetChartOfAccountsByTypeAndCategory((int)Enumerations.ChartOfAccountType.Asset, (int)Enumerations.ChartOfAccountCategory.DetailAccount);
                    var findcustomerbyid = customerservice.FindCustomerById(findloanrequestbyid.CustomerAccountId);
                    string customeraccountid = findloanrequestbyid.CustomerAccountId.ToString();
                    if (string.IsNullOrWhiteSpace(loanrequest.Remarks))
                    {
                        ModelState.AddModelError(string.Empty, "You must provide approval remarks.");
                        return View(findloanrequestbyid);
                    }

                    try
                    {
                        findloanrequestbyid.MCCMNC = (int)Enumerations.MobileNetworkCode.KE_Safaricom;
                        findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Pending;
                        findloanrequestbyid.Remarks = loanrequest.Remarks;
                        findloanrequestbyid.ApprovedDate = DateTime.Today;
                        findloanrequestbyid.SystemTraceAuditNumber = findloanrequestbyid.Id;

                        #region Debit Loan Transaction History Table
                        var defaultsavingproductglaccount = savingsproductservice.FindDefaultSavingsProduct();
                        if (defaultsavingproductglaccount == null)
                        {
                            ModelState.AddModelError(string.Empty, "Failed. There is no default saving product.");
                            return View(findloanrequestbyid);
                        }
                        //Debit Loan Account
                        int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                        LoanTransactionHistory debitloanaccount = new LoanTransactionHistory()
                        {
                             LoanRequestId = findloanrequestbyid.Id,
                            ChartofAccountId = findloanproduct.LoanGLAccountId,
                            ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                            TransactionType = Enumerations.TransactionType.Debit,
                            CustomersAccountId = findloanrequestbyid.CustomerAccountId,                            
                            Description = findloanrequestbyid.Remarks,
                            CreatedBy = User.Identity.Name,
                            IsApproved = true,
                            TransactionCategory = (int)Enumerations.TransactionCategory.Approval,
                            TransactionAmount = findloanrequestbyid.LoanAmount,
                            BranchId = findloanrequestbyid.BranchId,
                            TransactionBatchNumber = batchNo
                        };
                       
                        #endregion

                        #region Credit Loan Transaction History Table
                        var id = findloanproduct.LoanGLAccountId.ToString();
                        string _customeraccountId = FindCustomerAccountByCustomerIdandAccountType.Id.ToString();
                        LoanTransactionHistory creditsavingglaccount = new LoanTransactionHistory()
                        {
                             LoanRequestId = findloanrequestbyid.Id,
                            ChartofAccountId = defaultsavingproductglaccount.ChartofAccountId,
                            ContraAccountId = Guid.Parse(id),
                            TransactionType = Enumerations.TransactionType.Credit,
                            CustomersAccountId = Guid.Parse(_customeraccountId),                            
                            Description = findloanrequestbyid.Remarks,
                            IsApproved = true,
                            TransactionCategory = (int)Enumerations.TransactionCategory.Approval,
                            CreatedBy = User.Identity.Name,
                            TransactionAmount = findloanrequestbyid.LoanAmount,
                             BranchId = findloanrequestbyid.BranchId,
                             TransactionBatchNumber = batchNo
                        };
                       
                        #endregion

                        findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Approved;
                        findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Pending;
                        using (DataContext datacontext = new DataContext())
                        {
                            using (var transaction = datacontext.Database.BeginTransaction())
                            {
                                try
                                {
                                    loanrequestService.UpdateLoanRequest(findloanrequestbyid, serviceHeader);
                                    loantransactionhistoryservice.AddNewLoanHistory(debitloanaccount, serviceHeader);
                                    loantransactionhistoryservice.AddNewLoanHistory(creditsavingglaccount, serviceHeader);
                                    var emailAlertDTO = EmailNotification(findloanrequestbyid);
                                    emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                                    NewTextAlertOnApproval(findloanrequestbyid.CustomerAccount.Customer.FullName, findloanrequestbyid.CustomerAccount.Customer.MobileNumber, findloanrequestbyid.LoanReferenceNumber, findloanrequestbyid.LoanAmount);
                                    transaction.Commit();
                                }

                                catch (Exception ex)
                                {
                                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                    transaction.Rollback();
                                }
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        ModelState.AddModelError(string.Empty, "An error has occurred while processing your request.");
                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    }
                }                
            }
            return RedirectToAction("allloanrequest");
        }

        private static EmailAlert EmailNotification(LoanRequest findloanrequestbyid)
        {
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append(string.Format("Dear {0}", findloanrequestbyid.CustomerAccount.Customer.FullName));
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.Append(string.Format("Your loan of KES {0} with reference number-{1} has been approved.",findloanrequestbyid.LoanAmount, findloanrequestbyid.LoanReferenceNumber));
            bodyBuilder.AppendLine("<br />");
            bodyBuilder.AppendLine("<br />");

            var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
            bodyBuilder.Append(emailSignature);

            var emailAlertDTO = new EmailAlert
            {
                MailMessageFrom = DefaultSettings.Instance.Email,
                MailMessageTo = string.Format("{0}", findloanrequestbyid.CustomerAccount.Customer.EmailAddress),
                MailMessageSubject = string.Format("Loan Request Approved - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                MailMessageBody = string.Format("{0}", bodyBuilder),
                MailMessageIsBodyHtml = true,
                MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                MailMessageSecurityCritical = false,
                CreatedBy = "System",
                MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
            };
            return emailAlertDTO;
        }

        private void NewTextAlertOnApproval(string RecipientName, string RecepientNumber, string LoanRefNo, decimal LoanAmount)
        {
            //Create email alert and insert in the database
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append(string.Format("Dear {0} ", RecipientName));
            bodyBuilder.Append(string.Format("Your loan of KES {0} with reference number-{1} has been approved.", LoanAmount, LoanRefNo));
            bodyBuilder.Append(TextSignature);
            var textalert = new TextAlert
            {
                TextMessageRecipient = string.Format("{0}", RecepientNumber),
                TextMessageBody = string.Format("{0}", bodyBuilder),
                MaskedTextMessageBody = "",
                TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                TextMessageSecurityCritical = false,
                CreatedBy = User.Identity.Name,
                TextMessageSendRetry = 3
            };

            textalertservice.AddNewTextAlert(textalert, serviceHeader);
        }

        /// <summary>
        /// Reject loan reject
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        ///
        [HttpGet]
        [Authorize(Roles = "LoanOfficer, Administrator")]
        public ActionResult RejectLoan(Guid Id)
        {
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(Id);
           
            return View(findloanrequestbyid);
        }

        [ActionName("rejectloan")]
        [HttpPost]        
        public ActionResult RejectCustomerLoanRequest(LoanRequest loanrequest)
        {
            try
            {                
                var findloanrequestbyid = loanrequestService.GetLoanRequestById(loanrequest.Id);

                if (string.IsNullOrWhiteSpace(loanrequest.Remarks))
                {
                    ModelState.AddModelError(string.Empty, "You must provide rejection remarks.");
                    return View(loanrequest);
                }

                if(findloanrequestbyid.Status == (int)Enumerations.LoanStatus.Pending)
                {
                    findloanrequestbyid.Remarks = loanrequest.Remarks;
                    findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Rejected;
                    findloanrequestbyid.RejectionRemarks = loanrequest.Remarks;
                    findloanrequestbyid.ApprovedBy = User.Identity.Name;
                    findloanrequestbyid.ApprovedDate = DateTime.Now;
                    loanrequestService.UpdateLoanRequest(findloanrequestbyid, serviceHeader);

                    StringBuilder bodyBuilder = new StringBuilder();
                    bodyBuilder.Append(string.Format("Dear {0} ", findloanrequestbyid.CustomerAccount.Customer.FullName));
                    bodyBuilder.AppendLine("<br />");
                    bodyBuilder.AppendLine("<br />");
                    bodyBuilder.Append(string.Format("Your loan with reference number-{0} has been declined.", findloanrequestbyid.LoanReferenceNumber));
                    bodyBuilder.AppendLine("<br />");
                    bodyBuilder.AppendLine("<br />");

                    var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
                    bodyBuilder.Append(emailSignature);

                    var emailAlertDTO = new EmailAlert
                    {
                        MailMessageFrom = DefaultSettings.Instance.Email,
                        MailMessageTo = string.Format("{0}", findloanrequestbyid.CustomerAccount.Customer.EmailAddress),
                        MailMessageSubject = string.Format("Loan Request Declined - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                        MailMessageBody = string.Format("{0}", bodyBuilder),
                        MailMessageIsBodyHtml = true,
                        MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                        MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                        MailMessageSecurityCritical = false,
                        CreatedBy = "System",
                        MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                    };
                    emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                }
                StringBuilder _bodyBuilder = new StringBuilder();
                _bodyBuilder.Append(string.Format("Dear {0} ", findloanrequestbyid.CustomerAccount.Customer.FullName));
                _bodyBuilder.Append(string.Format("Your loan with reference number-{0} has been declined.", findloanrequestbyid.LoanReferenceNumber));
                _bodyBuilder.Append(TextSignature);
                var textalert = new TextAlert
                {
                    TextMessageRecipient = string.Format("{0}", findloanrequestbyid.CustomerAccount.Customer.MobileNumber),
                    TextMessageBody = string.Format("{0}", _bodyBuilder),
                    MaskedTextMessageBody = "",
                    TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                    TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                    TextMessageSecurityCritical = false,
                    CreatedBy = User.Identity.Name,
                    TextMessageSendRetry = 3
                };
                textalertservice.AddNewTextAlert(textalert, serviceHeader);
                return RedirectToAction("allloanrequest");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError(string.Empty, "An error has occurred while processing your request.");
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);          
            }
            return View(loanrequest);
        }

        [HttpGet]
        [Authorize(Roles = "FinanceManager, Administrator")]
        public ActionResult DisburseLoan(Guid Id)
        {
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(Id);
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);            
            ViewBag.EFTTypes = EFTInfo(null);
            return View(findloanrequestbyid);
        }

        [HttpPost]
        [ActionName("DisburseLoan")]        
        public ActionResult DisburseLoan(LoanRequest loanrequest)
        {
            LoanTransactionHistory loantransactionhistory = new LoanTransactionHistory();

            decimal totalcharges = 0;
            string paybillNumber = string.Empty;
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(loanrequest.Id);
            if (findloanrequestbyid == null)
            {
                ModelState.AddModelError(string.Empty, "An expected error has occurred.");
                return View(loanrequest);
            }

            if (findloanrequestbyid.Status == (int)Enumerations.LoanStatus.Approved)
            {
                var FindCustomerAccountByCustomerIdandAccountType = customeraccountservice.FindCustomerByCustomerIdandAccountType(findloanrequestbyid.CustomerAccount.CustomerId, (int)Enumerations.ProductCode.Savings);
                var defaultsavingproductglaccount = savingsproductservice.FindDefaultSavingsProduct();
                decimal findCustomerSavingsAccountBalance = customeraccountservice.FindCustomerAccountBalanceByProductCode(FindCustomerAccountByCustomerIdandAccountType.Id, defaultsavingproductglaccount.ChartofAccountId, (int)Enumerations.ProductCode.Savings);
                if ((findCustomerSavingsAccountBalance - findloanrequestbyid.LoanAmount) >= int.Parse(ConfigurationManager.AppSettings["LoanProcessingFee"]))
                {
                    using (var datacontext = new DataContext())
                    {
                        using (var transaction = datacontext.Database.BeginTransaction())
                        {
                            try
                            {
                                var MpesaGL = systemglmappingservice.FindSystemGeneralLedgerAccountMappingsByGLCode((int)Enumerations.SystemGeneralLedgerAccountCode.MPesaSettlementAccount);

                                if (MpesaGL == null)
                                {
                                    ModelState.AddModelError(string.Empty, "Failed. There is no M-Pesa Settlement GL Account available.");
                                    return View(findloanrequestbyid);
                                }
                                try
                                {
                                    var findloanproductcharges = loanproductcharges.GetAllLoanProductCharges(findloanrequestbyid.LoanProductId).Where(x => x.Charges.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.UpFront);
                                    if (findloanproductcharges != null && findloanproductcharges.Any())
                                    {
                                        foreach (var charge in findloanproductcharges)
                                        {
                                            var findgraduatedscalebychargeid = graduatedscaleservice.FindGraduatedScaleUsingRangeAndChargeId(findloanrequestbyid.LoanAmount, charge.ChargeId);
                                            if (findgraduatedscalebychargeid != null && findgraduatedscalebychargeid.Any())
                                            {
                                                foreach (var graduatedscale in findgraduatedscalebychargeid)
                                                {
                                                    var findloanamountrangeingradutedscale = graduatedscaleservice.FindGraduatedScaleUsingRangeAndChargeId(findloanrequestbyid.LoanAmount, graduatedscale.ChargeId);
                                                    var customersavingsaccountid = FindCustomerAccountByCustomerIdandAccountType.Id.ToString();
                                                    if (graduatedscale.ChargeType == Enumerations.ChargeTypes.FixedAmount)
                                                    {
                                                        if (findloanamountrangeingradutedscale != null)
                                                        {
                                                            var fixedchargetypeamount = graduatedscale.Value;

                                                            LoanRequestCharges(findloanrequestbyid.Id, charge.Charges.ChartofAccountId, graduatedscale.Id, fixedchargetypeamount);
                                                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                            #region Debit
                                                            LoanTransactionHistory postchargesdebit = new LoanTransactionHistory()
                                                            {
                                                                LoanRequestId = findloanrequestbyid.Id,
                                                                ChartofAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                                                ContraAccountId = charge.Charges.ChartofAccountId,
                                                                TransactionType = Enumerations.TransactionType.Debit,
                                                                CustomersAccountId = Guid.Parse(customersavingsaccountid),
                                                                Description = findloanrequestbyid.Remarks,
                                                                IsApproved = true,
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.LoanProcessingFee,
                                                                CreatedBy = User.Identity.Name,
                                                                TransactionAmount = fixedchargetypeamount,
                                                                BranchId = findloanrequestbyid.BranchId,
                                                                TransactionBatchNumber = batchNo
                                                            };

                                                            loantransactionhistoryservice.AddNewLoanHistory(postchargesdebit, serviceHeader);
                                                            #endregion

                                                            #region Credit
                                                            LoanTransactionHistory postchargescredit = new LoanTransactionHistory()
                                                            {
                                                                LoanRequestId = findloanrequestbyid.Id,
                                                                ChartofAccountId = charge.Charges.ChartofAccountId,
                                                                ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                                                TransactionType = Enumerations.TransactionType.Credit,
                                                                CustomersAccountId = Guid.Parse(customersavingsaccountid),
                                                                Description = findloanrequestbyid.Remarks,
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.LoanProcessingFee,
                                                                CreatedBy = User.Identity.Name,
                                                                IsApproved = true,
                                                                TransactionAmount = fixedchargetypeamount,
                                                                BranchId = findloanrequestbyid.BranchId,
                                                                TransactionBatchNumber = batchNo
                                                            };

                                                            loantransactionhistoryservice.AddNewLoanHistory(postchargescredit, serviceHeader);
                                                            #endregion
                                                        }
                                                    }

                                                    else if (graduatedscale.ChargeType == Enumerations.ChargeTypes.Percentage)
                                                    {
                                                        if (findloanamountrangeingradutedscale != null)
                                                        {
                                                            var percetagechargetypeamount = ((graduatedscale.Value) * findloanrequestbyid.LoanAmount) / 100;
                                                            LoanRequestCharges(findloanrequestbyid.Id, charge.Charges.ChartofAccountId, graduatedscale.Id, percetagechargetypeamount);
                                                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                            #region Debit
                                                            LoanTransactionHistory postchargesdebit = new LoanTransactionHistory()
                                                            {
                                                                LoanRequestId = findloanrequestbyid.Id,
                                                                ChartofAccountId = MpesaGL.ChartofAccountId,
                                                                ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                                                TransactionType = Enumerations.TransactionType.Debit,
                                                                CustomersAccountId = Guid.Parse(customersavingsaccountid),
                                                                Description = findloanrequestbyid.Remarks,
                                                                IsApproved = true,
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.LoanProcessingFee,
                                                                CreatedBy = User.Identity.Name,
                                                                TransactionAmount = percetagechargetypeamount,
                                                                BranchId = findloanrequestbyid.BranchId,
                                                                TransactionBatchNumber = batchNo
                                                            };

                                                            loantransactionhistoryservice.AddNewLoanHistory(postchargesdebit, serviceHeader);
                                                            #endregion

                                                            #region Credit
                                                            LoanTransactionHistory postchargescredit = new LoanTransactionHistory()
                                                            {
                                                                LoanRequestId = findloanrequestbyid.Id,
                                                                ChartofAccountId = MpesaGL.ChartofAccountId,
                                                                ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                                                TransactionType = Enumerations.TransactionType.Credit,
                                                                CustomersAccountId = Guid.Parse(customersavingsaccountid),
                                                                Description = findloanrequestbyid.Remarks,
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.LoanProcessingFee,
                                                                CreatedBy = User.Identity.Name,
                                                                IsApproved = true,
                                                                TransactionAmount = percetagechargetypeamount,
                                                                BranchId = findloanrequestbyid.BranchId,
                                                                TransactionBatchNumber = batchNo
                                                            };
                                                            loantransactionhistoryservice.AddNewLoanHistory(postchargescredit, serviceHeader);
                                                            #endregion
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ModelState.AddModelError(string.Empty, "Failed while trying to determine product charge type. Contact your system administrator for assistance.");
                                                        return View(findloanrequestbyid);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #region Debit Loan Transaction History Table
                                    var customeraccountid = FindCustomerAccountByCustomerIdandAccountType.Id.ToString();
                                    int batchNo1 = RandomBatchNumberGenerator.GetRandomNumber();
                                    LoanTransactionHistory debitsavingsaccount = new LoanTransactionHistory()
                                    {
                                        LoanRequestId = findloanrequestbyid.Id,
                                        ChartofAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                        ContraAccountId = MpesaGL.ChartofAccountId,
                                        TransactionType = Enumerations.TransactionType.Debit,
                                        CustomersAccountId = Guid.Parse(customeraccountid),
                                        Description = findloanrequestbyid.Remarks,
                                        IsApproved = true,
                                        TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement,
                                        CreatedBy = User.Identity.Name,
                                        TransactionAmount = findloanrequestbyid.LoanAmount,
                                        BranchId = findloanrequestbyid.BranchId,
                                        TransactionBatchNumber = batchNo1
                                    };

                                    #endregion

                                    #region Credit Loan Transaction History Table
                                    LoanTransactionHistory creditmpesaglaccount = new LoanTransactionHistory()
                                    {
                                        LoanRequestId = findloanrequestbyid.Id,
                                        ChartofAccountId = MpesaGL.ChartofAccountId,
                                        ContraAccountId = defaultsavingproductglaccount.ChartofAccountId,
                                        TransactionType = Enumerations.TransactionType.Credit,
                                        CustomersAccountId = Guid.Parse(customeraccountid),
                                        Description = findloanrequestbyid.Remarks,
                                        TransactionCategory = (int)Enumerations.TransactionCategory.Disbursement,
                                        CreatedBy = User.Identity.Name,
                                        IsApproved = true,
                                        TransactionAmount = findloanrequestbyid.LoanAmount,
                                        BranchId = findloanrequestbyid.BranchId,
                                        TransactionBatchNumber = batchNo1
                                    };

                                    #endregion

                                    var findloanrequestcharge = loanrequestchargeservice.FindAllLoanRequestChargesByLoanRequestId(findloanrequestbyid.Id);
                                    if (findloanrequestcharge != null && findloanrequestcharge.Any())
                                    {
                                        totalcharges = findloanrequestcharge.Sum(x => x.TransactionAmount);
                                    }
                                    if (findloanrequestbyid.Type == Enumerations.EFTType.Cheque)
                                    {
                                        findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Active;
                                        findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Completed;
                                    }
                                    else
                                    {
                                        findloanrequestbyid.Status = (int)Enumerations.LoanStatus.Disbursed;
                                        findloanrequestbyid.ProcessingStatus = (int)Enumerations.ProcessingStatus.Disburse;
                                    }

                                    var findLoanProductById = loanproductservice.FindLoanProductById(findloanrequestbyid.LoanProductId);
                                    if (findLoanProductById.PaymentFrequencyType == Enumerations.PaymentFrequency.Monthly)
                                    {
                                        findloanrequestbyid.NextPaymentDate = DateTime.Now.AddMonths(1);
                                    }

                                    else
                                    {
                                        findloanrequestbyid.NextPaymentDate = DateTime.Now.AddDays(7);
                                    }

                                    findloanrequestbyid.DisbursedAmount = findloanrequestbyid.LoanAmount;
                                    findloanrequestbyid.DisbursementDate = DateTime.Now;
                                    int addDays = findloanrequestbyid.NegotiatedInstallments * (int)findloanrequestbyid.LoanProduct.PaymentFrequencyType;
                                    findloanrequestbyid.LoanEndDate = DateTime.Now.AddDays(addDays);
                                    findloanrequestbyid.DisbursedBy = Request.RequestContext.HttpContext.User.Identity.Name;

                                    loanrequestService.UpdateLoanRequest(findloanrequestbyid, serviceHeader);
                                    loantransactionhistoryservice.AddNewLoanHistory(creditmpesaglaccount, serviceHeader);
                                    loantransactionhistoryservice.AddNewLoanHistory(debitsavingsaccount, serviceHeader);
                                }
                                catch (Exception ex)
                                {
                                    ModelState.AddModelError(string.Empty, "An error has occurred while processing your request.");
                                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                    return View(findloanrequestbyid);
                                }
                                transaction.Commit();
                            }

                            catch (Exception ex)
                            {
                                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                transaction.Rollback();
                                ModelState.AddModelError(string.Empty, "An error has occurred while processing your request.");
                                return View(findloanrequestbyid);
                            }
                        }
                    }
                }
                else
                {
                    decimal savingAccountBalance = 0m;
                    savingAccountBalance = findCustomerSavingsAccountBalance - findloanrequestbyid.LoanAmount;
                    string errorMessage = string.Format("Sorry but savings account has insufficient funds to facilitate loan processing fee. The available balance is KES {0}", savingAccountBalance);
                    ModelState.AddModelError(string.Empty, errorMessage);
                    return View(findloanrequestbyid);
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "This loan has already been disbursed.");
                return View(findloanrequestbyid);
            }
            return RedirectToAction("index");
        }

        private void LoanRequestCharges(Guid LoanRequestId, Guid ChartofAccountsId, Guid GraduatedScaleId, decimal amount)
        {
            LoanRequestCharge loanrequestcharge = new LoanRequestCharge();
            loanrequestcharge.GraduatedScaleId = GraduatedScaleId;
            loanrequestcharge.ChartofAccountId = ChartofAccountsId;
            loanrequestcharge.LoanRequestId = LoanRequestId;
            loanrequestcharge.TransactionAmount = amount;
            loanrequestchargeservice.AddLoanRequestCharge(loanrequestcharge, serviceHeader);
        }

        public ActionResult loanschedule(Guid Id)
        {
            var getuserdetails = customerservice.FindCustomerByMobileNumber(User.Identity.Name);
            ViewBag.FullNames = getuserdetails.FullName;
            ViewBag.Id = Id;
            return View();            
        }

        public JsonResult GetLoanSchedule(Guid Id, JQueryDataTablesModel datatablemodel)
        {                       
            var pagecollection = LoanScheduler(Id);
            
            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var customerloanschedule = CustomerLoanSchedule(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, CustomerLoanSchedule: pagecollection);


            return Json(new JQueryDataTablesResponse<LoanScheduleModel>(items: customerloanschedule,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<LoanScheduleModel> CustomerLoanSchedule(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanScheduleModel> CustomerLoanSchedule)
        {
            var GetCustomerLoanRequest = CustomerLoanSchedule;

            totalRecordCount = GetCustomerLoanRequest.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomerLoanRequest = GetCustomerLoanRequest.Where(c => c.OutstandingLoanBalance.ToString()
                    .ToLower().Contains(searchString.ToLower()) || c.NextPaymentDate.ToString().ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetCustomerLoanRequest.Count;

            IOrderedEnumerable<LoanScheduleModel> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "NextPaymentDate":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.NextPaymentDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.NextPaymentDate);
                        break;

                    case "PrincipleAmount":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.PrincipleAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.PrincipleAmount);
                        break;

                    case "InterestAmount":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount);
                        break;

                    case "TotalAmount":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.TotalAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TotalAmount);
                        break;

                    case "OutstandingLoanBalance":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.OutstandingLoanBalance)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.OutstandingLoanBalance);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        public ActionResult AccountStatement(Guid Id)
        {
            ViewBag.Id = Id;

            return View();
        }

        public JsonResult GetCustomerAccountStatement(Guid Id, JQueryDataTablesModel datatablemodel)
        {           
            var pagecollection = loantransactionhistoryservice.GetLoanTransactionHistoryByLoanRequestId(Id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var Accountstatement = CustomerAccountStatement(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, AccountStatement: pagecollection);


            return Json(new JQueryDataTablesResponse<LoanTransactionHistory>(items: Accountstatement,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<LoanTransactionHistory> CustomerAccountStatement(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanTransactionHistory> AccountStatement)
        {
            var GetCustomerLoanRequest = AccountStatement;

            totalRecordCount = GetCustomerLoanRequest.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomerLoanRequest = GetCustomerLoanRequest.Where(c => c.LoanRequest.CustomerAccount.Customer.FullName.ToString()
                    .ToLower().Contains(searchString.ToLower()) || c.LoanRequest.LoanReferenceNumber.ToString().ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetCustomerLoanRequest.Count;

            IOrderedEnumerable<LoanTransactionHistory> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "LoanRequest.LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.LoanReferenceNumber);
                        break;

                    case "LoanRequest.CustomerAccount.Customer.FullName":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.CustomerAccount.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.CustomerAccount.Customer.FullName);
                        break;

                    case "TransactionTypeDesc":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.TransactionTypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransactionTypeDesc);
                        break;

                    case "RepaymentTypeDesc":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.RepaymentTypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.RepaymentTypeDesc);
                        break;

                    case "TransactionAmount":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.TransactionAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransactionAmount);
                        break;
                    
                    case "TransactionCategoryDesc":
                        sortedClients = sortedClients == null ? GetCustomerLoanRequest.CustomSort(sortedColumn.Direction, cust => cust.TransactionCategoryDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TransactionCategoryDesc);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult CancelLoan(Guid Id)
        {
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(Id);            

            return View(findloanrequestbyid);
        }

        [ActionName("cancelloan")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CustomerCancelLoan(LoanRequest loanrequest)
        {
            try
            {
                var findloanrequestbyid = loanrequestService.GetLoanRequestById(loanrequest.Id);

                if (string.IsNullOrWhiteSpace(loanrequest.Remarks))
                {
                    ModelState.AddModelError(string.Empty, "You must provide a reason.");
                    return View(findloanrequestbyid);
                }

                if (findloanrequestbyid.Status == (int)Enumerations.LoanStatus.Pending)
                {
                    findloanrequestbyid.Status = (int)Enumerations.LoanStatus.CustomerRejected;

                    findloanrequestbyid.Remarks = loanrequest.Remarks;

                    loanrequestService.UpdateLoanRequest(findloanrequestbyid, serviceHeader);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "This loan request has been rejected.");
                    return View(loanrequest);
                }

                return RedirectToAction("loanrequestlist", "Customer");
            }

            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "An error has occurred while processing your request.");
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
            return View();
        }

        //[HttpGet]
        public ActionResult CustomerLoanSchedule(Guid Id)
        {
            ViewBag.Id = Id;

            return View();
        }

        public JsonResult GetCustomerLoanSchedule(Guid Id, JQueryDataTablesModel datatablemodel)
        {
            int totalrecordcount = 0;

            int searchrecordcount = 0;

            var sortAscending = datatablemodel.sSortDir_.First() == "asc" ? true : false;

            var sortedcolumns = (from s in datatablemodel.GetSortedColumns() select s.PropertyName).ToList();

            var pagecollection = LoanScheduler(Id);

            if (pagecollection != null)
            {
                totalrecordcount = pagecollection.Count();

                searchrecordcount = !string.IsNullOrWhiteSpace(datatablemodel.sSearch) ? pagecollection.Count() : totalrecordcount;
            }

            var result = Extensions.ControllerExtensions.DataTablesJson(this, items: pagecollection, totalRecords: totalrecordcount, totalDisplayRecords: searchrecordcount, sEcho: datatablemodel.sEcho);

            return result;
        }


        [HttpGet]        
        public ActionResult CreateNewLoanRequest(string Id)
        {
            var customerlist = GetCustomerlist(Guid.Empty.ToString());
            ViewBag.RelationshipManagersSelectList = RelationshipManagerSelectList(Guid.Empty.ToString());
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            //ViewBag.BranchesSelectList = GetBranchesSelectList(string.Empty, string.Empty);
            ViewBag.EFTTypes = EFTInfo(null);
            var loanproductlist = Loanproductlist(Guid.Empty.ToString());
            ViewBag.CustomersList = customerlist;
            ViewBag.LoanProductsList = loanproductlist;
            string customerMobileNumber = string.Empty;
            if (string.IsNullOrWhiteSpace(Id))
            {
                Id = Guid.Empty.ToString();
                Guid customerId = Guid.Parse(Id);                
            }
            else
            {
                Guid CustomerId = Guid.Parse(Id);
                var findCustomerById = customerservice.FindCustomerById(CustomerId);
                customerMobileNumber = findCustomerById.MobileNumber;
            }
            var loanrequest = new LoanRequest()
            {                
                NextPaymentDate = DateTime.Today,
                CustomerId = Guid.Parse(Id),
                PrimaryAccountNumber = customerMobileNumber
            };
            return View(loanrequest);
        }

        [HttpPost]
        public ActionResult CreateNewLoanRequest(string Id, [Bind(Exclude = "NegotiatedInstallments")]LoanRequest loanrequest)
        {
            var customerlist = GetCustomerlist(Id);
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            ViewBag.EFTTypes = EFTInfo(null);
            ViewBag.RelationshipManagersSelectList = RelationshipManagerSelectList(Guid.Empty.ToString());
            //ViewBag.BranchesSelectList = GetBranchesSelectList(string.Empty, string.Empty);
            var loanproductlist = Loanproductlist(Guid.Empty.ToString());
            ViewBag.CustomersList = customerlist;
            ViewBag.LoanProductsList = loanproductlist;            
            var selectedtype = Request.Form["Type"];
            var customerId = Request.Form["CustomerId"];
            string customerMobileNumber = string.Empty;
            string paybillNumber = string.Empty;
            if (selectedtype == null)
            {
                ModelState.AddModelError(string.Empty, "You must select money transfer mode.");
                return View(loanrequest);
            }
            var efttype = int.Parse(selectedtype);
            
            if (loanrequest.LoanProductId == Guid.Empty)
            {
                ModelState.AddModelError(string.Empty, "You must select a loan product from the provided list.");
                return View(loanrequest);
            }

            if (loanrequest.RelationshipManagerId == Guid.Empty)
            {
                ModelState.AddModelError(string.Empty, "Loan Officer is required.");
                return View(loanrequest);
            }

            if (loanrequest.CustomerId == Guid.Empty)
            {
                ModelState.AddModelError(string.Empty, "You must select a customer from the list.");
                return View(loanrequest);
            }

            if (loanrequest.AcceptTermsAndConditions == false)
            {
                ModelState.AddModelError(string.Empty, "You must accept terms and conditions.");
                return View(loanrequest);
            }

            UserInfo userInfo = userInfoService.FindUserByUserName(User.Identity.Name);
            if (userInfo == null)
            {
                ModelState.AddModelError(string.Empty, "An error has occurred while processing. Please contact system administrator for assistance.");                
                LoggerFactory.CreateLog().LogError("Error -> {0}", "No current user found while processing this loan request.");
                return View(loanrequest);
            }
            Tuple<decimal, decimal, decimal> amountvalues = loanrequestService.GetMaxMinDefaultLoanAmount(loanrequest.LoanProductId);

            Tuple<int, decimal> termandinterestrate = loanproductservice.GetLoanTermandInterestRateByProductId(loanrequest.LoanProductId);

            var findcustomerbycustomerid = customerservice.FindCustomerById(loanrequest.CustomerId);
            if (findcustomerbycustomerid == null)
            {
                ModelState.AddModelError(string.Empty, "Failed while retrieving customer details. Customer doesn't exist.");
                return View();
            }

            loanrequest.LoanEndDate = DateTime.Today.AddMonths(termandinterestrate.Item1);

            string loanrefno = GenerateRandomLoanReferenceNumber(8);
            var findloanproductbyid = loanproductservice.FindLoanProductById(loanrequest.LoanProductId);
            loanrequest.LoanStartDate = DateTime.Now;
            loanrequest.BranchId = userInfo.BranchId;
            //loanrequest.NegotiatedInstallments = NegotiatedInstallments;
            if (loanrequest.NegotiatedInstallments == 0)
            {
                if (findloanproductbyid.PaymentFrequencyType == Enumerations.PaymentFrequency.Monthly)
                {
                    loanrequest.NegotiatedInstallments = termandinterestrate.Item1;
                }
                else if(findloanproductbyid.PaymentFrequencyType == Enumerations.PaymentFrequency.Weekly)
                {
                    loanrequest.NegotiatedInstallments = termandinterestrate.Item1 * 4;
                }
                
            }
            if (ModelState.IsValid)
            {

                if (efttype == (int)Enumerations.EFTType.AccountToMobile)
                {
                    Regex regex = new Regex(@"^254(?:[0-9]??){6,14}[0-9]$");
                    Match match = regex.Match(loanrequest.PrimaryAccountNumber);
                    if (!match.Success)
                    {
                        ModelState.AddModelError(string.Empty, "Failed. If transfer mode is to mobile, the Primary Account Number should start 254, followed by the national number");
                        return View(loanrequest);
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(loanrequest.ApplicantBankCode))
                    {
                        ModelState.AddModelError(string.Empty, "Failed. If transfer mode is to bank account, you must select your Bank from the list.");
                        return View(loanrequest);
                    }
                    else
                    {
                        paybillNumber = GetBranchesSelectList(loanrequest.ApplicantBankCode);
                        if (paybillNumber.Equals("0"))
                        {
                            ModelState.AddModelError(string.Empty, "The system could not much a valid Paybill Number with the selected bank. Please select another bank or change the funds transfer method.");
                            return View(loanrequest);
                        }
                    }
                }

                var findcustomeraccountnumbers = customeraccountservice.GetAllCustomerAccountNumbers(loanrequest.CustomerId).ToArray();
                
                var NextCustomerAccountLoanAccountNumber = customerservice.NextCustomerLoanAccountNumber(findcustomerbycustomerid.CustomerNumber, findcustomerbycustomerid.Id, findloanproductbyid.ProductCode);
                CustomersAccount customersaccount = new CustomersAccount();
                customersaccount.AccountNumber = NextCustomerAccountLoanAccountNumber;
                customersaccount.IsActive = true;
                customersaccount.AccountType = (int)Enumerations.ProductCode.Loan;
                customersaccount.CustomerId = findcustomerbycustomerid.Id;
                try
                {
                    var checkingoutstandingloan = loanrequestService.GetLoanRequestByCustomerAccountId(loanrequest.CustomerId);
                    int getnotificationenabledmode = customerservice.GetCustomerEnabledNotificationMode(loanrequest.CustomerId);
                    var countunclearedloans = checkingoutstandingloan.Where(x => x.Status != (int)Enumerations.LoanStatus.Cleared && x.Status != (int)Enumerations.LoanStatus.Rejected && x.Status != (int)Enumerations.LoanStatus.CustomerRejected && x.Status != (int)Enumerations.LoanStatus.Review && x.Status != (int)Enumerations.LoanStatus.TransactionReversed).Count();
                    if (loanrequest.LoanAmount <= amountvalues.Item1 && loanrequest.LoanAmount >= amountvalues.Item2)
                    {
                        if (checkingoutstandingloan.Count() != 0)
                        {
                            if (countunclearedloans >= 1)
                            {
                                ModelState.AddModelError(string.Empty, "Sorry, you have an uncleared loan. Clear it first and try again later.");
                                return View(loanrequest);
                            }
                            else
                            {
                                decimal interest = 0m;
                                if (findloanproductbyid.InterestChargeType == (int)Enumerations.InterestChargeType.Fixed)
                                {                                    
                                    interest = loanrequest.NegotiatedInstallments * findloanproductbyid.InterestValue;
                                }
                                else
                                {
                                    var interestratepermonth = findloanproductbyid.InterestValue / 12;
                                    interest = (interestratepermonth * loanrequest.NegotiatedInstallments * loanrequest.LoanAmount) / 100;
                                }

                                loanrequest.InterestAmount = interest;
                                loanrequest.LoanReferenceNumber = loanrefno;                                
                                loanrequest.CustomerAccountId = customersaccount.Id;
                                if (efttype == (int)Enumerations.EFTType.BusinessToBusiness)
                                {
                                    loanrequest.BankCode = paybillNumber;
                                    loanrequest.Type = Enumerations.EFTType.BusinessToBusiness;
                                }
                                if (efttype == (int)Enumerations.EFTType.AccountToMobile)
                                {
                                    loanrequest.Type = Enumerations.EFTType.AccountToMobile;
                                }
                                else if (efttype == (int)Enumerations.EFTType.Cheque)
                                {
                                    loanrequest.Type = Enumerations.EFTType.Cheque;
                                }

                                loanrequest.Status = (int)Enumerations.LoanStatus.Pending;
                                loanrequest.Id = Guid.NewGuid();
                                customeraccountservice.AddNewCustomersAccount(customersaccount, serviceHeader);
                                loanrequestService.AddNewLoanRequest(loanrequest, serviceHeader);
                                NewNotificationEmail(ConfigurationManager.AppSettings["NotificationsEmailAddress"].ToString(), findcustomerbycustomerid.FullName);
                                if (getnotificationenabledmode == 1)
                                {
                                    NewEmailAlert(findcustomerbycustomerid.FullName, loanrefno, findcustomerbycustomerid.EmailAddress, loanrequest.LoanAmount);
                                }

                                else if (getnotificationenabledmode == 2)
                                {
                                    NewTextAlert(findcustomerbycustomerid.FirstName, loanrefno, findcustomerbycustomerid.MobileNumber, loanrequest.LoanAmount);
                                }
                                else if (getnotificationenabledmode == 3)
                                {
                                    NewEmailAlert(findcustomerbycustomerid.FullName, loanrefno, findcustomerbycustomerid.EmailAddress, loanrequest.LoanAmount);
                                    NewTextAlert(findcustomerbycustomerid.FirstName, loanrefno, findcustomerbycustomerid.MobileNumber, loanrequest.LoanAmount);
                                }
                                if (Roles.IsUserInRole("LoanOfficer"))
                                {
                                    return RedirectToAction("loanrequestlist", "LoanRequest");
                                }
                                return RedirectToAction("allloanrequest", "LoanRequest");
                            }
                        }

                        else
                        {
                            //decimal interestpermonth = termandinterestrate.Item2 / 12;
                            if (efttype == (int)Enumerations.EFTType.BusinessToBusiness)
                            {
                                loanrequest.BankCode = paybillNumber;
                                loanrequest.Type = Enumerations.EFTType.BusinessToBusiness;
                            }
                            if (efttype == (int)Enumerations.EFTType.AccountToMobile)
                            {
                                loanrequest.Type = Enumerations.EFTType.AccountToMobile;
                            }
                            else
                            {
                                loanrequest.Type = Enumerations.EFTType.Cheque;
                            }

                            decimal interest = 0m;
                            if (findloanproductbyid.InterestChargeType == (int)Enumerations.InterestChargeType.Fixed)
                            {
                                interest = loanrequest.NegotiatedInstallments * findloanproductbyid.InterestValue;
                            }
                            else
                            {
                                var interestratepermonth = findloanproductbyid.InterestValue / 12;
                                interest = (interestratepermonth * loanrequest.NegotiatedInstallments * loanrequest.LoanAmount) / 100;
                            }

                            loanrequest.InterestAmount = interest;
                            loanrequest.LoanReferenceNumber = loanrefno;
                            loanrequest.Status = (int)Enumerations.LoanStatus.Pending;
                            loanrequest.CustomerAccountId = customersaccount.Id;                            
                            using (var datacontext = new DataContext())
                            {
                                using (var transaction = datacontext.Database.BeginTransaction())
                                {
                                    try
                                    {
                                        customeraccountservice.AddNewCustomersAccount(customersaccount, serviceHeader);
                                        loanrequestService.AddNewLoanRequest(loanrequest, serviceHeader);
                                        NewNotificationEmail(ConfigurationManager.AppSettings["NotificationsEmailAddress"].ToString(), findcustomerbycustomerid.FullName);
                                        if (getnotificationenabledmode == 1)
                                        {
                                            NewEmailAlert(findcustomerbycustomerid.FullName, loanrefno, findcustomerbycustomerid.EmailAddress, loanrequest.LoanAmount);
                                        }

                                        else if (getnotificationenabledmode == 2)
                                        {
                                            NewTextAlert(findcustomerbycustomerid.FullName, loanrefno, findcustomerbycustomerid.MobileNumber, loanrequest.LoanAmount);
                                        }
                                        else if (getnotificationenabledmode == 3)
                                        {
                                            NewEmailAlert(findcustomerbycustomerid.FullName, loanrefno, findcustomerbycustomerid.EmailAddress, loanrequest.LoanAmount);
                                            NewTextAlert(findcustomerbycustomerid.FirstName, loanrefno, findcustomerbycustomerid.MobileNumber, loanrequest.LoanAmount);
                                        }
                                    }

                                    catch (Exception ex)
                                    {
                                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                        return View(loanrequest);
                                    }
                                }
                            }

                            if (Roles.IsUserInRole("LoanOfficer"))
                            {
                                return RedirectToAction("loanrequestlist", "LoanRequest");
                            }
                            return RedirectToAction("allloanrequest", "LoanRequest");
                        }
                    }

                    else
                    {
                        string errormessage = string.Format("Minimum amount you can borrow is {0} and maximum amount {1}. Make sure loan amount is within the range", (amountvalues.Item2), (amountvalues.Item1));
                        ModelState.AddModelError(string.Empty, errormessage);
                        return View(loanrequest);
                    }
                }

                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, "Operation was unsuccessful, correct the errors and try again.");
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                }
            }
            return View(loanrequest);
        }

        //
        // GET: /Account/TermsAndConditions

        [Authorize]
        public ActionResult TermsAndConditions()
        {
            // Return view
            return File("~/Content/Uploads/bank_terms.pdf", "application/pdf");
        }

        public ActionResult Documents(Guid id)
        {
            ViewBag.Id = id;
            var findloanrequestbyid = loanrequestService.GetLoanRequestById(id);
            ViewBag.CustomerFullName = findloanrequestbyid.CustomerAccount.Customer.FullName;
            ViewBag.LoanRefNo = findloanrequestbyid.LoanReferenceNumber;

            return View();
        }

        public JsonResult GetLoanRequestDocuments(JQueryDataTablesModel datatablemodel, Guid Id)
        {
            var customerdocuments = loanrequestdocumentservice.GetAllLoanRequestDocumentsByLoanRequestId(Id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetAllLoanRequestDocuments(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, CustomerLoanDocumentList: customerdocuments);


            var results = Json(new JQueryDataTablesResponse<LoanRequestDocumentsService.LoanRequestDocumentView>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));

            results.MaxJsonLength = Int32.MaxValue;

            return results;
        }

        public static IList<LoanRequestDocumentsService.LoanRequestDocumentView> GetAllLoanRequestDocuments(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanRequestDocumentsService.LoanRequestDocumentView> CustomerLoanDocumentList)
        {
            var GetCustomerDocumentList = CustomerLoanDocumentList;

            totalRecordCount = GetCustomerDocumentList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomerDocumentList = CustomerLoanDocumentList.Where(c => c.LoanRequest.LoanReferenceNumber
                    .ToLower().Contains(searchString.ToLower())
                    || c.LoanRequest.CustomerAccount.Customer.FullName.ToLower().Contains(searchString.ToLower())
                    ).ToList();
            }

            searchRecordCount = GetCustomerDocumentList.Count;

            IOrderedEnumerable<LoanRequestDocumentsService.LoanRequestDocumentView> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "LoanRequest.CustomerAccount.Customer.FullName.FullName":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.CustomerAccount.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.CustomerAccount.Customer.FullName);
                        break;

                    case "LoanRequest.LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanRequest.LoanReferenceNumber);
                        break;

                    case "DocumentTypeDesc":
                        sortedClients = sortedClients == null ? GetCustomerDocumentList.CustomSort(sortedColumn.Direction, cust => cust.DocumentTypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.DocumentTypeDesc);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult EditLoanRequest(Guid Id)
        {
            var findLoanRequestById = loanrequestService.GetLoanRequestById(Id);
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            ViewBag.EFTTypes = EFTInfo(findLoanRequestById);
            if (findLoanRequestById == null)
                return View();
            
            return View(findLoanRequestById);
        }

        [HttpPost]
        public ActionResult EditLoanRequest(/*string NextPaymentDate, */LoanRequest loanrequest)
        {
            var findloanrequest = loanrequestService.GetLoanRequestById(loanrequest.Id);
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            ViewBag.EFTTypes = EFTInfo(findloanrequest);
            if (findloanrequest == null)
                throw new ArgumentNullException("loanrequest cannot be null");

            var selectedtype = Request.Form["Type"];
            string paybillNumber = string.Empty;
            if (selectedtype == null)
            {
                ModelState.AddModelError(string.Empty, "You must select money transfer mode.");
                return View(loanrequest);
            }
            var efttype = int.Parse(selectedtype);
            //var efttype = (int)loanrequest.Type;

            try
            {                
                if (efttype == (int)Enumerations.EFTType.AccountToMobile)
                {
                    Regex regex = new Regex(@"^254(?:[0-9]??){6,14}[0-9]$");
                    Match match = regex.Match(loanrequest.PrimaryAccountNumber);
                    if (!match.Success)
                    {
                        ModelState.AddModelError(string.Empty, "Failed. If transfer mode is to mobile, the Primary Account Number should start 254, followed by the national number");
                        return View(loanrequest);
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(loanrequest.ApplicantBankCode))
                    {
                        ModelState.AddModelError(string.Empty, "Failed. If transfer mode is to bank account, you must select your Bank from the list.");
                        return View(loanrequest);
                    }
                    else
                    {
                        paybillNumber = GetBranchesSelectList(loanrequest.ApplicantBankCode);
                        if (paybillNumber.Equals("0"))
                        {
                            ModelState.AddModelError(string.Empty, "The system could not much a valid Paybill Number with the selected bank. Please select another bank or change the funds transfer method.");
                            return View(loanrequest);
                        }
                    }
                }
                if (efttype == (int)Enumerations.EFTType.BusinessToBusiness)
                {
                    findloanrequest.BankCode = paybillNumber;
                    findloanrequest.Type = Enumerations.EFTType.BusinessToBusiness;
                }
                if (efttype == (int)Enumerations.EFTType.AccountToMobile)
                {
                    findloanrequest.Type = Enumerations.EFTType.AccountToMobile;
                }
                if (efttype == (int)Enumerations.EFTType.Cheque)
                {
                    findloanrequest.Type = Enumerations.EFTType.Cheque;
                }

                findloanrequest.PrimaryAccountNumber = loanrequest.PrimaryAccountNumber;                
                findloanrequest.LoanAmount = loanrequest.LoanAmount;
                findloanrequest.OutstandingLoanBalance = loanrequest.OutstandingLoanBalance;
                findloanrequest.NegotiatedInstallments = loanrequest.NegotiatedInstallments;
                //findloanrequest.NextPaymentDate = loanrequest.NextPaymentDate;
                if (findloanrequest.Status == (int)Enumerations.LoanStatus.Pending)
                {
                    loanrequestService.UpdateLoanRequest(findloanrequest, serviceHeader);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "This loan cannot be edited at this stage.");
                    return View(loanrequest);

                }
            }

            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                return View(loanrequest);
            }
            return RedirectToAction("AllLoanRequest");
        }


        [NonAction]
        public List<SelectListItem> GetCustomerlist(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultcustomer = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Customer", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultcustomer);

            var othercustomers = customerservice.GetAllCustomers().Where(x=> x.IsEnabled == true && x.IsApproved == true);

            if (string.IsNullOrWhiteSpace(selectedvalue))
            {
                selectedvalue = Guid.Empty.ToString();
            }
            foreach (var item in othercustomers)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.FullNamemobileNumber, Value = item.Id.ToString("D") });

            return SelectList;
        }

        [NonAction]
        public List<SelectListItem> Loanproductlist(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultloan = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Loan Product", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultloan);

            var otherloanlist = loanproductservice.GetAllLoanProducts().Where(x => x.IsEnabled == true);

            foreach (var item in otherloanlist)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.LoanProductDescription, Value = item.Id.ToString("D") });

            return SelectList;
        }

        [NonAction]
        public List<SelectListItem> DefaultLoanproductlist(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultloan = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Loan Product", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultloan);

            var otherloanlist = loanproductservice.FindDefaultLoanProductList().Where(x => x.IsEnabled == true);

            foreach (var item in otherloanlist)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.LoanProductDescription, Value = item.Id.ToString("D") });

            return SelectList;
        }

        public List<SelectListItem> RelationshipManagerSelectList(string selectdValue)
        {
            var currentUser = User.Identity.Name;
            var findCurrentUserInfo = userInfoService.FindUserByUserName(currentUser);
            List<SelectListItem> selectList = new List<SelectListItem>();

            var defaultRM = new SelectListItem { Selected = (selectdValue == Guid.Empty.ToString("D")), Text = "Select Relationship Manager", Value = Guid.Empty.ToString("D") };

            selectList.Add(defaultRM);
            if (findCurrentUserInfo != null)
            {
                var otherRM = relationshipManagerService.FindRelationshipManagerByBranchId(findCurrentUserInfo.BranchId).Where(x => x.IsEnabled == true);

                foreach (var item in otherRM)
                    selectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectdValue)), Text = item.RMFullName, Value = item.Id.ToString("D") });
            }     
       

            return selectList;
        }

        /// <summary>
        /// Loan reference number generator
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private string GenerateRandomLoanReferenceNumber(int length)
        {
            string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[length];
            Random rd = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }
	}
}
﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.Repayments
{
    public interface IRepayment
    {
        IList<LoanRequest> GetLoanRequestsDueToday();

        LoanRequest FindLoanRequestById(Guid id);

        IList<LoanRequest> FindActiveMicroLoanRequest();

        Tuple<decimal, decimal> GetTotalExpectedPaidAmountFromDisbursementDateToNextPaymentDate(Guid loanRequestId);
    }
}

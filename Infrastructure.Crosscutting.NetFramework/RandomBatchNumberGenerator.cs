﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Crosscutting.NetFramework
{
    public static class RandomBatchNumberGenerator
    {
        //Function to get random number
        private static readonly Random getrandom = new Random();
        private static readonly object syncLock = new object();
        public static int GetRandomNumber()
        {
            lock (syncLock)
            { // synchronize
                return getrandom.Next(1000000000);
            }
        }
    }
}

﻿$(function () {
    $('#chartaccountcatergory').change(function () {
        var id = $('#chartaccountcatergory :selected').val();
        id = id == "" ? 0 : id;
        //When select 'optionLabel' we need to reset it to default as well. So not need 
        //travel back to server.
        if (id == "") {
            $('#selectedLanguange').empty();
            $('#selectedLanguange').append('<option value="">--Select a language--</option>');
            return;
        }

        //This is where the dropdownlist cascading main function
        $.ajax({
            type: "POST",
            url: "ChartofAccount/AccountSelectList",                            //Your Action name in the DropDownListConstroller.cs
            data: "{'id':" + id + "}",  //Parameter in this function, Is cast sensitive and also type must be string
            contentType: "application/json; charset=utf-8",
            dataType: "json"

        }).done(function (data) {
            //When succeed get data from server construct the dropdownlist here.
            if (data != null) {

                $('#selectedLanguange').empty();
                $.each(data.languanges, function (index, data) {
                    $('#selectedLanguange').append('<option value="' + data.Value + '">' + data.Text + '</option>');
                });


            }
        }).fail(function (response) {
            if (response.status != 0) {
                alert(response.status + " " + response.statusText);
            }
        });
    });



});
﻿using Nikopeshe.Entity.DataModels;
using Nikopeshe.Entity.Helpers;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Models;
using Nikopeshe.WebApplication.Utilities;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Nikopeshe.WebApplication.Controllers
{
    public class County
    {
        public string Code { get; set; }

        public string Name { get; set; }

        HashSet<Constituency> _constituency;

       
        public virtual ICollection<Constituency> Constituencies
        {
            get
            {
                if (_constituency == null)
                    _constituency = new HashSet<Constituency>();

                return _constituency;
            }
            set
            {
                _constituency = new HashSet<Constituency>(value);
            }
        }   
    }

    public class Constituency
    {
        public string Code { get; set; }

        public string Name { get; set; }

        HashSet<CAW> _countyAssemblyWard;

        public virtual ICollection<CAW> CountyAssemblyWards
        {
            get
            {
                if (_countyAssemblyWard == null)
                    _countyAssemblyWard = new HashSet<CAW>();

                return _countyAssemblyWard;
            }
            set
            {
                _countyAssemblyWard = new HashSet<CAW>(value);
            }
        }
    }

    public class CAW
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }

    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class BaseController : Controller
    {
        [NonAction]
        public List<County> GetCountyList()
        {
            var countyCodes = new List<string>();

            var countyNames = new List<string>();

            var constituencyCodes = new List<string>();

            var constituencyNames = new List<string>();

            var constituencyIndexes = new List<int>();

            var cawCodes = new List<string>();

            var cawNames = new List<string>();

            var cawIndexes = new List<int>();

            string cawsFileName = string.Format(@"{0}Content\uploads\County_Constituency_CAW.xls", AppDomain.CurrentDomain.BaseDirectory);

            using (FileStream file = new FileStream(cawsFileName, FileMode.Open, FileAccess.Read))
            {
                HSSFWorkbook hssfWorkbook = new HSSFWorkbook(file);

                if (hssfWorkbook != null)
                {
                    ISheet sheet = hssfWorkbook.GetSheetAt(0);

                    if (sheet != null)
                    {
                        IEnumerator rows = sheet.GetRowEnumerator();

                        if (rows != null)
                        {
                            rows.MoveNext(); // skip header row

                            int rowCounter = default(int);

                            while (rows.MoveNext())
                            {
                                IRow row = (HSSFRow)rows.Current;

                                ICell cell1Value = row.GetCell(0);  // COUNTY CODE
                                ICell cell2Value = row.GetCell(1);  // COUNTY NAME
                                ICell cell3Value = row.GetCell(2);  // CONST. CODE
                                ICell cell4Value = row.GetCell(3);  // CONST. NAME
                                ICell cell5Value = row.GetCell(4);  // CAW CODE
                                ICell cell6Value = row.GetCell(5);  // COUNTY ASSEMBLY WARD NAME

                                object col1Value = cell1Value != null ? cell1Value.ToString() : null;
                                object col2Value = cell2Value != null ? cell2Value.ToString() : null;
                                object col3Value = cell3Value != null ? cell3Value.ToString() : null;
                                object col4Value = cell4Value != null ? cell4Value.ToString() : null;
                                object col5Value = cell5Value != null ? cell5Value.ToString() : null;
                                object col6Value = cell6Value != null ? cell6Value.ToString() : null;

                                if (!countyCodes.Contains(col1Value.ToString().Trim()))
                                {
                                    countyCodes.Add(col1Value.ToString().Trim());

                                    constituencyIndexes.Add(rowCounter);
                                }

                                if (!countyNames.Contains(col2Value.ToString().Trim()))
                                    countyNames.Add(col2Value.ToString().Trim());

                                constituencyCodes.Add(col3Value.ToString().Trim());

                                constituencyNames.Add(col4Value.ToString().Trim());

                                //if (!cawCodes.Contains(col5Value.ToString().Trim()))
                                //{
                                //    cawCodes.Add(col5Value.ToString().Trim());

                                //    cawIndexes.Add(rowCounter);
                                //}

                                //if (!cawCodes.Contains(col5Value.ToString().Trim()))
                                //    cawCodes.Add(col5Value.ToString().Trim());

                                //cawCodes.Add(col5Value.ToString().Trim());

                                //cawNames.Add(col6Value.ToString().Trim());

                                rowCounter += 1;
                            }
                        }
                    }
                }
            }

            #region
            var tupleList = new List<Tuple<long, long>>();

            //var tupleList2 = new List<Tuple<long, long>>();

            var indexCounter = 0;

            foreach (var item in constituencyIndexes)
            {
                if (indexCounter + 1 < constituencyIndexes.Count)
                {
                    var tuple = new Tuple<long, long>(constituencyIndexes[indexCounter], constituencyIndexes[indexCounter + 1]);

                    tupleList.Add(tuple);
                }
                else
                {
                    var tuple = new Tuple<long, long>(constituencyIndexes.Last(), constituencyCodes.Count);

                    tupleList.Add(tuple);
                }

                indexCounter += 1;
            }

            //foreach (var item in cawIndexes)
            //{
            //    if (indexCounter + 1 < cawIndexes.Count)
            //    {
            //        var tuple = new Tuple<long, long>(cawIndexes[indexCounter], cawIndexes[indexCounter + 1]);

            //        tupleList2.Add(tuple);
            //    }
            //    else
            //    {
            //        var tuple = new Tuple<long, long>(cawIndexes.Last(), constituencyCodes.Count);

            //        tupleList2.Add(tuple);
            //    }

            //    indexCounter += 1;
            //}

            var constituencyNameSets = new List<string[]>();

            var constituencyCodeSets = new List<string[]>();

            foreach (var tuple in tupleList)
            {
                var skip = (int)tuple.Item1;

                var take = (int)tuple.Item2 - (int)tuple.Item1;

                var targetConstituencyNames = constituencyNames.Skip(skip).Take(take).Distinct().ToArray();

                constituencyNameSets.Add(targetConstituencyNames);

                var targetConstituencyCodes = constituencyCodes.Skip(skip).Take(take).Distinct().ToArray();

                constituencyCodeSets.Add(targetConstituencyCodes);
            }

            //var cwaNameSets = new List<string[]>();

            //var cwaCodeSets = new List<string[]>();

            //foreach (var tuple in tupleList)
            //{
            //    var skip = (int)tuple.Item1;

            //    var take = (int)tuple.Item2 - (int)tuple.Item1;

            //    var targetCwaNames = cawNames.Skip(skip).Take(take).Distinct().ToArray();

            //    cwaNameSets.Add(targetCwaNames);

            //    var targetCwayCodes = cawCodes.Skip(skip).Take(take).Distinct().ToArray();

            //    cwaCodeSets.Add(targetCwayCodes);
            //}

            #endregion

            #region banks

            var countyList = new List<County>();

            var countyCounter = 0;

            foreach (var countyName in countyNames)
            {
                var county = new County { Code = countyCodes[countyCounter], Name = countyName };

                var constituencyCounter = 0;

                var countySet = constituencyNameSets[countyCounter];
                foreach (var constituencyName in countySet)
                {
                    var constituency = new Constituency { Code = constituencyCodeSets[countyCounter][constituencyCounter], Name = constituencyName };

                    county.Constituencies.Add(constituency);

                    constituencyCounter += 1;
                }

                countyList.Add(county);

                countyCounter += 1;
            }

            #endregion

            //#region Constituency

            //var ConstituencyList = new List<Constituency>();

            //var ConstituencyCounter = 0;

            //foreach (var countyName in constituencyNames)
            //{
            //    var county = new Constituency { Code = constituencyCodes[ConstituencyCounter], Name = countyName };

            //    var cawCounter = 0;

            //    var countySet = cwaNameSets[ConstituencyCounter];
            //    foreach (var constituencyName in countySet)
            //    {
            //        var constituency = new CAW { Code = cwaCodeSets[cawCounter][cawCounter], Name = constituencyName };

            //        county.CountyAssemblyWard.Add(constituency);

            //        cawCounter += 1;
            //    }

            //    ConstituencyList.Add(county);

            //    countyCounter += 1;
            //}

            //#endregion

            return countyList.OrderBy(x => x.Name).ToList();
        }

        public List<Constituency> GetConstituency2List()
        {
            var countyCodes = new List<string>();

            var countyNames = new List<string>();

            var constituencyCodes = new List<string>();

            var constituencyNames = new List<string>();

            var constituencyIndexes = new List<int>();

            var cawCodes = new List<string>();

            var cawNames = new List<string>();

            var cawIndexes = new List<int>();

            string cawsFileName = string.Format(@"{0}Content\uploads\County_Constituency_CAW.xls", AppDomain.CurrentDomain.BaseDirectory);

            using (FileStream file = new FileStream(cawsFileName, FileMode.Open, FileAccess.Read))
            {
                HSSFWorkbook hssfWorkbook = new HSSFWorkbook(file);

                if (hssfWorkbook != null)
                {
                    ISheet sheet = hssfWorkbook.GetSheetAt(0);

                    if (sheet != null)
                    {
                        IEnumerator rows = sheet.GetRowEnumerator();

                        if (rows != null)
                        {
                            rows.MoveNext(); // skip header row

                            int rowCounter = default(int);

                            while (rows.MoveNext())
                            {
                                IRow row = (HSSFRow)rows.Current;

                                ICell cell1Value = row.GetCell(0);  // COUNTY CODE
                                ICell cell2Value = row.GetCell(1);  // COUNTY NAME
                                ICell cell3Value = row.GetCell(2);  // CONST. CODE
                                ICell cell4Value = row.GetCell(3);  // CONST. NAME
                                ICell cell5Value = row.GetCell(4);  // CAW CODE
                                ICell cell6Value = row.GetCell(5);  // COUNTY ASSEMBLY WARD NAME

                                object col1Value = cell1Value != null ? cell1Value.ToString() : null;
                                object col2Value = cell2Value != null ? cell2Value.ToString() : null;
                                object col3Value = cell3Value != null ? cell3Value.ToString() : null;
                                object col4Value = cell4Value != null ? cell4Value.ToString() : null;
                                object col5Value = cell5Value != null ? cell5Value.ToString() : null;
                                object col6Value = cell6Value != null ? cell6Value.ToString() : null;

                                if (!countyCodes.Contains(col1Value.ToString().Trim()))
                                {
                                    countyCodes.Add(col1Value.ToString().Trim());

                                    constituencyIndexes.Add(rowCounter);
                                }

                                if (!countyNames.Contains(col2Value.ToString().Trim()))
                                    countyNames.Add(col2Value.ToString().Trim());

                                if (!constituencyCodes.Contains(col3Value.ToString().Trim()))
                                {
                                    //cawCodes.Add(col5Value.ToString().Trim());

                                    cawIndexes.Add(rowCounter);
                                }

                                constituencyCodes.Add(col3Value.ToString().Trim());

                                constituencyNames.Add(col4Value.ToString().Trim());



                                //if (!cawCodes.Contains(col5Value.ToString().Trim()))
                                //    cawCodes.Add(col5Value.ToString().Trim());

                                cawCodes.Add(col5Value.ToString().Trim());

                                cawNames.Add(col6Value.ToString().Trim());

                                rowCounter += 1;
                            }
                        }
                    }
                }
            }

            #region
            var tupleList = new List<Tuple<long, long>>();

            var tupleList2 = new List<Tuple<long, long>>();

            var indexCounter = 0;

            foreach (var item in constituencyIndexes)
            {
                if (indexCounter + 1 < constituencyIndexes.Count)
                {
                    var tuple = new Tuple<long, long>(constituencyIndexes[indexCounter], constituencyIndexes[indexCounter + 1]);

                    tupleList.Add(tuple);
                }
                else
                {
                    var tuple = new Tuple<long, long>(constituencyIndexes.Last(), constituencyCodes.Count);

                    tupleList.Add(tuple);
                }

                indexCounter += 1;
            }

            indexCounter = 0;
            foreach (var item in cawIndexes)
            {
                if (indexCounter + 1 < cawIndexes.Count)
                {
                    var tuple = new Tuple<long, long>(cawIndexes[indexCounter], cawIndexes[indexCounter + 1]);

                    tupleList2.Add(tuple);
                }
                else
                {
                    var tuple = new Tuple<long, long>(cawIndexes.Last(), cawCodes.Count);

                    tupleList2.Add(tuple);
                }

                indexCounter += 1;
            }

            var constituencyNameSets = new List<string[]>();

            var constituencyCodeSets = new List<string[]>();

            List<string> uniqueConstituencyCodes = constituencyCodes.Distinct().ToList();

            List<string> uniqueConstituencyNames = constituencyNames.Distinct().ToList();

            foreach (var tuple in tupleList)
            {
                var skip = (int)tuple.Item1;

                var take = (int)tuple.Item2 - (int)tuple.Item1;

                var targetConstituencyNames = constituencyNames.Skip(skip).Take(take).Distinct().ToArray();

                constituencyNameSets.Add(targetConstituencyNames);

                var targetConstituencyCodes = constituencyCodes.Skip(skip).Take(take).Distinct().ToArray();

                constituencyCodeSets.Add(targetConstituencyCodes);
            }

            var cwaNameSets = new List<string[]>();

            var cwaCodeSets = new List<string[]>();

            foreach (var tuple in tupleList2)
            {
                var skip = (int)tuple.Item1;

                var take = (int)tuple.Item2 - (int)tuple.Item1;

                var targetCwaNames = cawNames.Skip(skip).Take(take).Distinct().ToArray();

                cwaNameSets.Add(targetCwaNames);

                var targetCwayCodes = cawCodes.Skip(skip).Take(take).Distinct().ToArray();

                cwaCodeSets.Add(targetCwayCodes);
            }

            #endregion

            #region banks

            var countyList = new List<County>();

            var countyCounter = 0;

            foreach (var countyName in countyNames)
            {
                var county = new County { Code = countyCodes[countyCounter], Name = countyName };

                var constituencyCounter = 0;

                var countySet = constituencyNameSets[countyCounter];
                foreach (var constituencyName in countySet)
                {
                    var constituency = new Constituency { Code = constituencyCodeSets[countyCounter][constituencyCounter], Name = constituencyName };

                    county.Constituencies.Add(constituency);

                    constituencyCounter += 1;
                }

                countyList.Add(county);

                countyCounter += 1;
            }

            #endregion

            #region Constituency

            var ConstituencyList = new List<Constituency>();

            var ConstituencyCounter = 0;

            foreach (var constituencyName in uniqueConstituencyNames)
            {
                var constituency = new Constituency { Code = uniqueConstituencyCodes[ConstituencyCounter], Name = constituencyName };

                var cawCounter = 0;

                var countySet = cwaNameSets[ConstituencyCounter];
                foreach (var cwaName in countySet)
                {
                    var caw = new CAW { Code = cwaCodeSets[ConstituencyCounter][cawCounter], Name = cwaName };

                    constituency.CountyAssemblyWards.Add(caw);

                    cawCounter += 1;
                }

                ConstituencyList.Add(constituency);

                ConstituencyCounter += 1;
            }

            #endregion

            return ConstituencyList.OrderBy(x => x.Name).ToList();
        }

        [NonAction]
        public List<Constituency> GetConstituencyList(string countyCode)
        {
            var counties = GetCountyList();

            if (counties != null)
            {
                var targetCounties = (from b in counties
                                  where b.Code == countyCode
                                  select b).FirstOrDefault();

                if (targetCounties != null)
                    return targetCounties.Constituencies.OrderBy(x => x.Name).ToList();
                else return null;
            }
            else return null;
        }

        [NonAction]
        public string GetCountyName(string countyCode)
        {
            var counties = GetCountyList();

            if (counties != null)
            {
                var targetCounties = (from b in counties
                                      where b.Code == countyCode
                                      select b).FirstOrDefault();

                if (targetCounties != null)
                    return targetCounties.Name;
                else return null;
            }
            else return null;
        }

        [NonAction]
        protected List<SelectListItem> GetConstituencySelectList(string countyCode, string constituencyValue)
        {
            List<SelectListItem> branchesSelectList = new List<SelectListItem> { };

            var defaultBranchItem = new SelectListItem { Selected = (constituencyValue == string.Empty), Text = "Choose Constituency", Value = string.Empty };

            branchesSelectList.Add(defaultBranchItem);

            var otherBranchItems = GetConstituencyList(countyCode);

            if (otherBranchItems != null)
            {
                foreach (var item in otherBranchItems)
                {
                    branchesSelectList.Add(new SelectListItem { Selected = item.Code == constituencyValue, Text = item.Name, Value = (item.Code) });
                }
            }

            return branchesSelectList;
        }

        [NonAction]
        public List<CAW> GetCountyAssemblyWardList(string countyCode, string constituencyCode)
        {
            var constituencies = GetConstituency2List();

            if (constituencies != null)
            {
                var targetConstituencies = (from b in constituencies
                                            where b.Code == constituencyCode
                                            select b).FirstOrDefault();

                if (targetConstituencies != null)
                    return targetConstituencies.CountyAssemblyWards.OrderBy(x => x.Name).ToList();
                else return null;
            }
            else return null;
        }

        [NonAction]
        protected List<SelectListItem> GetCountyAssemblyWardSelectList(string countyCode, string constituencyCode, string cawValue)
        {
            List<SelectListItem> cawsSelectList = new List<SelectListItem> { };

            var defaultCawItem = new SelectListItem { Selected = (cawValue == string.Empty), Text = "Choose Ward", Value = string.Empty };

            cawsSelectList.Add(defaultCawItem);

            var countyAssemblyWards = GetCountyAssemblyWardList(countyCode, constituencyCode);

            if (countyAssemblyWards != null)
            {

                foreach (var item in countyAssemblyWards)
                {
                    cawsSelectList.Add(new SelectListItem { Selected = item.Code == cawValue, Text = item.Name, Value = (item.Code) });
                }

            }

            return cawsSelectList;
        }

        [NonAction]
        protected List<SelectListItem> GetCountysSelectList(string selectedValue)
        {
            List<SelectListItem> banksSelectList = new List<SelectListItem> { };

            var defaultBankItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = "Choose County", Value = string.Empty };

            banksSelectList.Add(defaultBankItem);

            var otherBankItems = GetCountyList();

            if (otherBankItems != null)
            {
                foreach (var item in otherBankItems)
                {
                    banksSelectList.Add(new SelectListItem { Selected = item.Code == selectedValue, Text = item.Name, Value = (item.Code) });
                }
            }
            return banksSelectList;
        }

        public JsonResult GetCascadeCounties()
        {
            var counties = GetCountyList();
            var result = Json(null);
            if (counties != null)
            {
                foreach (var county in counties)
                {
                    result = Json(new County() { Code = county.Code, Constituencies = county.Constituencies, Name = county.Name }, JsonRequestBehavior.AllowGet);
                }
            }

            return result;
        }

        //public JsonResult GetCascadeConstituency(string bankcode)
        //{
        //    var products = GetConstituencyList(bankcode);
        //    var result = Json(null);
        //    if (products != null)
        //    {
        //        foreach (var item in products)
        //        {
        //            result = Json(new Constituency() { Code = item.Code, CountyAssemblyWard = item.CountyAssemblyWard, Name = item.Name });
        //        }
        //    }

        //    return result;
        //}

        public List<Bank> GetBanksList()
        {
            var bankCodes = new List<string>();

            var bankNames = new List<string>();

            var branchCodes = new List<string>();

            var branchNames = new List<string>();

            var branchIndexes = new List<int>();

            var paybillIndexes = new List<int>();

            var paybillNumbers = new List<string>();

            string bankBranchesFileName = string.Format(@"{0}Content\uploads\BanksBranches.xls", AppDomain.CurrentDomain.BaseDirectory);

            using (FileStream file = new FileStream(bankBranchesFileName, FileMode.Open, FileAccess.Read))
            {
                HSSFWorkbook hssfWorkbook = new HSSFWorkbook(file);

                if (hssfWorkbook != null)
                {
                    ISheet sheet = hssfWorkbook.GetSheetAt(0);

                    if (sheet != null)
                    {
                        IEnumerator rows = sheet.GetRowEnumerator();

                        if (rows != null)
                        {
                            rows.MoveNext(); // skip header row

                            int rowCounter = default(int);

                            while (rows.MoveNext())
                            {
                                IRow row = (HSSFRow)rows.Current;

                                ICell cell1Value = row.GetCell(0);  // BankCode
                                ICell cell2Value = row.GetCell(1);  // BranchCode
                                ICell cell3Value = row.GetCell(2);  // BankName
                                ICell cell4Value = row.GetCell(3);  // BranchName
                                ICell cell5Value = row.GetCell(4);  // Status
                                ICell cell6Value = row.GetCell(5);  // Paybill Number

                                object col1Value = cell1Value != null ? cell1Value.ToString() : null;
                                object col2Value = cell2Value != null ? cell2Value.ToString() : null;
                                object col3Value = cell3Value != null ? cell3Value.ToString() : null;
                                object col4Value = cell4Value != null ? cell4Value.ToString() : null;
                                object col5Value = cell5Value != null ? cell5Value.ToString() : null;
                                object col6Value = cell6Value != null ? cell6Value.ToString() : null;

                                if (!bankCodes.Contains(col1Value.ToString().Trim()))
                                {
                                    bankCodes.Add(col1Value.ToString().Trim());

                                    branchIndexes.Add(rowCounter);

                                    paybillIndexes.Add(rowCounter);
                                }

                                if (!bankNames.Contains(col3Value.ToString().Trim()))
                                    bankNames.Add(col3Value.ToString().Trim());

                                branchCodes.Add(col2Value.ToString().Trim());

                                branchNames.Add(col4Value.ToString().Trim());

                                paybillNumbers.Add(col6Value.ToString().Trim());

                                rowCounter += 1;
                            }
                        }
                    }
                }
            }

            #region branches

            var tupleList = new List<Tuple<long, long>>();

            var indexCounter = 0;

            foreach (var item in branchIndexes)
            {
                if (indexCounter + 1 < branchIndexes.Count)
                {
                    var tuple = new Tuple<long, long>(branchIndexes[indexCounter], branchIndexes[indexCounter + 1]);

                    tupleList.Add(tuple);
                }
                else
                {
                    var tuple = new Tuple<long, long>(branchIndexes.Last(), branchCodes.Count);

                    tupleList.Add(tuple);
                }

                indexCounter += 1;
            }

            var branchNameSets = new List<string[]>();

            var branchCodeSets = new List<string[]>();

            foreach (var tuple in tupleList)
            {
                var skip = (int)tuple.Item1;

                var take = (int)tuple.Item2 - (int)tuple.Item1;

                var targetBranchNames = branchNames.Skip(skip).Take(take).ToArray();

                branchNameSets.Add(targetBranchNames);

                var targetBranchCodes = branchCodes.Skip(skip).Take(take).ToArray();

                branchCodeSets.Add(targetBranchCodes);
            }

            #endregion

            #region banks

            var banksList = new List<Bank>();

            var bankCounter = 0;

            foreach (var bankName in bankNames)
            {
                var bank = new Bank { Code = bankCodes[bankCounter], Name = bankName };

                var branchCounter = 0;

                foreach (var branchName in branchNameSets[bankCounter])
                {
                    var branch = new ApplicantBranch { Code = branchCodeSets[bankCounter][branchCounter], BranchName = branchName };

                    bank.Branches.Add(branch);
                    
                    branchCounter += 1;
                }

                banksList.Add(bank);

                bankCounter += 1;
            }
            
            #endregion
            
            return banksList.OrderBy(x => x.Name).ToList();
        }

        [NonAction]
        public string GetBankPaybillNumber(string bankCode)
        {
            var banks = GetBanksList();

            if (banks != null)
            {
                var targetBank = (from b in banks
                                  where b.Code == bankCode
                                  select b).FirstOrDefault();

                if (targetBank != null)
                    return targetBank.PaybillNumber;
                else return null;
            }
            else return null;
        }

        [NonAction]
        public List<ApplicantBranch> GetBranchesList(string bankCode)
        {
            var banks = GetBanksList();

            if (banks != null)
            {
                var targetBank = (from b in banks
                                  where b.Code == bankCode
                                  select b).FirstOrDefault();

                if (targetBank != null)
                    return targetBank.Branches.OrderBy(x => x.BranchName).ToList();
                else return null;
            }
            else return null;
        }
       
        [NonAction]
        protected List<SelectListItem> GetBanksSelectList(string selectedValue)
        {
            List<SelectListItem> banksSelectList = new List<SelectListItem> { };

            var defaultBankItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = "Choose Bank", Value = string.Empty };

            banksSelectList.Add(defaultBankItem);

            var otherBankItems = GetBanksList();

            if (otherBankItems != null)
            {
                foreach (var item in otherBankItems)
                {
                    banksSelectList.Add(new SelectListItem { Selected = item.Code == selectedValue, Text = item.Name, Value = (item.Code) });
                }
            }
            return banksSelectList;
        }

        [NonAction]
        protected List<SelectListItem> GetBranchesSelectList(string bankCode, string selectedValue)
        {
            List<SelectListItem> branchesSelectList = new List<SelectListItem> { };

            var defaultBranchItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = "Choose Branch", Value = string.Empty };

            branchesSelectList.Add(defaultBranchItem);

            var otherBranchItems = GetBranchesList(bankCode);

            if (otherBranchItems != null)
            {
                foreach (var item in otherBranchItems)
                {
                    branchesSelectList.Add(new SelectListItem { Selected = item.Code == selectedValue, Text = item.BranchName, Value = item.Code });
                }
            }

            return branchesSelectList;
        }
        //[NonAction]
        //protected string GetBranchesSelectList(string bankCode)
        //{            
        //    var otherBranchItems = GetBranchesList(bankCode);
            
        //    return otherBranchItems;
        //}

        public List<CheckBoxListInfo> EFTInfo(LoanRequest loanRequest)
        {
            List<CheckBoxListInfo> eftInfo = new List<CheckBoxListInfo>();
            eftInfo.Add(new CheckBoxListInfo(((int)Enumerations.EFTType.AccountToMobile).ToString(), EnumHelper.GetDescription(Enumerations.EFTType.AccountToMobile), loanRequest != null ? loanRequest.Type == Enumerations.EFTType.AccountToMobile : true));
            //eftInfo.Add(new CheckBoxListInfo(((int)Enumerations.EFTType.AccountToAccount).ToString(), EnumHelper.GetDescription(Enumerations.EFTType.AccountToAccount), loanRequest != null ? loanRequest.Type == Enumerations.EFTType.AccountToAccount : false));                        

            return eftInfo;
        }

        public IList<CheckBoxListInfo> BranchType(Branch branch)
        {
            List<CheckBoxListInfo> branchTypeList = new List<CheckBoxListInfo>();
            branchTypeList.Add(new CheckBoxListInfo(((int)Enumerations.BranchType.Branch).ToString(), EnumHelper.GetDescription(Enumerations.BranchType.Branch), branch != null ? branch.Type == (int)Enumerations.BranchType.Branch : true));
            branchTypeList.Add(new CheckBoxListInfo(((int)Enumerations.BranchType.Agency).ToString(), EnumHelper.GetDescription(Enumerations.BranchType.Agency), branch != null ? branch.Type == (int)Enumerations.BranchType.Agency : false));
            return branchTypeList;
        }

        public IList<CheckBoxListInfo> FileUploadCategoryCheckBoxList()
        {
            IList<CheckBoxListInfo> fileUploadCheckBoxList = new List<CheckBoxListInfo>();
            fileUploadCheckBoxList.Add(new CheckBoxListInfo(((int)Enumerations.UploadFileCategory.ProcessingFee).ToString(), EnumHelper.GetDescription(Enumerations.UploadFileCategory.ProcessingFee), false));
            fileUploadCheckBoxList.Add(new CheckBoxListInfo(((int)Enumerations.UploadFileCategory.Reconciliation).ToString(), EnumHelper.GetDescription(Enumerations.UploadFileCategory.Reconciliation), false));
            return fileUploadCheckBoxList;
        }

        public IList<CheckBoxListInfo> InterestChargeType(LoanProduct loanProduct)
        {
            List<CheckBoxListInfo> interestChargeTypeList = new List<CheckBoxListInfo>();
            interestChargeTypeList.Add(new CheckBoxListInfo(((int)Enumerations.InterestChargeType.Fixed).ToString(), EnumHelper.GetDescription(Enumerations.InterestChargeType.Fixed), loanProduct != null ? loanProduct.InterestChargeType == (int)Enumerations.InterestChargeType.Fixed : false));
            interestChargeTypeList.Add(new CheckBoxListInfo(((int)Enumerations.InterestChargeType.Percentage).ToString(), EnumHelper.GetDescription(Enumerations.InterestChargeType.Percentage), loanProduct != null ? loanProduct.InterestChargeType == (int)Enumerations.InterestChargeType.Percentage : true));
            return interestChargeTypeList;
        }

        public IList<CheckBoxListInfo> ChargeRecoveryMethodCheckList(Charge charge)
        {
            List<CheckBoxListInfo> chargeRecoveryCheckList = new List<CheckBoxListInfo>();
            chargeRecoveryCheckList.Add(new CheckBoxListInfo(((int)Enumerations.ChargeRecoveryMethod.UpFront).ToString(), EnumHelper.GetDescription(Enumerations.ChargeRecoveryMethod.UpFront), charge != null ? charge.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.UpFront : false));
            chargeRecoveryCheckList.Add(new CheckBoxListInfo(((int)Enumerations.ChargeRecoveryMethod.PerInstallment).ToString(), EnumHelper.GetDescription(Enumerations.ChargeRecoveryMethod.PerInstallment), charge != null ? charge.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment : false));
            return chargeRecoveryCheckList;
        }

        public List<CheckBoxListInfo> maritalStatusInfoCheckList()
        {
            List<CheckBoxListInfo> maritalStatusInfo = new List<CheckBoxListInfo>();

            maritalStatusInfo.Add(new CheckBoxListInfo(((int)Enumerations.MaritalStatus.Single).ToString(), EnumHelper.GetDescription(Enumerations.MaritalStatus.Single), false));

            maritalStatusInfo.Add(new CheckBoxListInfo(((int)Enumerations.MaritalStatus.Married).ToString(), EnumHelper.GetDescription(Enumerations.MaritalStatus.Married), false));

            maritalStatusInfo.Add(new CheckBoxListInfo(((int)Enumerations.MaritalStatus.Divorced).ToString(), EnumHelper.GetDescription(Enumerations.MaritalStatus.Divorced), false));

            maritalStatusInfo.Add(new CheckBoxListInfo(((int)Enumerations.MaritalStatus.Widowed).ToString(), EnumHelper.GetDescription(Enumerations.MaritalStatus.Widowed), false));

            return maritalStatusInfo;
        }

        public List<CheckBoxListInfo> titleInfoCheckList()
        {
            List<CheckBoxListInfo> titleInfo = new List<CheckBoxListInfo>();

            titleInfo.Add(new CheckBoxListInfo(((int)Enumerations.Title.Mr).ToString(), EnumHelper.GetDescription(Enumerations.Title.Mr), false));

            titleInfo.Add(new CheckBoxListInfo(((int)Enumerations.Title.Mrs).ToString(), EnumHelper.GetDescription(Enumerations.Title.Mrs), false));

            titleInfo.Add(new CheckBoxListInfo(((int)Enumerations.Title.Miss).ToString(), EnumHelper.GetDescription(Enumerations.Title.Miss), false));

            titleInfo.Add(new CheckBoxListInfo(((int)Enumerations.Title.Prof).ToString(), EnumHelper.GetDescription(Enumerations.Title.Prof), false));

            titleInfo.Add(new CheckBoxListInfo(((int)Enumerations.Title.Dr).ToString(), EnumHelper.GetDescription(Enumerations.Title.Dr), false));

            return titleInfo;
        }

        

        public List<CheckBoxListInfo> roleList()
        {
            List<CheckBoxListInfo> roleInfo = new List<CheckBoxListInfo>();

            roleInfo.Add(new CheckBoxListInfo(((int)Enumerations.RoleCategory.Administrator).ToString(), EnumHelper.GetDescription(Enumerations.RoleCategory.Administrator), false));

            roleInfo.Add(new CheckBoxListInfo(((int)Enumerations.RoleCategory.LoanManager).ToString(), EnumHelper.GetDescription(Enumerations.RoleCategory.LoanManager), false));

            roleInfo.Add(new CheckBoxListInfo(((int)Enumerations.RoleCategory.LoanOfficer).ToString(), EnumHelper.GetDescription(Enumerations.RoleCategory.LoanOfficer), false));

            roleInfo.Add(new CheckBoxListInfo(((int)Enumerations.RoleCategory.Customer).ToString(), EnumHelper.GetDescription(Enumerations.RoleCategory.Customer), false));

            roleInfo.Add(new CheckBoxListInfo(((int)Enumerations.RoleCategory.FinanceManager).ToString(), EnumHelper.GetDescription(Enumerations.RoleCategory.FinanceManager), false));

            return roleInfo;
        }

        public List<CheckBoxListInfo> GetUserRoleInfo(string userName, bool filterRoles = true)
        {
            List<CheckBoxListInfo> rolesInfo = new List<CheckBoxListInfo>();

            string[] allRoles = (filterRoles)
                ? (Roles.GetAllRoles().Except(new string[] { "Maker", "Checker" })).ToArray()
                : Roles.GetAllRoles();

            Array.ForEach(allRoles, (roleName) =>
            {
                rolesInfo.Add(new CheckBoxListInfo(roleName, roleName, Roles.IsUserInRole(userName, roleName)));
            });

            return rolesInfo;
        }

        public List<SystemRoles> GetUserRoleInfoList(bool filterRoles = true)
        {
            List<SystemRoles> rolesInfo = new List<SystemRoles>();

            string[] allRoles = (filterRoles)
                ? (Roles.GetAllRoles().Except(new string[] { "Customer" })).ToArray()
                : Roles.GetAllRoles();

            Array.ForEach(allRoles, (roleName) =>
            {
                rolesInfo.Add(new SystemRoles{RoleName = roleName});
            });

            return rolesInfo;
        }

        public List<CheckBoxListInfo> genderInfoCheckList()
        {
            List<CheckBoxListInfo> genderCheckList = new List<CheckBoxListInfo>();
            string[] gender = Enum.GetValues(typeof(Enumerations.Gender)).Cast<Enum>().Select(v => v.ToString()).ToArray();

            Array.ForEach(gender, (genderName) =>
            {
                genderCheckList.Add(new CheckBoxListInfo(genderName, genderName, true));
            });
           
            return genderCheckList;
        }

        [NonAction]
        public List<SelectListItem> IncomeRangeSelectList()
        {
            List<SelectListItem> obj = new List<SelectListItem>();

            obj.Add(new SelectListItem { Text = "Select Income Range", Value = "Select Income Range" });

            obj.Add(new SelectListItem { Text = "999 - 10000", Value = "999 - 10000" });

            obj.Add(new SelectListItem { Text = "10001 - 20000", Value = "10001 - 20000" });

            obj.Add(new SelectListItem { Text = "20001 - 40000", Value = "20001 - 40000" });

            obj.Add(new SelectListItem { Text = "400001 - 80000", Value = "400001 - 80000" });

            obj.Add(new SelectListItem { Text = "80001 and above", Value = "80001 and above" });

            return obj;
        }

        [NonAction]
        protected List<SelectListItem> GetUploadFileCategorySelectList(string selectedValue)
        {
            List<SelectListItem> eftCodesSelectList = new List<SelectListItem> { };

            var defaultRMItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = "Choose Transaction Type", Value = string.Empty };

            eftCodesSelectList.Add(defaultRMItem);

            var tuple = EnumValueDescriptionCache.GetValues(typeof(Enumerations.UploadFileCategory));

            for (int n = 0; n < tuple.Item1.Length; n++)
            {
                var info =
                    new CheckBoxListInfo(
                        tuple.Item1[n].ToString() /*value*/,
                        tuple.Item2[n].ToString()  /*displayText*/,
                        tuple.Item1[n].ToString() == selectedValue  /*isChecked*/);
                
                eftCodesSelectList.Add(new SelectListItem { Selected = info.IsChecked, Text = info.DisplayText, Value = info.Value });
            }

            return eftCodesSelectList;
        }

        /// <summary>
        /// Loan reference number generator
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public string GenerateRandomLoanReferenceNumber(int length)
        {
            string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[length];
            Random rd = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }

    }

    public class Bank
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string PaybillNumber { get; set; }

        HashSet<ApplicantBranch> _branches;

        public virtual ICollection<ApplicantBranch> Branches
        {
            get
            {
                if (_branches == null)
                    _branches = new HashSet<ApplicantBranch>();

                return _branches;
            }
            set
            {
                _branches = new HashSet<ApplicantBranch>(value);
            }
        }
    }

    public class ApplicantBranch
    {
        public string Code { get; set; }

        public string PayBillNumber { get; set; }

        public string BranchName { get; set; }
    }
}
﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class LoanProductsChargeService : ILoanProductsChargeService
    {
        private IRepository<LoanProductsCharge> loanproductschargerepository;

        private string ErrorMessage = string.Empty;

        public LoanProductsChargeService(IRepository<LoanProductsCharge> _loanproductschargerepository)
        {
            if (_loanproductschargerepository == null)
            {
                throw new ArgumentNullException("null repository");
            }
            this.loanproductschargerepository = _loanproductschargerepository;
        }

        public void AddNewLoanProductsCharge(LoanProductsCharge loanproductcharge, ServiceHeader serviceHeader)
        {
            if (loanproductcharge == null)
                throw new ArgumentNullException("entity cannot be null.");

            try
            {
                loanproductschargerepository.Insert(loanproductcharge, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} ErrorMessage{1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public void UpdateLoanProductsCharge(LoanProductsCharge loanproductcharge, ServiceHeader serviceHeader)
        {
            if (loanproductcharge == null)
                throw new ArgumentNullException("entity cannot be null.");

            try
            {
                loanproductschargerepository.Update(loanproductcharge, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} ErrorMessage{1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }
        
        public LoanProductsCharge FindLoanProductsChargeById(Guid Id)
        {
            if (Id == Guid.Empty)
                throw new ArgumentNullException("Guid cannot be null.");

            var findloanproductchargebyid = loanproductschargerepository.GetById(Id);

            return findloanproductchargebyid;
        }

        public IList<LoanProductsCharge> GetAllLoanProductCharges(Guid LoanProductId, IsolationLevel isolationLevel)
        {
            if (LoanProductId == Guid.Empty)
                throw new ArgumentNullException("LoanProductId cannot be null.");

            var findallloanproductcharges = loanproductschargerepository.GetAll(isolationLevel).Where(x => x.LoanProductId == LoanProductId);

            return findallloanproductcharges.ToList();
        }

        public void DeleteLoanProductCharge(LoanProductsCharge loanproductcharge, ServiceHeader serviceHeader)
        {
            if (loanproductcharge == null)
                throw new ArgumentNullException("entity cannot be null.");

            try
            {
                loanproductschargerepository.Delete(loanproductcharge, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} ErrorMessage{1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public IList<LoanProductsCharge> GetRecurringLoanProductChargeByProductId(Guid LoanProductId, IsolationLevel isolationLevel)
        {
            if (LoanProductId == Guid.Empty)
                throw new ArgumentNullException("null id");

            var findRecuringChargesByProduct = loanproductschargerepository.GetAll(isolationLevel).Where(x => x.Charges.IsRecurring == true && x.LoanProductId == LoanProductId);

            return findRecuringChargesByProduct.ToList();
        }

        public LoanProductsCharge FindLoanProductChargeByChargeId(Guid ChargeId)
        {
            var findLoanChargeByChargeId = loanproductschargerepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.ChargeId == ChargeId).FirstOrDefault();
            return findLoanChargeByChargeId;
        }
    }
}

﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    [Serializable]    
    public class LoanProduct : BaseEntity
    {        
        [Display(Name ="Product Name")]
        [Required]
        [MaxLength(100)]
        public string ProductName { get; set; }

        /// <summary>
        /// Interest rate charged
        /// </summary>
        [Display(Name = "Value")]
        [Required]
        [Index("InterestRate")]
        public Decimal InterestValue { get; set; }

        public string LoanProductDescription
        {
            get
            {
                return string.Format("{0} => Range({1} - {2}", ProductName, MinimumAmount, MaximumAmount);
            }
        }
        /// <summary>
        /// Period of payment
        /// </summary>
        [Display(Name = "Term(In Months)")]
        [Required]
        [Index("Term")]
        public int Term { get; set; }        

        /// <summary>
        /// Loan Minimum amount
        /// </summary>
        [Display(Name="Minimum Amount")]
        [Required]
        [Index("MinimumAmount")]
        [DisplayFormat(DataFormatString = "{0:0,0}")]
        public Decimal MinimumAmount { get; set; }

        [Required]
        [Display(Name="Maximum Amount")]
        [Index("MaximumAmount")]
        [DisplayFormat(DataFormatString = "{0:0,0}")]
        public Decimal MaximumAmount { get; set; }

        [Display(Name="Payment Frequency")]
        [Required]
        [Index("PaymentFrequency")]
        public Enumerations.PaymentFrequency PaymentFrequencyType { get; set; }

        [Display(Name="Default Amount")]
        [Required]
        [Index("DefaultMinimumLoanAmount")]
        [DisplayFormat(DataFormatString = "{0:0,0}")]
        public Decimal DefaultLoanAmount { get; set; }

        [Display(Name="Interest GL Account")]
        [Index("InterestGLAccount")]
        public Guid InterestGLAccountId { get; set; }

        [Index("ProductCode", IsUnique = true)]
        [Display(Name = "Product Code")]
        [Column(TypeName = "VARCHAR")]
        [MaxLength(100)]
        public string ProductCode { get; set; }

        [Display(Name = "Loan GL Account")]
        [Index("LoanGLAccount")]
        public Guid LoanGLAccountId { get; set; }

        public virtual ChartofAccount LoanChartAccount { get; set; }

        public virtual ChartofAccount InterestChartAccount { get; set; }

        [Index("IsEnabled")]
        [Index]
        public bool IsEnabled { get; set; }

        [Display(Name="Description")]
        [MaxLength(100)]
        public string Description { get; set; }

        [NotMapped]
        [Display(Name="Number of installments")]
        public int NumberofInstallments
        {
            get
            {
                return (int)PaymentFrequencyType * Term;
            }
        }

        [NotMapped]
        public string Commands { get; set; }

        [NotMapped]
        [Display(Name="Select Charges...")]
        public string Charges { get; set; }

        [Display(Name="Is Default?")]
        public bool IsDefault { get; set; }

        [Display(Name = "Is Micro Loan")]
        public bool IsMicroLoan { get; set; }

        [Display(Name="Interest Charge Type")]
        public int InterestChargeType { get; set; }

        public string InterestChargeTypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.InterestChargeType), InterestChargeType) ? EnumHelper.GetDescription((Enumerations.InterestChargeType)InterestChargeType) : string.Empty;
            }
        }

        public Enumerations.PrincipalRecoveryMethod PrincipalRecoveryMethod { get; set; }

        public string PrincipalRecoveryMethodDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.PrincipalRecoveryMethod), PrincipalRecoveryMethod) ? EnumHelper.GetDescription((Enumerations.PrincipalRecoveryMethod)PrincipalRecoveryMethod) : string.Empty;
            }
        }
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;

namespace Nikopeshe.Services.Interfaces
{
    public interface IUserInfoService
    {
        void AddNewUser(UserInfo userinfo, ServiceHeader serviceHeader);

        IList<UserInfo> GetAllUsers();

        UserInfo FindUserByUserName(string username);

        void UpdateUser(UserInfo userinfo, ServiceHeader serviceHeader);

        UserInfo FindUserById(Guid id);
    }
}

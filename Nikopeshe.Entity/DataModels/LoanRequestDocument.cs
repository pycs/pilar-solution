﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    public class LoanRequestDocument : BaseEntity
    {
        [Index]
        [Display(Name = "Loan Request")]
        public Guid LoanRequestId { get; set; }

        public virtual LoanRequest LoanRequest { get; set; }

        [NotMapped]
        public Guid CustomerId { get; set; }

        [Index]
        [Display(Name = "Document Type")]
        public Enumerations.LoanRequestDocumentTypes DocumentType { get; set; }

        public string DocumentTypeDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.LoanRequestDocumentTypes), DocumentType) ? EnumHelper.GetDescription((Enumerations.LoanRequestDocumentTypes)DocumentType) : string.Empty;
            }
        }

        public byte[] Image { get; set; }
    }
}

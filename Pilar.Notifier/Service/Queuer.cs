﻿using Crosscutting.NetFramework.Logging;
using Pilar.Notifier.Configuration;
using Pilar.Services.Services;
using Quartz;
using Quartz.Impl;
using System;
using System.ComponentModel.Composition;
using System.Configuration;

namespace Pilar.Notifier.Service
{
    [Export(typeof(IPlugin))]
    public class Queuer : IPlugin
    {
        private readonly IScheduler _scheduler;

        public Queuer()
        {
            // Get a reference to the scheduler
            var sf = new StdSchedulerFactory();

            // Get an instance of the Quartz.Net scheduler
            _scheduler = sf.GetScheduler();
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{E2064A6F-25A1-4ABB-9683-C0B00E8EFD45}"); }
        }

        public string Description
        {
            get { return "NOTIFIER QUEUER"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                // Start the scheduler if its in standby
                if (!_scheduler.IsStarted)
                    _scheduler.Start();

                // Define the Job to be scheduled
                var job = JobBuilder.Create<NotifierQueuingJob>()
                    .WithIdentity("NotifierQueuingJob", "Pilar")
                    .RequestRecovery()
                    .Build();

                // Associate a trigger with the Job
                var cronExpression = ConfigurationManager.AppSettings["NotifierQueuingJobCronExpression"];
                var trigger = (ICronTrigger)TriggerBuilder.Create()
                    .WithIdentity("NotifierQueuingJob", "Pilar")
                    .WithCronSchedule(cronExpression)
                    .StartAt(DateTime.UtcNow)
                    .WithPriority(1)
                    .Build();

                // Validate that the job doesn't already exist
                if (_scheduler.CheckExists(new JobKey("NotifierQueuingJob", "Pilar")))
                {
                    _scheduler.DeleteJob(new JobKey("NotifierQueuingJob", "Pilar"));
                }

                var schedule = _scheduler.ScheduleJob(job, trigger);
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        public void Exit()
        {
            _scheduler.Shutdown();
        }

        #endregion
    }
}

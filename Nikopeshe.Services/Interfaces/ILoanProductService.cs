﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface ILoanProductService
    {
        /// <summary>
        /// Add new loan product
        /// </summary>
        /// <param name="loanproduct"></param>
        void AddNewLoanProduct(LoanProduct loanproduct, ServiceHeader serviceHeader);

        /// <summary>
        /// List of all loan products
        /// </summary>
        /// <returns></returns>
        IList<LoanProduct> GetAllLoanProducts(IsolationLevel isolationLevel);

        /// <summary>
        /// Update loan product
        /// </summary>
        /// <param name="loanproduct"></param>
        void UpdateLoanProduct(LoanProduct loanproduct, ServiceHeader serviceHeader);

        /// <summary>
        /// delete loan product
        /// </summary>
        /// <param name="loanproduct"></param>
        void DeleteLoanProduct(LoanProduct loanproduct, ServiceHeader serviceHeader);

        /// <summary>
        /// Find loan product by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        LoanProduct FindLoanProductById(Guid Id);

        /// <summary>
        /// Find minimum loan amount by loan id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Decimal FindMinimumLoanAmountById(Guid LoanProductId);

        /// <summary>
        /// Find maximum loan amount by loan id
        /// </summary>
        /// <param name="LoanProductId"></param>
        /// <returns></returns>
        Decimal FindMaximumLoanAmountById(Guid LoanProductId);

        /// <summary>
        /// Get loan product default amount using loan product Id;
        /// </summary>
        /// <param name="LoanProductId"></param>
        /// <returns></returns>
        Decimal GetDefaultLoanAmountById(Guid LoanProductId);

        /// <summary>
        /// Get loan term and interest rate by Id 
        /// </summary>
        /// <param name="LoanProductId"></param>
        /// <returns></returns>
        Tuple<int, decimal> GetLoanTermandInterestRateByProductId(Guid LoanProductId);

        string NextLoanProductCode();

        int FindDefaultLoanProductCount();

        IList<LoanProduct> FindMicroLoan();

        LoanProduct FindDefaultLoanProduct();

        IList<LoanProduct> FindDefaultLoanProductList();
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.DTO;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.Services.Specifications;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class PartnerService : IPartnerService
    {
        private readonly IRepository<Partner> partnerRepository;

        private string errorMessage = string.Empty;

        public PartnerService(IRepository<Partner> _partnerRepository)
        {
            partnerRepository = _partnerRepository;
        }

        public void AddNewPartner(Partner partner, ServiceHeader serviceHeader)
        {
            if (partner == null)
                throw new ArgumentNullException("partner cannot be null");

            try
            {
                partnerRepository.Insert(partner, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} ErroMessage: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw new Exception(errorMessage, ex);
            }  
        }

        public void UpdatePartner(Partner partner, ServiceHeader serviceHeader)
        {
            if (partner == null)
                throw new ArgumentNullException("partner cannot be null");

            try
            {
                partnerRepository.Update(partner, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} ErroMessage: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw new Exception(errorMessage, ex);
            }
        }

        public PageCollectionInfo<Partner> FindPartnersFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = PartnerSpecification.DefaultSpec(searchString);

            ISpecification<Partner> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var orderLinePagedCollection = partnerRepository.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (orderLinePagedCollection != null)
            {
                var pageCollection = orderLinePagedCollection.Items.ToList();

                var totalRecordCount = orderLinePagedCollection.TotalItems;

                return new PageCollectionInfo<Partner> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }

        public PageCollectionInfo<Partner> FindCustomersByPartnerIdFilterInPage(Guid id, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending)
        {
            var filter = PartnerSpecification.FindPartnerById(id, searchString);

            ISpecification<Partner> spec = filter;

            var sortFields = new List<string> { "CreatedDate" };

            var orderLinePagedCollection = partnerRepository.AllMatchingPaged(spec, startIndex, pageSize, sortFields, sortAscending);

            if (orderLinePagedCollection != null)
            {
                var pageCollection = orderLinePagedCollection.Items.ToList();

                var totalRecordCount = orderLinePagedCollection.TotalItems;

                return new PageCollectionInfo<Partner> { PageCollection = pageCollection, ItemsCount = totalRecordCount };
            }
            else return null;
        }

        public IList<Partner> GetAllPartners(IsolationLevel isolationLevel)
        {
            var findpartners = partnerRepository.GetAll(isolationLevel).ToList();

            return findpartners;
        }
    }
}

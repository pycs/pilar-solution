﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.Charge
{
    public interface IChargeService
    {
        Nikopeshe.Entity.DataModels.Charge FindChargeById(Guid Id);
    }
}

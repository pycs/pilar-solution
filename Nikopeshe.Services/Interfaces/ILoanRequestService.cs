﻿using Nikopeshe.Data;
using Nikopeshe.Entity;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.DTO;
using Nikopeshe.Services.InterfaceImplementations;
using System;
using System.Collections.Generic;
using System.Data;

namespace Nikopeshe.Services.Interfaces
{
    public interface ILoanRequestService
    {
        void AddNewLoanRequest(LoanRequest loanrequest, ServiceHeader serviceHeader);

        void UpdateLoanRequest(LoanRequest loanrequest, ServiceHeader serviceHeader);

        IList<LoanRequest> GetAllLoanRequests();

        LoanRequest GetLoanRequestById(Guid LoanRequestId);

        IList<LoanRequest> GetLoanRequestByCustomerAccountId(Guid CustomerId);

        LoanRequest GetLoanRequestByLoanProductId(Guid LoanProductId);

        IList<LoanRequest> GetAllLoanRequestByStatus(int Status, IsolationLevel isolationLevel);

        IList<LoanRequest> GetAllCustomerLoanRequests(Guid CustomerId);

        int GetCustomerLoanStatus(Guid CustomerId, int status);

        Tuple<Decimal, Decimal, Decimal> GetMaxMinDefaultLoanAmount(Guid loanproductid);

        LoanRequest FindLoanRequestByReferenceNumber(string number);

        IList<LoanRequest> GetAllCustomerActiveLoanRequests(Guid CustomerId);

        int CountLoanRequestsByStatus(int status);

        Tuple<decimal, decimal, int, int>  SumAllActiveLoansAmountTotalInterest(Guid branchId);

        IList<LoanRequest> GetAllCustomerLoanRequestWithPendingApprovedDibursedActiveStates(Guid CustomerId, Guid LoanProductId);

        IList<LoanRequest> GetCustomerPendingLoanRequest(Guid CustomerId);

        IList<CustomerLookUpDetails> CustomerLookUpDetails();

        PageCollectionInfo<LoanRequest> GetCustomerLookupDetailsByFilterInPage(int status, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        IList<LoanRequestList> GetAllLoanRequestsWithBalance();

        Tuple<decimal, decimal> LoanRequestInterestAndCharges(Guid LoanRequestId);

        IList<LoanRequestList> GetAllLoanRequestsWithBalanceByStatus(int Status);

        PageCollectionInfo<LoanRequest> FindLoanRequestFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        PageCollectionInfo<LoanRequest> FindLoanRequestByBranchFilterInPage(Guid branchId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        PageCollectionInfo<LoanRequest> FindLoanRequestByGroupFilterInPage(Guid groupId, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        PageCollectionInfo<LoanRequest> FindDisbursementListFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        PageCollectionInfo<LoanRequest> FindPendingDisbursementListFilterInPage(int status,int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);
    }
}

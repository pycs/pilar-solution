﻿using Nikopeshe.Data;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using System;
using System.Net.Http;
using System.ServiceModel;
using System.Web.Http;

namespace Nikopeshe.WebApplication.Controllers.APICotrollers
{
    public class LoanRequestController : ApiController
    {
        private readonly ILoanRequestService loanrequestService;
        private readonly ICustomerService customerservice;
        private readonly IEmailAlertService emailalerservice;
        private readonly ITextAlertService textalertservice;
        private readonly ISystemGeneralLedgerAccountMappingService systemglmappingservice;
        private readonly ISavingsProductService savingsproductservice;
        private readonly ICustomerAccountService customeraccountservice;
        private readonly ILoanProductService loanproductservice;
        private readonly ILoanTransactionHistoryService loantransactionhistoryservice;
        private readonly IChartofAccountService chartofaccountservice;
        private readonly ILoanProductsChargeService loanproductcharges;
        private readonly IGraduatedScaleService graduatedscaleservice;
        private readonly ILoanRequestChargeService loanrequestchargeservice;
        private readonly IStaticSettingService staticsettings;
        private readonly ILoanRequestDocumentsService loanrequestdocumentservice;
        private readonly ICustomerDocumentService customerdocumentservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public LoanRequestController(ILoanRequestService _loanrequestservice, ICustomerService _customerservice,
            ILoanProductService _loanproductservice, ISystemGeneralLedgerAccountMappingService _systemglmappingservice,
            ISavingsProductService _savingsproductservice, IChartofAccountService _chartofaccountservice,
            ICustomerAccountService _customeraccountservice, IEmailAlertService _emailalerservice,
            ITextAlertService _textalertservice, ILoanTransactionHistoryService _loantransactionhistoryservice,
            ILoanProductsChargeService _loanproductcharges, IGraduatedScaleService _graduatedscaleservice,
            ILoanRequestChargeService _loanrequestchargeservice, IStaticSettingService _staticsettings,
            ILoanRequestDocumentsService _loanrequestdocumentservice, ICustomerDocumentService _customerdocumentservice)
        {
            if(_loanproductservice == null || _emailalerservice == null || _systemglmappingservice == null
                || _savingsproductservice == null || _chartofaccountservice == null || _textalertservice == null
                || _customeraccountservice == null || _loanrequestservice == null || _customerservice == null 
                || _loantransactionhistoryservice == null || _loanproductcharges == null || _graduatedscaleservice == null
                || _loanrequestchargeservice == null || _customerdocumentservice == null)
                throw new ArgumentNullException("null service reference");

            this.loanproductservice = _loanproductservice;
            this.emailalerservice = _emailalerservice;
            this.systemglmappingservice = _systemglmappingservice;
            this.savingsproductservice = _savingsproductservice;
            this.chartofaccountservice = _chartofaccountservice;
            this.textalertservice = _textalertservice;
            this.customeraccountservice = _customeraccountservice;
            this.loanrequestService = _loanrequestservice;
            this.customerservice = _customerservice;
            this.loantransactionhistoryservice = _loantransactionhistoryservice;
            this.loanproductcharges = _loanproductcharges;
            this.graduatedscaleservice = _graduatedscaleservice;
            this.loanrequestchargeservice = _loanrequestchargeservice;
            this.staticsettings = _staticsettings;
            this.loanrequestdocumentservice = _loanrequestdocumentservice;
            this.customerdocumentservice = _customerdocumentservice;
        }

         // POST api/loanrequest
        /// <summary>
        /// This method is used for loan request.
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Http Response Message</returns>
        public HttpResponseMessage Post()
        {
            return null;
        }
    }
}
﻿using Nikopeshe.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.Utility
{
    public class Utility : IUtility
    {
        public void ClearLoans()
        {
            DataContext dataContext = new DataContext();
            dataContext.Database.CommandTimeout = 1800;
            try
            {               
                var results = dataContext.Database.ExecuteSqlCommand("spPilarClearLoans");
            }

            catch (Exception ex)
            {
                //LoggerFactory.CreateLog().LogInfo("{0}->Arrears Calculator..." + ex);
            }

            finally
            {
                dataContext.Database.Connection.Close();
            }
        }
    }
}

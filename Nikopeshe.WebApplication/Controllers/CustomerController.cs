﻿using Crosscutting.NetFramework.Logging;
using Infrastructure.Crosscutting.NetFramework;
using Infrastructure.Crosscutting.NetFramework.Utils;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using MvcContrib.Pagination;
using Nikopeshe.Data;
using Nikopeshe.Data.Mappings;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.DTO;
using Nikopeshe.Services.InterfaceImplementations;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using Nikopeshe.WebApplication.Models;
using Nikopeshe.WebApplication.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class CustomerController : BaseController
    {
        internal readonly ICustomerService customerservice;
        internal readonly ILoanRequestService loanrequestservice;
        internal readonly ILoanTransactionHistoryService loantransactionhistory;
        internal readonly ISystemGeneralLedgerAccountMappingService glMappingService;
        internal readonly ISavingsProductService savingProductService;
        internal readonly DomainAuditTrailService domainaudittrail;
        internal readonly IEmailAlertService emailalerservice;
        internal readonly IUserInfoService userInfoService;
        internal readonly ICustomerAccountService customeraccountservice;
        internal readonly IBranchService branchservice;
        internal readonly IStaticSettingService staticSettingService;
        internal readonly IGroupService groupService;
        internal readonly IRelationshipManagerService relationshipManagerService;
        internal readonly ICustomerGroupRelationshipService customerGroupRelationshipService;
        private readonly IPartnerService partnerService;
        private readonly IProfessionService professionService;
        public static Guid relationshipManagerGuid = Guid.Empty;
        internal ITextAlertService textalertservice;
        private string ClientCode = ConfigurationManager.AppSettings["ClientCode"];
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        private string TextSignature = ConfigurationManager.AppSettings["TextSignature"].ToString();
        private string ApplicationName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        public CustomerController(ICustomerService _customerservice, ILoanTransactionHistoryService _loantransactionhistory,
            ILoanRequestService _loanrequestservice, DomainAuditTrailService _domainaudittrail, IEmailAlertService _emailalerservice,
            ICustomerAccountService _customeraccountservice, IBranchService _branchservice, ITextAlertService _textalertservice,
            IStaticSettingService _staticSettingService, ISystemGeneralLedgerAccountMappingService _glMappingService,
            ISavingsProductService _savingProductService, IUserInfoService _userInfoService, IRelationshipManagerService _relationshipManagerService,
            IGroupService _groupService, ICustomerGroupRelationshipService _customerGroupRelationshipService, IProfessionService _professionService,
            IPartnerService _partnerService)
        {
            if (_customeraccountservice == null || _loanrequestservice == null || _domainaudittrail == null || _customerservice == null
                || _emailalerservice == null || _loantransactionhistory == null || _branchservice == null || _textalertservice == null)
                throw new ArgumentNullException("null service reference");

            this.customeraccountservice = _customeraccountservice;
            this.loanrequestservice = _loanrequestservice;
            this.domainaudittrail = _domainaudittrail;
            this.customerservice = _customerservice;
            this.emailalerservice = _emailalerservice;
            this.loantransactionhistory = _loantransactionhistory;
            this.branchservice = _branchservice;
            this.userInfoService = _userInfoService;
            this.textalertservice = _textalertservice;
            this.staticSettingService = _staticSettingService;
            this.glMappingService = _glMappingService;
            this.savingProductService = _savingProductService;
            this.relationshipManagerService = _relationshipManagerService;
            this.groupService = _groupService;
            this.customerGroupRelationshipService = _customerGroupRelationshipService;
            this.professionService = _professionService;
            this.partnerService = _partnerService;
        }

        /// <summary>
        /// Get all customers
        /// </summary>
        /// <returns></returns>
        /// 
        [Authorize(Roles = "Administrator, LoanOfficer, DataManager")]
        public ActionResult Index()
        {
            return View();
        }


        //[AcceptAjax]
        [HttpPost]
        public JsonResult GetCustomerByBranchFilterInPage(JQueryDataTablesModel jQueryDataTablesModel)
        {
            var findCurrentUserId = User.Identity.Name;

            var sortAscending = jQueryDataTablesModel.sSortDir_.First() == "asc" ? true : false;

            var sortedColumns = (from s in jQueryDataTablesModel.GetSortedColumns() select s.PropertyName).ToList();           

            var pageCollectionInfo = new PageCollectionInfo<Customer>();

            if (findCurrentUserId != null)
            {
                var findCurrentUserInfo = userInfoService.FindUserByUserName(findCurrentUserId);
                if (findCurrentUserInfo != null)
                {
                    if (findCurrentUserInfo.Branch.BranchCode == "000")
                    {
                        pageCollectionInfo = customerservice.FindCustomersFilterInPage(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, sortAscending);
                    }
                    else
                    {
                        pageCollectionInfo = customerservice.FindCustomersByBranchIdFilterInPage(findCurrentUserInfo.BranchId, jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, sortAscending);
                    }
                }
                else
                {
                    pageCollectionInfo = customerservice.FindCustomersFilterInPage(jQueryDataTablesModel.iDisplayStart, jQueryDataTablesModel.iDisplayLength, sortedColumns, jQueryDataTablesModel.sSearch, sortAscending);
                }
            }

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            
            if (pageCollectionInfo != null && pageCollectionInfo.PageCollection.Any())
            {
                totalRecordCount = pageCollectionInfo.ItemsCount;

                searchRecordCount = !string.IsNullOrWhiteSpace(jQueryDataTablesModel.sSearch) ? pageCollectionInfo.PageCollection.Count : totalRecordCount;
            }

            return this.DataTablesJson(items: pageCollectionInfo.PageCollection, totalRecords: totalRecordCount, totalDisplayRecords: searchRecordCount, sEcho: jQueryDataTablesModel.sEcho);
        }

        public JsonResult getallcustomers(JQueryDataTablesModel datatablemodel)
        {
            var findCurrentUserId = User.Identity.Name;
            var pagecollection = new List<Customer>();
            if (findCurrentUserId != null)
            {
                var findCurrentUserInfo = userInfoService.FindUserByUserName(findCurrentUserId);
                if (findCurrentUserInfo != null)
                {
                    if (findCurrentUserInfo.Branch.BranchCode == "000")
                    {
                        pagecollection = customerservice.GetAllCustomers(IsolationLevel.ReadUncommitted).Where(x => x.CustomerType == Enumerations.CustomerTypes.Individual).ToList();
                    }
                    else
                    {
                        pagecollection = customerservice.GetCustomersByBranchId(findCurrentUserInfo.BranchId, IsolationLevel.ReadUncommitted).Where(x => x.CustomerType == Enumerations.CustomerTypes.Individual).ToList();
                    }
                }
                else
                {
                    pagecollection = customerservice.GetAllCustomers(IsolationLevel.ReadUncommitted).Where(x => x.CustomerType ==  Enumerations.CustomerTypes.Individual).ToList();
                }
            }

            int totalrecordcount = 0;

            int searchrecordcount = 0;

            var transactionList = GetAllCustomer(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalrecordcount, searchRecordCount: out searchrecordcount, searchString: datatablemodel.sSearch, CustomerList: pagecollection);


            return Json(new JQueryDataTablesResponse<Customer>(items: transactionList,
                totalRecords: totalrecordcount,
                totalDisplayRecords: searchrecordcount,
                sEcho: datatablemodel.sEcho));

        }

        public static IList<Customer> GetAllCustomer(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<Customer> CustomerList)
        {
            var GetCustomersList = CustomerList;

            totalRecordCount = GetCustomersList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomersList = GetCustomersList.Where(c => c.FullName
                    .ToLower().Contains(searchString.ToLower())
                    || c.IDNumber.ToLower().Contains(searchString.ToLower()) || c.MobileNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetCustomersList.Count;

            IOrderedEnumerable<Customer> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "FullName":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.FullName);
                        break;

                    case "MobileNumber":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.MobileNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.MobileNumber);
                        break;

                    case "IDNumber":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.IDNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IDNumber);
                        break;

                    case "Branch.BranchName":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName);
                        break;

                    case "IncomeRange":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.IncomeRange)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IncomeRange);
                        break;

                    case "IsEnabled":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        //
        // GET: /Loanrequest/       
        [Authorize]
        public ActionResult loanrequestlist()
        {
            return View();
        }

        public JsonResult getallloanrequestspercustomer(JQueryDataTablesModel datatablemodel)
        {
            var username = User.Identity.Name;
            var allloanrequestpagecollection = loanrequestservice.GetAllLoanRequests();
            var getuserid = customerservice.FindCustomerByMobileNumber(username);

            var pagecollection = loanrequestservice.GetAllCustomerActiveLoanRequests(getuserid.Id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetCustomerLoanRequest(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, CustomerLoanRequestList: pagecollection);


            return Json(new JQueryDataTablesResponse<LoanRequest>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));

        }

        public static IList<LoanRequest> GetCustomerLoanRequest(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<LoanRequest> CustomerLoanRequestList)
        {
            var GetLoanRequestList = CustomerLoanRequestList;

            totalRecordCount = GetLoanRequestList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetLoanRequestList = GetLoanRequestList.Where(c => c.LoanProduct.ProductName
                    .ToLower().Contains(searchString.ToLower())
                    || c.CustomerAccount.Customer.FullName.ToLower().Contains(searchString.ToLower()) || c.LoanReferenceNumber.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetLoanRequestList.Count;

            IOrderedEnumerable<LoanRequest> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "CustomerAccount.Customer.FullName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CustomerAccount.Customer.FullName);
                        break;

                    case "LoanProduct.ProductName":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanProduct.ProductName);
                        break;

                    case "LoanReferenceNumber":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanReferenceNumber);
                        break;

                    case "LoanAmount":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LoanAmount);
                        break;

                    case "InterestAmount":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestAmount);
                        break;

                    case "StatusDesc":
                        sortedClients = sortedClients == null ? GetLoanRequestList.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.StatusDesc);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult CustomerRegistration()
        {
            ViewBag.IncomeRange = IncomeRangeSelectList();
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();
            ViewBag.GroupSelectList = GetGroups(Guid.Empty.ToString());
            ViewBag.GenderCheckList = genderInfoCheckList();
            var currentUser = User.Identity.Name;
            var findCurrentUserDetails = userInfoService.FindUserByUserName(currentUser);
            Guid branchId = Guid.Empty;
            if (findCurrentUserDetails != null)
            {
                if (findCurrentUserDetails.Branch.BranchCode == "000")
                {
                    branchId = Guid.Empty;
                }
                else
                {
                    branchId = findCurrentUserDetails.BranchId;
                }
            }
            else
            {
                branchId = Guid.Empty;
            }

            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());

            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(Guid.Empty.ToString(), branchId);

            var customer = new Customer
            {
                DateofBirth = DateTime.Now,
                BelongsToAGroup = true
            };
            return View(customer);
        }

        [HttpPost]
        public ActionResult CustomerRegistration(string DateofBirth, [Bind(Exclude = "DateofBirth")]Customer customer)
        {
            ViewBag.IncomeRange = IncomeRangeSelectList();
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            ViewBag.GroupSelectList = GetGroups(Guid.Empty.ToString());
            string mobilenumber = customer.MobileNumber;
            var currentUser = User.Identity.Name;
            Guid branchId = Guid.Empty;
            var findCurrentUserDetails = userInfoService.FindUserByUserName(currentUser);

            if (findCurrentUserDetails != null)
            {
                if (findCurrentUserDetails.Branch.BranchCode == "000")
                {
                    branchId = Guid.Empty;
                }
                else
                {
                    branchId = findCurrentUserDetails.BranchId;
                }
            }
            else
            {
                branchId = Guid.Empty;
            }
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(Guid.Empty.ToString(), branchId);
            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();
            ViewBag.GenderCheckList = genderInfoCheckList();
            if (string.IsNullOrWhiteSpace(DateofBirth))
            {
                ModelState.AddModelError(string.Empty, "Date of Birth is required.");
                return View();
            }
            if (customer.BranchId == Guid.Empty)
            {
                ModelState.AddModelError(string.Empty, "Branch cannot be null.");
                return View(customer);
            }

            if (string.IsNullOrWhiteSpace(string.Concat(customer.CustomerType)))
            {
                ModelState.AddModelError(string.Empty, "Customer Type cannot be null.");
                return View(customer);
            }

            if (customer.BelongsToAGroup)
            {
                if (customer.GroupId == Guid.Empty)
                {
                    ModelState.AddModelError(string.Empty, "If this customer belongs to a Group, Group Name cannot be null");
                    return View(customer);
                }
            }
            DateTime dDate;

            if (DateTime.TryParseExact(DateofBirth, "dd/MM/yyyy", null, DateTimeStyles.None, out dDate) == true)
            {
                customer.DateofBirth = DateTime.ParseExact(DateofBirth, "dd/MM/yyyy", null);
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid Date of Birth format.");
                return View(customer);
            }
            customer.ApprovalCode = GenerateRandomCustomerApprovalCode(6);
            bool checkduplicatecustomer;
            bool iscustomerunderage;
            string nextcustomernumber = customerservice.NextCustomerNumber();
            customer.CustomerNumber = nextcustomernumber;
            string nextcustomeraccountnumber = customerservice.NextCustomerSavingAccountNumber(nextcustomernumber);
            CustomersAccount customersaccount = new CustomersAccount();
            if (ModelState.IsValid)
            {
                customer.IsApproved = true;
                customer.IsEnabled = true;
                customer.FirstName = customer.FirstName.ToUpper();
                customer.MiddleName = customer.MiddleName.ToUpper();
                customer.LastName = customer.LastName.ToUpper();
                customer.CustomerType = Enumerations.CustomerTypes.Individual;
                customer.RegisteredBy = HttpContext.User.Identity.Name;
                customer.CustomerRegistrationType = Enumerations.CustomerRegistrationType.WalkIn;
                string password = Membership.GeneratePassword(8, 2);
                customersaccount.CustomerId = customer.Id;
                customersaccount.AccountType = (int)Enumerations.ProductCode.Savings;
                customersaccount.AccountNumber = nextcustomeraccountnumber;
                customersaccount.IsActive = true;
                checkduplicatecustomer = customerservice.CheckDuplicateCustomer(customer.MobileNumber, customer.IDNumber);
                iscustomerunderage = customerservice.ValidateCustomerAge(customer.DateofBirth);
                try
                {
                    if (checkduplicatecustomer)
                    {
                        ModelState.AddModelError(string.Empty, string.Format("Customer with Mobile Number {0} or Id Number {1} already exists", customer.MobileNumber, customer.IDNumber));

                        return View(customer);
                    }

                    if (!iscustomerunderage)
                    {
                        ModelState.AddModelError(string.Empty, string.Format("You must be 18 years and above."));

                        return View(customer);
                    }

                    else if (customer.IncomeRange.Equals("Select Income Range"))
                    {
                        ModelState.AddModelError(string.Empty, string.Format("Income range is required."));

                        return View(customer);
                    }
                    var findmembershipuser = Membership.GetUser(customer.MobileNumber);
                    if (checkduplicatecustomer == false && findmembershipuser == null)
                    {
                        var findStaticSetting = staticSettingService.FindStaticSettingByKey("CREATEMEMBERSHIPACCOUNT");

                        if (findStaticSetting.Value == "1")
                        {
                            MembershipCreateStatus status;
                            MembershipUser createuser = Membership.CreateUser(customer.MobileNumber, password, customer.EmailAddress, "Where were you when you first heard about 9/11?", "School", true, out status);
                            Roles.AddUserToRole(customer.MobileNumber, "Customer");
                            string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                            string loginurl = "<a href='" + mvcclientendposint + "'/Account/Login> click here</a>";

                            if (status == MembershipCreateStatus.Success)
                            {
                                //Create email alert and insert in the database
                                StringBuilder bodyBuilder = new StringBuilder();
                                bodyBuilder.Append("Dear Sir/Madam,");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.Append(string.Format("Welcome to {0}. Below you will find your login details.", DefaultSettings.Instance.ApplicationDisplayName));
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.Append("Username: " + customer.MobileNumber);
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.Append("Password: " + password);
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.Append(string.Format("Continue to approve your account and borrow {0}.", loginurl));
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");

                                var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
                                bodyBuilder.Append(emailSignature);

                                var emailAlertDTO = new EmailAlert
                                {
                                    MailMessageFrom = DefaultSettings.Instance.Email,
                                    MailMessageTo = string.Format("{0}", customer.EmailAddress),
                                    MailMessageSubject = string.Format("Login Details - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                    MailMessageBody = string.Format("{0}", bodyBuilder),
                                    MailMessageIsBodyHtml = true,
                                    MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                    MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                    MailMessageSecurityCritical = false,
                                    CreatedBy = "System",
                                    MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                };

                                emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                                NewTextAlert(customer.FullName, customer.MobileNumber, customer.MobileNumber, password, customer.ApprovalCode);
                                TempData["Message"] = "Account created successfully. An email/text message has been sent to you with your approval code. Provide the code after login to approve your account.";
                            }

                            switch (status)
                            {
                                case MembershipCreateStatus.Success:
                                    ModelState.AddModelError(string.Empty, "User Account created successfully");

                                    break;
                                case MembershipCreateStatus.DuplicateUserName:
                                    ModelState.AddModelError(string.Empty, "There already exists a user with this username.");
                                    break;

                                case MembershipCreateStatus.DuplicateEmail:
                                    ModelState.AddModelError(string.Empty, "There already exists a user with this email address.");
                                    break;
                                case MembershipCreateStatus.InvalidEmail:
                                    ModelState.AddModelError(string.Empty, "The email address you provided in invalid.");
                                    break;
                                case MembershipCreateStatus.InvalidAnswer:
                                    ModelState.AddModelError(string.Empty, "The security answer is invalid.");
                                    break;
                                case MembershipCreateStatus.InvalidPassword:
                                    ModelState.AddModelError(string.Empty, "The password you provided is invalid. It must be seven characters long and have at least one non-alphanumeric character.");

                                    break;
                                default:
                                    ModelState.AddModelError(string.Empty, status.ToString());
                                    break;
                            }
                        }

                        var findStaticSettingByKey = staticSettingService.FindStaticSettingByKey("CUSTOMERJOININGFEE");
                        //Post Joining fee to Income GL
                        if (customer.JoiningFee == true)
                        {
                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                            var findCustomerJoiningFeeGL = glMappingService.FindSystemGeneralLedgerAccountMappingsByGLCode((int)Enumerations.SystemGeneralLedgerAccountCode.CustomerJoiningFee);
                            var findDefaultSavingProduct = savingProductService.FindDefaultSavingsProduct();
                            #region Dr Saving GL, Cr Joining Fee GL
                            LoanTransactionHistory drSavingGL = new LoanTransactionHistory()
                            {
                                TransactionType = Enumerations.TransactionType.Debit,
                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                TransactionAmount = decimal.Parse(findStaticSettingByKey.Value),
                                TotalTransactionAmount = decimal.Parse(findStaticSettingByKey.Value),
                                ChartofAccountId = findDefaultSavingProduct.ChartofAccountId,
                                ContraAccountId = findCustomerJoiningFeeGL.ChartofAccountId,
                                CustomersAccountId = customersaccount.Id,
                                Description = "Customer Joining Fee",
                                CreatedBy = User.Identity.Name,
                                IsApproved = true,
                                ApprovedBy = string.Empty,
                                TransactionCategory = (int)Enumerations.TransactionCategory.CustomerJoiningFee,
                                BranchId = customer.BranchId,
                                TransactionBatchNumber = batchNo
                            };

                            #endregion

                            #region Cr Saving GL, Dr Joining Fee GL
                            LoanTransactionHistory crJoiningFeeGL = new LoanTransactionHistory()
                            {
                                TransactionType = Enumerations.TransactionType.Credit,
                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                TransactionAmount = decimal.Parse(findStaticSettingByKey.Value),
                                TotalTransactionAmount = decimal.Parse(findStaticSettingByKey.Value),
                                ChartofAccountId = findCustomerJoiningFeeGL.ChartofAccountId,
                                ContraAccountId = findDefaultSavingProduct.ChartofAccountId,
                                CustomersAccountId = customersaccount.Id,
                                Description = "Customer Joining Fee",
                                CreatedBy = User.Identity.Name,
                                IsApproved = true,
                                ApprovedBy = string.Empty,
                                TransactionCategory = (int)Enumerations.TransactionCategory.CustomerJoiningFee,
                                BranchId = customer.BranchId,
                                TransactionBatchNumber = batchNo
                            };
                            using (var dataContext = new DataContext())
                            {
                                using (var transaction = dataContext.Database.BeginTransaction())
                                {
                                    try
                                    {
                                        if (customer.BelongsToAGroup)
                                        {
                                            var customerGroupRelationship = new CustomerGroupRelationship
                                            {
                                                Id = Guid.NewGuid(),
                                                CreatedDate = DateTime.Now,
                                                CustomerId = customer.Id,
                                                GroupId = customer.GroupId,
                                                GroupRole = (int)customer.GroupRole
                                            };

                                            customerGroupRelationshipService.AddNewCustomerGroupRelationship(customerGroupRelationship, serviceHeader);
                                        }
                                        customerservice.AddNewCustomer(customer, serviceHeader);
                                        customeraccountservice.AddNewCustomersAccount(customersaccount, serviceHeader);
                                        loantransactionhistory.AddNewLoanHistory(drSavingGL, serviceHeader);
                                        loantransactionhistory.AddNewLoanHistory(crJoiningFeeGL, serviceHeader);
                                        transaction.Commit();
                                    }

                                    catch (Exception ex)
                                    {
                                        transaction.Rollback();
                                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                    }
                                }
                            }

                            #endregion
                        }

                        else
                        {
                            customerservice.AddNewCustomer(customer, serviceHeader);
                            customeraccountservice.AddNewCustomersAccount(customersaccount, serviceHeader);
                            if (customer.BelongsToAGroup)
                            {
                                var customerGroupRelationship = new CustomerGroupRelationship
                                {
                                    Id = Guid.NewGuid(),
                                    CreatedDate = DateTime.Now,
                                    CustomerId = customer.Id,
                                    GroupId = customer.GroupId,
                                    GroupRole = (int)customer.GroupRole
                                };

                                customerGroupRelationshipService.AddNewCustomerGroupRelationship(customerGroupRelationship, serviceHeader);
                            }
                        }
                    }

                    else
                    {
                        ModelState.AddModelError(string.Empty, "An error has occurred while processing. Make sure the mobile number is unique.");
                        return View(customer);
                    }

                    return RedirectToAction("Successful", "Customer");
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(customer);
                }
            }
            return View(customer);
        }

        private void NewTextAlert(string RecipientName, string RecepientNumber, string Username, string Password, string AuthorizationCode)
        {
            //Create email alert and insert in the database
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append(string.Format("Dear {0} ", RecipientName));
            bodyBuilder.Append(string.Format("Welcome to {0}. Your Username - {1}, Password - {2} ", ApplicationName, Username, Password));
            bodyBuilder.Append(TextSignature);
            bodyBuilder.Append("Thank you.");
            var textalert = new TextAlert
            {
                TextMessageRecipient = string.Format("{0}", RecepientNumber),
                TextMessageBody = string.Format("{0}", bodyBuilder),
                MaskedTextMessageBody = "",
                TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                TextMessageSecurityCritical = false,
                CreatedBy = User.Identity.Name,
                TextMessageSendRetry = 3
            };

            textalertservice.AddNewTextAlert(textalert, serviceHeader);
        }

        /// <summary>
        /// User account creation success page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Successful()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult editcustomer(Guid Id)
        {
            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();
            ViewBag.GenderCheckList = genderInfoCheckList();
            ViewBag.IncomeRange = IncomeRangeSelectList();
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            var findcustomer = customerservice.FindCustomerById(Id);
            var currentUser = User.Identity.Name;
            var findCurrentUserDetials = userInfoService.FindUserByUserName(currentUser);
            Guid branchId = Guid.Empty;
            if (findCurrentUserDetials != null)
            {
                if (findCurrentUserDetials.Branch.BranchCode == "000")
                {
                    branchId = Guid.Empty;
                }
                else
                {
                    branchId = findCurrentUserDetials.BranchId;
                }
            }
            else
            {
                branchId = Guid.Empty;
            }
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(findcustomer.RelationshipManagerId.ToString(), branchId);
            var mobilenumber = findcustomer.MobileNumber;
            var idnumber = findcustomer.IDNumber;
            ViewBag.Mobilenumber = mobilenumber;
            ViewBag.IdNumber = idnumber;
            return View(findcustomer);
        }

        [Authorize]
        public ActionResult editcustomer(Customer customer)
        {
            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();
            ViewBag.GenderCheckList = genderInfoCheckList();
            ViewBag.IncomeRange = IncomeRangeSelectList();
            var currentUser = User.Identity.Name;
            var findCurrentUserDetials = userInfoService.FindUserByUserName(currentUser);
            var findcustomer = customerservice.FindCustomerById(customer.Id);
            Guid branchId = Guid.Empty;
            if (findCurrentUserDetials != null)
            {
                if (findCurrentUserDetials.Branch.BranchCode == "000")
                {
                    branchId = Guid.Empty;
                }
                else
                {
                    branchId = findCurrentUserDetials.BranchId;
                }
            }
            else
            {
                branchId = Guid.Empty;
            }
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(findcustomer.RelationshipManagerId.ToString(), branchId);
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            if (ModelState.IsValid)
            {
                var mobilenumber = findcustomer.MobileNumber;
                var idnumber = findcustomer.IDNumber;
                findcustomer.FirstName = customer.FirstName.ToUpper();
                findcustomer.LastName = customer.LastName.ToUpper();
                findcustomer.EmailAddress = customer.EmailAddress;
                findcustomer.Occupation = customer.Occupation;
                findcustomer.Gender = customer.Gender;
                findcustomer.BranchId = customer.BranchId;
                findcustomer.MaritalStatus = customer.MaritalStatus;
                findcustomer.IncomeRange = customer.IncomeRange;
                findcustomer.MiddleName = customer.MiddleName.ToUpper();
                findcustomer.MobileNumber = customer.MobileNumber;
                findcustomer.IDNumber = customer.IDNumber;
                findcustomer.RelationshipManagerId = customer.RelationshipManagerId;
                findcustomer.KRAPIN = customer.KRAPIN;
                ViewBag.Mobilenumber = mobilenumber;
                ViewBag.IdNumber = idnumber;
                if (customer.BranchId == Guid.Empty)
                {
                    ModelState.AddModelError(string.Empty, "Branch cannot be null.");
                    return View();
                }

                bool checkduplicatecustomer = customerservice.CheckDuplicateCustomer(customer.MobileNumber, customer.IDNumber);
                if (customer.BranchId == Guid.Empty)
                {
                    ModelState.AddModelError(string.Empty, "Branch cannot be null.");
                    return View(customer);
                }

                if (string.IsNullOrWhiteSpace(string.Concat(customer.CustomerType)))
                {
                    ModelState.AddModelError(string.Empty, "Customer Type cannot be null.");
                    return View(customer);
                }

                bool iscustomerunderage = customerservice.ValidateCustomerAge(customer.DateofBirth);
                if (!iscustomerunderage)
                {
                    ModelState.AddModelError(string.Empty, string.Format("You must be 18 years and above."));

                    return View(customer);
                }

                else if (customer.IncomeRange.Equals("Select Income Range"))
                {
                    ModelState.AddModelError(string.Empty, string.Format("Income range is required."));

                    return View(customer);
                }

                try
                {
                    customerservice.UpdateCustomer(findcustomer, serviceHeader);
                    if (User.IsInRole("Customer"))
                    {
                        return RedirectToAction("CustomerProfile");
                    }

                    else
                    {
                        return RedirectToAction("Index");
                    }
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                }
            }
            return View(customer);
        }

        [Authorize]
        public ActionResult customerdetails(Guid Id)
        {
            var findcutomerbyid = customerservice.FindCustomerById(Id);

            return View(findcutomerbyid);
        }

        public ActionResult CustomerProfile()
        {
            var customerdetials = customerservice.FindCustomerByMobileNumber(User.Identity.Name);

            return View(customerdetials);
        }
        [Authorize]
        public ActionResult customer()
        {
            return View();
        }

        public JsonResult getcustomerbyid(JQueryDataTablesModel datatablemodel)
        {
            int totalrecordcount = 0;

            int searchedrecordcount = 0;

            var sortAscending = datatablemodel.sSortDir_.First() == "asc" ? true : false;

            var sortedcolumns = (from s in datatablemodel.GetSortedColumns() select s.PropertyName).ToList();

            var pagecollection = customerservice.GetCustomerByMobileNumber(User.Identity.Name);

            if (pagecollection != null)
            {
                totalrecordcount = pagecollection.Count();

                searchedrecordcount = !string.IsNullOrWhiteSpace(datatablemodel.sSearch) ? pagecollection.Count() : totalrecordcount;
            }

            var result = Extensions.ControllerExtensions.DataTablesJson(this, items: pagecollection, totalRecords: totalrecordcount, totalDisplayRecords: searchedrecordcount, sEcho: datatablemodel.sEcho);

            return result;
        }

        [HttpGet]
        [Authorize]
        public ActionResult ApproveCustomer()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ApproveCustomer(CustomerVerification customerverification)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Customer findcustomerbyapprovalcode = customerservice.FindCustomerByApprovalCode(customerverification.ApprovalCode);

                    if (findcustomerbyapprovalcode == null)
                    {
                        ModelState.AddModelError(string.Empty, "Verification Code is incorrect, please try again.");
                        return View(customerverification);
                    }

                    else
                    {
                        findcustomerbyapprovalcode.IsApproved = true;
                        customerservice.UpdateCustomer(findcustomerbyapprovalcode, serviceHeader);

                        return RedirectToAction("Dashboard", "Customer");
                    }
                }
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
            return View(customerverification);
        }

        [HttpGet]
        public ActionResult Edit(Guid Id)
        {
            var customer = customerservice.FindCustomerById(Id);
            ViewBag.IncomeRange = GetIncomeRangeSelectList(customer.IncomeRange);
            ViewBag.BanksSelectList = GetBanksSelectList(customer.BankCode);
            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();
            ViewBag.TitleCheckList = titleInfoCheckList();
            ViewBag.BranchesSelectList = BranchesSelectList(customer.BranchId.ToString());
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(customer.RelationshipManagerId.ToString(), customer.BranchId);
            ViewBag.GroupSelectList = GetGroups(Guid.Empty.ToString());
            ViewBag.GenderCheckList = genderInfoCheckList();
            ViewBag.ProfessionSelectList = GetProfessionSelectList(customer.ProfessionId.ToString());
            ViewBag.CountySelectList = GetCountysSelectList(customer.CountyCode);
            ViewBag.ConstituencySelectList = GetConstituencySelectList(customer.CountyCode, customer.ConstituencyCode);
            ViewBag.CountyAssemblyWardSelectList = GetCountyAssemblyWardSelectList(customer.CountyCode, customer.ConstituencyCode, customer.WardCode);
            ViewBag.TitleToView = customer.Title;
            ViewBag.ClientCode = ClientCode;

            return View(customer);
        }

        [ActionName("Edit")]
        [HttpPost]
        public ActionResult Edit(Customer customer)
        {
            ViewBag.IncomeRange = IncomeRangeSelectList();
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(Guid.Empty.ToString(), Guid.Empty);
            ViewBag.GroupSelectList = GetGroups(Guid.Empty.ToString());
            ViewBag.GenderCheckList = genderInfoCheckList();
            ViewBag.ProfessionSelectList = GetProfessionSelectList(string.Empty);
            ViewBag.CountySelectList = GetCountysSelectList(string.Empty);
            ViewBag.ConstituencySelectList = GetConstituencySelectList(string.Empty, string.Empty);
            ViewBag.CountyAssemblyWardSelectList = GetCountyAssemblyWardSelectList(string.Empty, string.Empty, string.Empty);
            ViewBag.TitleToView = customer.Title;
            ViewBag.ClientCode = ClientCode;

            var FindCustomerById = customerservice.FindCustomerById(customer.Id);
            if (ModelState.IsValid)
            {
                try
                {
                    var rulesException = new RulesException();

                    FindCustomerById.BranchId = customer.BranchId;
                    FindCustomerById.KRAPIN = customer.KRAPIN;
                    FindCustomerById.FirstName = customer.FirstName;
                    FindCustomerById.LastName = customer.LastName;
                    FindCustomerById.MobileNumber = customer.MobileNumber;
                    FindCustomerById.MaritalStatus = customer.MaritalStatus;
                    FindCustomerById.MiddleName = customer.MiddleName;
                    FindCustomerById.PoBoxNumber = customer.PoBoxNumber;
                    FindCustomerById.PostalCode = customer.PostalCode;
                    FindCustomerById.Gender = customer.Gender;
                    FindCustomerById.CountyCode = customer.CountyCode;
                    FindCustomerById.ConstituencyCode = customer.ConstituencyCode;
                    FindCustomerById.WardCode = customer.WardCode;
                    FindCustomerById.PlotNumber = customer.PlotNumber;
                    FindCustomerById.Village = customer.Village;
                    FindCustomerById.Landmark = customer.Landmark;
                    FindCustomerById.GroupId = customer.GroupId;
                    FindCustomerById.GroupMemberNumber = customer.GroupMemberNumber;
                    FindCustomerById.GroupRole = customer.GroupRole;
                    FindCustomerById.RelationshipManagerId = customer.RelationshipManagerId;
                    
                    if (string.IsNullOrWhiteSpace(string.Concat(customer.Title)))
                        rulesException.ErrorForModel("Customer title cannot be null");
                    FindCustomerById.Title = customer.Title;

                    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^[0-9A-Za-z]+$");

                    if (customer.IDNumber.Length > 15 || !regex.IsMatch(customer.IDNumber))
                        rulesException.ErrorForModel("ID Number is not valid");
                    FindCustomerById.IDNumber = customer.IDNumber;

                    if (customer.ProfessionId == Guid.Empty || customer.ProfessionId == null)
                        rulesException.ErrorForModel("Profession is required.");
                    FindCustomerById.ProfessionId = customer.ProfessionId;

                    if (ClientCode.Equals("4"))
                    {
                        if (customer.GroupId == Guid.Empty)
                        {
                            rulesException.ErrorForModel("Group Name cannot be null");
                        }
                        else
                        {
                            FindCustomerById.BelongsToAGroup = customer.BelongsToAGroup = true;
                        }

                        if (customer.GroupRole == (int)0)
                            rulesException.ErrorForModel("Group Role cannot be null.");
                        FindCustomerById.GroupRole = customer.GroupRole;

                        if (string.IsNullOrWhiteSpace(customer.GroupMemberNumber))
                            rulesException.ErrorForModel("Group Member Number is required.");
                        FindCustomerById.GroupMemberNumber = customer.GroupMemberNumber;

                        if (string.IsNullOrWhiteSpace(customer.NextOfKinIDNumber))
                            rulesException.ErrorForModel("Next of Kin ID Number cannot be null.");
                        FindCustomerById.NextOfKinIDNumber = customer.NextOfKinIDNumber;

                        if (string.IsNullOrWhiteSpace(customer.NextOfKinName))
                            rulesException.ErrorForModel("Next of Kin Name cannot be null.");
                        FindCustomerById.NextOfKinName = customer.NextOfKinName;

                        if (string.IsNullOrWhiteSpace(customer.NextofKinRelationship))
                            rulesException.ErrorForModel("Next of Kin Relationship cannot be null.");
                        FindCustomerById.NextofKinRelationship = customer.NextofKinRelationship;

                        if (string.IsNullOrWhiteSpace(customer.NextOfKinTelNumber))
                            rulesException.ErrorForModel("Next of Kin Mobile Number cannot be null.");
                        FindCustomerById.NextOfKinTelNumber = customer.NextOfKinTelNumber;
                    }
                    else
                    {
                        FindCustomerById.NextOfKinIDNumber = customer.NextOfKinIDNumber;
                        FindCustomerById.NextOfKinName = customer.NextOfKinName;
                        FindCustomerById.NextofKinRelationship = customer.NextofKinRelationship;
                        FindCustomerById.NextOfKinTelNumber = customer.NextOfKinTelNumber;
                    }

                    if (customer.MaritalStatus == Enumerations.MaritalStatus.Married)
                    {
                        if (string.IsNullOrWhiteSpace(customer.SpouseName))
                            rulesException.ErrorForModel("Spouse Name cannot be null.");
                        FindCustomerById.SpouseName = customer.SpouseName;

                        if (string.IsNullOrWhiteSpace(customer.SpouseIdNumber))
                            rulesException.ErrorForModel("Spouse ID Number cannot be null.");
                        FindCustomerById.SpouseIdNumber = customer.SpouseIdNumber;

                        if (string.IsNullOrWhiteSpace(customer.SpouseTelephoneNumber))
                            rulesException.ErrorForModel("Spouse Telephone Number cannot be null.");
                        FindCustomerById.SpouseTelephoneNumber = customer.SpouseTelephoneNumber;
                    }

                    bool iscustomerunderage = customerservice.ValidateCustomerAge(customer.DateofBirth);
                    if (!iscustomerunderage)
                        rulesException.ErrorForModel("You must be 18 years and above.");
                    FindCustomerById.DateofBirth = customer.DateofBirth;
                    //if (customer.IncomeRange.Equals("Select Income Range"))
                    //     rulesException.ErrorForModel("Income range is required.");
                    //FindCustomerById.IncomeRange = customer.IncomeRange;
                    if (rulesException.Errors.Any())
                        throw rulesException;

                    customerservice.UpdateCustomer(FindCustomerById, serviceHeader);
                    return RedirectToAction("Index");

                }
                catch (RulesException ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    ex.CopyTo(ModelState);
                    return View(customer);
                }
            }
            return View(customer);
        }

        [HttpGet]
        [Authorize]
        public ActionResult Dashboard()
        {
            var paybillnumber = ConfigurationManager.AppSettings["PaybillNumber"].ToString();
            ViewBag.PaybillNumber = paybillnumber;
            return View();
        }

        public ActionResult CustomerLookUpView(int? page)
        {
            var pagecollection = customerservice.GetAllCustomers(IsolationLevel.ReadUncommitted).ToArray();
            ViewBag.Users = pagecollection.AsPagination(page ?? 1, 10);
            ViewBag.CustomerList = pagecollection;
            return PartialView("_CustomerLoopUp");
        }

        public ActionResult AccountStatement()
        {
            return View();
        }

        public ActionResult AccountStatementView(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException("id cannot be null");
            }

            string customerName = string.Empty;
            var findCustomerById = customerservice.FindCustomerById(Guid.Parse(id));
            if (findCustomerById != null)
            {
                customerName = findCustomerById.FullNamemobileNumber;
            }

            ViewBag.Id = id;
            ViewBag.CustomerName = customerName;
            return View();
        }

        public JsonResult GetCustomerAccountStatementView(string id, JQueryDataTablesModel dataTableModel)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException("id cannot be null");
            }
            var customer = new Customer();
            var customerId = Guid.Parse(id);
            if (customerId != Guid.Empty)
            {
                customer = customerservice.FindCustomerById(customerId);
            }
            var pagecollection = customerservice.CustomerAccountStatement(id, customer.CreatedDate, DateTime.Now);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var loanrequestList = GetCustomerAccountBalance(startIndex: dataTableModel.iDisplayStart,
                pageSize: dataTableModel.iDisplayLength, sortedColumns: dataTableModel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: dataTableModel.sSearch, accountStatement: pagecollection);


            return Json(new JQueryDataTablesResponse<CustomerAccountStatement>(items: loanrequestList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: dataTableModel.sEcho));
        }

        public static IList<CustomerAccountStatement> GetCustomerAccountBalance(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<CustomerAccountStatement> accountStatement)
        {
            var GetaccountStatmentList = accountStatement;

            totalRecordCount = GetaccountStatmentList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetaccountStatmentList = GetaccountStatmentList.Where(c => c.AccountName.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetaccountStatmentList.Count;

            IOrderedEnumerable<CustomerAccountStatement> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "TrxDate":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.TrxDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.TrxDate);
                        break;

                    case "AccountName":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.AccountName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.AccountName);
                        break;

                    case "credit":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.credit)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.credit);
                        break;

                    case "debit":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.debit)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.debit);
                        break;

                    case "id":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.id)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.id);
                        break;

                    case "IntBalance":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.IntBalance)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IntBalance);
                        break;

                    case "InterestCR":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.InterestCR)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestCR);
                        break;

                    case "InterestDR":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.InterestDR)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.InterestDR);
                        break;

                    case "PrimaryDescription":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.PrimaryDescription)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.PrimaryDescription);
                        break;

                    case "Product":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.Product)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Product);
                        break;

                    case "reference":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.reference)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.reference);
                        break;

                    case "RunningTotal":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.RunningTotal)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.RunningTotal);
                        break;

                    case "SecondaryDescription":
                        sortedClients = sortedClients == null ? GetaccountStatmentList.CustomSort(sortedColumn.Direction, cust => cust.SecondaryDescription)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.SecondaryDescription);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }


        public ActionResult CustomerSavingsAccountTransations()
        {
            return View();
        }

        /// <summary>
        /// Random approval cod generator
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private string GenerateRandomCustomerApprovalCode(int length)
        {
            string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[length];
            Random rd = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }

        [NonAction]
        public List<SelectListItem> BranchesSelectList(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultvalue = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Branch", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultvalue);

            var otherlist = branchservice.GetAllBranches().Where(x => x.IsEnabled == true);

            foreach (var item in otherlist)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.BranchName, Value = item.Id.ToString("D") });

            return SelectList;
        }

        [NonAction]
        public List<SelectListItem> GetIncomeRangeSelectList(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

             var otherlist = IncomeRangeSelectList();

            foreach (var item in otherlist)
                if (item.Value == selectedvalue)
                {
                    SelectList.Add(new SelectListItem { Selected = true, Text = item.Text, Value = item.Value });
                }
                else
                {
                    SelectList.Add(new SelectListItem { Text = item.Text, Value = item.Value });
                }
            return SelectList;
        }

        [NonAction]
        public List<SelectListItem> GetAllRelationshipInList(string selectedValue, Guid BranchId)
        {
            List<SelectListItem> branchesSelectList = new List<SelectListItem> { };

            var defaultBranchItem = new SelectListItem { Selected = (BranchId.ToString() == string.Empty), Text = "Choose Relationship Manager", Value = string.Empty };

            branchesSelectList.Add(defaultBranchItem);

            var otherBranchItems = relationshipManagerService.FindRelationshipManagerByBranchId(BranchId);

            if (otherBranchItems != null)
            {
                foreach (var item in otherBranchItems)
                {
                    branchesSelectList.Add(new SelectListItem { Selected = (item.Id.ToString() == selectedValue.ToString()), Text = item.RMFullName, Value = (item.Id.ToString()) });
                }
            }

            return branchesSelectList;
        }

        [NonAction]
        public List<SelectListItem> GetAllRelationshipManagersByBranchIdAndRelationshipManagerId(string selectedValue, Guid BranchId)
        {
            List<SelectListItem> returnList = new List<SelectListItem>();

            var findAllRM = new List<RelationshipManager>();

            var defaultRM = new SelectListItem { Selected = (selectedValue == Guid.Empty.ToString("D")), Text = "Select Relationship Manager", Value = Guid.Empty.ToString("D") };

            returnList.Add(defaultRM);
            var rmId = Guid.Parse(selectedValue);
            findAllRM = relationshipManagerService.FindRelationshipManagerByBranchIdAndRelationshipManagerId(BranchId, rmId).ToList();

            foreach (var RM in findAllRM)
                returnList.Add(new SelectListItem { Selected = RM.Id == Guid.Parse(selectedValue), Text = RM.RMFullName, Value = RM.Id.ToString("D") });

            return returnList;
        }

        [NonAction]
        public List<SelectListItem> GetGroups(string selectedValue)
        {
            List<SelectListItem> returnList = new List<SelectListItem>();

            var findAllGroup = new List<Group>();

            var defaultGroup = new SelectListItem { Selected = (selectedValue == Guid.Empty.ToString("D")), Text = "Select Group", Value = Guid.Empty.ToString("D") };

            returnList.Add(defaultGroup);
            var rmId = Guid.Parse(selectedValue);
            findAllGroup = groupService.GetAllGroup(IsolationLevel.ReadUncommitted).ToList();

            foreach (var group in findAllGroup)
                returnList.Add(new SelectListItem { Selected = group.Id == Guid.Parse(selectedValue), Text = group.GroupName, Value = group.Id.ToString("D") });

            return returnList;
        }

        [HttpGet]
        public ActionResult TransferCustomers()
        {
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(Guid.Empty.ToString(), Guid.Empty);
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            return View();
        }

        [HttpPost]
        public ActionResult TransferCustomers([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<Customer> customer, [Bind(Prefix = "xId")]string targetBranchId, string Name2)
        {
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(Guid.Empty.ToString(), Guid.Empty);
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            return View();
        }

        public ActionResult FilterMenuCustomization_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(customerservice.GetAllCustomers(IsolationLevel.ReadUncommitted).ToDataSourceResult(request));
        }

        public ActionResult FilterMenuCustomization_RMs()
        {
            var rmList = relationshipManagerService.GetAllRelationshipManagers(IsolationLevel.ReadUncommitted);
            return Json(rmList.Select(e => e.LastName).Distinct(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilterMenuCustomization_Branch()
        {
            var rmList = branchservice.GetAllBranches();
            return Json(rmList.Select(e => e.BranchName).Distinct(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.IncomeRange = IncomeRangeSelectList();
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(Guid.Empty.ToString(), Guid.Empty);
            ViewBag.GroupSelectList = GetGroups(Guid.Empty.ToString());
            ViewBag.ProfessionSelectList = GetProfessionSelectList(string.Empty);
            ViewBag.GenderCheckList = genderInfoCheckList();
            ViewBag.CountySelectList = GetCountysSelectList(string.Empty);
            ViewBag.ConstituencySelectList = GetConstituencySelectList(string.Empty, string.Empty);
            ViewBag.CountyAssemblyWardSelectList = GetCountyAssemblyWardSelectList(string.Empty, string.Empty, string.Empty);
            ViewBag.ClientCode = ClientCode;
            ViewBag.RelationshipManagerGuid = relationshipManagerGuid;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Customer customer)
        {
            ViewBag.IncomeRange = IncomeRangeSelectList();
            ViewBag.BanksSelectList = GetBanksSelectList(string.Empty);
            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            ViewBag.RelationshipManagerSelectList = GetAllRelationshipInList(Guid.Empty.ToString(), Guid.Empty);
            ViewBag.GroupSelectList = GetGroups(Guid.Empty.ToString());
            ViewBag.GenderCheckList = genderInfoCheckList();
            ViewBag.ProfessionSelectList = GetProfessionSelectList(string.Empty);
            ViewBag.ClientCode = ClientCode;
            ViewBag.CountySelectList = GetCountysSelectList(string.Empty);
            ViewBag.ConstituencySelectList = GetConstituencySelectList(string.Empty, string.Empty);
            ViewBag.CountyAssemblyWardSelectList = GetCountyAssemblyWardSelectList(string.Empty, string.Empty, string.Empty);

            if (ModelState.IsValid)
            {
                try
                {
                    var rulesException = new RulesException();

                    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("^[0-9A-Za-z]+$");

                    if (customer.IDNumber.Length > 15 || !regex.IsMatch(customer.IDNumber))
                        rulesException.ErrorForModel("ID Number is not valid");

                    if (string.IsNullOrWhiteSpace(string.Concat(customer.Title)))
                        rulesException.ErrorForModel("Customer Type cannot be null");

                    if (customer.ProfessionId == Guid.Empty || customer.ProfessionId == null)
                        rulesException.ErrorForModel("Profession is required.");

                    if(!string.IsNullOrWhiteSpace( customer.WardName ) && customer.BranchId==Guid.Empty && customer.GroupId == Guid.Empty){
                        var findGroup = groupService.FindGroupByGroupNumber(customer.WardName);
                        customer.GroupId = findGroup.Id;
                        customer.BranchId = (Guid)findGroup.BranchId;
                        customer.RelationshipManagerId = findGroup.RelationshipManagerId;
                        customer.GroupId = findGroup.Id;
                    }

                    if (ClientCode.Equals("4"))
                    {
                        if (customer.GroupId == Guid.Empty)
                        {
                            rulesException.ErrorForModel("Group Name cannot be null");
                        }
                        else
                        {
                            customer.BelongsToAGroup = true;
                        }                        
                    }

                    if (customer.MaritalStatus == Enumerations.MaritalStatus.Married)
                    {
                        if (string.IsNullOrWhiteSpace(customer.SpouseName))
                            rulesException.ErrorForModel("Spouse Name cannot be null.");

                        if (string.IsNullOrWhiteSpace(customer.IDNumber))
                            rulesException.ErrorForModel("Spouse ID Number cannot be null.");

                        if (string.IsNullOrWhiteSpace(customer.SpouseTelephoneNumber))
                            rulesException.ErrorForModel("Spouse Telephone Number cannot be null.");
                    }

                    //bool checkDuplicateGroupRole = customerservice.ValidateGroupRole(customer.GroupRole);
                    //if(checkDuplicateGroupRole)
                    //{
                    //    rulesException.ErrorForModel(string.Format("Group role {0} already exists", customer.GroupRole.ToString()));
                    //}

                    bool iscustomerunderage = customerservice.ValidateCustomerAge(customer.DateofBirth);
                    if (!iscustomerunderage)
                        rulesException.ErrorForModel("You must be 18 years and above.");
                    bool checkduplicatecustomer = customerservice.CheckDuplicateCustomer(customer.MobileNumber, customer.IDNumber);
                    if (checkduplicatecustomer)
                        rulesException.ErrorForModel(string.Format("Customer with Mobile Number {0} or Id Number {1} already exists", customer.MobileNumber, customer.IDNumber));
                    
                    if (rulesException.Errors.Any())
                        throw rulesException;

                    #region Customer Details

                    string nextcustomernumber = customerservice.NextCustomerNumber();
                    customer.ApprovalCode = GenerateRandomCustomerApprovalCode(6);
                    customer.CustomerNumber = nextcustomernumber;
                    customer.IsApproved = true;
                    customer.IsEnabled = true;
                    customer.CustomerType = Enumerations.CustomerTypes.Individual;
                    customer.FirstName = customer.FirstName.ToUpper();
                    if (!string.IsNullOrWhiteSpace(customer.MiddleName))
                        customer.MiddleName = customer.MiddleName.ToUpper();
                    customer.LastName = customer.LastName.ToUpper();
                    customer.RecordStatus = (int)Enumerations.RecordStatus.New;
                    customer.RegisteredBy = HttpContext.User.Identity.Name;
                    customer.CustomerRegistrationType = Enumerations.CustomerRegistrationType.WalkIn;

                    if (ConfigurationManager.AppSettings["ClientCode"].Equals("6"))
                    {
                        var mobilePIN = string.Empty;

                        mobilePIN = string.Format("{0}", MiscUtil.StaticRandom.Next(1000, 9999));

                        customer.PIN = PasswordHash.CreateHash(string.Format("{0}", mobilePIN));

                        // Compose text
                        var smsBody = new StringBuilder();

                        //smsBody.Append(string.Format("{0}", DefaultSettings.Instance.ApplicationDisplayName));
                        //smsBody.Append(Environment.NewLine);
                        smsBody.Append(string.Format("Your {0} PIN is: {1}. Please dial *384*33# to enjoy this free service.", DefaultSettings.Instance.ApplicationDisplayName, mobilePIN));
                        smsBody.Append(Environment.NewLine);

                        var textAlertDTO = new TextAlert
                        {
                            TextMessageOrigin = (int)Nikopeshe.Entity.DataModels.Enumerations.MessageOrigin.Within,
                            TextMessageRecipient = customer.MobileNumber.StartsWith("+") ? customer.MobileNumber : string.Format("+{0}", customer.MobileNumber),
                            TextMessageBody = string.Format("{0}", smsBody),
                            TextMessageDLRStatus = (int)Nikopeshe.Entity.DataModels.Enumerations.DLRStatus.Pending,
                            TextMessageSecurityCritical = true,
                        };

                        textalertservice.AddNewTextAlert(textAlertDTO, serviceHeader);
                    }

                    #endregion

                    #region Customer Account Details

                    CustomersAccount customersaccount = new CustomersAccount();
                    string nextcustomeraccountnumber = customerservice.NextCustomerSavingAccountNumber(nextcustomernumber);
                    customersaccount.CustomerId = customer.Id;
                    customersaccount.AccountType = (int)Enumerations.ProductCode.Savings;
                    customersaccount.AccountNumber = nextcustomeraccountnumber;
                    customersaccount.IsActive = true;

                    #endregion

                    var findmembershipuser = Membership.GetUser(customer.MobileNumber);

                    if (checkduplicatecustomer == false && findmembershipuser == null)
                    {
                        string password = Membership.GeneratePassword(8, 2);
                        var findStaticSetting = staticSettingService.FindStaticSettingByKey("CREATEMEMBERSHIPACCOUNT");

                        if (findStaticSetting.Value == "1")
                        {
                            MembershipCreateStatus status;
                            MembershipUser createuser = Membership.CreateUser(customer.MobileNumber, password, customer.EmailAddress, "Where were you when you first heard about 9/11?", "School", true, out status);
                            Roles.AddUserToRole(customer.MobileNumber, "Customer");
                            string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                            string loginurl = "<a href='" + mvcclientendposint + "'/Account/Login> click here</a>";

                            if (status == MembershipCreateStatus.Success)
                            {
                                //Create email alert and insert in the database
                                StringBuilder bodyBuilder = new StringBuilder();
                                bodyBuilder.Append("Dear Sir/Madam,");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.Append(string.Format("Welcome to {0}. Below you will find your login details.", DefaultSettings.Instance.ApplicationDisplayName));
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.Append("Username: " + customer.MobileNumber);
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.Append("Password: " + password);
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.Append(string.Format("Continue to approve your account and borrow {0}.", loginurl));
                                bodyBuilder.AppendLine("<br />");
                                bodyBuilder.AppendLine("<br />");

                                var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
                                bodyBuilder.Append(emailSignature);

                                var emailAlertDTO = new EmailAlert
                                {
                                    MailMessageFrom = DefaultSettings.Instance.Email,
                                    MailMessageTo = string.Format("{0}", customer.EmailAddress),
                                    MailMessageSubject = string.Format("Login Details - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                    MailMessageBody = string.Format("{0}", bodyBuilder),
                                    MailMessageIsBodyHtml = true,
                                    MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                    MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                    MailMessageSecurityCritical = false,
                                    CreatedBy = "System",
                                    MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                };

                                emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                                NewTextAlert(customer.FullName, customer.MobileNumber, customer.MobileNumber, password, customer.ApprovalCode);
                                TempData["Message"] = "Account created successfully. An email/text message has been sent to you with your approval code. Provide the code after login to approve your account.";
                            }

                            switch (status)
                            {
                                case MembershipCreateStatus.Success:
                                    ModelState.AddModelError(string.Empty, "User Account created successfully");

                                    break;
                                case MembershipCreateStatus.DuplicateUserName:
                                    ModelState.AddModelError(string.Empty, "There already exists a user with this username.");
                                    break;

                                case MembershipCreateStatus.DuplicateEmail:
                                    ModelState.AddModelError(string.Empty, "There already exists a user with this email address.");
                                    break;
                                case MembershipCreateStatus.InvalidEmail:
                                    ModelState.AddModelError(string.Empty, "The email address you provided in invalid.");
                                    break;
                                case MembershipCreateStatus.InvalidAnswer:
                                    ModelState.AddModelError(string.Empty, "The security answer is invalid.");
                                    break;
                                case MembershipCreateStatus.InvalidPassword:
                                    ModelState.AddModelError(string.Empty, "The password you provided is invalid. It must be seven characters long and have at least one non-alphanumeric character.");

                                    break;
                                default:
                                    ModelState.AddModelError(string.Empty, status.ToString());
                                    break;
                            }
                        }

                        var findStaticSettingByKey = staticSettingService.FindStaticSettingByKey("CUSTOMERJOININGFEE");
                        //Post Joining fee to Income GL
                        if (customer.JoiningFee == true)
                        {
                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                            var findCustomerJoiningFeeGL = glMappingService.FindSystemGeneralLedgerAccountMappingsByGLCode((int)Enumerations.SystemGeneralLedgerAccountCode.CustomerJoiningFee);
                            var findDefaultSavingProduct = savingProductService.FindDefaultSavingsProduct();
                            #region Dr Saving GL, Cr Joining Fee GL
                            LoanTransactionHistory drSavingGL = new LoanTransactionHistory()
                            {
                                TransactionType = Enumerations.TransactionType.Debit,
                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                TransactionAmount = decimal.Parse(findStaticSettingByKey.Value),
                                TotalTransactionAmount = decimal.Parse(findStaticSettingByKey.Value),
                                ChartofAccountId = findDefaultSavingProduct.ChartofAccountId,
                                ContraAccountId = findCustomerJoiningFeeGL.ChartofAccountId,
                                CustomersAccountId = customersaccount.Id,
                                Description = "Customer Joining Fee",
                                CreatedBy = User.Identity.Name,
                                IsApproved = true,
                                ApprovedBy = string.Empty,
                                TransactionCategory = (int)Enumerations.TransactionCategory.CustomerJoiningFee,
                                BranchId = customer.BranchId,
                                TransactionBatchNumber = batchNo
                            };

                            #endregion

                            #region Cr Saving GL, Dr Joining Fee GL
                            LoanTransactionHistory crJoiningFeeGL = new LoanTransactionHistory()
                            {
                                TransactionType = Enumerations.TransactionType.Credit,
                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                TransactionAmount = decimal.Parse(findStaticSettingByKey.Value),
                                TotalTransactionAmount = decimal.Parse(findStaticSettingByKey.Value),
                                ChartofAccountId = findCustomerJoiningFeeGL.ChartofAccountId,
                                ContraAccountId = findDefaultSavingProduct.ChartofAccountId,
                                CustomersAccountId = customersaccount.Id,
                                Description = "Customer Joining Fee",
                                CreatedBy = User.Identity.Name,
                                IsApproved = true,
                                ApprovedBy = string.Empty,
                                TransactionCategory = (int)Enumerations.TransactionCategory.CustomerJoiningFee,
                                BranchId = customer.BranchId,
                                TransactionBatchNumber = batchNo
                            };
                            using (var dataContext = new DataContext())
                            {
                                using (var transaction = dataContext.Database.BeginTransaction())
                                {
                                    try
                                    {

                                        customerservice.AddNewCustomer(customer, serviceHeader);
                                        customeraccountservice.AddNewCustomersAccount(customersaccount, serviceHeader);
                                        if (customer.GroupId != Guid.Empty)
                                        {
                                            var customerGroupRelationship = new CustomerGroupRelationship
                                            {
                                                Id = Guid.NewGuid(),
                                                CreatedDate = DateTime.Now,
                                                CustomerId = customer.Id,
                                                GroupId = customer.GroupId,
                                                GroupRole = (int)customer.GroupRole
                                            };

                                            customerGroupRelationshipService.AddNewCustomerGroupRelationship(customerGroupRelationship, serviceHeader);
                                        }
                                        loantransactionhistory.AddNewLoanHistory(drSavingGL, serviceHeader);
                                        loantransactionhistory.AddNewLoanHistory(crJoiningFeeGL, serviceHeader);
                                        transaction.Commit();
                                    }

                                    catch (Exception ex)
                                    {
                                        transaction.Rollback();
                                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                    }
                                }
                            }

                            #endregion
                        }

                        else
                        {
                            customerservice.AddNewCustomer(customer, serviceHeader);
                            customeraccountservice.AddNewCustomersAccount(customersaccount, serviceHeader);
                            if (customer.GroupId != Guid.Empty)
                            {
                                var customerGroupRelationship = new CustomerGroupRelationship
                                {
                                    Id = Guid.NewGuid(),
                                    CreatedDate = DateTime.Now,
                                    CustomerId = customer.Id,
                                    GroupId = customer.GroupId,
                                    GroupRole = (int)customer.GroupRole,
                                    GroupMemberNo = customer.GroupMemberNumber
                                };

                                customerGroupRelationshipService.AddNewCustomerGroupRelationship(customerGroupRelationship, serviceHeader);
                            }
                        }
                        //return RedirectToAction("Successful", "Customer");
                        return RedirectToAction("Index");
                    }

                    else
                    {
                        ModelState.AddModelError(string.Empty, "An error has occurred while processing. Make sure the mobile number is unique.");
                        return View(customer);
                    }
                }

                catch (RulesException ex)
                {
                    ex.CopyTo(ModelState);
                }
            }

            return View(customer);
        }

        [HttpGet]
        public ActionResult AddCustomer()
        {
            ViewBag.PartnersSelectList = GetPartnersSelectList(Guid.Empty.ToString());
            return View();
        }

        [HttpPost]
        public ActionResult AddCustomer(USSDCustomer customer)
        {
            ViewBag.PartnersSelectList = GetPartnersSelectList(Guid.Empty.ToString());

            if (ModelState.IsValid)
            {
                try
                {
                    var rulesException = new RulesException();
                    bool checkduplicatecustomer = customerservice.CheckDuplicateCustomer(customer.MobileNumber, customer.IDNumber);
                    if (checkduplicatecustomer)
                        rulesException.ErrorForModel(string.Format("Customer with Mobile Number {0} or Id Number {1} already exists", customer.MobileNumber, customer.IDNumber));

                    if (customer.PartnerId == Guid.Empty || customer.PartnerId == null)
                        rulesException.ErrorForModel("Partner cannot be null.");

                    if (rulesException.Errors.Any())
                        throw rulesException;

                    Guid customerid = Guid.NewGuid();

                    var headOfficeBranchId = branchservice.FindBranchByBranchCode("000");
                    var customerNumber = customerservice.NextCustomerNumber();
                    string mobilePIN = string.Format("{0}", MiscUtil.StaticRandom.Next(1000, 9999));
                    var customerInstance = new Customer
                    {
                        Id = customerid,
                        BranchId = headOfficeBranchId.Id,
                        ConstituencyCode = "000",
                        CountyCode = "000",
                        IDNumber = customer.IDNumber,
                        CustomerNumber = customerNumber,
                        CustomerRegistrationType = Enumerations.CustomerRegistrationType.WalkIn,
                        CustomerType = Enumerations.CustomerTypes.Individual,
                        DateofBirth = DateTime.Today,
                        FirstName = customer.FirstName.ToUpper(),
                        LastName = customer.LastName.ToUpper(),
                        MobileNumber = customer.MobileNumber,
                        Occupation = "Null",
                        MaritalStatus = Enumerations.MaritalStatus.Single,
                        PartnerId = customer.PartnerId,
                        PIN = PasswordHash.CreateHash(string.Format("{0}", mobilePIN)),
                        PreQualifiedAmount = customer.PreQualifiedAmount,
                        RegisteredBy = User.Identity.Name,
                        Title = Enumerations.Title.Prof,
                        Gender = Enumerations.Gender.Male,
                        CreatedDate = DateTime.Now,
                        IsEnabled = true
                    };

                    customerservice.AddNewCustomer(customerInstance, serviceHeader);

                    // Compose text
                    var smsBody = new StringBuilder();

                    //smsBody.Append(string.Format("{0}", DefaultSettings.Instance.ApplicationDisplayName));
                    //smsBody.Append(Environment.NewLine);
                    smsBody.Append(string.Format("Your {0} PIN is: {1}. Please dial *384*33# to enjoy this service.", DefaultSettings.Instance.ApplicationDisplayName, mobilePIN));
                    smsBody.Append(Environment.NewLine);

                    var textAlertDTO = new TextAlert
                    {
                        TextMessageOrigin = (int)Nikopeshe.Entity.DataModels.Enumerations.MessageOrigin.Within,
                        TextMessageRecipient = customer.MobileNumber.StartsWith("+") ? customer.MobileNumber : string.Format("+{0}", customer.MobileNumber),
                        TextMessageBody = string.Format("{0}", smsBody),
                        TextMessageDLRStatus = (int)Nikopeshe.Entity.DataModels.Enumerations.DLRStatus.Pending,
                        TextMessageSecurityCritical = true,
                    };

                    textalertservice.AddNewTextAlert(textAlertDTO, serviceHeader);

                    CustomersAccount customersaccount = new CustomersAccount
                    {
                        CustomerId = customerid,
                        Id = Guid.NewGuid(),
                        AccountNumber = customerservice.NextCustomerSavingAccountNumber(customerNumber),
                        AccountType = (int)Enumerations.ProductCode.Savings,
                        CreatedDate = DateTime.Now,
                        IsActive = true
                    };

                    customeraccountservice.AddNewCustomersAccount(customersaccount, serviceHeader);

                    return RedirectToAction("Index", "Customer");
                }

                catch (RulesException ex)
                {
                    ex.CopyTo(ModelState);
                }
            }
            return View(customer);
        }

        #region Helpers
        public JsonResult LoadRMsByBranch(Guid branchId)
        {
            var loadBranches = relationshipManagerService.FindRelationshipManagerByBranchId(branchId);

            var returnData = loadBranches.Select(x => new SelectListItem()
            {
                Text = x.Branch.BranchName,
                Value = x.Id.ToString()
            });

            return Json(returnData, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult RMSelectList(string id)
        {
            //if(string.IsNullOrWhiteSpace(id))
            //{
            //    throw new ArgumentNullException("Must select a value from the drop down");             
            //}
            var accountList = relationshipManagerService.FindRelationshipManagerByBranchId(Guid.Parse(id));

            //var accountlist = GetBranchesList(id);

            var modelData = accountList.Select(m => new SelectListItem()
            {
                Text = m.RMFullName,
                Value = m.Id.ToString(),
                Selected = false,
            });

            return Json(modelData, JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult RMSelectListByGroupNumber(string groupNumber)
        {
            try
            {
                //if(string.IsNullOrWhiteSpace(id))
                //{
                //    throw new ArgumentNullException("Must select a value from the drop down");             
                //}
                var findGroup = groupService.FindGroupByGroupNumber(groupNumber);

                //var accountList = relationshipManagerService.FindRelationshipManagerByBranchId((Guid)findGroup.BranchId);

                var accountList = relationshipManagerService.FindRelationshipManagerSelectListById((Guid)findGroup.RelationshipManagerId);

                //var accountlist = GetBranchesList(id);

                var modelData = accountList.Select(m => new SelectListItem()
                {
                    Text = m.RMFullName,
                    Value = m.Id.ToString(),
                    Selected = false,
                });

                return Json(modelData, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult BranchByGroupNumber(string id)
        {
            try
            {
                //if(string.IsNullOrWhiteSpace(id))
                //{
                //    throw new ArgumentNullException("Must select a value from the drop down");             
                //}
                var findGroup = groupService.FindGroupByGroupNumber(id);
                var findGroups = groupService.GetAllGroup(IsolationLevel.ReadCommitted).Where(x => x.GroupNumber == id);
                var accountList = relationshipManagerService.FindRelationshipManagerByBranchId((Guid)findGroup.BranchId);

                //var accountlist = GetBranchesList(id);

                var modelData = accountList.Select(m => new SelectListItem()
                {
                    Text = m.RMFullName,
                    Value = m.Id.ToString(),
                    Selected = false,
                });

                var modelData2 = findGroups.Select(m => new SelectListItem()
                {
                    Text = findGroup.GroupName,
                    Value = findGroup.Id.ToString(),
                    Selected = false,
                });

                return Json(modelData2, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [NonAction]
        protected List<SelectListItem> GetProfessionSelectList(string selectedValue)
        {
            List<SelectListItem> professionSelectList = new List<SelectListItem> { };

            var defaultProfessionItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = "Choose Main Activity", Value = string.Empty };

            professionSelectList.Add(defaultProfessionItem);

            var otherProfessionItems = professionService.GetAllProfessions(IsolationLevel.ReadCommitted);

            if (otherProfessionItems != null)
            {
                foreach (var item in otherProfessionItems)
                {
                    professionSelectList.Add(new SelectListItem { Selected = item.Id.ToString() == selectedValue, Text = item.ProfessionName, Value = item.Id.ToString() });
                }
            }
            return professionSelectList;
        }

        [NonAction]
        protected List<SelectListItem> GetPartnersSelectList(string selectedValue)
        {
            List<SelectListItem> partnersSelectList = new List<SelectListItem>() { };

            var defaultPartner = new SelectListItem { Selected = (selectedValue == string.Empty), Text = "Select Partner", Value = string.Empty };

            partnersSelectList.Add(defaultPartner);

            var otherPartners = partnerService.GetAllPartners(IsolationLevel.ReadCommitted);

            if(otherPartners != null && otherPartners.Any())
            {
                foreach (var partner in otherPartners)
                {
                    partnersSelectList.Add(new SelectListItem
                    {
                        Selected = partner.Id.ToString() == selectedValue, Text = partner.PartnerName, Value = partner.Id.ToString()
                    });
                }
            }

            return partnersSelectList;
        }
        #endregion
    }

    public class USSDCustomer
    {
        public Guid PartnerId { get; set; }

        [Required]
        [Display(Name="First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(12, MinimumLength = 12)]
        [Display(Name = "Mobile Number(2547********)")]
        [RegularExpression(@"^254(?:[0-9]??){6,14}[0-9]$", ErrorMessage = "The mobile number should start with country code 254 followed by phone number")]
        public string MobileNumber { get; set; }

        [Required]
        [Display(Name = "ID Number")]
        public string IDNumber { get; set; }

        [Required]
        [Display(Name = "Prequalified Amount")]
        public decimal PreQualifiedAmount { get; set; }
    }
}
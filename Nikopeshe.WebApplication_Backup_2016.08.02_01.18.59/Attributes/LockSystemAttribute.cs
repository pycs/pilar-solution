﻿using Nikopeshe.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Nikopeshe.WebApplication.Attributes
{
    public class LockSystemAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException("filterContext");

            IPrincipal user = filterContext.HttpContext.User;

            DataContext datacontext = new DataContext();

            if (user != null && user.Identity.IsAuthenticated)
            {
                bool role = Roles.IsUserInRole("Customer");

                // has the customer approved account
                if (role)
                {
                    var LOCKSYSTEMKEY = datacontext.StaticSetting.Where(x => x.Key == "ENABLELOCKSYSTEM").FirstOrDefault().ToString();
                    var OPENINGTIME = datacontext.StaticSetting.Where(x => x.Key == "OPENINGTIME").FirstOrDefault().ToString();
                    var CLOSINGTIME = datacontext.StaticSetting.Where(x => x.Key == "CLOSINGTIME").FirstOrDefault().ToString();
                    if (!string.IsNullOrWhiteSpace(LOCKSYSTEMKEY) && !string.IsNullOrWhiteSpace(OPENINGTIME) && !string.IsNullOrWhiteSpace(CLOSINGTIME))
                    {

                    }
                }
            }
        }
    }
}
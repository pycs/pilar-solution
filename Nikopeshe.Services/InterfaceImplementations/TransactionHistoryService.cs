﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class TransactionHistoryService : ITransactionHistoryService
    {
        private readonly IRepository<TransactionHistory> transactionhistoryrepository;

        string errormessage = string.Empty;

        public TransactionHistoryService(IRepository<TransactionHistory> _transactionhistoryrepository)
        {
            if(_transactionhistoryrepository == null)
                throw new ArgumentNullException("null repository");

            this.transactionhistoryrepository = _transactionhistoryrepository;
        }

        public void AddNewTransactionHistory(TransactionHistory transactionhistory, ServiceHeader serviceHeader)
        {
            if(transactionhistory == null)
            {
                throw new ArgumentNullException("Entity cannot be null");
            }

            try
            {
                transactionhistoryrepository.Insert(transactionhistory, serviceHeader);
            }

            catch(DbEntityValidationException ex)
            {
                foreach(var validationerrors in ex.EntityValidationErrors)
                {
                    foreach(var validationerror in validationerrors.ValidationErrors)
                    {
                        errormessage += string.Format("Error message: {0} Property{1}", validationerror.ErrorMessage, validationerror.PropertyName);                        
                    }
                }
                throw new Exception(errormessage, ex);
            }
        }

        public TransactionHistory GetAllLoanTransactionsHistoryById(Guid Id)
        {
            return transactionhistoryrepository.GetById(Id);
        }

        public void UpdateTransactionHistory(TransactionHistory transactionhistory, ServiceHeader serviceHeader)
        {
            if (transactionhistory == null)
            {
                throw
                     new ArgumentNullException("Entity cannot be null");
            }

            try
            {
                transactionhistoryrepository.Update(transactionhistory, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationerrors in ex.EntityValidationErrors)
                {
                    foreach (var validationerror in validationerrors.ValidationErrors)
                    {
                        errormessage += string.Format("Error message: {0} Property{1}", validationerror.ErrorMessage, validationerror.PropertyName);
                    }
                }
                throw new Exception(errormessage, ex);
            }
        }

        public IList<TransactionHistory> GetAllLoanTransactionsHistory()
        {
            return transactionhistoryrepository.GetAll(IsolationLevel.ReadUncommitted).ToList();
        }

        public IList<TransactionHistory> GetAllCustomerTransationHistoryByCustomerId(Guid customerid)
        {
            return transactionhistoryrepository.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.CustomerId == customerid).ToList();
        }
    }
}

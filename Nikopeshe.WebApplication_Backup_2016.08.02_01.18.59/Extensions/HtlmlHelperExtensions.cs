﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.WebApplication.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Nikopeshe.WebApplication.Extensions
{
    public static class HtlmlHelperExtensions
    {        
        public static string HeadsUp(this HtmlHelper htmlhelper)
        {
            string result = string.Empty;

            var roles = Roles.GetRolesForUser();

            DataContext datacontext = new DataContext();            

            var user = HttpContext.Current.User.Identity.Name;

            var userInfo = datacontext.UserInfo.Where(x => x.UserName == user).FirstOrDefault();

            if(roles != null && roles.Any())
            {
                switch(roles.Single())
                {
                    case "Administrator":
                    case "LoanOfficer":
                    case "FinanceManager":
                    case "LoanManager":
                    var sbInternal = new StringBuilder();
                    var findloanrequestpendingapproval = new List<LoanRequest>();
                    var findloanrequestpendingprocessing = new List<LoanRequest>();
                    if (userInfo != null)
                    {
                        if (userInfo.Branch.BranchCode == "000")
                        {
                            findloanrequestpendingapproval = datacontext.LoanRequest.Where(x => x.Status == (int)Enumerations.LoanStatus.Pending).ToList();
                            findloanrequestpendingprocessing = datacontext.LoanRequest.Where(x => x.Status == (int)Enumerations.LoanStatus.Approved).ToList();
                        }
                        else
                        {
                            findloanrequestpendingapproval = datacontext.LoanRequest.Where(x => x.Status == (int)Enumerations.LoanStatus.Pending && x.BranchId == userInfo.BranchId).ToList();
                            findloanrequestpendingprocessing = datacontext.LoanRequest.Where(x => x.Status == (int)Enumerations.LoanStatus.Approved && x.BranchId == userInfo.BranchId).ToList();
                        }
                    }
                    else
                    {
                        findloanrequestpendingapproval = datacontext.LoanRequest.Where(x => x.Status == (int)Enumerations.LoanStatus.Pending).ToList();
                        findloanrequestpendingprocessing = datacontext.LoanRequest.Where(x => x.Status == (int)Enumerations.LoanStatus.Approved).ToList();
                    }
                   
                    
                    var pendingloanrequestcount = findloanrequestpendingapproval.Count();
                    var findloanrequestpendingprocessingcount = findloanrequestpendingprocessing.Count();


                    sbInternal.Append("<div class=\"sublinks\">");
                    sbInternal.Append("<ul>");
                    sbInternal.Append(string.Format("<li>Pending Approval ({0})</li>", pendingloanrequestcount));
                    sbInternal.Append(string.Format("<li>Pending Disbursement ({0})</li>", findloanrequestpendingprocessingcount));
                    //sbInternal.Append(string.Format("<li><a href=\"{0}\" target=\"#\">Credit Review ()</a></li>", ConfigurationManager.AppSettings["CreditReviewReportUrl"]));
                    sbInternal.Append("</ul>");
                    sbInternal.Append("</div>");

                    result = sbInternal.ToString();

                    break;

                    case "Customer":

                    try
                    {
                        var sbInternal1 = new StringBuilder();
                        var findcustomerid = datacontext.Customer.Where(x => x.MobileNumber == user).FirstOrDefault();
                        var findcustomersavingsaccount = datacontext.CustomersAccount.Where(x => x.CustomerId == findcustomerid.Id && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
                        var findcustomeractiveloanrequest = datacontext.LoanRequest.Where(x => x.CustomerAccount.CustomerId == findcustomerid.Id && x.Status == (int)Enumerations.LoanStatus.Active).FirstOrDefault();
                        if(findcustomeractiveloanrequest != null)
                        {
                            List<LoanTransactionHistory> findloantransactionsbyloanrequestid = datacontext.LoanTransactionHistory.Where(x => x.LoanRequestId == findcustomeractiveloanrequest.Id && x.IsApproved == true && x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment
                                && x.TransactionType == Enumerations.TransactionType.Debit &&x.CustomersAccountId == findcustomersavingsaccount.Id).ToList();
                            var totalpricipalamount = findloantransactionsbyloanrequestid.Sum(x => x.TransactionAmount);                            
                            var totalpaidamount = totalpricipalamount;
                            var totalbalance = (findcustomeractiveloanrequest.LoanAmount + findcustomeractiveloanrequest.InterestAmount) - totalpaidamount;
                            var totalloanamount = findcustomeractiveloanrequest.LoanAmount + findcustomeractiveloanrequest.InterestAmount;
                            decimal principleperschedule = Math.Round(findcustomeractiveloanrequest.LoanAmount / findcustomeractiveloanrequest.NegotiatedInstallments, 2);
                            decimal interperschedule = Math.Round(findcustomeractiveloanrequest.InterestAmount / findcustomeractiveloanrequest.NegotiatedInstallments, 2);
                            decimal totalrepayment = principleperschedule + interperschedule;
                            sbInternal1.Append("<div class=\"sublinks\">");
                            sbInternal1.Append("<ul>");
                            sbInternal1.Append(string.Format("<li>Total Loan Amount ({0})</li>", totalloanamount));
                            sbInternal1.Append(string.Format("<li>Total Paid Amount ({0})</li>", totalpaidamount));
                            sbInternal1.Append(string.Format("<li>Total Balance ({0})</li>", totalbalance));
                            sbInternal1.Append(string.Format("<li>Next Due Date ({0}) - Amount Due ({1})</li>", findcustomeractiveloanrequest.NextPaymentDate.ToString(), totalrepayment));
                            sbInternal1.Append("</ul>");
                            sbInternal1.Append("</div>");
                            result = sbInternal1.ToString();
                        }
                        else
                        {
                            sbInternal1.Append("<div class=\"sublinks\">");
                            sbInternal1.Append("<ul>");                            
                            sbInternal1.Append(string.Format("<li>You have no active loan.</li>"));
                            sbInternal1.Append("</ul>");
                            sbInternal1.Append("</div>");
                            result = sbInternal1.ToString();
                        }                                         
                    }
                    catch(Exception ex)
                    {
                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    }

                    break;
                }
            }

            return result;
        }  
      
        public static string UrlLinkHeadsUp(this HtmlHelper htmlhelper)
        {
            string result = string.Empty;

            var roles = Roles.GetRolesForUser();

            if (roles != null && roles.Any())
            {
                switch (roles.Single())
                {
                    case "Administrator":

                        var sbInternal = new StringBuilder();

                        sbInternal.Append("<div class=\"navbar-bottom\">");
                        sbInternal.Append("<ul id=\"menu\">");
                        sbInternal.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">Pending Extraction ()</a></li>", ConfigurationManager.AppSettings["PendingExtractionReportUrl"]));
                        sbInternal.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">Pending Processing ()</a></li>", ConfigurationManager.AppSettings["PendingProcessingReportUrl"]));
                        sbInternal.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">Credit Review ()</a></li>", ConfigurationManager.AppSettings["CreditReviewReportUrl"]));
                        sbInternal.Append("</ul>");
                        sbInternal.Append("</div>");

                        result = sbInternal.ToString();

                        break;

                    case "LoanOfficer":

                        sbInternal = new StringBuilder();

                        sbInternal.Append("<div class=\"sublinks\">");
                        sbInternal.Append("<ul>");
                        sbInternal.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">Pending Extraction ()</a></li>", ConfigurationManager.AppSettings["PendingExtractionReportUrl"]));
                        sbInternal.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">Pending Processing ()</a></li>", ConfigurationManager.AppSettings["PendingProcessingReportUrl"]));
                        sbInternal.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">Credit Review ()</a></li>", ConfigurationManager.AppSettings["CreditReviewReportUrl"]));
                        sbInternal.Append("</ul>");
                        sbInternal.Append("</div>");

                        result = sbInternal.ToString();

                        break;

                    case "Customer":

                        var sbInternal1 = new StringBuilder();

                        sbInternal1.Append("<div class=\"sublinks\">");
                        sbInternal1.Append("<ul>");
                        sbInternal1.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">Pending Extraction ()</a></li>", ConfigurationManager.AppSettings["PendingExtractionReportUrl"]));
                        sbInternal1.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">Pending Extraction ()</a></li>", ConfigurationManager.AppSettings["PendingExtractionReportUrl"]));
                        sbInternal1.Append(string.Format("<li><a href=\"{0}\" target=\"_blank\">Pending Extraction ()</a></li>", ConfigurationManager.AppSettings["PendingExtractionReportUrl"]));
                        sbInternal1.Append("</ul>");
                        sbInternal1.Append("</div>");

                        result = sbInternal1.ToString();

                        break;

                }
            }

            return result;
        }

        public static string IsActive(this HtmlHelper html, string control, string action)
        {
            var routeData = html.ViewContext.RouteData;

            var routeAction = (string)routeData.Values["action"];
            var routeControl = (string)routeData.Values["controller"];

            // both must match
            var returnActive = control == routeControl &&
                               action == routeAction;

            return returnActive ? "active" : "";
        }

        public static string DatePicker(this HtmlHelper helper, string name, object date)
        {
            StringBuilder html = new StringBuilder();

            // Build our base input element   
            html.Append("<input type=\"text\" id=\"" + name + "\" name=\"" + name + "\"");

            // Model Binding Support    
            if (date != null)
            {
                string dateValue = String.Empty;
                if (date is DateTime? && ((DateTime)date) != DateTime.MinValue)
                    dateValue = ((DateTime)date).ToShortDateString();
                else if (date is DateTime && (DateTime)date != DateTime.MinValue)
                    dateValue = ((DateTime)date).ToShortDateString();
                else if (date is string)
                    dateValue = (string)date;
                html.Append(" value=\"" + dateValue + "\"");
            }

            // We're hard-coding the width here, a better option would be to pass in html attributes and reflect through them    
            // here ( default to 100px width if no style attributes ) 
            html.Append(" style=\"width: 100px;\" />");

            // Now we call the datepicker function, passing in our options.  Again, a future enhancement would be to   
            // pass in date options as a list of attributes ( min dates, day/month/year formats, etc. )    
            // html.Append("<script type=\"text/javascript\">$(document).ready(function() { $('#" + name + "').datepicker({dateFormat: 'dd/mm/yy'}); });</script>");
            html.Append("<script type=\"text/javascript\">$(document).ready(function() { $('#" + name + "').datepicker({ dateFormat: 'dd/mm/yy' }); });</script>");

            return html.ToString();
        }

        public static string DateTimePicker(this HtmlHelper helper, string name, object date)
        {
            StringBuilder html = new StringBuilder();

            // Build our base input element   
            html.Append("<input type=\"text\" id=\"" + name + "\" name=\"" + name + "\"");

            // Model Binding Support    
            if (date != null)
            {
                string dateValue = String.Empty;
                if (date is DateTime? && ((DateTime)date) != DateTime.MinValue)
                    dateValue = ((DateTime)date).ToShortDateString();
                else if (date is DateTime && (DateTime)date != DateTime.MinValue)
                    dateValue = ((DateTime)date).ToShortDateString();
                else if (date is string)
                    dateValue = (string)date;
                html.Append(" value=\"" + dateValue + "\"");
            }

            // We're hard-coding the width here, a better option would be to pass in html attributes and reflect through them    
            // here ( default to 100px width if no style attributes ) 
            html.Append(" style=\"width: 100px;\" />");

            // Now we call the datepicker function, passing in our options.  Again, a future enhancement would be to   
            // pass in date options as a list of attributes ( min dates, day/month/year formats, etc. )    
            // html.Append("<script type=\"text/javascript\">$(document).ready(function() { $('#" + name + "').datepicker({dateFormat: 'dd/mm/yy'}); });</script>");
            html.Append("<script type=\"text/javascript\">$(document).ready(function() { $('#" + name + "').datetimepicker({ dateFormat: 'dd/mm/yy' }); });</script>");

            return html.ToString();
        }

        public static string TimePicker(this HtmlHelper helper, string name, object date)
        {
            StringBuilder html = new StringBuilder();

            // Build our base input element   
            html.Append("<input type=\"text\" id=\"" + name + "\" name=\"" + name + "\"");

            // Model Binding Support    
            if (date != null)
            {
                string dateValue = String.Empty;
                if (date is DateTime? && ((DateTime)date) != DateTime.MinValue)
                    dateValue = ((DateTime)date).ToShortDateString();
                else if (date is DateTime && (DateTime)date != DateTime.MinValue)
                    dateValue = ((DateTime)date).ToShortDateString();
                else if (date is string)
                    dateValue = (string)date;
                html.Append(" value=\"" + dateValue + "\"");
            }

            // We're hard-coding the width here, a better option would be to pass in html attributes and reflect through them    
            // here ( default to 100px width if no style attributes ) 
            html.Append(" style=\"width: 100px;\" />");

            // Now we call the datepicker function, passing in our options.  Again, a future enhancement would be to   
            // pass in date options as a list of attributes ( min dates, day/month/year formats, etc. )    
            // html.Append("<script type=\"text/javascript\">$(document).ready(function() { $('#" + name + "').datepicker({dateFormat: 'dd/mm/yy'}); });</script>");
            html.Append("<script type=\"text/javascript\">$(document).ready(function() { $('#" + name + "').timepicker({ dateFormat: 'dd/mm/yy' }); });</script>");

            return html.ToString();
        }

        public static string DatePicker(this HtmlHelper helper, string name, string imageUrl, int? tabIndex, object date)
        {
            StringBuilder html = new StringBuilder();

            // Build our base input element   
            if (tabIndex.HasValue)
                html.Append("<input type=\"text\" id=\"" + name + "\" name=\"" + name + "\"" + "\" tabindex=\"" + tabIndex.Value + "\"");
            else
                html.Append("<input type=\"text\" id=\"" + name + "\" name=\"" + name + "\"");

            // Model Binding Support    
            if (date != null)
            {
                string dateValue = String.Empty;
                if (date is DateTime? && ((DateTime)date) != DateTime.MinValue)
                    dateValue = ((DateTime)date).ToShortDateString();
                else if (date is DateTime && (DateTime)date != DateTime.MinValue)
                    dateValue = ((DateTime)date).ToShortDateString();
                else if (date is string)
                    dateValue = (string)date;
                html.Append(" value=\"" + dateValue + "\"");
            }

            // We're hard-coding the width here, a better option would be to pass in html attributes and reflect through them    
            // here ( default to 100px width if no style attributes ) 
            // TODO - sri - set class here
            html.Append(" style=\"width: 100px;\" />");

            // Now we call the datepicker function, passing in our options.  Again, a future enhancement would be to   
            // pass in date options as a list of attributes ( min dates, day/month/year formats, etc. )    
            html.Append("<script type=\"text/javascript\">$(document).ready(function() { $('#" + name + "').datepicker({changeMonth: true, changeYear: true, showButtonPanel: true, showOn: 'button', buttonImageOnly: true, buttonImage: '" + imageUrl + "', duration: 0 }); });</script>");

            return html.ToString();
        }

        public static string CheckBoxList(this HtmlHelper htmlHelper, string name, List<CheckBoxListInfo> listInfo)
        {
            return htmlHelper.CheckBoxList(name, listInfo, ((IDictionary<string, object>)null));
        }

        public static string CheckBoxList(this HtmlHelper htmlHelper, string name, List<CheckBoxListInfo> listInfo, object htmlAttributes)
        {
            return htmlHelper.CheckBoxList(name, listInfo, ((IDictionary<string, object>)new RouteValueDictionary(htmlAttributes)));
        }

        public static string CheckBoxList(this HtmlHelper htmlHelper, string name, List<CheckBoxListInfo> listInfo, IDictionary<string, object> htmlAttributes)
        {            
            if (listInfo.Count < 1)
                throw new ArgumentException("The list must contain at least one value", "listInfo");

            StringBuilder sb = new StringBuilder();

            foreach (CheckBoxListInfo info in listInfo)
            {
                TagBuilder builder = new TagBuilder("input");
                if (info.IsChecked) builder.MergeAttribute("checked", "checked");
                builder.MergeAttributes<string, object>(htmlAttributes);
                builder.MergeAttribute("type", "checkbox");
                builder.MergeAttribute("value", info.Value);
                builder.MergeAttribute("name", name);
                builder.InnerHtml = info.DisplayText;
                sb.Append(builder.ToString(TagRenderMode.Normal));
                sb.Append("<br />");
            }

            return sb.ToString();
        }

        /// <summary>
        /// Returns the ViewBag of the parent page. Allows a child view to access the parent view's ViewBag object. A child view may then set a property accessible by the parent page.
        /// </summary>
        /// <returns>ViewBag</returns>
        public static dynamic GetPageViewBag(this HtmlHelper html)
        {
            if (html == null || html.ViewContext == null) //this means that the page is root or parial view
            {
                return html.ViewBag;
            }

            ControllerBase controller = html.ViewContext.Controller;

            while (controller.ControllerContext.IsChildAction)  //traverse hierachy to get root controller
            {
                controller = controller.ControllerContext.ParentActionViewContext.Controller;
            }

            return controller.ViewBag;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Entity.DataModels
{
    [Serializable]
    public class RelationshipManager : BaseEntity
    {
        [Display(Name="First Name")]
        [Required]
        [MaxLength(256)]
        public string FirstName { get; set; }

        [Display(Name="Last Name")]
        [Required]
        [MaxLength(256)]
        public string LastName { get; set; }

        public string RMFullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        
        public string FullName { get; set; }
        [Display(Name="Id Number")]
        [Index("IdNumber", IsUnique = false)]
        [Column(TypeName = "VARCHAR")]
        public string IDNumber { get; set; }

        [Display(Name="Email Address")]        
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }
        
        [Display(Name="Mobile Number")]
        [Index("MobileNumber", IsUnique = false)]
        [Column(TypeName = "VARCHAR")]
        public string MobileNumber { get; set; }

        [Display(Name="Branch")]
        [Index]
        public Guid BranchId { get; set; }

        public virtual Branch Branch { get; set; }

        [NotMapped]
        public string Commands { get; set; }

        [Display(Name ="Is Enabled?")]
        public bool IsEnabled { get; set; }        
    }
}

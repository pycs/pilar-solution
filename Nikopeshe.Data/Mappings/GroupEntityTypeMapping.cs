﻿using Nikopeshe.Entity.DataModels;
using System.Data.Entity.ModelConfiguration;

namespace Nikopeshe.Data.Mappings
{
    public class GroupEntityTypeMapping : EntityTypeConfiguration<Group>
    {
        public GroupEntityTypeMapping()
        {
            this.HasKey(x => x.Id);
            this.ToTable(string.Format("{0}Groups", DefaultSettings.Instance.TablePrefix));
        }
    }
}

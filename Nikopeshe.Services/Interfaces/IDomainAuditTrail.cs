﻿using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface IDomainAuditTrail 
    {
        IList<DomainAudit> GetAllDomainAudotTrail();

        PageCollectionInfo<DomainAudit> FindDomainAuditFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);
    }
}

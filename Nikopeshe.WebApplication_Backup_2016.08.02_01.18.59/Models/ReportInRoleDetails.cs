﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nikopeshe.WebApplication.Models
{
    public class ReportInRoleDetails
    {
        public Guid ReportId { get; set; }

        public string ReportName { get; set; }

        public string FolderName { get; set; }

        public bool IsEnabled { get; set; }

        public string Actions { get; set; }
    }
}
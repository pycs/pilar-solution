﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.Repayments
{
    public class Repayment : IRepayment
    {
        DataContext dataContext = new DataContext();

        public IList<LoanRequest> GetLoanRequestsDueToday()
        {
            var dueLoanTodayList = new List<LoanRequest>();

            var findLoansDueToday = dataContext.LoanRequest.Where(
                    x => (x.NextPaymentDate >= DateTime.Today
                    && x.Status == (int)Enumerations.LoanStatus.Active)).ToList();

            if (findLoansDueToday != null && findLoansDueToday.Any())
            {
                foreach (var loan in findLoansDueToday)
                {
                    var findCustomerLoanAccount = dataContext.CustomersAccount.Where(c => c.Id == loan.CustomerAccountId).FirstOrDefault();
                    var findCustomerSavingsAccountId = dataContext.CustomersAccount.Where(x => x.CustomerId == findCustomerLoanAccount.CustomerId && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();

                    if (findCustomerSavingsAccountId != null)
                    {
                        var idStartDate = new SqlParameter { ParameterName = "@CustomerAccountID", Value = findCustomerSavingsAccountId.Id };

                        var results = dataContext.Database.SqlQuery<CustomerAccountInfo>("sp_Customer_Savings_Loan_AccountBalance @CustomerAccountID", idStartDate).Cast<CustomerAccountInfo>().FirstOrDefault();

                        if (results.AccountBalance > 0m)
                        {
                            dueLoanTodayList.Add(new LoanRequest
                            {
                                Id = loan.Id
                            });
                        }
                    }
                }
            }
            
            return dueLoanTodayList;
        }

        public LoanRequest FindLoanRequestById(Guid id)
        {
            if (id == Guid.Empty)
                throw new ArgumentNullException("id cannot be null");

            var findLoanRequestById = dataContext.LoanRequest.Find(id);

            return findLoanRequestById;
        }

        public IList<LoanRequest> FindActiveMicroLoanRequest()
        {
            var findMicroLoans = new List<LoanRequest>();

            var findMicroLoan = dataContext.LoanProduct.Where(x => x.IsMicroLoan == true).Select(x => x.Id).FirstOrDefault();

            if (findMicroLoan != null && findMicroLoan != Guid.Empty)
            {
                var findActiveMicroLoans = dataContext.LoanRequest.Where(x => x.LoanProductId == findMicroLoan && x.Status == (int)Enumerations.LoanStatus.Active).ToList();

               if (findActiveMicroLoans != null && findActiveMicroLoans.Any())
                {
                    foreach (var microLoan in findActiveMicroLoans)
                    {
                        var findCustomerLoanAccount = dataContext.CustomersAccount.Where(c => c.Id == microLoan.CustomerAccountId).FirstOrDefault();
                        var findCustomerSavingsAccountId = dataContext.CustomersAccount.Where(x => x.CustomerId == findCustomerLoanAccount.CustomerId && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();

                        if (findCustomerSavingsAccountId != null)
                        {
                            var idStartDate = new SqlParameter { ParameterName = "@CustomerAccountID", Value = findCustomerSavingsAccountId.Id };

                            var results = dataContext.Database.SqlQuery<CustomerAccountInfo>("sp_Customer_Savings_Loan_AccountBalance @CustomerAccountID", idStartDate).Cast<CustomerAccountInfo>().FirstOrDefault();

                            if (results.AccountBalance > 0m)
                            {
                                findMicroLoans.Add(new LoanRequest 
                                { 
                                    Id = microLoan.Id
                                });
                            }
                        }
                    }
                }
            }

            return findMicroLoans;
        }

        public Tuple<decimal, decimal> GetTotalExpectedPaidAmountFromDisbursementDateToNextPaymentDate(Guid loanRequestId)
        {
            var findLoanRequestById = dataContext.LoanRequest.Find(loanRequestId);

            decimal interestAmount = 0m;

            decimal principalAmount = 0m;

            if (findLoanRequestById != null)
            {
                var findLoanProduct = dataContext.LoanProduct.Find(findLoanRequestById.LoanProductId);

                var timeDifference = (findLoanRequestById.NextPaymentDate.Value - findLoanRequestById.DisbursementDate.Value).Days;

                if (findLoanProduct.PaymentFrequencyType == Enumerations.PaymentFrequency.Weekly)
                {
                    var x = timeDifference / 7;

                    interestAmount = (findLoanRequestById.InterestAmount / findLoanRequestById.NegotiatedInstallments) * x;

                    principalAmount = (findLoanRequestById.LoanAmount / findLoanRequestById.NegotiatedInstallments) * x;
                }
                else
                {
                    var y = timeDifference / 30;

                    interestAmount = (findLoanRequestById.InterestAmount / findLoanRequestById.NegotiatedInstallments) * y;

                    principalAmount = (findLoanRequestById.LoanAmount / findLoanRequestById.NegotiatedInstallments) * y;
                }

            }
            return new Tuple<decimal, decimal>(principalAmount, interestAmount);
        }
    }
}

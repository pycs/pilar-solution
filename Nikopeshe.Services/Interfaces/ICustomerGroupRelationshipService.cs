﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;

namespace Nikopeshe.Services.Interfaces
{
    public interface ICustomerGroupRelationshipService
    {
        void AddNewCustomerGroupRelationship(CustomerGroupRelationship customerGroup, ServiceHeader serviceHeader);

        void UpdateCustomerGroupRelationship(CustomerGroupRelationship customerGroup, ServiceHeader serviceHeader);

        void DeleteCustomerGroupRelationship(Guid Id, ServiceHeader serviceHeader);

        CustomerGroupRelationship FindCustomerGroupRelationshipById(Guid Id);

        IList<CustomerGroupRelationship> GetCustomerGroupRelationshipByGroupId(Guid GroupId);        
    }
}

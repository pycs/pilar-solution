﻿using Nikopeshe.Data.Mappings;
using System;
using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Security;

namespace Nikopeshe.WebApplication.Attributes
{
    public class ForcePasswordChangeAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException("filterContext");

            IPrincipal user = filterContext.HttpContext.User;

            if (user != null && user.Identity.IsAuthenticated)
            {
                MembershipUser membershipUser = Membership.GetUser();

                DefaultSettings.Instance.RootUser = membershipUser.UserName;
                // has the user ever changed their password?
                if (membershipUser != null && membershipUser.LastPasswordChangedDate == membershipUser.CreationDate && membershipUser.UserName != DefaultSettings.Instance.RootUser)
                {
                    UrlHelper urlHelper = new UrlHelper(filterContext.RequestContext);

                    filterContext.Result = new RedirectResult(urlHelper.Action("ChangePassword", "Account"));
                }               
            }
        }
    }
}
﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.CustomerService
{
    public interface ICustomerService
    {
        Customer FindCustomerByMobileNumber(string MobileNumber);

        Customer FindCustomerByIdNumber(string IdNumber);

        decimal CustomerLoanBalance(Guid customerId);
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{    
    public class LoanProductsCharge : BaseEntity
    {
        [Index]
        [Display(Name="Loan Product")]
        public Guid LoanProductId { get; set; }

        public virtual LoanProduct LoanProduct { get; set; }

        [Index]
        [Display(Name = "Charge Name")]
        public Guid ChargeId { get; set; }

        public virtual Charge Charges { get; set; }

        [Display(Name = "Charge Name")]
        [Index]
        public bool IsEnabled { get; set; }

        [NotMapped]
        public string Commands { get; set; }       
    }
}

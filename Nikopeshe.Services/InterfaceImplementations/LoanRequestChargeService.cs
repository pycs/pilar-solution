﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class LoanRequestChargeService : ILoanRequestChargeService
    {
        private readonly IRepository<LoanRequestCharge> loanrequestchargerepository;

        private string ErrorMessage = string.Empty;

        public LoanRequestChargeService(IRepository<LoanRequestCharge> _loanrequestchargerepository)
        {
            if (_loanrequestchargerepository == null)
                throw new ArgumentNullException("null service");

            this.loanrequestchargerepository = _loanrequestchargerepository;
        }

        public void AddLoanRequestCharge(LoanRequestCharge loanrequestcharge, ServiceHeader serviceHeader)
        {
            if (loanrequestcharge == null)
                throw new ArgumentNullException("loanrequestcharge cannot be null");

            try
            {
                loanrequestchargerepository.Insert(loanrequestcharge, serviceHeader);
            }

            catch(DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                           validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);                
            }
        }

        public IList<LoanRequestCharge> FindAllLoanRequestChargesByLoanRequestId(Guid LoanRequestId)
        {
            if (LoanRequestId == Guid.Empty)
                throw new ArgumentNullException("Guid cannot be null.");

            var findloanrequestchargebyloanrequestid = loanrequestchargerepository.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.LoanRequestId == LoanRequestId);

            return findloanrequestchargebyloanrequestid.ToList();
        }
    }
}

﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Data.Mappings;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using Nikopeshe.WebApplication.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Text;
using MvcContrib.Pagination;
using System.Web.Mvc;
using System.Web.Security;

namespace Nikopeshe.WebApplication.Controllers
{
    [ExcludeFilter(typeof(CustomerApproveAccountAtribute))]
    [ExcludeFilter(typeof(ForcePasswordChangeAttribute))]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class AccountController : BaseController
    {
        internal readonly IEmailAlertService emailalerservice;
        internal readonly ITextAlertService textalertservice;
        private readonly IPasswordRecoveryRequestService passwordRecoveryRequest;
        internal readonly ICustomerService customerservice;
        internal readonly ICustomerAccountService customeraccountservice;
        internal readonly IUserInfoService userinfoservice;
        internal readonly IBranchService branchservice;
        private string TextSignature = ConfigurationManager.AppSettings["TextSignature"].ToString();
        private string ApplicationName = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public AccountController(IEmailAlertService _emailalerservice, ITextAlertService _textalertservice, 
            ICustomerAccountService _customeraccountservice, ICustomerService _customerservice,
            IUserInfoService _userinfoservice, IBranchService _branchservice, 
            IPasswordRecoveryRequestService _passwordRecoveryRequest)
        {
            if(_emailalerservice == null || _textalertservice == null || _customeraccountservice == null || _customerservice == null || _userinfoservice == null || _branchservice == null)
                throw new ArgumentNullException("null service reference");

            this.emailalerservice = _emailalerservice;
            this.userinfoservice = _userinfoservice;
            this.customeraccountservice = _customeraccountservice;
            this.customerservice = _customerservice;
            this.textalertservice = _textalertservice;
            this.branchservice = _branchservice;
            this.passwordRecoveryRequest = _passwordRecoveryRequest;
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {        
            return View();
        }

        public JsonResult GetAllUsers(JQueryDataTablesModel datatablemodel)
        {                       
            var pagecollection = userinfoservice.GetAllUsers();

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var usersList = GetAllMembers(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, MembersList: pagecollection);


            return Json(new JQueryDataTablesResponse<UserInfo>(items: usersList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));            
        }

        [HttpGet]
        public ActionResult createaccount()
        {
            ViewBag.IncomeRange = IncomeRangeSelectList();

            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();

            ViewBag.GenderCheckList = genderInfoCheckList();

            var customer = new Customer
            {
                DateofBirth = DateTime.Now
            };
            return View(customer);  
        }

        [HttpPost]        
        public ActionResult createaccount(string DateofBirth, [Bind(Exclude = "DateofBirth")]Customer customer)
        {
            ViewBag.IncomeRange = IncomeRangeSelectList();

            ViewBag.MaritalStatusCheckList = maritalStatusInfoCheckList();

            ViewBag.GenderCheckList = genderInfoCheckList();
            MembershipCreateStatus status;
            customer.IsEnabled = true;
            var finddefaultbranch = branchservice.FindBranchByBranchCode("000");
            if (finddefaultbranch == null)
            {
                ModelState.AddModelError("", "An error occured while creating account. Please contact system administrator for assistance.");
                return View(customer);
            }
            string mobilenumber = customer.MobileNumber;
            customer.IsApproved = false;
            customer.CustomerRegistrationType = Enumerations.CustomerRegistrationType.SelfRegistered;
            if (string.IsNullOrWhiteSpace(DateofBirth))
            {
                ModelState.AddModelError(string.Empty, "Date of Birth is required.");
                return View(customer);
            }
            DateTime dDate;

            if (DateTime.TryParseExact(DateofBirth, "dd/MM/yyyy", null, DateTimeStyles.None, out dDate) == true)
            {
                customer.DateofBirth = DateTime.ParseExact(DateofBirth, "dd/MM/yyyy", null);
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Invalid Date of Birth format.");
                return View(customer);
            }
            
            customer.BranchId = finddefaultbranch.Id;
            customer.CustomerType = Enumerations.CustomerTypes.Individual;
            customer.RegisteredBy = customer.MobileNumber;
            //customer.MobileNumber = mobilenumber.Substring(1);
            customer.ApprovalCode = GenerateRandomCustomerApprovalCode(6);
            bool checkduplicatecustomer;
            bool iscustomerunderage;
            string nextcustomernumber = customerservice.NextCustomerNumber();
            customer.CustomerNumber = nextcustomernumber;            
            string nextcustomeraccountnumber = customerservice.NextCustomerSavingAccountNumber(nextcustomernumber);
            CustomersAccount customersaccount = new CustomersAccount();
            if (ModelState.IsValid)
            {

                customersaccount.CustomerId = customer.Id;
                customersaccount.AccountType = (int)Enumerations.ProductCode.Savings;
                customersaccount.AccountNumber = nextcustomeraccountnumber;
                customersaccount.IsActive = true;
                checkduplicatecustomer = customerservice.CheckDuplicateCustomer(customer.MobileNumber, customer.IDNumber);
                iscustomerunderage = customerservice.ValidateCustomerAge(customer.DateofBirth);
                try
                {
                    if (checkduplicatecustomer)
                    {
                        ModelState.AddModelError(string.Empty, string.Format("Customer with Mobile Number {0} or Id Number {1} already exists", customer.MobileNumber, customer.IDNumber));

                        return View(customer);
                    }
                    else if (!iscustomerunderage)
                    {
                        ModelState.AddModelError(string.Empty, string.Format("You must be 18 years and above."));

                        return View(customer);
                    }
                    else if (string.IsNullOrWhiteSpace(string.Concat(customer.PIN)))
                    {
                        ModelState.AddModelError(string.Empty, string.Format("PIN field cannot be null."));

                        return View(customer);
                    }

                    else if (customer.IncomeRange.Equals("Select Income Range"))
                    {
                        ModelState.AddModelError(string.Empty, string.Format("Income range is required."));

                        return View(customer);
                    }

                    else if ((int)customer.MaritalStatus == 0 || (int)customer.Gender == 0)
                    {
                        ModelState.AddModelError(string.Empty, "Marital Status or Gender cannot be null.");
                        return View(customer);
                    }
                    
                    var findmembershipuser = Membership.GetUser(customer.MobileNumber);
                    if (checkduplicatecustomer == false && findmembershipuser == null)
                    {
                        using (var datacontext = new DataContext())
                        {
                            using (var transaction = datacontext.Database.BeginTransaction())
                            {
                                try
                                {

                                    MembershipUser createuser = Membership.CreateUser(customer.MobileNumber, customer.PIN, customer.EmailAddress, DefaultSettings.Instance.PasswordQuestion, DefaultSettings.Instance.PasswordAnswer, true, out status);
                                    string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                                    string loginurl = "<a href='" + mvcclientendposint + "'/Account/Login> click here</a>";
                                    switch (status)
                                    {
                                        case MembershipCreateStatus.Success:
                                            ModelState.AddModelError(string.Empty, "User Account created successfully");
                                            Roles.AddUserToRole(customer.MobileNumber, "Customer");
                                            customerservice.AddNewCustomer(customer, serviceHeader);
                                            customeraccountservice.AddNewCustomersAccount(customersaccount, serviceHeader);
                                            //Create email alert and insert in the database
                                            StringBuilder bodyBuilder = new StringBuilder();
                                            bodyBuilder.Append("Dear Sir/Madam,");
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.Append(string.Format("Welcome to {0}. Below you will find your login details.", DefaultSettings.Instance.ApplicationDisplayName));
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.Append("Username: " + customer.MobileNumber);
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.Append("Password: " + customer.PIN);
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.Append("Authorization Code : " + customer.ApprovalCode);
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.Append(string.Format("Continue to approve your account and borrow {0}.", loginurl));
                                            bodyBuilder.AppendLine("<br />");
                                            bodyBuilder.AppendLine("<br />");

                                            var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
                                            bodyBuilder.Append(emailSignature);

                                            var emailAlertDTO = new EmailAlert
                                            {
                                                MailMessageFrom = DefaultSettings.Instance.Email,
                                                MailMessageTo = string.Format("{0}", customer.EmailAddress),
                                                MailMessageSubject = string.Format("Login Details - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                                MailMessageBody = string.Format("{0}", bodyBuilder),
                                                MailMessageIsBodyHtml = true,
                                                MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                MailMessageSecurityCritical = false,
                                                CreatedBy = "System",
                                                MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                            };

                                            emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                                            NewTextAlert(customer.FullName, customer.MobileNumber, customer.MobileNumber, customer.PIN, customer.ApprovalCode);
                                            TempData["Message"] = "Account created successfully. An email/text message has been sent to you with your approval code. Provide the code after login to approve your account.";

                                            break;
                                        case MembershipCreateStatus.DuplicateUserName:
                                            ModelState.AddModelError(string.Empty, "There already exists a user with this username.");
                                            break;

                                        case MembershipCreateStatus.DuplicateEmail:
                                            ModelState.AddModelError(string.Empty, "There already exists a user with this email address.");
                                            break;
                                        case MembershipCreateStatus.InvalidEmail:
                                            ModelState.AddModelError(string.Empty, "The email address you provided in invalid.");
                                            break;
                                        case MembershipCreateStatus.InvalidAnswer:
                                            ModelState.AddModelError(string.Empty, "The security answer is invalid.");
                                            break;
                                        case MembershipCreateStatus.InvalidPassword:
                                            ModelState.AddModelError(string.Empty, "The password you provided is invalid. It must be seven characters long and have at least one non-alphanumeric character.");

                                            break;
                                        default:
                                            ModelState.AddModelError(string.Empty, status.ToString());
                                            break;
                                    }
                                    transaction.Commit();
                                }

                                catch(Exception ex)
                                {
                                    Roles.RemoveUserFromRole(customer.MobileNumber, "Customer");
                                    Membership.DeleteUser(customer.MobileNumber);
                                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                                    transaction.Rollback();
                                }
                            }
                        }                        
                    }
                    
                    return RedirectToAction("Successful", "Account");
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(customer);
                }
            }
            return View(customer);
        }

        private void NewTextAlert(string RecipientName, string RecepientNumber, string Username, string Password, string AuthorizationCode)
        {
            //Create email alert and insert in the database
            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.Append(string.Format("Dear {0} ", RecipientName));
            bodyBuilder.Append(string.Format("Welcome to {0}. Your Username - {1}, Password - {2}, AuthorizationCode - {3} ", ApplicationName, Username, Password, AuthorizationCode));            
            bodyBuilder.Append(TextSignature);
            bodyBuilder.Append("Thank You.");
            var textalert = new TextAlert
            {
                TextMessageRecipient = string.Format("{0}", RecepientNumber),                
                TextMessageBody = string.Format("{0}", bodyBuilder),
                MaskedTextMessageBody = "",
                TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                TextMessageSecurityCritical = false,
                CreatedBy = User.Identity.Name,
                TextMessageSendRetry = 3
            };

            textalertservice.AddNewTextAlert(textalert, serviceHeader);
        }

        /// <summary>
        /// User account creation success page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Successful()
        {
            return View();
        }

        [HttpGet]
        [Authorize]
        public ActionResult ApproveCustomer()
        {
            return View();
        }

        [HttpPost]        
        public ActionResult ApproveCustomer(CustomerVerification customerverification)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = User.Identity.Name;
                    if(string.IsNullOrWhiteSpace(user))
                    {
                        ModelState.AddModelError(string.Empty, "Failed while retrieving current user information.");
                        return View(customerverification);
                    }
                    Customer findcustomerbymobilenumber = customerservice.FindCustomerByMobileNumber(user);

                    if (findcustomerbymobilenumber.ApprovalCode != customerverification.ApprovalCode)
                    {
                        ModelState.AddModelError(string.Empty, "Verification Code is incorrect, please try again.");
                        return View(customerverification);
                    }

                    else
                    {
                        findcustomerbymobilenumber.IsApproved = true;
                        customerservice.UpdateCustomer(findcustomerbymobilenumber, serviceHeader);

                        return RedirectToAction("Dashboard", "Customer");
                    }
                }
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
            return View(customerverification);
        }

        public ActionResult NewApprovalCodeSuccess()
        {
            return View();
        }

        [HttpGet]
        public ActionResult NewApprovalCode()
        {
            var findcustomerbymobilenumber = customerservice.FindCustomerByMobileNumber(User.Identity.Name);

            if (ModelState.IsValid)
            {
                try
                {
                    var authorizationcode = GenerateRandomCustomerApprovalCode(15);
                    findcustomerbymobilenumber.ApprovalCode = authorizationcode;
                    customerservice.UpdateCustomer(findcustomerbymobilenumber, serviceHeader);
                    StringBuilder bodyBuilder = new StringBuilder();
                    StringBuilder textstringbuilder = new StringBuilder();
                    bodyBuilder.Append("Dear Sir/Madam,");
                    bodyBuilder.AppendLine("<br />");
                    bodyBuilder.AppendLine("<br />");
                    bodyBuilder.Append("As per your earlier request, find you new authorization code");
                    bodyBuilder.AppendLine("<br />");
                    bodyBuilder.AppendLine("<br />");
                    bodyBuilder.Append("Authorization Code : " + authorizationcode);
                    bodyBuilder.AppendLine("<br />");
                    bodyBuilder.AppendLine("<br />");
                    bodyBuilder.Append("Continue to approve your account and login.");
                    bodyBuilder.AppendLine("<br />");
                    bodyBuilder.AppendLine("<br />");
                    textstringbuilder.Append("As per your earlier request, find you new authorization code");
                    textstringbuilder.Append("Authorization Code : " + authorizationcode);
                    textstringbuilder.Append(TextSignature);
                    var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
                    bodyBuilder.Append(emailSignature);

                    var emailAlertDTO = new EmailAlert
                    {
                        MailMessageFrom = DefaultSettings.Instance.Email,
                        MailMessageTo = string.Format("{0}", findcustomerbymobilenumber.EmailAddress),
                        MailMessageSubject = string.Format("Login Details - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                        MailMessageBody = string.Format("{0}", bodyBuilder),
                        MailMessageIsBodyHtml = true,
                        MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                        MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                        MailMessageSecurityCritical = false,
                        CreatedBy = "System",
                        MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                    };
                    emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);

                    var textalert = new TextAlert
                    {
                        TextMessageRecipient = string.Format("{0}", findcustomerbymobilenumber.MobileNumber),
                        TextMessageBody = string.Format("{0}", textstringbuilder),
                        MaskedTextMessageBody = "",
                        TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                        TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                        TextMessageSecurityCritical = false,
                        CreatedBy = User.Identity.Name,
                        TextMessageSendRetry = 3
                    };

                    textalertservice.AddNewTextAlert(textalert, serviceHeader);

                    return RedirectToAction("NewApprovalCodeSuccess");
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                }
            }
            return View();
        }

        //[Authorize(Roles = "Administrator")]
        //[HttpGet]        
        //public ActionResult EditUser(Guid id)
        //{
        //    if(id == Guid.Empty){
        //        throw new ArgumentNullException("id cannot be null");
        //    }
        //    var userInfo = new UserInfo();
        //    var finduserbyusername = userinfoservice.FindUserById(id);
        //    if (finduserbyusername != null)
        //    {

        //    }
        //    List<SelectListItem> list = new List<SelectListItem>();
        //    SelectListItem item;
        //    foreach (string role in Roles.GetAllRoles())
        //    {
        //        item = new SelectListItem { Text = role, Value = role };
        //        list.Add(item);
        //    }
        //    if (finduserbyusername != null)
        //    {
                
        //        ViewBag.Roles = (IEnumerable<SelectListItem>)list;

        //        ViewBag.Username = finduserbyusername.UserName;

        //    }

        //    return View(userInfo);
        //}

        //[HttpPost]
        //[ActionName("EditUser")]        
        //public ActionResult EditUser(UserInfo userinfo)
        //{
        //    List<SelectListItem> list = new List<SelectListItem>();
        //    SelectListItem item;
        //    foreach (string role in Roles.GetAllRoles())
        //    {
        //        item = new SelectListItem { Text = role, Value = role };
        //        list.Add(item);
        //    }
        //    ViewBag.Roles = (IEnumerable<SelectListItem>)list;

        //    ViewBag.Username = userinfo.UserName;

        //    if(ModelState.IsValid)
        //    {
        //        try
        //        {
        //            var finduser = userinfoservice.FindUserByUserName(userinfo.UserName);
        //            finduser.Email = userinfo.Email;
        //            finduser.FirstName = userinfo.FirstName;
        //            finduser.LastName = userinfo.LastName;
        //            finduser.IsLockedOut = userinfo.IsLockedOut;
        //            finduser.IsApproved = userinfo.IsApproved;
        //            userinfoservice.UpdateUser(finduser, serviceHeader);
        //        }

        //        catch(Exception ex)
        //        {
        //            LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
        //            return View();
        //        }
        //    }           
        //    return View("Index");
        //}

        [Authorize(Roles = "Administrator")]
        public ActionResult UnlockUser(string id)
        {
            MembershipUser user;
            user = Membership.GetUser(id);
            bool IsLockedOut = user.IsLockedOut;

            if (ModelState.IsValid)
            {
                try
                {
                    user.UnlockUser();
                    user.IsApproved = true;
                    Membership.UpdateUser(user);
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View();
                }
            }
            return View("MembershipList");
        }

         [Authorize(Roles = "Administrator")]
        public ActionResult LockUser(string id)
        {            
            MembershipUser user;
            user = Membership.GetUser(id);

            if(ModelState.IsValid)
            {
                try
                {
                    user.IsApproved = false;
                    Membership.UpdateUser(user);                    
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View();
                }
            }
            return View("MembershipList");
        }
      
        public static IList<UserInfo> GetAllMembers(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<UserInfo> MembersList)
        {
            var Members = MembersList;

            totalRecordCount = Members.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                Members = Members.Where(c => c.UserName
                    .ToLower().Contains(searchString.ToLower()) || c.FirstName.ToLower().Contains(searchString.ToLower())).ToList(); ;
            }

            searchRecordCount = Members.Count;

            IOrderedEnumerable<UserInfo> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "UserName":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.UserName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.UserName);
                        break;

                    case "Email":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.Email)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Email);
                        break;

                    case "FirstName":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.FirstName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.FirstName);
                        break;

                    case "LastName":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.LastName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LastName);
                        break;

                    case "Branch.BranchName":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Branch.BranchName);
                        break;

                    case "IsApproved":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.IsApproved)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsApproved);
                        break;

                    case "IsLockedOut":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.IsLockedOut)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsLockedOut);
                        break;                   
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

         [Authorize(Roles = "Administrator")]
        public ActionResult MembershipList()
        {
            var getRolesInList = GetUserRoleInfoList(false);

            return View(getRolesInList);
        }

        public JsonResult GetAllMembersList(JQueryDataTablesModel datatablemodel)
        {
            var memberslist = from MembershipUser user in Membership.GetAllUsers()
                                                //from role in Roles.GetRolesForUser(user.UserName)
                                                select user;

            IList<MembershipUser> pagecollection = memberslist.ToList();

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var usersList = GetAllMemberInList(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, MembersList: pagecollection);


            return Json(new JQueryDataTablesResponse<MembershipUser>(items: usersList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<MembershipUser> GetAllMemberInList(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<MembershipUser> MembersList)
        {
            var Members = MembersList;

            totalRecordCount = Members.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                Members = Members.Where(c => c.UserName
                    .ToLower().Contains(searchString.ToLower()) || c.UserName.ToLower().Contains(searchString.ToLower())).ToList(); ;
            }

            searchRecordCount = Members.Count;

            IOrderedEnumerable<MembershipUser> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreationDate":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.CreationDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreationDate);
                        break;

                    case "UserName":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.UserName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.UserName);
                        break;

                    case "Email":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.Email)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Email);
                        break;

                    case "LastActivityDate":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.LastActivityDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LastActivityDate);
                        break;

                    case "LastLoginDate":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.LastLoginDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LastLoginDate);
                        break;
                   
                    case "IsOnline":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.IsOnline)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsOnline);
                        break;

                    case "IsApproved":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.IsApproved)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsApproved);
                        break;


                    case "IsLockedOut":
                        sortedClients = sortedClients == null ? Members.CustomSort(sortedColumn.Direction, cust => cust.IsLockedOut)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsLockedOut);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] /*http://stackoverflow.com/questions/14970102/anti-forgery-token-is-meant-for-user-but-the-current-user-is-username*/
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]        
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                    //LoggerFactory.CreateLog().LogError("Login", "Testing");
                    FormsAuthentication.SetAuthCookie(model.UserName, true);

                    var getroleforuser = Roles.GetRolesForUser(model.UserName);

                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }

                    else if (getroleforuser[0].Equals("Customer"))
                    {
                        return RedirectToAction("Dashboard", "Customer");
                    }
                    else
                    {
                        return RedirectToAction("Dashboard", "Dashboard");
                    }                                     
                }                             
                else
                {
                    ViewBag.Message = "Invalid username or password.";
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        [Authorize(Roles = "Administrator")]
        public ActionResult Register()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            SelectListItem item;
            foreach (string role in Roles.GetAllRoles())
            {
                item = new SelectListItem { Text = role, Value = role };
                list.Add(item);
            }
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            ViewBag.Roles = (IEnumerable<SelectListItem>)list;
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]        
        public ActionResult Register(UserInfo model)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            SelectListItem item;
            foreach (string role in Roles.GetAllRoles())
            {
                item = new SelectListItem { Text = role, Value = role };
                list.Add(item);
            }

            ViewBag.Roles = (IEnumerable<SelectListItem>)list;
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            MembershipCreateStatus status;
            if (ModelState.IsValid)
            {
                try
                {
                    string password = Membership.GeneratePassword(8, 2);

                    MembershipUser createuser = Membership.CreateUser(model.UserName, password, model.Email, "Where were you when you first heard about 9/11?", "School", true, out status);
                    if (status == MembershipCreateStatus.Success)
                    {
                        userinfoservice.AddNewUser(model, serviceHeader);

                        Roles.AddUserToRole(model.UserName, model.Roles);

                        //Create email alert and insert in the database
                        string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                        string loginurl = "<a href='" + mvcclientendposint + "'/Account/Login> here</a>";
                        StringBuilder bodyBuilder = new StringBuilder();
                        bodyBuilder.Append("Dear Sir/Madam,");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.Append(string.Format("Welcome to {0}. Below you will find your login details.", DefaultSettings.Instance.ApplicationDisplayName));
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.Append("Username: " + model.UserName);
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.Append("Password: " + password);
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.Append(string.Format("Please be sure to login and change your password {0}.", loginurl));
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.Append("");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.AppendLine("<br />");

                        var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
                        bodyBuilder.Append(emailSignature);                        
                        var emailAlertDTO = new EmailAlert
                        {
                            MailMessageFrom = DefaultSettings.Instance.Email,
                            MailMessageTo = string.Format("{0}", model.Email),
                            MailMessageSubject = string.Format("Login Details - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                            MailMessageBody = string.Format("{0}", bodyBuilder),
                            MailMessageIsBodyHtml = true,
                            MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                            MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                            MailMessageSecurityCritical = false,
                            CreatedBy = "System",
                            MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                        };

                        emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                    }

                    switch (status)
                    {
                        case MembershipCreateStatus.Success:
                            ModelState.AddModelError(string.Empty, "User Account created successfully");
                            break;
                        case MembershipCreateStatus.DuplicateUserName:
                            ModelState.AddModelError(string.Empty, "There already exists a user with this username.");
                            break;

                        case MembershipCreateStatus.DuplicateEmail:
                            ModelState.AddModelError(string.Empty, "There already exists a user with this email address.");
                            break;
                        case MembershipCreateStatus.InvalidEmail:
                            ModelState.AddModelError(string.Empty, "The email address you provided in invalid.");
                            break;
                        case MembershipCreateStatus.InvalidAnswer:
                            ModelState.AddModelError(string.Empty, "There security answer was invalid.");
                            break;
                        case MembershipCreateStatus.InvalidPassword:
                            ModelState.AddModelError(string.Empty, "The password you provided is invalid. It must be seven characters long and have at least one non-alphanumeric character.");

                            break;
                        default:
                            ModelState.AddModelError(string.Empty, status.ToString());
                            break;
                    }
                }

                catch (MemberAccessException ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(model);
                }
                 
            return RedirectToAction("membershiplist", "Account");
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult ViewUser(string id)
        {            
            var finduserbyusername = userinfoservice.FindUserByUserName(id);

            var rolename = Roles.GetRolesForUser(id);

            ViewBag.RoleName = rolename[0];

            ViewBag.Username = id;

            return View(finduserbyusername);
        }
       
        /// <summary>
        /// Loan reference number generator
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private string GenerateRandomPassword(int length)
        {
            string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()_+";
            char[] chars = new char[length];
            Random rd = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();            
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public ActionResult ForgotPassword()
        {            
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(PasswordRecoveryRequest recoveryrequest)
        {
            string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();

            if (ModelState.IsValid)
            {
                try
                {
                    var findmember = Membership.GetUser(recoveryrequest.UserName, false);
                    if (findmember != null)
                    {
                        Guid Id = Guid.NewGuid();
                        var passwordRecovery = new PasswordRecoveryRequest()
                        {
                            Id = Id,
                            UserName = recoveryrequest.UserName,
                            IsVerified = false
                        };

                        passwordRecoveryRequest.AddNewPasswordRecoveryRequest(passwordRecovery, serviceHeader);
                        
                        string loginurl = "<a href=" + mvcclientendposint + "/account/resetpassword/" + Id + "> click here</a>";
                        StringBuilder bodyBuilder = new StringBuilder();
                        bodyBuilder.Append("Dear Sir/Madam");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.Append("Please continue your password recovery process by "+ loginurl +"");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.AppendLine("<br />");

                        var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
                        bodyBuilder.Append(emailSignature);

                        var emailAlertDTO = new EmailAlert
                        {
                            MailMessageFrom = DefaultSettings.Instance.Email,
                            MailMessageTo = string.Format("{0}", findmember.Email),
                            MailMessageSubject = string.Format("Password Recovery Request - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                            MailMessageBody = string.Format("{0}", bodyBuilder),
                            MailMessageIsBodyHtml = true,
                            MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                            MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                            MailMessageSecurityCritical = false,
                            CreatedBy = "System",
                            MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                        };

                        emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                    }
                }

                catch (NotSupportedException ex)
                {
                    ModelState.AddModelError(string.Empty, "An error has occurred resetting your password: " + ex.Message + "." +
                               "Please check your values and try again.");
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(recoveryrequest);
                }
                catch (MembershipPasswordException ex)
                {
                    ModelState.AddModelError(string.Empty, "Invalid password answer. Please reenter the answer and try again.");
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(recoveryrequest);
                }
                catch (System.Configuration.Provider.ProviderException ex)
                {
                    ModelState.AddModelError(string.Empty, "The specified user name does not exist. Please check your value and try again.");
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(recoveryrequest);
                }
                return RedirectToAction("ForgotPasswordSuccessful");
            }
            return View(recoveryrequest);        
        }

        [HttpGet]
        public ActionResult ResetPassword(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new ArgumentNullException("Password Recovery Request Id cannot be null.");
            }
            string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
            string loginurl = "<a href='" + mvcclientendposint + "'/account/login> click here</a>";
            var validateRequestId = passwordRecoveryRequest.GetPasswordRecoveryRequestByVerificationId(id);
            if (validateRequestId != null && validateRequestId.IsVerified == false)
            {
                try
                {
                    var findmember = Membership.GetUser(validateRequestId.UserName, false);
                   
                    if (findmember != null)
                    {
                        string newPassword = Membership.Provider.ResetPassword(validateRequestId.UserName, DefaultSettings.Instance.PasswordAnswer);
                        TempData["ForgotPasswordMessage"] = "Your new password has been sent to you via email.";
                        StringBuilder bodyBuilder = new StringBuilder();
                        bodyBuilder.Append("Dear Sir/Madam");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.Append("Find you new password below.");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.Append("New Password: " + newPassword);                        
                        bodyBuilder.AppendLine("<br />");
                        bodyBuilder.AppendLine("<br />");

                        var emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
                        bodyBuilder.Append(emailSignature);

                        var emailAlertDTO = new EmailAlert
                        {
                            MailMessageFrom = DefaultSettings.Instance.Email,
                            MailMessageTo = string.Format("{0}", findmember.Email),
                            MailMessageSubject = string.Format("New Password - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                            MailMessageBody = string.Format("{0}", bodyBuilder),
                            MailMessageIsBodyHtml = true,
                            MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                            MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                            MailMessageSecurityCritical = false,
                            CreatedBy = "System",
                            MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                        };
                        validateRequestId.IsVerified = true;
                        passwordRecoveryRequest.UpdatePasswordRecoveryRequest(validateRequestId, serviceHeader);
                        emailalerservice.AddNewEmailAlert(emailAlertDTO, serviceHeader);
                    }

                    else
                    {
                        ModelState.AddModelError(string.Empty, "The specified user name does not exist. Please check your value and try again.");
                        return View();
                    }   
                }
                catch (NotSupportedException ex)
                {
                    ModelState.AddModelError(string.Empty, "An error has occurred resetting your password: " + ex.Message + "." +
                               "Please check your values and try again.");
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View();
                }
                catch (MembershipPasswordException ex)
                {
                    ModelState.AddModelError(string.Empty, "Invalid password answer. Please reenter the answer and try again.");
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View();
                }
                catch (System.Configuration.Provider.ProviderException ex)
                {
                    ModelState.AddModelError(string.Empty, "The specified user name does not exist. Please check your value and try again.");
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View();
                }                
            }

            return RedirectToAction("ResetPasswordSuccessful");
        }        

        public ActionResult ResetPasswordSuccessful()
        {
            return View();
        }

        public ActionResult ForgotPasswordSuccessful()
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]        
        public ActionResult ChangePassword(ManageUserViewModel model)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    var username = User.Identity.Name;

                    bool changepassword = Membership.Provider.ChangePassword(username, model.OldPassword, model.NewPassword);

                    if (changepassword)
                    {
                        ModelState.AddModelError(string.Empty, "Password Changed Successfully.");
                        return RedirectToAction("ChangePasswordSuccessful");
                    }

                    else
                    {
                        ModelState.AddModelError(string.Empty, "Password change failed. Please re-enter your values and try again.");
                        return View();
                    }
                }

                catch(Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(model);
                }                
            }
            return View();
        }

        public ActionResult ChangePasswordSuccessful()
        {
            return View();
        }

        public ActionResult UsersInRole(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException("id cannot be null.");
            }
            var userInRoleCollection = new List<UserInfo>();
            var usersInRole = Roles.GetUsersInRole(id);

            if (usersInRole != null && usersInRole.Any())
            {
                Array.ForEach(usersInRole, (roleName) =>
                {
                    var findUserInfo = userinfoservice.FindUserByUserName(roleName);
                    if (findUserInfo != null)
                    {
                        userInRoleCollection.Add(new UserInfo
                        {
                            BranchName = findUserInfo.Branch.BranchName,
                            Email = findUserInfo.Email,
                            FirstName = findUserInfo.FirstName,
                            LastName = findUserInfo.LastName,
                            UserName = findUserInfo.UserName,
                            Roles = id,
                            Id = findUserInfo.Id
                        });
                    }                    
                });
            }
            return View(userInRoleCollection);
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public ActionResult edituser(Guid id)
        {            
            string Username = string.Empty;

            var roleList = new List<CheckBoxListInfo>();

            if (id == Guid.Empty)
            {
                throw new ArgumentNullException("id cannot be null");
            }
            var userInfo = new UserInfo();
            var findUserById = userinfoservice.FindUserById(id);
            if (findUserById != null)
            {
                userInfo = findUserById;
                Username = findUserById.UserName;
                roleList = GetUserRoleInfo(findUserById.UserName, false);

            }
            ViewBag.Username = Username;
            ViewBag.Roles = roleList;
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());
            return View(userInfo);
        }

        [HttpPost]
        public ActionResult edituser(UserInfo userInfo)
        {
            var userinfo = userinfoservice.FindUserById(userInfo.Id);
            var roleList = new List<CheckBoxListInfo>();
            string Username = string.Empty;
            var selectedtype = Request.Form["Roles"];
            if (selectedtype == null)
            {
                ModelState.AddModelError(string.Empty, "No role selected.");
                return View(userInfo);
            }
            if (userinfo != null)
            {
                Username = userinfo.UserName;
                roleList = GetUserRoleInfo(userinfo.UserName, false);

            }
            ViewBag.Username = Username;
            ViewBag.Roles = roleList;
            ViewBag.BranchesSelectList = BranchesSelectList(Guid.Empty.ToString());            

            if (ModelState.IsValid)
            {
                try
                {
                    userinfo.Email = userInfo.Email;
                    userinfo.FirstName = userInfo.FirstName;
                    userinfo.BranchId = userInfo.BranchId;
                    userinfo.LastName = userInfo.LastName;
                    userinfo.IsLockedOut = userInfo.IsLockedOut;
                    userinfo.IsApproved = userInfo.IsApproved;
                    userinfoservice.UpdateUser(userinfo, serviceHeader);

                    //Update role
                    var findRoleForUser = Roles.GetRolesForUser(userinfo.UserName);
                    if (findRoleForUser != null)
                    {
                        if (findRoleForUser[0].ToString().ToLower() != selectedtype.ToLower())
                        {
                            Roles.RemoveUserFromRole(userinfo.UserName, findRoleForUser[0]);
                            Roles.AddUserToRole(userinfo.UserName, selectedtype);
                        }                        
                    }
                }

                catch (Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View();
                }
            }
            return RedirectToAction("membershiplist");
        }

        public class UserSummaryInfo
        {
            public string UserName { get; set; }

            public string Email { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }

            public bool IsApproved { get; set; }

            public string CreatedDate { get; set; }

            public string Commands { get; set; }
        }

        [NonAction]
        public List<SelectListItem> IncomeRangeSelectList()
        {
            List<SelectListItem> obj = new List<SelectListItem>();

            obj.Add(new SelectListItem { Text = "Select Income Range", Value = "Select Income Range" });

            obj.Add(new SelectListItem { Text = "999 - 10000", Value = "999 - 10000" });

            obj.Add(new SelectListItem { Text = "10001 - 20000", Value = "10001 - 20000" });

            obj.Add(new SelectListItem { Text = "20001 - 40000", Value = "20001 - 40000" });

            obj.Add(new SelectListItem { Text = "400001 - 80000", Value = "400001 - 80000" });

            obj.Add(new SelectListItem { Text = "80001 and above", Value = "80001 and above" });

            return obj;
        }

        /// <summary>
        /// Random approval code generator
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        private string GenerateRandomCustomerApprovalCode(int length)
        {
            string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[length];
            Random rd = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }

        [NonAction]
        public List<SelectListItem> BranchesSelectList(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultvalue = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Branch", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultvalue);

            var otherlist = branchservice.GetAllBranches().Where(x => x.IsEnabled == true);

            foreach (var item in otherlist)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.BranchNameBranchCode, Value = item.Id.ToString("D") });

            return SelectList;
        }

        public List<CheckBoxListInfo> maritalStatusInfoCheckList()
        {
            List<CheckBoxListInfo> maritalStatusInfo = new List<CheckBoxListInfo>();

            maritalStatusInfo.Add(new CheckBoxListInfo(((int)Enumerations.MaritalStatus.Single).ToString(), " Single", false));

            maritalStatusInfo.Add(new CheckBoxListInfo(((int)Enumerations.MaritalStatus.Married).ToString(), " Married", false));

            maritalStatusInfo.Add(new CheckBoxListInfo(((int)Enumerations.MaritalStatus.Divorced).ToString(), " Divorced", false));

            return maritalStatusInfo;
        }

        public List<CheckBoxListInfo> genderInfoCheckList()
        {
            List<CheckBoxListInfo> genderCheckList = new List<CheckBoxListInfo>();

            genderCheckList.Add(new CheckBoxListInfo(((int)Enumerations.Gender.Male).ToString(), "Male", false));

            genderCheckList.Add(new CheckBoxListInfo(((int)Enumerations.Gender.Female).ToString(), "Female", false));

            return genderCheckList;
        }
    }
}

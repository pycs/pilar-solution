﻿using Nikopeshe.Data;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Nikopeshe.Entity.DataModels;
using System.Text;
using System.Threading.Tasks;
using System.Messaging;

namespace Nikopeshe.WindowsService.EmailDispatcher.Configuration
{
    public class EmailQueueingJob : IJob
    {
        private readonly IMessageQueueService _messageQueueService;
        private readonly string _messageRelayQueuePath;
        private DataContext datacontext = new DataContext();

        public EmailQueueingJob()
        {
            _messageQueueService = new MessageQueueService();
            _messageRelayQueuePath = ConfigurationManager.AppSettings["MessageRelayQueuePath"];
        }

        #region IJob

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                // 1. Retrieve messages whose DLR status is UnKnown
                var emailAlertsWithDLRStatusPending = datacontext.EmailAlert.Where(x => x.MailMessageDLRStatus == (int)Enumerations.DLRStatus.Pending).Take(10);

                // 2. Send the messages to msmq - Normal priority
                if (emailAlertsWithDLRStatusPending != null && emailAlertsWithDLRStatusPending.Any())
                {
                    foreach (var item in emailAlertsWithDLRStatusPending)
                        if (item.MailMessageSendRetry == 0)
                            _messageQueueService.Send(_messageRelayQueuePath, item.Id, Enumerations.MessageCategory.EmailAlert, (MessagePriority)item.MailMessagePriority);                  
                }
            }

            catch(Exception ex)
            {
                Logger.CreateLog(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }
        #endregion
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Interfaces
{
    public interface IGroupHelperService
    {
        GroupHelper FindGroupHelperById(Guid Id);

        IList<GroupHelper> GetAllGroupHelpers(IsolationLevel isolationLevel);

        IList<GroupHelper> GetGroupHelpersByCountyCode(int countyCode);
    }
}

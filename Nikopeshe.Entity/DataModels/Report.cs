﻿using Nikopeshe.Entity.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{
    public class Report : BaseEntity
    {
        [Display(Name = "Folder Name")]
        [MaxLength(100)]
        [Required]
        public string FolderName { get; set; }

        [MaxLength(100)]
        [Display(Name="Report Name")]
        [Required]
        public string ReportName { get; set; }

        [Display(Name="Report Path")]
        //[Required]
        public string ReportPath { get; set; }

        [Display(Name="Authorized Role(s)")]
        [Required]
        [NotMapped]
        public int RoleCategory { get; set; }

        [Display(Name = "Role Description")]
        public string RoleDescription
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.RoleCategory), RoleCategory) ? EnumHelper.GetDescription((Enumerations.RoleCategory)RoleCategory) : string.Empty;
            }
        }

        public int ReportCategory { get; set; }

        public string ReportCategoryDesc
        {
            get
            {
                return Enum.IsDefined(typeof(Enumerations.ReportCategory), ReportCategory) ? EnumHelper.GetDescription((Enumerations.ReportCategory)ReportCategory) : string.Empty;
            }
        }
        [Display(Name="Is Enabled?")]
        public bool IsEnabled { get; set; }

        [Display(Name = "Commands")]
        [NotMapped]
        public string Commands { get; set; }

        [Display(Name="Is Parameterized?")]
        public bool IsParameterized { get; set; }

        [Display(Name="Parameter name")]
        public string ParameterName { get; set; }

        [Display(Name="Description")]
        public string Description { get; set; }        
    }
}

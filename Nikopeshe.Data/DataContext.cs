﻿using Nikopeshe.Data.Migrations;
using Nikopeshe.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Nikopeshe.Data
{
    public class DataContext : DbContext, IDataContext, IDisposable
    {
        private Dictionary<Type, DbContext> _initializedDbContexts;

        internal Dictionary<Type, DbContext> InitializedDbContexts { get { return _initializedDbContexts; } }
        public DataContext()
            : base("PilarDefaultConnnection")
        {
            this.Configuration.AutoDetectChangesEnabled = true;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataContext, Configuration>());
            _initializedDbContexts = new Dictionary<Type, DbContext>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                .Where(type => !String.IsNullOrEmpty(type.Namespace))
                .Where(type => type.BaseType != null && type.BaseType.IsGenericType && type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));

            foreach(var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();            
            base.OnModelCreating(modelBuilder);            
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.EmailAlert> EmailAlert { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.TextAlert> TextAlert { get; set; }        
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.LoanRequest> LoanRequest { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.Customer> Customer { get; set; }        
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.C2BTransaction> C2BTransaction { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.CustomersAccount> CustomersAccount { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.ChartofAccount> ChartofAccount { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.SavingsProduct> SavingsProduct { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.LoanProduct> LoanProduct { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.SystemGeneralLedgerAccountMapping> SystemGeneralLedgerAccountMapping { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.LoanTransactionHistory> LoanTransactionHistory { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.Branch> Branch { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.GraduatedScale> GraduatedScale { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.CustomerDocument> CustomerDocuments { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.LoanRequestDocument> LoanRequestDocument { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.StaticSetting> StaticSetting { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.LoanProductsCharge> LoanProductsCharge { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.Charge> Charge { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.UserInfo> UserInfo { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.Reconciliation> Reconciliation { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.Group> Group { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.GroupHelper> GroupHelper { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.Collateral> Collateral { get; set; }
        public System.Data.Entity.DbSet<Nikopeshe.Entity.DataModels.Guarantor> Guarantor { get; set; }

        // This is overridden to prevent someone from calling SaveChanges without specifying the user making the change       
        public int SaveChanges(ServiceHeader serviceHeader)
        {
            ChangeTracker.DetectChanges(); // Important!

            try
            {
                List<DomainAuditBulkCopy> bulkCopyList = new List<DomainAuditBulkCopy>();                                
                DataContext dataContext = new DataContext();
                //ServiceHeader serviceHeader = new ServiceHeader();
                // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
                foreach (var entry in this.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
                {
                    // For each changed record, get the audit record entries and add them

                    var wrapper = GetAuditRecordsForChange(dataContext, entry);

                    if (wrapper != null)
                    {
                        bulkCopyList.Add(wrapper.GetBulkCopyEntry(serviceHeader));
                    }
                }

                if (bulkCopyList.Any())
                {
                    BulkInsert<DomainAuditBulkCopy>(dataContext.Database.Connection.ConnectionString, bulkCopyList);
                }                                              
            }
            catch (Exception ex)
            { 
                /*swallow*/
            }

            // Call the original SaveChanges(), which will save both the changes made and the audit records
            return base.SaveChanges();
        }

        public override int SaveChanges()
        {
            ChangeTracker.DetectChanges(); // Important!

            try
            {
                List<DomainAuditBulkCopy> bulkCopyList = new List<DomainAuditBulkCopy>();
                ServiceHeader serviceHeader = new ServiceHeader();
                DataContext dataContext = new DataContext();

                // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
                foreach (var entry in this.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
                {
                    // For each changed record, get the audit record entries and add them

                    var wrapper = GetAuditRecordsForChange(dataContext, entry);

                    if (wrapper != null)
                    {
                        bulkCopyList.Add(wrapper.GetBulkCopyEntry(serviceHeader));
                    }
                }

                if (bulkCopyList.Any())
                {
                    BulkInsert<DomainAuditBulkCopy>(dataContext.Database.Connection.ConnectionString, bulkCopyList);
                }
            }
            catch (Exception ex)
            {

                /*swallow*/
            }

            // Call the original SaveChanges(), which will save both the changes made and the audit records
            return base.SaveChanges();
        }

        private static bool BulkInsert<T>(string connectionString, IList<T> list)
        {
            var result = default(bool);

            using (var bulkCopy = new SqlBulkCopy(connectionString, SqlBulkCopyOptions.FireTriggers))
            {
                bulkCopy.BulkCopyTimeout = 300;
                bulkCopy.BatchSize = 5000;
                bulkCopy.DestinationTableName = "pilar_DomainAudits";

                var table = new System.Data.DataTable();

                var props = TypeDescriptor.GetProperties(typeof(T))
                    //Dirty hack to make sure we only have system data types 
                    //i.e. filter out the relationships/collections
                                           .Cast<PropertyDescriptor>()
                                           .Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System"))
                                           .ToArray();

                foreach (var propertyInfo in props)
                {
                    bulkCopy.ColumnMappings.Add(propertyInfo.Name, propertyInfo.Name);

                    table.Columns.Add(propertyInfo.Name, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType);
                }

                var values = new object[props.Length];

                foreach (var item in list)
                {
                    for (var i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item);
                    }

                    table.Rows.Add(values);
                }

                bulkCopy.WriteToServer(table);

                result = true;
            }

            return result;
        }

        private static AuditInfoWrapper GetAuditRecordsForChange(DbContext dbContext, DbEntityEntry dbEntry)
        {
            // Get table name 
            string tableName = string.Format("pilar_{0}", dbContext.GetTableName(dbEntry));

            if (tableName.Equals("pilar_DomainAudits", StringComparison.OrdinalIgnoreCase))
                return null;

            var entityType = dbEntry.Entity.GetType();

            if (entityType.BaseType != null && entityType.Namespace == "System.Data.Entity.DynamicProxies")
                entityType = entityType.BaseType;

            // Get primary key value (If you have more than one key column, this will need to be adjusted)
            string keyName = entityType.GetProperties().Single(p => p.GetCustomAttributes(typeof(KeyAttribute), false).Count() > 0).Name;

            List<AuditInfo> infoList = new List<AuditInfo>();

            string eventType = string.Empty;

            string recordID = string.Empty;

            if (dbEntry.State == EntityState.Added)
            {
                foreach (string propertyName in dbEntry.CurrentValues.PropertyNames)
                {
                    var currentValues = string.Empty;
                    if (dbEntry.CurrentValues.GetValue<object>(propertyName) != null)
                    {
                        if (dbEntry.CurrentValues.GetValue<object>(propertyName) is DbPropertyValues)
                            currentValues = ((DbPropertyValues)dbEntry.CurrentValues.GetValue<object>(propertyName)).GetValues();
                        else currentValues = dbEntry.CurrentValues.GetValue<object>(propertyName).ToString();
                    }

                    eventType = "Entity_Added";

                    recordID = dbEntry.CurrentValues.GetValue<object>(keyName).ToString();

                    var addedAuditLog = new AuditInfo
                    {
                        ColumnName = propertyName,
                        OriginalValue = string.Empty,
                        NewValue = currentValues,
                    };

                    infoList.Add(addedAuditLog);
                }
            }
            else if (dbEntry.State == EntityState.Deleted)
            {
                foreach (string propertyName in dbEntry.OriginalValues.PropertyNames)
                {
                    var originalValues = string.Empty;
                    if (dbEntry.OriginalValues.GetValue<object>(propertyName) != null)
                    {
                        if (dbEntry.OriginalValues.GetValue<object>(propertyName) is DbPropertyValues)
                            originalValues = ((DbPropertyValues)dbEntry.OriginalValues.GetValue<object>(propertyName)).GetValues();
                        else originalValues = dbEntry.OriginalValues.GetValue<object>(propertyName).ToString();
                    }

                    eventType = "Entity_Deleted";

                    recordID = dbEntry.OriginalValues.GetValue<object>(keyName).ToString();

                    var deletedAuditLog = new AuditInfo
                    {
                        ColumnName = propertyName,
                        OriginalValue = originalValues,
                        NewValue = string.Empty,
                    };

                    infoList.Add(deletedAuditLog);
                }
            }
            else if (dbEntry.State == EntityState.Modified)
            {
                foreach (string propertyName in dbEntry.OriginalValues.PropertyNames)
                {
                    // For updates, we only want to capture the columns that actually changed
                    if (!object.Equals(dbEntry.OriginalValues.GetValue<object>(propertyName), dbEntry.CurrentValues.GetValue<object>(propertyName)))
                    {
                        var originalValues = string.Empty;
                        if (dbEntry.OriginalValues.GetValue<object>(propertyName) != null)
                        {
                            if (dbEntry.OriginalValues.GetValue<object>(propertyName) is DbPropertyValues)
                                originalValues = ((DbPropertyValues)dbEntry.OriginalValues.GetValue<object>(propertyName)).GetValues();
                            else originalValues = dbEntry.OriginalValues.GetValue<object>(propertyName).ToString();
                        }

                        var currentValues = string.Empty;
                        if (dbEntry.CurrentValues.GetValue<object>(propertyName) != null)
                        {
                            if (dbEntry.CurrentValues.GetValue<object>(propertyName) is DbPropertyValues)
                                currentValues = ((DbPropertyValues)dbEntry.CurrentValues.GetValue<object>(propertyName)).GetValues();
                            else currentValues = dbEntry.CurrentValues.GetValue<object>(propertyName).ToString();
                        }

                        eventType = "Entity_Modified";

                        recordID = dbEntry.OriginalValues.GetValue<object>(keyName).ToString();

                        var modifiedAuditLog = new AuditInfo
                        {
                            ColumnName = propertyName,
                            OriginalValue = originalValues,
                            NewValue = currentValues,
                        };

                        infoList.Add(modifiedAuditLog);
                    }
                }
            }

            // Otherwise, don't do anything, we don't care about Unchanged or Detached entities

            return new AuditInfoWrapper { TableName = tableName, EventType = eventType, RecordID = recordID, AuditInfoCollection = infoList };

        }

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);            
        }
    }

    [Serializable]
    public class AuditInfo
    {
        [XmlAttribute]
        public string ColumnName { get; set; }

        [XmlAttribute]
        public string OriginalValue { get; set; }

        [XmlAttribute]
        public string NewValue { get; set; }
    }

    [Serializable]
    public class AuditInfoWrapper
    {
        [XmlAttribute]
        public string TableName { get; set; }

        [XmlAttribute]
        public string EventType { get; set; }

        [XmlAttribute]
        public string RecordID { get; set; }

        public List<AuditInfo> AuditInfoCollection { get; set; }
    }

    public class DomainAuditBulkCopy
    {
        public Guid Id { get; set; }

        public string EventType { get; set; }

        public string TableName { get; set; }

        public string RecordID { get; set; }

        public string AdditionalNarration { get; set; }

        public string ApplicationUserName { get; set; }

        public string EnvironmentUserName { get; set; }

        public string RemoteIPAddress { get; set; }

        public string EnvironmentMachineName { get; set; }

        public string EnvironmentDomainName { get; set; }

        public string EnvironmentOSVersion { get; set; }

        public string EnvironmentMACAddress { get; set; }

        public string EnvironmentMotherboardSerialNumber { get; set; }

        public string EnvironmentProcessorId { get; set; }

        public DateTime EventDate { get; set; }

        public DateTime CreatedDate { get; set; }
    }

    internal static class Extensions
    {
        public static string GetTableName<T>(this DbContext context) where T : class
        {
            var workspace = ((IObjectContextAdapter)context).ObjectContext.MetadataWorkspace;

            var mappingItemCollection = (StorageMappingItemCollection)workspace.GetItemCollection(DataSpace.CSSpace);

            var storeContainer = ((EntityContainerMapping)mappingItemCollection[0]).StoreEntityContainer;

            var baseEntitySet = storeContainer.BaseEntitySets.Single(es => es.Name == typeof(T).Name);

            return string.Format("{0}.{1}", baseEntitySet.Schema, baseEntitySet.Table);
        }

        public static string GetTableName(this DbContext context, DbEntityEntry ent)
        {
            var objectContext = ((IObjectContextAdapter)context).ObjectContext;

            var entityType = ent.Entity.GetType();

            if (entityType.BaseType != null && entityType.Namespace == "System.Data.Entity.DynamicProxies")
                entityType = entityType.BaseType;

            string entityTypeName = entityType.Name;

            var container = objectContext.MetadataWorkspace.GetEntityContainer(objectContext.DefaultContainerName, DataSpace.CSpace);

            string entitySetName = (from meta in container.BaseEntitySets
                                    where meta.ElementType.Name == entityTypeName
                                    select meta.Name).First();
            return entitySetName;
        }

        public static string GetValues(this DbPropertyValues values)
        {
            var sb = new StringBuilder();

            foreach (var propertyName in values.PropertyNames)
            {
                sb.AppendLine(string.Format("Property '{0}' has value '{1}'", propertyName, values[propertyName]));
            }

            return string.Format("{0}", sb);
        }

        public static string GetXml(this AuditInfoWrapper value)
        {
            var xmlMessage = string.Empty;

            var utf8 = new UTF8Encoding();

            using (var stream = new MemoryStream())
            {
                using (var xtWriter = new XmlTextWriter(stream, utf8))
                {
                    var serializer = new XmlSerializer(typeof(AuditInfoWrapper));

                    serializer.Serialize(xtWriter, value);

                    xtWriter.Flush();

                    stream.Seek(0, System.IO.SeekOrigin.Begin);

                    xmlMessage = utf8.GetString(stream.ToArray());
                }
            }

            return xmlMessage;
        }

        public static DomainAuditBulkCopy GetBulkCopyEntry(this AuditInfoWrapper value, ServiceHeader serviceHeader)
        {
            return new DomainAuditBulkCopy
            {
                Id = Guid.NewGuid(),
                TableName = value.TableName,
                EventType = value.EventType,
                RecordID = value.RecordID,
                RemoteIPAddress = serviceHeader.RemoteIPAddress,
                AdditionalNarration = value.GetXml(),
                ApplicationUserName = serviceHeader != null ? serviceHeader.ApplicationUserName : string.Empty,
                EnvironmentUserName = serviceHeader != null ? serviceHeader.EnvironmentUserName : string.Empty,
                EnvironmentMachineName = serviceHeader != null ? serviceHeader.EnvironmentMachineName : string.Empty,
                EnvironmentDomainName = serviceHeader != null ? serviceHeader.EnvironmentDomainName : string.Empty,
                EnvironmentOSVersion = serviceHeader != null ? serviceHeader.EnvironmentOSVersion : string.Empty,
                EnvironmentMACAddress = serviceHeader != null ? serviceHeader.EnvironmentMACAddress : string.Empty,
                EnvironmentMotherboardSerialNumber = serviceHeader != null ? serviceHeader.EnvironmentMotherboardSerialNumber : string.Empty,
                EnvironmentProcessorId = serviceHeader != null ? serviceHeader.EnvironmentProcessorId : string.Empty,
                EventDate = DateTime.Now,
                CreatedDate = DateTime.Now
            };
        }
    }
}

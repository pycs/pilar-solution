﻿using Android.App;
using Android.OS;
using Android.Widget;
using Android.Text;
using Android.Content.PM;
using Android.Text.Method;
using Android.Graphics;
using Android.Content;

namespace Pilar.AndroidApplication
{
	[Activity (Label = "Sign up", Icon = "@drawable/BCPhoneicon",Theme = "@style/AppBaseTheme")]						
	public class RegisterActivity : Activity
	{
		Button SignUpButton;
		EditText FullName;
		EditText EmailAddress;
		EditText Password;
		EditText PhoneNumber;
		TextView Termsandconditions;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.Register);

			SignUpButton = FindViewById<Button> (Resource.Id.signupButton);
			FullName = FindViewById<EditText> (Resource.Id.firstname);
			EmailAddress = FindViewById<EditText> (Resource.Id.emailaddress);
			PhoneNumber = FindViewById<EditText> (Resource.Id.mobilenumber);
			Password = FindViewById<EditText> (Resource.Id.password);
			Termsandconditions = FindViewById<TextView> (Resource.Id.TCs);

			//SignUpButton.Click += (s, e) => Toast.MakeText (this, "Verification email has been sent to " + "Email", ToastLength.Long).Show ();
			SignUpButton.Click += (sender, e) => 
			{

				if(FullName.Text.Length == 0)
				{
					Toast.MakeText (this, "FullName is required!", ToastLength.Long).Show ();
				}
				else if(!isValidEmail(EmailAddress.Text) || EmailAddress.Text.Length == 0)
				{
					Toast.MakeText (this, "Email Address is required and must be valid.", ToastLength.Long).Show ();
				}
				else if(PhoneNumber.Text.Length == 0)
				{
					Toast.MakeText (this, "PhoneNumber is required!", ToastLength.Long).Show ();
				}
				else if(Password.Text.Length == 0)
				{
					Toast.MakeText (this, "Password is required!", ToastLength.Long).Show ();
				}
                //else
                //{
                //    //Toast.MakeText (this, "Verification email has been sent to " + EmailAddress.Text.ToString() , ToastLength.Long).Show ();
                //    //StartActivity(typeof(LoginActivity));
                //    AlertDialog.Builder alert = new AlertDialog.Builder (this);
                //    //alert.SetView(LayoutInflater.Inflate(Resource.Layout.input_dialog, null));
                //    var EmailReset = FindViewById<EditText>(Resource.Id.emailreset);
                //    alert.SetTitle("A confirmation email has been sent to " + EmailAddress.Text.ToString() + "");
                //    alert.SetPositiveButton("OK", (senderAlert, args) => StartActivity (typeof(LoginActivity)));
                //    //Toast.MakeText (this, "An email with instructions has been sent", ToastLength.Long).Show ());
                //    alert.Create().Show();
                //}
			};
			//Terms and Conditions
			Termsandconditions.Click += (sender, e) => {
				var uri = Android.Net.Uri.Parse("http://bump-cash.com/");
				var intent = new Intent(Intent.ActionView, uri);
				StartActivity(intent);
			};
		}
		//Email validation

		private bool isValidEmail(string email) {
			if (TextUtils.IsEmpty (email)) {
				return false;
			} else {
				return Android.Util.Patterns.EmailAddress.Matcher (email).Matches ();
			}
		}

		//
	}
}


﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Specifications
{
    public class GroupSpecification
    {
        public static ISpecification<Group> FindGroupById(Guid id, string text)
        {
            Specification<Group> specification = new DirectSpecification<Group>(c => c.Id == id);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var groupNameSpec = new DirectSpecification<Group>(c => c.GroupName.Contains(text));

                var groupNumberSpec = new DirectSpecification<Group>(c => c.GroupNumber.Contains(text));

                specification &= (groupNameSpec | groupNumberSpec);
            }

            return specification;
        }

        public static Specification<Group> DefaultSpec(string text)
        {
            Specification<Group> specification = new TrueSpecification<Group>();

            {
                var groupNameSpec = new DirectSpecification<Group>(c => c.GroupName.Contains(text));

                var groupNumberSpec = new DirectSpecification<Group>(c => c.GroupNumber.Contains(text));

                specification &= (groupNameSpec | groupNumberSpec);
            }

            return specification;
        }

        public static ISpecification<Group> FindGroupByBranchId(Guid id, string text)
        {
            Specification<Group> specification = new DirectSpecification<Group>(c => c.BranchId == id);

            if (!String.IsNullOrWhiteSpace(text))
            {
                var groupNameSpec = new DirectSpecification<Group>(c => c.GroupName.Contains(text));

                var groupNumberSpec = new DirectSpecification<Group>(c => c.GroupNumber.Contains(text));

                specification &= (groupNameSpec | groupNumberSpec);
            }

            return specification;
        }
    }
}

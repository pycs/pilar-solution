﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Entity.DataModels
{
    public class Guarantor : BaseEntity
    {
        [Index]
        public Guid? LoanRequestId { get; set; }

        public virtual LoanRequest LoanRequest { get; set; }

        [Display(Name = "First Name")]
        [Column(TypeName = "VARCHAR")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Column(TypeName = "VARCHAR")]
        public string LastName { get; set; }

        [Display(Name = "Address")]
        [Column(TypeName = "VARCHAR")]
        public string Address { get; set; }

        [Column(TypeName = "VARCHAR")]
        [StringLength(12, MinimumLength = 12)]
        [Display(Name = "Mobile Number(2547********)")]
        [RegularExpression(@"^254(?:[0-9]??){6,14}[0-9]$", ErrorMessage = "The mobile number should start with country code 254 followed by phone number")] 
        public string PhoneNumber { get; set; }

        [Display(Name = "ID Number")]
        [Column(TypeName = "VARCHAR")]
        [Index("Id_Number")]
        public string IDNumber { get; set; }

        [Display(Name = "Relationship")]
        [Column(TypeName = "VARCHAR")]
        public string Relationship { get; set; }

        [Display(Name = "Amount")]
        public decimal Amount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nikopeshe.WebApplication.Extensions
{
    public static class StringExtension
    {
        public static bool StringIsAllNumeric(this string s)
        {
            var result = default(bool);

            if (!string.IsNullOrWhiteSpace(s))
            {
                s = s.Trim();

                result = !s.ToCharArray().Any(c => Char.IsLetter(c) || Char.IsWhiteSpace(c) || Char.IsSymbol(c) || Char.IsSeparator(c) || Char.IsPunctuation(c) || Char.IsControl(c));
            }

            return result;
        }
    }
}
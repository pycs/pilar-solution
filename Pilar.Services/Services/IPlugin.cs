﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services
{
    public interface IPlugin
    {
        Guid Id { get; }

        string Description { get; }

        void DoWork(params string[] args);

        void Exit();
    }
}

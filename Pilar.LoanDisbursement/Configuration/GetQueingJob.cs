﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.Disbursement;
using Quartz;
using System;
using System.Configuration;
using System.Linq;

namespace Pilar.LoanDisbursement.Configuration
{
    public class GetQueingJob : IJob
    {
        private readonly IMessageQueueService _messageQueueService;
        private readonly ILoanProcessing _iloanprocessing;
        private readonly string _messageRelayQueuePath;
        private DataContext datacontext = new DataContext();
        public GetQueingJob()
        {
            _messageQueueService = new MessageQueueService();
            _iloanprocessing = new LoanProcessing();
            _messageRelayQueuePath = ConfigurationManager.AppSettings["SPSQueuePath"];
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var findpendingdisbursementloanrequest = _iloanprocessing.GetLoanDibursementWithPendingGetStatus();
                if (findpendingdisbursementloanrequest != null && findpendingdisbursementloanrequest.Any())
                    foreach (var pendingloanrequest in findpendingdisbursementloanrequest)
                    {
                        if (!string.IsNullOrWhiteSpace(pendingloanrequest.ThirdPartyTransactionId))
                        {
                            _messageQueueService.Send(_messageRelayQueuePath, pendingloanrequest.Id, Nikopeshe.Entity.DataModels.Enumerations.MessageCategory.SPSGetRequest, System.Messaging.MessagePriority.High);
                            pendingloanrequest.ProcessingStatus = (int)Enumerations.ProcessingStatus.QueryStatus;
                            //_iloanprocessing.UpdateLoanRequest(pendingloanrequest);
                        }
                    }                       
            }

            catch(Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo(ex.Message + ex.InnerException + ex.StackTrace);
            }
        }
    }
}

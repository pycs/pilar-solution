﻿namespace Nikopeshe.Entity.DataModels
{
    public class Reconciliation : BaseEntity
    {
        public string FileName { get; set; }

        public string FilePath { get; set; }

        public int FileStatus { get; set; }

        public int FileType { get; set; }

        public string Remarks { get; set; }

        public int FileContentType { get; set; }
    }
}

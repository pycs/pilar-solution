﻿using Crosscutting.NetFramework.Logging;
using Infrastructure.Crosscutting.NetFramework;
using Nikopeshe.Data;
using Nikopeshe.Data.Mappings;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.Charge;
using Pilar.Services.Services.CustomerService;
using Pilar.Services.Services.GraduatedScale;
using Pilar.Services.Services.IPNService;
using System;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Messaging;
using System.Net;
using System.Text;

namespace Pilar.Repayments.Sungroup.Services
{
    [Export(typeof(IPlugin))]
    public class IPNConfirmationAndProcessing : IPlugin
    {
        private MessageQueue _messageQueue;
        private IIPNService _iipnservice;
        private IChargeService chargeService;
        private ICustomerService _customerservice;
        private readonly string _queuePath;
        private IGraduatedScaleService graduatedScaleService;
        string CompanyId = ConfigurationManager.AppSettings["CompanyId"].ToString();
        private string TextSignature = ConfigurationManager.AppSettings["TextSignature"].ToString();
        string spsusername = ConfigurationManager.AppSettings["SPSUsername"].ToString();
        string spsapiendpoint = ConfigurationManager.AppSettings["SPSAPIEndPoint"].ToString();
        string spspassword = ConfigurationManager.AppSettings["SPSPassword"].ToString();
        string recipientEmailAddress = ConfigurationManager.AppSettings["NotificationsEmailAddress"].ToString();
        string emailSignature = ConfigurationManager.AppSettings["EmailSignature"].Replace(".", "<br />");
        string failedC2BEmailAddress = ConfigurationManager.AppSettings["C2BFailedApprovalEmailAddress"].ToString();
        string failedC2BEmailAddressCC = ConfigurationManager.AppSettings["C2BFailedApprovalEmailAddressCC"].ToString();
        private decimal totalloanamount = 0m;

        [ImportingConstructor]
        public IPNConfirmationAndProcessing()
        {
            _iipnservice = new IPNService();
            _customerservice = new CustomerService();
            chargeService = new ChargeService();
            graduatedScaleService = new GraduatedScaleService();
            _queuePath = ConfigurationManager.AppSettings["C2BQueuePath"];
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{78DF89F5-96DA-4381-97E5-3AA14A7C11AB}"); }
        }

        public string Description
        {
            get { return "C2B TRANSACTION CONFIRMATION"; }
        }


        public void DoWork(params string[] args)
        {
            try
            {
                if (!MessageQueue.Exists(_queuePath))
                    throw new ArgumentException("Queue does not exist at specified path", "_queuePath");
                else
                {
                    // initialize message queue
                    _messageQueue = new MessageQueue(_queuePath, QueueAccessMode.Receive);

                    // initialize message queue
                    _messageQueue = new MessageQueue(_queuePath);

                    // enable the AppSpecific field in the messages
                    _messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                    // set the formatter to binary.
                    _messageQueue.Formatter = new BinaryMessageFormatter();

                    // set an event to listen for new messages.
                    _messageQueue.ReceiveCompleted += MessageQueue_ReceiveCompleted;

                    // start listening
                    _messageQueue.BeginReceive();
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        public void Exit()
        {
            _messageQueue.ReceiveCompleted -= MessageQueue_ReceiveCompleted;

            _messageQueue.Close();
        }

        #endregion

        #region Event Handlers
        private void MessageQueue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                // retrieve message category
                var messageCategory = (Nikopeshe.Entity.DataModels.Enumerations.MessageCategory)e.Message.AppSpecific;
                switch (messageCategory)
                {
                    case Nikopeshe.Entity.DataModels.Enumerations.MessageCategory.C2BNotificationSync:

                        #region Dequeue and Save

                        var c2bTransaction = (C2BTransaction)e.Message.Body;
                        using (var _datacontext = new DataContext())
                        {
                            using (var transaction = _datacontext.Database.BeginTransaction())
                            {
                                var findC2BByMPesaReceiptNumber = _datacontext.C2BTransaction.Where(x => x.TransID == c2bTransaction.TransID).FirstOrDefault();

                                if (findC2BByMPesaReceiptNumber == null)
                                {
                                    c2bTransaction.Status = (int)Enumerations.C2BTransactionStatus.Approved;
                                    _datacontext.C2BTransaction.Add(c2bTransaction);
                                    _datacontext.SaveChanges();
                                    transaction.Commit();
                                }
                            }
                        }


                        #endregion

                        break;

                    case Nikopeshe.Entity.DataModels.Enumerations.MessageCategory.C2BConfirmation:

                        #region C2BConfirmation

                        try
                        {
                            var recordId = (Guid)e.Message.Body;
                            var datacontext = new DataContext();
                            var c2bnotification = datacontext.C2BTransaction.Find(recordId);
                            if (c2bnotification != null)
                            {
                                HttpWebRequest webRequest = WebRequest.Create(spsapiendpoint + c2bnotification.TransactionId) as HttpWebRequest;
                                if (webRequest == null)
                                    throw new NullReferenceException("request is not a http request");

                                webRequest.Method = "GET";
                                webRequest.ContentType = "application/json";

                                webRequest.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(spsusername + ":" + spspassword)));
                                HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;
#if DEBUG
                                LoggerFactory.CreateLog().LogInfo(webResponse.StatusDescription);
#endif
                                if (webResponse.StatusCode == HttpStatusCode.OK)
                                {
                                    c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Approved;
                                    c2bnotification.Result = "Approval Successful";
                                    var entity = datacontext.Entry(c2bnotification);
                                    entity.State = System.Data.Entity.EntityState.Modified;
#if DEBUG
                                    LoggerFactory.CreateLog().LogInfo("End " + webResponse.StatusCode.ToString());
#endif
                                }
                            }
                        }

                        catch (WebException ex)
                        {
                            LoggerFactory.CreateLog().LogInfo("MessageQueue_ReceiveCompleted.C2BTransactionConfirmation", ex.Message + ex.InnerException + ex.StackTrace);
                        }

                        #endregion

                        break;

                    case Nikopeshe.Entity.DataModels.Enumerations.MessageCategory.C2BProcessing:

                        #region C2B Post to Savings Account
                        using (var datacontext = new DataContext())
                        {
                            using (var transaction = datacontext.Database.BeginTransaction())
                            {
                                try
                                {
                                    var recordId = (Guid)e.Message.Body;
                                    var c2bnotification = datacontext.C2BTransaction.Find(recordId);
                                    if (c2bnotification != null)
                                    {
                                        //find m-pesa repayment gl account id 
                                        var findmpesarepaymentaccountid = datacontext.SystemGeneralLedgerAccountMapping.Where(x => x.SystemGeneralLedgerAccountCode == Enumerations.SystemGeneralLedgerAccountCode.MPesaRepaymentAccount).FirstOrDefault();
                                        var findmpessuspenseaccountid = datacontext.SystemGeneralLedgerAccountMapping.Where(x => x.SystemGeneralLedgerAccountCode == Enumerations.SystemGeneralLedgerAccountCode.MPesaSuspsnseAccount).FirstOrDefault();
                                        var finddefaultsavingaccountid = datacontext.SavingsProduct.ToList().FirstOrDefault();
                                        var findLoanTransactionHistoryByC2BId = _iipnservice.FindLoanTransactionHistoryByC2BTransactionId(c2bnotification.Id);
                                        var findStaticSetting = datacontext.StaticSetting.Where(x => x.Key == "CREDITSAVINGSACCOUNT").FirstOrDefault();

                                        if (findLoanTransactionHistoryByC2BId == null)
                                        {
                                            if (!string.IsNullOrWhiteSpace(c2bnotification.BillRefNumber))
                                            {
                                                //Check if Bill Ref No is greater or equal to 9 characters
                                                if (c2bnotification.BillRefNumber.Length != 0)
                                                {
                                                    string billRefNo = c2bnotification.BillRefNumber;
                                                    //string customerMobileNumber = "254" + billRefNo.Substring(billRefNo.Length - 9);

                                                    //Find Customer Using Bill Ref No.
                                                    var findCustomer = _customerservice.FindCustomerByIdNumber(billRefNo);
                                                    if (findCustomer != null)
                                                    {
                                                        //find active loan
                                                        var findcustomeractiveloan = (from a in datacontext.Customer
                                                                                      join b in datacontext.CustomersAccount
                                                                                          on a.Id equals b.CustomerId
                                                                                      join c in datacontext.LoanRequest on b.Id equals c.CustomerAccountId
                                                                                      where c.Status == (int)Enumerations.LoanStatus.Active && a.Id == findCustomer.Id
                                                                                      select c).FirstOrDefault();
                                                        var findcustomersavingsaccountid = datacontext.CustomersAccount.Where(x => x.CustomerId == findCustomer.Id && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
                                                        if (findcustomeractiveloan != null)
                                                        {

                                                            if (findcustomersavingsaccountid != null && findmpesarepaymentaccountid != null && finddefaultsavingaccountid != null)
                                                            {
                                                                int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                                #region Dr M-Pesa Repayment GL, Cr Savings GL
                                                                LoanTransactionHistory loantransactionhistory = new LoanTransactionHistory()
                                                                {
                                                                    TransactionType = Enumerations.TransactionType.Debit,
                                                                    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                    TransactionAmount = c2bnotification.TransAmount,
                                                                    TotalTransactionAmount = c2bnotification.TransAmount,
                                                                    C2BTransactionId = c2bnotification.Id,
                                                                    ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                    ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                    CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                    Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                    CreatedBy = "System",
                                                                    IsApproved = true,
                                                                    ApprovedBy = "System",
                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                    TransactionBatchNumber = batchNo
                                                                };
                                                                datacontext.LoanTransactionHistory.Add(loantransactionhistory);

                                                                LoanTransactionHistory loantransaction = new LoanTransactionHistory()
                                                                {
                                                                    TransactionType = Enumerations.TransactionType.Credit,
                                                                    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                    C2BTransactionId = c2bnotification.Id,
                                                                    TransactionAmount = c2bnotification.TransAmount,
                                                                    TotalTransactionAmount = c2bnotification.TransAmount,
                                                                    ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                    ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                    CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                    Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                    CreatedBy = "System",
                                                                    IsApproved = true,
                                                                    ApprovedBy = "System",
                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                    TransactionBatchNumber = batchNo
                                                                };
                                                                datacontext.LoanTransactionHistory.Add(loantransaction);
                                                                #endregion

                                                                //Update C2B Transaction Status
                                                                c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                                                var entry = datacontext.Entry(c2bnotification);
                                                                entry.State = EntityState.Modified;

                                                                #region Generate Payment Notification
                                                                StringBuilder bodyBuilder = new StringBuilder();
                                                                StringBuilder textBodyBuilder = new StringBuilder();
                                                                bodyBuilder.Append(string.Format("Dear {0}", findCustomer.FullName));
                                                                bodyBuilder.AppendLine("<br />");
                                                                textBodyBuilder.Append(string.Format("Dear {0} ", findCustomer.FirstName));
                                                                string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                                                                bodyBuilder.AppendLine("<br />");
                                                                string borrowloanlink = "<a href='" + mvcclientendposint + "'/Loanrequest/newloanrequest> click here</a>";

                                                                bodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                                bodyBuilder.AppendLine("<br />");
                                                                bodyBuilder.AppendLine("<br />");
                                                                textBodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                                string TextSignature = ConfigurationManager.AppSettings["TextSignature"];
                                                                bodyBuilder.AppendLine("Thank you for being our valued client.");
                                                                bodyBuilder.AppendLine("<br />");
                                                                bodyBuilder.AppendLine("<br />");
                                                                textBodyBuilder.Append(" Thank you.");
                                                                textBodyBuilder.Append(TextSignature);
                                                                bodyBuilder.Append(emailSignature);

                                                                var emailAlertDTO = new EmailAlert
                                                                {
                                                                    MailMessageFrom = DefaultSettings.Instance.Email,
                                                                    MailMessageTo = string.Format("{0}", findCustomer.EmailAddress),
                                                                    MailMessageSubject = string.Format("Loan Repayment - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                                                    MailMessageBody = string.Format("{0}", bodyBuilder),
                                                                    MailMessageIsBodyHtml = true,
                                                                    MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                    MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                    MailMessageSecurityCritical = false,
                                                                    CreatedBy = "System",
                                                                    MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                                                };
                                                                //datacontext.EmailAlert.Add(emailAlertDTO);

                                                                var textalert = new TextAlert
                                                                {
                                                                    TextMessageRecipient = string.Format("{0}", findCustomer.MobileNumber),
                                                                    TextMessageBody = string.Format("{0}", textBodyBuilder),
                                                                    MaskedTextMessageBody = "",
                                                                    TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                    TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                    TextMessageSecurityCritical = false,
                                                                    CreatedBy = "",
                                                                    TextMessageSendRetry = 3
                                                                };
                                                                datacontext.TextAlert.Add(textalert);
                                                                #endregion
                                                            }
                                                        }

                                                        else if (findcustomeractiveloan == null && findStaticSetting.Value == "1")
                                                        {
                                                            #region Credit Customer Savings Account and mark C2B as processed
                                                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                            var debitMpesaRepaymentGL = new LoanTransactionHistory
                                                            {
                                                                TransactionType = Enumerations.TransactionType.Debit,
                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                TransactionAmount = c2bnotification.TransAmount,
                                                                TotalTransactionAmount = c2bnotification.TransAmount,
                                                                C2BTransactionId = c2bnotification.Id,
                                                                ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                                BranchId = findCustomer.BranchId,
                                                                TransactionBatchNumber = batchNo
                                                            };
                                                            datacontext.LoanTransactionHistory.Add(debitMpesaRepaymentGL);

                                                            var creditCustomerSavingAccount = new LoanTransactionHistory
                                                            {
                                                                TransactionType = Enumerations.TransactionType.Credit,
                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                C2BTransactionId = c2bnotification.Id,
                                                                TransactionAmount = c2bnotification.TransAmount,
                                                                TotalTransactionAmount = c2bnotification.TransAmount,
                                                                ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                                BranchId = findCustomer.BranchId,
                                                                TransactionBatchNumber = batchNo
                                                            };
                                                            datacontext.LoanTransactionHistory.Add(creditCustomerSavingAccount);

                                                            c2bnotification.Result = string.Format("Could not match active loan with provided mobile number {0}", c2bnotification.MSISDN);
                                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                                            var entry = datacontext.Entry(c2bnotification);
                                                            entry.State = EntityState.Modified;

                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            //Mark C2B as unknown
                                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Unknown;
                                                            c2bnotification.Result = string.Format("Could not find customer active loan - {0}", c2bnotification.MSISDN);
                                                            var entry = datacontext.Entry(c2bnotification);
                                                            entry.State = EntityState.Modified;
                                                        }
                                                    }

                                                    else
                                                    {
                                                        var findCustomerUsingMSISDN = _customerservice.FindCustomerByMobileNumber(c2bnotification.MSISDN);
                                                        if (findCustomerUsingMSISDN != null)
                                                        {
                                                            //find active loan
                                                            var findcustomeractiveloanusingmsisdn = (from a in datacontext.Customer
                                                                                                     join b in datacontext.CustomersAccount
                                                                                                         on a.Id equals b.CustomerId
                                                                                                     join c in datacontext.LoanRequest on b.Id equals c.CustomerAccountId
                                                                                                     where c.Status == (int)Enumerations.LoanStatus.Active && a.Id == findCustomerUsingMSISDN.Id
                                                                                                     select c).FirstOrDefault();
                                                            var findcustomersavingsaccountid = datacontext.CustomersAccount.Where(x => x.CustomerId == findCustomerUsingMSISDN.Id && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
                                                            if (findcustomeractiveloanusingmsisdn != null)
                                                            {

                                                                if (findcustomersavingsaccountid != null && findmpesarepaymentaccountid != null && finddefaultsavingaccountid != null)
                                                                {
                                                                    #region Dr M-Pesa Repayment GL, Cr Savings GL
                                                                    int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                                    LoanTransactionHistory loantransactionhistory = new LoanTransactionHistory()
                                                                    {
                                                                        TransactionType = Enumerations.TransactionType.Debit,
                                                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                        TransactionAmount = c2bnotification.TransAmount,
                                                                        TotalTransactionAmount = c2bnotification.TransAmount,
                                                                        C2BTransactionId = c2bnotification.Id,
                                                                        ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                        ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                        CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                        Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                        CreatedBy = "System",
                                                                        IsApproved = true,
                                                                        ApprovedBy = "System",
                                                                        TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                        TransactionBatchNumber = batchNo
                                                                    };
                                                                    datacontext.LoanTransactionHistory.Add(loantransactionhistory);

                                                                    LoanTransactionHistory loantransaction = new LoanTransactionHistory()
                                                                    {
                                                                        TransactionType = Enumerations.TransactionType.Credit,
                                                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                        C2BTransactionId = c2bnotification.Id,
                                                                        TransactionAmount = c2bnotification.TransAmount,
                                                                        TotalTransactionAmount = c2bnotification.TransAmount,
                                                                        ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                        ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                        CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                        Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                        CreatedBy = "System",
                                                                        IsApproved = true,
                                                                        ApprovedBy = "System",
                                                                        TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                        TransactionBatchNumber = batchNo
                                                                    };
                                                                    datacontext.LoanTransactionHistory.Add(loantransaction);
                                                                    #endregion

                                                                    //Update C2B Transaction Status
                                                                    c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                                                    var entry = datacontext.Entry(c2bnotification);
                                                                    entry.State = EntityState.Modified;

                                                                    #region Generate Payment Notification
                                                                    StringBuilder bodyBuilder = new StringBuilder();
                                                                    StringBuilder textBodyBuilder = new StringBuilder();
                                                                    bodyBuilder.Append(string.Format("Dear {0}", findCustomerUsingMSISDN.FullName));
                                                                    bodyBuilder.AppendLine("<br />");
                                                                    textBodyBuilder.Append(string.Format("Dear {0} ", findCustomerUsingMSISDN.FirstName));
                                                                    string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                                                                    bodyBuilder.AppendLine("<br />");
                                                                    string borrowloanlink = "<a href='" + mvcclientendposint + "'/Loanrequest/newloanrequest> click here</a>";

                                                                    bodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                                    bodyBuilder.AppendLine("<br />");
                                                                    bodyBuilder.AppendLine("<br />");
                                                                    textBodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                                    string TextSignature = ConfigurationManager.AppSettings["TextSignature"];
                                                                    bodyBuilder.AppendLine("Thank you for being our valued client.");
                                                                    bodyBuilder.AppendLine("<br />");
                                                                    bodyBuilder.AppendLine("<br />");
                                                                    textBodyBuilder.Append(" Thank you.");
                                                                    bodyBuilder.Append(emailSignature);
                                                                    textBodyBuilder.Append(TextSignature);
                                                                    var emailAlertDTO = new EmailAlert
                                                                    {
                                                                        MailMessageFrom = DefaultSettings.Instance.Email,
                                                                        MailMessageTo = string.Format("{0}", findCustomerUsingMSISDN.EmailAddress),
                                                                        MailMessageSubject = string.Format("Loan Repayment - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                                                        MailMessageBody = string.Format("{0}", bodyBuilder),
                                                                        MailMessageIsBodyHtml = true,
                                                                        MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                        MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                        MailMessageSecurityCritical = false,
                                                                        CreatedBy = "System",
                                                                        MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                                                    };
                                                                    //datacontext.EmailAlert.Add(emailAlertDTO);

                                                                    var textalert = new TextAlert
                                                                    {
                                                                        TextMessageRecipient = string.Format("{0}", findCustomerUsingMSISDN.MobileNumber),
                                                                        TextMessageBody = string.Format("{0}", textBodyBuilder),
                                                                        MaskedTextMessageBody = "",
                                                                        TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                        TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                        TextMessageSecurityCritical = false,
                                                                        CreatedBy = "",
                                                                        TextMessageSendRetry = 3
                                                                    };
                                                                    datacontext.TextAlert.Add(textalert);
                                                                    #endregion
                                                                }
                                                            }

                                                            else if (findcustomeractiveloanusingmsisdn == null && findStaticSetting.Value == "1")
                                                            {
                                                                #region Credit Customer Savings Account and mark C2B as processed
                                                                int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                                var debitMpesaRepaymentGL = new LoanTransactionHistory
                                                                {
                                                                    TransactionType = Enumerations.TransactionType.Debit,
                                                                    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                    TransactionAmount = c2bnotification.TransAmount,
                                                                    TotalTransactionAmount = c2bnotification.TransAmount,
                                                                    C2BTransactionId = c2bnotification.Id,
                                                                    ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                    ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                    CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                    Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                    CreatedBy = "System",
                                                                    IsApproved = true,
                                                                    ApprovedBy = "System",
                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                                    BranchId = findCustomerUsingMSISDN.BranchId,
                                                                    TransactionBatchNumber = batchNo
                                                                };
                                                                datacontext.LoanTransactionHistory.Add(debitMpesaRepaymentGL);

                                                                var creditCustomerSavingAccount = new LoanTransactionHistory
                                                                {
                                                                    TransactionType = Enumerations.TransactionType.Credit,
                                                                    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                    C2BTransactionId = c2bnotification.Id,
                                                                    TransactionAmount = c2bnotification.TransAmount,
                                                                    TotalTransactionAmount = c2bnotification.TransAmount,
                                                                    ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                    ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                    CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                    Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                    CreatedBy = "System",
                                                                    IsApproved = true,
                                                                    ApprovedBy = "System",
                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                                    BranchId = findCustomerUsingMSISDN.BranchId,
                                                                    TransactionBatchNumber = batchNo
                                                                };
                                                                datacontext.LoanTransactionHistory.Add(creditCustomerSavingAccount);

                                                                c2bnotification.Result = string.Format("Could not match active loan with provided mobile number {0}", c2bnotification.MSISDN);
                                                                c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                                                var entry = datacontext.Entry(c2bnotification);
                                                                entry.State = EntityState.Modified;

                                                                #endregion
                                                            }

                                                            else
                                                            {
                                                                //Mark C2B as unknown
                                                                c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Unknown;
                                                                c2bnotification.Result = string.Format("Could not find customer active loan - {0}", c2bnotification.MSISDN);
                                                                var entry = datacontext.Entry(c2bnotification);
                                                                entry.State = EntityState.Modified;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                            //#region Mark C2B as suspense
                                                            //var debitMpesaRepaymentGL = new LoanTransactionHistory
                                                            //{
                                                            //    TransactionType = Enumerations.TransactionType.Debit,
                                                            //    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                            //    TransactionAmount = c2bnotification.TransAmount,
                                                            //    TotalTransactionAmount = c2bnotification.TransAmount,
                                                            //    C2BTransactionId = c2bnotification.Id,
                                                            //    ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                            //    ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                            //    Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                            //    CreatedBy = "System",
                                                            //    IsApproved = true,
                                                            //    ApprovedBy = "System",
                                                            //    TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                            //    TransactionBatchNumber = batchNo
                                                            //};
                                                            //datacontext.LoanTransactionHistory.Add(debitMpesaRepaymentGL);

                                                            //var creditCustomerSavingAccount = new LoanTransactionHistory
                                                            //{
                                                            //    TransactionType = Enumerations.TransactionType.Credit,
                                                            //    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                            //    C2BTransactionId = c2bnotification.Id,
                                                            //    TransactionAmount = c2bnotification.TransAmount,
                                                            //    TotalTransactionAmount = c2bnotification.TransAmount,
                                                            //    ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                            //    ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                            //    Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                            //    CreatedBy = "System",
                                                            //    IsApproved = true,
                                                            //    ApprovedBy = "System",
                                                            //    TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                            //    TransactionBatchNumber = batchNo
                                                            //};
                                                            //datacontext.LoanTransactionHistory.Add(creditCustomerSavingAccount);

                                                            c2bnotification.Result = string.Format("Could not find customer with provided mobile number {0}", c2bnotification.MSISDN);
                                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Waiting;
                                                            var entry = datacontext.Entry(c2bnotification);
                                                            entry.State = EntityState.Modified;

                                                            //#endregion
                                                        }
                                                    }

                                                }
                                                //Use MSISDN
                                                else
                                                {
                                                    var findCustomerUsingMSISDN = _customerservice.FindCustomerByMobileNumber(c2bnotification.MSISDN);
                                                    if (findCustomerUsingMSISDN != null)
                                                    {
                                                        //find active loan
                                                        var findcustomeractiveloanusingmsisdn = (from a in datacontext.Customer
                                                                                                 join b in datacontext.CustomersAccount
                                                                                                     on a.Id equals b.CustomerId
                                                                                                 join c in datacontext.LoanRequest on b.Id equals c.CustomerAccountId
                                                                                                 where c.Status == (int)Enumerations.LoanStatus.Active && a.Id == findCustomerUsingMSISDN.Id
                                                                                                 select c).FirstOrDefault();
                                                        var findcustomersavingsaccountid = datacontext.CustomersAccount.Where(x => x.CustomerId == findCustomerUsingMSISDN.Id && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
                                                        if (findcustomeractiveloanusingmsisdn != null)
                                                        {
                                                            if (findcustomersavingsaccountid != null && findmpesarepaymentaccountid != null && finddefaultsavingaccountid != null)
                                                            {
                                                                int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                                #region Dr M-Pesa Repayment GL, Cr Savings GL
                                                                LoanTransactionHistory loantransactionhistory = new LoanTransactionHistory()
                                                                {
                                                                    TransactionType = Enumerations.TransactionType.Debit,
                                                                    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                    TransactionAmount = c2bnotification.TransAmount,
                                                                    TotalTransactionAmount = c2bnotification.TransAmount,
                                                                    C2BTransactionId = c2bnotification.Id,
                                                                    ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                    ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                    CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                    Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                    CreatedBy = "System",
                                                                    IsApproved = true,
                                                                    ApprovedBy = "System",
                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                    TransactionBatchNumber = batchNo
                                                                };
                                                                datacontext.LoanTransactionHistory.Add(loantransactionhistory);

                                                                LoanTransactionHistory loantransaction = new LoanTransactionHistory()
                                                                {
                                                                    TransactionType = Enumerations.TransactionType.Credit,
                                                                    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                    C2BTransactionId = c2bnotification.Id,
                                                                    TransactionAmount = c2bnotification.TransAmount,
                                                                    TotalTransactionAmount = c2bnotification.TransAmount,
                                                                    ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                    ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                    CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                    Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                    CreatedBy = "System",
                                                                    IsApproved = true,
                                                                    ApprovedBy = "System",
                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                    TransactionBatchNumber = batchNo
                                                                };
                                                                datacontext.LoanTransactionHistory.Add(loantransaction);
                                                                #endregion

                                                                //Update C2B Transaction Status
                                                                c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                                                var entry = datacontext.Entry(c2bnotification);
                                                                entry.State = EntityState.Modified;

                                                                #region Generate Payment Notification
                                                                StringBuilder bodyBuilder = new StringBuilder();
                                                                StringBuilder textBodyBuilder = new StringBuilder();
                                                                bodyBuilder.Append(string.Format("Dear {0}", findCustomerUsingMSISDN.FullName));
                                                                bodyBuilder.AppendLine("<br />");
                                                                textBodyBuilder.Append(string.Format("Dear {0} ", findCustomerUsingMSISDN.FirstName));
                                                                string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                                                                bodyBuilder.AppendLine("<br />");
                                                                string borrowloanlink = "<a href='" + mvcclientendposint + "'/Loanrequest/newloanrequest> click here</a>";

                                                                bodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                                bodyBuilder.AppendLine("<br />");
                                                                bodyBuilder.AppendLine("<br />");
                                                                textBodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                                string TextSignature = ConfigurationManager.AppSettings["TextSignature"];
                                                                bodyBuilder.AppendLine("Thank you for being our valued client.");
                                                                bodyBuilder.AppendLine("<br />");
                                                                bodyBuilder.AppendLine("<br />");
                                                                textBodyBuilder.Append(" Thank you.");
                                                                textBodyBuilder.Append(TextSignature);
                                                                bodyBuilder.Append(emailSignature);

                                                                var emailAlertDTO = new EmailAlert
                                                                {
                                                                    MailMessageFrom = DefaultSettings.Instance.Email,
                                                                    MailMessageTo = string.Format("{0}", findCustomerUsingMSISDN.EmailAddress),
                                                                    MailMessageSubject = string.Format("Loan Repayment - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                                                    MailMessageBody = string.Format("{0}", bodyBuilder),
                                                                    MailMessageIsBodyHtml = true,
                                                                    MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                    MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                    MailMessageSecurityCritical = false,
                                                                    CreatedBy = "System",
                                                                    MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                                                };
                                                                //datacontext.EmailAlert.Add(emailAlertDTO);

                                                                var textalert = new TextAlert
                                                                {
                                                                    TextMessageRecipient = string.Format("{0}", findCustomerUsingMSISDN.MobileNumber),
                                                                    TextMessageBody = string.Format("{0}", textBodyBuilder),
                                                                    MaskedTextMessageBody = "",
                                                                    TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                    TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                    TextMessageSecurityCritical = false,
                                                                    CreatedBy = "",
                                                                    TextMessageSendRetry = 3
                                                                };
                                                                datacontext.TextAlert.Add(textalert);
                                                                #endregion
                                                            }
                                                        }

                                                        else if (findcustomeractiveloanusingmsisdn == null && findStaticSetting.Value == "1")
                                                        {
                                                            //#region Credit Customer Savings Account and mark C2B as suspense
                                                            //int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                            //var debitMpesaRepaymentGL = new LoanTransactionHistory
                                                            //{
                                                            //    TransactionType = Enumerations.TransactionType.Debit,
                                                            //    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                            //    TransactionAmount = c2bnotification.TransAmount,
                                                            //    TotalTransactionAmount = c2bnotification.TransAmount,
                                                            //    C2BTransactionId = c2bnotification.Id,
                                                            //    ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                            //    ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                            //    CustomersAccountId = findcustomersavingsaccountid.Id,
                                                            //    Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                            //    CreatedBy = "System",
                                                            //    IsApproved = true,
                                                            //    ApprovedBy = "System",
                                                            //    TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                            //    BranchId = findCustomerUsingMSISDN.BranchId,
                                                            //    TransactionBatchNumber = batchNo
                                                            //};
                                                            //datacontext.LoanTransactionHistory.Add(debitMpesaRepaymentGL);

                                                            //var creditCustomerSavingAccount = new LoanTransactionHistory
                                                            //{
                                                            //    TransactionType = Enumerations.TransactionType.Credit,
                                                            //    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                            //    C2BTransactionId = c2bnotification.Id,
                                                            //    TransactionAmount = c2bnotification.TransAmount,
                                                            //    TotalTransactionAmount = c2bnotification.TransAmount,
                                                            //    ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                            //    ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                            //    CustomersAccountId = findcustomersavingsaccountid.Id,
                                                            //    Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                            //    CreatedBy = "System",
                                                            //    IsApproved = true,
                                                            //    ApprovedBy = "System",
                                                            //    TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                            //    BranchId = findCustomerUsingMSISDN.BranchId,
                                                            //    TransactionBatchNumber = batchNo
                                                            //};
                                                            //datacontext.LoanTransactionHistory.Add(creditCustomerSavingAccount);

                                                            c2bnotification.Result = string.Format("Could not find customer with provided mobile number {0}", c2bnotification.MSISDN);
                                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Waiting;
                                                            var entry = datacontext.Entry(c2bnotification);
                                                            entry.State = EntityState.Modified;

                                                            //#endregion
                                                        }
                                                        else
                                                        {
                                                            //Mark C2B as unknown
                                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Unknown;
                                                            c2bnotification.Result = string.Format("Could not find customer active loan - {0}", c2bnotification.MSISDN);
                                                            var entry = datacontext.Entry(c2bnotification);
                                                            entry.State = EntityState.Modified;
                                                        }
                                                    }

                                                    else
                                                    {
                                                        //#region Credit Customer Savings Account and mark C2B as suspense
                                                        //int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                        //var debitMpesaRepaymentGL = new LoanTransactionHistory
                                                        //{
                                                        //    TransactionType = Enumerations.TransactionType.Debit,
                                                        //    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                        //    TransactionAmount = c2bnotification.TransAmount,
                                                        //    TotalTransactionAmount = c2bnotification.TransAmount,
                                                        //    C2BTransactionId = c2bnotification.Id,
                                                        //    ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                        //    ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,                                                            
                                                        //    Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                        //    CreatedBy = "System",
                                                        //    IsApproved = true,
                                                        //    ApprovedBy = "System",
                                                        //    TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit, 
                                                        //    TransactionBatchNumber = batchNo
                                                        //};
                                                        //datacontext.LoanTransactionHistory.Add(debitMpesaRepaymentGL);

                                                        //var creditCustomerSavingAccount = new LoanTransactionHistory
                                                        //{
                                                        //    TransactionType = Enumerations.TransactionType.Credit,
                                                        //    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                        //    C2BTransactionId = c2bnotification.Id,
                                                        //    TransactionAmount = c2bnotification.TransAmount,
                                                        //    TotalTransactionAmount = c2bnotification.TransAmount,
                                                        //    ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                        //    ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,                                                            
                                                        //    Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                        //    CreatedBy = "System",
                                                        //    IsApproved = true,
                                                        //    ApprovedBy = "System",
                                                        //    TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,     
                                                        //    TransactionBatchNumber = batchNo
                                                        //};
                                                        //datacontext.LoanTransactionHistory.Add(creditCustomerSavingAccount);

                                                        c2bnotification.Result = string.Format("Could not find customer with provided mobile number {0}", c2bnotification.MSISDN);
                                                        c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Waiting;
                                                        var entry = datacontext.Entry(c2bnotification);
                                                        entry.State = EntityState.Modified;

                                                        //#endregion
                                                    }
                                                }
                                            }
                                            //Uses MSISDN
                                            else if (!string.IsNullOrWhiteSpace(c2bnotification.MSISDN))
                                            {
                                                var findCustomerUsingMSISDN = _customerservice.FindCustomerByMobileNumber(c2bnotification.MSISDN);
                                                if (findCustomerUsingMSISDN != null)
                                                {
                                                    //find active loan
                                                    var findcustomeractiveloanusingmsisdn = (from a in datacontext.Customer
                                                                                             join b in datacontext.CustomersAccount
                                                                                                 on a.Id equals b.CustomerId
                                                                                             join c in datacontext.LoanRequest on b.Id equals c.CustomerAccountId
                                                                                             where c.Status == (int)Enumerations.LoanStatus.Active && a.Id == findCustomerUsingMSISDN.Id
                                                                                             select c).FirstOrDefault();
                                                    var findcustomersavingsaccountid = datacontext.CustomersAccount.Where(x => x.CustomerId == findCustomerUsingMSISDN.Id && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
                                                    if (findcustomeractiveloanusingmsisdn != null)
                                                    {

                                                        if (findcustomersavingsaccountid != null && findmpesarepaymentaccountid != null && finddefaultsavingaccountid != null)
                                                        {
                                                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                            #region Dr M-Pesa Repayment GL, Cr Savings GL
                                                            LoanTransactionHistory loantransactionhistory = new LoanTransactionHistory()
                                                            {
                                                                TransactionType = Enumerations.TransactionType.Debit,
                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                TransactionAmount = c2bnotification.TransAmount,
                                                                TotalTransactionAmount = c2bnotification.TransAmount,
                                                                C2BTransactionId = c2bnotification.Id,
                                                                ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                TransactionBatchNumber = batchNo
                                                            };
                                                            datacontext.LoanTransactionHistory.Add(loantransactionhistory);

                                                            LoanTransactionHistory loantransaction = new LoanTransactionHistory()
                                                            {
                                                                TransactionType = Enumerations.TransactionType.Credit,
                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                C2BTransactionId = c2bnotification.Id,
                                                                TransactionAmount = c2bnotification.TransAmount,
                                                                TotalTransactionAmount = c2bnotification.TransAmount,
                                                                ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                TransactionBatchNumber = batchNo
                                                            };
                                                            datacontext.LoanTransactionHistory.Add(loantransaction);
                                                            #endregion

                                                            //Update C2B Transaction Status
                                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                                            var entry = datacontext.Entry(c2bnotification);
                                                            entry.State = EntityState.Modified;

                                                            #region Generate Payment Notification
                                                            StringBuilder bodyBuilder = new StringBuilder();
                                                            StringBuilder textBodyBuilder = new StringBuilder();
                                                            bodyBuilder.Append(string.Format("Dear {0}", findCustomerUsingMSISDN.FullName));
                                                            bodyBuilder.AppendLine("<br />");
                                                            textBodyBuilder.Append(string.Format("Dear {0} ", findCustomerUsingMSISDN.FirstName));
                                                            string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                                                            bodyBuilder.AppendLine("<br />");
                                                            string borrowloanlink = "<a href='" + mvcclientendposint + "'/Loanrequest/newloanrequest> click here</a>";

                                                            bodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                            bodyBuilder.AppendLine("<br />");
                                                            bodyBuilder.AppendLine("<br />");
                                                            textBodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                            string TextSignature = ConfigurationManager.AppSettings["TextSignature"];
                                                            bodyBuilder.AppendLine("Thank you for being our valued client.");
                                                            bodyBuilder.AppendLine("<br />");
                                                            bodyBuilder.AppendLine("<br />");
                                                            textBodyBuilder.Append(" Thank you.");
                                                            textBodyBuilder.Append(TextSignature);
                                                            bodyBuilder.Append(emailSignature);

                                                            var emailAlertDTO = new EmailAlert
                                                            {
                                                                MailMessageFrom = DefaultSettings.Instance.Email,
                                                                MailMessageTo = string.Format("{0}", findCustomerUsingMSISDN.EmailAddress),
                                                                MailMessageSubject = string.Format("Loan Repayment - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                                                MailMessageBody = string.Format("{0}", bodyBuilder),
                                                                MailMessageIsBodyHtml = true,
                                                                MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                MailMessageSecurityCritical = false,
                                                                CreatedBy = "System",
                                                                MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                                            };
                                                            //datacontext.EmailAlert.Add(emailAlertDTO);

                                                            var textalert = new TextAlert
                                                            {
                                                                TextMessageRecipient = string.Format("{0}", findCustomerUsingMSISDN.MobileNumber),
                                                                TextMessageBody = string.Format("{0}", textBodyBuilder),
                                                                MaskedTextMessageBody = "",
                                                                TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                TextMessageSecurityCritical = false,
                                                                CreatedBy = "",
                                                                TextMessageSendRetry = 3
                                                            };
                                                            datacontext.TextAlert.Add(textalert);
                                                            #endregion
                                                        }
                                                    }

                                                    else if (findcustomeractiveloanusingmsisdn == null && findStaticSetting.Value == "1")
                                                    {
                                                        int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                        #region Credit Customer Savings Account and mark C2B as processed
                                                        var debitMpesaRepaymentGL = new LoanTransactionHistory
                                                        {
                                                            TransactionType = Enumerations.TransactionType.Debit,
                                                            RepaymentType = Enumerations.RepaymentType.MPesa,
                                                            TransactionAmount = c2bnotification.TransAmount,
                                                            TotalTransactionAmount = c2bnotification.TransAmount,
                                                            C2BTransactionId = c2bnotification.Id,
                                                            ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                            ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                            CustomersAccountId = findcustomersavingsaccountid.Id,
                                                            Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                            CreatedBy = "System",
                                                            IsApproved = true,
                                                            ApprovedBy = "System",
                                                            TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                            BranchId = findCustomerUsingMSISDN.BranchId,
                                                            TransactionBatchNumber = batchNo
                                                        };
                                                        datacontext.LoanTransactionHistory.Add(debitMpesaRepaymentGL);

                                                        var creditCustomerSavingAccount = new LoanTransactionHistory
                                                        {
                                                            TransactionType = Enumerations.TransactionType.Credit,
                                                            RepaymentType = Enumerations.RepaymentType.MPesa,
                                                            C2BTransactionId = c2bnotification.Id,
                                                            TransactionAmount = c2bnotification.TransAmount,
                                                            TotalTransactionAmount = c2bnotification.TransAmount,
                                                            ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                            ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                            CustomersAccountId = findcustomersavingsaccountid.Id,
                                                            Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                            CreatedBy = "System",
                                                            IsApproved = true,
                                                            ApprovedBy = "System",
                                                            TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                            BranchId = findCustomerUsingMSISDN.BranchId,
                                                            TransactionBatchNumber = batchNo
                                                        };
                                                        datacontext.LoanTransactionHistory.Add(creditCustomerSavingAccount);

                                                        c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                                        var entry = datacontext.Entry(c2bnotification);
                                                        entry.State = EntityState.Modified;

                                                        #endregion
                                                    }

                                                    else
                                                    {
                                                        //Mark C2B as unknown
                                                        c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Unknown;
                                                        c2bnotification.Result = string.Format("Could not find customer active loan - {0}", c2bnotification.MSISDN);
                                                        var entry = datacontext.Entry(c2bnotification);
                                                        entry.State = EntityState.Modified;
                                                    }
                                                }

                                                else
                                                {
                                                    //#region Credit Customer Savings Account and mark C2B as suspense
                                                    //int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                    //var debitMpesaRepaymentGL = new LoanTransactionHistory
                                                    //{
                                                    //    TransactionType = Enumerations.TransactionType.Debit,
                                                    //    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                    //    TransactionAmount = c2bnotification.TransAmount,
                                                    //    TotalTransactionAmount = c2bnotification.TransAmount,
                                                    //    C2BTransactionId = c2bnotification.Id,
                                                    //    ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                    //    ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                    //    Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                    //    CreatedBy = "System",
                                                    //    IsApproved = true,
                                                    //    ApprovedBy = "System",
                                                    //    TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                    //    TransactionBatchNumber = batchNo
                                                    //};
                                                    //datacontext.LoanTransactionHistory.Add(debitMpesaRepaymentGL);

                                                    //var creditCustomerSavingAccount = new LoanTransactionHistory
                                                    //{
                                                    //    TransactionType = Enumerations.TransactionType.Credit,
                                                    //    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                    //    C2BTransactionId = c2bnotification.Id,
                                                    //    TransactionAmount = c2bnotification.TransAmount,
                                                    //    TotalTransactionAmount = c2bnotification.TransAmount,
                                                    //    ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                    //    ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                    //    Description = c2bnotification.BillRefNumber + " " + c2bnotification.MSISDN + " " + c2bnotification.TransID,
                                                    //    CreatedBy = "System",
                                                    //    IsApproved = true,
                                                    //    ApprovedBy = "System",
                                                    //    TransactionCategory = (int)Enumerations.TransactionCategory.MPesaDeposit,
                                                    //    TransactionBatchNumber = batchNo
                                                    //};
                                                    //datacontext.LoanTransactionHistory.Add(creditCustomerSavingAccount);

                                                    c2bnotification.Result = string.Format("Could not match customer with provided mobile number {0}", c2bnotification.MSISDN);
                                                    c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Waiting;
                                                    var entry = datacontext.Entry(c2bnotification);
                                                    entry.State = EntityState.Modified;

                                                    //#endregion
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //Mark C2B as unknown
                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Duplicate;
                                            var entry = datacontext.Entry(c2bnotification);
                                            entry.State = EntityState.Modified;
                                        }
                                    }
                                    datacontext.SaveChanges();
                                    transaction.Commit();
                                }

                                catch (Exception ex)
                                {
                                    transaction.Rollback();
                                    LoggerFactory.CreateLog().LogError("Error => {0}", ex);
                                }
                            }
                        }
                        #endregion

                        break;

                    case Nikopeshe.Entity.DataModels.Enumerations.MessageCategory.C2BReversal:

                        #region C2B Transaction Reversal

                        using (var datacontext = new DataContext())
                        {
                            using (var trx = datacontext.Database.BeginTransaction())
                            {
                                try
                                {
                                    var recordId = (Guid)e.Message.Body;
                                    var c2bnotification = datacontext.C2BTransaction.Find(recordId);
                                    if (c2bnotification != null)
                                    {
                                        var findLoanTransactionHistoryByC2BId = datacontext.LoanTransactionHistory.Where(x => x.C2BTransactionId == c2bnotification.Id).ToList();

                                        if (findLoanTransactionHistoryByC2BId != null && findLoanTransactionHistoryByC2BId.Any())
                                        {
                                            foreach (var transaction in findLoanTransactionHistoryByC2BId)
                                            {
                                                int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                if (transaction.TransactionType == Enumerations.TransactionType.Credit)
                                                {
                                                    LoanTransactionHistory reverseCredit = new LoanTransactionHistory
                                                    {
                                                        TransactionType = Enumerations.TransactionType.Debit,
                                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                                        TransactionAmount = transaction.TransactionAmount,
                                                        TotalTransactionAmount = transaction.TotalTransactionAmount,
                                                        C2BTransactionId = c2bnotification.Id,
                                                        ChartofAccountId = transaction.ChartofAccountId,
                                                        ContraAccountId = transaction.ContraAccountId,
                                                        CustomersAccountId = transaction.CustomersAccountId,
                                                        Description = string.Format("Reversal - {0}", transaction.Description),
                                                        CreatedBy = "System",
                                                        IsApproved = true,
                                                        ApprovedBy = "System",
                                                        TransactionCategory = (int)Enumerations.TransactionCategory.Reversal,
                                                        TransactionBatchNumber = batchNo
                                                    };

                                                    datacontext.LoanTransactionHistory.Add(reverseCredit);
                                                }
                                                else
                                                {
                                                    LoanTransactionHistory reverseDebit = new LoanTransactionHistory
                                                    {
                                                        TransactionType = Enumerations.TransactionType.Credit,
                                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                                        TransactionAmount = transaction.TransactionAmount,
                                                        TotalTransactionAmount = transaction.TotalTransactionAmount,
                                                        C2BTransactionId = c2bnotification.Id,
                                                        ChartofAccountId = transaction.ChartofAccountId,
                                                        ContraAccountId = transaction.ContraAccountId,
                                                        CustomersAccountId = transaction.CustomersAccountId,
                                                        Description = string.Format("Reversal - {0}", transaction.Description),
                                                        CreatedBy = "System",
                                                        IsApproved = true,
                                                        ApprovedBy = "System",
                                                        TransactionCategory = (int)Enumerations.TransactionCategory.Reversal,
                                                        TransactionBatchNumber = batchNo
                                                    };

                                                    datacontext.LoanTransactionHistory.Add(reverseDebit);
                                                }
                                            }
                                        }

                                        c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Reversed;
                                        var updateC2BTransaction = datacontext.Entry(c2bnotification);
                                        updateC2BTransaction.State = EntityState.Modified;
                                    }
                                    datacontext.SaveChanges();
                                    trx.Commit();
                                }

                                catch (Exception ex)
                                {
                                    trx.Rollback();
                                    LoggerFactory.CreateLog().LogInfo("{0}->C2BReversal..." + ex + Description);
                                }
                            }
                        }

                        #endregion

                        break;

                    case Nikopeshe.Entity.DataModels.Enumerations.MessageCategory.C2BWaitingProcessing:

                        #region C2B Waiting Processing

                        using (var datacontext = new DataContext())
                        {
                            using (var transaction = datacontext.Database.BeginTransaction())
                            {
                                try
                                {
                                    var recordId = (Guid)e.Message.Body;
                                    var c2bnotification = datacontext.C2BTransaction.Find(recordId);
                                    if (c2bnotification != null)
                                    {
                                        //find m-pesa repayment gl account id 
                                        var findmpesarepaymentaccountid = datacontext.SystemGeneralLedgerAccountMapping.Where(x => x.SystemGeneralLedgerAccountCode == Enumerations.SystemGeneralLedgerAccountCode.MPesaRepaymentAccount).FirstOrDefault();
                                        var findmpessuspenseaccountid = datacontext.SystemGeneralLedgerAccountMapping.Where(x => x.SystemGeneralLedgerAccountCode == Enumerations.SystemGeneralLedgerAccountCode.MPesaSuspsnseAccount).FirstOrDefault();
                                        var finddefaultsavingaccountid = datacontext.SavingsProduct.ToList().FirstOrDefault();
                                        var findLoanTransactionHistoryByC2BId = _iipnservice.FindLoanTransactionHistoryByC2BTransactionId(c2bnotification.Id);
                                        var findStaticSetting = datacontext.StaticSetting.Where(x => x.Key == "CREDITSAVINGSACCOUNT").FirstOrDefault();

                                        if (findLoanTransactionHistoryByC2BId == null)
                                        {
                                            if (!string.IsNullOrWhiteSpace(c2bnotification.BillRefNumber))
                                            {
                                                //Check if Bill Ref No is greater or equal to 9 characters
                                                if (c2bnotification.BillRefNumber.Length != 0)
                                                {
                                                    string billRefNo = c2bnotification.BillRefNumber;
                                                    //string customerMobileNumber = "254" + billRefNo.Substring(billRefNo.Length - 9);

                                                    //Find Customer Using Bill Ref No.
                                                    var findCustomer = _customerservice.FindCustomerByIdNumber(billRefNo);

                                                    if (findCustomer != null)
                                                    {
                                                        var findcustomersavingsaccountid = datacontext.CustomersAccount.Where(x => x.CustomerId == findCustomer.Id && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
                                                        if (findcustomersavingsaccountid != null && findmpesarepaymentaccountid != null && finddefaultsavingaccountid != null)
                                                        {
                                                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                            #region Dr M-Pesa Repayment GL, Cr Savings GL
                                                            LoanTransactionHistory loantransactionhistory = new LoanTransactionHistory()
                                                            {
                                                                TransactionType = Enumerations.TransactionType.Debit,
                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                TransactionAmount = c2bnotification.TransAmount,
                                                                TotalTransactionAmount = c2bnotification.TransAmount,
                                                                C2BTransactionId = c2bnotification.Id,
                                                                ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                TransactionBatchNumber = batchNo
                                                            };
                                                            datacontext.LoanTransactionHistory.Add(loantransactionhistory);

                                                            LoanTransactionHistory loantransaction = new LoanTransactionHistory()
                                                            {
                                                                TransactionType = Enumerations.TransactionType.Credit,
                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                C2BTransactionId = c2bnotification.Id,
                                                                TransactionAmount = c2bnotification.TransAmount,
                                                                TotalTransactionAmount = c2bnotification.TransAmount,
                                                                ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                TransactionBatchNumber = batchNo
                                                            };
                                                            datacontext.LoanTransactionHistory.Add(loantransaction);
                                                            #endregion

                                                            //Update C2B Transaction Status
                                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                                            var entry = datacontext.Entry(c2bnotification);
                                                            entry.State = EntityState.Modified;

                                                            #region Generate Payment Notification
                                                            StringBuilder bodyBuilder = new StringBuilder();
                                                            StringBuilder textBodyBuilder = new StringBuilder();
                                                            bodyBuilder.Append(string.Format("Dear {0}", findCustomer.FullName));
                                                            bodyBuilder.AppendLine("<br />");
                                                            textBodyBuilder.Append(string.Format("Dear {0} ", findCustomer.FirstName));
                                                            string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                                                            bodyBuilder.AppendLine("<br />");
                                                            string borrowloanlink = "<a href='" + mvcclientendposint + "'/Loanrequest/newloanrequest> click here</a>";

                                                            bodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                            bodyBuilder.AppendLine("<br />");
                                                            bodyBuilder.AppendLine("<br />");
                                                            textBodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                            string TextSignature = ConfigurationManager.AppSettings["TextSignature"];
                                                            bodyBuilder.AppendLine("Thank you for being our valued client.");
                                                            bodyBuilder.AppendLine("<br />");
                                                            bodyBuilder.AppendLine("<br />");
                                                            textBodyBuilder.Append(" Thank you.");
                                                            textBodyBuilder.Append(TextSignature);
                                                            bodyBuilder.Append(emailSignature);

                                                            var emailAlertDTO = new EmailAlert
                                                            {
                                                                MailMessageFrom = DefaultSettings.Instance.Email,
                                                                MailMessageTo = string.Format("{0}", findCustomer.EmailAddress),
                                                                MailMessageSubject = string.Format("Loan Repayment - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                                                MailMessageBody = string.Format("{0}", bodyBuilder),
                                                                MailMessageIsBodyHtml = true,
                                                                MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                MailMessageSecurityCritical = false,
                                                                CreatedBy = "System",
                                                                MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                                            };
                                                            //datacontext.EmailAlert.Add(emailAlertDTO);

                                                            var textalert = new TextAlert
                                                            {
                                                                TextMessageRecipient = string.Format("{0}", findCustomer.MobileNumber),
                                                                TextMessageBody = string.Format("{0}", textBodyBuilder),
                                                                MaskedTextMessageBody = "",
                                                                TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                TextMessageSecurityCritical = false,
                                                                CreatedBy = "",
                                                                TextMessageSendRetry = 3
                                                            };
                                                            datacontext.TextAlert.Add(textalert);
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            //Update C2B Transaction Status
                                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Waiting;
                                                            var entry = datacontext.Entry(c2bnotification);
                                                            entry.State = EntityState.Modified;
                                                        }
                                                    }

                                                    else
                                                    {
                                                        var findCustomerUsingMSISDN = _customerservice.FindCustomerByMobileNumber(c2bnotification.MSISDN);
                                                        if (findCustomerUsingMSISDN != null)
                                                        {
                                                            var findcustomersavingsaccountid = datacontext.CustomersAccount.Where(x => x.CustomerId == findCustomerUsingMSISDN.Id && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
                                                            if (findcustomersavingsaccountid != null && findmpesarepaymentaccountid != null && finddefaultsavingaccountid != null)
                                                            {
                                                                #region Dr M-Pesa Repayment GL, Cr Savings GL
                                                                int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                                LoanTransactionHistory loantransactionhistory = new LoanTransactionHistory()
                                                                {
                                                                    TransactionType = Enumerations.TransactionType.Debit,
                                                                    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                    TransactionAmount = c2bnotification.TransAmount,
                                                                    TotalTransactionAmount = c2bnotification.TransAmount,
                                                                    C2BTransactionId = c2bnotification.Id,
                                                                    ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                    ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                    CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                    Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                    CreatedBy = "System",
                                                                    IsApproved = true,
                                                                    ApprovedBy = "System",
                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                    TransactionBatchNumber = batchNo
                                                                };
                                                                datacontext.LoanTransactionHistory.Add(loantransactionhistory);

                                                                LoanTransactionHistory loantransaction = new LoanTransactionHistory()
                                                                {
                                                                    TransactionType = Enumerations.TransactionType.Credit,
                                                                    RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                    C2BTransactionId = c2bnotification.Id,
                                                                    TransactionAmount = c2bnotification.TransAmount,
                                                                    TotalTransactionAmount = c2bnotification.TransAmount,
                                                                    ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                    ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                    CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                    Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                    CreatedBy = "System",
                                                                    IsApproved = true,
                                                                    ApprovedBy = "System",
                                                                    TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                    TransactionBatchNumber = batchNo
                                                                };
                                                                datacontext.LoanTransactionHistory.Add(loantransaction);
                                                                #endregion

                                                                //Update C2B Transaction Status
                                                                c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                                                var entry = datacontext.Entry(c2bnotification);
                                                                entry.State = EntityState.Modified;

                                                                #region Generate Payment Notification
                                                                StringBuilder bodyBuilder = new StringBuilder();
                                                                StringBuilder textBodyBuilder = new StringBuilder();
                                                                bodyBuilder.Append(string.Format("Dear {0}", findCustomerUsingMSISDN.FullName));
                                                                bodyBuilder.AppendLine("<br />");
                                                                textBodyBuilder.Append(string.Format("Dear {0} ", findCustomerUsingMSISDN.FirstName));
                                                                string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                                                                bodyBuilder.AppendLine("<br />");
                                                                string borrowloanlink = "<a href='" + mvcclientendposint + "'/Loanrequest/newloanrequest> click here</a>";

                                                                bodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                                bodyBuilder.AppendLine("<br />");
                                                                bodyBuilder.AppendLine("<br />");
                                                                textBodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                                string TextSignature = ConfigurationManager.AppSettings["TextSignature"];
                                                                bodyBuilder.AppendLine("Thank you for being our valued client.");
                                                                bodyBuilder.AppendLine("<br />");
                                                                bodyBuilder.AppendLine("<br />");
                                                                textBodyBuilder.Append(" Thank you.");
                                                                bodyBuilder.Append(emailSignature);
                                                                textBodyBuilder.Append(TextSignature);
                                                                var emailAlertDTO = new EmailAlert
                                                                {
                                                                    MailMessageFrom = DefaultSettings.Instance.Email,
                                                                    MailMessageTo = string.Format("{0}", findCustomerUsingMSISDN.EmailAddress),
                                                                    MailMessageSubject = string.Format("Loan Repayment - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                                                    MailMessageBody = string.Format("{0}", bodyBuilder),
                                                                    MailMessageIsBodyHtml = true,
                                                                    MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                    MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                    MailMessageSecurityCritical = false,
                                                                    CreatedBy = "System",
                                                                    MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                                                };
                                                                //datacontext.EmailAlert.Add(emailAlertDTO);

                                                                var textalert = new TextAlert
                                                                {
                                                                    TextMessageRecipient = string.Format("{0}", findCustomerUsingMSISDN.MobileNumber),
                                                                    TextMessageBody = string.Format("{0}", textBodyBuilder),
                                                                    MaskedTextMessageBody = "",
                                                                    TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                    TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                    TextMessageSecurityCritical = false,
                                                                    CreatedBy = "",
                                                                    TextMessageSendRetry = 3
                                                                };
                                                                datacontext.TextAlert.Add(textalert);
                                                                #endregion
                                                            }
                                                            else
                                                            {
                                                                //Mark C2B as unknown
                                                                c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Waiting;
                                                                var entry = datacontext.Entry(c2bnotification);
                                                                entry.State = EntityState.Modified;
                                                            }
                                                        }
                                                        else
                                                        {

                                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Waiting;
                                                            var entry = datacontext.Entry(c2bnotification);
                                                            entry.State = EntityState.Modified;
                                                        }
                                                    }

                                                }
                                                //Use MSISDN
                                                else
                                                {
                                                    var findCustomerUsingMSISDN = _customerservice.FindCustomerByMobileNumber(c2bnotification.MSISDN);
                                                    if (findCustomerUsingMSISDN != null)
                                                    {
                                                        var findcustomersavingsaccountid = datacontext.CustomersAccount.Where(x => x.CustomerId == findCustomerUsingMSISDN.Id && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
                                                        if (findcustomersavingsaccountid != null && findmpesarepaymentaccountid != null && finddefaultsavingaccountid != null)
                                                        {
                                                            int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                            #region Dr M-Pesa Repayment GL, Cr Savings GL
                                                            LoanTransactionHistory loantransactionhistory = new LoanTransactionHistory()
                                                            {
                                                                TransactionType = Enumerations.TransactionType.Debit,
                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                TransactionAmount = c2bnotification.TransAmount,
                                                                TotalTransactionAmount = c2bnotification.TransAmount,
                                                                C2BTransactionId = c2bnotification.Id,
                                                                ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                TransactionBatchNumber = batchNo
                                                            };
                                                            datacontext.LoanTransactionHistory.Add(loantransactionhistory);

                                                            LoanTransactionHistory loantransaction = new LoanTransactionHistory()
                                                            {
                                                                TransactionType = Enumerations.TransactionType.Credit,
                                                                RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                C2BTransactionId = c2bnotification.Id,
                                                                TransactionAmount = c2bnotification.TransAmount,
                                                                TotalTransactionAmount = c2bnotification.TransAmount,
                                                                ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                                ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                                CustomersAccountId = findcustomersavingsaccountid.Id,
                                                                Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                                CreatedBy = "System",
                                                                IsApproved = true,
                                                                ApprovedBy = "System",
                                                                TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                TransactionBatchNumber = batchNo
                                                            };
                                                            datacontext.LoanTransactionHistory.Add(loantransaction);
                                                            #endregion

                                                            //Update C2B Transaction Status
                                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                                            var entry = datacontext.Entry(c2bnotification);
                                                            entry.State = EntityState.Modified;

                                                            #region Generate Payment Notification
                                                            StringBuilder bodyBuilder = new StringBuilder();
                                                            StringBuilder textBodyBuilder = new StringBuilder();
                                                            bodyBuilder.Append(string.Format("Dear {0}", findCustomerUsingMSISDN.FullName));
                                                            bodyBuilder.AppendLine("<br />");
                                                            textBodyBuilder.Append(string.Format("Dear {0} ", findCustomerUsingMSISDN.FirstName));
                                                            string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                                                            bodyBuilder.AppendLine("<br />");
                                                            string borrowloanlink = "<a href='" + mvcclientendposint + "'/Loanrequest/newloanrequest> click here</a>";

                                                            bodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                            bodyBuilder.AppendLine("<br />");
                                                            bodyBuilder.AppendLine("<br />");
                                                            textBodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                            string TextSignature = ConfigurationManager.AppSettings["TextSignature"];
                                                            bodyBuilder.AppendLine("Thank you for being our valued client.");
                                                            bodyBuilder.AppendLine("<br />");
                                                            bodyBuilder.AppendLine("<br />");
                                                            textBodyBuilder.Append(" Thank you.");
                                                            textBodyBuilder.Append(TextSignature);
                                                            bodyBuilder.Append(emailSignature);

                                                            var emailAlertDTO = new EmailAlert
                                                            {
                                                                MailMessageFrom = DefaultSettings.Instance.Email,
                                                                MailMessageTo = string.Format("{0}", findCustomerUsingMSISDN.EmailAddress),
                                                                MailMessageSubject = string.Format("Loan Repayment - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                                                MailMessageBody = string.Format("{0}", bodyBuilder),
                                                                MailMessageIsBodyHtml = true,
                                                                MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                MailMessageSecurityCritical = false,
                                                                CreatedBy = "System",
                                                                MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                                            };
                                                            //datacontext.EmailAlert.Add(emailAlertDTO);

                                                            var textalert = new TextAlert
                                                            {
                                                                TextMessageRecipient = string.Format("{0}", findCustomerUsingMSISDN.MobileNumber),
                                                                TextMessageBody = string.Format("{0}", textBodyBuilder),
                                                                MaskedTextMessageBody = "",
                                                                TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                                TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                                TextMessageSecurityCritical = false,
                                                                CreatedBy = "",
                                                                TextMessageSendRetry = 3
                                                            };
                                                            datacontext.TextAlert.Add(textalert);
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            //Mark C2B as Waiting
                                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Waiting;
                                                            var entry = datacontext.Entry(c2bnotification);
                                                            entry.State = EntityState.Modified;
                                                        }
                                                    }

                                                    else
                                                    {
                                                        c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Waiting;
                                                        var entry = datacontext.Entry(c2bnotification);
                                                        entry.State = EntityState.Modified;
                                                    }
                                                }
                                            }
                                            //Uses MSISDN
                                            else if (!string.IsNullOrWhiteSpace(c2bnotification.MSISDN))
                                            {
                                                var findCustomerUsingMSISDN = _customerservice.FindCustomerByMobileNumber(c2bnotification.MSISDN);
                                                if (findCustomerUsingMSISDN != null)
                                                {
                                                    var findcustomersavingsaccountid = datacontext.CustomersAccount.Where(x => x.CustomerId == findCustomerUsingMSISDN.Id && x.AccountType == (int)Enumerations.ProductCode.Savings).FirstOrDefault();
                                                    if (findcustomersavingsaccountid != null && findmpesarepaymentaccountid != null && finddefaultsavingaccountid != null)
                                                    {
                                                        int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                        #region Dr M-Pesa Repayment GL, Cr Savings GL
                                                        LoanTransactionHistory loantransactionhistory = new LoanTransactionHistory()
                                                        {
                                                            TransactionType = Enumerations.TransactionType.Debit,
                                                            RepaymentType = Enumerations.RepaymentType.MPesa,
                                                            TransactionAmount = c2bnotification.TransAmount,
                                                            TotalTransactionAmount = c2bnotification.TransAmount,
                                                            C2BTransactionId = c2bnotification.Id,
                                                            ChartofAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                            ContraAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                            CustomersAccountId = findcustomersavingsaccountid.Id,
                                                            Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                            CreatedBy = "System",
                                                            IsApproved = true,
                                                            ApprovedBy = "System",
                                                            TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                            TransactionBatchNumber = batchNo
                                                        };
                                                        datacontext.LoanTransactionHistory.Add(loantransactionhistory);

                                                        LoanTransactionHistory loantransaction = new LoanTransactionHistory()
                                                        {
                                                            TransactionType = Enumerations.TransactionType.Credit,
                                                            RepaymentType = Enumerations.RepaymentType.MPesa,
                                                            C2BTransactionId = c2bnotification.Id,
                                                            TransactionAmount = c2bnotification.TransAmount,
                                                            TotalTransactionAmount = c2bnotification.TransAmount,
                                                            ChartofAccountId = finddefaultsavingaccountid.ChartofAccountId,
                                                            ContraAccountId = findmpesarepaymentaccountid.ChartofAccountId,
                                                            CustomersAccountId = findcustomersavingsaccountid.Id,
                                                            Description = string.Format("M-Pesa Deposit - {0} {1} {2}", c2bnotification.BillRefNumber, c2bnotification.MSISDN, c2bnotification.TransID),
                                                            CreatedBy = "System",
                                                            IsApproved = true,
                                                            ApprovedBy = "System",
                                                            TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                            TransactionBatchNumber = batchNo
                                                        };
                                                        datacontext.LoanTransactionHistory.Add(loantransaction);
                                                        #endregion

                                                        //Update C2B Transaction Status
                                                        c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                                        var entry = datacontext.Entry(c2bnotification);
                                                        entry.State = EntityState.Modified;

                                                        #region Generate Payment Notification
                                                        StringBuilder bodyBuilder = new StringBuilder();
                                                        StringBuilder textBodyBuilder = new StringBuilder();
                                                        bodyBuilder.Append(string.Format("Dear {0}", findCustomerUsingMSISDN.FullName));
                                                        bodyBuilder.AppendLine("<br />");
                                                        textBodyBuilder.Append(string.Format("Dear {0} ", findCustomerUsingMSISDN.FirstName));
                                                        string mvcclientendposint = ConfigurationManager.AppSettings["MvcClientURL"].ToString();
                                                        bodyBuilder.AppendLine("<br />");
                                                        string borrowloanlink = "<a href='" + mvcclientendposint + "'/Loanrequest/newloanrequest> click here</a>";

                                                        bodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                        bodyBuilder.AppendLine("<br />");
                                                        bodyBuilder.AppendLine("<br />");
                                                        textBodyBuilder.Append(string.Format("Your payment of KES{0} has been received and processed successfully.", c2bnotification.TransAmount));
                                                        string TextSignature = ConfigurationManager.AppSettings["TextSignature"];
                                                        bodyBuilder.AppendLine("Thank you for being our valued client.");
                                                        bodyBuilder.AppendLine("<br />");
                                                        bodyBuilder.AppendLine("<br />");
                                                        textBodyBuilder.Append(" Thank you.");
                                                        textBodyBuilder.Append(TextSignature);
                                                        bodyBuilder.Append(emailSignature);

                                                        var emailAlertDTO = new EmailAlert
                                                        {
                                                            MailMessageFrom = DefaultSettings.Instance.Email,
                                                            MailMessageTo = string.Format("{0}", findCustomerUsingMSISDN.EmailAddress),
                                                            MailMessageSubject = string.Format("Loan Repayment - {0}", DefaultSettings.Instance.ApplicationDisplayName),
                                                            MailMessageBody = string.Format("{0}", bodyBuilder),
                                                            MailMessageIsBodyHtml = true,
                                                            MailMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                            MailMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                            MailMessageSecurityCritical = false,
                                                            CreatedBy = "System",
                                                            MailMessagePriority = (int)Enumerations.QueuePriority.Normal,
                                                        };
                                                        datacontext.EmailAlert.Add(emailAlertDTO);

                                                        var textalert = new TextAlert
                                                        {
                                                            TextMessageRecipient = string.Format("{0}", findCustomerUsingMSISDN.MobileNumber),
                                                            TextMessageBody = string.Format("{0}", textBodyBuilder),
                                                            MaskedTextMessageBody = "",
                                                            TextMessageDLRStatus = (int)Enumerations.DLRStatus.Pending,
                                                            TextMessageOrigin = (int)Enumerations.MessageOrigin.Within,
                                                            TextMessageSecurityCritical = false,
                                                            CreatedBy = "",
                                                            TextMessageSendRetry = 3
                                                        };
                                                        datacontext.TextAlert.Add(textalert);
                                                        #endregion
                                                    }

                                                    else
                                                    {
                                                        //Mark C2B as waiting
                                                        c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Waiting;
                                                        var entry = datacontext.Entry(c2bnotification);
                                                        entry.State = EntityState.Modified;
                                                    }
                                                }

                                                else
                                                {
                                                    //Mark C2B as waiting
                                                    c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Waiting;
                                                    var entry = datacontext.Entry(c2bnotification);
                                                    entry.State = EntityState.Modified;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //Mark C2B as processed
                                            c2bnotification.Status = (int)Enumerations.C2BTransactionStatus.Processed;
                                            var entry = datacontext.Entry(c2bnotification);
                                            entry.State = EntityState.Modified;
                                        }
                                    }
                                    datacontext.SaveChanges();
                                    transaction.Commit();
                                }

                                catch (Exception ex)
                                {
                                    transaction.Rollback();
                                    LoggerFactory.CreateLog().LogError("Error => {0}", ex);
                                }
                            }
                        }

                        #endregion

                        break;

                }
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogError("{0}->MessageQueue_ReceiveCompleted...", ex, Description);
            }
            finally
            {
                var messageQueue = (MessageQueue)sender;

                // listen for the next message.
                messageQueue.BeginReceive();
            }
        }
        #endregion
    }
}

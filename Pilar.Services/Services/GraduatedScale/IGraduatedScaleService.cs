﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.GraduatedScale
{
    public interface IGraduatedScaleService
    {
        List<Nikopeshe.Entity.DataModels.GraduatedScale> FindGraduatedScaleUsingRangeAndChargeId(decimal amount, Guid ChargeId);
    }
}

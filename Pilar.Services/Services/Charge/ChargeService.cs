﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Data;

namespace Pilar.Services.Services.Charge
{
    public class ChargeService : IChargeService
    {
        public Nikopeshe.Entity.DataModels.Charge FindChargeById(Guid Id)
        {
            DataContext dataContext = new DataContext();

            var findchargebyid = dataContext.Charge.Find(Id);

            return findchargebyid;
        }
    }
}

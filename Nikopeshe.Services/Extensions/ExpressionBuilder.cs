﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.Extensions
{
    public class ExpressionBuilder
    {
        public static Expression<Func<TValue, bool>> BuildOrExpressionTree<TValue, TCompareAgainst>(
        IEnumerable<TCompareAgainst> wantedItems,
        Expression<Func<TValue, TCompareAgainst>> convertBetweenTypes)
        {
            ParameterExpression inputParam = convertBetweenTypes.Parameters[0];

            Expression binaryExpressionTree = BuildBinaryOrTree(wantedItems.GetEnumerator(), convertBetweenTypes.Body, null);

            return Expression.Lambda<Func<TValue, bool>>(binaryExpressionTree, new[] { inputParam });
        }

        private static Expression BuildBinaryOrTree<T>(IEnumerator<T> itemEnumerator, Expression expressionToCompareTo, Expression expression)
        {
            if (itemEnumerator.MoveNext() == false)
                return expression;

            ConstantExpression constant = Expression.Constant(itemEnumerator.Current, typeof(T));
            BinaryExpression comparison = Expression.Equal(expressionToCompareTo, constant);

            BinaryExpression newExpression;

            if (expression == null)
                newExpression = comparison;
            else
                newExpression = Expression.OrElse(expression, comparison);

            return BuildBinaryOrTree(itemEnumerator, expressionToCompareTo, newExpression);
        }
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class LoanProductService : ILoanProductService
    {
        private IRepository<LoanProduct> loanproductRepository;

        string errorMessage = string.Empty;

        public LoanProductService(IRepository<LoanProduct> _loanproduct)
        {
            if(_loanproduct == null)
                throw new ArgumentNullException("null repository");

            this.loanproductRepository = _loanproduct;
        }

        /// <summary>
        /// add new loan product
        /// </summary>
        /// <param name="loanproduct"></param>
        public void AddNewLoanProduct(LoanProduct loanproduct, ServiceHeader serviceHeader)
        {
            try
            {
                if (loanproduct == null)
                    throw new ArgumentNullException("null entity");

                loanproductRepository.Insert(loanproduct, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(errorMessage, ex);
            }
        }

        /// <summary>
        /// Get all loan products by id
        /// </summary>
        /// <returns></returns>
        public IList<LoanProduct> GetAllLoanProducts(IsolationLevel isolationLevel)
        {
            return loanproductRepository.GetAll(isolationLevel).ToList();
        }

        /// <summary>
        /// update loan product
        /// </summary>
        /// <param name="loanproduct"></param>
        public void UpdateLoanProduct(LoanProduct loanproduct, ServiceHeader serviceHeader)
        {
            try
            {
                if (loanproduct == null)
                    throw new ArgumentNullException("null entity");

                loanproductRepository.Update(loanproduct, serviceHeader);
            }

            catch(DbEntityValidationException ex)
            {
                foreach(var validationErrors in ex.EntityValidationErrors)
                {
                    foreach(var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(errorMessage, ex);
            }
        }

        /// <summary>
        /// delete loan product
        /// </summary>
        /// <param name="loanproduct"></param>
        public void DeleteLoanProduct(LoanProduct loanproduct, ServiceHeader serviceHeader)
        {
            try
            {
                if (loanproduct == null)
                    throw new ArgumentNullException("null entity");

                loanproductRepository.Delete(loanproduct, serviceHeader);
            }

            catch(DbEntityValidationException ex)
            {
                foreach(var validationErrors in ex.EntityValidationErrors)
                {
                    foreach(var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw new Exception(errorMessage, ex);
            }            
        }

        /// <summary>
        /// Find loan product by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public LoanProduct FindLoanProductById(Guid Id)
        {
            if (Id == Guid.Empty)
                throw new ArgumentNullException("null entity");

            return loanproductRepository.GetById(Id);
        }

        /// <summary>
        /// Find minimum loan amount
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Decimal FindMinimumLoanAmountById(Guid Id)
        {
            var amount = loanproductRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.Id == Id);
            var minimumloan = amount.Select(x => x.MinimumAmount).FirstOrDefault();
            return minimumloan;
        }

        /// <summary>
        /// Finfd maximum loan amount
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Decimal FindMaximumLoanAmountById(Guid id)
        {
            var amount = loanproductRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.Id == id);
            var maxloan = amount.Select(x => x.MaximumAmount).FirstOrDefault();
            return maxloan;
        }

        /// <summary>
        /// Get defualt loan amount using product id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Decimal GetDefaultLoanAmountById(Guid Id)
        {
            var amount = loanproductRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.Id == Id);
            var maxloan = amount.Select(x => x.DefaultLoanAmount).FirstOrDefault();
            return maxloan;
        }

        /// <summary>
        /// Get Loan term and interest rate by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Tuple<int, decimal> GetLoanTermandInterestRateByProductId(Guid Id)
        {
            var getproductbyid = loanproductRepository.GetById(Id);

            var loanterm = getproductbyid.Term;
            var interestrate = getproductbyid.InterestValue;

            return new Tuple<int, decimal>(loanterm, interestrate);
        }

        public string NextLoanProductCode()
        {
            string generatenextloanproductcode = string.Empty;

            var findlastissuedsavingsproductcode = loanproductRepository.GetAll(IsolationLevel.ReadCommitted).OrderByDescending(x => x.CreatedDate).FirstOrDefault();

            if (findlastissuedsavingsproductcode == null)
            {
                generatenextloanproductcode = string.Concat((int)Enumerations.ProductCode.Loan);
            }
            else
            {
                generatenextloanproductcode = string.Format("{0}", int.Parse(findlastissuedsavingsproductcode.ProductCode) + 1);
            }
            return generatenextloanproductcode.ToString();
        }

        public int FindDefaultLoanProductCount()
        {
            var finddefaultproduct = loanproductRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.IsDefault == true);

            return finddefaultproduct.Count();
        }

        public IList<LoanProduct> FindMicroLoan()
        {
            var findMicroLoanCount = loanproductRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.IsMicroLoan == true).ToList();

            return findMicroLoanCount;
        }

        public LoanProduct FindDefaultLoanProduct()
        {
            var finddefaultproduct = loanproductRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.IsDefault == true).FirstOrDefault();

            return finddefaultproduct;
        }

        public IList<LoanProduct> FindDefaultLoanProductList()
        {
            var finddefaultproduct = loanproductRepository.GetAll(IsolationLevel.ReadUncommitted).Where(x => x.IsDefault == true);

            return finddefaultproduct.ToList();
        }
    }
}

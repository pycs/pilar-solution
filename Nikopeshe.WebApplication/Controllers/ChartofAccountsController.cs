﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    [Authorize]
    public class ChartofAccountsController : Controller
    {
        private readonly IChartofAccountService chartofaccountservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public ChartofAccountsController(IChartofAccountService _chartofaccountservice)
        {
            if(_chartofaccountservice == null)
                throw new ArgumentNullException("null service reference");

            this.chartofaccountservice = _chartofaccountservice;
        }


        // GET: ChartofAccounts
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult getallchartofaccounts(int id, JQueryDataTablesModel datatablemodel)
        {
            var pagecollection = chartofaccountservice.GetAllChartofAccountByAccountType(id);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var ChartofAccountList = GetAllChartofAccounts(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, ChartofAccountList: pagecollection);


            return Json(new JQueryDataTablesResponse<ChartofAccount>(items: ChartofAccountList,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<ChartofAccount> GetAllChartofAccounts(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<ChartofAccount> ChartofAccountList)
        {
            var ChartofAccount = ChartofAccountList;

            totalRecordCount = ChartofAccount.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                ChartofAccount = ChartofAccount.Where(c => c.AccountName
                    .ToLower().Contains(searchString.ToLower()) || c.AccountCode.ToString().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = ChartofAccount.Count;

            IOrderedEnumerable<ChartofAccount> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "Parent.AccountName":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.Parent.AccountName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Parent.AccountName);
                        break;

                    case "ChartOfAccountTypeDescription":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.ChartOfAccountTypeDescription)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChartOfAccountTypeDescription);
                        break;

                    case "AccountCode":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.AccountCode)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.AccountCode);
                        break;

                    case "AccountName":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.AccountName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.AccountName);
                        break;

                    case "ChartOfAccountCategoryDescription":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.ChartOfAccountCategoryDescription)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChartOfAccountCategoryDescription);
                        break;

                    case "IsActive":
                        sortedClients = sortedClients == null ? ChartofAccount.CustomSort(sortedColumn.Direction, cust => cust.IsActive)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsActive);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult CreateNewChartOfAccount()
        {
            ViewBag.AccountIdentifier = ChartOfAccountCategorySelectList();
            ViewBag.ParentHeaderAccounts = GetBranchesSelectList(0, string.Empty);
            return View();        
        }

        [HttpPost]
        public ActionResult CreateNewChartOfAccount(ChartofAccount chartOfAccount, FormCollection collection)
        {
            ViewBag.AccountIdentifier = ChartOfAccountCategorySelectList();

            return View();
        }

        [NonAction]
        public List<SelectListItem> ChartOfAccountCategorySelectList()
        {
            List<SelectListItem> obj = new List<SelectListItem>();
            obj.Add(new SelectListItem { Text = "Select Account Category", Value = "" });

            obj.Add(new SelectListItem { Text = "HeaderAccount", Value = (0x1000).ToString() });

            obj.Add(new SelectListItem { Text = "DetailAccount", Value = (0x1000 + 1).ToString() });

            return obj;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadChartofAccountbyCategoryAndType(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException("Id cannot be null.");
            }
            int category = int.Parse(id);
            var headerList = chartofaccountservice.GetChartOfAccountsByTypeAndCategory(category, (int)Enumerations.ChartOfAccountCategory.HeaderAccount);

            var modelData = headerList.Select(m => new SelectListItem()
            {
                Text = m.Parent.AccountName,
                Value = m.ParentId.ToString(),
            });

            return Json(modelData, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        protected List<SelectListItem> GetBranchesSelectList(int id, string selectedValue)
        {
            List<SelectListItem> branchesSelectList = new List<SelectListItem> { };

            var defaultBranchItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = "Choose Branch", Value = string.Empty };

            branchesSelectList.Add(defaultBranchItem);
           
            var headerList = chartofaccountservice.GetChartOfAccountsByTypeAndCategory(id, (int)Enumerations.ChartOfAccountCategory.HeaderAccount);

            if (headerList != null)
            {
                foreach (var item in headerList)
                {
                    branchesSelectList.Add(new SelectListItem { Selected = item.ParentId.ToString() == selectedValue, Text = item.Parent.AccountName, Value = item.ParentId.ToString() });
                }
            }

            return branchesSelectList;
        }
    }
}
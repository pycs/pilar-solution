﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class ChartofAccountService : IChartofAccountService
    {
        readonly IRepository<ChartofAccount> chartofaccountrepository;

        readonly IRepository<LoanTransactionHistory> loantransactionhistoryrepository;

        string errorMessage = string.Empty;

        public ChartofAccountService(IRepository<ChartofAccount> _chartofaccountrepository, IRepository<LoanTransactionHistory> _loantransactionhistoryrepository)
        {
            if (_chartofaccountrepository == null || _loantransactionhistoryrepository == null)
                throw new ArgumentNullException("_loantransactionhistoryrepository, _chartofaccountrepository");

            this.chartofaccountrepository = _chartofaccountrepository;
            this.loantransactionhistoryrepository = _loantransactionhistoryrepository;
        }

        public void AddNewChartofAccount(ChartofAccount chartofaccount, ServiceHeader serviceHeader)
        {
            try
            {
                if (chartofaccount == null)
                    throw new ArgumentNullException("entity cannot be null");

                chartofaccountrepository.Insert(chartofaccount, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(errorMessage, ex);
            }
        }

        public ChartofAccount FindChartofAccountById(Guid Id)
        {
            ChartofAccount findchartofaccountbyid = chartofaccountrepository.GetById(Id);

            return findchartofaccountbyid;
        }

        public void UpdateChartofAccount(ChartofAccount chartofaccount, ServiceHeader serviceHeader)
        {
            try
            {
                if (chartofaccount == null)
                    throw new ArgumentNullException("entity cannot be null");

                chartofaccountrepository.Update(chartofaccount, serviceHeader);
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(errorMessage, ex);
            }
        }

        public IList<ChartofAccount> GetChartofAccountByAccountCategory(int chartofaccountcategory)
        {
            if (string.IsNullOrWhiteSpace(string.Concat(chartofaccountcategory)))
                throw new ArgumentNullException("null entity");

            var results = chartofaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.ChartOfAccountCategory == chartofaccountcategory).ToList();

            return results;
        }    
   
        public ChartofAccount FindChartOfAccountByParentId(Guid parentId)
        {
            ChartofAccount chartofaccount = chartofaccountrepository.GetAll(IsolationLevel.ReadCommitted).FirstOrDefault();

            return chartofaccount;
        }

        public IList<ChartofAccount> GetAllChartofAccounts()
        {
            return chartofaccountrepository.GetAll(IsolationLevel.ReadUncommitted).ToList();
        }

        public IList<ChartofAccount> GetChartofAccountByAccountType(int type)
        {
            var selectchartbytype = chartofaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.ChartOfAccountType == (Enumerations.ChartOfAccountType)type && x.ChartOfAccountCategory == (int)Enumerations.ChartOfAccountCategory.HeaderAccount);

            return selectchartbytype.ToList();
        }

        public IList<ChartofAccount> GetAllChartofAccountByAccountType(int type)
        {
            var selectchartbytype = chartofaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.ChartOfAccountType == (Enumerations.ChartOfAccountType)type && (x.ChartOfAccountCategory == (int)Enumerations.ChartOfAccountCategory.HeaderAccount || x.ChartOfAccountCategory == (int)Enumerations.ChartOfAccountCategory.DetailAccount));

            return selectchartbytype.ToList();
        }

        public IList<ChartofAccount> GetChartOfAccountsByTypeAndCategory(int type1, int category)
        {
            var list = chartofaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.ChartOfAccountType == (Enumerations.ChartOfAccountType)type1 && x.ChartOfAccountCategory == category && x.IsActive == true);

            return list.ToList();
        }

        public IList<ChartofAccount> GetChartOfAccountsByTypeAndCategory(int type1, int type2, int category)
        {
            var list = chartofaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => (x.ChartOfAccountType == (Enumerations.ChartOfAccountType)type1 || x.ChartOfAccountType == (Enumerations.ChartOfAccountType)type2) && x.ChartOfAccountCategory == category && x.IsActive == true);

            return list.ToList();
        }

        public bool ValidateAccountCode(int AccountCode)
        {
            var findaccountcode = chartofaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.AccountCode == AccountCode);
            if (findaccountcode.Count() >= 1)

                return true;

            return false;
        }

        public ChartofAccount FindChartofAccountByAccountCode(int AccountCode)
        {
            if (string.IsNullOrWhiteSpace(string.Concat(AccountCode)))
            {
                throw new ArgumentNullException("Account Code cannot be null");
            }

            var findaccountcodebyaccountcode = chartofaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.AccountCode == AccountCode);

            return findaccountcodebyaccountcode.FirstOrDefault();
        }

        public IList<LoanTransactionHistory> GetChartofAccountStatement(Guid ChartofAccountId)
        {
            if(ChartofAccountId == Guid.Empty)
            {
                throw new ArgumentNullException("Account Code cannot be null");
            }

            var GetAllTransactionByAccountCode = loantransactionhistoryrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.ChartofAccountId == ChartofAccountId);

            return GetAllTransactionByAccountCode.ToList();
        }
    }
}

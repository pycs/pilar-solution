﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NikopesheWindowsService
{
    public interface ITaskScheduler
    {
        string Name { get; }
        //void EmailQueueJobDefination();
        //void SPSGetJobDefination();
        //void SPSPostJobDefination();
        //void MpesaRepayment();
        void C2BConfirmation();
        //void LoanStatusWatchdogJob();
        void Stop();
    }
}

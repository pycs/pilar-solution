﻿using Crosscutting.NetFramework.Logging;
using Pilar.LoanRepaymentProcessing.Configuration;
using Pilar.Services.Services;
using Quartz;
using Quartz.Impl;
using System;
using System.ComponentModel.Composition;
using System.Configuration;

namespace Pilar.LoanRepaymentProcessing.Services
{
    [Export(typeof(IPlugin))]
    public class Queuer : IPlugin
    {
        private readonly IScheduler _scheduler;

        public Queuer()
        {
            // Get a reference to the scheduler
            var sf = new StdSchedulerFactory();

            // Get an instance of the Quartz.Net scheduler
            _scheduler = sf.GetScheduler();
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{224C76FA-15DA-4F4F-9DE8-8CE61AE777E2}"); }
        }

        public string Description
        {
            get { return "LOANPROCESSING.QUEUER"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                // Start the scheduler if its in standby
                if (!_scheduler.IsStarted)
                    _scheduler.Start();

                // Define the Job to be scheduled
                var job = JobBuilder.Create<QueuingJob>()
                    .WithIdentity("LoanProcessingQueueingJob", "Pilar")
                    .RequestRecovery()
                    .Build();

                // Associate a trigger with the Job
                var cronExpression = ConfigurationManager.AppSettings["LoanProcessingCronExpression"];
                var trigger = (ICronTrigger)TriggerBuilder.Create()
                    .WithIdentity("LoanProcessingQueueingJob", "Pilar")
                    .WithCronSchedule(cronExpression)
                    .StartAt(DateTime.UtcNow)
                    .WithPriority(1)
                    .Build();

                // Validate that the job doesn't already exist
                if (_scheduler.CheckExists(new JobKey("LoanProcessingQueueingJob", "Pilar")))
                {
                    _scheduler.DeleteJob(new JobKey("LoanProcessingQueueingJob", "Pilar"));
                }

                var schedule = _scheduler.ScheduleJob(job, trigger);

#if DEBUG
                //ExceptionLoggerFactory.CreateLog("{0}->DoWork..." +  Description);
#endif
            }
            catch (Exception ex)
            {
                //ExceptionLoggerFactory.CreateLog("{0}->DoWork..." + ex + Description);
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        public void Exit()
        {
            _scheduler.Shutdown();
        }

        #endregion
    }
}

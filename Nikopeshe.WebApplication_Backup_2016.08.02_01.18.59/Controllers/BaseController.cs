﻿using Nikopeshe.Entity.DataModels;
using Nikopeshe.Entity.Helpers;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Models;
using Nikopeshe.WebApplication.Utilities;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Nikopeshe.WebApplication.Controllers
{
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class BaseController : Controller
    {                
        [NonAction]
        public List<Bank> GetBanksList()
        {
            var bankCodes = new List<string>();

            var bankNames = new List<string>();

            var branchCodes = new List<string>();    
            
            var payBills = new List<string>();

            string bankBranchesFileName = string.Format(@"{0}Content\uploads\BanksPayBillNumbers.xls", AppDomain.CurrentDomain.BaseDirectory);

            using (FileStream file = new FileStream(bankBranchesFileName, FileMode.Open, FileAccess.Read))
            {
                HSSFWorkbook hssfWorkbook = new HSSFWorkbook(file);

                if (hssfWorkbook != null)
                {
                    ISheet sheet = hssfWorkbook.GetSheetAt(0);

                    if (sheet != null)
                    {
                        IEnumerator rows = sheet.GetRowEnumerator();

                        if (rows != null)
                        {
                            rows.MoveNext(); // skip header row

                            int rowCounter = default(int);

                            while (rows.MoveNext())
                            {
                                IRow row = (HSSFRow)rows.Current;

                                ICell cell1Value = row.GetCell(0);  // BankCode
                                ICell cell2Value = row.GetCell(1);  // BankName
                                ICell cell3Value = row.GetCell(2);  // Status
                                ICell cell4Value = row.GetCell(3);  // PayBillNumber
                                ICell cell5Value = row.GetCell(4);  // KeyWord

                                object col1Value = cell1Value != null ? cell1Value.ToString() : null;
                                object col2Value = cell2Value != null ? cell2Value.ToString() : null;
                                object col3Value = cell3Value != null ? cell3Value.ToString() : null;
                                object col4Value = cell4Value != null ? cell4Value.ToString() : null;
                                object col5Value = cell5Value != null ? cell5Value.ToString() : null;

                                if (!bankCodes.Contains(col1Value.ToString().Trim()))
                                {
                                    bankCodes.Add(col1Value.ToString().Trim());                                    
                                }

                                if (!bankNames.Contains(col2Value.ToString().Trim()))
                                    bankNames.Add(col2Value.ToString().Trim());

                                //if (!payBills.Contains(col4Value.ToString().Trim()))
                                    payBills.Add(col4Value.ToString().Trim());

                                rowCounter += 1;
                            }
                        }
                    }
                }
            }

            #region branches

            var tupleList = new List<Tuple<long, long>>();            
            
            #endregion

            #region banks

            var banksList = new List<Bank>();

            var bankCounter = 0;            

            foreach (var bankName in bankNames)
            {
                var bank = new Bank { Code = bankCodes[bankCounter], Name = bankName, PayBillNumber = payBills[bankCounter]};                

                banksList.Add(bank);

                bankCounter += 1;
            }

            #endregion

            return banksList.OrderBy(x => x.Name).ToList();
        }

        [NonAction]
        public string GetBranchesList(string bankCode)
        {
            var banks = GetBanksList();

            if (banks != null)
            {
                var targetBank = (from b in banks
                                  where b.Code == bankCode
                                  select b).FirstOrDefault();

                if (targetBank != null)
                    return targetBank.PayBillNumber;
                else return null;
            }
            else return null;
        }

        [NonAction]
        protected List<SelectListItem> GetBanksSelectList(string selectedValue)
        {
            List<SelectListItem> banksSelectList = new List<SelectListItem> { };

            var defaultBankItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = "Choose Bank", Value = string.Empty };

            banksSelectList.Add(defaultBankItem);

            var otherBankItems = GetBanksList();

            if (otherBankItems != null)
            {
                foreach (var item in otherBankItems)
                {
                    banksSelectList.Add(new SelectListItem { Selected = item.Code == selectedValue, Text = item.Name, Value = item.Code });
                }
            }
            return banksSelectList;
        }

        [NonAction]
        protected string GetBranchesSelectList(string bankCode)
        {            
            var otherBranchItems = GetBranchesList(bankCode);
            
            return otherBranchItems;
        }

        public List<CheckBoxListInfo> EFTInfo(LoanRequest loanRequest)
        {
            List<CheckBoxListInfo> eftInfo = new List<CheckBoxListInfo>();
            eftInfo.Add(new CheckBoxListInfo(((int)Enumerations.EFTType.AccountToMobile).ToString(), EnumHelper.GetDescription(Enumerations.EFTType.AccountToMobile), loanRequest != null ? loanRequest.Type == Enumerations.EFTType.AccountToMobile : true));
            //eftInfo.Add(new CheckBoxListInfo(((int)Enumerations.EFTType.AccountToAccount).ToString(), EnumHelper.GetDescription(Enumerations.EFTType.AccountToAccount), loanRequest != null ? loanRequest.Type == Enumerations.EFTType.AccountToAccount : false));                        

            return eftInfo;
        }

        public IList<CheckBoxListInfo> BranchType(Branch branch)
        {
            List<CheckBoxListInfo> branchTypeList = new List<CheckBoxListInfo>();
            branchTypeList.Add(new CheckBoxListInfo(((int)Enumerations.BranchType.Branch).ToString(), EnumHelper.GetDescription(Enumerations.BranchType.Branch), branch != null ? branch.Type == (int)Enumerations.BranchType.Branch : true));
            branchTypeList.Add(new CheckBoxListInfo(((int)Enumerations.BranchType.Agency).ToString(), EnumHelper.GetDescription(Enumerations.BranchType.Agency), branch != null ? branch.Type == (int)Enumerations.BranchType.Agency : false));
            return branchTypeList;
        }

        public IList<CheckBoxListInfo> FileUploadCategoryCheckBoxList()
        {
            IList<CheckBoxListInfo> fileUploadCheckBoxList = new List<CheckBoxListInfo>();
            fileUploadCheckBoxList.Add(new CheckBoxListInfo(((int)Enumerations.UploadFileCategory.ProcessingFee).ToString(), EnumHelper.GetDescription(Enumerations.UploadFileCategory.ProcessingFee), false));
            fileUploadCheckBoxList.Add(new CheckBoxListInfo(((int)Enumerations.UploadFileCategory.Reconciliation).ToString(), EnumHelper.GetDescription(Enumerations.UploadFileCategory.Reconciliation), false));
            return fileUploadCheckBoxList;
        }

        public IList<CheckBoxListInfo> InterestChargeType(LoanProduct loanProduct)
        {
            List<CheckBoxListInfo> interestChargeTypeList = new List<CheckBoxListInfo>();
            interestChargeTypeList.Add(new CheckBoxListInfo(((int)Enumerations.InterestChargeType.Fixed).ToString(), EnumHelper.GetDescription(Enumerations.InterestChargeType.Fixed), loanProduct != null ? loanProduct.InterestChargeType == (int)Enumerations.InterestChargeType.Fixed : false));
            interestChargeTypeList.Add(new CheckBoxListInfo(((int)Enumerations.InterestChargeType.Percentage).ToString(), EnumHelper.GetDescription(Enumerations.InterestChargeType.Percentage), loanProduct != null ? loanProduct.InterestChargeType == (int)Enumerations.InterestChargeType.Percentage : true));
            return interestChargeTypeList;
        }

        public IList<CheckBoxListInfo> ChargeRecoveryMethodCheckList(Charge charge)
        {
            List<CheckBoxListInfo> chargeRecoveryCheckList = new List<CheckBoxListInfo>();
            chargeRecoveryCheckList.Add(new CheckBoxListInfo(((int)Enumerations.ChargeRecoveryMethod.UpFront).ToString(), EnumHelper.GetDescription(Enumerations.ChargeRecoveryMethod.UpFront), charge != null ? charge.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.UpFront : false));
            chargeRecoveryCheckList.Add(new CheckBoxListInfo(((int)Enumerations.ChargeRecoveryMethod.PerInstallment).ToString(), EnumHelper.GetDescription(Enumerations.ChargeRecoveryMethod.PerInstallment), charge != null ? charge.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment : false));
            return chargeRecoveryCheckList;
        }

        public List<CheckBoxListInfo> maritalStatusInfoCheckList()
        {
            List<CheckBoxListInfo> maritalStatusInfo = new List<CheckBoxListInfo>();

            maritalStatusInfo.Add(new CheckBoxListInfo(((int)Enumerations.MaritalStatus.Single).ToString(), EnumHelper.GetDescription(Enumerations.MaritalStatus.Single), false));

            maritalStatusInfo.Add(new CheckBoxListInfo(((int)Enumerations.MaritalStatus.Married).ToString(), EnumHelper.GetDescription(Enumerations.MaritalStatus.Married), false));

            maritalStatusInfo.Add(new CheckBoxListInfo(((int)Enumerations.MaritalStatus.Divorced).ToString(), EnumHelper.GetDescription(Enumerations.MaritalStatus.Divorced), false));

            maritalStatusInfo.Add(new CheckBoxListInfo(((int)Enumerations.MaritalStatus.Widowed).ToString(), EnumHelper.GetDescription(Enumerations.MaritalStatus.Widowed), false));

            return maritalStatusInfo;
        }

        

        public List<CheckBoxListInfo> roleList()
        {
            List<CheckBoxListInfo> roleInfo = new List<CheckBoxListInfo>();

            roleInfo.Add(new CheckBoxListInfo(((int)Enumerations.RoleCategory.Administrator).ToString(), EnumHelper.GetDescription(Enumerations.RoleCategory.Administrator), false));

            roleInfo.Add(new CheckBoxListInfo(((int)Enumerations.RoleCategory.LoanManager).ToString(), EnumHelper.GetDescription(Enumerations.RoleCategory.LoanManager), false));

            roleInfo.Add(new CheckBoxListInfo(((int)Enumerations.RoleCategory.LoanOfficer).ToString(), EnumHelper.GetDescription(Enumerations.RoleCategory.LoanOfficer), false));

            roleInfo.Add(new CheckBoxListInfo(((int)Enumerations.RoleCategory.Customer).ToString(), EnumHelper.GetDescription(Enumerations.RoleCategory.Customer), false));

            roleInfo.Add(new CheckBoxListInfo(((int)Enumerations.RoleCategory.FinanceManager).ToString(), EnumHelper.GetDescription(Enumerations.RoleCategory.FinanceManager), false));

            return roleInfo;
        }

        public List<CheckBoxListInfo> GetUserRoleInfo(string userName, bool filterRoles = true)
        {
            List<CheckBoxListInfo> rolesInfo = new List<CheckBoxListInfo>();

            string[] allRoles = (filterRoles)
                ? (Roles.GetAllRoles().Except(new string[] { "Maker", "Checker" })).ToArray()
                : Roles.GetAllRoles();

            Array.ForEach(allRoles, (roleName) =>
            {
                rolesInfo.Add(new CheckBoxListInfo(roleName, roleName, Roles.IsUserInRole(userName, roleName)));
            });

            return rolesInfo;
        }

        public List<SystemRoles> GetUserRoleInfoList(bool filterRoles = true)
        {
            List<SystemRoles> rolesInfo = new List<SystemRoles>();

            string[] allRoles = (filterRoles)
                ? (Roles.GetAllRoles().Except(new string[] { "Customer" })).ToArray()
                : Roles.GetAllRoles();

            Array.ForEach(allRoles, (roleName) =>
            {
                rolesInfo.Add(new SystemRoles{RoleName = roleName});
            });

            return rolesInfo;
        }

        public List<CheckBoxListInfo> genderInfoCheckList()
        {
            List<CheckBoxListInfo> genderCheckList = new List<CheckBoxListInfo>();
            string[] gender = Enum.GetValues(typeof(Enumerations.Gender)).Cast<Enum>().Select(v => v.ToString()).ToArray();

            Array.ForEach(gender, (genderName) =>
            {
                genderCheckList.Add(new CheckBoxListInfo(genderName, genderName, true));
            });
           
            return genderCheckList;
        }

        [NonAction]
        public List<SelectListItem> IncomeRangeSelectList()
        {
            List<SelectListItem> obj = new List<SelectListItem>();

            obj.Add(new SelectListItem { Text = "Select Income Range", Value = "Select Income Range" });

            obj.Add(new SelectListItem { Text = "999 - 10000", Value = "999 - 10000" });

            obj.Add(new SelectListItem { Text = "10001 - 20000", Value = "10001 - 20000" });

            obj.Add(new SelectListItem { Text = "20001 - 40000", Value = "20001 - 40000" });

            obj.Add(new SelectListItem { Text = "400001 - 80000", Value = "400001 - 80000" });

            obj.Add(new SelectListItem { Text = "80001 and above", Value = "80001 and above" });

            return obj;
        }

        [NonAction]
        protected List<SelectListItem> GetUploadFileCategorySelectList(string selectedValue)
        {
            List<SelectListItem> eftCodesSelectList = new List<SelectListItem> { };

            var defaultRMItem = new SelectListItem { Selected = (selectedValue == string.Empty), Text = "Choose Transaction Type", Value = string.Empty };

            eftCodesSelectList.Add(defaultRMItem);

            var tuple = EnumValueDescriptionCache.GetValues(typeof(Enumerations.UploadFileCategory));

            for (int n = 0; n < tuple.Item1.Length; n++)
            {
                var info =
                    new CheckBoxListInfo(
                        tuple.Item1[n].ToString() /*value*/,
                        tuple.Item2[n].ToString()  /*displayText*/,
                        tuple.Item1[n].ToString() == selectedValue  /*isChecked*/);
                
                eftCodesSelectList.Add(new SelectListItem { Selected = info.IsChecked, Text = info.DisplayText, Value = info.Value });
            }

            return eftCodesSelectList;
        }
    }

    public class Bank
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string PayBillNumber { get; set; }       
    }

    public class ApplicantBranch
    {
        public string Code { get; set; }

        public string PayBillNumber { get; set; }
    }
}
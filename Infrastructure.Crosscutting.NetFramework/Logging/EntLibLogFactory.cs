﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crosscutting.NetFramework.Logging
{
    public class EntLibLogFactory : ILoggerFactory
    {
        public ILogger Create()
        {
            return new EntLibLog();
        }
    }
}

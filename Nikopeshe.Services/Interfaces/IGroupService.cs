﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.DTO;
using System;
using System.Collections.Generic;
using System.Data;

namespace Nikopeshe.Services.Interfaces
{
    public interface IGroupService
    {
        void CreateNewGroup(Group group, Customer customer, ServiceHeader serviceHeader);

        void UpdateGroup(Group Group, ServiceHeader serviceHeader);

        Group FindGroupById(Guid Id);

        IList<Group> GetAllGroup(IsolationLevel isolationLevel);

        Group FindGroupByRegistrationNumber(string regNumber);

        Group FindGroupByGroupNumber(string GroupNumber);

        Group FindGroupByCustomerId(Guid CustomerId);

        Group FindGroupCustomerAccountsIdByGroupId(Guid GroupId);

        Group FindGroupByRM(Guid RMId);

        IList<string> GetDistinctCountyCodes();

        int FindGroupCountByCountyCode(string CountyCode);

        bool CheckDuplicateGroupNumber(string groupNumber);

        PageCollectionInfo<Group> FindGroupsFilterInPage(int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);

        PageCollectionInfo<Group> FindGroupsByBranchIdFilterInPage(Guid id, int startIndex, int pageSize, IList<string> sortedColumns, string searchString, bool sortAscending);
    }
}

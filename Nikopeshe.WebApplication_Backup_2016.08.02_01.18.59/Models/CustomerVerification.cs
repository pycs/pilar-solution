﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nikopeshe.WebApplication.Models
{
    public class CustomerVerification
    {
        [Display(Name = "Approval Code")]
        [Required]
        public string ApprovalCode { get; set; }
    }
}
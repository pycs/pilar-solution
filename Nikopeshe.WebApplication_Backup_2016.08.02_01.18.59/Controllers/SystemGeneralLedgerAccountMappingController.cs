﻿using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Nikopeshe.WebApplication.Extensions;
using System.Web.Mvc;
using System.ServiceModel;
using Nikopeshe.Data;
using Nikopeshe.WebApplication.Attributes;
using Crosscutting.NetFramework.Logging;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize(Roles = "Administrator")]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class SystemGeneralLedgerAccountMappingController : Controller
    {
        readonly ISystemGeneralLedgerAccountMappingService systemglaccountmappings;
        readonly IChartofAccountService chartofaccountservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public SystemGeneralLedgerAccountMappingController(ISystemGeneralLedgerAccountMappingService _systemglaccountmappings, IChartofAccountService _chartofaccountservice)
        {
            if(_systemglaccountmappings == null || _chartofaccountservice == null)
                throw new ArgumentNullException("null service reference");

            this.chartofaccountservice = _chartofaccountservice;
            this.systemglaccountmappings = _systemglaccountmappings;
        }
        
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetSystemGeneralLedgerAccountMappings(JQueryDataTablesModel datatablemodel)
        {
            int totalrecordcount = 0;

            int searchrecordcount = 0;

            var pagecollection = systemglaccountmappings.GetAllSystemGeneralLedgerAccountMappings();
            var loanrequestList = GetSystemGeneralLedgerAccountMappingsList(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalrecordcount, searchRecordCount: out searchrecordcount, searchString: datatablemodel.sSearch, SystemGeneralLedgerAccountMappingList: pagecollection);

            return Json(new JQueryDataTablesResponse<SystemGeneralLedgerAccountMapping>(items: loanrequestList,
                totalRecords: totalrecordcount,
                totalDisplayRecords: searchrecordcount,
                sEcho: datatablemodel.sEcho));
        }

        private IList<SystemGeneralLedgerAccountMapping> GetSystemGeneralLedgerAccountMappingsList(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<SystemGeneralLedgerAccountMapping> SystemGeneralLedgerAccountMappingList)
        {
            var GetLoanproductList = SystemGeneralLedgerAccountMappingList;

            totalRecordCount = GetLoanproductList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetLoanproductList = SystemGeneralLedgerAccountMappingList.Where(c => string.Concat(c.SystemGeneralLedgerAccountCode)
                    .ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetLoanproductList.Count;

            IOrderedEnumerable<SystemGeneralLedgerAccountMapping> sortedClients = null;

            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "SystemGeneralLedgerAccountCode":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.SystemGeneralLedgerAccountCode)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.SystemGeneralLedgerAccountCode);
                        break;

                    case "ChartofAccount.AccountName":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.AccountName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.AccountName);
                        break;

                    case "SystemGeneralLedgerAccountDesc":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.SystemGeneralLedgerAccountDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.SystemGeneralLedgerAccountDesc);
                        break;

                    case "IsEnabled":
                        sortedClients = sortedClients == null ? GetLoanproductList.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled);
                        break;

                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [HttpGet]
        public ActionResult CreateNewMapping()
        {
            ViewBag.ChartofAccountsSelectList = GetAllDetailChartofaccounts(Guid.Empty.ToString());

            return View();
        }

        [HttpPost]        
        public ActionResult CreateNewMapping(SystemGeneralLedgerAccountMapping glmapping)
        {
            ViewBag.ChartofAccountsSelectList = GetAllDetailChartofaccounts(Guid.Empty.ToString());

            if (ModelState.IsValid)
            {
                if(glmapping.ChartofAccountId == Guid.Empty || glmapping.SystemGeneralLedgerAccountCode == 0)
                {
                    ModelState.AddModelError(string.Empty, "Chart of accounts or Account code cannot be null.");
                    return View(glmapping);
                }
                try
                {
                    systemglaccountmappings.AddNewSystemGeneralLedgerAccountMapping(glmapping, serviceHeader);
                    return RedirectToAction("Index");
                }

                catch(Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                }
            }
            return View(glmapping);
        }

        [HttpGet]
        public ActionResult UpdateGlMapping(Guid Id)
        {
            var findglmappingbyid = systemglaccountmappings.FindSystemGeneralLedgerAccountMappingsById(Id);

            ViewBag.ChartofAccountsSelectList = GetAllDetailChartofaccounts(Guid.Empty.ToString());

            return View(findglmappingbyid);
        }

        [HttpPost]
        [ActionName("UpdateGlMapping")]        
        public ActionResult UpdateGlMapping(SystemGeneralLedgerAccountMapping systemledgermapping)
        {
            ViewBag.ChartofAccountsSelectList = GetAllDetailChartofaccounts(Guid.Empty.ToString());

            var findglmappingbyid = systemglaccountmappings.FindSystemGeneralLedgerAccountMappingsById(systemledgermapping.Id);            

            if (ModelState.IsValid)
            {
                if (systemledgermapping.ChartofAccountId == Guid.Empty || systemledgermapping.SystemGeneralLedgerAccountCode == 0)
                {
                    ModelState.AddModelError(string.Empty, "Chart of accounts or Account code cannot be null.");
                    return View(systemledgermapping);
                }
                try
                {
                    findglmappingbyid.ChartofAccountId = systemledgermapping.ChartofAccountId;
                    findglmappingbyid.SystemGeneralLedgerAccountCode = systemledgermapping.SystemGeneralLedgerAccountCode;
                    findglmappingbyid.IsEnabled = systemledgermapping.IsEnabled;
                    systemglaccountmappings.UpdateSystemGeneralLedgerAccountMapping(findglmappingbyid, serviceHeader);
                    return RedirectToAction("Index");
                }
                catch(Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                }
            }
            return View(findglmappingbyid);
        }

        [NonAction]
        public List<SelectListItem> GetAllDetailChartofaccounts(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultaccount = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select Chart of Account", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultaccount);

            var otherliabilityaccounts = chartofaccountservice.GetChartofAccountByAccountCategory((int)Enumerations.ChartOfAccountCategory.DetailAccount);

            foreach (var item in otherliabilityaccounts)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.ChartofAccountDescription, Value = item.Id.ToString("D") });

            return SelectList;
        }
    }
}
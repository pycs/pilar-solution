﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Data;

namespace Nikopeshe.Services.Interfaces
{
    public interface ILoanProductsChargeService
    {
        void AddNewLoanProductsCharge(LoanProductsCharge loanproductcharge, ServiceHeader serviceHeader);

        void UpdateLoanProductsCharge(LoanProductsCharge loanproductcharge, ServiceHeader serviceHeader);

        LoanProductsCharge FindLoanProductsChargeById(Guid Id);

        IList<LoanProductsCharge> GetAllLoanProductCharges(Guid LoanProductId, IsolationLevel isolationLevel);

        void DeleteLoanProductCharge(LoanProductsCharge loanproductcharge, ServiceHeader serviceHeader);

        IList<LoanProductsCharge> GetRecurringLoanProductChargeByProductId(Guid LoanProductId, IsolationLevel isolationLevel);

        LoanProductsCharge FindLoanProductChargeByChargeId(Guid ChargeId);
    }
}

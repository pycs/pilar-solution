﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.IPNService;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Repayments.Configuration
{
    public class IPNQueuer2 : IJob
    {
        private readonly IMessageQueueService _messageQueueService;
        private readonly IIPNService _iipnservice;
        private readonly string _messageRelayQueuePath;

        public IPNQueuer2()
        {
            _messageQueueService = new MessageQueueService();
            _iipnservice = new IPNService();
            _messageRelayQueuePath = ConfigurationManager.AppSettings["C2BQueuePath"];
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                //1. Find Approved Consumer to Business Transactions with approved status
                var findpendingapprovalc2b = _iipnservice.FindC2BTransactionByStatus((int)Enumerations.C2BTransactionStatus.Approved).Take(100);

                if (findpendingapprovalc2b != null && findpendingapprovalc2b.Any())
                {
                    foreach (var item in findpendingapprovalc2b)
                    {
                        item.Status = (int)Enumerations.C2BTransactionStatus.Queued;
                        _iipnservice.UpdateC2BTransaction(item);
                        _messageQueueService.Send(_messageRelayQueuePath, item.Id, Enumerations.MessageCategory.C2BProcessing, MessagePriority.High);
                    }
                }
                //2. Find Queued Consumer to Business transaction given elapse time span

                var findQueuedConsumetToBusinessTransactions = _iipnservice.FindQueuedConsumerToBusinessTransactionsWithGivenElapseTimeInMinutes(5);

                if (findQueuedConsumetToBusinessTransactions != null && findQueuedConsumetToBusinessTransactions.Any())
                {
                    foreach (var c2b in findQueuedConsumetToBusinessTransactions)
                    {
                        c2b.Status = (int)Enumerations.C2BTransactionStatus.Approved;
                        _iipnservice.UpdateC2BTransaction(c2b);
                    }
                }
            }

            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("IPNConfirmationQueueingJob.Execute...{0}" + ex);
            }
        }
    }
}

﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class CustomerAccountService : ICustomerAccountService
    {
        private readonly IRepository<CustomersAccount> customersaccountrepository;
        private readonly IRepository<LoanTransactionHistory> loanTransactionHistoryRepository;
        private readonly IRepository<Customer> customerRepository;
        public DataContext dataContext = new DataContext();
        string ErrorMessage = string.Empty;

        public CustomerAccountService(IRepository<CustomersAccount> _customersaccountrepository,
            IRepository<LoanTransactionHistory> _loanTransactionHistoryRepository,
            IRepository<Customer> _customerRepository, IRepository<SavingsProduct> _savingsAccountRepository)
        {
            if (_customersaccountrepository == null)
                throw new ArgumentNullException("customer account repository is null");

            this.customersaccountrepository = _customersaccountrepository;
            this.loanTransactionHistoryRepository = _loanTransactionHistoryRepository;
            this.customerRepository = _customerRepository;
        }

        public void AddNewCustomersAccount(CustomersAccount customersaccount, ServiceHeader serviceHeader)
        {
            try
            {
                if (customersaccount == null)
                {
                    throw new ArgumentNullException("customers entity cannot be null");
                }

                customersaccountrepository.Insert(customersaccount, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public void UpdateCustomersAccount(CustomersAccount customersaccount, ServiceHeader serviceHeader)
        {            
            try
            {
                if(customersaccount == null)
                {
                    throw new ArgumentNullException("customer entity cannot be null");               
                }
                customersaccountrepository.Update(customersaccount, serviceHeader);

            }
            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            }
        }

        public CustomersAccount FindCustomersAccountById(Guid Id)
        {
            if(Id == Guid.Empty)
            {
                throw new ArgumentNullException("Id cannot be null");
            }

            var findcustomersaccountbyid = customersaccountrepository.GetById(Id);

            return findcustomersaccountbyid;
        }

        public IList<CustomersAccount> GetAllCustomersAccount(IsolationLevel isolationLevel)
        {
            return customersaccountrepository.GetAll(isolationLevel).ToList();
        }

        public CustomersAccount FindCustomersAccountByAccountNumber(string AccountNumber)
        {
            var findcustomerbyaccountnumber = customersaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.AccountNumber == AccountNumber);

            return findcustomerbyaccountnumber.FirstOrDefault();
        }

        public IList<CustomersAccount> GetCustomersAccountByAccountType(int AccountType)
        {
            var findcustomersaccountbyaccounttype = customersaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.AccountType == AccountType);

            return findcustomersaccountbyaccounttype.ToList();
        }

        public CustomersAccount FindCustomersAccountByCustomerId(Guid CustomerId)
        {
            var findcustomeraccountbycustomerid = customersaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CustomerId == CustomerId);

            return findcustomeraccountbycustomerid.FirstOrDefault();
        }


        public CustomersAccount FindCustomerByCustomerIdandAccountType(Guid CustomerId, int AccountType)
        {
            var findcustomerbycustomeridandaccounttype = customersaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CustomerId == CustomerId && x.AccountType == AccountType).FirstOrDefault();

            return findcustomerbycustomeridandaccounttype;
        }

        public IList<CustomersAccount> GetAllCustomerAccountNumbers(Guid CustomerId, IsolationLevel isolationLevel)
        {
            var findallcustomeraccountnumber = customersaccountrepository.GetAll(isolationLevel).Where(x => x.CustomerId == CustomerId);

            return findallcustomeraccountnumber.ToList();
        }

        public decimal FindCustomerAccountBalanceByProductCode(Guid CustomerAccountId, Guid targetProductId, int ProductCode)
        {
            if (CustomerAccountId == Guid.Empty)
            {
                throw new ArgumentNullException("Customer id cannot be null");
            }

            decimal availableBalance = 0m;
            if (ProductCode == (int)Enumerations.ProductCode.Savings)
            {
                var creditTransactions = loanTransactionHistoryRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.IsApproved == true && x.TransactionType == Enumerations.TransactionType.Credit
                                         && x.CustomersAccountId == CustomerAccountId && x.ChartofAccountId == targetProductId).ToList();

                var sumOfCredits = creditTransactions.Sum(x => x.TransactionAmount);

                var debitTransactions = loanTransactionHistoryRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.IsApproved == true && x.TransactionType == Enumerations.TransactionType.Debit
                                         && x.CustomersAccountId == CustomerAccountId && x.ChartofAccountId == targetProductId).ToList();

                var sumOfDebits = debitTransactions.Sum(x => x.TransactionAmount);

                availableBalance = sumOfCredits - sumOfDebits;
            }

            else if(ProductCode == (int)Enumerations.ProductCode.Loan)
            {
                var creditTransactions = loanTransactionHistoryRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.IsApproved == true && x.TransactionType == Enumerations.TransactionType.Credit
                                         && x.CustomersAccountId == CustomerAccountId && x.ChartofAccountId == targetProductId).ToList();

                var sumOfCredits = creditTransactions.Sum(x => x.TransactionAmount);

                var debitTransactions = loanTransactionHistoryRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.IsApproved == true && x.TransactionType == Enumerations.TransactionType.Debit
                                         && x.CustomersAccountId == CustomerAccountId && x.ChartofAccountId == targetProductId).ToList();

                var sumOfDebits = debitTransactions.Sum(x => x.TransactionAmount);

                availableBalance = sumOfCredits - sumOfDebits;
            }

            return availableBalance;
        }

        public CustomerAccountInfo GetCustomerAccountDetailsByCustomerSavingsAccountId(Guid SavingsAccountId)
        {
            if (SavingsAccountId == Guid.Empty)
                throw new ArgumentNullException("customer id cannot be null");

            var idStartDate = new SqlParameter { ParameterName = "@CustomerAccountID", Value = SavingsAccountId };

            var results = dataContext.Database.SqlQuery<CustomerAccountInfo>("sp_Customer_Savings_Loan_AccountBalance @CustomerAccountID", idStartDate).Cast< CustomerAccountInfo>().First();

            return results;
        }

        public CustomersAccount GetCustomerAccountNumberByAccountType(Guid? CustomerId, int AccountType)
        {
            if (CustomerId == Guid.Empty)
            {
                throw new ArgumentNullException("Customer id cannot be null");
            }


            var findallcustomeraccountnumber = customersaccountrepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.CustomerId == CustomerId && x.AccountType == AccountType).FirstOrDefault();

            return findallcustomeraccountnumber;
        }
    }
}
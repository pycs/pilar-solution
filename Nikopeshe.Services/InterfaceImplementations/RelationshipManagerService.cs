﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class RelationshipManagerService : IRelationshipManagerService
    {
        public IRepository<RelationshipManager> relationshipManagerRepository;

        public string errorMessage = string.Empty;

        public RelationshipManagerService(IRepository<RelationshipManager> _relationshipManagerRepository)
        {
            this.relationshipManagerRepository = _relationshipManagerRepository;
        }

        public void AddNewRelationshipManager(RelationshipManager relationshipManager, ServiceHeader serviceHeader)
        {
            if (relationshipManager == null)
                throw new ArgumentNullException("null entity");

            try
            {
                relationshipManagerRepository.Insert(relationshipManager, serviceHeader);
            }

            catch(DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach(var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", errorMessage);
                    }
                }
            }
        }

        public void UpdateRelationshipManager(RelationshipManager relationshipManager, ServiceHeader serviceHeader)
        {
            if (relationshipManager == null)
                throw new ArgumentNullException("null entity");

            try
            {
                relationshipManagerRepository.Update(relationshipManager, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                        LoggerFactory.CreateLog().LogInfo("Error -> {0}", errorMessage);
                    }
                }
            }
        }

        public List<RelationshipManager> GetAllRelationshipManagers(IsolationLevel isolationLevel)
        {
            var findAllRM = relationshipManagerRepository.GetAll(isolationLevel);

            return findAllRM.ToList();
        }

        public RelationshipManager FindRelationshipManagerById(Guid Id)
        {
            if (Id == Guid.Empty) {
                throw new ArgumentNullException("Id cannot be null");
            }

            var findRMById = relationshipManagerRepository.GetById(Id);

            return findRMById;
        }

        public List<RelationshipManager> FindRelationshipManagerSelectListById(Guid Id)
        {
            if (Id == Guid.Empty)
            {
                throw new ArgumentNullException("Id cannot be null");
            }

            var findRMById = relationshipManagerRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.Id == Id).ToList();

            return findRMById;
        }

        public RelationshipManager FindRelationshipManagerByIdNumber(string IdNumber)
        {
            if (string.IsNullOrWhiteSpace(IdNumber))
            {
                throw new ArgumentNullException("Id Number cannot be null");
            }

            var findRMByIdNumber = relationshipManagerRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.IDNumber == IdNumber).FirstOrDefault();

            return findRMByIdNumber;
        }

        public RelationshipManager FindRelationshipManagerByMobileNumber(string MobileNumber)
        {
            if (string.IsNullOrWhiteSpace(MobileNumber))
            {
                throw new ArgumentNullException("mobile Number cannot be null");
            }

            var findRMByMobileNumber = relationshipManagerRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.MobileNumber == MobileNumber).FirstOrDefault();

            return findRMByMobileNumber;
        }

        public List<RelationshipManager> FindRelationshipManagerByBranchId(Guid BranchId)
        {
            //if (BranchId == Guid.Empty)
            //    throw new ArgumentNullException("null");

            var findRMByBranch = relationshipManagerRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.BranchId == BranchId).ToList();

            return findRMByBranch;
        }

        public List<RelationshipManager> FindRelationshipManagerByBranchIdAndRelationshipManagerId(Guid BranchId, Guid RelationshipManagerid)
        {
            var RMList = new List<RelationshipManager>();
            if (BranchId == Guid.Empty && RelationshipManagerid == Guid.Empty)
            {
                RMList = relationshipManagerRepository.GetAll(IsolationLevel.ReadCommitted).ToList();
            }
            else if (BranchId != Guid.Empty && RelationshipManagerid == Guid.Empty)
            {
                RMList = relationshipManagerRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.BranchId == BranchId).ToList();
            }

            else if (BranchId == Guid.Empty && RelationshipManagerid == Guid.Empty)
            {
                RMList = relationshipManagerRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.Id == RelationshipManagerid).ToList();
            }
            else
            {
                RMList = relationshipManagerRepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.BranchId == BranchId && x.Id == RelationshipManagerid).ToList();
            }

            return RMList;
        }
    }
}

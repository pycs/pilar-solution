﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nikopeshe.WebApplication.Models
{
    public class GraduatedScalePartialViewModel
    {
        [Display(Name = "Lower Limit")]        
        [Required]
        public decimal LowerLimit { get; set; }

        [Display(Name = "Upper Limit")]        
        [Required]
        public decimal UpperLimit { get; set; }

        [Required]
        [Display(Name = "ChargeType")]
        public Enumerations.ChargeTypes ChargeType { get; set; }
        
        [Display(Name = "Charge Percentage")]
        public decimal ChargePercentage { get; set; }
        
        [Display(Name = "Charge Fixed Amount")]
        public decimal ChargeFixedAmount { get; set; }
    }
}
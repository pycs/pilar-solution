﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nikopeshe.Entity.DataModels
{
    public class DomainAudit : BaseEntity
    {
        public DomainAudit()
        {
            Id = Guid.NewGuid();
            CreatedDate = DateTime.Now;
        }
        public string EventType { get; set; }

        public string TableName { get; set; }

        [Column(TypeName = "VARCHAR")]
        [Index("IX_DomainAudit_RecordID")]        
        public string RecordID { get; set; }

        public string AdditionalNarration { get; set; }

        public string ApplicationUserName { get; set; }

        public string EnvironmentUserName { get; set; }

        public string EnvironmentMachineName { get; set; }

        public string EnvironmentDomainName { get; set; }

        public string EnvironmentOSVersion { get; set; }

        public string EnvironmentMACAddress { get; set; }

        public string EnvironmentMotherboardSerialNumber { get; set; }

        public string EnvironmentProcessorId { get; set; }        

        public string RemoteIPAddress { get; set; }

        public DateTime EventDate { get; set; }        
    }
}

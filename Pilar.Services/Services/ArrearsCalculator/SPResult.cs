﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.ArrearsCalculator
{   
    [Serializable]
    public class SPResult
    {        
        public string CustomerFirstName {get; set;}
        public string CustomerMiddleName {get; set;}
        public string CustomerLastName {get; set;}
        public string CustomerName {get; set;}
        public string CustomerIdNumber {get; set;}
        public string PhoneNumber {get; set;}
        public DateTime CustomerDOB {get; set;}
        public string BranchName {get; set;}
        public string BranchCode {get; set;}
        public string ProductName {get; set;}
        public string LoanOfficer {get; set;}
        public DateTime LoanStartDate { get; set; }
        public DateTime NextPaymentDate {get; set;}
        public decimal LoanAmount {get; set;}
        public decimal PrincipalPerInstallment {get; set;}
        public decimal InterestPerInstallment {get; set;}
        public decimal TotalInstallmentAmount {get; set;}
        public decimal ArrearsAmount {get; set;}
        public decimal ExpectedPrincipalInstallment {get; set;}
        public decimal ExpectedInterestInstallment {get; set; }
        public decimal TotalPaidAmount {get; set; }
        public int ElapsedSchedules {get; set; }
        public int OverDueDays {get; set; }        
        public string LoanRequestId { get; set; }
    }
}

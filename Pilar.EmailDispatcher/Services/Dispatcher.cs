﻿using Microsoft.Practices.Unity.Utility;
using Nikopeshe.Infrastructure;
using Pilar.Services.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;
using Nikopeshe.Entity.DataModels;
using Crosscutting.NetFramework.Logging;

namespace Pilar.EmailDispatcher.Services
{
    [Export(typeof(IPlugin))]
    public class Dispatcher : IPlugin
    {
        private MessageQueue _messageQueue;
        private IEmailAlertService _emailAlertService;
        private readonly string _queuePath;
        
        private readonly ISmtpService _smtpService;

        [ImportingConstructor]
        public Dispatcher(            
            ISmtpService smtpService)
        {            
            _emailAlertService = new EmailAlertService();
            _smtpService = smtpService;

            _queuePath = ConfigurationManager.AppSettings["MessageRelayQueuePath"];
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{373F4AB2-EB2D-400A-BBDC-E8BD5BB03430}"); }
        }

        public string Description
        {
            get { return "EMAIL.DISPATCHER"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                if (!MessageQueue.Exists(_queuePath))
                    throw new ArgumentException("Queue does not exist at specified path", "_queuePath");
                else
                {
                    // initialize message queue
                    _messageQueue = new MessageQueue(_queuePath);

                    // enable the AppSpecific field in the messages
                    _messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                    // set the formatter to binary.
                    _messageQueue.Formatter = new BinaryMessageFormatter();

                    // set an event to listen for new messages.
                    _messageQueue.ReceiveCompleted += MessageQueue_ReceiveCompleted;                   

                    // start listening
                    _messageQueue.BeginReceive();
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);                
            }
        }

        public void Exit()
        {
            _messageQueue.Close();
        }

        #endregion

        #region Event Handlers

        private void MessageQueue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                // retrieve sms alert id
                var recordId = (Guid)e.Message.Body;

                // retrieve message category
                var messageCategory = (Enumerations.MessageCategory)e.Message.AppSpecific;

                switch (messageCategory)
                {
                    case Enumerations.MessageCategory.EmailAlert:

                        #region Email

                        var emailAlert =  _emailAlertService.FindEmailAlertById(recordId);

                        if (emailAlert != null && !emailAlert.MailMessageTo.Equals("N/A"))
                        {
                            switch ((Enumerations.DLRStatus)emailAlert.MailMessageDLRStatus)
                            {
                                case Enumerations.DLRStatus.UnKnown:
                                case Enumerations.DLRStatus.Pending:

                                    try
                                    {
                                        if (!string.IsNullOrWhiteSpace(emailAlert.MailMessageCC) && !string.IsNullOrWhiteSpace(emailAlert.MailMessageTo))
                                            _smtpService.SendEmail(emailAlert.MailMessageFrom, emailAlert.MailMessageTo, emailAlert.MailMessageCC, emailAlert.MailMessageSubject, emailAlert.MailMessageBody, emailAlert.MailMessageIsBodyHtml);
                                        else _smtpService.SendEmail(emailAlert.MailMessageFrom, emailAlert.MailMessageTo, emailAlert.MailMessageSubject, emailAlert.MailMessageBody, emailAlert.MailMessageIsBodyHtml);

                                        emailAlert.MailMessageDLRStatus = (int)Enumerations.DLRStatus.Delivered;
                                        emailAlert.MailMessageSendRetry = 1;

                                        _emailAlertService.UpdateEmailAlertDRLStatus(recordId, (int)Enumerations.DLRStatus.Delivered);
                                    }
                                    catch (Exception ex)
                                    {
                                        LoggerFactory.CreateLog().LogInfo("MessageCategory.EmailAlert" + ex);
                                    }

                                    break;
                                default:
                                    break;
                            }
                        }

                        #endregion

                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->Process->ReceiveCompleted FAILED...ReceiveCompletedEventArgs={1}" + ex + Description + e);
            }

            finally
            {
                // listen for the next message.
                _messageQueue.BeginReceive();
            }            
        }

        #endregion
    }
}

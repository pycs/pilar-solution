﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure.Datatables;
using Nikopeshe.Services.Interfaces;
using Nikopeshe.WebApplication.Attributes;
using Nikopeshe.WebApplication.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Web.Mvc;

namespace Nikopeshe.WebApplication.Controllers
{
    [Authorize(Roles = "Administrator")]
    [ExcludeFilter(typeof(AuthorizeIPAddressAttribute))]
    public class ChargesController : BaseController
    {
        private readonly IChargesService chargeservice;
        private readonly IChartofAccountService chartofaccountservice;
        private readonly IGraduatedScaleService graduatesscaleservice;
        ServiceHeader serviceHeader = CustomHeaderUtility.ReadHeader(OperationContext.Current);
        public ChargesController(IChargesService _chargeservice,
            IChartofAccountService _chartofaccountservice,
            IGraduatedScaleService _graduatesscaleservice)
        {
            if (_chargeservice == null || _chartofaccountservice == null)
                throw new ArgumentNullException("Empty Charge Service");

            this.chargeservice = _chargeservice;
            this.graduatesscaleservice = _graduatesscaleservice;
            this.chartofaccountservice = _chartofaccountservice;
        }
        // GET: Charges
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAllCharges(JQueryDataTablesModel datatablemodel)
        {
            var pagecollection = chargeservice.GetAllCharges();

            int totalrecordcount = 0;

            int searchrecordcount = 0;

            var transactionList = GetAllChargesList(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalrecordcount, searchRecordCount: out searchrecordcount, searchString: datatablemodel.sSearch, ChargeList: pagecollection);


            return Json(new JQueryDataTablesResponse<Charge>(items: transactionList,
                totalRecords: totalrecordcount,
                totalDisplayRecords: searchrecordcount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<Charge> GetAllChargesList(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<Charge> ChargeList)
        {
            var GetCustomersList = ChargeList;

            totalRecordCount = GetCustomersList.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetCustomersList = GetCustomersList.Where(c => c.ChargeName
                    .ToLower().Contains(searchString.ToLower())
                    || c.ChartofAccount.ChartofAccountDescription.ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetCustomersList.Count;

            IOrderedEnumerable<Charge> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "ChargeName":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.ChargeName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChargeName);
                        break;

                    case "ChartofAccount.ChartofAccountDescription":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.ChartofAccountDescription)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChartofAccount.ChartofAccountDescription);
                        break;

                    case "IsEnabled":
                        sortedClients = sortedClients == null ? GetCustomersList.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.IsEnabled);
                        break;                    
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }
        
        [HttpGet]
        public ActionResult CreateNewCharge()
        {
            ViewBag.ChartofAccountSelectList = IncomeChartofAccountsSelectList(Guid.Empty.ToString());
            ViewBag.ChargeRecoveryTypeCheckList = ChargeRecoveryMethodCheckList(null);
            ViewBag.ChargeTypes = InterestChargeType(null);
            return View();
        }

        [HttpPost]        
        public ActionResult CreateNewCharge(Charge charge)
        {
            ViewBag.ChartofAccountSelectList = IncomeChartofAccountsSelectList(Guid.Empty.ToString());
            ViewBag.ChargeRecoveryTypeCheckList = ChargeRecoveryMethodCheckList(null);
            ViewBag.ChargeTypes = InterestChargeType(null);
            if(ModelState.IsValid)
            {
                try
                {
                    if (charge.ChartofAccountId == Guid.Empty)
                    {
                        ModelState.AddModelError(string.Empty, "Chart of Accounts cannot be null.");
                        return View(charge);
                    }

                    chargeservice.AddNewCharge(charge, serviceHeader);
                    return RedirectToAction("CreateNewGraduatedScale", "GraduatedScale");        
                }

                catch(Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(charge);
                }
            }
            return View(charge);
        }

        [HttpGet]
        public ActionResult EditCharge(Guid Id)
        {
            var findchargebyid = chargeservice.FindChargeById(Id);
            ViewBag.ChartofAccountSelectList = IncomeChartofAccountsSelectList(Guid.Empty.ToString());
            ViewBag.ChargeRecoveryTypeCheckList = ChargeRecoveryMethodCheckList(findchargebyid);
            
            return View(findchargebyid);
        }

        [HttpPost]
        [ActionName("EditCharge")]        
        public ActionResult EditCharge(Charge charge)
        {
            var findchargebyid = chargeservice.FindChargeById(charge.Id);
            ViewBag.ChargeRecoveryTypeCheckList = ChargeRecoveryMethodCheckList(findchargebyid);
            ViewBag.ChartofAccountSelectList = IncomeChartofAccountsSelectList(Guid.Empty.ToString());
            if(ModelState.IsValid)
            {
                try
                {                    
                    findchargebyid.ChartofAccountId = charge.ChartofAccountId;
                    findchargebyid.ChargeName = charge.ChargeName;
                    findchargebyid.ChargeRecoveryMethod = charge.ChargeRecoveryMethod;
                    findchargebyid.IsRecurring = charge.IsRecurring;
                    findchargebyid.IsEnabled = charge.IsEnabled;
                    chargeservice.UpdateCharge(findchargebyid, serviceHeader);
                    return RedirectToAction("Index");
                }
                
                catch(Exception ex)
                {
                    LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
                    return View(charge);
                }
            }
            return View();
        }

        public ActionResult ChargeGraduatedScales(Guid Id)
        {
            ViewBag.Id = Id;
            string ChargeName = string.Empty;
            var findcustomer = graduatesscaleservice.FindGraduatedScaleByChargeId(Id);
            if (findcustomer != null)
            {
                ChargeName = findcustomer.Charge.ChargeName;
            }
            ViewBag.Name = ChargeName;
            return View();
        }

        public JsonResult GetAllChargeGraduatedScales(Guid Id, JQueryDataTablesModel datatablemodel)
        {
            var pagecollection = graduatesscaleservice.GetAllGraduatedScalesByChargeId(Id, System.Data.IsolationLevel.ReadUncommitted);

            int totalRecordCount = 0;

            int searchRecordCount = 0;

            var customerloanrequest = GraduatedScales(startIndex: datatablemodel.iDisplayStart,
                pageSize: datatablemodel.iDisplayLength, sortedColumns: datatablemodel.GetSortedColumns(),
                totalRecordCount: out totalRecordCount, searchRecordCount: out searchRecordCount, searchString: datatablemodel.sSearch, gradutuatedScaleLIst: pagecollection);


            return Json(new JQueryDataTablesResponse<GraduatedScale>(items: customerloanrequest,
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: datatablemodel.sEcho));
        }

        public static IList<GraduatedScale> GraduatedScales(int startIndex, int pageSize, ReadOnlyCollection<SortedColumn> sortedColumns, out int totalRecordCount, out int searchRecordCount, string searchString, IList<GraduatedScale> gradutuatedScaleLIst)
        {
            var GetgradutuatedScaleLIst = gradutuatedScaleLIst;

            totalRecordCount = GetgradutuatedScaleLIst.Count;

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                GetgradutuatedScaleLIst = GetgradutuatedScaleLIst.Where(c => c.Charge.ChargeName
                    .ToLower().Contains(searchString.ToLower())).ToList();
            }

            searchRecordCount = GetgradutuatedScaleLIst.Count;

            IOrderedEnumerable<GraduatedScale> sortedClients = null;
            foreach (var sortedColumn in sortedColumns)
            {
                switch (sortedColumn.PropertyName)
                {
                    case "CreatedDate":
                        sortedClients = sortedClients == null ? GetgradutuatedScaleLIst.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.CreatedDate);
                        break;

                    case "Charge.ChargeName":
                        sortedClients = sortedClients == null ? GetgradutuatedScaleLIst.CustomSort(sortedColumn.Direction, cust => cust.Charge.ChargeName)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Charge.ChargeName);
                        break;

                    case "ChargeTypeDesc":
                        sortedClients = sortedClients == null ? GetgradutuatedScaleLIst.CustomSort(sortedColumn.Direction, cust => cust.ChargeTypeDesc)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.ChargeTypeDesc);
                        break;

                    case "LowerLimit":
                        sortedClients = sortedClients == null ? GetgradutuatedScaleLIst.CustomSort(sortedColumn.Direction, cust => cust.LowerLimit)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.LowerLimit);
                        break;

                    case "UpperLimit":
                        sortedClients = sortedClients == null ? GetgradutuatedScaleLIst.CustomSort(sortedColumn.Direction, cust => cust.UpperLimit)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.UpperLimit);
                        break;

                    case "Value":
                        sortedClients = sortedClients == null ? GetgradutuatedScaleLIst.CustomSort(sortedColumn.Direction, cust => cust.Value)
                            : sortedClients.CustomSort(sortedColumn.Direction, cust => cust.Value);
                        break;
                }
            }

            return sortedClients.Skip(startIndex).Take(pageSize).ToList();
        }

        [NonAction]
        public List<SelectListItem> IncomeChartofAccountsSelectList(string selectedvalue)
        {
            List<SelectListItem> SelectList = new List<SelectListItem> { };

            var defaultvalue = new SelectListItem { Selected = (selectedvalue == Guid.Empty.ToString("D")), Text = "Select GL Account", Value = Guid.Empty.ToString("D") };

            SelectList.Add(defaultvalue);

            var otherlist = chartofaccountservice.GetChartOfAccountsByTypeAndCategory((int)Enumerations.ChartOfAccountType.Income, (int)Enumerations.ChartOfAccountCategory.DetailAccount);

            foreach (var item in otherlist)
                SelectList.Add(new SelectListItem { Selected = (item.Id == Guid.Parse(selectedvalue)), Text = item.ChartOfAccountDescriptionByCategory, Value = item.Id.ToString("D") });

            return SelectList;
        }
    }
}
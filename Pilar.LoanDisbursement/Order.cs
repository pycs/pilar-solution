﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.LoanDisbursement
{
    public class Order
    {
        public int Type { get; set; }

        [JsonIgnore]
        public string TypeDesc { get; set; }

        public Guid CompanyId { get; set; }

        [JsonIgnore]
        public string CompanyDesc { get; set; }

        public string Remarks { get; set; }

        [JsonIgnore]
        public bool IsDelivered { get; set; }

        public List<OrderLine> OrderLines { get; set; }
    }
}

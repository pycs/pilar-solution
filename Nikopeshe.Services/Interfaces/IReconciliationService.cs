﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System.Collections.Generic;

namespace Nikopeshe.Services.Interfaces
{
    public interface IReconciliationService
    {
        void Add(Reconciliation reconFile, ServiceHeader serviceHeader);

        IList<Reconciliation> FindAllReconciliationFiles();

        IList<Reconciliation> FindAllReconciliationFilesByStatus(int Status);
    }
}

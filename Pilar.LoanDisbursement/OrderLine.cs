﻿using Newtonsoft.Json;

namespace Pilar.LoanDisbursement
{
    public class OrderLine
    {
        [JsonIgnore]
        public string TypeDesc { get; set; }

        public string Payee { get; set; }

        public string PrimaryAccountNumber { get; set; }

        public decimal Amount { get; set; }

        public string BankCode { get; set; }

        public int Type { get; set; }

        public int MCCMNC { get; set; }

        [JsonIgnore]
        public string MCCMNCDesc { get; set; }

        public string Reference { get; set; }

        public string SystemTraceAuditNumber { get; set; }

        [JsonIgnore]
        public int Status { get; set; }

        [JsonIgnore]
        public string StatusDesc { get; set; }

        [JsonIgnore]
        public string B2MResponseCode { get; set; }

        [JsonIgnore]
        public string B2MResponseDesc { get; set; }

        [JsonIgnore]
        public string B2MResultCode { get; set; }

        [JsonIgnore]
        public string B2MResultDesc { get; set; }

        [JsonIgnore]
        public string B2MTransactionID { get; set; }

        [JsonIgnore]
        public string TransactionDateTime { get; set; }

        [JsonIgnore]
        public decimal WorkingAccountAvailableFunds { get; set; }

        [JsonIgnore]
        public decimal UtilityAccountAvailableFunds { get; set; }

        [JsonIgnore]
        public decimal ChargePaidAccountAvailableFunds { get; set; }

        [JsonIgnore]
        public string TransactionCreditParty { get; set; }
    }
}

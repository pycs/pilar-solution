﻿using Nikopeshe.Entity.DataModels;
using Nikopeshe.Entity.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services
{
    [Export(typeof(IMessageQueueService))]
    public class MessageQueueService : IMessageQueueService
    {
        public string Send(string queuePath, object data, Enumerations.MessageCategory messageCategory, MessagePriority priority)
        {
            var result = string.Empty;

            if (!string.IsNullOrWhiteSpace(queuePath) && MessageQueue.Exists(queuePath))
            {
                // Open the queue.
                using (MessageQueue messageQueue = new MessageQueue(queuePath))
                {
                    // Enable the AppSpecific field in the messages.
                    messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                    // Set the formatter to binary.
                    messageQueue.Formatter = new BinaryMessageFormatter();

                    // Since we're using a transactional queue, make a transaction.
                    using (MessageQueueTransaction mqt = new MessageQueueTransaction())
                    {
                        mqt.Begin();

                        // Create message.
                        using (Message message = new Message(data, new BinaryMessageFormatter()))
                        {
                            message.Label = string.Format("Message Category: {0}", EnumHelper.GetDescription(messageCategory));
                            message.AppSpecific = (int)messageCategory;
                            message.Priority = priority;

                            // store message Id
                            result = data.ToString();

                            // Send the message.
                            messageQueue.Send(message, mqt);
                        }

                        mqt.Commit();
                    }
                }
            }

            return result;
        }
    }
}

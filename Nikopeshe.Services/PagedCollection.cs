﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services
{
    //[DataContract]
    public class PagedCollection<TEntity> : Entity
    {
        #region Fields

        private int _pageSize = 12;

        #endregion

        #region Properties

        //[DataMember]
        public int PageIndex { get; set; }

        //[DataMember]
        public virtual int PageSize
        {
            get { return _pageSize; }
            set { _pageSize = value; }
        }

        //[DataMember]
        public IEnumerable<TEntity> Items { get; set; }

        //[DataMember]
        public int TotalItems { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Adapter a PagedCollection
        /// </summary>
        /// <typeparam name="T">Type of paged collection</typeparam>
        /// <typeparam name="TEntity">Type of class a convert</typeparam>
        /// <param name="pagedCollection">Paged collection</param>
        /// <param name="model">Model to adapter</param>
        public void PagedCollectionAdapter<T>(PagedCollection<TEntity> pagedCollection, ref T model) where T : PagedCollection<TEntity>, new()
        {
            if (pagedCollection == null || model == null)
                throw new ArgumentNullException("model");
            model = new T { PageIndex = pagedCollection.PageIndex, PageSize = pagedCollection.PageSize, Items = pagedCollection.Items, TotalItems = pagedCollection.TotalItems };
        }

        #endregion
    }
}

﻿using Crosscutting.NetFramework.Logging;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Infrastructure;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.IPNService
{
    public class IPNService : IIPNService
    {
        private DataContext datacontext = new DataContext();

        private string ErrorMessage = string.Empty;

        public void UpdateC2BTransaction(C2BTransaction c2bmapping)
        {
            if (c2bmapping == null)
                throw new ArgumentNullException("entity cannot be null");

            try
            {
                using (var context = new DataContext())
                {
                    var entity = context.Entry(c2bmapping);
                    entity.State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                }
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                LoggerFactory.CreateLog().LogInfo("Error -> {0}", ex.Message + ex.InnerException + ex.StackTrace);
            }
        }

        public IList<C2BTransaction> FindC2BTransactionByStatus(int Status)
        {
            var findc2bbystatus = new List<C2BTransaction>();

            using (datacontext)
            {
                using (var transaction = datacontext.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted))
                {
                    findc2bbystatus = datacontext.C2BTransaction.Where(x => x.Status == Status).ToList();
                }
            }
            return findc2bbystatus.ToList();
        }

        public C2BTransaction FindC2BTransactionByThirdPartyTransID(string ThirdPartyTransID)
        {
            if (string.IsNullOrWhiteSpace(ThirdPartyTransID))
                throw new ArgumentNullException("Third Party Transaction cannot be null");

            var findc2btransactionbythirdpartyid = datacontext.C2BTransaction.Where(x => x.ThirdPartyTransID == ThirdPartyTransID);

            return findc2btransactionbythirdpartyid.FirstOrDefault();
        }

        public C2BTransaction FindC2BTransactionById(Guid Id)
        {
            if (string.IsNullOrWhiteSpace(Id.ToString()))
                throw new ArgumentNullException("Transaction Id cannot be null");

            var findc2btransctionbytransactionid = datacontext.C2BTransaction.Find(Id);

            return findc2btransctionbytransactionid;
        }

        public IList<LoanProductsCharge> GetRecurringLoanProductChargeByProductId(Guid LoanProductId)
        {
            if (LoanProductId == Guid.Empty)
                throw new ArgumentNullException("null id");

            var findRecuringChargesByProduct = datacontext.LoanProductsCharge.Where(x => x.Charges.IsRecurring == true && x.LoanProductId == LoanProductId);

            return findRecuringChargesByProduct.ToList();
        }

        public LoanTransactionHistory FindLoanTransactionHistoryByC2BTransactionId(Guid C2BTransactionId)
        {
            if (C2BTransactionId == Guid.Empty)
                throw new ArgumentNullException("null");

            var findLoanRequestByC2BId = datacontext.LoanTransactionHistory.Where(x => x.C2BTransactionId == C2BTransactionId).FirstOrDefault();

            return findLoanRequestByC2BId;
        }

        public CustomerAccountInfo CustomerSavingsAccountBalance(Guid CustomerSavingsAccountId)
        {
            var customerAccountDetails = new CustomerAccountInfo();

            if (CustomerSavingsAccountId != Guid.Empty)
            {
                var idStartDate = new SqlParameter { ParameterName = "@CustomerAccountID", Value = CustomerSavingsAccountId };

                customerAccountDetails = datacontext.Database.SqlQuery<CustomerAccountInfo>("sp_Customer_Savings_Loan_AccountBalance @CustomerAccountID", idStartDate).Cast<CustomerAccountInfo>().FirstOrDefault();
            }

            return customerAccountDetails;
        }

        public Tuple<decimal, decimal, decimal> ExpectedTotalPrincipalExpectedTotalInterestExpectedTotalCharges(Guid LoanRequestId)
        {
            var expectedTotalPrincipal = 0m;
            var expectedTotalInterest = 0m;
            var expectedTotalCharges = 0m;
            if (LoanRequestId == Guid.Empty)
            {
                throw new ArgumentNullException("loan request id cannot be null");
            }

            var findLoanRequestById = datacontext.LoanRequest.Find(LoanRequestId);
            if (findLoanRequestById == null)
            {
                throw new ArgumentNullException("could not find loan request by id provided");
            }

            expectedTotalPrincipal = findLoanRequestById.LoanAmount;
            expectedTotalInterest = findLoanRequestById.InterestAmount;

            var findLoanProductById = datacontext.LoanProduct.Find(findLoanRequestById.LoanProductId);
            var findLoanProductCharges = datacontext.LoanProductsCharge.Where(x => x.LoanProductId == findLoanProductById.Id).ToList();
            if (findLoanProductCharges != null && findLoanProductCharges.Any())
            {
                foreach (var loanProductCharge in findLoanProductCharges)
                {
                    var findPerInstallmentChargesById = datacontext.Charge.Where(x => x.Id == loanProductCharge.ChargeId &&  x.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment).FirstOrDefault();
                    var findGraduatedScaleByChargeid = (from a in datacontext.GraduatedScale
                                                        where a.ChargeId == findPerInstallmentChargesById.Id && (a.LowerLimit <= findLoanRequestById.LoanAmount && a.UpperLimit >= findLoanRequestById.LoanAmount)
                                                        select a);
                    if (findPerInstallmentChargesById != null)
                    {
                        foreach (var graduatedScale in findGraduatedScaleByChargeid)
                        {
                            var chargeType = graduatedScale.ChargeType;
                            if (chargeType == Enumerations.ChargeTypes.FixedAmount)
                            {
                                expectedTotalCharges = (graduatedScale.Value * findLoanRequestById.NegotiatedInstallments) + expectedTotalCharges;
                            }

                            else
                            {
                                expectedTotalCharges = (((graduatedScale.Value * findLoanRequestById.LoanAmount) / 100) * findLoanRequestById.NegotiatedInstallments) + expectedTotalCharges;
                            }
                        }
                    }
                }                
            }
            return new Tuple<decimal, decimal, decimal>(expectedTotalPrincipal, expectedTotalInterest, expectedTotalCharges);
        }

        public Tuple<decimal, decimal, decimal> PaidPrincipalPaidInterestPaidCharges(Guid LoanRequestId, Guid CustomerSavingAccountId, Guid LoanProductId)
        {
            if (LoanRequestId == Guid.Empty || CustomerSavingAccountId == Guid.Empty)
            {
                throw new ArgumentNullException("loan request id cannot be null");
            }

            var findLoanRequestById = datacontext.LoanRequest.Find(LoanRequestId);
            var findLoanProductById = datacontext.LoanProduct.Find(LoanProductId);
            if (findLoanRequestById == null)
            {
                throw new ArgumentNullException("could not find loan request by id provided");
            }
            var totalpaidpricipalamount = 0m;
            var totalPaidInterestAmounnt = 0m;
            var totalPaidChargeAmount = 0m;
            var findloantransactionsbyloanrequestid = new List<LoanTransactionHistory>();
            var findInteresttransactionsbyloanrequestid = new List<LoanTransactionHistory>();
            var findChargetransactionsbyloanrequestid = new List<LoanTransactionHistory>();

            findloantransactionsbyloanrequestid = datacontext.LoanTransactionHistory.Where(x => x.LoanRequestId == LoanRequestId
                                                                    && x.IsApproved == true && x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment
                                                                    && x.TransactionType == Enumerations.TransactionType.Credit && x.CustomersAccountId == findLoanRequestById.CustomerAccountId && x.ChartofAccountId == findLoanProductById.LoanGLAccountId).ToList();
            if(findloantransactionsbyloanrequestid != null)
            {
                totalpaidpricipalamount = findloantransactionsbyloanrequestid.Sum(x => x.TransactionAmount);
            }
            
            findInteresttransactionsbyloanrequestid = datacontext.LoanTransactionHistory.Where(x => x.LoanRequestId == LoanRequestId
                                                                    && x.IsApproved == true && x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment
                                                                    && x.TransactionType == Enumerations.TransactionType.Credit && x.ChartofAccountId == findLoanProductById.InterestGLAccountId).ToList();
            if (findInteresttransactionsbyloanrequestid != null)
            {
                totalPaidInterestAmounnt = findInteresttransactionsbyloanrequestid.Sum(x => x.TransactionAmount);
            }
            var findLoanProductCharges = datacontext.LoanProductsCharge.Where(x => x.LoanProductId == findLoanProductById.Id && x.Charges.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment).ToList();
            if (findLoanProductCharges != null && findLoanProductCharges.Any())
            {
                foreach (var loanProductCharge in findLoanProductCharges)
                {
                    findChargetransactionsbyloanrequestid = datacontext.LoanTransactionHistory.Where(x => x.LoanRequestId == LoanRequestId
                                                                    && x.IsApproved == true && x.TransactionCategory == (int)Enumerations.TransactionCategory.Repayment
                                                                    && x.TransactionType == Enumerations.TransactionType.Credit && x.ChartofAccountId == loanProductCharge.Charges.ChartofAccountId).ToList();
                    if (findChargetransactionsbyloanrequestid != null)
                    {
                        totalPaidChargeAmount = findChargetransactionsbyloanrequestid.Sum(x => x.TransactionAmount) + totalPaidChargeAmount;
                    }
                }
            }
            return new Tuple<decimal, decimal, decimal>(totalpaidpricipalamount, totalPaidInterestAmounnt, totalPaidChargeAmount);
        }

        public IList<C2BTransaction> FindC2BPendingVerificationAndProcessing()
        {
            var pendingVerificationAndProcessing = datacontext.C2BTransaction.Where(x => x.Status == (int)Enumerations.C2BTransactionStatus.Pending || x.Status == (int)Enumerations.C2BTransactionStatus.Approved);

            return pendingVerificationAndProcessing.ToList();
        }

        public Tuple<decimal, decimal, decimal> ExpectedPrincipalExpectedInterestExpectedChargesGivenElapsedSchedules(Guid LoanRequestId, int ElapsedSchedules)
        {
            var expectedPrincipal = 0m;            
            var expectedInterest = 0m;
            var expectedFixedCharges = 0m;
            var expectedPercentageCharge = 0m;
            var expectedTotalCharges = 0m;
            
            if (LoanRequestId == Guid.Empty)
            {
                throw new ArgumentNullException("loan request id cannot be null");
            }

            var findLoanRequestById = datacontext.LoanRequest.Find(LoanRequestId);
            if (findLoanRequestById == null)
            {
                throw new ArgumentNullException("could not find loan request by id provided");
            }

            expectedPrincipal = findLoanRequestById.LoanAmount / findLoanRequestById.NegotiatedInstallments * ElapsedSchedules;
            expectedInterest = findLoanRequestById.InterestAmount / findLoanRequestById.NegotiatedInstallments * ElapsedSchedules;

            var findLoanProductById = datacontext.LoanProduct.Find(findLoanRequestById.LoanProductId);
            var findLoanProductCharges = datacontext.LoanProductsCharge.Where(x => x.LoanProductId == findLoanProductById.Id).ToList();
            if (findLoanProductCharges != null && findLoanProductCharges.Any())
            {
                foreach (var loanProductCharge in findLoanProductCharges)
                {
                    var findPerInstallmentChargesById = datacontext.Charge.Where(x => x.Id == loanProductCharge.ChargeId && x.ChargeRecoveryMethod == (int)Enumerations.ChargeRecoveryMethod.PerInstallment).FirstOrDefault();
                    var findGraduatedScaleByChargeid = (from a in datacontext.GraduatedScale
                                                        where a.ChargeId == findPerInstallmentChargesById.Id && (a.LowerLimit <= findLoanRequestById.LoanAmount && a.UpperLimit >= findLoanRequestById.LoanAmount)
                                                        select a);
                    if (findPerInstallmentChargesById != null)
                    {
                        foreach (var graduatedScale in findGraduatedScaleByChargeid)
                        {
                            var chargeType = graduatedScale.ChargeType;
                            if (chargeType == Enumerations.ChargeTypes.FixedAmount)
                            {
                                expectedFixedCharges = graduatedScale.Value;
                            }

                            else
                            {
                                expectedPercentageCharge = ((graduatedScale.Value * findLoanRequestById.LoanAmount) / 100);
                            }
                        }
                        expectedTotalCharges = (expectedFixedCharges + expectedPercentageCharge) * ElapsedSchedules;
                    }
                }
            }
            return new Tuple<decimal, decimal, decimal>(expectedPrincipal, expectedInterest, expectedTotalCharges);
        }

        public IList<C2BTransaction> FindQueuedConsumerToBusinessTransactionsWithGivenElapseTimeInMinutes(int minutes)
        {
            var C2BTransactionsList = new List<C2BTransaction>();

            using (var context = new DataContext())
            {
                var findQueuedC2B = context.C2BTransaction.Where(x => x.Status == (int)Enumerations.C2BTransactionStatus.Queued);

                if (findQueuedC2B != null && findQueuedC2B.Any())
                {
                    foreach (var c2b in findQueuedC2B)
                    {
                        var timeNow = DateTime.Now;

                        var timeDifference = (timeNow - c2b.CreatedDate).TotalMinutes;

                        var getLoanTrxsHistoryByC2BId = context.LoanTransactionHistory.Where(x => x.C2BTransactionId == c2b.Id).Count();

                        if (timeDifference > minutes && getLoanTrxsHistoryByC2BId == 0)
                        {
                            C2BTransactionsList.Add(new C2BTransaction() 
                            {
                                Id = c2b.Id,
                                BillRefNumber = c2b.BillRefNumber,
                                BusinessShortCode = c2b.BusinessShortCode,
                                Commands = c2b.Commands,
                                CreatedDate = c2b.CreatedDate,
                                FileName = c2b.FileName,
                                InvoiceNumber = c2b.InvoiceNumber,
                                KYCInfo_FirstName = c2b.KYCInfo_FirstName,
                                KYCInfo_LastName = c2b.KYCInfo_LastName,
                                KYCInfo_MiddleName = c2b.KYCInfo_MiddleName,
                                MSISDN = c2b.MSISDN,
                                Result = c2b.Result,
                                Status = c2b.Status,
                                ThirdPartyTransID = c2b.ThirdPartyTransID,
                                TransactionId = c2b.TransactionId,
                                TransAmount = c2b.TransAmount,
                                TransID = c2b.TransID,
                                TransTime = c2b.TransTime
                            });
                        }
                    }
                }
            }

            return C2BTransactionsList;
        }
    }
}

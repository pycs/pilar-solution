﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.WindowsService.Interfaces
{
    public interface ISmtpService
    {
        void AddAttachment(string filePath);

        void SendEmail(MailMessage mailMessage);

        void SendEmail(MailAddress from, MailAddressCollection to, string subject, string body, bool isBodyHtml);

        void SendEmail(string from, string to, string subject, string body, bool isBodyHtml);

        void SendEmail(MailAddress from, MailAddressCollection to, MailAddressCollection cc, string subject, string body, bool isBodyHtml);

        void SendEmail(string from, string to, string cc, string subject, string body, bool isBodyHtml);
    }
}

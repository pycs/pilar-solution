﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using System;

namespace Nikopeshe.Services.Interfaces
{
    public interface IPasswordRecoveryRequestService
    {
        void AddNewPasswordRecoveryRequest(PasswordRecoveryRequest passwordrecoveryrequest, ServiceHeader serviceHeader);

        PasswordRecoveryRequest GetPasswordRecoveryRequestByVerificationId(Guid Id);

        void UpdatePasswordRecoveryRequest(PasswordRecoveryRequest passwordrecoveryrequest, ServiceHeader serviceHeader);
    }
}

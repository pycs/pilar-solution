﻿using Crosscutting.NetFramework.Logging;
using Infrastructure.Crosscutting.NetFramework;
using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Pilar.Services.Services;
using Pilar.Services.Services.ArrearsCalculator;
using Pilar.Services.Services.IPNService;
using System;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Messaging;

namespace Pilar.ArrearsCalculator.Services
{
    [Export(typeof(IPlugin))]
    public class Processor : IPlugin
    {
        private MessageQueue _messageQueue;
        private readonly string _queuePath;
        private readonly IIPNService ipnService;
        private readonly ICalculator icalculator;

        [ImportingConstructor]
        public Processor()
        {
            _queuePath = ConfigurationManager.AppSettings["ArrearsQueuePath"];
            ipnService = new IPNService();
            icalculator = new Calculator();
        }

        #region IPlugin

        public Guid Id
        {
            get { return new Guid("{301B01A8-B49B-4FB8-8F8E-71384882ED61}"); }
        }

        public string Description
        {
            get { return "ARREARS.PROCESSOR"; }
        }

        public void DoWork(params string[] args)
        {
            try
            {
                if (!MessageQueue.Exists(_queuePath))
                    throw new ArgumentException("Queue does not exist at specified path", "_queuePath");
                else
                {
                    // initialize message queue
                    _messageQueue = new MessageQueue(_queuePath);

                    // enable the AppSpecific field in the messages
                    _messageQueue.MessageReadPropertyFilter.AppSpecific = true;

                    // set the formatter to binary.
                    _messageQueue.Formatter = new BinaryMessageFormatter();

                    // set an event to listen for new messages.
                    _messageQueue.ReceiveCompleted += MessageQueue_ReceiveCompleted;

                    // start listening
                    _messageQueue.BeginReceive();
                }
            }
            catch (Exception ex)
            {
                LoggerFactory.CreateLog().LogInfo("{0}->DoWork..." + ex + Description);
            }
        }

        public void Exit()
        {
            _messageQueue.Close();
        }

        #endregion

        #region Event Handlers
        private void MessageQueue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                // retrieve message category
                var messageCategory = (Nikopeshe.Entity.DataModels.Enumerations.MessageCategory)e.Message.AppSpecific;
                switch (messageCategory)
                {
                    case Nikopeshe.Entity.DataModels.Enumerations.MessageCategory.Arrears:

                        #region Arrears

                        var loanInArrears = (Pilar.Services.Services.ArrearsCalculator.SPResult)e.Message.Body;
                        var datacontext = new DataContext();
                        if (loanInArrears != null)
                        {
                            var findDefaultSavingAccount = datacontext.SavingsProduct.Where(x => x.IsDefault == true).FirstOrDefault();
                            var parseLoanRequestId = Guid.Parse(loanInArrears.LoanRequestId);
                            var findLoanRequestById = datacontext.LoanRequest.Find(parseLoanRequestId);

                            using (var dataContext = new DataContext())
                            {
                                using (var transaction = datacontext.Database.BeginTransaction())
                                {
                                    try
                                    {
                                        if (findLoanRequestById != null)
                                        {
                                            DateTime now = DateTime.Now;
                                            DateTime nextPaymentDate = findLoanRequestById.NextPaymentDate.Value;
                                            TimeSpan dateDifference = now - nextPaymentDate;
                                            double difference = dateDifference.TotalDays;
                                            if (difference > 0)
                                            {
                                                if (findDefaultSavingAccount.Id != Guid.Empty)
                                                {
                                                    if (parseLoanRequestId != Guid.Empty)
                                                    {
                                                        var findCustomerSavingsAccount = icalculator.FindCustomerSavingsAccount(parseLoanRequestId);
                                                        if (findCustomerSavingsAccount != null)
                                                        {
                                                            decimal findCustomerAccountBalance = ipnService.CustomerSavingsAccountBalance(findCustomerSavingsAccount, findDefaultSavingAccount.ChartofAccountId);
                                                            if (findCustomerAccountBalance > 0m && loanInArrears.ElapsedSchedules > 0)
                                                            {
                                                                decimal interestInArrears = 0m;
                                                                decimal principalInArrears = 0m;                                                               
                                                                decimal receivedInterestInArrears = 0m;                                                                
                                                                decimal receivedPrincipalInArrears = 0m;
                                                                decimal TransactionAmount = findCustomerAccountBalance;

                                                                var getPrincipalInterestChargeInArrearsGivenElapsedSchedules = ipnService.ExpectedPrincipalExpectedInterestExpectedChargesGivenElapsedSchedules(parseLoanRequestId, loanInArrears.ElapsedSchedules);
                                                                var PaidPrincipalInterestCharges = ipnService.PaidPrincipalPaidInterestPaidCharges(parseLoanRequestId, findCustomerSavingsAccount, findLoanRequestById.LoanProductId);
                                                                var ExpectedTotalPrincipalInterestCharges = ipnService.ExpectedTotalPrincipalExpectedTotalInterestExpectedTotalCharges(parseLoanRequestId);

                                                                #region
                                                                //Check if expected interest has  been paid
                                                                if (getPrincipalInterestChargeInArrearsGivenElapsedSchedules.Item2 > PaidPrincipalInterestCharges.Item2)
                                                                {
                                                                    int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                                    interestInArrears = getPrincipalInterestChargeInArrearsGivenElapsedSchedules.Item2 - PaidPrincipalInterestCharges.Item2;
                                                                    if (TransactionAmount >= interestInArrears)
                                                                    {
                                                                        receivedInterestInArrears = interestInArrears;
                                                                    }
                                                                    else
                                                                    {
                                                                        receivedInterestInArrears = TransactionAmount;
                                                                    }

                                                                    LoanTransactionHistory debitsavingsgl = new LoanTransactionHistory()
                                                                    {
                                                                        LoanRequestId = parseLoanRequestId,
                                                                        TransactionType = Enumerations.TransactionType.Debit,
                                                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                        TransactionAmount = receivedInterestInArrears,
                                                                        TotalTransactionAmount = TransactionAmount,
                                                                        ChartofAccountId = findDefaultSavingAccount.ChartofAccountId,
                                                                        ContraAccountId = Guid.Parse(findLoanRequestById.LoanProduct.InterestGLAccountId.ToString()),
                                                                        CustomersAccountId = findCustomerSavingsAccount,
                                                                        Description = "Arrears - Interest Liquidation",
                                                                        CreatedBy = "System",
                                                                        IsApproved = true,
                                                                        ApprovedBy = "System",
                                                                        TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                        BranchId = findLoanRequestById.CustomerAccount.Customer.BranchId,
                                                                        TransactionBatchNumber = batchNo
                                                                    };

                                                                    LoanTransactionHistory creditincomegl = new LoanTransactionHistory()
                                                                    {
                                                                        LoanRequestId = parseLoanRequestId,
                                                                        TransactionType = Enumerations.TransactionType.Credit,
                                                                        RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                        TransactionAmount = receivedInterestInArrears,
                                                                        TotalTransactionAmount = TransactionAmount,
                                                                        ChartofAccountId = findLoanRequestById.LoanProduct.InterestGLAccountId,
                                                                        ContraAccountId = findDefaultSavingAccount.ChartofAccountId,
                                                                        CustomersAccountId = findLoanRequestById.CustomerAccountId,
                                                                        Description = "Arrears - Interest Liquidation",
                                                                        CreatedBy = "System",
                                                                        IsApproved = true,
                                                                        ApprovedBy = "System",
                                                                        TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                        BranchId = findLoanRequestById.CustomerAccount.Customer.BranchId,
                                                                        TransactionBatchNumber = batchNo
                                                                    };

                                                                    datacontext.LoanTransactionHistory.Add(debitsavingsgl);
                                                                    datacontext.LoanTransactionHistory.Add(creditincomegl);

                                                                    TransactionAmount = TransactionAmount - receivedInterestInArrears;
                                                                }
                                                                #endregion

                                                                #region Recover Expected Principal Amount
                                                                if (getPrincipalInterestChargeInArrearsGivenElapsedSchedules.Item1 > PaidPrincipalInterestCharges.Item1)
                                                                {
                                                                    principalInArrears = getPrincipalInterestChargeInArrearsGivenElapsedSchedules.Item1 - PaidPrincipalInterestCharges.Item1;

                                                                    if (TransactionAmount >= principalInArrears)
                                                                    {
                                                                        receivedPrincipalInArrears = principalInArrears;
                                                                    }
                                                                    else
                                                                    {
                                                                        receivedPrincipalInArrears = TransactionAmount;
                                                                    }
                                                                    TransactionAmount = TransactionAmount - principalInArrears;
                                                                    if (receivedPrincipalInArrears > 0m)
                                                                    {
                                                                        int batchNo = RandomBatchNumberGenerator.GetRandomNumber();
                                                                        LoanTransactionHistory debitsavingsgl = new LoanTransactionHistory()
                                                                        {
                                                                            LoanRequestId = parseLoanRequestId,
                                                                            TransactionType = Enumerations.TransactionType.Debit,
                                                                            RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                            TransactionAmount = receivedPrincipalInArrears,
                                                                            TotalTransactionAmount = receivedPrincipalInArrears,
                                                                            ChartofAccountId = findDefaultSavingAccount.ChartofAccountId,
                                                                            ContraAccountId = findLoanRequestById.LoanProduct.LoanGLAccountId,
                                                                            CustomersAccountId = findCustomerSavingsAccount,
                                                                            Description = "Arrears - Principal Liquidation",
                                                                            CreatedBy = "System",
                                                                            IsApproved = true,
                                                                            ApprovedBy = "System",
                                                                            TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                            BranchId = findLoanRequestById.CustomerAccount.Customer.BranchId,
                                                                            TransactionBatchNumber = batchNo
                                                                        };
                                                                        datacontext.LoanTransactionHistory.Add(debitsavingsgl);

                                                                        LoanTransactionHistory creditloangl = new LoanTransactionHistory()
                                                                        {
                                                                            LoanRequestId = parseLoanRequestId,
                                                                            TransactionType = Enumerations.TransactionType.Credit,
                                                                            RepaymentType = Enumerations.RepaymentType.MPesa,
                                                                            TransactionAmount = receivedPrincipalInArrears,
                                                                            TotalTransactionAmount = receivedPrincipalInArrears,
                                                                            ChartofAccountId = findLoanRequestById.LoanProduct.LoanGLAccountId,
                                                                            ContraAccountId = findDefaultSavingAccount.ChartofAccountId,
                                                                            CustomersAccountId = findLoanRequestById.CustomerAccountId,
                                                                            Description = "Arrears - Principal Liquidation",
                                                                            CreatedBy = "System",
                                                                            IsApproved = true,
                                                                            ApprovedBy = "System",
                                                                            TransactionCategory = (int)Enumerations.TransactionCategory.Repayment,
                                                                            BranchId = findLoanRequestById.CustomerAccount.Customer.BranchId,
                                                                            TransactionBatchNumber = batchNo
                                                                        };
                                                                        datacontext.LoanTransactionHistory.Add(creditloangl);
                                                                    }
                                                                }

                                                                if (findCustomerAccountBalance >= loanInArrears.ArrearsAmount)
                                                                {
                                                                    if (findLoanRequestById.LoanProduct.PaymentFrequencyType == Enumerations.PaymentFrequency.Monthly)
                                                                    {
                                                                        findLoanRequestById.NextPaymentDate = findLoanRequestById.NextPaymentDate.Value.AddMonths(loanInArrears.ElapsedSchedules);
                                                                    }
                                                                    else
                                                                    {

                                                                        findLoanRequestById.NextPaymentDate = findLoanRequestById.NextPaymentDate.Value.AddDays(7 * loanInArrears.ElapsedSchedules);
                                                                    }

                                                                    var entry = datacontext.Entry(findLoanRequestById);
                                                                    entry.State = EntityState.Modified;
                                                                }
                                                                #endregion
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        datacontext.SaveChanges();
                                        transaction.Commit();
                                    }                                        
                                    catch(Exception ex)
                                    {
                                        transaction.Rollback();
                                        LoggerFactory.CreateLog().LogError("Error -> {0}", ex);
                                    }
                                }                                
                            }
                        }

                        #endregion

                        break;
                }
            }

            catch (Exception ex)
            {                
                LoggerFactory.CreateLog().LogError("Error -> {0}", ex);
            }

            finally
            {
                _messageQueue.BeginReceive();
            }
        }
        #endregion
    }
}

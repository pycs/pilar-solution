﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class UserInfoService : IUserInfoService
    {
        readonly IRepository<UserInfo> userinforepository;

        string errormessage = string.Empty;

        public UserInfoService(IRepository<UserInfo> _userinforepository)
        {
            if(_userinforepository == null)
                throw new ArgumentNullException("null repository");

            this.userinforepository = _userinforepository;
        }

        public void AddNewUser(UserInfo userinfo, ServiceHeader serviceHeader)
        {
            if (userinfo == null)
                throw new ArgumentNullException("null entity");

            userinforepository.Insert(userinfo, serviceHeader);
        }

        public IList<UserInfo> GetAllUsers()
        {
            var getallusers = userinforepository.GetAll(IsolationLevel.ReadUncommitted);

            return getallusers.ToList();
        }

        public UserInfo FindUserByUserName(string username)
        {
            var finduserbyusername = userinforepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.UserName == username);

            return finduserbyusername.FirstOrDefault();
        }

        public void UpdateUser(UserInfo userinfo, ServiceHeader serviceHeader)
        {
            try
            {
                if (userinfo == null)
                    throw new ArgumentNullException("userinfo cannot be null");                

                userinforepository.Update(userinfo, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationerrors in ex.EntityValidationErrors)
                {
                    foreach (var validationerror in validationerrors.ValidationErrors)
                    {
                        errormessage += string.Format("Error message: {0} Property{1}", validationerror.ErrorMessage, validationerror.PropertyName);
                    }
                }
                throw new Exception(errormessage, ex);
            }
        }

        public UserInfo FindUserById(Guid id)
        {
            return userinforepository.GetById(id);
        }
    }
}

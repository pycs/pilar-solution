﻿using Nikopeshe.Entity.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pilar.Services.Services.TextAlertServices
{
    public interface ITextAlertService
    {
        void AddNewTextAlert(TextAlert emailalert);

        IList<TextAlert> FindTextAlertByDRLStatus(int status);

        void UpdateTextAlertDRLStatus(Guid Id, int status);

        void UpdateTextAlertDRLStatus(Guid Id, int TextMessageDLRStatus, string ATMessageId, string Cost, string Status);

        TextAlert FindTextAlertById(Guid Id);
    }
}

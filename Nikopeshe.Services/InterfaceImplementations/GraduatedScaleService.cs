﻿using Nikopeshe.Data;
using Nikopeshe.Entity.DataModels;
using Nikopeshe.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nikopeshe.Services.InterfaceImplementations
{
    public class GraduatedScaleService : IGraduatedScaleService
    {
        private readonly IRepository<GraduatedScale> graduatedscalerepository;

        private string ErrorMessage = string.Empty;

        private DataContext datacontext = new DataContext();

        public GraduatedScaleService(IRepository<GraduatedScale> _graduatedscalerepository)
        {
            if (_graduatedscalerepository == null)
                throw new ArgumentNullException("repository cannot be null.");

            this.graduatedscalerepository = _graduatedscalerepository;
        }

        public void AddNewGraduatedScale(GraduatedScale graduatedscale, ServiceHeader serviceHeader)
        {
            if (graduatedscale == null)
                throw new ArgumentNullException("entity cannot be null.");

            try
            {
                graduatedscalerepository.Insert(graduatedscale, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            } 
        }

        public void UpdateGraduatedScale(GraduatedScale graduatedscale, ServiceHeader serviceHeader)
        {
            if(graduatedscale == null)
                throw new ArgumentNullException("entity cannot be null.");

            try
            {
                graduatedscalerepository.Update(graduatedscale, serviceHeader);
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        ErrorMessage += string.Format("Property: {0} Error: {1}",
                        validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }
                }
                throw new Exception(ErrorMessage, ex);
            } 
        }

        public GraduatedScale FindGraduatedScaleById(Guid Id)
        {
            if(Id == Guid.Empty)
                throw new ArgumentNullException("Guid cannot be null");

            var findgraduatedscalebyid = graduatedscalerepository.GetById(Id);

            return findgraduatedscalebyid;
        }

        public GraduatedScale FindGraduatedScaleByChargeId(Guid ChargeId)
        {
            if (ChargeId == Guid.Empty)
                throw new ArgumentNullException("Guid cannot be null");

            var findgradutedscalebychargeid = graduatedscalerepository.GetAll(IsolationLevel.ReadCommitted).Where(x => x.ChargeId == ChargeId);

            return findgradutedscalebychargeid.FirstOrDefault();
        }

        public IList<GraduatedScale> GetAllGraduatedScales(IsolationLevel isolationLevel)
        {
            var findallgraduatedscales = graduatedscalerepository.GetAll(isolationLevel);

            return findallgraduatedscales.ToList();
        }

        public List<GraduatedScale> FindGraduatedScaleUsingRangeAndChargeId(decimal amount, Guid ChargeId)
        {
            var findgradutedscale = (from a in datacontext.GraduatedScale
                                     where a.ChargeId == ChargeId && (a.LowerLimit <= amount && a.UpperLimit >= amount) 
                                     select a);

            return findgradutedscale.ToList();
        }

        public IList<GraduatedScale> GetAllGraduatedScalesByChargeId(Guid ChargeId, IsolationLevel isolationLevel)
        {
            if (ChargeId == Guid.Empty)
            {
                throw new ArgumentNullException("Charge Id cannot be null.");
            }

            var findgradutedscalesbychargeid = graduatedscalerepository.GetAll(isolationLevel).Where(x => x.ChargeId == ChargeId);

            return findgradutedscalesbychargeid.ToList();
        }

        public IList<GraduatedScale> CountGraduatedScaleWithChargeAndRange(Guid ChargeId, decimal LowerLimit, decimal UpperLimit)
        {
            var validatecharge = graduatedscalerepository.GetAll(IsolationLevel.ReadCommitted).Where(a => a.ChargeId == ChargeId && a.LowerLimit <= LowerLimit && a.UpperLimit >= UpperLimit);

            return validatecharge.ToList();
        }

        public GraduatedScale ValidateGraduatedScale(Guid ChargeId, decimal LowerLimit, decimal UpperLimit)
        {
            var validatecharge = graduatedscalerepository.GetAll(IsolationLevel.ReadCommitted).Where(a => a.ChargeId == ChargeId && a.LowerLimit <= LowerLimit && a.UpperLimit >= UpperLimit);

            return validatecharge.FirstOrDefault();
        } 
    }
}
